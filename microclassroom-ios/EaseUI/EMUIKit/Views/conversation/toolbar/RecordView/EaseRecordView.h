/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */


#import <UIKit/UIKit.h>

typedef enum{
    EaseRecordViewTypeTouchDown,
    EaseRecordViewTypeTouchUpInside,
    EaseRecordViewTypeTouchUpOutside,
    EaseRecordViewTypeDragInside,
    EaseRecordViewTypeDragOutside,
}EaseRecordViewType;

@interface EaseRecordView : UIView

@property (nonatomic,strong) NSArray *voiceMessageAnimationImages UI_APPEARANCE_SELECTOR;

@property (nonatomic,strong) NSString *upCancelText UI_APPEARANCE_SELECTOR;

@property (nonatomic,strong) NSString *loosenCancelText UI_APPEARANCE_SELECTOR;

-(void)recordButtonTouchDown;
-(void)recordButtonTouchUpInside;
-(void)recordButtonTouchUpOutside;
-(void)recordButtonDragInside;
-(void)recordButtonDragOutside;

@end
