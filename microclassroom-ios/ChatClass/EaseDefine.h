//
//  EaseDefine.h
//  EnergyConservationPark
//
//  Created by Hanks on 2017/4/17.
//  Copyright © 2017年 keanzhu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EaseSDKHelper.h"

static NSString *kMessageExt = @"MessageExt";
static NSString *kMessageType = @"MessageType";
static NSString *kConversationChatter = @"ConversationChatter";

@interface EaseDefine : NSObject

@end
