/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */

#import "ConversationListController.h"
#import "ChatViewController.h"
#import "EMSearchBar.h"
#import "EMSearchDisplayController.h"
#import "RealtimeSearchUtil.h"
#import "ChatDemoHelper.h"
#import "NotiResultHelper.h"

@implementation EMConversation (search)

@end

@interface ConversationListController ()<EaseConversationListViewControllerDelegate, EaseConversationListViewControllerDataSource,UISearchDisplayDelegate, UISearchBarDelegate>

@property (nonatomic, strong) UIView *networkStateView;
@property (nonatomic, strong) EMSearchBar           *searchBar;

@property (strong, nonatomic) EMSearchDisplayController *searchController;

@end

@implementation ConversationListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"我的消息";
    // Do any additional setup after loading the view.
    self.showRefreshHeader = YES;
    self.delegate = self;
    self.dataSource = self;
    [self tableViewDidTriggerHeaderRefresh];
    //    [self.view addSubview:self.searchBar];
    //    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    //    }];
    [self networkStateView];
    
//    [self searchController];
    
    [self removeEmptyConversationsFromDB];
    KAddobserverNotifiCation(@selector(refreshDataSource), @"refreshConversionDataSource");
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)removeEmptyConversationsFromDB
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSMutableArray *needRemoveConversations;
    for (EMConversation *conversation in conversations) {
        if (!conversation.latestMessage || (conversation.type == EMConversationTypeChatRoom)) {
            if (!needRemoveConversations) {
                needRemoveConversations = [[NSMutableArray alloc] initWithCapacity:0];
            }
            
            [needRemoveConversations addObject:conversation];
        }
    }
    
    if (needRemoveConversations && needRemoveConversations.count > 0) {
        [[EMClient sharedClient].chatManager deleteConversations:needRemoveConversations isDeleteMessages:YES completion:nil];
    }
}

#pragma mark - getter
- (UIView *)networkStateView
{
    if (_networkStateView == nil) {
        _networkStateView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
        _networkStateView.backgroundColor = [UIColor colorWithRed:255 / 255.0 green:199 / 255.0 blue:199 / 255.0 alpha:0.5];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (_networkStateView.frame.size.height - 20) / 2, 20, 20)];
        imageView.image = [UIImage imageNamed:@"messageSendFail"];
        [_networkStateView addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame) + 5, 0, _networkStateView.frame.size.width - (CGRectGetMaxX(imageView.frame) + 15), _networkStateView.frame.size.height)];
        label.font = [UIFont systemFontOfSize:15.0];
        label.textColor = [UIColor grayColor];
        label.backgroundColor = [UIColor clearColor];
        label.text = NSLocalizedString(@"network.disconnection", @"Network disconnection");
        [_networkStateView addSubview:label];
    }
    
    return _networkStateView;
}

- (UISearchBar *)searchBar
{
    if (!_searchBar) {
        _searchBar = [[EMSearchBar alloc] initWithFrame: CGRectMake(0, KTopHeight, self.view.frame.size.width, 44)];
        _searchBar.delegate = self;
        _searchBar.placeholder = NSLocalizedString(@"search", @"Search");
        _searchBar.backgroundColor = [UIColor colorWithRed:0.747 green:0.756 blue:0.751 alpha:1.000];
    }
    
    return _searchBar;
}

- (EMSearchDisplayController *)searchController
{
    if (_searchController == nil) {
        _searchController = [[EMSearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
        _searchController.delegate = self;
        _searchController.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _searchController.searchResultsTableView.tableFooterView = [[UIView alloc] init];
        
        __weak ConversationListController *weakSelf = self;
        [_searchController setCellForRowAtIndexPathCompletion:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath) {
            NSString *CellIdentifier = [EaseConversationCell cellIdentifierWithModel:nil];
            EaseConversationCell *cell = (EaseConversationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            // Configure the cell...
            if (cell == nil) {
                cell = [[EaseConversationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            id<IConversationModel> model = [weakSelf.searchController.resultsSource objectAtIndex:indexPath.row];
            cell.model = model;
            
            cell.detailLabel.attributedText = [weakSelf conversationListViewController:weakSelf latestMessageTitleForConversationModel:model];
            cell.timeLabel.text = [weakSelf conversationListViewController:weakSelf latestMessageTimeForConversationModel:model];
            return cell;
        }];
        
        [_searchController setHeightForRowAtIndexPathCompletion:^CGFloat(UITableView *tableView, NSIndexPath *indexPath) {
            return [EaseConversationCell cellHeightWithModel:nil];
        }];
        
        [_searchController setDidSelectRowAtIndexPathCompletion:^(UITableView *tableView, NSIndexPath *indexPath) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [weakSelf.searchController.searchBar endEditing:YES];
            id<IConversationModel> model = [weakSelf.searchController.resultsSource objectAtIndex:indexPath.row];
            EMConversation *conversation = model.conversation;
            ChatViewController *chatController;
//            if ([[RobotManager sharedInstance] isRobotWithUsername:conversation.conversationId]) {
//                chatController = [[RobotChatViewController alloc] initWithConversationChatter:conversation.conversationId conversationType:conversation.type];
//                chatController.title = [[RobotManager sharedInstance] getRobotNickWithUsername:conversation.conversationId];
//            }else {
//#ifdef REDPACKET_AVALABLE
//                chatController = [[RedPacketChatViewController alloc]
//#else
//                                  chatController = [[ChatViewController alloc]
//#endif
//                                                    initWithConversationChatter:conversation.conversationId conversationType:conversation.type];
//                                  //                chatController.title = [conversation showName];
//                                  }
                                  //            [weakSelf.navigationController pushViewController:chatController animated:YES];
                                  [weakSelf.parentContrl.navigationController pushViewController:chatController animated:YES];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"setupUnreadMessageCount" object:nil];
                                  [weakSelf.tableView reloadData];
                                  }];
            }
            
            return _searchController;
        }
         
#pragma mark - EaseConversationListViewControllerDelegate
         
         - (void)conversationListViewController:(EaseConversationListViewController *)conversationListViewController
                                     didSelectConversationModel:(id<IConversationModel>)conversationModel
        {
            if (conversationModel) {
                EMConversation *conversation = conversationModel.conversation;
                if (conversation) {
//                    if ([[RobotManager sharedInstance] isRobotWithUsername:conversation.conversationId]) {
//                        RobotChatViewController *chatController = [[RobotChatViewController alloc] initWithConversationChatter:conversation.conversationId conversationType:conversation.type];
//                        chatController.title = [[RobotManager sharedInstance] getRobotNickWithUsername:conversation.conversationId];
//                        //                [self.navigationController pushViewController:chatController animated:YES];
//                        [self.parentContrl.navigationController pushViewController:chatController animated:YES];
//                    } else {
                        //判断是否跳转
                        [conversation markAllMessagesAsRead:nil];
                        
                        [NotiResultHelper resultConversation:conversation andController:self.parentContrl];
                        
//                    }
                    
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"setupUnreadMessageCount" object:nil];
            }
        }
         
#pragma mark - EaseConversationListViewControllerDataSource
         
         - (id<IConversationModel>)conversationListViewController:(EaseConversationListViewController *)conversationListViewController
                                           modelForConversation:(EMConversation *)conversation
        {
            EaseConversationModel *model = [[EaseConversationModel alloc] initWithConversation:conversation];
            
            if (model.conversation.type == EMConversationTypeChat) {
//                if ([[RobotManager sharedInstance] isRobotWithUsername:conversation.conversationId]) {
//                    model.title = [[RobotManager sharedInstance] getRobotNickWithUsername:conversation.conversationId];
//                } else {
                    NSDictionary *ext =     model.conversation.latestMessage.ext;
                    model.title = [ext objectForKey:NICKNAME];
                    NSString * imageUrl = [ext objectForKey:PHOTOURL];
                    model.avatarURLPath = imageUrl;
                    
//                }
            } else if (model.conversation.type == EMConversationTypeGroupChat) {
                NSString *imageName = @"groupPublicHeader";
                NSDictionary *ext = conversation.latestMessage.ext;
                model.title = [ext objectForKey:GROUPNAME];
                imageName = [ext objectForKey:GROUPURL];
                model.avatarURLPath = imageName;
                
            }
            return model;
        }
         
         - (NSAttributedString *)conversationListViewController:(EaseConversationListViewController *)conversationListViewController
                         latestMessageTitleForConversationModel:(id<IConversationModel>)conversationModel
        {
            NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:@""];
            EMMessage *lastMessage = [conversationModel.conversation latestMessage];
            if (lastMessage) {
                NSString *latestMessageTitle = @"";
                EMMessageBody *messageBody = lastMessage.body;
                switch (messageBody.type) {
                    case EMMessageBodyTypeImage:{
                        latestMessageTitle = NSLocalizedString(@"message.image1", @"[image]");
                    } break;
                    case EMMessageBodyTypeText:{
                        // 表情映射。
                        NSString *didReceiveText = [EaseConvertToCommonEmoticonsHelper
                                                    convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
                        latestMessageTitle = didReceiveText;
                        if ([lastMessage.ext objectForKey:MESSAGE_ATTR_IS_BIG_EXPRESSION]) {
                            latestMessageTitle = @"[动画表情]";
                        }
                    } break;
                    case EMMessageBodyTypeVoice:{
                        latestMessageTitle = NSLocalizedString(@"message.voice1", @"[voice]");
                    } break;
                    case EMMessageBodyTypeLocation: {
                        latestMessageTitle = NSLocalizedString(@"message.location1", @"[location]");
                    } break;
                    case EMMessageBodyTypeVideo: {
                        latestMessageTitle = NSLocalizedString(@"message.video1", @"[video]");
                    } break;
                    case EMMessageBodyTypeFile: {
                        latestMessageTitle = NSLocalizedString(@"message.file1", @"[file]");
                    } break;
                    default: {
                    } break;
                }
                
                NSDictionary *ext = conversationModel.conversation.latestMessage.ext;
                lastMessage.ext = ext;
                if (lastMessage.direction == EMMessageDirectionReceive) {
                    NSString *from = [ext objectForKey:NICKNAME];
                    if (NOEmptyStr(from)){
                        latestMessageTitle = [NSString stringWithFormat:@"%@: %@", from, latestMessageTitle];
                    }
                }
                if (ext && [ext[kHaveUnreadAtMessage] intValue] == kAtAllMessage) {
                    latestMessageTitle = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"group.atAll", nil), latestMessageTitle];
                    attributedStr = [[NSMutableAttributedString alloc] initWithString:latestMessageTitle];
                    [attributedStr setAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:1.0 green:.0 blue:.0 alpha:0.5]} range:NSMakeRange(0, NSLocalizedString(@"group.atAll", nil).length)];
                    
                }
                else if (ext && [ext[kHaveUnreadAtMessage] intValue] == kAtYouMessage) {
                    latestMessageTitle = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"group.atMe", @"[Somebody @ me]"), latestMessageTitle];
                    attributedStr = [[NSMutableAttributedString alloc] initWithString:latestMessageTitle];
                    [attributedStr setAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:1.0 green:.0 blue:.0 alpha:0.5]} range:NSMakeRange(0, NSLocalizedString(@"group.atMe", @"[Somebody @ me]").length)];
                }
                else {
                    attributedStr = [[NSMutableAttributedString alloc] initWithString:latestMessageTitle];
                }
            }
            
            return attributedStr;
        }
         
         - (NSString *)conversationListViewController:(EaseConversationListViewController *)conversationListViewController
                          latestMessageTimeForConversationModel:(id<IConversationModel>)conversationModel
        {
            NSString *latestMessageTime = @"";
            EMMessage *lastMessage = [conversationModel.conversation latestMessage];;
            if (lastMessage) {
                latestMessageTime = [NSDate formattedTimeFromTimeInterval:lastMessage.timestamp];
            }
            
            
            return latestMessageTime;
        }
         
#pragma mark - UISearchBarDelegate
         
         - (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
        {
            [searchBar setShowsCancelButton:YES animated:YES];
            
            return YES;
        }
         
         - (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
        {
            __weak typeof(self) weakSelf = self;
            [[RealtimeSearchUtil currentUtil] realtimeSearchWithSource:self.dataArray searchText:(NSString *)searchText collationStringSelector:@selector(title) resultBlock:^(NSArray *results) {
                if (results) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf.searchController.resultsSource removeAllObjects];
                        [weakSelf.searchController.resultsSource addObjectsFromArray:results];
                        [weakSelf.searchController.searchResultsTableView reloadData];
                    });
                }
            }];
        }
         
         - (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
        {
            return YES;
        }
         
         - (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
        {
            [searchBar resignFirstResponder];
        }
         
         - (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
        {
            searchBar.text = @"";
            [[RealtimeSearchUtil currentUtil] realtimeSearchStop];
            [searchBar resignFirstResponder];
            [searchBar setShowsCancelButton:NO animated:YES];
        }
         
#pragma mark - public
         
         -(void)refresh
        {
            [self refreshAndSortView];
        }
         
         -(void)refreshDataSource
        {
            [self tableViewDidTriggerHeaderRefresh];
        }
         
         - (void)isConnect:(BOOL)isConnect{
             if (!isConnect) {
                 self.tableView.tableHeaderView = _networkStateView;
             }
             else{
                 self.tableView.tableHeaderView = nil;
             }
             
         }
         
         - (void)networkChanged:(EMConnectionState)connectionState
        {
            if (connectionState == EMConnectionDisconnected) {
                self.tableView.tableHeaderView = _networkStateView;
            }
            else{
                self.tableView.tableHeaderView = nil;
            }
        }
         
         - (NSString *)getFriendNameFromMsg:(EMMessage *)msg {
             
             NSString * friendName;
             switch (msg.direction) {
                 case EMMessageDirectionSend:
                     friendName = msg.to;
                     break;
                 case EMMessageDirectionReceive:
                     friendName = msg.from;
                     break;
                 default:
                     break;
             }
             
             return friendName;
         }
         
         
         @end
