//
//  PushInfoModel.m
//  CarService
//
//  Created by huwenbo on 15/8/8.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "PushInfoModel.h"

@implementation PushInfoModel
-(NSURL*)photoImageForWebURL{
    NSString* path =[NSString stringWithFormat: @"http://112.124.115.80:35000/%s",[self.photo UTF8String]];
    NSURL* url = [NSURL URLWithString:[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];//网络图片url
    
    return url;
}

-(UIImage *)photoPlaceholderImage{
    UIImage *aimage = [UIImage imageNamed:@"parkFriend_headImage"];
//    [DemoGlobalClass sharedInstance].sex==ECSexType_Female?[UIImage imageNamed:@"小头像-07"]:[UIImage imageNamed:@"小头像-07"];return aimage;
    return aimage;
}
@end
