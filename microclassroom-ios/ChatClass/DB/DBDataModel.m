//
//  DBDataModel.m
//  CarService
//
//  Created by apple on 15/7/23.
//  Copyright © 2015年 fenglun. All rights reserved.
//

#import "DBDataModel.h"
#import "FriendModel.h"
//#import "TKAddressBook.h"
#import "PushInfoModel.h"
#import "AssociatorModel.h"
#import "FMDatabaseHelper.h"
@interface DBDataModel()
//- (void)readDataFromTable;
@property (nonatomic, strong) FMDatabaseHelper* dataBaseHelper;
@end


@implementation DBDataModel




+ (instancetype)sharedDBDataModel
{
    return [[self alloc] init];
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _dataBaseHelper = [[FMDatabaseHelper alloc] initWith:[NSString stringWithFormat:@"%@.db",KgetUserValueByParaName(USERID)]];
    }
    return self;
}

#pragma mark -----视频播放进度

- (void)createDataTable_UserVideoProgress {
    if(![self.dataBaseHelper checkTableExist:TABLE_VIDEOPROGRESS]) {
        NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
        [args setObject:@"text" forKey:@"userId"];//用户ID
        [args setObject:@"text" forKey:@"videoId"];//昵称
        [args setObject:@"double" forKey:@"videoProgress"];//头象

        [self.dataBaseHelper createTableWith:TABLE_VIDEOPROGRESS args:args];
    }
}

- (void)addRowData_UserVideoProgressWithTime:(double)time videoId:(NSString *)videoId {
    
    if ([self isHave_UserVideoProgressWithUserId:KgetUserValueByParaName(USERID) videoId:videoId]) {
        
        [self update_UserVideoProgressWithUserId:KgetUserValueByParaName(USERID)
                                         videoId:videoId
                                            time:time];
        
    } else if ([self.dataBaseHelper checkTableExist:TABLE_VIDEOPROGRESS]) {
        
        NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
        [args setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
        [args setObject:videoId forKey:@"videoId"];
        [args setObject:@(time) forKey:@"videoProgress"];

        [self.dataBaseHelper insertTo:TABLE_VIDEOPROGRESS args:args];
     
    }
    
}

- (double)getCurrentTimeWithVideoId:(NSString*)videoId {
    
    double time = 0.0;
    
    if([self.dataBaseHelper checkTableExist:TABLE_VIDEOPROGRESS]) {
        FMResultSet *result = [self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select * from %@ where userId=\"%@\" and videoId=\"%@\"",TABLE_VIDEOPROGRESS,KgetUserValueByParaName(USERID), videoId]];
        while ([result next]) {
            time = [result doubleForColumn:@"videoProgress"];
        }
        [result close];
    }
    
    return time;

}

//更新进度
- (void)update_UserVideoProgressWithUserId:(NSString *)userId videoId:(NSString *)videoId time:(double)time {
    if([self.dataBaseHelper checkTableExist:TABLE_VIDEOPROGRESS]) {
        NSString *sql=[NSString stringWithFormat:@"update %@ set videoProgress=%@ where userId='%@' and videoId='%@' ",TABLE_VIDEOPROGRESS,  @(time), userId, videoId];
        [self.dataBaseHelper updatebysql:sql];
        
    }
}

//判断是否已经存在进度数据
- (BOOL)isHave_UserVideoProgressWithUserId:(NSString *)userId videoId:(NSString *)videoId {
    
    BOOL isHave = NO;
    
    if([self.dataBaseHelper checkTableExist:TABLE_VIDEOPROGRESS]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select * from %@ where userId=\"%@\" and videoId=\"%@\"   ", TABLE_VIDEOPROGRESS, userId,  videoId]];
        while ([result next]) {
            isHave = YES;
        }
        
        [result close];
    }
    
    return isHave;
}
#pragma mark -----会员表
- (void)createDataTable_Associator{
    if(![self.dataBaseHelper checkTableExist:TABLE_ASSOCIATOR]) {
        NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
        [args setObject:@"text" forKey:@"voipAccount"];//容联账号
        [args setObject:@"text" forKey:@"userId"];//用户ID
        [args setObject:@"text" forKey:@"nickName"];//昵称
        [args setObject:@"text" forKey:@"photo"];//头象
        [args setObject:@"text" forKey:@"subAccount"];//荣联视频账号
        [args setObject:@"integer" forKey:@"type"];//数据类型   1=对应会员表 2=对应洗车技师表 3=群的头像
        [self.dataBaseHelper createTableWith:TABLE_ASSOCIATOR args:args];
        [args removeAllObjects];
    }
}


// 1.判断表里有没有这条数据 2.yes 更新数据 3.no 添加数据
- (void)addRowData_Associator:(AssociatorModel *)item{
    
    if ([self isHave_Associators_userId:item] == 1){
        [self update_unread_Associators:item];
        
    }//如果friend表里面有数据 就不加载 直接更新
    
    else if([self.dataBaseHelper checkTableExist:TABLE_ASSOCIATOR]) {
        NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
        [args setObject:item.voipAccount forKey:@"voipAccount"];
        [args setObject:item.userId forKey:@"userId"];
        [args setObject:item.nickName forKey:@"nickName"];
        [args setObject:item.photo forKey:@"photo"];
        [args setObject:item.type forKey:@"type"];
        [args setObject:item.subAccount forKey:@"subAccount"];
        
        [self.dataBaseHelper insertTo:TABLE_ASSOCIATOR args:args];
        [args removeAllObjects];
    }
}


//更新已读
- (void)update_unread_Associators:(AssociatorModel *)item{
    if([self.dataBaseHelper checkTableExist:TABLE_ASSOCIATOR]) {
        NSDictionary *args = @{@"voipAccount":item.voipAccount,
                               @"nickName":item.nickName,
                               @"type":item.type,
                               @"subAccount":item.subAccount,
                               @"photo":item.photo};
        NSDictionary *wheres = @{@"userId":item.userId,@"type":item.type};
        
        [self.dataBaseHelper update:TABLE_ASSOCIATOR wheres:wheres args:args];
    }
}

//判断是否已经存在这个好友数据
- (int)isHave_Associators_userId:(AssociatorModel*)associator{
    int isHave=0;
    if([self.dataBaseHelper checkTableExist:TABLE_ASSOCIATOR]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(*) as have from %@ where userId=\"%@\" and type=\"%@\"   ",TABLE_ASSOCIATOR,associator.userId, associator.type]];
        while ([result next]) {
            isHave=[result intForColumn:@"have"];
        }
        [result close]; 
    }
    return isHave;
}

- (NSString*)readDataFromTablePhoto_Associators:(NSString*)voipAccount{
    if([self.dataBaseHelper checkTableExist:TABLE_ASSOCIATOR]) {
        NSString *photo_path=@"";
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select photo from %@ where voipAccount=\"%@\"   ",TABLE_ASSOCIATOR,voipAccount]];
        while ([result next]) {
            photo_path=[result stringForColumn:@"photo"];
        }
        [result close];
        return photo_path;
    }
    return nil;
}
-(NSString *)getDisPlayName_Associators:(NSString *)voipAccount{
    NSString *nikName=@"";
    if([self.dataBaseHelper checkTableExist:TABLE_ASSOCIATOR]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select nickName from %@ where voipAccount='%@' ",TABLE_ASSOCIATOR,voipAccount]];
        while ([result next]) {
            nikName=[result stringForColumn:@"nickName"];
        }
        [result close];
        return nikName;
    }
    return nikName;
}
- (NSString*)readDataFromTableuserId_Associators:(NSString*)voipAccount{
    if([self.dataBaseHelper checkTableExist:TABLE_ASSOCIATOR]) {
        NSString *userId=@"";
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select userId from %@ where voipAccount=\"%@\"   ",TABLE_ASSOCIATOR,voipAccount]];
        while ([result next]) {
            userId=[result stringForColumn:@"userId"];
        }
        [result close];
        return userId;
    }
    return nil;
}

//群组头像
-(NSString*) getphotoforGroup_Associators:(NSString*)groupId{
    if([self.dataBaseHelper checkTableExist:TABLE_ASSOCIATOR]) {
        NSString *photo_path=@"";
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select photo from %@ where userId=\"%@\" and type=3   ",TABLE_ASSOCIATOR,groupId]];
        while ([result next]) {
            photo_path=[result stringForColumn:@"photo"];
        }
        [result close];
        return photo_path;
    }
    return nil;
}


#pragma mark -------------------消息推送表部分---------------------------------------------
//建立
- (void)createDataTable_PushInfo{
    if(![self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
        [args setObject:@"text" forKey:@"sessionId"];//消息推送的容联账号
        [args setObject:@"text" forKey:@"userId"];//参与者的用户ID
        [args setObject:@"text" forKey:@"nikName"];//昵称
        [args setObject:@"text" forKey:@"photo"];//头象
        [args setObject:@"text" forKey:@"content"];//简要文字
        [args setObject:@"integer" forKey:@"unread"];//是否已经读 本地维护
        [args setObject:@"integer" forKey:@"dateTime"];//参与时间
        [args setObject:@"integer" forKey:@"btype"];//消息类型
        [args setObject:@"integer" forKey:@"type"];//消息类型
        [args setObject:@"integer" forKey:@"infoId"];
        [args setObject:@"text" forKey:@"josnText"];
        
        [self.dataBaseHelper createTableWith:TABLE_PUSHS args:args];
        [args removeAllObjects];
    }
}




- (void)addRowData_PushInfo:(PushInfoModel *)item{
        if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
            if([self isHaveType:item]==YES){
                NSString *sql=[NSString stringWithFormat:@"update %@ set unread=unread+1,userId='%@',nikName='%@', photo='%@',content='%@',type='%@',dateTime='%@' where  btype='%@' and infoId='%@' ",TABLE_PUSHS,item.userId,item.nikName,item.photo,item.content,item.type,item.dateTime,item.btype,item.infoId ];
                [self.dataBaseHelper updatebysql:sql];  
            }else{
                NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
                [args setObject:item.sessionId forKey:@"sessionId"];//推送发送的用户账号
                [args setObject:item.userId forKey:@"userId"];
                [args setObject:item.nikName forKey:@"nikName"];
                [args setObject:item.photo forKey:@"photo"];
                [args setObject:item.content forKey:@"content"];
                [args setObject:item.unread forKey:@"unread"];
                [args setObject:item.dateTime forKey:@"dateTime"];
                [args setObject:item.btype forKey:@"btype"];
                [args setObject:item.type forKey:@"type"];
                [args setObject:item.infoId forKey:@"infoId"];
                [self.dataBaseHelper insertTo:TABLE_PUSHS args:args];
                [args removeAllObjects];
            }

        }
}
-(BOOL)isHaveType:(PushInfoModel *)item{//判断同一类型，同一信息主体是否存在
    BOOL ishave=NO;
    FMResultSet *rscount=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(1) as unreadcount from %@  where  btype='%@' and infoId='%@' ",TABLE_PUSHS,item.btype,item.infoId]];
    while ([rscount next]) {
        int unread=[rscount intForColumn:@"unreadcount"];
        if(unread>0){
          ishave=YES; 
        }
         break;
    }
    [rscount close];
    return ishave;
}
-(PushInfoModel*)getPushitem:(FMResultSet *)result index:(int)flag{
    PushInfoModel *item = [[PushInfoModel alloc] init];
    item.sessionId=[result stringForColumn:@"sessionId"];
    item.userId=[result stringForColumn:@"userId"];
    item.nikName=[result stringForColumn:@"nikName"];
    item.photo=[result stringForColumn:@"photo"];
    MyLog(@"item.photo=====%@",item.photo);
    item.content=[result stringForColumn:@"content"];
    item.unread=[result stringForColumn:@"unread"];
    item.dateTime=[result stringForColumn:@"dateTime"];
    item.btype=[result stringForColumn:@"btype"];
    item.type=[result stringForColumn:@"type"];
    item.infoId=[result stringForColumn:@"infoId"];
    if(flag==1){//归总
        FMResultSet *rscount=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(1) as unreadcount from %@  where unread='0' and btype='%@'  ",TABLE_PUSHS,item.btype]];
        while ([rscount next]) {
            item.unreadcount=[rscount intForColumn:@"unreadcount"];
        }
        [rscount close];
    }else{
        item.unreadcount=0;
    }
    return item;
}


- (NSMutableArray*)readDataFromTablePushInfo_index{//获取动态消息类型首页
    NSMutableArray *pushInfo = [NSMutableArray array];
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        FMResultSet *rsindex=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select distinct btype  from %@  where 1=1 order by dateTime desc  ",TABLE_PUSHS]];
        while ([rsindex next]) {
            FMResultSet *result=[self.dataBaseHelper selectbysql:[NSString stringWithFormat:@"select * from %@ where  btype='%@' order by dateTime desc limit 0,1 ",TABLE_PUSHS,[rsindex stringForColumn:@"btype"]]];
            while ([result next]) {
                PushInfoModel* item=[self getPushitem:result index:1];
                [pushInfo addObject:item];
            }
            [result close];
         }
        [rsindex close];
    }
    return pushInfo;
}
- (NSMutableArray*)readPushInfosFromTableByType:(NSString *)btype{//获取某一类型的动态消息
    NSMutableArray *pushInfo = [NSMutableArray array];
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql:[NSString stringWithFormat:@"select * from %@ where  btype='%@' order by dateTime desc limit 0,50 ",TABLE_PUSHS,btype]];
        while ([result next]) {
            PushInfoModel* item=[self getPushitem:result index:0];
            [pushInfo addObject:item];
        }
        [result close];
    }
    return pushInfo;
}
- (NSMutableArray*)readPushInfosFromTable:(NSString *)sessionId{//获取某一类型的动态消息
    NSMutableArray *pushInfo = [NSMutableArray array];
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql:[NSString stringWithFormat:@"select * from %@  where sessionId=\"%@\" order by dateTime desc limit 0,500 ",TABLE_PUSHS,sessionId]];
        while ([result next]) {
            PushInfoModel* item=[self getPushitem:result index:0];
            [pushInfo addObject:item];
        }
        [result close];
    }
    return pushInfo;
}

//更新已读
- (void)update_unread_PushInfosbysessionid:(NSString *)sessionId{
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        NSString *sql=[NSString stringWithFormat:@"update %@ set unread=0 where  sessionId=\"%@\" ",TABLE_PUSHS,sessionId];
        [self.dataBaseHelper updatebysql:sql];
    }
}
- (void)update_unread_PushInfos:(PushInfoModel *)item{
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        NSString *sql=[NSString stringWithFormat:@"update %@ set unread=0 where btype=\"%@\"  and infoId=\"%@\"   ",TABLE_PUSHS,item.btype,item.infoId ];
        [self.dataBaseHelper updatebysql:sql];
    }
}
-(int)getunreadcountbyseesionid:(NSString *)sessionId{
    int unreadcount=0;
    FMResultSet *rscount=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select sum(unread) as unreadcount from %@  where   sessionId=\"%@\" ",TABLE_PUSHS,sessionId]];
    while ([rscount next]) {
        unreadcount=[rscount intForColumn:@"unreadcount"];
    }
    [rscount close];
    return unreadcount;
}

- (void)delPushItem:(PushInfoModel *)item{//删除某一条
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        NSString *sql=[NSString stringWithFormat:@"delete from  %@  where  userId=\"%@\" and type=\"%@\"  and infoId=\"%@\" and dateTime=\"%@\" ",TABLE_PUSHS,item.userId,item.type,item.infoId,item.dateTime];
        [self.dataBaseHelper updatebysql:sql];
    }
}
- (void)delPushtype:(NSString*)btype{//删除某一类型
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        NSString *sql=[NSString stringWithFormat:@"delete from  %@  where btype=\"%@\" ",TABLE_PUSHS,btype];
        [self.dataBaseHelper updatebysql:sql];
    }
}
- (void)delPushBySeesionId:(NSString *)sessionId{
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        NSString *sql=[NSString stringWithFormat:@"delete from  %@  where sessionId=\"%@\" ",TABLE_PUSHS,sessionId];
        [self.dataBaseHelper updatebysql:sql];
    }
}
- (void)delPushAll{
    if([self.dataBaseHelper checkTableExist:TABLE_PUSHS]) {
        [self.dataBaseHelper clearTable:TABLE_PUSHS];
    }
}



#pragma mark -------------------好友表部分---------------------------------------------
//建立表
- (void)createDataTable_Friend{
    if(![self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
        [args setObject:@"text" forKey:@"userId"];
        [args setObject:@"text" forKey:@"mobile"];
        [args setObject:@"text" forKey:@"userName"];
        [args setObject:@"text" forKey:@"pinying_head"];
        [args setObject:@"text" forKey:@"photo"];
        [args setObject:@"text" forKey:@"subAccount"];
        [args setObject:@"text" forKey:@"voipAccount"];
        [args setObject:@"text" forKey:@"noteName"];
        [args setObject:@"text" forKey:@"hxAccount"];
        [args setObject:@"integer" forKey:@"status"];//1=互为好友 2=待我确认 3=待对方确认 4＝通讯录内云币且未被请求的
        [self.dataBaseHelper createTableWith:TABLE_FRIENDS args:args];
        [self.dataBaseHelper createbysql:[NSString stringWithFormat:@"create index userIdindex on %@(%@)",TABLE_FRIENDS,@"userId"]];
        [args removeAllObjects];
    }
}

//清空后，向本地表中增加所有的数据
- (void)addAllRowData_Friend:(NSMutableArray *)frinds{
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        [self.dataBaseHelper clearTable:TABLE_FRIENDS];
        NSMutableArray *sql_array=[NSMutableArray array];
        for (FriendModel *item in frinds) {
            [sql_array addObject:[NSString stringWithFormat:@"insert into %@(userId,mobile,userName,photo,pinying_head,noteName,status,hxAccount)values(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\");\n",TABLE_FRIENDS,item.userId,item.mobile,item.userName,item.photo,item.pinying_head, item.noteName,item.status,item.hxAccount]];
        }
        [self.dataBaseHelper insertAutoCommitToSql:sql_array];
    }
}
- (void)del_Friends_inserver{//删除本地存在于服务器上的好友
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        NSString *sql=[NSString stringWithFormat:@"delete from  %@  where status<>4 ",TABLE_FRIENDS];
        [self.dataBaseHelper updatebysql:sql];
    }
}


//向本地表中追加所有的数据
- (void)addYunboData_Friend:(NSMutableArray *)frinds{
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        NSMutableArray *sql_array=[NSMutableArray array];
        for (FriendModel *item in frinds) {
            [sql_array addObject:[NSString stringWithFormat:@"insert into %@(userId,mobile,userName,photo,pinying_head,noteName,status,hxAccount)values(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\,\"%@\");\n",TABLE_FRIENDS,item.userId,item.mobile,item.userName,item.photo,item.pinying_head, item.noteName,item.status,item.hxAccount]];
        }
        [self.dataBaseHelper insertAutoCommitToSql:sql_array];
    }
}

//向本地表中增加一条数据
- (void)addRowData_Friend:(FriendModel *)item{
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
        [args setObject:item.userId forKey:@"userId"];
        [args setObject:item.mobile  forKey:@"mobile"];
        [args setObject:item.userName forKey:@"userName"];
        [args setObject:item.noteName forKey:@"noteName"];
        [args setObject:item.photo forKey:@"photo"];
        [args setObject:item.status forKey:@"status"];
        [args setObject:item.pinying_head forKey:@"pinying_head"];
        [args setObject:item.hxAccount forKey:@"hxAccount"];
        [self.dataBaseHelper insertTo:TABLE_FRIENDS args:args];
        [args removeAllObjects];
    }
}
//判断是否已经存在互为好友数据
- (int)isFriend_ByVoipAccount:(NSString*)voipAccount{
    int isHave=0;
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(*) as have from %@ where voipAccount=\"%@\"  and status=1 ",TABLE_FRIENDS,voipAccount]];
        while ([result next]) {
            isHave=[result intForColumn:@"have"];
        }
        [result close];
    }
    return isHave;
}
-(int)getFriendCount{
    int FriendCount=0;
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(*) as FriendCount from %@ where  status=1 ",TABLE_FRIENDS]];
        while ([result next]) {
            FriendCount=[result intForColumn:@"FriendCount"];
        }
        [result close];
    }
    return FriendCount;
}
//判断是否已经存在这个好友数据  1 表示有  0 表示 无
- (int)isHave_userid:(NSString *)userID{
    int isHave=0;
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(*) as have from %@ where userId=\"%@\"   ", TABLE_FRIENDS, userID]];
        while ([result next]) {
            isHave=[result intForColumn:@"have"];
        }
        [result close];
    }
    return isHave;
}
//读取本地数据 photo
- (NSURL*)readDataFromTablePhoto:(NSString*)voipAccount{
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        NSString *photo_path=@"";
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select photo from %@ where voipAccount=\"%@\"   ",TABLE_FRIENDS,voipAccount]];
        while ([result next]) {
            photo_path=[result stringForColumn:@"photo"];
        }
        [result close];
        
        if(NOEmptyStr(photo_path)){
            NSURL* url = [NSURL URLWithString:[photo_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];//网络图片url
            return url;
        }else{
            NSString *path= [self readDataFromTablePhoto_Associators:voipAccount
             ];
            if(NOEmptyStr(path)){
                NSURL* url = [NSURL URLWithString:[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];//网络图片url
               return url;
            }
        }

    }
    return nil;
}

//读取本地数据 getId

- (NSString*)getUserIdreadDataFromTablevoipAccount:(NSString*)voipAccount{
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        NSString *userID=@"";
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select userId from %@ where voipAccount=\"%@\"   ",TABLE_FRIENDS,voipAccount]];
        while ([result next]) {
            userID=[result stringForColumn:@"userId"];
        }
        [result close];
        
        return userID;
    }
    return nil;
}



//读取本地数据 status=1时的数据
- (NSArray*)readDataFromTableDistinctstatus{
    NSMutableArray *phoneArr = [NSMutableArray array];
    NSMutableArray *indexArray = [NSMutableArray array];
    
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        
        FMResultSet *rsindex=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select distinct pinying_head as pinying_head from %@  where pinying_head<>'#' order by pinying_head asc  ",TABLE_FRIENDS]];
        while ([rsindex next]) {
            NSMutableArray *friendarry=[self getArrayBySql:[NSString stringWithFormat:@"select * from %@ where pinying_head='%@'and status=1 order by userName asc ",TABLE_FRIENDS,[rsindex stringForColumn:@"pinying_head"]]]; //userName
            if(friendarry!=nil
               && [friendarry isKindOfClass:[NSMutableArray class]]
               && friendarry.count>0){
                [indexArray addObject:[rsindex stringForColumn:@"pinying_head"]];
                [phoneArr addObject:friendarry];
            }
        }
        [rsindex close];
        
        NSMutableArray *friendarry_other=[self getArrayBySql:[NSString stringWithFormat:@"select  * from %@ where pinying_head='#' and status=1 order by userName asc ",TABLE_FRIENDS]];
        if(friendarry_other!=nil
           && [friendarry_other isKindOfClass:[NSMutableArray class]]
           && friendarry_other.count>0){
            [indexArray addObject:@"#"];
            [phoneArr addObject:friendarry_other];
        }
        
        
    }
    MyLog(@"/////%@///// %@", phoneArr, indexArray);
    
    NSArray *arr = [NSArray arrayWithObjects:phoneArr, indexArray, nil];
    
    return arr;
}
//不在群里的好友
- (NSArray*)readDataFromTable_voipAccount:(NSString *)sql_voipAccount{
    NSMutableArray *phoneArr = [NSMutableArray array];
    NSMutableArray *indexArray = [NSMutableArray array];
    
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        
        FMResultSet *rsindex=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select distinct pinying_head as pinying_head from %@  where pinying_head<>'#' and voipAccount not in(%@) order by pinying_head asc  ",TABLE_FRIENDS,sql_voipAccount]];
        while ([rsindex next]) {
            NSMutableArray *friendarry=[self getArrayBySql:[NSString stringWithFormat:@"select * from %@ where pinying_head='%@'and status=1 and voipAccount not in(%@) order by userName asc ",TABLE_FRIENDS,[rsindex stringForColumn:@"pinying_head"],sql_voipAccount]]; //userName
            if(friendarry!=nil
               && [friendarry isKindOfClass:[NSMutableArray class]]
               && friendarry.count>0){
                [indexArray addObject:[rsindex stringForColumn:@"pinying_head"]];
                [phoneArr addObject:friendarry];
            }
        }
        [rsindex close];
        
        NSMutableArray *friendarry_other=[self getArrayBySql:[NSString stringWithFormat:@"select  * from %@ where pinying_head='#' and status=1 and voipAccount not in(%@) order by userName asc ",TABLE_FRIENDS,sql_voipAccount]];
        if(friendarry_other!=nil
           && [friendarry_other isKindOfClass:[NSMutableArray class]]
           && friendarry_other.count>0){
            [indexArray addObject:@"#"];
            [phoneArr addObject:friendarry_other];
        }
        
        
    }
    MyLog(@"/////%@///// %@", phoneArr, indexArray);
    
    NSArray *arr = [NSArray arrayWithObjects:phoneArr, indexArray, nil];
    
    return arr;
}

- (BOOL)isFriend:(NSString *)userId{//判断是否为好友
    BOOL friend=NO;
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        FMResultSet *rsindex=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(1) as friend_count from %@  where status=1 and userId=\"%@\" ",TABLE_FRIENDS,userId]];
        while ([rsindex next]) {
            if([rsindex intForColumn:@"friend_count"]>0){
                friend=YES;
            }
        }
        [rsindex close];
    }
    return friend;
}

- (NSString *)getFriendBadgeValue{//待我审核的好友
    NSString *BadgeValue=@"";
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        FMResultSet *rsindex=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(1) as badgevalue from %@  where status=2  ",TABLE_FRIENDS]];
        while ([rsindex next]) {
            BadgeValue=[rsindex stringForColumn:@"badgevalue"];
        }
        if([BadgeValue isEqualToString:@"0"]){
            BadgeValue=@"";
        }
        [rsindex close];
      
    }
    return BadgeValue;
}
- (NSArray*)readDataFromTable{
    NSMutableArray *phoneArr = [NSMutableArray array];
    NSMutableArray *indexArray = [NSMutableArray array];

    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        
        
        NSMutableArray *friendarry_other1=[self getArrayBySql:[NSString stringWithFormat:@"select  * from %@ where pinying_head='待' order by userName asc ",TABLE_FRIENDS]];
        if(friendarry_other1!=nil
           && [friendarry_other1 isKindOfClass:[NSMutableArray class]]
           && friendarry_other1.count>0){
            [indexArray addObject:@"待"];
            [phoneArr addObject:friendarry_other1];
        }

        
        FMResultSet *rsindex=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select distinct pinying_head as pinying_head from %@  where pinying_head<>'待' and pinying_head<>'#' order by pinying_head asc  ",TABLE_FRIENDS]];
        
        while ([rsindex next]) {
            NSMutableArray *friendarry=[self getArrayBySql:[NSString stringWithFormat:@"select * from %@ where pinying_head='%@' order by userName asc ",TABLE_FRIENDS,[rsindex stringForColumn:@"pinying_head"]]]; //userName
            if(friendarry!=nil
               && [friendarry isKindOfClass:[NSMutableArray class]]
               && friendarry.count>0){
                [indexArray addObject:[rsindex stringForColumn:@"pinying_head"]];
                [phoneArr addObject:friendarry];
                
            }
        }
        [rsindex close];
        
        NSMutableArray *friendarry_other=[self getArrayBySql:[NSString stringWithFormat:@"select  * from %@ where pinying_head='#' order by userName asc ",TABLE_FRIENDS]];
        if(friendarry_other!=nil
           && [friendarry_other isKindOfClass:[NSMutableArray class]]
           && friendarry_other.count>0){
            [indexArray addObject:@"#"];
            [phoneArr addObject:friendarry_other];
        }
        
        
    }
    //MyLog(@"/////%@///// %@", phoneArr, indexArray);
    
    NSArray *arr = [NSArray arrayWithObjects:phoneArr, indexArray, nil];
    
    return arr;
}




-(NSMutableArray *)getArrayBySql:(NSString *)sql{//根据sql读数据库
    NSMutableArray *addressbookarry=[NSMutableArray array];
    FMResultSet *result=[self.dataBaseHelper selectbysql:sql];
    while ([result next]) {
        FriendModel *addressBook = [[FriendModel alloc] init];
        addressBook.userId=[result stringForColumn:@"userId"];
        addressBook.userName=[result stringForColumn:@"userName"];
        addressBook.mobile=[result stringForColumn:@"mobile"];
        addressBook.noteName=[result stringForColumn:@"noteName"];
        if(IsEmptyStr(addressBook.noteName)){//huwb1021 ADD
              addressBook.noteName=[[DBDataModel sharedDBDataModel] getAddressBookName:addressBook.mobile];
        }
        addressBook.photo = [result stringForColumn:@"photo"];
        addressBook.status = [result stringForColumn:@"status"];
        addressBook.hxAccount = [result stringForColumn:@"hxAccount"];
        //MyLog(@"addressBook.status=====%@",addressBook.status);
        [addressbookarry addObject:addressBook];
    }
    [result close];
    return addressbookarry;
    
}
//更新备注
- (void)update_noteName_Friend:(NSString *)noteName uid:(NSString *)userId{
    NSDictionary *args = @{@"noteName":noteName};
    NSDictionary *wheres = @{@"userId":userId};
    [self.dataBaseHelper update:TABLE_FRIENDS wheres:wheres args:args];

}
//请求好友时 更新本地数据
- (void)updateForAddfrind:(FriendModel *)item{
    NSDictionary *args = @{@"status":item.status};
    NSDictionary *wheres = @{@"userId":item.userId};
    [self.dataBaseHelper update:TABLE_FRIENDS wheres:wheres args:args];

}

//通过好友验证时 更新本地数据
- (void)updateConfirmAddAsFriend:(FriendModel *)item{
    NSDictionary *args = @{@"status": @"1"};
    NSDictionary *wheres = @{@"userId": item.userId};
    [self.dataBaseHelper update:TABLE_FRIENDS wheres:wheres args:args];
}

//刷新 更新本地数据 1.先判断本地是否有 2.有则更新数据
- (void)updateFriendPhotoData:(FriendModel *)item{
    if ([self isHave_userid: item.userId] == 1) { // 有
        NSDictionary *args;
        if([item.status intValue] == 1){//非好友数据
            if(item.userName.length==11 && [item.userName rangeOfString:@"****"].location!=NSNotFound){
                args=@{@"noteName":item.noteName,@"photo":item.photo};
            }else{
                args=@{@"userName":item.userName, @"noteName":item.noteName,@"photo":item.photo};
            }
        }else{
            args=@{@"photo":item.photo};
        }
        NSDictionary *wheres = @{@"userId": item.userId};
       [self.dataBaseHelper update:TABLE_FRIENDS wheres:wheres args:args];
    }
}

//删除一个好友时 更新本地数据
- (void)deleteFriend:(FriendModel *)item{
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        NSDictionary *wheres = @{@"userId": item.userId};
        if ([_dataBaseHelper deleteFrom:TABLE_FRIENDS wheres:wheres]) {
            MyLog(@"删除一个好友成功");
        }
    }
}
//获取好友名字
-(NSString *)getDisPlayName:(NSString *)subAccount{
    NSString *username=@"";
    NSString *noteName=@"";
     if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
    FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select noteName,userName from %@ where voipAccount='%@'   ",TABLE_FRIENDS,subAccount]];
    while ([result next]) {
        username=[result stringForColumn:@"userName"];
        noteName=[result stringForColumn:@"noteName"];
    }
    [result close];
     }
    
    
    if(NOEmptyStr(noteName)){
        return noteName;
    }else if(NOEmptyStr(username)){
        return username;
    }else if(IsEmptyStr(noteName) && IsEmptyStr(username)){//
        NSString *name = [self getDisPlayName_Associators:subAccount];
        return name;
    }
    return @"";
}

 
//获取帮帮
-(NSString *)getBangbangDisPlayName:(NSString *)UserId{
    NSString *noteName=@"";
    if([self.dataBaseHelper checkTableExist:TABLE_FRIENDS]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select noteName from %@ where userId='%@'   ",TABLE_FRIENDS,UserId]];
        while ([result next]) {
            noteName=[result stringForColumn:@"noteName"];
        }
        [result close];
    }
    
    if(NOEmptyStr(noteName)){
        return noteName;
    }
    return @"";
}
#pragma mark -----通讯录部分---------------------------------------------
-(NSString *)getAddressBookName:(NSString *)Tel{
    NSString *name=@"";
    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select name from %@ where tel='%@'   ",TABLE_ADDRESSBOOKS,Tel]];
        while ([result next]) {
            name=[result stringForColumn:@"name"];
        }
        [result close];
    }
    
    if(NOEmptyStr(name)){
        return name;
    }
    return @"";
}
//建立表
- (void)createDataTable{
    NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
    if(![self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
        [args setObject:@"text" forKey:@"userid"];
        [args setObject:@"text" forKey:@"name"];
        [args setObject:@"text" forKey:@"tel"];
        [args setObject:@"text" forKey:@"pinyin"];
        [args setObject:@"text" forKey:@"pinyinHeard"];
        [args setObject:@"integer" forKey:@"isYunBo"];
        [self.dataBaseHelper createTableWith:TABLE_ADDRESSBOOKS args:args];
        [self.dataBaseHelper createbysql:[NSString stringWithFormat:@"create index pinyinHeard_index  on %@(%@)",TABLE_ADDRESSBOOKS,@"pinyinHeard"]];
        [args removeAllObjects];
    }
}
////向本地表中增加所有的数据
//- (void)addAllRowData:(NSMutableArray *)addressBooks{
//    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
//        [self.dataBaseHelper clearTable:TABLE_ADDRESSBOOKS];
//        NSMutableArray *sql_array=[NSMutableArray array];
//        for (TKAddressBook *addressBook in addressBooks) {
//            [sql_array addObject:[NSString stringWithFormat:@"insert into %@(name,tel,pinyin,pinyinHeard,isYunBo)values(\"%@\",\"%@\",\"%@\",\"%@\",0);\n",TABLE_ADDRESSBOOKS,addressBook.name,addressBook.tel,addressBook.pinyin,[addressBook.pinyinHeard substringToIndex:1]]];
//        }
//        [self.dataBaseHelper insertAutoCommitToSql:sql_array];
//    }
//}


////本地表中所有手机号码读取数据,
//- (NSMutableArray *)readTKAddressBookFromTable:(NSString *)keyword{
//    NSMutableArray *addressbookarry=[NSMutableArray array];
//    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
//        FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select * from %@  where name like '%@%@%@",TABLE_ADDRESSBOOKS,@"%",keyword,@"%"]];
//        while ([result next]) {
//            TKAddressBook *addressBook = [[TKAddressBook alloc] init];
//            addressBook.name=[result stringForColumn:@"name"];
//            addressBook.tel=[result stringForColumn:@"tel"];
//            addressBook.pinyin=[result stringForColumn:@"pinyin"];
//            addressBook.pinyinHeard=[result stringForColumn:@"pinyinHeard"];
//            addressBook.isYunBo=[result intForColumn:@"isYunBo"];
//            [addressbookarry addObject:addressBook];
//        }
//        [result close];
//    }
//    return addressbookarry;
//}
-(NSMutableArray *)getAddressIndexArray{
     NSMutableArray* _indexArray = [NSMutableArray array];
   if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
    FMResultSet *rsindex=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select distinct pinyinHeard as pinyinHeard from %@  where pinyinHeard<>'#' and pinyinHeard<>'云' order by pinyinHeard asc  ",TABLE_ADDRESSBOOKS]];
    while ([rsindex next]) {
        [_indexArray addObject:[rsindex stringForColumn:@"pinyinHeard"]];
    }
    [rsindex close];
    }
    return _indexArray;
}


//-(NSMutableArray *)getAddressArray:(NSMutableArray*)IndexArray{
//    NSMutableArray* _AddressArray = [NSMutableArray array];
//    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
//        [IndexArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
//            NSMutableArray *addressbookarry=[NSMutableArray array];
//            FMResultSet *result=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select distinct name,tel,isYunBo from %@ where pinyinHeard='%@' order by name asc ",TABLE_ADDRESSBOOKS,obj]];
//            while ([result next]) {
//                TKAddressBook *addressBook = [[TKAddressBook alloc] init];
//                addressBook.name=[result stringForColumn:@"name"];
//                addressBook.tel=[result stringForColumn:@"tel"];
//                //addressBook.pinyin=[result stringForColumn:@"pinyin"];
//                //addressBook.pinyinHeard=[result stringForColumn:@"pinyinHeard"];
//                addressBook.isYunBo=[result intForColumn:@"isYunBo"];
//                [addressbookarry addObject:addressBook];
//            }
//            [result close];
//            [_AddressArray addObject:addressbookarry];
//        }];
//        
//    }
//    return _AddressArray;
//}



////向本地表中增加一条数据
//- (void)addRowData:(TKAddressBook *)addressBook{
//    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
//        NSMutableDictionary* args = [[NSMutableDictionary alloc] init];
//        [args setObject:addressBook.name forKey:@"name"];
//        [args setObject:addressBook.tel  forKey:@"tel"];
//        [args setObject:addressBook.pinyin forKey:@"pinyin"];
//        if(![addressBook.pinyinHeard isEqualToString:@""]){
//            [args setObject:[addressBook.pinyinHeard substringToIndex:1] forKey:@"pinyinHeard"];
//        }else{
//            [args setObject:@"" forKey:@"pinyinHeard"];
//        }
//        [args setObject:@"0" forKey:@"isYunBo"];
//        [self.dataBaseHelper insertTo:TABLE_ADDRESSBOOKS args:args];
//        [args removeAllObjects];
//    }
//}
//本地表中读取数据满足条件的
- (NSMutableArray *)readTelDataFromTable{
    NSMutableArray *telarry=[NSMutableArray array];
    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
        FMResultSet *rstel=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select distinct tel from %@  where  length(tel)=11 and tel not in (select mobile as tel from %@) ",TABLE_ADDRESSBOOKS,TABLE_FRIENDS]];
        while ([rstel next]) {
            //MyLog(@"tel====%@\n",[rstel stringForColumn:@"tel"]);
            [telarry addObject:[rstel stringForColumn:@"tel"]];
        }
        [rstel close];
    }
    return telarry;
}
//本地表中读取数据有多少条的
- (int)readRowCount{
    int rowcount=0;
    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
        FMResultSet *rsrowcount=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select count(*) as rowcount from %@  ",TABLE_ADDRESSBOOKS]];
        while ([rsrowcount next]) {
            rowcount=[rsrowcount intForColumn:@"rowcount"];
        }
        [rsrowcount close];
    }
    return rowcount;
}

//更新本地表中数据 arrytel所有为用户的手机号码
- (void)updateDataYunBo:(NSMutableArray *)frinds{
    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
        [self.dataBaseHelper updatebysql:[NSString stringWithFormat:@"update %@ set  isYunBo=0 ",TABLE_ADDRESSBOOKS]];
        for (FriendModel *item in frinds) {
            [self.dataBaseHelper updatebysql:[NSString stringWithFormat:@"update %@ set  pinyinHeard='云' ,isYunBo=1 where tel='%@' ",TABLE_ADDRESSBOOKS,item.mobile]];
        }
    }
    
}
//读取通讯录中不在好友表里的所有手机号码
- (NSMutableArray *)readAddressTelsNotInFrinds{
    NSMutableArray *telarry=[NSMutableArray array];
    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
        FMResultSet *rstel=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select distinct tel from %@  where  length(tel)=11 and tel not in (select mobile as tel from %@) ",TABLE_ADDRESSBOOKS,TABLE_FRIENDS]];
        while ([rstel next]) {
            //MyLog(@"tel====%@\n",[rstel stringForColumn:@"tel"]);
            [telarry addObject:[rstel stringForColumn:@"tel"]];
        }
        [rstel close];
    }
    return telarry;
}

//通过tel读取其在通讯录中的姓名
-(NSString *)getNameBytel:(NSString *)tel{
    NSString *name=@"";
    if([self.dataBaseHelper checkTableExist:TABLE_ADDRESSBOOKS]) {
        FMResultSet *rsrow=[self.dataBaseHelper selectbysql: [NSString stringWithFormat:@"select name  from %@  where tel=\"%@\"",TABLE_ADDRESSBOOKS,tel]];
        while ([rsrow next]) {
            name=[rsrow stringForColumn:@"name"];
        }
        [rsrow close];
    }
    if([name isEqualToString:@""]){
        name=tel;
    }
    return name;
}
#pragma mark ------------公共




@end
