//
//  DBDataModel.h
//  CarService
//
//  Created by apple on 15/7/23.
//  Copyright © 2015年 fenglun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FriendModel;
@class TKAddressBook;
@class PushInfoModel;
@class AssociatorModel;
// .h文件
#define XPSingletonH(name) +(instancetype)shared##name;

// .m文件
#define XPSingletonM(name) \
static id _instance; \
\
+ (id)allocWithZone:(struct _NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
\
+ (instancetype)shared##name \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
return _instance; \
}


@interface DBDataModel : NSObject
//XPSingletonH(DBDataModel)

+ (instancetype)sharedDBDataModel;//初始化

#pragma mark -----视频播放进度

- (void)createDataTable_UserVideoProgress;/**< 创建视频播放进度表  */
- (void)addRowData_UserVideoProgressWithTime:(double)time videoId:(NSString *)videoId;
- (double)getCurrentTimeWithVideoId:(NSString*)videoId;


#pragma mark -----会员表

- (void)createDataTable_Associator;/**< 创建会员表  */
-(void)addRowData_Associator:(AssociatorModel *)item;/**< 插入一条数据 1.判断表里有没有这条数据 2.yes 更新数据 3.no 添加数据   */
//- (void)update_unread_Associators:(AssociatorModel *)item;/**< 更新已读  */
-(NSString*) getphotoforGroup_Associators:(NSString*)groupId;
-(NSString*)readDataFromTableuserId_Associators:(NSString*)voipAccount;
#pragma mark -----消息部分

- (void)createDataTable_PushInfo;
- (void)addRowData_PushInfo:(PushInfoModel *)item;
- (NSMutableArray*)readDataFromTablePushInfo_index;
- (NSMutableArray*)readPushInfosFromTableByType:(NSString *)btype;
- (NSMutableArray*)readPushInfosFromTable:(NSString *)sessionId;
- (void)update_unread_PushInfosbysessionid:(NSString *)sessionId;
- (void)update_unread_PushInfos:(PushInfoModel *)item;
-(int)getunreadcountbyseesionid:(NSString *)sessionId;
- (void)delPushtype:(NSString*)btype;
- (void)delPushItem:(PushInfoModel *)item;
- (void)delPushAll;
- (void)delPushBySeesionId:(NSString *)sessionId;
#pragma mark -----好友部分

-(int)getFriendCount;
-(NSString *)getDisPlayName:(NSString *)subAccount;
-(NSString *)getBangbangDisPlayName:(NSString *)UserId;//帮帮 需要显示的昵称
- (BOOL)isFriend:(NSString *)userId;//判断该id是否为你的那友
- (NSString *)getFriendBadgeValue;//获取好友表中待处理的记录数
- (NSArray*)readDataFromTable_voipAccount:(NSString *)sql_voipAccount;
- (NSArray*)readDataFromTableDistinctstatus;/**< 读取本地数据 status=1时的数据  */
- (NSArray*)readDataFromTable;/**< 读取本地数据  */
- (NSString*)getUserIdreadDataFromTablevoipAccount:(NSString*)voipAccount;
- (void)updateConfirmAddAsFriend:(FriendModel *)item;/**< 通过好友验证时 更新本地数据  */

- (void)updateForAddfrind:(FriendModel *)item;/**< 请求好友时 更新本地数据  */
- (void)updateFriendPhotoData:(FriendModel *)item;/**< 刷新 头像本地数据  */
- (void)deleteFriend:(FriendModel *)item; /**< 删除好友时 更新本地数据  */

- (void)update_noteName_Friend:(NSString *)noteName uid:(NSString *)userId;//更新备注

- (void)createDataTable_Friend;/**< 建立Friend表  */
- (void)del_Friends_inserver;

- (void)addAllRowData_Friend:(NSMutableArray *)frinds;/**< 清空后，向本地表中增加所有的数据  */

- (void)addYunboData_Friend:(NSMutableArray *)frinds;/**< 向本地表中追加所有的数据  */
- (void)addRowData_Friend:(FriendModel *)item;
- (int)isFriend_ByVoipAccount:(NSString*)voipAccount;
- (int)isHave_userid:(NSString *)userID;/**<  用用户ID判断是否有这条信息  */
- (NSURL*)readDataFromTablePhoto:(NSString*)voipAccount;/**< 读取本地数据 photo URL  */

#pragma mark -----通讯录部分
-(NSString *)getAddressBookName:(NSString *)Tel;
//建立表
- (void)createDataTable;
//向本地表中增加所有的数据
- (void)addAllRowData:(NSMutableArray *)addressBooks;
//本地表中所有手机号码读取数据,
- (NSMutableArray *)readTKAddressBookFromTable:(NSString *)keyword;
//向本地表中增加一条数据
- (void)addRowData:(TKAddressBook *)addressBook;
//本地表中读取数据满足条件的
- (NSMutableArray *)readTelDataFromTable;
//本地表中读取数据有多少条的
- (int)readRowCount;
- (NSMutableArray *)readAddressTelsNotInFrinds;
//更新本地表中数据 arrytel所有为用户的手机号码
- (void)updateDataYunBo:(NSMutableArray *)frinds;
-(NSString *)getNameBytel:(NSString *)tel;
-(NSMutableArray *)getAddressIndexArray;
-(NSMutableArray *)getAddressArray:(NSMutableArray*)IndexArray;


@end
