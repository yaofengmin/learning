//
//  PushInfoModel.h
//  CarService
//
//  Created by huwenbo on 15/8/8.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//


#import "YHBaseModel.h"
@interface PushInfoModel : YHBaseModel

@property (nonatomic ,copy) NSString *sessionId;
@property (nonatomic ,copy) NSString *userId;
@property (nonatomic, copy) NSString *nikName;
@property (nonatomic ,copy) NSString *photo;
@property (nonatomic ,copy) NSString *content;
@property (nonatomic ,copy) NSString  *unread;
@property (nonatomic ,copy) NSString  *dateTime;
@property (nonatomic ,copy) NSString  *btype;
@property (nonatomic ,copy) NSString  *type;
@property (nonatomic , strong) NSString *infoId;
@property (nonatomic , strong) NSString *josnText;
@property int  unreadcount;
- (NSURL*)photoImageForWebURL;
- (UIImage*)photoPlaceholderImage;@end
