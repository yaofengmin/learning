//
//  NotiResultHelper.m
//  MicroClassroom
//
//  Created by Hanks on 2016/10/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "NotiResultHelper.h"
#import "PersonHomeViewController.h"
//#import "EMConversation.h"
#import "FriendModel.h"
#import "CommentdetailViewController.h"
#import "WFMessageBody.h"
#import "YMTextData.h"
#import "ChatViewController.h"
#import "ClassDetailViewController.h"
@implementation NotiResultHelper

+(void)resultConversation:(EMConversation *)conversation andController:(UIViewController *)contr {
    NSDictionary *ext = conversation.latestMessage.ext;
    switch ([[ext objectForKey:@"type"] integerValue]) {
        case USER_ADD_REQUEST:{
                PersonHomeViewController *per = [[PersonHomeViewController alloc]init];
                per.userId = [[ext objectForKey:@"infoId"] intValue];
            [contr.navigationController pushViewController:per animated:YES];
        }
            break;
        case USER_ADD_PASSED:{
            PersonHomeViewController *per = [[PersonHomeViewController alloc]init];
            per.userId = [[ext objectForKey:@"infoId"] intValue];
            [contr.navigationController pushViewController:per animated:YES];
        }
            break;
        case USER_DELETED:{
            PersonHomeViewController *per = [[PersonHomeViewController alloc]init];
            per.userId = [[ext objectForKey:@"infoId"] intValue];
            [contr.navigationController pushViewController:per animated:YES];
        }
            break;
        case CIRCLE_PUB_AT:{}
            break;
        case CIRCLE_THUMB:{
            GetMessagesType type;
            switch ([ext[@"data"][@"message_ctype"] integerValue]) {
                case GetMessagesTypeJiangHu:{
                    type = GetMessagesTypeJiangHu;
                }
                    break;
                case GetMessagesTypeStudyGroup:{
                    type = GetMessagesTypeStudyGroup;
                }
                    break;
                default:
                    break;
            }
        [self requestDetailWithId:ext[@"infoId"] andMsgType:type controll:contr];
        }
            break;
        case CIRCLE_COMMENT:{
            GetMessagesType type;
            switch ([ext[@"data"][@"message_ctype"] integerValue]) {
                case GetMessagesTypeJiangHu:{
                    type = GetMessagesTypeJiangHu;
                }
                    break;
                case GetMessagesTypeStudyGroup:{
                    type = GetMessagesTypeStudyGroup;
                }
                    break;
                default:
                    break;
            }
            [self requestDetailWithId:ext[@"infoId"] andMsgType:type controll:contr];
   
        }
            break;
        case CIRCLE_COMMENT_RESPONSE:{
            GetMessagesType type;
            switch ([ext[@"data"][@"message_ctype"] integerValue]) {
                case GetMessagesTypeJiangHu:{
                    type = GetMessagesTypeJiangHu;
                }
                    break;
                case GetMessagesTypeStudyGroup:{
                    type = GetMessagesTypeStudyGroup;
                }
                    break;
                default:
                    break;
            }
            [self requestDetailWithId:ext[@"infoId"] andMsgType:type controll:contr];
            
        }
            break;
        case CIRCLE_HELP_ADD:{
            GetMessagesType type;
            switch ([ext[@"data"][@"message_ctype"] integerValue]) {
                case GetMessagesTypeJiangHu:{
                    type = GetMessagesTypeJiangHu;
                }
                    break;
                case GetMessagesTypeStudyGroup:{
                    type = GetMessagesTypeStudyGroup;
                }
                    break;
                default:
                    break;
            }
            [self requestDetailWithId:ext[@"infoId"] andMsgType:type controll:contr];
            
        }
            break;
        case CIRCLE_HELP_ACCEPT:{
            GetMessagesType type;
            switch ([ext[@"data"][@"message_ctype"] integerValue]) {
                case GetMessagesTypeJiangHu:{
                    type = GetMessagesTypeJiangHu;
                }
                    break;
                case GetMessagesTypeStudyGroup:{
                    type = GetMessagesTypeStudyGroup;
                }
                    break;
                default:
                    break;
            }
            [self requestDetailWithId:ext[@"infoId"] andMsgType:type controll:contr];
            
        }
            break;
        case CIRCLE_HELP_ACCEPT_FAILD:{
            GetMessagesType type;
            switch ([ext[@"data"][@"message_ctype"] integerValue]) {
                case GetMessagesTypeJiangHu:{
                    type = GetMessagesTypeJiangHu;
                }
                    break;
                case GetMessagesTypeStudyGroup:{
                    type = GetMessagesTypeStudyGroup;
                }
                    break;
                default:
                    break;
            }
            [self requestDetailWithId:ext[@"infoId"] andMsgType:type controll:contr];
            
        }
            break;
        case CIRCLE_HELP_FINISH:{
            GetMessagesType type;
            switch ([ext[@"data"][@"message_ctype"] integerValue]) {
                case GetMessagesTypeJiangHu:{
                    type = GetMessagesTypeJiangHu;
                }
                    break;
                case GetMessagesTypeStudyGroup:{
                    type = GetMessagesTypeStudyGroup;
                }
                    break;
                default:
                    break;
            }
            [self requestDetailWithId:ext[@"infoId"] andMsgType:type controll:contr];
            
        }
            break;
        case CIRCLE_HELP_FINISH_FAILD:{
            GetMessagesType type;
            switch ([ext[@"data"][@"message_ctype"] integerValue]) {
                case GetMessagesTypeJiangHu:{
                    type = GetMessagesTypeJiangHu;
                }
                    break;
                case GetMessagesTypeStudyGroup:{
                    type = GetMessagesTypeStudyGroup;
                }
                    break;
                default:
                    break;
            }
            [self requestDetailWithId:ext[@"infoId"] andMsgType:type controll:contr];
        }
            break;
        case CLASS_JOIN:{ // 申请加入
            
            NSMutableDictionary *paramDic = @{Service:AppAcceptJoinClasses,
                                       @"userId":KgetUserValueByParaName(USERID),
                                       @"classId":ext[@"infoId"],
                                       @"JoinUserId":ext[@"fromId"]
                                       }.mutableCopy;
            
            [YHJHelp aletWithTitle:@"班级申请" Message:[NSString stringWithFormat:@"%@申请加入班级",ext[@"fromName"]] sureTitle:@"同意" CancelTitle:@"拒绝" SureBlock:^{
                [paramDic setObject:@"1" forKey:@"state"];
               [MBProgressHUD showHUDAddedTo:contr.view animated:YES];
                [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                    [MBProgressHUD hideHUDForView:contr.view animated:YES];                    if (result) {
                            [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    }
                }];
                
            } andCancelBlock:^{
                [paramDic setObject:@"2" forKey:@"state"];
                [MBProgressHUD showHUDAddedTo:contr.view animated:YES];
                [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                    [MBProgressHUD hideHUDForView:contr.view animated:YES];                    if (result) {
                        [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                       
                    }
                }];
            }andDelegate:contr];
        }
            break;
        case CLASS_ACCEPTJOIN: {
            ClassDetailViewController *detail = [[ClassDetailViewController alloc]initWithClassId:ext[@"infoId"]];
            [contr.navigationController pushViewController:detail animated:YES];
        }
            break;
        case  CLASS_FAILDJOIN:{
        }
            break;
        case APPADMIN:{
        }
            break;
        default: {
            FriendModel *model = [FriendModel new];
            model.nickname = [ext objectForKey:NICKNAME];
            model.userId = [ext objectForKey:@"fromId"];
            model.photo  = [ext objectForKey:PHOTOURL];
            
            ChatViewController *chatController = [[ChatViewController alloc]
                                                  initWithConversationChatter:conversation.conversationId conversationType:conversation.type andData:model];
            chatController.groupName = [ext objectForKey:GROUPNAME];
            chatController.groupUrl  = [ext objectForKey:GROUPURL];
            [contr.navigationController pushViewController:chatController animated:YES];
            

        }
            break;
    }
}


+(void)requestDetailWithId:(NSString *)msgId andMsgType:(GetMessagesType) type controll:(UIViewController *)cotr {
    [MBProgressHUD showHUDAddedTo:cotr.view animated:YES];
    NSDictionary *paramDic = @{Service:CircleGetMessageInfo,
                               @"msgId":msgId};
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:cotr.view animated:YES];
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                WFMessageBody *model = [WFMessageBody   mj_objectWithKeyValues:result[REQUEST_INFO]];
                YMTextData *ymData = [[YMTextData alloc] init ];
                ymData.messageBody = model;
                CommentdetailViewController *detailC = [[CommentdetailViewController alloc]initWithShowMessageType:type andClassId:@""];
                detailC.ymData = ymData;
                [cotr.navigationController pushViewController:detailC animated:YES];
            }else {
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
    }];
}











@end
