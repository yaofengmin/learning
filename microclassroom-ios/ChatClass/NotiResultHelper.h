//
//  NotiResultHelper.h
//  MicroClassroom
//
//  Created by Hanks on 2016/10/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>
@class EMConversation;
@interface NotiResultHelper : NSObject

+(void)resultConversation:(EMConversation *)conversation andController:(UIViewController *)contr;


@end
