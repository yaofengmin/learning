//
//  HTTPManager.m
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "HTTPManager.h"
#import "AFAppNetClient.h"
#import "YhjRootViewController.h"
#import "YHNavgationController.h"
#import "UserInfoModel.h"
#import "LoginViewController.h"

#define SHOWSERVICE   @""

@implementation HTTPManager

+ (NSURLSessionDataTask *)getDataWithApiPre:(NSString *)apiPre GetParameters:(NSDictionary *)getParameters andPostParameters:(NSDictionary *)postParameters andBlocks:(void (^)(NSDictionary *result))block{
    
    
    if (!postParameters) {

        return [[AFAppNetClient sharedClient] GET:apiPre parameters:getParameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSNumber *ret = [responseObject objectForKey:@"ret"];
            NSDictionary *resultdic = (NSDictionary *)responseObject;
            if ([ret isEqualToNumber:@200]) {
                if (block) {
                    block(resultdic[@"data"]);
                }
            }else if ([ret isEqualToNumber:@401])
            {
                if (NOEmptyStr(KgetUserValueByParaName(USERID))) {
                    [self logout];
                }
            }else
            {
                [WFHudView showMsg:responseObject[@"msg"] inView:nil];
                block(nil);
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if (error.code == -1001) {
                [WFHudView showMsg:@"请求超时,请检查网络" inView :nil];
            }
        }];
    
    }else {
        if ([[getParameters objectForKey:@"pics"] count]) {
           
            return [[AFAppNetClient sharedClient] POST:apiPre parameters:postParameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                
                NSString *fileLoc =KgetUserValueByParaName(USERID);
                
                if (NOEmptyStr([getParameters objectForSafeKey:@"fileLoc"])) {
                    fileLoc = [getParameters objectForSafeKey:@"fileLoc"];
                }
                NSArray *imageArr = getParameters[@"pics"];
                for(int i= 0 ; i< imageArr.count; i++){
                    NSData *imageData = imageArr[i];
                    
                    [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"%@[]",fileLoc] fileName:[NSString stringWithFormat:@"%d.jpg",i+1] mimeType:@"image/jpeg"];
                }
            } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                NSNumber *ret = [responseObject objectForKey:@"ret"];
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if ([ret isEqualToNumber:@200]) {
                    
                    block(resultdic[@"data"]);
                    
                    
                }else if ([ret isEqualToNumber:@401])
                {
                    [self logout];
                }else
                {
                    [WFHudView showMsg:responseObject[@"msg"] inView:nil];
                    block(nil);
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                if (error.code == -1001) {
                    [WFHudView showMsg:@"请求超时,请检查网络" inView :nil];
                }
                block(nil);
            }];
        }else{

            return [[AFAppNetClient sharedClient] POST:apiPre parameters:postParameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if([[postParameters objectForKey:@"service"]
                    isEqualToString:SHOWSERVICE]){
                    NSLog(@"%@",responseObject);
                }
                
                NSNumber *ret = [responseObject objectForKey:@"ret"];
                if ([ret isEqualToNumber: @200]) {
                    
                    if (block) {
                        block(resultdic[@"data"]);
                    }
                } else if ([ret isEqualToNumber:@401])
                {
                    [self logout];
                }
                else
                {
                    block(nil);
                    
                    [WFHudView showMsg:responseObject[@"msg"] inView:nil];
                }
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                if (error.code == -1001) {
                    [WFHudView showMsg:@"请求超时,请检查网络" inView :nil];
                }
                block(nil);
            }];
        }
    }
    
}



+ (NSURLSessionDataTask *)getDataWithUrl:(NSString *)urlPath andPostParameters:(NSDictionary *)postPatameters andBlocks:(void (^)(NSDictionary *result))block
{
 
    return [self getDataWithApiPre:urlPath GetParameters:nil andPostParameters:[self configCommonPamerDic:postPatameters] andBlocks:block];
}


+(NSURLSessionDataTask *) publishCarCirclWithUrl:(NSString *)urlPath andPostParameters:(NSDictionary *)postParamers andImageDic:(NSDictionary *)imagePatamer andBlocks:(void(^)(NSDictionary *result)) block
{
    return [self getDataWithApiPre:urlPath GetParameters:imagePatamer andPostParameters:[self configCommonPamerDic:postParamers] andBlocks:block];
    
}



+(NSDictionary *)configCommonPamerDic:(NSDictionary *)configDic {
    
    NSMutableDictionary *sendDic = [NSMutableDictionary dictionaryWithDictionary:configDic];
    NSString *appVersion = [NSString stringWithFormat:@"IOS_%@",APPversion];
    [sendDic setObject:appVersion forKey:@"res"];
    [sendDic setObject:[NSString CurrentTime1970] forKey:@"time"];
    [sendDic setObject:@"go2tostudy87d3c6e2c54bbnhe9693282c75e2ef795d3" forKey:@"key"];
    NSString *userId =[sendDic objectForSafeKey:USERID];
    
    //请求有userId时 需要 拼secret
    if (IsEmptyStr(userId)) {//没有userId 这个参数就不需要拼接secret
    }else{
//        NSString *resStr =[UserInfoModel getInfoModel].secret;
        NSString *resStr = KgetUserValueByParaName(LoginSecret);
        if (resStr) {
            [sendDic setObject:resStr forKey:@"secret"];
        }
    }
    NSMutableString *resultStr = [[NSMutableString alloc]init];
    NSArray *newKeys = [[sendDic allKeys] sortedArrayUsingSelector:@selector(compare:)];
    for (NSString *key in newKeys) {
        id value =sendDic[key];
        if (![value isKindOfClass:[NSDictionary class]]&&![value isKindOfClass:[NSArray class]])
        {
            NSString *strValue = [NSString stringWithFormat:@"%@",value];
            if (IsEmptyStr(strValue)) {
                [sendDic removeObjectForKey:key];
            }else
            {
                [resultStr appendFormat:@"%@=%@&",key,sendDic[key]];
                
            }
        }
    }
    
    NSString *MD5Str = [NSString md5:[resultStr substringToIndex:resultStr.length-1]];
    
    
    [sendDic setObject:MD5Str forKey:@"sign"];
    [sendDic removeObjectForKey:@"secret"];
    if([[sendDic objectForKey:@"service"]
        
        isEqualToString:SHOWSERVICE]){
        NSLog(@"sendDic = %@",sendDic);
        NSLog(@"MD5Str = %@",MD5Str);
        NSLog(@"resultStr =%@",resultStr);
        
    }
    
    return sendDic;
}


#pragma mark  ===退出===
+ (void)logout {
   
    KPostNotifiCation(KNOTIFICATION_LOGINCHANGE, @(NO));
    
}


@end
