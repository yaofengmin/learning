//
//  AFAppNetClient.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AFAppNetClient.h"

static AFAppNetClient *_shareClient = nil;

@implementation AFAppNetClient

+(instancetype) sharedClient
{
    
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
       
        _shareClient = [[super allocWithZone:NULL]init];
        _shareClient.responseSerializer = [AFJSONResponseSerializer serializer];
        _shareClient.requestSerializer.timeoutInterval = 30;
        _shareClient.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/xml", @"text/plain",nil];
        
      
        
    });
//
//    DELAYEXECUTE(1,
//
//                 if (![YHJHelp isReachable]) {
//                     [WFHudView showMsg:@"您的网络连接有点问题哦~" inView:nil];
//                 } );
    
    return _shareClient;
    
}

@end
 
