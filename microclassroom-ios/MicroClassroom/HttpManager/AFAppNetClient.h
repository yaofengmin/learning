//
//  AFAppNetClient.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface AFAppNetClient : AFHTTPSessionManager

+(instancetype) sharedClient;

@end
