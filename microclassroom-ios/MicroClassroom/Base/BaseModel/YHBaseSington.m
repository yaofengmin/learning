//
//  YHBaseSington.m
//  yunbo2016
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHBaseSington.h"



@implementation YHBaseSington

+(instancetype)shareInstance
{
    return [[super alloc]init];
}


- (void)showAlterWithMessage:(NSString*)message{
    UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alter show];
}


@end
