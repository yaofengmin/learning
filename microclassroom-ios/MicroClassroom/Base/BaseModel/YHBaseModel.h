//
//  YHBaseModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"

@interface YHBaseModel : NSObject

-(void)setValue:(id)value forUndefinedKey:(NSString *)key;
+(void)printWithPropertyStr:(NSDictionary *)dataDic;
-(void)encodeWithCoder:(NSCoder *)aCoder;
- (instancetype)initWithCoder:(NSCoder *)coder;


@end
