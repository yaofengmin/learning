//
//  YHBaseModel.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"
#import <objc/runtime.h>


@implementation YHBaseModel


-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
    MyLog(@"找不到这个Key－－－－－－%@",key);
}


+(void)printWithPropertyStr:(NSDictionary *)dataDic
{
    NSMutableString *strM = [NSMutableString string];
    [dataDic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        
        NSString *type;
        
        if ([obj isKindOfClass:NSClassFromString(@"__NSCFArray")]){
            type = @"NSArray";
            
        }else if ([obj isKindOfClass:NSClassFromString(@"__NSCFDictionary")]){
            type = @"NSDictionary";
        }else
        {
            
            type = @"NSString";
            
        }
        
        NSString *str;
        if ([type isEqualToString:@"NSString"]) {
            str = [NSString stringWithFormat:@"@property (nonatomic, copy) %@ *%@;",type,key];
        }else{
            str = [NSString stringWithFormat:@"@property (nonatomic, strong) %@ *%@;",type,key];
        }
        
        [strM appendFormat:@"\n%@\n",str];
    }];
    
    NSLog(@"%@",strM);
}


-(void)encodeWithCoder:(NSCoder *)aCoder
{
    unsigned int   count ;
    
    objc_property_t *propertList = class_copyPropertyList([self class], &count);
    for (int i =0 ; i<count; i++) {
        
        objc_property_t property = propertList[i];
        const char *name         = property_getName(property);
        NSString *propertyName   = [NSString stringWithUTF8String:name];
        NSString *propertyValue =[self valueForKey:propertyName];
        [aCoder encodeObject:propertyValue forKey:propertyName];
    }
    free(propertList);
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    unsigned int count ;
    
    objc_property_t *properList = class_copyPropertyList([self class], &count);
    for (int i =0 ; i<count; i++) {
        
        objc_property_t property = properList[i];
        const char *name = property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:name];
        NSString *propertyValue = [coder decodeObjectForKey:propertyName];
        if (propertyValue == nil) {
            propertyValue = @"";
        }
        [self setValue:propertyValue forKey:propertyName];
    }
    
    free(properList);
    return self;
}




@end
