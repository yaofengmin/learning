//
//  YHBaseTableViewCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YHBaseTableViewCellConfig <NSObject>

@required

-(void)configWithModel:(id)cellModel;

@end

@interface YHBaseTableViewCell : UITableViewCell<YHBaseTableViewCellConfig>

+ (CGFloat)cellHeight;

@end
