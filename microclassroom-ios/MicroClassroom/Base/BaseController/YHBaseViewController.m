//
//  YHBaseViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"
#import <RDVTabBarController.h>
#import "GpsManager.h"

@interface YHBaseViewController ()<UIGestureRecognizerDelegate>

@end

@implementation YHBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.navigationBar.barTintColor = kNavigationBarColor;
    self.yh_navigationItem.titleColor = kNavigationBarTintColor;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self configNaviBar];
    
}


- (void)configNaviBar{
    
    if ((self.isRootVC || !self.yh_navigationBar.superview) && self.yh_navigationBar) {
        [self.view addSubview:self.yh_navigationBar];
        [self.yh_navigationBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.top.equalTo(@0);
            make.height.equalTo(KTopHeight);
        }];
    }
}

#pragma mark - Private Helper
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.isRootVC) {
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
        
    }else
    {
        [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
        
    }
}



-(void)popView:(NSString*)msg
{
    if(msg==nil || msg.length==0)
        return;
    
    UILabel* view=nil;
    UIView* bgView=nil;
    CGSize viewsize;
    
    CGSize max=CGSizeMake(150, 80);
    viewsize = [msg boundingRectWithSize:max options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial-BoldMT" size:15]} context:nil].size;
    
    NSInteger viewW = 80;
    NSInteger viewH = 80;
    viewW=viewW>viewsize.width?viewW:viewsize.width;
    viewH=viewH>viewsize.height?viewH:viewsize.height;
    
    if(viewW>60)//留出左右间隔
        viewW+=30;
    
    bgView = [[UILabel alloc] init];
    [bgView layer].cornerRadius = 3;
    [bgView layer].masksToBounds = YES;
    bgView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    view = [[UILabel alloc] initWithFrame:CGRectMake(15, 0,viewW-30,viewH)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    
    [view setText:msg];
    view.numberOfLines=2;
    view.textColor = [UIColor whiteColor];
    [view setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
    view.textAlignment = NSTextAlignmentCenter;
    
    [bgView addSubview:view];
    [self.view.window addSubview:bgView];
    
    [bgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(0);
        make.size.equalTo(CGSizeMake(viewW, viewH));
    }];
    bgView.alpha=0;
    bgView.transform = CGAffineTransformMakeScale(1.9,1.9);
    //进入动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    bgView.transform = CGAffineTransformMakeScale(1,1);
    bgView.alpha=1;
    [UIView commitAnimations];
    
    //     消失动画
    [self performSelector:@selector(popViewDisAnima:)withObject:bgView afterDelay:1.7];
    
    
}


-(void)popViewDisAnima:(UIView*)view
{
    //消失动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    view.transform = CGAffineTransformMakeScale(0.1,0.1);
    view.alpha=0;
    [UIView commitAnimations];
    [self performSelector:@selector(closeToast:)withObject:view afterDelay:0.4];
}

-(void)closeToast:(UIView*)view;
{
    [view removeFromSuperview];
}



#pragma mark handle error message
- (void)showErrorMessage:(NSString *)message {
    
    [self showErrorMessage:message withDelegate:nil];
}



- (void)showErrorMessage:(NSString *)message withDelegate:(id)delegate{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"抢地主", @"title") message:message delegate:delegate cancelButtonTitle:NSLocalizedString(@"好", @"title") otherButtonTitles:nil];
    [alertView show];
    
    
}



-(void)updateLocation
{
    //保存最最新的经纬度
    [GpsManager getLoctationSuccuss:^(CGFloat lng, CGFloat lat,NSDictionary *addressDic) {
        
        NSString *longitude = [NSString stringWithFormat:@"%.6f",lng];
        NSString *latitude  = [NSString stringWithFormat:@"%.6f",lat];
        KsetUserValueByParaName(longitude , USERLoctionLng);
        KsetUserValueByParaName(latitude ,USERLoctionLat)
        
    } andFaild:^{}];
}

# pragma mark - Rotate
-(BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}


-(void)dealloc
{
    MyLog(@"%@\n😃dealloc😍",NSStringFromClass([self class]));
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (@available(iOS 13.0, *)) {
        return  UIStatusBarStyleDarkContent;
   }
    return UIStatusBarStyleDefault;
}




@end
