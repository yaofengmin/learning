//
//  YHBaseWebViewController.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"

@interface YHBaseWebViewController : YHBaseViewController

@property (nonatomic ,strong) UIWebView *web;
@property (nonatomic ,copy) NSString *urlStr;
@property (nonatomic ,copy) NSDictionary *shareDic;

@property (nonatomic,assign) BOOL isSign;

-(instancetype)initWithUrlStr:(NSString *)urlStr andNavTitle:(NSString *)navTitle;

@end
