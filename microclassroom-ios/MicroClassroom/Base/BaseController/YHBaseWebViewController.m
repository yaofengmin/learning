//
//  YHBaseWebViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseWebViewController.h"

#import "UIWebView+DKProgress.h"
#import "DKProgressLayer.h"

@interface YHBaseWebViewController ()<UIWebViewDelegate>
@property (nonatomic ,copy) NSString *NavTitle;
@end

@implementation YHBaseWebViewController

-(instancetype)initWithUrlStr:(NSString *)urlStr andNavTitle:(NSString *)navTitle

{
    if (self= [super init]) {
        _urlStr = urlStr;
        _NavTitle = navTitle;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.yh_navigationItem.title = _NavTitle;
    
    _web = [[UIWebView alloc]init];
    _web.scrollView.bounces = NO;
    _web.delegate   = self;
    _web.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_web];
    [_web  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    NSString *urlStr = [_urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURLRequest *requset = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [_web loadRequest:requset];
    self.web.dk_progressLayer = [[DKProgressLayer alloc] initWithFrame:CGRectMake(0, KTopHeight-3, KScreenWidth, 3)];
    self.web.dk_progressLayer.progressColor = MainBtnColor;
    self.web.dk_progressLayer.progressStyle = DKProgressStyle_Noraml;
    [self.view.layer addSublayer:_web.dk_progressLayer];
}


- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr;
    NSURLRequest *requset = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlStr]];
    [_web loadRequest:requset];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    
}
#pragma mark --- 返回上一级
-(void)backBtnAction{
    if ([self.web canGoBack]) {
        [self.web goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *htmlTitle = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    if (htmlTitle.length != 0) {
        self.yh_navigationItem.title = htmlTitle;
    }else{
        self.yh_navigationItem.title = _NavTitle;
    }
    if (self.isSign) {//如果是签到x详情需要刷新的话
        if (![UserInfoModel getInfoModel].isSign) {//没有签到,点击进入web自动签到,然后刷新签到数据即可,签到过就不用处理了
            KPostNotifiCation(@"signWebRefresh", nil);
        }
    }
    //    [_viewHud hide:YES];
    //    [self.web stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none'"];
    //    [self.web stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none'"];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (@available(iOS 13.0, *)) {
        return  UIStatusBarStyleDarkContent;
   }
    return UIStatusBarStyleDefault;
}


@end
