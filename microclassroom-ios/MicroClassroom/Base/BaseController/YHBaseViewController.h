//
//  YHBaseViewController.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "UIViewController+NavigationConfig.h"


#define SHOWHUD [MBProgressHUD showHUDAddedTo:self.view animated:YES]
#define HIDENHUD [MBProgressHUD hideHUDForView:self.view animated:YES]
@interface YHBaseViewController : UIViewController

/**
 *  设置导航左边的按钮。（实际上可以设置控制器的 viewController.sc_navigationItem.leftBarButtonItem 属性）
 */
@property (nonatomic, strong) YHBarButtonItem *leftBarButtonItem;
/**
 *  设置导航右边的按钮。（实际上可以设置控制器的 viewController.sc_navigationItem.rightBarButtonItem 属性）
 */
@property (nonatomic, strong) YHBarButtonItem *rightBarButtonItem;
/**
 *  用法(block传值)
 *	self.rightBarButtonItem = [[YHBarButtonItem alloc] initWithTitle:@"发布" style:YHBarButtonItemStylePlain handler:^(id sender) {
 *		[self doSomeThing];//
 *	}];
 *	[self.rightBarButtonItem.button setTitleColor:WColorMain forState:UIControlStateNormal];
 *	self.sc_navigationItem.rightBarButtonItem = self.rightBarButtonItem;
 *
 *	也可以直接
 *	self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc] initWithTitle:@"发布" style:YHBarButtonItemStylePlain handler:^(id sender) {
 *		[self doSomeThing];//
 *	}];
 */

/**
 *  控制器的中心文字（实际上可以设置控制器的 viewController.yh_navigationItem.title 属性）
 *   一般直接设置 yh_navigationItem.title 即可
 */


- (void)popView:(NSString*)msg;

-(void)updateLocation;

@end
