//
//  YHNavigationItem.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHNavigationItem.h"
#import "YHBarButtonItem.h"
#import "UIViewController+NavigationConfig.h"

static NSInteger const WRightPedding = -8;
@interface YHNavigationItem ()

@property (nonatomic, strong, readwrite) UILabel *titleLabel;
@property (nonatomic, assign) UIViewController *yh_viewController;

@end

@implementation YHNavigationItem


-(void)setFont:(UIFont *)font
{
    _font = font;
    
    if (!font) {
        _titleLabel.font = Font(15);
    }else
    {
        _titleLabel.font = _font;
    }
    
}

- (void)setTitle:(NSString *)title {
    
    _title = title;
    
    if (!title) {
        _titleLabel.text = @"";
        return;
    }
    
    if ([title isEqualToString:_titleLabel.text]) {
        return;
    }
    
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel sizeToFit];
        [_titleLabel setFont:[UIFont boldSystemFontOfSize:KNavigationBarTitleFont]];
        [_titleLabel setTextColor:kNavigationBarTintColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        [_yh_viewController.yh_navigationBar addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_lessThanOrEqualTo(KScreenWidth - 100);
            make.centerX.equalTo(_yh_viewController.yh_navigationBar.mas_centerX);
            make.centerY.equalTo(_yh_viewController.yh_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
        }];
        _titleView = _titleLabel;
    }
    
    _titleLabel.text = title;
}

- (void)setLeftBarButtonItem:(YHBarButtonItem *)leftBarButtonItem {
    if (_yh_viewController) {
        [_leftBarButtonItem.customView removeFromSuperview];
        [_yh_viewController.yh_navigationBar addSubview:leftBarButtonItem.customView];
        [leftBarButtonItem.customView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.centerY.equalTo(_yh_viewController.yh_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
        }];
    }
    
    _leftBarButtonItem = leftBarButtonItem;
}

- (void)setRightBarButtonItem:(YHBarButtonItem *)rightBarButtonItem {
    
    if (_yh_viewController) {
        [_rightBarButtonItem.customView removeFromSuperview];
        [_yh_viewController.yh_navigationBar addSubview:rightBarButtonItem.customView];
        [rightBarButtonItem.customView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(WRightPedding);
            make.centerY.equalTo(_yh_viewController.yh_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
        }];
    }
    
    _rightBarButtonItem = rightBarButtonItem;
}

- (void)setTitleView:(UIView *)titleView {
    [_titleLabel removeFromSuperview];
    _titleLabel = nil;
    _title = nil;
    CGSize size = titleView.frame.size;
    if (_yh_viewController) {
        [_yh_viewController.yh_navigationBar addSubview:titleView];
        [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_yh_viewController.yh_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
            make.centerX.equalTo(0);
            make.size.mas_equalTo(size);
        }];
    }
    _titleView = titleView;
}


-(void)setTitleColor:(UIColor *)titleColor
{
    if (_titleLabel) {
        _titleLabel.textColor = titleColor;
    }
}



@end
