//
//  YHNavigationBar.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHNavigationBar.h"

@implementation YHNavigationBar

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        self.dynamic = NO;
        self.blurEnabled = NO;
        self.blurRadius  =25;
        self.tintColor   = BColor;
        self.backgroundColor   = BColor;
        self.updateInterval = 1.0 / 10 ;
        self.layer.borderWidth = 1.0;
        self.layer.borderColor = NDColor.CGColor;
    }
    
    
    return self;
}



- (instancetype)init {
    
    return [self initWithFrame:CGRectZero];
}




@end
