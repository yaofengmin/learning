//
//  YHBarButtonItem.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBarButtonItem.h"


@interface YHBarButtonItem()

@property(nonatomic, strong) UIImage *buttonImage;
@property(nonatomic, strong) UILabel *badgeLabel;
@property (nonatomic, copy) void (^actionBlock)(id);

@end

@implementation YHBarButtonItem


- (instancetype)initWithCustomView:(UIView *)view
{
    if (self = [super init]) {
       
        view.viewHeight = KNavigationBarHeight;
        view.viewWidth += 30;
        view.viewCenterY = KStatusBarHeight + KNavigationBarHeight / 2.0;
        _button = (UIButton *)view;
        _customView = view;
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.greaterThanOrEqualTo(@32);
            make.width.greaterThanOrEqualTo(@48);
        }];
        
    }
    
    return self;
}


-(instancetype)initWithTitle:(NSString *)title style:(YHBarButtonItemStyle)style handler:(void(^)(id send)) action
{
    if (self = [super init]) {
     
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:title forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:KNavigationBarItemTitleFont]];
        [button setTitleColor:kNavigationBarTintColor forState:UIControlStateNormal];
        [button sizeToFit];
        button.viewCenterY = KStatusBarHeight + KNavigationBarHeight / 2.0;
        button.x = 0;
        _button = button;
        _customView = button;
        if (style == YHBarButtonItemStyleBordered) {
            button.layer.cornerRadius = 4.0f;
            button.layer.borderColor = [UIColor whiteColor].CGColor;
            button.layer.borderWidth = 0.5f;
            button.layer.masksToBounds = YES;
        }else if (style == YHBarButtonItemStylePlain){
            
            
        }else if (style == YHBarButtonItemStyleDone){
            
            
        }else
            
        {
            
            //未来拓展
        }
        
        _actionBlock = action;
        
        [button addTarget:self action:@selector(handleTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(handleTouchDown:) forControlEvents:UIControlEventTouchDown];
        [button addTarget:self action:@selector(handleTouchUp:) forControlEvents:UIControlEventTouchCancel|UIControlEventTouchUpOutside|UIControlEventTouchDragOutside];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@25);
            make.width.greaterThanOrEqualTo(@60);
        }];
        
    }
    
    return self;
}


-(instancetype)initWithImage:(UIImage *)image  style:(YHBarButtonItemStyle)style handler:(void(^)(id send)) action
{
    if (self = [super init]) {
        
        _buttonImage = image ;
        
        UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        [button setImage:image forState:UIControlStateHighlighted];
        [button sizeToFit];
        button.viewCenterY = KStatusBarHeight + KNavigationBarHeight / 2.0;
        _button = button;
        _customView = button;
        _actionBlock = action;
        
        
        [button addTarget:self action:@selector(handleTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(handleTouchDown:) forControlEvents:UIControlEventTouchDown];
        [button addTarget:self action:@selector(handleTouchUp:) forControlEvents:UIControlEventTouchCancel|UIControlEventTouchUpOutside|UIControlEventTouchDragOutside];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.greaterThanOrEqualTo(@32);
            make.width.greaterThanOrEqualTo(@48);
        }];
        
    }
    
    return self;
}








#pragma mark - Private Methods

- (void)handleTouchUpInside:(UIButton *)button {
    if (_actionBlock) {
        _actionBlock(button);
    }
    [UIView animateWithDuration:0.2 animations:^{
        button.alpha = 1.0;
    }];
    
}

- (void)handleTouchDown:(UIButton *)button {
    
    button.alpha = 0.3;
    
}

- (void)handleTouchUp:(UIButton *)button {
    
    [UIView animateWithDuration:0.3 animations:^{
        button.alpha = 1.0;
    }];
    
}


- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    
    if (enabled) {
        _customView.userInteractionEnabled = YES;
        _customView.alpha = 1.0;
    } else {
        _customView.userInteractionEnabled = NO;
        _customView.alpha = 0.3;
    }
    
}




@end
