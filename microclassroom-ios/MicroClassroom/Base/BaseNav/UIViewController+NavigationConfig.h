//
//  UIViewController+NavigationConfig.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHNavigationItem.h"
#import "YHBarButtonItem.h"
#import "YHNavigationBar.h"

@interface UIViewController (NavigationConfig)

/**
 *  导航栏上面的工具
 */
@property (nonatomic, strong) YHNavigationItem *yh_navigationItem;
/**
 *  导航条
 */
@property (nonatomic, strong) YHNavigationBar *yh_navigationBar;
/**
 *  导航条是否被隐藏
 */
@property(nonatomic, assign, getter = yh_isNavigationBarHidden) BOOL yh_navigationBarHidden;
/**
 *  是否是导航控制器的根控制器。(不用管这个属性)
 */
@property (nonatomic, assign) BOOL isRootVC;


/**
 *  设置导航条隐藏与否
 *
 *  @param hidden   是否隐藏
 *  @param animated 是否动画
 */
- (void)sc_setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated;
/**
 *  在导航条上面加上加载数据的动画
 */
- (void)naviBeginRefreshing;
- (void)naviEndRefreshing;

- (YHBarButtonItem *)createBackItem;

/**
 *  返回触发的方法
 */
-(void)backBtnAction;


@end
