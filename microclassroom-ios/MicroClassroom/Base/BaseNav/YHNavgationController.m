//
//  YHNavgationController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHNavgationController.h"
#import "UIViewController+NavigationConfig.h"
#import "YHNavigationBar.h"
#import "YHNavigationItem.h"



@interface YHNavgationController ()<UINavigationControllerDelegate>

@end



@implementation YHNavgationController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationBarHidden = YES;
    super.delegate = self;
    
}



#pragma mark - Push & Pop

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [self configureNavigationBarForViewController:viewController];
    [super pushViewController:viewController animated:animated];
}


-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [viewController.view bringSubviewToFront:viewController.yh_navigationBar];
}


- (void)configureNavigationBarForViewController:(UIViewController *)viewController {
    
    if (!viewController.yh_navigationItem) {
        
        YHNavigationItem *navigationItem = [[YHNavigationItem alloc] init];
        [navigationItem setValue:viewController forKey:@"_yh_viewController"];
        viewController.yh_navigationItem = navigationItem;
    }
    
    if (!viewController.yh_navigationBar) {
        
        viewController.yh_navigationBar = [[YHNavigationBar alloc] init];
    }
    
    if (!viewController.yh_navigationItem.leftBarButtonItem && !viewController.isRootVC) {
        viewController.yh_navigationItem.leftBarButtonItem = [viewController createBackItem];
    }
}

-(BOOL)shouldAutorotate
{
    return [[self.viewControllers lastObject] shouldAutorotate];
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}


- (UIViewController *)childViewControllerForStatusBarHidden {
    return  self.topViewController;
}

- (UIViewController *)childViewControllerForStatusBarStyle {
    return self.topViewController;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    UIViewController *vc = self.topViewController;
    return [vc preferredStatusBarStyle];
}
@end
