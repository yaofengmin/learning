//
//  YHNavigationItem.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YHBarButtonItem;
@interface YHNavigationItem : NSObject

@property (nonatomic, strong  ) YHBarButtonItem *leftBarButtonItem;
@property (nonatomic, strong  ) YHBarButtonItem *rightBarButtonItem;
@property (nonatomic, copy    ) NSString        *title;
@property (nonatomic, strong    ) UIFont        *font;
@property (nonatomic, strong  ) UIView          *titleView;
@property (nonatomic ,strong)  UIColor          *titleColor;


@end
