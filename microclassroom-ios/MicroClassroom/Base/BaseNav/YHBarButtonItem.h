//
//  YHBarButtonItem.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, YHBarButtonItemStyle){
    
    YHBarButtonItemStyleDone,
    YHBarButtonItemStylePlain,
    YHBarButtonItemStyleBordered,
    
};

@interface YHBarButtonItem : NSObject


@property(nonatomic, strong) UIView   *customView;
@property(nonatomic, strong) UIButton *button;
@property(nonatomic, assign, getter = isEnabled) BOOL enabled;


- (instancetype)initWithCustomView:(UIView *)view;

-(instancetype)initWithTitle:(NSString *)title style:(YHBarButtonItemStyle)style handler:(void(^)(id send)) action;


-(instancetype)initWithImage:(UIImage *)image  style:(YHBarButtonItemStyle)style handler:(void(^)(id send)) action;

@end
