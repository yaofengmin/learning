//
//  UIViewController+NavigationConfig.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "UIViewController+NavigationConfig.h"
#import <objc/runtime.h>


static char const * const kNaviHidden  = "kSPNaviHidden";
static char const * const kNaviBarItem = "kSPNaviBar";
static char const * const kNaviBarView = "kNaviBarView";
static char const * const kRootVC      = "isRootVC";
static NSInteger const WPeddingWidth   = 16;

@implementation UIViewController (NavigationConfig)


-(void)setYh_navigationBarHidden:(BOOL)yh_navigationBarHidden
{
    objc_setAssociatedObject(self, kNaviHidden, @(yh_navigationBarHidden), OBJC_ASSOCIATION_ASSIGN);
}


-(BOOL)yh_isNavigationBarHidden
{
    return [objc_getAssociatedObject(self, kNaviHidden) boolValue];
}



-(void)setIsRootVC:(BOOL)isRootVC
{
    objc_setAssociatedObject(self, kRootVC, @(isRootVC), OBJC_ASSOCIATION_ASSIGN);
}

-(BOOL)isRootVC
{
    return  [objc_getAssociatedObject(self, kRootVC) boolValue];
}



-(void)setYh_navigationItem:(YHNavigationItem *)yh_navigationItem
{
    objc_setAssociatedObject(self, kNaviBarItem, yh_navigationItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (YHNavigationItem *)yh_navigationItem {
    return objc_getAssociatedObject(self, kNaviBarItem);
}




-(void)setYh_navigationBar:(YHNavigationBar *)yh_navigationBar
{
    objc_setAssociatedObject(self, kNaviBarView, yh_navigationBar, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (YHNavigationBar *)yh_navigationBar {
    return objc_getAssociatedObject(self, kNaviBarView);
}



- (void)sc_setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated {
    if (hidden) {
        self.yh_navigationBar.blurEnabled = NO;
        self.yh_navigationBar.dynamic = NO;
        [self.yh_navigationBar mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(-KTopHeight);
        }];
        if (animated) {
            [UIView animateWithDuration:0.25 * 2 animations:^{
                [self.yh_navigationBar layoutIfNeeded];
                for (UIView *view in self.yh_navigationBar.subviews) {
                    view.alpha = 0.0;
                }
            } completion:^(BOOL finished) {
                self.yh_navigationBarHidden = YES;
            }];
        }else {
            [self.yh_navigationBar layoutIfNeeded];
            self.yh_navigationBarHidden = YES;
        }
    } else {
        [self.yh_navigationBar mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@0);
        }];
        if (animated) {
            
            [UIView animateWithDuration:0.25 * 2 animations:^{
                [self.yh_navigationBar layoutIfNeeded];
                for (UIView *view in self.yh_navigationBar.subviews) {
                    view.alpha = 1.0;
                }
            } completion:^(BOOL finished) {
//                self.yh_navigationBarHidden = NO;
//                self.yh_navigationBar.blurEnabled = YES;
//                self.yh_navigationBar.dynamic = YES;
            }];
        }else {
            [self.yh_navigationBar layoutIfNeeded];
            self.yh_navigationBarHidden = NO;
            self.yh_navigationBar.blurEnabled = YES;
            self.yh_navigationBar.dynamic = YES;
        }
    }
}



- (void)naviBeginRefreshing {
    
    NSInteger WActivityViewWidth = 35;
    
    UIActivityIndicatorView *activityView;
    for (UIView *view in self.yh_navigationBar.subviews) {
        if ([view isKindOfClass:[UIActivityIndicatorView class]]) {
            activityView = (UIActivityIndicatorView *)view;
        }
        if ([view isEqual:self.yh_navigationItem.rightBarButtonItem.customView]) {
            [view removeFromSuperview];
        }
    }
    
    if (!activityView) {
        activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [activityView setColor:[UIColor blackColor]];
        [self.yh_navigationBar addSubview:activityView];
        [activityView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(WActivityViewWidth));
            make.height.equalTo(@(WActivityViewWidth));
            make.top.equalTo(@(KStatusBarHeight + (KNavigationBarHeight - WActivityViewWidth) / 2));
            make.right.equalTo(@(-WPeddingWidth));
        }];
    }
    
    [activityView startAnimating];
    
}



- (void)naviEndRefreshing {
    
    UIActivityIndicatorView *activityView;
    for (UIView *view in self.yh_navigationBar.subviews) {
        if ([view isKindOfClass:[UIActivityIndicatorView class]]) {
            activityView = (UIActivityIndicatorView *)view;
        }
    }
    if (self.yh_navigationItem.rightBarButtonItem) {
        [self.yh_navigationBar addSubview:self.yh_navigationItem.rightBarButtonItem.customView];
    }
    [activityView stopAnimating];
}



- (YHBarButtonItem *)createBackItem {
    
    @weakify(self);
    
  
    return [[YHBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"publish_back"] style:YHBarButtonItemStyleDone handler:^(id sender) {
        @strongify(self);
        
        [self backBtnAction];
    }];
}



-(void)backBtnAction
{
    if (![self.navigationController popViewControllerAnimated:YES]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


@end
