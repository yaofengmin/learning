//
//  FriendModel.h
//  CarService
//
//  Created by yunbo on 15/7/15.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "YHBaseModel.h"
@interface FriendModel : YHBaseModel

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *noteName;
@property (nonatomic, copy) NSString *devId;
@property (nonatomic, copy) NSString *userName;
/**单位职务*/
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *personalSign;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *hxAccount;
@property (nonatomic, copy) NSString *pinyin;
@property (nonatomic, copy) NSString *pinyinHeardZM;
@property (nonatomic, copy) NSString *pinying_head;
@property (nonatomic, copy) NSArray *label_have;
@property (nonatomic, strong)NSArray *label_need;
@property (nonatomic ,copy) NSString *nickname;
@property (nonatomic, assign) NSInteger isCertified;
@property (nonatomic, copy) NSString *pic;


- (NSURL*)photoImageForWebURL;

- (UIImage*)photoPlaceholderImage;
@end
