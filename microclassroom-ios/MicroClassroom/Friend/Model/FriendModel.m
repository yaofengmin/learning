//
//  FriendModel.m
//  CarService
//
//  Created by yunbo on 15/7/15.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "FriendModel.h"
#import "PinYinForObjc.h"
#import "MJExtension.h"


#define PinyinSeparator @"-"
@implementation FriendModel
//+(NSDictionary*) replacedKeyFromPropertyName{
//    return @{@"userName":@"nickname"};
//}

/**
 *  生成拼音
 */
-(NSString *)pinyin:(NSString *)text
{
    // 拼音格式
    HanyuPinyinOutputFormat *pY = [[HanyuPinyinOutputFormat alloc]init];
    //   小写
    pY.caseType = CaseTypeLowercase;
    // 音调
    pY.toneType = ToneTypeWithoutTone;
    // 类似lv
    pY.vCharType =VCharTypeWithV;
    NSString *pinyin = [PinyinHelper toHanyuPinyinStringWithNSString:text withHanyuPinyinOutputFormat:pY withNSString:PinyinSeparator];
    return pinyin;
}


-(void)setUserName:(NSString *)userName{
    _userName = userName;
    if (IsEmptyStr(self.nickname)) {
        _nickname = userName;
    }
    NSString *pinyin = [self pinyin:userName];
    _pinyin = [pinyin stringByReplacingOccurrencesOfString:PinyinSeparator  withString:@""];
    // 替换多音字
    if(userName.length>1){
        if ([[userName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"长"]) {
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"C"];
        }else if ([[userName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"曾"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1)withString:@"Z"];
        }else if ([[userName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"解"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"X"];
        }else if ([[userName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"仇"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"Q"];
        }else if ([[userName substringWithRange:NSMakeRange(0, 1)]    isEqualToString:@"朴"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"P"];
        }else if ([[userName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"查"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"Z"];
        }else if ([[userName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"单"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"S"];
        }
      
    }
    
    NSArray *pinyinArr = [pinyin componentsSeparatedByString:PinyinSeparator];
    NSMutableString *must = [NSMutableString string];
    for (NSString *str in pinyinArr) {
        if (NOEmptyStr(str)) {
            NSString *suStr = [str substringToIndex:1];
            [must appendString:suStr];
        }
        _pinyinHeardZM = must;
        
        NSString *regex = @"[A-Za-z]+";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        NSString *initialStr = [_pinyin length]?[[_pinyin substringToIndex:1] uppercaseString]:@"";
        if ([predicate evaluateWithObject:initialStr])
        {
            _pinying_head= initialStr;
        }else{
            _pinying_head =@"#";
        }
        
    }
 
}
-(NSString*)RemoveSpecialCharacter: (NSString *)str {
    NSRange urgentRange = [str rangeOfCharacterFromSet: [NSCharacterSet characterSetWithCharactersInString: @",.？、 ~￥#&<>《》()[]{}【】^@/￡¤|§¨「」『』￠￢￣~@#&*（）——+|《》$_€-"]];
    if (urgentRange.location != NSNotFound)
    {
        return [self RemoveSpecialCharacter:[str stringByReplacingCharactersInRange:urgentRange withString:@""]];
        
    }
    return str;
}
-(NSURL*)photoImageForWebURL{
    NSString* path =[NSString stringWithFormat: @"%s",[self.photo UTF8String]];
    NSURL* url = [NSURL URLWithString:[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];//网络图片url
    return url;
}
-(UIImage *)photoPlaceholderImage{
    UIImage *aimage =[UIImage imageNamed:@"parkFriend_headImage"];
    return aimage;
}


@end
