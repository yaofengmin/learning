#ifndef UrlHead_h
#define UrlHead_h
#import "InitConfigModel.h"

#define HTTPKEY  @"go2tostudy87d3c6e2c54bbnhe9693282c75e2ef795d3"


#ifdef DEBUG

//#define MainAPI  @"http://go2study8api.pinnc.com/v1_1/" // 测试
#define MainAPI  @"http://xingyeapi.go2study8.com/v1_1/" // 正式
#define ApnsCertName @"study_dev"
#else

#define MainAPI  @"http://xingyeapi.go2study8.com/v1_1/"
#define ApnsCertName @"study_dis"
#endif


/**app分享连接*/
#define APPSHARELINK  [InitConfigModel shareInstance].down_ios_url

//课间和江湖详细的分享地址
#define JHKJSHARELINK  [InitConfigModel shareInstance].jianghukejian_ios_url

#endif /* UrlHead_h */
