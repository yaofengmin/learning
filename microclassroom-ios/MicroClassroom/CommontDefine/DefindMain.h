//
//  DefindMain.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "EXTScope.h"
#import "UserDefautHead.h"
#import "NotifcationHead.h"
#import "UrlHead.h"



#ifndef DefindMain_h
#define DefindMain_h

//----------------获得版本号--------------------
#define APPversion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]



// ------------------- 自定义LOG---------


#ifdef DEBUG
#define MyLog(format, ...)                     do {                                                                          \
fprintf(stderr, "<%s : %d> %s\n",                                           \
[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String],  \
__LINE__, __func__);                                                        \
(NSLog)((format), ##__VA_ARGS__);                                           \
fprintf(stderr, "-------\n") ;                                          \
} while (0)
#else

#define MyLog(format, ...)

#endif



// -----------获取系统信息----------------------

#define IOS10            ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 10.0)
#define IOS9            ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 9.0)
#define IOS8            ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 8.0)
#define IOS7            ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0)

//-----------获取设备类型-----------------------
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_MAX_LENGTH (MAX(KScreenWidth, KScreenHeight))
#define SCREEN_MIN_LENGTH (MIN(KScreenWidth, KScreenHeight))
#define IPHONE3_5INCH   ([[UIScreen mainScreen] bounds].size.height == 480)
#define IPHONE_5_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH <= 568.0)
#define IPHONE_6_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH <= 667.0)
#define IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

//判断iPad
#define isPad ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//判断iPHoneX
#define iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
//判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)



//-------------------获取设备大小-------------------------
// NavBar高度
#define KNavigationBarHeight (44.0)
// 状态栏高度
#define KStatusBarHeight ((iPhoneX == YES || IS_IPHONE_Xr == YES || IS_IPHONE_Xs == YES || IS_IPHONE_Xs_Max == YES) ? 44.0 : 20.0)
// 顶部高度
#define KTopHeight       ((iPhoneX==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs== YES || IS_IPHONE_Xs_Max== YES) ? 88.0 : 64.0)

// 底部 TabBar 高度
#define KTabBarHeight    ((iPhoneX == YES || IS_IPHONE_Xr == YES || IS_IPHONE_Xs == YES || IS_IPHONE_Xs_Max == YES) ? (49.f+34.f) : 49.f)
//刘海高度
#define kBottomOffSet ((iPhoneX == YES || IS_IPHONE_Xr == YES || IS_IPHONE_Xs == YES || IS_IPHONE_Xs_Max == YES) ? 34.f : 0.f)

// 动态获取屏幕宽高
#define KScreenHeight ([UIScreen mainScreen].bounds.size.height)
#define KScreenWidth  ([UIScreen mainScreen].bounds.size.width)



// ---------------- 随机数 ------------
#define KArcNum(x) arc4random_uniform(x)


//--------------- 颜色设置----------------

#define KColorRGBA(r, g, b, a) [UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:(a)]
#define KColorRGB(r, g, b) KColorRGBA(r, g, b, 1.f)

#define KColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]






//--------- 判断是否是空字符串 非空字符串 ＝ yes ------


#define  NOEmptyStr(string)  string == nil ||[string isEqualToString: @""] || string == NULL ||[string isKindOfClass:[NSNull class]]||[[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0 ? NO : YES


#define  IsEmptyStr(string) string == nil || string == NULL || [string isEqualToString:@""] ||[string isKindOfClass:[NSNull class]]||[[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0 ? YES : NO


//------------添加/移除通知 ----------

#define KPostNotifiCation(ActionName,infoDic) [[NSNotificationCenter defaultCenter] postNotificationName:ActionName object:infoDic]

#define KAddobserverNotifiCation(ActionName,postName) [[NSNotificationCenter defaultCenter] addObserver:self selector:ActionName name:postName object:nil]

#define KRemoverNotifi(ActionName)    [[NSNotificationCenter defaultCenter] removeObserver:self name:ActionName object:nil]



//----------本地偏好设置 -----------


#define KsetUserValueByParaName(Object,Key) \
[[NSUserDefaults standardUserDefaults] setObject:Object forKey:Key];\
[[NSUserDefaults standardUserDefaults] synchronize];\


#define KgetUserValueByParaName(Key)  [[NSUserDefaults standardUserDefaults] objectForKey:Key]



//---------沙盒路径-------------

#define DOCUMENT_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]





//------------------其他相关配置------------------

#define Font(FONT)  [UIFont systemFontOfSize:FONT]
#define KNavigationBarTitleFont 18
#define KNavigationBarItemTitleFont 14
//#define kNavigationBarColor     KColorFromRGB(0xFF3535)
#define kNavigationBarColor     KColorFromRGB(0xff3347)
#define kNavigationBarLineColor [UIColor colorWithWhite:0.869 alpha:1]
//#define kNavigationBarTintColor [UIColor whiteColor]
#define kNavigationBarTintColor KColorFromRGB(0x040340)




//----------通用按钮相关配置-----------
#define MainBtnUColor   KColorFromRGB(0x4F93FC)
#define MainBtnColor    KColorFromRGB(0xFF3535)
#define MainBackColor   KColorFromRGB(0xefeef3)// 主背景色


//---------------通用文字颜色配置----------

#define MainTextGrayColor        KColorFromRGB(0x4F93FC)
#define BColor                   KColorFromRGB(0xffffff)
#define CColor                   KColorFromRGB(0x585858)
#define DColor                   KColorFromRGB(0x8d8d8d)
#define EColor                   KColorFromRGB(0xb8b8b8)
#define FColor                   KColorFromRGB(0xbdbdbd)
#define LColor                   KColorFromRGB(0xe0e0e0)
#define ImageBackColor           KColorFromRGB(0xeeeeee)
#define GColor                   KColorFromRGB(0x00bd81)

#define NBColor                  KColorFromRGB(0xf7f7fa)
#define NCColor                  KColorFromRGB(0xf0f0f5)
#define NDColor                  KColorFromRGB(0xe6e6ed)
#define NEColor                  KColorFromRGB(0xff3347)
#define NFColor                  KColorFromRGB(0xe62e40)
#define NGColor                  KColorFromRGB(0xffe957)
#define HColor                   KColorFromRGB(0x040340)
#define IColor                   KColorFromRGB(0x969699)
#define JColor                   KColorFromRGB(0x646466)
#define KColor                   KColorFromRGB(0X4B49C3)


//*******朋友圈相关配置**********//


#define limitline 6
#define kDistance 17 //说说和图片的间隔
#define kOpDistance 12 //说说和投票选项的间隔
#define kOpToBottom 18 //投票选项和底部的间隔

#define kCommentH  44 + 10  //底部评论显示的高度
#define kLocationToBottom 24 //内容距离底部的高度
#define kTopViewHeight 72 //

#define kVoteBottom 20 + 35 // 投票距离底部的距离 + 底部投票时间的展示高度


static  const CGFloat ShowImage_H        = 70 ;//图片高度
static  const CGFloat ShowOption_H        = 40 ;//投票选项高度
static  const CGFloat offSet_X           = 64.0;//文字左边起始距离
static  const CGFloat  CircleBottomViewH = 10.0;//圈底部高度
static const  CGFloat SEGMENTHEIGHT      = 44.0f;//segment高度


#define kSelf_SelectedColor [UIColor colorWithWhite:0 alpha:0.4] //点击背景  颜色
#define kUserName_SelectedColor [UIColor colorWithWhite:0 alpha:0.25]//点击姓名颜色

#define DELAYEXECUTE(delayTime,func) (dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{func;}))

//----------------提示弹窗的初始化--------------
#define alertContent(content) \
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" \
message:content \
delegate:nil   \
cancelButtonTitle:@"确定" \
otherButtonTitles:nil];  \
[alert show];

//-------首页宏-----
#define homeListpadding (IPHONE_5?10:(IPHONE_6?15:20))
#define itemPerLine 4
#define kItemW (KScreenWidth-homeListpadding*(itemPerLine+1))/itemPerLine
#define kItemH 30
#define kListBarH 44
#define kItemFont (IPHONE_5?10:(IPHONE_6?11:12))

//-----直播UI
#define kViewFramePath  @"frame"
#define DEMO_AccessToken    @"" //直播Token 详见：▪ 自助式网络直播API -> 观众管理 ->verify/access-token
#define DEMO_account   KgetUserValueByParaName(USERID) //账号 详见：▪ 自助式网络直播API -> 活动管理 ->user/register 创建用户
#define DEMO_AppKey         @"1ed0a1b9edaa823408dd9a94a42d5e66"        //AppKey   详见： ▪ API&SDK权限申请
#define DEMO_AppSecretKey   @"1aaab7a24da4a7c4527445f2ab98515a"  //AppSecretKey
#define DEMO_password       @"20161001" //密码 详见：▪ 自助式网络直播API -> 活动管理 ->user/register 创建用户
#define DEMO_ActivityId     @""

//---- 分享相关配置
#define UMSHAREKEY  @"5d8ad572570df310a90001fb"  //友盟key

/**
 *  微信分享
 */
#define WeChatShareAppKey      @"wx939112df0f2f8f41"  // 微信
#define WeChatShareAppSecret   @"407c964e5eb535af7fa016a39722b52e"
#define WeChatUniversalLinks    @"https://www.go2study8.com/"

// itunes
#define Micro_ITUNES @"https://itunes.apple.com/cn/app/%E5%AD%A6%E4%B9%A0%E5%BE%AE%E8%AF%BE%E5%A0%82/id1480193851?mt=8"


//游客账号密码
#define kTouristsMoblie @"17630150513"
#define kTouristsPsw    @"111111"

//所属平台id
//特殊学员(brand_id>0)个人信息中的资源 需求 来往城市 行业 不显示；链城小站不显示
#define KBrandInfoId     @"brandId"


//找投资
#define kReimbursement  @"86"
//觅人才
#define kCeremonial     @"87"
//寻合作
#define kCompounding    @"88"
//卖产品
#define kPersonal       @"89"
//其他
#define kOtherKind      @"90"

//有资金
#define kFunding       @"81"
//有渠道
#define kDitching      @"82"
//有人脉
#define kManned        @"83"
//秀能力
#define kExcellent     @"84"
//其他
#define kLOtherKind    @"85"

#endif /* DefindMain_h */
