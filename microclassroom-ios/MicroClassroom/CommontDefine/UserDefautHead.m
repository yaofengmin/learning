//
//  UserDefautHead.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//


#import "UserDefautHead.h"

const NSString * const REQUEST_CODE     = @"code";
const NSString * const REQUEST_MESSAGE  = @"message";
const NSString * const REQUEST_INFO     = @"info";
const NSString * const REQUEST_LIST     = @"list";
const NSString * const ALERTTITLE       = @"";
const NSString * const USERID           = @"userId";
const NSString * const MYINVITECODE     = @"myInviteCode";
const NSString * const USERSecret       = @"secret";
const NSString * const LoginSecret      = @"loginSecret";
const NSString * const NCUserLastAreaId = @"cityId";
const NSString * const NCUserLastAreaName = @"lastAreadName";
const NSString * const TABLE_FRIENDS    = @"FriendList";
const NSString * const TABLE_ASSOCIATOR = @"associator";
const NSString * const TABLE_VIDEOPROGRESS = @"videoProgress";
const NSString * const TABLE_AUDIOPROGRESS = @"audioProgress";
const NSString * const USERNAME         = @"userName";
const NSString * const PHOTOURL         = @"fromPhoto";
const NSString * const GROUPURL         = @"groupUrl";
const NSString * const GROUPNAME        = @"groupName";
const NSString * const USERLoctionLng   = @"longitude";
const NSString * const USERLoctionLat   = @"latitude";
const NSString * const NICKNAME         = @"fromName";
const NSString * const FROMID           = @"fromId";
//-----------服务器方法头----------------
const NSString * const Service                   = @"service";
const NSString * const Register                  = @"User.register";
const NSString * const GetToken                  = @"User.getToken";
const NSString * const Login                     = @"User.login";
const NSString * const Logout                    = @"User.logout";
const NSString * const ResetPwd                  = @"User.resetPwd";
const NSString * const GetBaseInfo               = @"User.getBaseInfo";
const NSString * const SetIosToken               = @"User.setIosToken";
const NSString * const SetPwd                    = @"User.setPwd";
const NSString * const SetNickname               = @"User.setNickname";
const NSString * const SetSex                    = @"User.setSex";
const NSString * const SetAvatar                 = @"User.setAvatar";
const NSString * const SetPersonalSign           = @"User.setPersonalSign";
const NSString * const SetTitle                  = @"User.setTitle";
const NSString * const SetCompany                = @"User.setCompany";
const NSString * const AddFollows                = @"User.addFollows";
const NSString * const DeleteUserFollow          = @"User.deleteUserFollow";
const NSString * const AddCollectoins            = @"User.addCollectoins";
const NSString * const DeleteUserCollection      = @"User.deleteUserCollection";
const NSString * const ClearUserCollection       = @"User.clearUserCollection";
const NSString * const GetUserCollections        = @"User.getUserCollections";
const NSString * const GetUserPiliList           = @"VideoLive.getMyVideoLiveList";
const NSString * const GetInitConfig             = @"App.getInitConfig";
const NSString * const Feedback                  = @"User.feedback";
const NSString * const GetIndexList              = @"Videos.getVideoClass";
const NSString * const GetVideoListByClass       = @"Videos.getVideoListByClass";
const NSString * const GetVideoInfo              = @"Videos.getVideoInfo";
const NSString * const AddComment                = @"App.addComment";
const NSString * const CirclePublishMessage      = @"Circle.publishMessage";
const NSString * const CircleGetMessages         = @"Circle.getMessages";
const NSString * const CircleGetMessageComments  = @"Circle.getMessageComments";
const NSString * const CircleThumbMessage        = @"Circle.thumbMessage";
const NSString * const CircleCancelThumb         = @"Circle.cancelThumb";
const NSString * const CircleCommentMessage      = @"Circle.commentMessage";
const NSString * const CircleDeleteMessage       = @"Circle.deleteMessage";
const NSString * const CircleGetMyCircle         = @"Circle.getMyCircle";
const NSString * const CircleGetHelpListInfo     = @"Circle.getHelpListInfo";
const NSString * const CircleGetUserInfo         = @"Circle.getUserInfo";
const NSString * const CircleGetFriendList       = @"Circle.getFriendList";
const NSString * const CircleAddUserAsFriend     = @"Circle.addUserAsFriend";
const NSString * const CircleConfirmAddAsFriend  = @"Circle.confirmAddAsFriend";
const NSString * const CircleDeleteFriend        = @"Circle.deleteFriend";
const NSString * const CircleSetFriendNoteName   = @"Circle.setFriendNoteName";
const NSString * const CircleSearchUser          = @"Circle.searchUser";
const NSString * const CircleGetGroupUserInfo    = @"Circle.getGroupUserInfo";
const NSString * const CircleAddChatGroup        = @"Circle.addChatGroup";
const NSString * const CircleGetChatGroupsInfo   = @"Circle.getChatGroupsInfo";
const NSString * const CircleModifyChatGroupInfo = @"Circle.modifyChatGroupInfo";
const NSString * const CircleGetUserList         = @"Circle.getUserList";
const NSString * const CircleGetFollowsList      = @"Circle.getFollowsList";
const NSString * const AppCreatePiliStrea        = @"App.createPiliStrea";
const NSString * const AppGetPiliStream          = @"App.getPiliStream";
const NSString * const AppGetCodeList            = @"App.getCodeList";
const NSString * const AppGetPiliList            = @"VideoLive.getVideoLiveList";   //App.getPiliList VideoLive.getVideoLiveList
const NSString * const UserGetUserClasses        = @"User.getUserClasses";
const NSString * const GetSiteIndex              = @"Beseness.getSiteIndex";
const NSString * const GetSiteMessages           = @"Beseness.getSiteMessages";
const NSString * const GetSiteMessageInfo        = @"Beseness.getSiteMessageInfo";
const NSString * const UserTopupByThird          = @"User.topupByThird";
const NSString * const PayForProducts            = @"Pinnc.payForProducts";
const NSString * const GetOrderList              = @"Pinnc.getOrderList";
const NSString * const UserGetBalance            = @"User.getBalance";
const NSString * const UserGetBalanceAndRecords  = @"User.getBalanceAndRecords";
const NSString * const UserApplyWithdraw         = @"User.applyWithdraw";
const NSString * const UserSetMoreInfo           = @"User.setMoreInfo";
const NSString * const CircleAddMessageHelp      = @"Circle.addMessageHelp";
const NSString * const AppGetUserLevelSet        = @"App.getUserLevelSet";
const NSString * const UserGetUsersByClassId     = @"User.getUsersByClassId";
const NSString * const AppGetUserStudy           = @"App.getUserStudy";
const NSString * const CircleAcceptMessageHelp   = @"Circle.acceptMessageHelp";
const NSString * const AppGetUserStudyTime       = @"App.getUserStudyTime";
const NSString * const CircleFinishMessageHelp   = @"Circle.finishMessageHelp";
const NSString * const UserGetDisplayInfo        = @"User.getDisplayInfo";
const NSString * const AppDoReward               = @"App.doReward";
const NSString * const PinncPayForOrder          = @"Pinnc.payForOrder";
const NSString * const AppGetSeacherCodes        = @"App.getSeacherCodes";
const NSString * const AppGetClasses             = @"App.getClasses";
const NSString * const AppJoinClasses            = @"App.joinClasses";

//直播
const NSString * const AppGetPiliInfo            = @"VideoLive.getVideoLiveInfo";// App.getPiliInfo
const NSString * const AppGetMyVideo             = @"VideoLive.getMyVideoLiveList";
const NSString * const AppGetVideoDetail         = @"VideoLive.getVideoLiveInfo";
const NSString * const VideoLiveBeginPush        = @"VideoLive.beginPush";
const NSString * const SetAlwaysRecordByCid      = @"VideoLive.setAlwaysRecordByCid";
const NSString * const VideoLiveKeepPush         = @"VideoLive.keepPush";
const NSString * const VideoLiveEndPush          = @"VideoLive.endPush";
const NSString * const VideoLiveSleepPush        = @"VideoLive.sleepPush";
const NSString * const VideoLiveGetChannerState  = @"VideoLive.getChannelStats";
const NSString * const VideoLiveGetVideoListByCid  = @"VideoLive.getVideoListByCid";
const NSString * const VideoLiveEnrollVideoLives   = @"VideoLive.enrollVideoLives";


const NSString * const CircleGetCommonFriendList = @"Circle.getCommonFriendList";
const NSString * const AppGetPiliState           = @"App.getPiliState";
const NSString * const UserGetDisplayListInfo    = @"User.getDisplayListInfo";
const NSString * const AppGetPiliAccessToken     = @"App.getPiliAccessToken";
const NSString * const CircleGetMessageInfo      = @"Circle.getMessageInfo";
const NSString * const AppAcceptJoinClasses      = @"App.acceptJoinClasses";
const NSString * const PinncCommentOrder         = @"Pinnc.commentOrder";
const NSString * const AppGetTeacherPiliList     = @"App.getTeacherPiliList";
const NSString * const AppGetPiliGenToken        = @"App.getPiliGenToken";
const NSString * const UserSetIndustryId         = @"User.setIndustryId";
const NSString * const CircleDeleteCommentMessage = @"Circle.delCommentMessage";
const NSString * const PinncApplePayForOk        = @"Pinnc.applePayForOk";

//享
const NSString * const GetNewsList               = @"App.getNewsList";
const NSString * const GetNewsCate               = @"App.getNewsCate";
//const NSString * const GetCodeList               = @"App.getCodeList";
const NSString * const GetNewsInfo               = @"App.getNewsInfo";

const NSString * const MyCollections             = @"Collections.get";

const NSString * const CircleAddCollection       = @"Collections.add";
const NSString * const CircleCancelCollection    = @"Collections.delete";


//系统消息
const NSString * const GetNotifiMessageList      = @"App.getMessageList";
const NSString * const VidoeoGetComments         = @"Videos.getComments";

const NSString * const GetUserHonorList          = @"User.getUserHonorList";

//微课删除评论
const NSString * const VideoDeleteCommentMessage = @"App.delComment";

const NSString * const AddIntegrateRecord        = @"Integrate.addIntegrateRecord";

//笔记
const NSString * const VideosAddNotes            = @"Videos.addNotes";
const NSString * const GetGetMyNotes             = @"Videos.getMyNotes";
const NSString * const DelMyNotes                = @"Videos.delNotes";
const NSString * const UpdateNotes               = @"Videos.updateNote";


const NSString * const CircleAddMessagesQusA     = @"Circle.addMessagesQusA";

const NSString * const GetMyReadVideoList        = @"Videos.getMyReadVideoList";


//讲师信息
const NSString * const GetTeachersById           = @"User.getTeachersById";

//分享到班级
const NSString * const CircleCopyNewsToMessage   = @"Circle.copyNewsToMessage";
//积分记录
const NSString * const GetIntegrateRecord        = @"Integrate.getIntegrateRecord";

