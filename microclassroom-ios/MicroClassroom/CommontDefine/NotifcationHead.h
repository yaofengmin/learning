//
//  NotifcationHead.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#ifndef NotifcationHead_h
#define NotifcationHead_h



#define FriendReply                 @"FriendReply"
//#define JiangHuTableRefresh         @"JiangHuTableRefresh" //江湖刷新
#define NewMsgNotification          @"NewNewMsgNotification"
#define DeskmateRefresh             @"DeskmateRefresh" // 同学刷新
#define CircleListReloadOneData     @"CircleListReloadOneData" //课间和班级刷新
#define CircleListReloadAllData     @"CircleListReloadAllData" //课间和班级刷新
#define KLOGINVIEWISSHOWTOURISTS    @"LoginViewIsShowTourists"//配置接口获取完之后确定是否显示游客登录按钮
#define NOTIGICATIONFINDVVIEWRELOAD @"RefreshLoginSuccessWithInfo" //登录成功之后,重新刷新共享界面 我的界面 直播界面

#define NOTIGICATIONFINDFITERBUTTON @"refreshFindFiterButton" //登录成功之后,重新刷新we+筛选按钮  (branId 大于 0 隐藏发现中的筛选按钮)

#endif /* NotifcationHead_h */
