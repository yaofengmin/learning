//
//  ENUMDefine.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#ifndef ENUMDefine_h
#define ENUMDefine_h

/**
 *  性别枚举
 */
typedef NS_ENUM(NSInteger, MCSexType) {
    /**
     *  男
     */
    MCSexTypeMan = 1,
    
    /**
     *  女
     */
    MCSexTypeWoman = 2
};

/**
 *  关系枚举
 */
typedef NS_ENUM(NSInteger, MCRelationType) {
    /**
     *  自己
     */
    MCRelationTypeSelf = -1,
    
    /**
     *  陌生人
     */
    MCRelationTypeStranger = 0,
    
    /**
     *  好友
     */
    MCRelationTypeFriend = 1,
    
    /**
     *  等待我接受
     */
    MCRelationTypeWaitMe = 2,
    
    /**
     *  等待他人接受我
     */
    MCRelationTypeWaitOther = 3
};

/**
 *  关系枚举
 */
typedef NS_ENUM(NSInteger, MCEditAction) {
    
    /**
     *  改昵称
     */
    MCEditActionNickName = 1,
    
    /**
     *  改备注
     */
    MCEditActionRemarkName = 2,
    
    /**
     *  改个性签名
     */
    MCEditActionSign = 3,
    
    /**
     *  改个人介绍
     */
    MCEditActionBrief = 4,
    
    /**
     *  改邮箱
     */
    MCEditActionEmail = 5,
    /**
     *  改单位名称
     */
    MCEditActionCompany = 6,
    /**
     *  改单位职务
     */
    MCEditActionTitle = 7,
    
    /**
     *  改联系方式
     */
    MCEditActionLinkTel = 8,
    /**
     兴趣爱好
     */
    MCEditActionHobby ,
    /**
     领域
     */
    
    MCEditActionKnowledge,
    /**
     来往城市
     */
    MCEditActionRuningCity,
    /**
     所在地区
     */
    MCEditChangeArea,
};


typedef NS_ENUM(NSInteger, GestureType) {
    
    /**
     *  点击
     */
    TapGesType = 1,
    /**
     *  长按
     */
    LongGesType,
    
};


typedef NS_ENUM(NSInteger, GetMessagesType) {
    /**
     *  课间
     */
    GetMessagesTypeDuringClass = 0,
    
    /**
     *  班级动态
     */
    GetMessagesTypeStudyGroup = 1,
    /**
     *  收藏
     */
    GetMessagesTypeCollection = 2 ,

    /**
     *  获取自己发布的
     */
    
    GetMessagesTypeGetMyCircle = 3,
    
//    GetMessagesTypeGetMyJiangHu = 4,
    GetMessagesTypeGetMyStudyGroup = 4,
    
    GetMessagesTypeJiangHu,
};


typedef NS_ENUM(NSInteger, PayType) {
    /**
     *  微信支付
     */
    PayTypeWeiXinPay,
    /**
     * 支付宝支付
     */
    PayTpyeAlipy,
    
};

/**
 *  直播状态
 */
typedef NS_ENUM(NSInteger, MCZhiBoStatus) {
    /**
     *  可直播
     */
    MCZhiBoStatusIsAvaliable = 1,
    /**
     *  热约中
     */
    MCZhiBoStatusBooking = 2,
    
    /**
     *  已结束
     */
    MCZhiBoStatusEnd = 3,
    
    /**
     *  直播中
     */
    MCZhiBoStatusLiving = 4,
 
   
};

/**
 *  我的订单 订单状态
 */
typedef NS_ENUM(NSInteger, MCMyOrderStatus) {
    /**
     *  等待支付
     */
    MCMyOrderStatusWaitPay = -1,
    /**
     *  订单取消
     */
    MCMyOrderStatusCancel = 0,
    /**
     *  支付完成
     */
    MCMyOrderStatusPayed = 1
};

/**
 *  我的订单 订单种类
 */
typedef NS_ENUM(NSInteger, MCMyOrderType) {
    /**
     *  小站
     */
    MCMyOrderTypeSite = 1,
    /**
     *  直播
     */
    MCMyOrderTypeZhiBo = 2,
    /**
     *  会员等级
     */
    MCMyOrderTypeVIP = 3
};

typedef enum{
    topViewClick = 0,
    FromTopToTop = 1,
    FromTopToTopLast = 2,
    FromTopToBottomHead = 3,
    FromBottomToTopLast = 4
} animateType;


typedef NS_ENUM(NSInteger ,YHSearchType) {
    
    YHSearchTypeJiangHu = 1,//江湖
    YHSearchTypeMate = 2,//同窗 ,同学
    YHSearchTypeOnlineLive,//直播
    YHSearchTypeRodio, //视频
//    YHSearchTypeCircle,//共享标签 102
};


typedef NS_ENUM(NSInteger, JiangHuHelpType) {
    HelpDefaultType , //单纯的刷新
    HelpIngType = 1, //求助中
    HelpWorkIng ,  //解决中
    HelpSuccessType, //成功
    HelpFailType, // 失败
};


typedef NS_ENUM(NSInteger,WatchVideoType)
{
    kWatchVideoNone,
    kWatchVideoRTMP,
    kWatchVideoHLS,
    kWatchVideoPlayback
};


typedef NS_ENUM(NSInteger ,MicNotificationType) {
   
   USER_ADD_REQUEST   = 11, 
   USER_ADD_PASSED    ,
   USER_DELETED       ,
   CIRCLE_PUB_AT      = 21,
   CIRCLE_THUMB       ,
   CIRCLE_COMMENT   ,
   CIRCLE_COMMENT_RESPONSE,
   CIRCLE_HELP_ADD   = 31,
   CIRCLE_HELP_ACCEPT ,
   CIRCLE_HELP_ACCEPT_FAILD,
   CIRCLE_HELP_FINISH,
   CIRCLE_HELP_FINISH_FAILD,
   CLASS_JOIN  = 41,
   CLASS_ACCEPTJOIN,
   CLASS_FAILDJOIN,
   APPADMIN         = 51, //系统消息
};


/**
 *  编辑类型枚举
 */
typedef NS_ENUM(NSInteger, MCEditType) {
    
    MCEditTypeIndustry = 8,
    /**
     *  资源
     */
    MCEditTypeHave = 10,
    
    /**
     *  需求
     */
    MCEditTypeNeed = 11,
    
    
};


//分享
typedef NS_ENUM(NSInteger ,YHSearchTopType) {
    
    YHSearchTypeShreDetail = 0,//分享列表 100
    YHSearchTypeAskAndShare = 1,//问&享 101
    YHSearchTypeCircle,//共享标签 102
};

typedef NS_ENUM(NSInteger, GetMessageListType) {
    /**
     *  普通的动态
     */
    GetMessagesListTypeNoraml = 0,
    
    /**
     *  发送投票动态
     */
    GetMessagesVoteType = 1,
    /**
     *  分享到班级的动态
     */
    GetMessageShareType = 2 ,
    
    /**
     *  文件动态
     */
    GetMessageFileType = 3,
    
};


typedef NS_ENUM(NSInteger, MicClassMp3Type) {
    /**
     *  视频
     */
    MicClassMp3TypeVideo = 0,
    
    /**
     *  音频
     */
    MicClassMp3TypeAudio = 1,
    /**
     *  图书
     */
    MicClassMp3TypeBook = 2 ,
    
    /**
     * web
     */
    MicClassMp3TypeWeb = 3,
    
};


typedef NS_ENUM(NSInteger, LabelCodeIdType) {
    /**
     *  有资金
     */
    LabelFundingType = 81,
    
    /**
     *  有渠道
     */
    LabelDitching = 82,
    /**
     *  有人脉
     */
    LabelManned = 83 ,
    
    /**
     *  秀能力
     */
    LabelExcellent = 84,
    
    /**
     *  (资源)其他
     */
    LabelLOtherKind = 85,
    /**
     *  找投资
     */
    LabelReimbursement = 86,
    
    /**
     *  觅人才
     */
    LabelCeremonial = 87,
    /**
     *  寻合作
     */
    LabelCompounding = 88,
    
    /**
     *  卖产品
     */
    LabelPersonal = 89,
    
    /**
     *  (需求)其他
     */
    LabelOtherKind= 90,
    
};
#endif /* ENUMDefine_h */
