//
//  UserDefautHead.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//


#ifndef  LocUserDefautConfig_h
#define  LocUserDefautConfig_h

#define TABLE_ADDRESSBOOKS @"AddressBooks"//通讯录列表
#define TABLE_PUSHS        @"pushs"   //推送信息表

#define HCmyDyItems        @"myDyItems"   // 我的页面


UIKIT_EXTERN NSString * const REQUEST_CODE;
UIKIT_EXTERN NSString * const REQUEST_MESSAGE;
UIKIT_EXTERN NSString * const REQUEST_INFO;
UIKIT_EXTERN NSString * const REQUEST_LIST;
UIKIT_EXTERN NSString * const ALERTTITLE;
UIKIT_EXTERN NSString * const USERID;
UIKIT_EXTERN NSString * const MYINVITECODE;
UIKIT_EXTERN NSString * const USERSecret;
UIKIT_EXTERN NSString * const LoginSecret;
UIKIT_EXTERN NSString * const NCUserLastAreaId; //用户最后一次选择的地区Id
UIKIT_EXTERN NSString * const NCUserLastAreaName; // 用户最后一次选择的名字
UIKIT_EXTERN NSString * const TABLE_FRIENDS; //好友表
UIKIT_EXTERN NSString * const TABLE_ASSOCIATOR; // 会员表
UIKIT_EXTERN NSString * const TABLE_VIDEOPROGRESS; // 视频进度
UIKIT_EXTERN NSString * const TABLE_AUDIOPROGRESS; // 音频进度
UIKIT_EXTERN NSString * const USERNAME;//用户名
UIKIT_EXTERN NSString * const PHOTOURL; // 用户头像地址
UIKIT_EXTERN NSString * const GROUPNAME; //群组头像
UIKIT_EXTERN NSString * const FROMID; //推送id
UIKIT_EXTERN NSString * const USERLoctionLng; // 用户的经度
UIKIT_EXTERN NSString * const USERLoctionLat; // 用户的纬度
UIKIT_EXTERN NSString * const NICKNAME;  //用户昵称
UIKIT_EXTERN NSString * const GROUPURL; //群组头像






//---------------系统相关------------
UIKIT_EXTERN NSString * const Service;   //指令参数
UIKIT_EXTERN NSString * const Register;  //注册
UIKIT_EXTERN NSString * const GetToken;  //获取token
UIKIT_EXTERN NSString * const Login;     //登录
UIKIT_EXTERN NSString * const Logout;    //退出
UIKIT_EXTERN NSString * const ResetPwd;  //重置密码
UIKIT_EXTERN NSString * const GetBaseInfo; //获取个人信息
UIKIT_EXTERN NSString * const SetIosToken; //设置token
UIKIT_EXTERN NSString * const SetPwd;      //修改密码
UIKIT_EXTERN NSString * const SetNickname; //设置昵称
UIKIT_EXTERN NSString * const SetSex;      //设置性别
UIKIT_EXTERN NSString * const SetAvatar;   //修改头像
UIKIT_EXTERN NSString * const SetPersonalSign;  //修改/设置 个性签名
UIKIT_EXTERN NSString * const SetCompany;     // 设置单位名称
UIKIT_EXTERN NSString * const SetTitle;     // 设置单位职务
UIKIT_EXTERN NSString * const GetInitConfig; //获取系统配置
UIKIT_EXTERN NSString * const Feedback;    //用户反馈
UIKIT_EXTERN NSString * const UserSetMoreInfo; //设置更多信息
UIKIT_EXTERN NSString * const GetUserPiliList; //我的直播
UIKIT_EXTERN NSString * const UserGetDisplayInfo; // 获取IM的UI信息
UIKIT_EXTERN NSString * const UserSetIndustryId; //个人信息 设置用户行业
UIKIT_EXTERN NSString * const GetNotifiMessageList; //获取系统消息
UIKIT_EXTERN NSString * const GetUserHonorList; //勋章
UIKIT_EXTERN NSString * const GetIntegrateRecord; //积分记录


//-----------微课------------

UIKIT_EXTERN NSString * const GetIndexList;  // 微课首页banner 和 segment title
UIKIT_EXTERN NSString * const GetVideoListByClass;  // 微课首页类别视频
UIKIT_EXTERN NSString * const GetVideoInfo; //视频详细页接口
UIKIT_EXTERN NSString * const AddComment; //视频评价接口
UIKIT_EXTERN NSString * const DeleteUserFollow;  //增加关注
UIKIT_EXTERN NSString * const AddFollows;  // 删除关注
UIKIT_EXTERN NSString * const AddCollectoins; // 增加收藏
UIKIT_EXTERN NSString * const DeleteUserCollection; // 取消收藏
UIKIT_EXTERN NSString * const ClearUserCollection; // 清空收藏
UIKIT_EXTERN NSString * const GetUserCollections; // 获取收藏
UIKIT_EXTERN NSString * const GetSiteIndex; // 链城小站 广告
UIKIT_EXTERN NSString * const GetSiteMessages; // 链城小站 首页
UIKIT_EXTERN NSString * const GetSiteMessageInfo; // 链城小站 详情
UIKIT_EXTERN NSString * const PinncCommentOrder; //小站评价

//------------------圈子部分--------------------
UIKIT_EXTERN NSString * const CirclePublishMessage; //发布课间或是班级的动态  ctype 1=动态（classesId＝0表示课间 classesId>0表示某一班级的） 2=江湖
UIKIT_EXTERN NSString * const CircleGetMessages; //获取最新动态
UIKIT_EXTERN NSString * const CircleGetMessageComments; //获取朋友圈动态的评论
UIKIT_EXTERN NSString * const CircleThumbMessage;//点赞
UIKIT_EXTERN NSString * const CircleCancelThumb; // 取消赞
UIKIT_EXTERN NSString * const CircleCommentMessage; //江湖评论
UIKIT_EXTERN NSString * const AppGetCodeList; //获取江湖标签 1=学校类型；2=教师部门；3=学员组号；4=课程类型；5=链城小站；6=江湖的标签 及个人的需求与资料；7=设施
UIKIT_EXTERN NSString * const CircleDeleteMessage; //江湖删除说说
UIKIT_EXTERN NSString * const CircleDeleteCommentMessage; //江湖删除评论
UIKIT_EXTERN NSString * const CircleGetMyCircle; //获取我的发布和参与 参数里的 pubType 字段，1表示用户发布的，2表示用户参与的用于根据msgId
UIKIT_EXTERN NSString * const CircleGetHelpListInfo; //获取帮助详情
UIKIT_EXTERN NSString * const CircleReportMessage; //举报
UIKIT_EXTERN NSString * const CircleGetUserInfo; //获取其他用户信息
UIKIT_EXTERN NSString * const CircleGetMessageInfo; //获取单条信息详细
UIKIT_EXTERN NSString * const UserGetUserClasses; //获取我的班级
UIKIT_EXTERN NSString * const UserGetUsersByClassId; //班级详细
UIKIT_EXTERN NSString * const AppGetUserStudy; //获取学习任务
UIKIT_EXTERN NSString * const AppGetUserStudyTime;//获取所有有效学习任务
UIKIT_EXTERN NSString * const AppGetClasses; //获取app中所有班级
UIKIT_EXTERN NSString * const AppJoinClasses; //加入班级
UIKIT_EXTERN NSString * const AppAcceptJoinClasses; // 同意或者拒绝加入班级


//------------------好友部分--------------------

UIKIT_EXTERN NSString * const CircleGetFriendList; //获取好友列表（0表示所有的，1表示互相都是好友、2表示等待我确认、3表示等待对方确
UIKIT_EXTERN NSString * const CircleAddUserAsFriend; //请求添加对方为好友
UIKIT_EXTERN NSString * const CircleConfirmAddAsFriend; //确认通过对方的添加好友请
UIKIT_EXTERN NSString * const CircleDeleteFriend; //删除好友
UIKIT_EXTERN NSString * const CircleSetFriendNoteName; //设置好友备注
UIKIT_EXTERN NSString * const CircleSearchUser; //根据手机号码搜索用户
UIKIT_EXTERN NSString * const CircleGetGroupUserInfo; //获取群组成员的用户信息
UIKIT_EXTERN NSString * const CircleAddChatGroup; //新建聊天群组并上传信息
UIKIT_EXTERN NSString * const CircleGetChatGroupsInfo; //获取聊天群组信息
UIKIT_EXTERN NSString * const CircleModifyChatGroupInfo; //修改聊天群组信息
UIKIT_EXTERN NSString * const CircleGetUserList; //获取同窗
UIKIT_EXTERN NSString * const CircleGetFollowsList; //获取关注
UIKIT_EXTERN NSString * const CircleAddMessageHelp; //江湖的我来帮助接口
UIKIT_EXTERN NSString * const CircleAcceptMessageHelp; //接受或者拒绝帮助
UIKIT_EXTERN NSString * const CircleFinishMessageHelp; //完成或者帮助失败
UIKIT_EXTERN NSString * const AppGetSeacherCodes; //获取搜索条件
UIKIT_EXTERN NSString * const CircleGetCommonFriendList; //获取共同好友
UIKIT_EXTERN NSString * const UserGetDisplayListInfo; //获取好友头像昵称信息

//--------------------直播相关-----------------

UIKIT_EXTERN NSString * const AppCreatePiliStrea; //创建一个直播推流
UIKIT_EXTERN NSString * const AppGetPiliStream; //获取直播信息
UIKIT_EXTERN NSString * const AppGetPiliList; // 获取直播列表
UIKIT_EXTERN NSString * const AppGetPiliInfo; //获取直播详情
UIKIT_EXTERN NSString * const AppGetPiliState; //获取直播的当前状态
UIKIT_EXTERN NSString * const AppGetPiliAccessToken; //获取直播token
UIKIT_EXTERN NSString * const AppGetPiliGenToken;  //推流token
UIKIT_EXTERN NSString * const AppGetTeacherPiliList; //获取老师能直播的列表

UIKIT_EXTERN NSString * const AppGetMyVideo; //我的直播
UIKIT_EXTERN NSString * const AppGetVideoDetail; //直播详情
UIKIT_EXTERN NSString * const VideoLiveBeginPush; //开始直播
UIKIT_EXTERN NSString * const SetAlwaysRecordByCid; //开始录播
UIKIT_EXTERN NSString * const VideoLiveKeepPush; //保持直播
UIKIT_EXTERN NSString * const VideoLiveEndPush; //停止直播
UIKIT_EXTERN NSString * const VideoLiveSleepPush; //切换至后台
UIKIT_EXTERN NSString * const VideoLiveGetChannerState; //获取网易上的状态
UIKIT_EXTERN NSString * const VideoLiveGetVideoListByCid; //获取直播回放地址
UIKIT_EXTERN NSString * const VideoLiveEnrollVideoLives; //预约状态


//-------------------支付相关---------------------
UIKIT_EXTERN NSString * const UserTopupByThird; //用户充值
UIKIT_EXTERN NSString * const PayForProducts;   // 小站下单
UIKIT_EXTERN NSString * const GetOrderList;     // 我的订单列表
UIKIT_EXTERN NSString * const UserGetBalance;   //获取用户余额
UIKIT_EXTERN NSString * const UserGetBalanceAndRecords;//账户流水明细
UIKIT_EXTERN NSString * const UserApplyWithdraw; //提现
UIKIT_EXTERN NSString * const AppGetUserLevelSet;//购买会员
UIKIT_EXTERN NSString * const AppDoReward; //打赏
UIKIT_EXTERN NSString * const PinncPayForOrder; // 订单支付
UIKIT_EXTERN NSString * const PinncApplePayForOk; // 苹果充值验证

//-------------------共享---------------------

UIKIT_EXTERN NSString * const GetNewsCate; //共享分类
UIKIT_EXTERN NSString * const GetCodeList; //共享的标签
UIKIT_EXTERN NSString * const GetNewsList; //共享列表
UIKIT_EXTERN NSString * const GetNewsInfo; //共享详细
UIKIT_EXTERN NSString * const MyCollections; //我的收藏


UIKIT_EXTERN NSString * const CircleAddCollection; // 添加收藏
UIKIT_EXTERN NSString * const CircleCancelCollection; // 取消收藏
UIKIT_EXTERN NSString * const AddIntegrateRecord; //增加积分

UIKIT_EXTERN NSString * const VidoeoGetComments; // 视频评论列表
UIKIT_EXTERN NSString * const VideoDeleteCommentMessage; //微课删除评论

UIKIT_EXTERN NSString * const VideosAddNotes; //写笔记
UIKIT_EXTERN NSString * const GetGetMyNotes; //我的笔记
UIKIT_EXTERN NSString * const DelMyNotes; //删除笔记
UIKIT_EXTERN NSString * const UpdateNotes; //修改笔记


UIKIT_EXTERN NSString * const CircleAddMessagesQusA; // 投票

UIKIT_EXTERN NSString * const GetMyReadVideoList; //我的在读列表

UIKIT_EXTERN NSString * const GetTeachersById; //讲师信息

UIKIT_EXTERN NSString * const CircleCopyNewsToMessage; //分享到班级


#endif
