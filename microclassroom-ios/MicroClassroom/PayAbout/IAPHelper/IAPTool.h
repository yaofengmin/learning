
//
//  IAPTool.h
//  ;
//
//  Created by Htain Lin Shwe on 10/7/12.
//  Copyright (c) 2012 Edenpod. All rights reserved.
//

#import "IAPHelper.h"
#import <Foundation/Foundation.h>
#import "Singleton.h"
/**
 *  内支付工具类
 */
@interface IAPTool : NSObject {
}

singleton_interface(IAPTool);

/** 订单号 */
@property (nonatomic, copy) NSString *orderId;
/** iap帮助类 */
@property (nonatomic, strong) IAPHelper *iap;
/** 产品信息 */
@property (nonatomic, strong) SKProductsResponse *response;

+ (id)toJSON:(NSString *)json;
/** 获取产品信息 */
//+ (NSMutableArray *)getProducts;

/** 获取商品列表 */
- (void)initProducts;

/** 通过ProductId获取商品对象 */
- (SKProduct *)getSKProductByProductId:(NSString *)productId;


- (void)subimtFailedReceipt;

@end
