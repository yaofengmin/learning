//
//  IAPTool.m
//  inappPurchasesTest
//
//  Created by Htain Lin Shwe on 10/7/12.
//  Copyright (c) 2012 Edenpod. All rights reserved.
//


#import "GTMBase64.h"
#import "IAPTool.h"
#import "RechargeModel.h"

#if !__has_feature(objc_arc)
#error You need to either convert your project to ARC or add the -fobjc-arc compiler flag to IAPTool.m.
#endif

@implementation IAPTool

singleton_implementation(IAPTool);

@synthesize iap = _iap;

+ (id)toJSON:(NSString *)json {
    NSError *e = nil;
    if (json) {
        id jsonObject = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding]
                                                        options:NSJSONReadingMutableContainers
                                                          error:&e];

        if (e == nil) {
            return jsonObject;
        } else {
            NSLog(@"%@", [e localizedDescription]);
            return nil;
        }
    }
    return nil;
}

#pragma mark - 内购商品ID
+ (NSSet *)getProductesIDs {

    NSArray<RechargeModel *> *showList = [InitConfigModel shareInstance].topup_virtual;
    NSMutableSet *identifiers = [NSMutableSet set];
    for (RechargeModel *model in showList) {
        [identifiers addObject:model.apiId];
    }
    return identifiers;
}


#pragma mark 判断productID是否为有效商品ID

+ (BOOL)productesIDContainsID:(NSString *)productID {
    if ([productID isEqualToString:@""] || productID == nil) return NO;

    NSSet *identifiers = [IAPTool getProductesIDs];
    return [identifiers containsObject:productID];
}

- (void)initProducts {
    if (![IAPTool sharedIAPTool].iap) {
        NSSet *dataSet = [IAPTool getProductesIDs];
        [IAPTool sharedIAPTool].iap = [[IAPHelper alloc] initWithProductIdentifiers:dataSet];
    }
// 沙盒模式
#if (IAP_PRODUCTION == 0)
    [IAPTool sharedIAPTool].iap.production = NO;
// 线上模式
#else
    [IAPTool sharedIAPTool].iap.production = YES;
#endif

    [[IAPTool sharedIAPTool].iap requestProductsWithCompletion:^(SKProductsRequest *request, SKProductsResponse *response) {
        _response = response;
    }];
}

- (SKProduct *)getSKProductByProductId:(NSString *)productId {
    if (_response) {
        for (int i = 0; i < _response.products.count; i++) {
            SKProduct *product = [_response.products objectAtIndex:i];
            if ([productId isEqualToString:product.productIdentifier]) {
                return product;
            }
        }
    }
    return nil;
}

@end
