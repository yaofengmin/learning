//
//  LoginViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//
#import "AppDelegate.h"

#import "YHNavgationController.h"
#import "YhjRootViewController.h"
#import "LoginViewController.h"
#import "ForgetPasswordVC.h"
#import "RegisterVC.h"

#import "UserInfoModel.h"

#import "NSString+MD5.h"

#import "SecondLaunchImageViewController.h"

static NSString * const kLoginNavTitle = @"登录";
static NSString * const kLoginNavRightItemTitle = @"注册";

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *phoneNumTxt;
@property (weak, nonatomic) IBOutlet UIView *phoneNumView;

@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UIView *pwdView;
@property (weak, nonatomic) IBOutlet UIImageView *loginImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logImageTop;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *TouristsBtn;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationBar.hidden = YES;
    
    [self layoutSubViews];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewEndEditing)];
    [self.view addGestureRecognizer:tap];
    
    KAddobserverNotifiCation(@selector(isShowTouristsBtn:), KLOGINVIEWISSHOWTOURISTS);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AutoLayout

- (void)layoutSubViews {
    self.TouristsBtn.hidden = ![InitConfigModel shareInstance].isShowTourists;
//    self.TouristsBtn.hidden = YES;
    self.loginImage.layer.cornerRadius = 4.0;
    self.loginImage.layer.masksToBounds = YES;
    self.phoneNumView.layer.cornerRadius = 4.0;
    self.phoneNumView.layer.masksToBounds = YES;
    self.pwdView.layer.cornerRadius = 4.0;
    self.pwdView.layer.masksToBounds = YES;
    self.loginBtn.layer.cornerRadius = 4.0;
    self.loginBtn.layer.masksToBounds = YES;
}




#pragma mark - UIButton Functions

- (void)login:(UIButton *)sender {
    
    if ([self checkParam]) {
        [self loginWitMoblie:self.phoneNumTxt.text psw:self.pwdTxt.text];
    }
    
}

-(void)loginWitMoblie:(NSString *)moblie psw:(NSString *)psw
{
    NSDictionary *paramDic = @{@"service": Login,
                               @"mobile": moblie,
                               @"passwd": [NSString md5:psw]
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                UserInfoModel * user = [UserInfoModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                [UserInfoModel saveUserInfoModelWithModel:user];
                KsetUserValueByParaName([NSString md5:self.pwdTxt.text], USERSecret);
                KsetUserValueByParaName(user.secret, LoginSecret);
                [(AppDelegate *)[[UIApplication sharedApplication] delegate] homeRequestWithFinishBlock:^{//获取个人信息完成
                    KPostNotifiCation(NOTIGICATIONFINDVVIEWRELOAD, nil);
                    KPostNotifiCation(NOTIGICATIONFINDFITERBUTTON, nil);
                    if (![[[[UIApplication sharedApplication]delegate] window].rootViewController isEqual:self.navigationController]) {
                        [(AppDelegate *)[[UIApplication sharedApplication] delegate] loginEMSDK];
                        [self backBtnAction];
                    }else{
                        [[UIApplication sharedApplication].keyWindow removeAllSubviews];
                        NSDictionary *infoDic = KgetUserValueByParaName(HCmyDyItems);
                        if ([[infoDic objectForSafeKey:@"brandInfo"] isKindOfClass:[NSDictionary class]]) {//存在平台信息,启动平台信息logo
                            SecondLaunchImageViewController  *lauchImage = [[SecondLaunchImageViewController alloc]init];
                            YHNavgationController *rootNav = [[YHNavgationController alloc]initWithRootViewController:lauchImage];
                            [[[[UIApplication sharedApplication] delegate] window] setRootViewController:rootNav];
                        }else{
                            YhjRootViewController  *rootVc = [[YhjRootViewController alloc]init];
                            YHNavgationController *rootNav = [[YHNavgationController alloc]initWithRootViewController:rootVc];
                            [(AppDelegate *)[[UIApplication sharedApplication] delegate] loginEMSDK];
                            [[[[UIApplication sharedApplication] delegate] window] setRootViewController:rootNav];
                        }
                    }
                }];
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}
- (IBAction)forgetPassword:(UIButton *)sender {
    
    [self.navigationController pushViewController:[[ForgetPasswordVC alloc] init]
                                         animated:YES];
    
}

#pragma mark ===判断参数===

-(BOOL)checkParam {
    
    if ([YHJHelp valiMobile:self.phoneNumTxt.text]) {
        [WFHudView showMsg:@"请填写正确手机号码" inView:nil];
        return NO;
        
    }
    
    if (self.pwdTxt.text.length == 0) {
        [WFHudView showMsg:@"请填写密码" inView:nil];
        return NO;
    }
    
    //各参数情况判断
    
    return YES;
}
- (IBAction)registerAction:(UIButton *)sender {
    
    [self.navigationController pushViewController:[[RegisterVC alloc] init]
                                         animated:YES];
    
}
- (IBAction)loginAction:(UIButton *)sender {
    [self.view endEditing:YES];
    [self login:sender];
}

#pragma mark === 以游客方式登录
- (IBAction)touristsAction:(UIButton *)sender {
    [self.view endEditing:YES];
    [self loginWitMoblie:kTouristsMoblie psw:kTouristsPsw];
}

-(void)isShowTouristsBtn:(NSNotification *)nofi
{
    self.TouristsBtn.hidden = ![InitConfigModel shareInstance].isShowTourists;
//    self.TouristsBtn.hidden = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField =0;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
#pragma mark === 点击键盘收回
-(void)viewEndEditing
{
    [self.view endEditing:YES];
}

- (void)dealloc
{
    KRemoverNotifi(KLOGINVIEWISSHOWTOURISTS);
}

@end
