//
//  RegisterVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "RegisterVC.h"
#import "FillInfomationVC.h"
#import "YHBaseWebViewController.h"
#import "VerifyCodeBtn.h"

static NSString * const kRegisterNavTitle = @"注册";
static NSString * const kNextBtnTitle = @"下一步";
static NSString * const kDefualtVerifyCode = @"NULL";

@interface RegisterVC ()

{
    NSString * _verifyCode;
}

@property (weak, nonatomic) IBOutlet UIButton *readProtocolBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectReadStateBtn;
@property (weak, nonatomic) IBOutlet VerifyCodeBtn *sendCodeBtn;
@property (weak, nonatomic) IBOutlet UIView *verifyCodeView;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumTxt;
@property (weak, nonatomic) IBOutlet UIImageView *login_icon;
@property (weak, nonatomic) IBOutlet UITextField *verifyCodeTxt;
@property (weak, nonatomic) IBOutlet UIView *phoneNumView;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpNav];
    [self setSendVerifyCodeBtn];
    [self layoutSubViews];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewEndEditing)];
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSendVerifyCodeBtn {
    
    __weak __typeof(VerifyCodeBtn *)weakBtn = self.sendCodeBtn;
    @weakify(self);
    [self.sendCodeBtn setTime:60 ClickBlock:^{
        @strongify(self);
        
        [self getVerifyCode:^{
            [weakBtn startCountDown];
            [self.verifyCodeTxt becomeFirstResponder];
        } fail:^{
            [weakBtn cancelCountDown];
        }];
        
    }];
    
}

#pragma mark - AutoLayout
- (void)layoutSubViews {
    
    self.login_icon.layer.cornerRadius = 4.0;
    self.login_icon.layer.masksToBounds = YES;
    self.phoneNumView.layer.cornerRadius = 4.0;
    self.phoneNumView.layer.masksToBounds = YES;
    self.verifyCodeView.layer.cornerRadius = 4.0;
    self.verifyCodeView.layer.masksToBounds = YES;
    self.nextBtn.layer.cornerRadius = 4.0;
    self.nextBtn.layer.masksToBounds = YES;
    self.sendCodeBtn.layer.cornerRadius = 4.0;
    self.sendCodeBtn.layer.masksToBounds = YES;
    
//    self.readProtocolBtn
    NSString *text = @"我已阅读《软件服务协议》";
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:text];
    [string addAttributes:@{NSForegroundColorAttributeName:NEColor} range:NSMakeRange(4, text.length - 4)];
    
    [self.readProtocolBtn.titleLabel setAttributedText:string];
}

#pragma mark - Navigation
- (void)setUpNav {
    
//    self.yh_navigationItem.title = kRegisterNavTitle;
    self.yh_navigationBar.hidden = YES;
    
}

#pragma mark - UIButton Functions
- (void)goToNext:(UIButton *)sender {
    
    if ([self checkNextParam]) {
        FillInfomationVC * targetVC = [[FillInfomationVC alloc] init];
        targetVC.verifyCode = _verifyCode;
        targetVC.mobile = self.phoneNumTxt.text;
        [self.navigationController pushViewController:targetVC animated:YES];
    }
        
}

- (IBAction)readProtocol:(UIButton *)sender {
    
    YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:[InitConfigModel shareInstance].reg_url andNavTitle:@"软件许可及服务协议"];
    [self.navigationController pushViewController:web animated:YES];
}

- (IBAction)changeProtocolReadState:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
}

- (void)getVerifyCode:(void(^)())success fail:(void(^)())fail {
    
    if ([self checkGetVerifyCodeParam]) {
        
        NSDictionary *paramDic = @{@"service": GetToken,
                                   @"mobile": self.phoneNumTxt.text,
                                   @"checkExists": @YES
                                   };
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功

                    _verifyCode = result[@"token"];
                    success();
                    
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    fail();
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
                
            }
        }];
        
        
    } else { fail(); }
    
}

-(BOOL)checkNextParam
{
    
    if ([YHJHelp valiMobile:self.phoneNumTxt.text] || self.verifyCodeTxt.text.length == 0) {
        [WFHudView showMsg:@"请填写全部信息" inView:nil];
        return NO;
    }
    
    if ([_verifyCode isEqualToString:kDefualtVerifyCode]) {
        [WFHudView showMsg:@"请获取验证码" inView:nil];
        return NO;
    }
    
    if (![self.verifyCodeTxt.text isEqualToString:_verifyCode]) {
        [WFHudView showMsg:@"请填写正确验证码" inView:nil];
        return NO;
    }
    
    if (!self.selectReadStateBtn.selected) {
        [WFHudView showMsg:@"请阅读并同意协议" inView:nil];
        return NO;
    }
    
    return YES;
}

-(BOOL)checkGetVerifyCodeParam
{
    
    if ([YHJHelp valiMobile:self.phoneNumTxt.text]) {
        [WFHudView showMsg:@"请填写正确手机号" inView:nil];
        return NO;
    }
    
    return YES;
}
- (IBAction)nextAction:(UIButton *)sender {
    [self goToNext:sender];
}
- (IBAction)backAction:(UIButton *)sender {
    [self backBtnAction];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField =0;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
#pragma mark === 点击键盘收回
-(void)viewEndEditing
{
    [self.view endEditing:YES];
}

@end
