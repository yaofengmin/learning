//
//  ForgetPasswordVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "ForgetPasswordVC.h"

#import "VerifyCodeBtn.h"

static NSString * const kForgetPwdNavTitle = @"重置密码";
static NSString * const kComfirmBtnTitle = @"确认";
static NSString * const kDefualtVerifyCode = @"NULL";

@interface ForgetPasswordVC ()

{
    NSString * _verifyCode;
}

@property (weak, nonatomic) IBOutlet VerifyCodeBtn *sendCodeBtn;
@property (weak, nonatomic) IBOutlet UIView *pwdView;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumTxt;
@property (weak, nonatomic) IBOutlet UITextField *verifyCodeTxt;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;

@end

@implementation ForgetPasswordVC

- (void)dealloc {
    
    NSLog(@"%@ dealloc",self);
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpNav];
    
    [self setSendVerifyCodeBtn];
    
    [self layoutSubViews];
    
    _verifyCode = kDefualtVerifyCode;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSendVerifyCodeBtn {

    __weak __typeof(VerifyCodeBtn *)weakBtn = self.sendCodeBtn;
    @weakify(self);
    [self.sendCodeBtn setTime:60 ClickBlock:^{
        @strongify(self);
        
        [self getVerifyCode:^{
            [weakBtn startCountDown];
        } fail:^{
            [weakBtn cancelCountDown];
        }];
  
    }];
    
}

#pragma mark - AutoLayout

- (void)layoutSubViews {
    
    DIYButton *comfirmBtn = [DIYButton buttonWithImage:nil andTitel:kComfirmBtnTitle andBackColor:MainBtnColor];
    [self.view addSubview:comfirmBtn];
    
    [comfirmBtn addTarget:self action:@selector(comfirm:) forControlEvents:UIControlEventTouchUpInside];
    comfirmBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [comfirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.leading.equalTo(self.pwdView.mas_leading);
        make.trailing.equalTo(self.pwdView.mas_trailing);
        make.top.equalTo(self.pwdView.mas_bottom).offset(60);
        make.height.mas_equalTo(@45);
        
    }];
    
}

#pragma mark - Navigation
- (void)setUpNav {
    
    self.yh_navigationItem.title = kForgetPwdNavTitle;
    
}

#pragma mark - UIButton Functions

- (void)comfirm:(UIButton *)sender {
    
    if ([self checkCommitParam]) {
        
        NSDictionary *paramDic = @{@"service": ResetPwd,
                                   @"mobile": self.phoneNumTxt.text,
                                   @"newPwd": self.pwdTxt.text
                                   };
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
                
            }
        }];
        
    }

    
}

- (void)getVerifyCode:(void(^)())success fail:(void(^)())fail {
    
    if ([self checkGetVerifyCodeParam]) {
        
        NSDictionary *paramDic = @{@"service": GetToken,
                                   @"mobile": self.phoneNumTxt.text,
                                   @"checkExists": @(0)
                                   };
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    // TODO
                    _verifyCode =result[@"token"];
                    success();
                    
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    fail();
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];

    } else { fail(); }

}

#pragma mark ===判断参数===

-(BOOL)checkCommitParam
{
    
    if ([YHJHelp valiMobile:self.phoneNumTxt.text] || self.pwdTxt.text.length == 0 || self.verifyCodeTxt.text.length == 0) {
        [WFHudView showMsg:@"请填写全部信息" inView:nil];
        return NO;
        
    }
    
    if (self.pwdTxt.text.length < 6) {
        
        [WFHudView showMsg:@"密码不少于6位" inView:nil];
        return NO;
        
    }
    
    if ([_verifyCode isEqualToString:kDefualtVerifyCode]) {
        [WFHudView showMsg:@"请获取验证码" inView:nil];
        return NO;
        
    }
    
    if (![self.verifyCodeTxt.text isEqualToString:_verifyCode]) {
        [WFHudView showMsg:@"请填写正确验证码" inView:nil];
        return NO;
    
    }
    
    return YES;
}

-(BOOL)checkGetVerifyCodeParam
{
    
    if ([YHJHelp valiMobile:self.phoneNumTxt.text]) {
        
        [WFHudView showMsg:@"请填写正确手机号码" inView:nil];
        return NO;
    }
    
    return YES;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField =0;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
@end
