//
//  FillInfomationVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>

#import "FillInfomationVC.h"
#import "UserInfoModel.h"
#import "CameraTakeManager.h"
#import "NSString+MD5.h"
#import "AppDelegate.h"

static NSString * const kFillInfoNavTitle = @"完善信息";
static NSString * const kCompleteBtnTitle = @"完成";

@interface FillInfomationVC ()
<
CLLocationManagerDelegate
>

{
    CLLocationManager * _locationManager;
    CLLocationCoordinate2D _userCoordinate;
    UIButton * _lastBtn;
}

@property (weak, nonatomic) IBOutlet UIButton *manBtn;
@property (weak, nonatomic) IBOutlet UIButton *womanBtn;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIView *pwdView;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *nicknameTxt;
@property (weak, nonatomic) IBOutlet UITextField *inviteTxt;
@property (weak, nonatomic) IBOutlet UITextField *honourField;

@property (nonatomic ,strong) UIImage *headImage;
@end

@implementation FillInfomationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpNav];
    [self layoutSubViews];
    [self setUpLocationManager];
    
    _lastBtn = self.manBtn;
    
    self.avatarImageView.userInteractionEnabled = YES;

    [self.avatarImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                       action:@selector(selectAvater:)]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpLocationManager {
    
    _locationManager = [[CLLocationManager alloc] init];
//    [_locationManager requestAlwaysAuthorization];
    [_locationManager requestWhenInUseAuthorization];
    
    [self startLoaction];

}

- (void)startLoaction
{
    if([CLLocationManager locationServicesEnabled]){
        
        _locationManager.distanceFilter  = kCLDistanceFilterNone;    //距离筛选器，单位米（移动多少米才回调更新）
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest; //精确度
        [_locationManager setDelegate:self];
        [_locationManager startUpdatingLocation];
        
    }
    
}

#pragma mark – CLLocationManagerDelegate
//定位成功回调
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
  
        _userCoordinate = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
        [_locationManager stopUpdatingLocation];
    
}

//定位失败回调
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {

}

#pragma mark - AutoLayout
- (void)layoutSubViews {
    
    DIYButton *completeBtn = [DIYButton buttonWithImage:nil andTitel:kCompleteBtnTitle andBackColor:MainBtnColor];
    [self.view addSubview:completeBtn];
    
    [completeBtn addTarget:self action:@selector(complete:) forControlEvents:UIControlEventTouchUpInside];
    completeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [completeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.leading.equalTo(self.pwdView.mas_leading);
        make.trailing.equalTo(self.pwdView.mas_trailing);
        make.top.equalTo(self.honourField.mas_bottom).offset(100);
        make.height.mas_equalTo(@45);
        
    }];
    
}

#pragma mark - Navigation
- (void)setUpNav {
    self.yh_navigationItem.title = kFillInfoNavTitle;
}

#pragma mark - UIButton Functions
- (void)complete:(UIButton *)sender {
    
    if ([self checkcompleteParam]) {
        
        NSDictionary *paramDic = @{@"service": Register,
                                   @"mobile": self.mobile,
                                   @"password": [NSString md5:self.pwdTxt.text],
                                   @"token": self.verifyCode,
                                   @"nickname": self.nicknameTxt.text,
                                   @"sex": _lastBtn == self.manBtn ? @"男" : @"女",
                                   @"longitude": @(_userCoordinate.longitude),
                                   @"latitude": @(_userCoordinate.latitude),
                                   @"referee":self.inviteTxt.text,
                                   @"title":self.honourField.text
                                   };
        
        NSDictionary *imageDic = @{@"pics":@[UIImageJPEGRepresentation(self.avatarImageView.image, 0.5)],
                                   @"fileLoc":self.mobile};
        
        SHOWHUD;
        
        [HTTPManager publishCarCirclWithUrl:MainAPI andPostParameters:paramDic andImageDic:imageDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    
                    UserInfoModel * user = [UserInfoModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                    
                    [UserInfoModel saveUserInfoModelWithModel:user];
        
                    KsetUserValueByParaName([NSString md5:self.pwdTxt.text], USERSecret);
                    [[[UIApplication sharedApplication] delegate].window removeAllSubviews];
                    [(AppDelegate *)[[UIApplication sharedApplication] delegate] setUpRootViewController];
                
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    
                }
                
            }
        }];
        
    }
}

- (IBAction)selectSex:(UIButton *)sender {
    
    _lastBtn.selected = NO;
    _lastBtn = sender;
    _lastBtn.selected = YES;
    
}

- (void)selectAvater:(UITapGestureRecognizer *)sender {

    [self.view endEditing:YES];
    @weakify(self);
    [[CameraTakeManager sharedInstance] cameraSheetInController:self handler:^(UIImage *image, NSString *imagePath) {
        @strongify(self);
        self.avatarImageView.image = image;
        self.headImage = image;
    } cancelHandler:^{}];
    
}

- (BOOL)checkcompleteParam
{
    if (self.headImage == nil) {
        [WFHudView showMsg:@"请设置您的头像" inView:self.view];
        return NO;
    }
    if (self.nicknameTxt.text.length == 0) {
        
        [WFHudView showMsg:@"请填写昵称" inView:self.view];
        return NO;
    }
    
    if (self.pwdTxt.text.length < 6) {
        
        [WFHudView showMsg:@"密码不少于6位" inView:self.view];
        return NO;
    
    }
    
    if (IsEmptyStr(self.inviteTxt.text)) {
        [WFHudView showMsg:@"请填写您的推荐人" inView:self.view];
        return NO;
    }
    
    
    if (IsEmptyStr(self.honourField.text)) {
        [WFHudView showMsg:@"请填写您的职务" inView:self.view];
        return NO;
    }
    return YES;
}


//- (void)backBtnAction {
//
//    UIViewController *controller = self.navigationController.viewControllers[0];
//    [controller dismissViewControllerAnimated:YES completion:nil];
//}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField =0;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}

@end
