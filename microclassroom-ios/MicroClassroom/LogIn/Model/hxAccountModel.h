//
//  hxAccountModel.h
//  EnergyConservationPark
//
//  Created by Hanks on 16/7/11.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "YHBaseModel.h"

@interface hxAccountModel : YHBaseModel

@property(nonatomic, copy) NSString *username;
@property(nonatomic, copy) NSString *uuid;
@property(nonatomic, copy) NSString *psd;

@end
