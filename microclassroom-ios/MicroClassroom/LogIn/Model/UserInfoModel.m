//
//  UserInfoModel.m
//  EnergyConservationPark
//
//  Created by Hanks on 16/6/4.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "UserInfoModel.h"
#import "MyHomeInfoModel.h"

#define filPath   [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"userInfo.data"]


@interface UserInfoModel ()<NSCoding>

@end
static UserInfoModel *model;
@implementation UserInfoModel


-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    
    
}

-(instancetype)initWithCoder:(NSCoder *)coder
{
  return [super initWithCoder:coder];
}

+(void)saveUserInfoModelWithModel:(UserInfoModel *)model {
    [NSKeyedArchiver archiveRootObject:model toFile:filPath];
    KsetUserValueByParaName(model.userId, USERID);
    
}

+(void)clearModel {
    [NSKeyedArchiver archiveRootObject:[NSNull null] toFile:filPath];
    KsetUserValueByParaName(@"", USERID);
    model = nil;
}

+(UserInfoModel *)getInfoModel
{
    id model = [NSKeyedUnarchiver unarchiveObjectWithFile:filPath];
    
    if (model == nil) {
        return (UserInfoModel *)model;
    }else if ([model isEqual:[NSNull null]]){
        return nil;
    }else{
        return model;
    }
 
}

+(void)initialize
{
     model = (UserInfoModel *)[NSKeyedUnarchiver unarchiveObjectWithFile:filPath];
}

+(NSDictionary *)mj_objectClassInArray{
    return @{@"menusTopList":[MyHomeInfoModel class],@"menusCenterList":[MyHomeInfoModel class]};

}
@end
