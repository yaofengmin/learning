//
//  UserInfoModel.h
//  EnergyConservationPark
//
//  Created by Hanks on 16/6/4.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "YHBaseModel.h"
#import "hxAccountModel.h"
#import "MyHomeInfoModel.h"
@interface UserInfoModel : YHBaseModel

@property (nonatomic, copy) NSString *myInviteCode;

/**
 学号
 */
@property (nonatomic, copy) NSString *userCode;

@property (nonatomic, copy) NSString *mobile;

@property (nonatomic, copy) NSString *linkTel;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *email;

@property (nonatomic, copy) NSString *brief;

@property (nonatomic, copy) NSString *company;//单位

/**
 兴趣爱好
 */
@property (nonatomic, copy) NSString *hobby;

/**
 领域
 */
@property (nonatomic, copy) NSString *knowledge;

/**
 来往城市
 */
@property (nonatomic, copy) NSString *runingCity;

@property (nonatomic, copy) NSString *personalSign;

/**
 行业
 */
@property (nonatomic, copy) NSString *industryId;
@property (nonatomic, copy) NSString *industryName;

/**
 codeId = 4;
 codeName = "\U5bfb\U533b";
 meno = "";
 */
@property (nonatomic, copy) NSArray *label_need;

@property (nonatomic, copy) NSArray *label_have;

@property (nonatomic, strong) hxAccountModel *hxAccount;

@property (nonatomic, copy) NSString *sex;

@property (nonatomic, copy) NSString *userId;

@property (nonatomic, copy) NSString *photo;

@property (nonatomic, copy) NSString *virtualBalance;

@property (nonatomic, assign) int teacherId;
/**
 *  等级
 */
@property (nonatomic, copy) NSString *grade; // 如果是 99  那么发布时可以选置顶

@property (nonatomic, copy) NSString *grade_name;

@property (nonatomic, copy) NSString *secret;

@property (nonatomic, copy) NSString *cashBalance;

@property (nonatomic, strong) NSArray *pics;

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *areaId;

@property (nonatomic, copy) NSString *areaName;

@property (nonatomic, assign) NSInteger isCertified;

//1=表示今天已签到过  0=未签名
@property (nonatomic,assign) NSInteger isSign;

@property (nonatomic,copy) NSString *userName;

@property (nonatomic,copy) NSString *realName;
@property (nonatomic, copy) NSString *haveHonor;
@property (nonatomic, copy) NSString *integrate;
@property (nonatomic, copy) NSString *weCount;

//"我的"第一分区内容
@property (nonatomic, strong) NSArray *menusTopList;
//"我的"第二分区内容
@property (nonatomic, strong) NSArray *menusCenterList;
//第二张启动页
@property (nonatomic, strong) MyHomeInfoModel *brandInfo;



+ (void)saveUserInfoModelWithModel:(UserInfoModel *)model;
+ (UserInfoModel *)getInfoModel;
+ (void)clearModel;

//找投资-有资金；人才-能力；合作-人脉；产品-渠道；其他-其他

@end
