//
//  VerifyCodeBtn.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/1.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "VerifyCodeBtn.h"

static NSString * const kDefaultTitle = @"发送验证码";

@interface VerifyCodeBtn()

{
    int _timeTotalCount;
    int _countTime;
    NSTimer * _timer;
}

@property (nonatomic, copy) void (^clickBlock)();

@end

@implementation VerifyCodeBtn

- (void)awakeFromNib {
    
    [self setTitle:kDefaultTitle forState:UIControlStateNormal];
    [self addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    [super awakeFromNib];
}

- (void)setTime:(int)time ClickBlock:(void (^)())block {
    
    self.clickBlock = block;
    _timeTotalCount = time;

}

- (void)clickAction:(UIButton *)sender {

    if (self.clickBlock == nil) { return; }
    else {
        
        self.enabled = NO;
        self.clickBlock();
        
    }
    
}

- (void)startCountDown {
    
    _countTime = _timeTotalCount;
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCountDown) userInfo:nil repeats:true];
    
}

- (void)cancelCountDown {
    
    self.enabled = YES;
    [self setTitle:kDefaultTitle forState:UIControlStateNormal];
    
}

- (void)timerCountDown {
    
    _countTime -= 1;
    
    if (_countTime == 0) {
        
        [self setTitle:kDefaultTitle forState:UIControlStateNormal];
        [_timer invalidate];
        self.enabled = YES;
        
    } else  {
        
        [self setTitle:[NSString stringWithFormat:@"%d s",_countTime] forState:UIControlStateNormal];
        
    }


}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
