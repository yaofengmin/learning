//
//  VerifyCodeBtn.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/1.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyCodeBtn : UIButton
- (void)setTime:(int)time ClickBlock:(void (^)())block;

- (void)cancelCountDown;
- (void)startCountDown;
@end
