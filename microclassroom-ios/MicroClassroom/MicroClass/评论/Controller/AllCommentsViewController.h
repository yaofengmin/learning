//
//  AllCommentsViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/17.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

@class MicroClassCommentModel;

@interface AllCommentsViewController : YHBaseViewController
@property (nonatomic, strong) NSMutableArray<MicroClassCommentModel *> *Comments;

@property (nonatomic,copy) NSString *videoId;

@end
