//
//  AllReplyViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/17.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

@class MicroClassReplyCommentModel;
@class MicroClassCommentModel;
@interface AllReplyViewController : YHBaseViewController
@property (nonatomic, strong) NSArray<MicroClassReplyCommentModel *> *replay_commnets;

@property (nonatomic,strong) MicroClassCommentModel *commentModel;

@property (nonatomic,assign) NSString *videoId;

@property (nonatomic,assign) int pId;

@property (nonatomic,assign) NSInteger cellIndex;

@property (nonatomic,copy) NSString *nickName;
@property (nonatomic ,copy) void(^deletedMsg)(MicroClassCommentModel *ymd , NSInteger indext);

@end
