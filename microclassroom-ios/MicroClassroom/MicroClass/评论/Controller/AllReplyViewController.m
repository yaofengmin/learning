//
//  AllReplyViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/17.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "AllReplyViewController.h"

#import "MicroClassCommentTableViewCell.h"
#import "MyReplyCommentTableViewCell.h"

#import "CommentToolBar.h"

#import "CommonHeader.h"

#import "MicroClassBannerSegmentModel.h"

static NSString *replyHeadCellID = @"MicroClassCommentCellID";
static NSString *replyDetailCellID = @"ReplyCommentCellID";

static int reloadCount;

@interface AllReplyViewController ()<UITableViewDelegate,UITableViewDataSource,CommentToolbarDelegate,deleteAction>
@property (nonatomic,strong) CommentToolBar *toolBar;
@property (nonatomic ,strong) NSMutableArray *replys;
@property (nonatomic ,strong) UITableView *tableV;

@end

@implementation AllReplyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"全部回复";
    self.yh_navigationItem.leftBarButtonItem.button.hidden = YES;
    self.replys = [NSMutableArray arrayWithArray:self.replay_commnets];
    [self initalTable];
    [self initalKeyBoard];
    
}

-(void)initalTable
{
    self.tableV = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableV.backgroundColor = [UIColor whiteColor];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
    [self.view addSubview:self.tableV];
    
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    if ([self.tableV respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableV  setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    if (IOS8) {
        if ([self.tableV  respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableV  setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 15)];
        }
    }
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassCommentTableViewCell" bundle:nil] forCellReuseIdentifier:replyHeadCellID];
    [self.tableV registerNib:[UINib nibWithNibName:@"MyReplyCommentTableViewCell" bundle:nil] forCellReuseIdentifier:replyDetailCellID];
}
-(void)initalKeyBoard
{
    CGFloat chatbarHeight = [CommentToolBar defaultHeight];
    self.toolBar = [[CommentToolBar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - chatbarHeight, self.view.frame.size.width, chatbarHeight) buttonIsHidden:YES backIsHidden:NO];
    self.toolBar.delegate = self;
    self.toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    self.toolBar.inputTextView.placeHolder = [NSString stringWithFormat:@"回复%@",self.nickName];
    [self.view addSubview:self.toolBar];
}
#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    return self.replys.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MicroClassCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:replyHeadCellID forIndexPath:indexPath];
        [cell configWithCommentModel:_commentModel];
        cell.allBtn.hidden = YES;
        cell.delegate = self;
        cell.stamp = indexPath;
        return cell;
    }else{
        MyReplyCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:replyDetailCellID forIndexPath:indexPath];
        [cell configWithModel:self.replys[indexPath.row]];
        return cell;
    }
}
#pragma mark === UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 50;
    }
    return 0.001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [_commentModel commentCellHeight];
    }
    return [MyReplyCommentTableViewCell cellHeightWithReplyCommentModel:self.replys[indexPath.row]];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return;
    }
    MicroClassReplyCommentModel *model = self.replys[indexPath.row];
    
    if ([[NSString stringWithFormat:@"%d",model.userId] isEqualToString:KgetUserValueByParaName(USERID)]) {
        
        UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"确定要删除该条评论吗?" cancelButtonItem:[RIButtonItem itemWithLabel:@"取消" action:^{
            
        }] destructiveButtonItem:[RIButtonItem itemWithLabel:@"删除" action:^{
            NSDictionary *paramDic = @{Service:VideoDeleteCommentMessage,
                                       @"userId":KgetUserValueByParaName(USERID),
                                       @"commentId":@(model.comContentId)
                                       };
            
            SHOWHUD;
            [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                HIDENHUD;
                if (result) {  // 删除一条评论
                    if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                        //                        [self requestComment];
                        [self.replys removeObjectAtIndex:indexPath.row];
                        [self.tableV reloadData];
                    }
                }
            }];
        }] otherButtonItems:nil, nil];
        [sheet showInView:self.view];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        CommonHeader *header = [[NSBundle  mainBundle] loadNibNamed:@"CommonHeader" owner:self options:nil].firstObject;
        return header;
    }
    return nil;
}

#pragma mark === 键盘


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

-(void)didSendText:(NSString *)text
{
    [self sendComment:text];
}


#pragma mark === toolbar按钮点击
-(void)handleItemWithSender:(UIButton *)sender
{
    switch (sender.tag) {
        case 1://返回
        {
            [self backBtnAction];
        }
            break;
        default:
            break;
    }
    
}
#pragma mark - 发起评论
- (void)sendComment:(NSString *)text{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        [self.view endEditing:YES];
        
        NSDictionary *paramDic = @{@"service": AddComment,
                                   @"userId": KgetUserValueByParaName(USERID),
                                   @"toId": self.videoId,
                                   @"toUserId":@(self.commentModel.userId),
                                   @"ctype": @(1),
                                   @"pId":@(self.pId),
                                   @"content": text};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    [self requestList];
                    
                }
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
        }];
    }
    
}


#pragma mark === 删除某条说说
-(void)delectMsgWithIndext:(NSIndexPath *)indext
{
    [YHJHelp aletWithTitle:nil Message:@"确认删除这条信息吗" sureTitle:@"确定" CancelTitle:@"取消" SureBlock:^{
        NSDictionary *paramDic = @{Service:VideoDeleteCommentMessage,
                                   @"userId":KgetUserValueByParaName(USERID),
                                @"commentId":@(self.commentModel.comContentId)
                                   };
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    if (self.deletedMsg) {
                        self.deletedMsg(self.commentModel,self.cellIndex);
                    }
                    [self backBtnAction];
                }else {
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];
    } andCancelBlock:nil andDelegate:self];
    
    
}
#pragma mark === 评论接口,获取全部回复数据
- (void)requestList {
    NSDictionary *paramDic = @{Service:VidoeoGetComments,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"toId":self.videoId,
                               @"ctype":@"1"};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                [self.replys removeAllObjects];
                NSArray *comments = [MicroClassCommentModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                for (MicroClassCommentModel  *model in comments) {
                    if (model.comContentId == self.commentModel.comContentId) {
                        self.replys = [NSMutableArray arrayWithArray:model.replay_commnets];
                        reloadCount++;
                        [self reloadMyTableVie];
                    }
                }
            }else{
                [self.replys removeAllObjects];
                [self.tableV reloadData];
            }
        }
        
    }];
}
-(void)reloadMyTableVie{
    
    [self.tableV reloadData];
    if (reloadCount>=2) {
        [self.tableV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.replys.count-1 inSection:1] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //    [IQKeyboardManager sharedManager].enable = YES;
    //    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField =0;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //    [IQKeyboardManager sharedManager].enable = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
