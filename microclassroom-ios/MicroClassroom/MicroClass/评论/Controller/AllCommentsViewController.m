//
//  AllCommentsViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/17.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "AllCommentsViewController.h"

#import "AllReplyViewController.h"

#import "MicroClassCommentTableViewCell.h"

#import "CommentToolBar.h"
#import "CirleRequstTool.h"


#import "MicroClassBannerSegmentModel.h"

static int reloadCount;

static NSString *commentCellID = @"MicroClassCommentCellID";
@interface AllCommentsViewController ()<UITableViewDelegate,UITableViewDataSource,CommentToolbarDelegate,CirleRequstToolDelegate,deleteAction>
@property (nonatomic,strong) CommentToolBar *toolBar;
@property (nonatomic,assign) NSInteger selectedIndexPath;
@property (nonatomic , strong)   CirleRequstTool      *tool;
@property (nonatomic ,strong)UITableView *tableV;
@property (nonatomic,strong) DIYTipView *tipView;

@end

@implementation AllCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"全部评论";
    self.yh_navigationItem.leftBarButtonItem.button.hidden = YES;
    _tool = [[CirleRequstTool alloc]init];
    _tool.delegate = self;
    [self initalTable];
    [self initalKeyBoard];
    reloadCount = 0;
    [self requestList];
}
- (void)requestList {
    NSDictionary *paramDic = @{Service:VidoeoGetComments,//GetVideoComments
                               @"userId":KgetUserValueByParaName(USERID),
                               @"toId":self.videoId,
                               @"ctype":@"1"};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.Comments = [MicroClassCommentModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                if (self.Comments.count > 0) {
                    reloadCount++;
                    [self reloadMyTableVie];
                }else{
                    [self.tableV reloadData];
                }
            }
        }
        if (self.Comments.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关评论" andBtnTitle:nil andClickBlock:nil];
            }
            [self.tableV addSubview:self.tipView];
            [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(self.view);
                make.size.equalTo(CGSizeMake(120, 120));
            }];
        }else
        {
            if (self.tipView.superview) {
                
                [self.tipView removeFromSuperview];
            }
        }
        
    }];
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.backgroundColor = [UIColor whiteColor];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    MyLog(@"%f",[UIApplication sharedApplication].statusBarFrame.size.height);
    MyLog(@"%f",KTopHeight);
    MyLog(@"%f",KNavigationBarHeight);
    
    self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, [CommentToolBar defaultHeight], 0));
    }];
    if ([self.tableV respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableV  setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    if (IOS8) {
        if ([self.tableV  respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableV  setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 15)];
        }
    }
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassCommentTableViewCell" bundle:nil] forCellReuseIdentifier:commentCellID];
    
}
-(void)initalKeyBoard
{
    CGFloat chatbarHeight = [CommentToolBar defaultHeight];
    self.toolBar = [[CommentToolBar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - chatbarHeight, self.view.frame.size.width, chatbarHeight) buttonIsHidden:YES backIsHidden:NO];
    self.toolBar.delegate = self;
    self.toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:self.toolBar];
}

#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.Comments.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MicroClassCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:commentCellID forIndexPath:indexPath];
    cell.stamp = indexPath;
    cell.delegate = self;
    [cell configWithCommentModel:self.Comments[indexPath.row]];
    cell.allBtn.hidden = YES;
    
    return cell;
}
#pragma mark === UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MicroClassCommentModel *model = self.Comments[indexPath.row];
    return [model commentCellHeight];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MicroClassCommentModel *model = self.Comments[indexPath.row];
    AllReplyViewController *replyVC = [[AllReplyViewController alloc]init];
    replyVC.commentModel = self.Comments[indexPath.row];
    replyVC.replay_commnets = self.Comments[indexPath.row].replay_commnets;
    replyVC.videoId = self.videoId;
    replyVC.pId = model.comContentId;
    replyVC.nickName = model.nickName;
    replyVC.cellIndex = indexPath.row;
    @weakify(self);
    
    replyVC.deletedMsg = ^(MicroClassCommentModel *ymd , NSInteger indext) {
        @strongify(self);
        [self.Comments removeObject:ymd];
        [self.tableV deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indext inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationLeft];
    };
    
    [self.navigationController pushViewController:replyVC animated:YES];
}


#pragma mark === 键盘


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

-(void)didSendText:(NSString *)text
{
    [self sendComment:text];
}

#pragma mark === toolbar按钮点击
-(void)handleItemWithSender:(UIButton *)sender
{
    switch (sender.tag) {
        case 1://返回
        {
            [self backBtnAction];
        }
            break;
        default:
            break;
    }
    
}
#pragma mark - 发起评论
- (void)sendComment:(NSString *)text{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        [self.view endEditing:YES];
        
        NSDictionary *paramDic = @{@"service": AddComment,
                                   @"userId": KgetUserValueByParaName(USERID),
                                   @"toId": self.videoId,
                                   @"toUserId":@0,
                                   @"ctype": @(1),
                                   @"pId": @(0),
                                   @"content": text};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    self.toolBar.inputTextView.text = @"";
                    [self requestList];
                }
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
        }];
    }
    
}


#pragma mark === CirleRequstToolDelegate
-(void)addThumbWithBody:(MicroClassCommentModel *)body
{
    [self reloadOneRowsWithData:body andRow:self.selectedIndexPath];
}
-(void)cancelThumbWithBody:(MicroClassCommentModel *)body
{
    [self reloadOneRowsWithData:body andRow:self.selectedIndexPath];
}
#pragma mark === 刷新一个row ===
-(void)reloadOneRowsWithData:(MicroClassCommentModel *)ymData andRow:(NSInteger) row {
    
    [self.Comments replaceObjectAtIndex:row withObject:ymData];
    [self.tableV reloadData];
}

-(void)reloadMyTableVie{
    
    [self.tableV reloadData];
    if (reloadCount>=2) {
        [self.tableV scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.Comments.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
}

#pragma mark === 删除某条说说
-(void)delectMsgWithIndext:(NSIndexPath *)indext
{
    MicroClassCommentModel *model = self.Comments[indext.row];
    [YHJHelp aletWithTitle:nil Message:@"确认删除这条信息吗" sureTitle:@"确定" CancelTitle:@"取消" SureBlock:^{
        NSDictionary *paramDic = @{Service:VideoDeleteCommentMessage,//VideoDeleteCommentMessage
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"commentId":@(model.comContentId)
                                   };
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    [self.Comments removeObject:model];
                    [self.tableV reloadData];
                }else {
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];
    } andCancelBlock:nil andDelegate:self];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField =0;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

