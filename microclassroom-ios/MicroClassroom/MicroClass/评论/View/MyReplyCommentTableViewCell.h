//
//  ReplyCommentTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/17.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WFReplyBody.h"
@class MicroClassReplyCommentModel;

@interface MyReplyCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UIView *contentBg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;

@property (nonatomic ,strong) WFReplyBody *model;

+ (CGFloat)cellHeightWithReplyCommentModel:(MicroClassReplyCommentModel *)model;
-(void)configWithModel:(MicroClassReplyCommentModel *)cellModel;
@end
