//
//  ReplyCommentTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/17.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyReplyCommentTableViewCell.h"

#import "MicroClassBannerSegmentModel.h"
#import "NSString+Size.h"

@implementation MyReplyCommentTableViewCell

- (void)awakeFromNib {
	[super awakeFromNib];
	self.headImage.backgroundColor = ImageBackColor;
	self.headImage.layer.cornerRadius = 30 / 2.0;
	self.headImage.layer.masksToBounds = YES;
    
    self.contentBg.layer.cornerRadius = 4.0;
    self.contentBg.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

-(void)setModel:(WFReplyBody *)model
{
	[self.headImage sd_setImageWithURL:[NSURL URLWithString:model.userPhoto] placeholderImage:[UIImage imageNamed:@"moren_head"]];
	self.nameLabel.text = model.userName;
	self.timeLabel.text = model.postTime;
	self.content.text = model.content;
}

-(void)configWithModel:(MicroClassReplyCommentModel *)cellModel {
	[self setAvatar:cellModel.photo];
	self.nameLabel.text = cellModel.nickName;
	self.content.text = [NSString stringWithFormat:@"回复 %@:%@",cellModel.toNickName,cellModel.comcontent];
    
    self.contentHeight.constant = [self cellHeightWithText:cellModel];
}
- (void)setAvatar:(NSString *)avatarStr {
	
	[self.headImage sd_setImageWithURL:[NSURL URLWithString:avatarStr]];
}
+ (CGFloat)cellHeightWithReplyCommentModel:(MicroClassReplyCommentModel *)model {
	
	NSString * content = [NSString stringWithFormat:@"%@ 回复 %@:%@", model.nickName, model.toNickName, model.comcontent];
    CGFloat contentH = ceil([content sizeWithFontSize:14 andRectSize:CGSizeMake(KScreenWidth - 30, 0)].height) + 20;
    
    CGFloat height = 75 + (contentH < 35?35:contentH);
	
	return height;
}

- (CGFloat)cellHeightWithText:(MicroClassReplyCommentModel *)model {
    
    NSString * content = [NSString stringWithFormat:@"%@ 回复 %@:%@", model.nickName, model.toNickName, model.comcontent];
    
    CGFloat height = ceil([content sizeWithFontSize:14 andRectSize:CGSizeMake(KScreenWidth - 30, 0)].height);
    
    return height + 20;
}
@end
