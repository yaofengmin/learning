//
//  InitConfigModel.h
//  EnergyConservationPark
//
//  Created by Hanks on 16/6/14.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "YHBaseModel.h"

/**
 我的里面需要添加的广告
 */

@interface MeAdListConfigModel : YHBaseModel
@property (nonatomic ,copy) NSString *itemType;
@property (nonatomic ,copy) NSString *name;
@property (nonatomic ,copy) NSString *outLinkOrId;
@property (nonatomic ,copy) NSString *photoHeight;
@property (nonatomic ,copy) NSString *photosWidth;
@property (nonatomic ,copy) NSString *pic;
@property (nonatomic ,copy) NSString *position_id;

@end
@interface InitConfigModel : YHBaseModel

@property (nonatomic ,strong) NSDictionary       *server_phones;
@property (nonatomic ,copy  ) NSString           *reg_url;

/**
 *  关于我们
 */
@property (nonatomic ,copy  ) NSString           *aboutme_url;
/**打赏金额列表*/
@property (nonatomic ,strong) NSArray            *reward_moenys;
//@property (nonatomic ,strong) NSArray            *orderrejected_reason;

/**
 下载地址
 */
@property (nonatomic ,copy  ) NSString           *down_ios_url;
@property (nonatomic ,copy  ) NSString           *jianghukejian_ios_url;
///**
// *  注意事项说明
// */
@property (nonatomic ,copy  ) NSString           *topup_meno;
/**
 *  最小提现金额
 */
@property (nonatomic ,copy  ) NSString           *user_withdraw_min_money;
/**
 *  充值配置 
 
 {
 "balance": 95,
 "virtual": 5
 }
 
 */
@property (nonatomic ,strong) NSArray            *topup_virtual;
@property (nonatomic ,strong) NSString           *topup_url;
@property (nonatomic ,copy  ) NSString           *open_pay;

/**
 课间详情分享url 分享的时候需要拼接动态msgId
 */
@property (nonatomic ,copy  ) NSString           *kejian_url;

@property (nonatomic ,strong) NSArray            *meAdList;//广告
/**
 分享logo
 */
@property (nonatomic ,copy  ) NSString           *logoUrl;

/**
 是否显示游客登录按钮  1--显示
 */
@property (nonatomic ,assign) BOOL               isShowTourists;

/**
 we+ 模块是否显示
 */
@property (nonatomic ,assign) BOOL     isShowCircle;

@property (nonatomic,copy) NSString *wxAppId;

@property (nonatomic,copy) NSString *wxStartPage;

@property (nonatomic,copy) NSString *userBuyTel;

@property (nonatomic,copy) NSString *wxGhId;
//签到详情web
@property (nonatomic,copy) NSString *userSign_url;

+(instancetype)shareInstance;


@end
