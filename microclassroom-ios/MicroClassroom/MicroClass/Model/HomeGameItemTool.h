//
//  GameItemTool.h
//  ZQNewHomeTopDemo
//
//  Created by Hanks on 16/8/19.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HomeGameItemModel;
static const NSString *KAlllistArrKey = @"KAlllistArrKey";
static const NSString *KTopListArrKey = @"KTopListArrKey";

@interface HomeGameItemTool : NSObject
/**
 *  保存所有数组数据   需要先保存全部数组 去触发topList的更新
 *
 *  @param list <#list description#>
 */
+ (void)saveModelAllList:(NSArray *)list;

/**
 *  不做判断直接保存顶部数组数据;
 */
+ (void)saveTopChangeList:(NSArray *)list;


/**
 *  保存顶部数组数据
 *
 *  @param list <#list description#>
 */
+ (void)saveModelTopList:(NSArray *)list;

/**
 *  获取top 和 其他所有数据
 *
 *  @return <#return value description#>
 */
+ (NSDictionary*)getItemAllList;


+ (NSArray<HomeGameItemModel *>*)getDefultDataWithAllList:(NSArray *)allList;

@end
