//
//  InitConfigModel.m
//  EnergyConservationPark
//
//  Created by Hanks on 16/6/14.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "InitConfigModel.h"
#import "RechargeModel.h"

@implementation MeAdListConfigModel

@end

@implementation InitConfigModel
+(instancetype)shareInstance
{
    static InitConfigModel *configModel;
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
        
        configModel = [[InitConfigModel alloc]init];
    });
    
    return configModel;
}

+(NSDictionary *)mj_objectClassInArray {
    return @{@"topup_virtual":[RechargeModel class],@"meAdList":[MeAdListConfigModel class]};
}


@end
