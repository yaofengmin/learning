//
//  MicroClassBannerSegmentModel.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/6.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MicroClassBannerSegmentModel.h"

@implementation MicroClassVideoModel

@end

@implementation MicroClassVideoListModel
+ (NSDictionary *)objectClassInArray{
    return @{
             @"list" : @"MicroClassVideoModel"
             };
}
@end

@implementation BannerModel

@end

@implementation MicroClassBannerSegmentModel
+ (NSDictionary *)objectClassInArray{
    return @{
             @"video_class" : @"MicroClassVideoTypeModel",
             @"display_list" : @"BannerModel"
             };
}
@end

@implementation MicroClassTeacherModel
- (BOOL)getFollowState {
    
    if (_isFollow == 0) {
        return NO;
    } else {
        return YES;
    }
    
}

- (void)resetFollowState:(BOOL)selected {
    _isFollow = selected ? 1 : 0;
}


@end

@implementation MicroClassReplyCommentModel

@end

@implementation MicroClassCommentModel

+ (NSDictionary *)objectClassInArray{
    return @{
             @"replay_commnets" : @"MicroClassReplyCommentModel"
             };
}

-(CGFloat)commentCellHeight
{
    CGFloat cellH = 10 + 30 + 5 + 16 + 1;
    if (self.replay_commnets.count > 0) {
        cellH = cellH + 30;
    }else{
        if ([[NSString stringWithFormat:@"%d",self.userId] isEqualToString:KgetUserValueByParaName(USERID)]) {
            cellH = cellH + 30;
        }
    }
    cellH = cellH + [self cacluteLabelHeight:self.comcontent];//内容
    return cellH;
}

-(CGFloat)cacluteLabelHeight:(NSString *)text
{
    if (text.length == 0) {
        return 0;
    }
    NSAttributedString *textStr = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    CGRect msgContentSize = [textStr boundingRectWithSize:CGSizeMake(KScreenWidth - 92, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    
    
    return (msgContentSize.size.height + 20) < 35?35:(msgContentSize.size.height + 20);
}
@end
@implementation ZhiBoVcloudModel

@end

@implementation MicroClassDetailModel

- (BOOL)getCollectionState {
    
    if (_isCollection == 0) {
        return NO;
    } else {
        return YES;
    }
    
}

+ (NSDictionary *)objectClassInArray{
    return @{
             @"Comments" : @"MicroClassCommentModel",
             @"linkVideos" : @"MicroClassVideoModel"
             };
}
//直播描述
-(CGFloat)LiveRaideoDesCellHeight
{
    CGFloat cellH = 206;
    cellH = cellH + [self cacluteTitleLabelHeight:self.videoTitle];//标题
    cellH = cellH + [self cacluteLabelHeight:self.videoDesc];//描述
    cellH = cellH + [self cacluteLabelHeight:self.authorDes];//作者简介
    cellH = cellH + (self.showAll?30:0);
    return cellH;
}
//音视频详情
-(CGFloat)VideoDesCellHeight
{
    CGFloat cellH = 185;
    cellH = cellH + [self cacluteTitleLabelHeight:self.videoTitle];//标题
    cellH = cellH + [self cacluteLabelHeight:self.videoDesc];//描述
    cellH = cellH + [self cacluteLabelHeight:self.authorDes];//作者简介
    cellH = cellH + (self.showAll?30:0);
    return cellH;
}

-(CGFloat)cacluteTitleLabelHeight:(NSString *)text
{
    if (text.length == 0) {
        return 0;
    }
    NSAttributedString *textStr = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    CGRect msgContentSize = [textStr boundingRectWithSize:CGSizeMake(KScreenWidth - 30, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    
    
    return msgContentSize.size.height + 5;
}
-(CGFloat)cacluteLabelHeight:(NSString *)text
{
    if (text.length == 0) {
        return 0;
    }
    NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:9.0];
    NSAttributedString *textStr = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSParagraphStyleAttributeName:paragraphStyle}];
    CGRect msgContentSize = [textStr boundingRectWithSize:CGSizeMake(KScreenWidth - 30, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    
    
    return msgContentSize.size.height + 5;
}
@end


@implementation MicroClassBuyModel

@end
@implementation VideoMenuesModel

@end



@implementation MicroClassBooksDetailModel

+ (NSDictionary *)objectClassInArray{
    return @{
             @"Comments"   : @"MicroClassCommentModel",
             @"linkVideos" : @"MicroClassVideoModel",
             @"videoMenus" : @"VideoMenuesModel"
             };
}


-(CGFloat)DesHeight
{
    CGFloat cellH = 164;
    cellH = cellH + [self cacluteHeight:self.videoDesc withWidth:KScreenWidth - 58];
    cellH = cellH + [self cacluteHeight:self.authorDes withWidth:KScreenWidth - 58];
    cellH = cellH + (self.showAll?30:0);
    return cellH;
}

-(CGFloat)cacluteHeight:(NSString *)text withWidth:(CGFloat )width
{
    if (text.length == 0) {
        return 0;
    }
    NSAttributedString *textStr = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    CGRect msgContentSize = [textStr boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    
    return msgContentSize.size.height;
}

@end


