//
//  MicroClassBannerSegmentModel.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/6.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"
#import "HomeGameItemModel.h"
@interface MicroClassVideoModel : YHBaseModel

@property (nonatomic, copy) NSString *videoId;
@property (nonatomic, assign) int isVip;

/**
 0 -- video
 1 -- audio
 2 -- book
 3 -- web
 */
@property (nonatomic, assign) NSInteger isMp3;

@property (nonatomic, copy) NSString *videoTitle;
@property (nonatomic, copy) NSString *videoLength;
@property (nonatomic, copy) NSString *videoLaud;
@property (nonatomic, copy) NSString *videoLogo;
@property (nonatomic, copy) NSString *videoUrl;
@property (nonatomic, copy) NSString *teacherName;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *videoKeyword;
@property (nonatomic,copy) NSString *webUrl;//微课跳转web

@end

@interface BannerModel : YHBaseModel

/**
 1 业务   2 .url
 */
@property (nonatomic, copy) NSString *itemType;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *position_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *photosWidth;
@property (nonatomic, copy) NSString *photoHeight;
@property (nonatomic, copy) NSString *outLinkOrId;
@property (nonatomic, copy) NSString *site_detail_url;
@property (nonatomic ,assign) NSInteger isMp3;
@end

@interface MicroClassVideoListModel : YHBaseModel

@property (nonatomic, assign) int total;
@property (nonatomic, copy) NSArray<MicroClassVideoModel *> *list;

+ (NSDictionary *)objectClassInArray;
@end

@interface MicroClassBannerSegmentModel : YHBaseModel

@property (nonatomic, copy) NSArray<HomeGameItemModel *> *video_class;
@property (nonatomic, copy) NSArray<BannerModel *> *display_list;

+ (NSDictionary *)objectClassInArray;
@end

@interface MicroClassTeacherModel : YHBaseModel
@property (nonatomic, copy) NSString *teacherId;
@property (nonatomic, copy) NSString *teacherName;
@property (nonatomic, copy) NSString *author;

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *pic;

@property (nonatomic, assign) int userId;
@property (nonatomic, copy) NSString *position;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, assign) int isFollow;

- (BOOL)getFollowState;
- (void)resetFollowState:(BOOL)selected;
@end

@interface MicroClassReplyCommentModel : YHBaseModel
@property (nonatomic, assign) int comContentId;
@property (nonatomic, assign) int pId;
@property (nonatomic, copy) NSString *comcontent;
@property (nonatomic, copy) NSString *comTime;
@property (nonatomic, assign) int userId;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, assign) int toUserId;
@property (nonatomic, copy) NSString *toNickName;
@property (nonatomic, copy) NSString *toPhoto;
@end

@interface MicroClassCommentModel : YHBaseModel
@property (nonatomic, assign) int comContentId;
@property (nonatomic, assign) int pId;
@property (nonatomic, copy) NSString *comcontent;
@property (nonatomic, copy) NSString *comTime;
@property (nonatomic, assign) int userId;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSArray<MicroClassReplyCommentModel *> *replay_commnets;
+ (NSDictionary *)objectClassInArray;
-(CGFloat)commentCellHeight;

@end


@interface MicroClassBuyModel : YHBaseModel

@property (nonatomic, copy) NSString *orderId;
// 0 非预订 1 预订
@property (nonatomic, copy) NSString *orderStatus;
@property (nonatomic, copy) NSString *unitPrice;

@end
@interface ZhiBoVcloudModel : YHBaseModel
@property (nonatomic, copy) NSString *httpPullUrl;
@property (nonatomic, copy) NSString *hlsPullUrl;
@property (nonatomic, copy) NSString *rtmpPullUrl;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *pushUrl;
@property (nonatomic, copy) NSString *cid;

//chatRoom
@property (nonatomic, copy) NSString *groupId;
@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, copy) NSString *photo;

@end


@interface MicroClassDetailModel : YHBaseModel

@property (nonatomic, copy) NSString *videoId;
@property (nonatomic, copy) NSString *teacherId;
@property (nonatomic, copy) NSString *videoTitle;
@property (nonatomic, copy) NSString *videoLength;
@property (nonatomic, copy) NSString *videoLaud;
@property (nonatomic, copy) NSString *videoLogo;
@property (nonatomic, copy) NSString *videoUrl;
@property (nonatomic, copy) NSString *videoKeyword;
@property (nonatomic, copy) NSString *videoDesc;
@property (nonatomic, copy) NSString *addTime;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *endTime;
@property (nonatomic, assign) NSInteger isMp3;
@property (nonatomic,assign) BOOL showAll;//是否显示阅读全文
@property (nonatomic,copy) NSString *authorLogo;//头像
@property (nonatomic,copy) NSString *autorDes;//简介
@property (nonatomic,copy) NSString *author;//直播者
@property (nonatomic,copy) NSString *viedoLength;//预计时长
@property (nonatomic,assign) NSInteger status;//状态


@property (nonatomic,copy) NSString *authorDes;//简介

@property (nonatomic,copy) NSString *detailUrl;//查看全部详情url


/**
 标记是否有权限播放  0 可以放  1 不能放
 */
@property (nonatomic, copy) NSString *isVip;

/**
1=老师主页 2=用户会员主页
*/
@property (nonatomic,assign) NSInteger linkCtype;

/**
 金额
 */
@property (nonatomic, copy) NSString *money;
@property (nonatomic, assign) int isCollection;
@property (nonatomic, strong) MicroClassTeacherModel *teacherInfo;
@property (nonatomic, strong) NSMutableArray<MicroClassCommentModel *> *Comments;
@property (nonatomic, strong) NSMutableArray<MicroClassVideoModel *> *linkVideos;
@property (nonatomic, strong) MicroClassBuyModel *buyinfo;

-(CGFloat)VideoDesCellHeight;

//直播部分
@property (nonatomic, copy) NSString *pushUserId;
@property (nonatomic, copy) NSString *limitNum;
@property (nonatomic, strong) ZhiBoVcloudModel *vcloud;
@property (nonatomic, assign) NSInteger isJoin;
@property (nonatomic, copy) NSString *canPush;
@property (nonatomic, copy) NSString *enrollCount;
@property (nonatomic, strong) ZhiBoVcloudModel *chatRoom;

- (BOOL)getCollectionState;

-(CGFloat)LiveRaideoDesCellHeight;
@end

//目录
@interface VideoMenuesModel : YHBaseModel
@property (nonatomic,copy) NSString *menusId;
@property (nonatomic,copy) NSString *titel;
@property (nonatomic,copy) NSString *contentDetailUrl;


@end


//图书详情
@interface MicroClassBooksDetailModel : YHBaseModel
@property (nonatomic,copy) NSString *videoId;
@property (nonatomic,copy) NSString *author;
@property (nonatomic,copy) NSString *authorDes;
@property (nonatomic,copy) NSString *authorLogo;
@property (nonatomic,copy) NSString *videoTitle;
@property (nonatomic,copy) NSString *videoLength;
@property (nonatomic,copy) NSString *videoLaud;
@property (nonatomic,copy) NSString *videoLogo;
@property (nonatomic,copy) NSString *videoUrl;
@property (nonatomic,copy) NSString *videoDesc;
@property (nonatomic,copy) NSString *addTime;
@property (nonatomic,copy) NSString *isMp3;
@property (nonatomic,assign) BOOL isCollection;
@property (nonatomic,copy) NSString *publishHouse;//出版社
@property (nonatomic,copy) NSString *publishDate;//出版日期
@property (nonatomic, strong) NSMutableArray<MicroClassCommentModel *> *Comments;
@property (nonatomic, strong) NSMutableArray<MicroClassVideoModel *> *linkVideos;
@property (nonatomic, strong) NSMutableArray<VideoMenuesModel *> *videoMenus;
@property (nonatomic,assign) BOOL isReed;//是否在读
@property (nonatomic,assign) BOOL showAll;//是否显示阅读全文

@property (nonatomic,copy) NSString *detailUrl;//查看全部详情url

-(CGFloat)DesHeight;


@end


