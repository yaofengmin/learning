//
//  GameItemTool.m
//  ZQNewHomeTopDemo
//
//  Created by Hanks on 16/8/19.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "HomeGameItemTool.h"
#import "HomeGameItemModel.h"



#define HOME_ITEM_FILETOPLOC  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"HomeItemModelTopList.data"]

#define HOME_ITEM_FILEALLLOC  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"HomeItemModelAllList.data"]

static NSArray *listArr;
static NSArray *topListArr;
@implementation HomeGameItemTool


+ (void)saveModelTopList:(NSArray *)list {

   [NSKeyedArchiver archiveRootObject:list toFile:HOME_ITEM_FILETOPLOC];

    [self getTopList];
}


#pragma - private

+ (void)saveTopChangeList:(NSArray *)list {
    [self _saveModelTopList:list];
}

+ (void)_saveModelTopList:(NSArray *)list {
    [NSKeyedArchiver archiveRootObject:list toFile:HOME_ITEM_FILETOPLOC];
}


+ (void)saveModelAllList:(NSArray *)list{
    
    [NSKeyedArchiver archiveRootObject:list toFile:HOME_ITEM_FILEALLLOC];
    
    listArr =  [NSKeyedUnarchiver unarchiveObjectWithFile:HOME_ITEM_FILEALLLOC];
    if (listArr == nil) {
        
        listArr = @[];
    }
}

+(NSDictionary*)getItemAllList{
    
    return @{
             KAlllistArrKey:(listArr == nil ? @[] : listArr) ,
             KTopListArrKey:(topListArr == nil ? @[] :topListArr)
            };
}


+ (void)getTopList {
    
    NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithFile:HOME_ITEM_FILETOPLOC];
    if (arr == nil) {
        topListArr = @[];
        
    }else{
        topListArr = arr;
    }
}

+ (void)initialize{
    [self getTopList];
    listArr = [NSKeyedUnarchiver unarchiveObjectWithFile:HOME_ITEM_FILEALLLOC];
}


+(NSArray<HomeGameItemModel *>*)getDefultDataWithAllList:(NSArray *)allList {
    
    NSMutableArray *dataArr = [NSMutableArray array];
    for (HomeGameItemModel *model in allList) {
        if ([model.cateId integerValue] < 0) {
            [dataArr addObject:model];
        }
    }
    return dataArr;
}

+(HomeGameItemModel *)getModelWithName:(NSString *)name andItemId:(NSString *)Id  {
    
    HomeGameItemModel *model   = [HomeGameItemModel new];
    model.cateName = name;
    model.cateId   = Id;
    return model;
}




@end
