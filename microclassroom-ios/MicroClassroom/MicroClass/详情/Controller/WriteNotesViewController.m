//
//  WriteNotesViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "WriteNotesViewController.h"

#import "MyNotesViewController.h"

#import "WriteNotesTableViewCell.h"

static NSString *notesCellID = @"WriteNotesCellID";
@interface WriteNotesViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,copy) NSString *titles;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,strong) MyNotesListModel *model;
@property (nonatomic,assign) BOOL isWrite;//是否是直接写(否---代表我的笔记需要带数据 是直接写)

@property (nonatomic ,strong) UITableView *tableV;
@end

@implementation WriteNotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"写笔记";
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithTitle:@"保存" style:YHBarButtonItemStylePlain handler:^(id send) {
        @strongify(self);
        [self saveData];
    }];
    if (!self.isWrite) {
        self.titles = _model.titel;
        self.content = _model.content;
    }
    [self initalTable];
}
-(instancetype)initWithModel:(MyNotesListModel *)model isWrite:(BOOL)isWirte
{
    if (self = [super init]) {
        _model = model;
        self.isWrite = isWirte;
        
    }
    return self;
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.estimatedRowHeight = 300;
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.tableFooterView = [[UIView alloc]init];
    self.tableV.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"WriteNotesTableViewCell" bundle:nil] forCellReuseIdentifier:notesCellID];
}
#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WriteNotesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:notesCellID forIndexPath:indexPath];
    if (!_isWrite) {
        cell.titleTipLabel.hidden = YES;
        cell.contentTip.hidden = YES;
        cell.titleTextView.text = _model.titel;
        cell.contentTextView.text = _model.content;
    }
    @weakify(self);
    cell.titleBlock = ^(NSString *text) {
        @strongify(self);
        self.titles = text;
    };
    cell.contentBlock = ^(NSString *text) {
        @strongify(self);
        self.content = text;
    };
    return cell;
}
#pragma mark === UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 400;
}

#pragma  mark === 保存
-(void)saveData
{
    if ([self chectPram]) {
        if (self.isWrite) {
            NSDictionary *paramDic = @{@"service": VideosAddNotes,
                                       @"userId":KgetUserValueByParaName(USERID),
                                       @"videoId":self.videoId,
                                       @"titel":self.titles,
                                       @"content":self.content};
            
            [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
            
        }else{
            NSDictionary *paramDic = @{@"service": UpdateNotes,
                                       @"userId":KgetUserValueByParaName(USERID),
                                       @"noteId":self.model.noteId,
                                       @"titel":self.titles,
                                       @"content":self.content};
            
            [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    for (UIViewController *vc in self.navigationController.viewControllers) {
                        if ([vc isKindOfClass:[MyNotesViewController class]]) {
                            
                            [self.navigationController popToViewController:vc animated:YES];
                        }
                    }
                }
            }];
        }
    }
}

-(BOOL)chectPram
{
    if (self.titles.length == 0) {
        [WFHudView showMsg:@"请输入标题" inView:self.view];
        return NO;
    }else if (self.content.length == 0){
        [WFHudView showMsg:@"请输入内容" inView:self.view];
        return NO;
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
