//
//  MicroClassDetailViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

@interface MicroClassDetailViewController : YHBaseViewController
@property (nonatomic, copy) NSString *videoId;

@end
