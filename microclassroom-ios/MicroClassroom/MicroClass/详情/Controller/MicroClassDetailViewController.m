//
//  MicroClassDetailViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MicroClassDetailViewController.h"
//播放器
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HTPlayer.h"//视频
#import "HTAudioPlayer.h"//音频

#import "MicroClassAudioHeader.h"

//Cell
#import "MicroClassDetailFirstCell.h"
#import "MicroClassDetailSecondCell.h"
//#import "MicroClassThirdCell.h"
//#import "MicroClassThirdAudioCell.h"
#import "CommentTableViewCell.h"
#import "MicroClassListCell.h"
#import "MicroClassCommentTableViewCell.h"

#import "WriteNotesViewController.h"
#import "AllReplyViewController.h"
#import "AllCommentsViewController.h"
#import "BooksDetailViewController.h"
#import "YHBaseWebViewController.h"

#import "CommentToolBar.h"
#import "CommonHeader.h"
#import "WFMessageBody.h"
#import "DBDataModel.h"
#import "CirleRequstTool.h"
#import "VideoLastTimeView.h"

#import "MicroClassBannerSegmentModel.h"

#import "TeacherInfoViewController.h"
#import "PersonHomeViewController.h"

#import "MicroClassBookListTableViewCell.h"

static CGFloat const kMicroClassListCellHeight = 120.0;

static NSString *MicroDetailFirstCellID = @"MicroClassFirstCellID";
static NSString *MicroDetailSecondtCellID = @"MicroClassSecondCellID";
static NSString *MicroDetailCommentCellID = @"CommentTableCellID";
static NSString *KMicroDetailBookListCellID = @"MicroClassBookListCellID";

static NSString * const kMicroClassListCellID = @"MicroClassListCell";
static NSString * const kMicroClassCommentCellID = @"MicroClassCommentCellID";
@interface MicroClassDetailViewController ()<UITableViewDelegate,UITableViewDataSource,CommentToolbarDelegate,deleteAction,CirleRequstToolDelegate,teacherHeadTap,VideoUpdateTime>

{
    CGFloat _yOffset;
    //后台播放任务Id
    UIBackgroundTaskIdentifier _bgTaskId;
}

@property (nonatomic,strong) CommentToolBar *toolBar;
@property (nonatomic,strong) UIView *videoPlayView;
@property (nonatomic,strong) UIView *audioHeader;
//音频
@property (nonatomic,strong) HTAudioPlayer *audioPlayer;
@property (nonatomic ,strong)MicroClassAudioHeader *audioPlayHeader;
//视频
@property (strong, nonatomic)HTPlayer *htPlayer;
@property (nonatomic,strong) UIButton *writeBtn;

@property (strong, nonatomic)  MicroClassDetailModel *mainModel;

//点赞
@property (nonatomic,assign) NSInteger selectedIndexPath;
@property (nonatomic , strong)   CirleRequstTool      *tool;

@property (nonatomic ,strong) UITableView *tableV;

@property (nonatomic ,assign) BOOL isFullScreen;//是否全屏.防止连续进入两次视频详情全屏出现问题
@end

@implementation MicroClassDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationBar.hidden = YES;
#warning 如果支持后台播放,需要注开
    _bgTaskId=[YHJHelp backgroundPlayerID:_bgTaskId];
    [self pullData];
    _tool = [[CirleRequstTool alloc]init];
    _tool.delegate = self;
    KAddobserverNotifiCation(@selector(fullScreenBtnClick:), kHTPlayerFullScreenBtnNotificationKey);//全屏
    KAddobserverNotifiCation(@selector(PlayFinished:), kAudioHTPlayerFinishedPlayNotificationKey);//音频播放完成
    KAddobserverNotifiCation(@selector(PlayFinished:), kHTPlayerFinishedPlayNotificationKey);//视频播放完成
    KAddobserverNotifiCation(@selector(playPause:), kAudioHTPlayerPauseVideoNotificationKey);//音频暂停
    KAddobserverNotifiCation(@selector(startPlay:), kAudioHTPlayerStartPlayNotificationKey);//音频开始
    KAddobserverNotifiCation(@selector(appBecomeActive:), UIApplicationDidBecomeActiveNotification);
    KAddobserverNotifiCation(@selector(appWillResignActive:), UIApplicationWillResignActiveNotification);
    //    处理中断事件的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleInterreption:) name:AVAudioSessionInterruptionNotification object:[AVAudioSession sharedInstance]];
}

-(void)appBecomeActive:(NSNotification *)nofi
{
    
}

-(void)appWillResignActive:(NSNotification *)nofi
{
    //    //音频
    //    if (_audioPlayer) {
    //        // [_audioPlayer stopPlay];
    //    }
    
    //视频
    if (_htPlayer) {
        [_htPlayer stopPlay];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
}


- (void)pullData {
    NSMutableDictionary *paramDic = @{@"service": GetVideoInfo,
                                      @"videoId": self.videoId
                                      }.mutableCopy;
    
    if (KgetUserValueByParaName(USERID)) {
        [paramDic setObject:KgetUserValueByParaName(USERID) forKey:@"user_Id"];
    }
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                _mainModel = [MicroClassDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                
                if (self.tableV.superview == nil) {
                    [self initalTable];
                }else{
                    [self.tableV reloadData];
                }
                if (_mainModel.isMp3 == MicClassMp3TypeAudio) {
                    [self creatAudioHeader];
                }else{
                    
                    [self creatVideoHeader];
                }
                
                self.toolBar.collectionBtn.selected = _mainModel.isCollection;
                self.toolBar.commentBadgeNum.text = [NSString stringWithFormat:@"%ld",(unsigned long)_mainModel.Comments.count];
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableV.backgroundColor = [UIColor whiteColor];
    self.tableV.estimatedRowHeight = 120;
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.separatorColor = KColorFromRGB(0xededed);
    [self createTableHeadView:self.tableV];
    self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 100)];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        
    }];
    
    if (@available(iOS 11.0, *)) {
        self.tableV.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassDetailFirstCell" bundle:nil] forCellReuseIdentifier:MicroDetailFirstCellID];
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassDetailSecondCell" bundle:nil] forCellReuseIdentifier:MicroDetailSecondtCellID];
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassCommentTableViewCell" bundle:nil] forCellReuseIdentifier:kMicroClassCommentCellID];
    [self.tableV registerClass:[CommentTableViewCell class] forCellReuseIdentifier:@"CommentYableViewCell"];
    [self.tableV registerNib:[UINib nibWithNibName:kMicroClassListCellID bundle:nil] forCellReuseIdentifier:kMicroClassListCellID];
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassBookListTableViewCell" bundle:nil] forCellReuseIdentifier:KMicroDetailBookListCellID];
    
    if (!self.toolBar.superview) {
        [self initalKeyBoard];
    }
    
    [self creatWriteBtn];
    
}
-(void)createTableHeadView:(UITableView *)tableView{
    CGFloat height = KScreenWidth * 170 / 375 + 50;
    
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, height)];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.tableHeaderView = tableHeaderView;
    tableView.backgroundColor = BColor;
}

#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return _mainModel.linkVideos.count;
    }else if (section == 2){
        return _mainModel.Comments.count > 5?5:_mainModel.Comments.count;
    }
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
            MicroClassDetailFirstCell *cell = [tableView dequeueReusableCellWithIdentifier:MicroDetailFirstCellID forIndexPath:indexPath];
            cell.model = _mainModel;
            cell.delegate = self;
            return cell;
        }
            break;
        case 1:
        {
            MicroClassVideoModel *model = _mainModel.linkVideos[indexPath.row];
            if (model.isMp3 == MicClassMp3TypeBook) {
                MicroClassBookListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KMicroDetailBookListCellID forIndexPath:indexPath];
                [cell configDetailWithModel:model];
                return cell;
            }else{
                MicroClassListCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassListCellID forIndexPath:indexPath];
                [cell configWithModel:_mainModel.linkVideos[indexPath.row]];
                return cell;
            }
        }
            break;
        case 2:
        {
            MicroClassCommentTableViewCell *cell  =[tableView dequeueReusableCellWithIdentifier:kMicroClassCommentCellID forIndexPath:indexPath];
            cell.delegate = self;
            cell.stamp = indexPath;
            [cell configWithCommentModel:_mainModel.Comments[indexPath.row]];
            return cell;
        }
            break;
        default:
            break;
    }
    return nil;
}


#pragma mark === UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [_mainModel VideoDesCellHeight];
    }
    else if (indexPath.section == 1){
        MicroClassVideoModel *model = _mainModel.linkVideos[indexPath.row];
        if (model.isMp3 == MicClassMp3TypeWeb) {
            return 110.0;
        }
        return kMicroClassListCellHeight;
    }
    else if (indexPath.section == 2){
        MicroClassCommentModel *model = _mainModel.Comments[indexPath.row];
        return [model commentCellHeight];
    }else{
        return 100.0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        if (self.mainModel.linkVideos.count) {
            return 60;
        }else{
            return 0.01;
        }
    }else if (section == 2){
        if (self.mainModel.Comments.count) {
            return 60;
        }else{
            return 0.01;
        }
    }
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        if (self.mainModel.Comments.count > 5) {
            return 60;
        }else{
            return 0.01;
        }
    }
    return 0.01;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        if (self.mainModel.linkVideos.count) {
            CommonHeader *header = [[NSBundle  mainBundle] loadNibNamed:@"CommonHeader" owner:self options:nil].firstObject;
            header.titleLabel.text = @"学习学习再学习";
            return header;
        }
    }else if (section == 2){
        if (self.mainModel.Comments.count) {
            CommonHeader *header = [[NSBundle  mainBundle] loadNibNamed:@"CommonHeader" owner:self options:nil].firstObject;
            header.titleLabel.text = @"评论";
            return header;
        }
    }
    return nil;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 2) {
        if (self.mainModel.Comments.count > 5) {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 60)];
            view.backgroundColor = [UIColor whiteColor];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTitle:@"查看全部评论" forState:UIControlStateNormal];
            [btn setTitleColor:NEColor forState:UIControlStateNormal];
            btn.titleLabel.font = Font(14);
            [btn addTarget:self action:@selector(allCommentAction:) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(view);
            }];
            CGRect sectionRect = [tableView rectForSection:section];
            CGRect newFrame = CGRectMake(CGRectGetMinX(view.frame), CGRectGetMinY(sectionRect), CGRectGetWidth(view.frame), CGRectGetHeight(view.frame));
            view.frame = newFrame;
            return view;
        }
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        MicroClassVideoModel *model = self.mainModel.linkVideos[indexPath.row];
        if (model.isMp3 == MicClassMp3TypeAudio || model.isMp3 == MicClassMp3TypeVideo) {
            if (_audioPlayer) {
                [_audioPlayer stopPlay];
            }
            MicroClassDetailViewController *detailVC = [[MicroClassDetailViewController alloc]init];
            detailVC.videoId = model.videoId;
            [self.navigationController pushViewController:detailVC animated:YES];
        }else if (model.isMp3 == MicClassMp3TypeBook){
            BooksDetailViewController *booksDetailVC = [[BooksDetailViewController alloc]init];
            booksDetailVC.videoId = model.videoId;
            [self.navigationController pushViewController:booksDetailVC animated:YES];
        }else{
            YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.webUrl andNavTitle:model.videoTitle];
            [self.navigationController pushViewController:web animated:YES];
        }
    }if (indexPath.section == 2) {
        MicroClassCommentModel *model = _mainModel.Comments[indexPath.row];
        AllReplyViewController *replyVC = [[AllReplyViewController alloc]init];
        replyVC.commentModel = model;
        replyVC.replay_commnets = _mainModel.Comments[indexPath.row].replay_commnets;
        replyVC.videoId = _mainModel.videoId;
        replyVC.pId = model.comContentId;
        replyVC.nickName = model.nickName;
        replyVC.cellIndex = indexPath.row;
        @weakify(self);
        replyVC.deletedMsg = ^(MicroClassCommentModel *ymd , NSInteger indext) {
            @strongify(self);
            [self.mainModel.Comments removeObjectAtIndex:indext];
            self.toolBar.commentBadgeNum.text = [NSString stringWithFormat:@"%ld",(unsigned long)_mainModel.Comments.count];
            [self.tableV reloadData];
        };
        [self.navigationController pushViewController:replyVC animated:YES];
    }
}


#pragma mark === 写按钮
-(void)creatWriteBtn
{
    _writeBtn  =[UIButton buttonWithType:UIButtonTypeCustom];
    [_writeBtn setImage:[UIImage imageNamed:@"notes"] forState:UIControlStateNormal];
    [_writeBtn addTarget:self action:@selector(handleWriteNotesAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_writeBtn];
    [_writeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.bottom.equalTo(-100);
        make.size.equalTo(CGSizeMake(71, 71));
    }];
}


#pragma mark === 写
-(void)handleWriteNotesAction:(UIButton *)sender
{
    WriteNotesViewController *writeNotesVC = [[WriteNotesViewController alloc]initWithModel:nil isWrite:YES];
    writeNotesVC.videoId = self.mainModel.videoId;
    [self.navigationController pushViewController:writeNotesVC animated:YES];
}

#pragma mark === 查看全部评论
-(void)allCommentAction:(UIButton *)sender
{
    AllCommentsViewController *commentVC = [[AllCommentsViewController alloc]init];
    commentVC.videoId = _mainModel.videoId;
    [self.navigationController pushViewController:commentVC animated:YES];
}

#pragma mark === 点击查看全部回复
-(void)seeAllCommentWithIndex:(NSIndexPath *)indexPath
{
    MicroClassCommentModel *model = _mainModel.Comments[indexPath.row];
    AllReplyViewController *replyVC = [[AllReplyViewController alloc]init];
    replyVC.commentModel = model;
    replyVC.replay_commnets = _mainModel.Comments[indexPath.row].replay_commnets;
    replyVC.videoId = _mainModel.videoId;
    replyVC.pId = model.comContentId;
    replyVC.nickName = model.nickName;
    replyVC.cellIndex = indexPath.row;
    @weakify(self);
    replyVC.deletedMsg = ^(MicroClassCommentModel *ymd , NSInteger indext) {
        @strongify(self);
        [self.mainModel.Comments removeObjectAtIndex:indext];
        self.toolBar.commentBadgeNum.text = [NSString stringWithFormat:@"%ld",(unsigned long)_mainModel.Comments.count];
        [self.tableV reloadData];
    };
    [self.navigationController pushViewController:replyVC animated:YES];
}
#pragma mark ====================================  评论 ======================================================
-(void)initalKeyBoard
{
    CGFloat chatbarHeight = [CommentToolBar defaultHeight];
    self.toolBar = [[CommentToolBar alloc] initWithFrame:CGRectMake(0, KScreenHeight - chatbarHeight, self.view.frame.size.width, chatbarHeight) buttonIsHidden:NO backIsHidden:NO];
    self.toolBar.delegate = self;
    self.toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:self.toolBar];
    
}

#pragma mark === toolbar按钮点击
-(void)handleItemWithSender:(UIButton *)sender
{
    switch (sender.tag) {
        case 1://返回
        {
            [self backBtnAction];
            [self.toolBar removeFromSuperview];
        }
            break;
        case 2://评论
        {
            AllCommentsViewController *commentVC = [[AllCommentsViewController alloc]init];
            commentVC.videoId = _mainModel.videoId;
            [self.navigationController pushViewController:commentVC animated:YES];
        }
            break;
        case 3://收藏
        {
            WFMessageBody *body = [[WFMessageBody alloc]init];
            body.msgId = _mainModel.videoId;
            if (sender.selected) {
                [self.tool collectionWith:body type:@"1" stamp:nil vc:self];
            }else{
                [self.tool cancelCollectionWith:body type:@"1" stamp:nil vc:self];
            }
        }
            break;
        case 4://分享
        {
            [[YHJHelp shareInstance]  showShareInController:self andShareURL:self.mainModel.videoUrl andTitle:self.mainModel.videoTitle andShareText:@"" andShareImage:nil isShowStudyGroup:NO isDIYUrl:NO];
        }
            break;
        default:
            break;
    }
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
    CGFloat offsetY = scrollView.contentOffset.y;
    //    NSLog(@"************* %f",offsetY);
    
    if (_audioHeader != nil) {
        CGFloat height = KScreenWidth * 170 / 375;
        if (scrollView.contentOffset.y > height) {
            _audioHeader.center = CGPointMake(_audioHeader.center.x, _yOffset - height);
            return;
        }
        CGFloat h = _yOffset - offsetY;
        _audioHeader.center = CGPointMake(_audioHeader.center.x, h);
    }
}

#pragma mark === 发送评论
-(void)didSendText:(NSString *)text
{
    
    [self sendComment:text];
}


#pragma mark ======================== 音视频 ===========================================================================
#pragma mark === 视频
-(void)creatVideoHeader
{
    if (self.videoPlayView == nil) {
        self.videoPlayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 211)];
        self.htPlayer.alpha = 1;
        self.tableV.tableHeaderView = self.videoPlayView;
    }
    
    
}
#pragma mark === player

- (HTPlayer *)htPlayer {
    if (!_htPlayer) {
        [[DBDataModel sharedDBDataModel] createDataTable_UserVideoProgress];
        CGRect rect = CGRectMake(0, 0, KScreenWidth,211);
        NSString *videoUrl = _mainModel.videoUrl;
        _htPlayer = [[HTPlayer alloc]initWithFrame:rect videoURLStr:videoUrl];
        _htPlayer.canPlay = self.mainModel.isVip.integerValue;
        [_videoPlayView addSubview:_htPlayer];
        [self initalVideoLastTimeView];
    }
    return _htPlayer;
}


#pragma mark === 音频
-(void)creatAudioHeader
{
    if (self.audioHeader == nil) {
        CGFloat height = KScreenWidth * 170 / 375;
        self.audioHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, height + 50)];
        _yOffset = self.audioHeader.viewCenterY;
        _audioPlayHeader = [[NSBundle mainBundle] loadNibNamed:@"MicroClassAudioHeader" owner:self options:nil].firstObject;
        [_audioPlayHeader.newsImage sd_setImageWithURL:[NSURL URLWithString:_mainModel.videoLogo]];
        [_audioPlayHeader.imageBg sd_setImageWithURL:[NSURL URLWithString:_mainModel.videoLogo]];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        // 毛玻璃view 视图
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //添加到要有毛玻璃特效的控件中
        [_audioPlayHeader.imageBg addSubview:effectView];
        [effectView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        //设置模糊透明度
        effectView.alpha = .9f;
        [self.audioHeader addSubview:_audioPlayHeader];
        [_audioPlayHeader mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 50, 0));
        }];
        [self.view addSubview:self.audioHeader];
    }
    self.audioPlayer.alpha = 1;
    
    
}
-(HTAudioPlayer *)audioPlayer
{
    if (!_audioPlayer) {
        [[DBDataModel sharedDBDataModel] createDataTable_UserVideoProgress];
        _audioPlayer = [[NSBundle mainBundle] loadNibNamed:@"HTAudioPlayer" owner:self options:nil].firstObject;
        _audioPlayer.videoURLStr = _mainModel.videoUrl;
        _audioPlayer.alpha = 0.9;
        [self initalVideoLastTimeView];
        [self.audioHeader addSubview:_audioPlayer];
        [_audioPlayer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(50);
        }];
    }
    return _audioPlayer;
}
#pragma mark === 初始化上次播放记录弹框
- (void)initalVideoLastTimeView
{
    if ([[DBDataModel sharedDBDataModel] getCurrentTimeWithVideoId:_videoId] > 1.0) {
        VideoLastTimeView *timeView = [[NSBundle mainBundle] loadNibNamed:@"VideoLastTimeView" owner:self options:nil].firstObject;
        if (_htPlayer) {
            timeView.timeLabel.text = [NSString stringWithFormat:@"记忆您上次看到 %@",[_htPlayer convertTime:[[DBDataModel sharedDBDataModel] getCurrentTimeWithVideoId:_videoId]]];
            [_videoPlayView addSubview:timeView];
        }
        if (_audioPlayer) {
            timeView.timeLabel.text = [NSString stringWithFormat:@"记忆您上次看到 %@",[_audioPlayer convertTime:[[DBDataModel sharedDBDataModel] getCurrentTimeWithVideoId:_videoId]]];
            [self.audioHeader addSubview:timeView];
        }
        timeView.delegate = self;
        [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.bottom.mas_equalTo(-60);
            make.size.mas_equalTo(CGSizeMake(260, 40));
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            timeView.alpha = 0.0;
            [timeView removeFromSuperview];
        });
    }
}

#pragma mark Notification

- (void)fullScreenBtnClick:(NSNotification *)notice{
    
    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationLandscapeLeft: case UIDeviceOrientationLandscapeRight:
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationPortrait]
                                        forKey:@"orientation"];
            break;
        case UIDeviceOrientationPortrait: case UIDeviceOrientationPortraitUpsideDown: case UIDeviceOrientationFaceDown: case UIDeviceOrientationUnknown:case UIDeviceOrientationFaceUp:
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft]
                                        forKey:@"orientation"];
            break;
            
        default:
            break;
    }
    
    
}


#pragma mark === 如果需要识别屏幕旋转,系统必须选中横竖屏,然后实现下列方法
-(BOOL)shouldAutorotate
{
    return YES;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    
    return UIInterfaceOrientationPortraitUpsideDown;
}

#pragma mark - Rotation  如果手机设置了竖屏锁定,就不会横竖屏自动切换,需要手动,
- (void)viewWillTransitionToSize:(CGSize)mySize withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         if (mySize.width > mySize.height) {
             
             [self sc_setNavigationBarHidden:YES animated:YES];
             [_htPlayer toFullScreenWithInterfaceOrientation:UIInterfaceOrientationLandscapeLeft];
             
         } else {
             
             [self sc_setNavigationBarHidden:NO animated:YES];
             [_htPlayer reductionWithInterfaceOrientation:self.videoPlayView];
         }
         
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
         
     }];
    
    [super viewWillTransitionToSize:mySize withTransitionCoordinator:coordinator];
}

#pragma mark === 周期
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.mainModel.isMp3 == MicClassMp3TypeVideo) {
        KPostNotifiCation(@"InterfaceOrientationNotification", @(YES));
    }
    if (_htPlayer) {
        [_htPlayer play];
    }
    if (_audioPlayer) {
        //        [_audioPlayer play];
        //        [self resumeRotateWithTime:@""];
    }
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 0;
    [[UIApplication sharedApplication]setIdleTimerDisabled:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
    if (self.mainModel.isMp3 == MicClassMp3TypeVideo) {
        [self.htPlayer stopPlay];
    }else{
        //        [self.audioPlayer stopPlay];
    }
    KPostNotifiCation(@"InterfaceOrientationNotification", @(NO));
    [IQKeyboardManager sharedManager].enable = NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

#pragma mark - 发起评论
- (void)sendComment:(NSString *)text{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        [self.view endEditing:YES];
        
        NSDictionary *paramDic = @{@"service": AddComment,
                                   @"userId": KgetUserValueByParaName(USERID),
                                   @"toId": self.mainModel.videoId,
                                   @"toUserId":@0,
                                   @"ctype": @(1),
                                   @"pId": @(0),
                                   @"content": text};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    [self pullData];
                    self.toolBar.inputTextView.text = @"";
                }
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
        }];
    }
}


#pragma mark === CirleRequstToolDelegate
-(void)addThumbWithBody:(MicroClassCommentModel *)body
{
    [self reloadOneRowsWithData:body andRow:self.selectedIndexPath];
}
-(void)cancelThumbWithBody:(MicroClassCommentModel *)body
{
    [self reloadOneRowsWithData:body andRow:self.selectedIndexPath];
}
#pragma mark === 刷新一个row ===
-(void)reloadOneRowsWithData:(MicroClassCommentModel *)ymData andRow:(NSInteger) row {
    
    [_mainModel.Comments replaceObjectAtIndex:row withObject:ymData];
    [self.tableV reloadData];
}
#pragma mark === 删除某条说说
-(void)delectMsgWithIndext:(NSIndexPath *)indext
{
    MicroClassCommentModel *model = self.mainModel.Comments[indext.row];
    [YHJHelp aletWithTitle:nil Message:@"确认删除这条信息吗" sureTitle:@"确定" CancelTitle:@"取消" SureBlock:^{
        NSDictionary *paramDic = @{Service:VideoDeleteCommentMessage,
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"commentId":@(model.comContentId)
                                   };
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    [self.mainModel.Comments removeObject:model];
                    self.toolBar.commentBadgeNum.text = [NSString stringWithFormat:@"%ld",(unsigned long)_mainModel.Comments.count];
                    [self.tableV reloadData];
                }else {
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];
    } andCancelBlock:nil andDelegate:self];
    
    
}
#pragma mark === 点击讲师头像查看个人信息
-(void)handleTeacherHeadWithUserId:(NSString *)userId
{
    //1=老师主页 2=用户会员主页
    if (_mainModel.linkCtype == 1) {
        TeacherInfoViewController * targetVC = [[TeacherInfoViewController alloc] init];
        targetVC.teacherId = userId;
        [self.navigationController pushViewController:targetVC animated:YES];
    }else{
        PersonHomeViewController * targetVC = [[PersonHomeViewController alloc] init];
        targetVC.userId = userId.intValue;
//        targetVC.userId = 35842;
        [self.navigationController pushViewController:targetVC animated:YES];
    }
}
#pragma mark === 播放完成
-(void)PlayFinished:(NSNotification *)nofi
{
    [self requetPlayFinishData];
    [self stopRotating];
}
#pragma mark === 增加积分
-(void)requetPlayFinishData
{
    NSDictionary *paramDic = @{Service:AddIntegrateRecord,
                               @"userId":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID),
                               @"outId":self.videoId,
                               @"keyWord":@"filish_video"
                               };
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
            }
        }
    }];
}
#pragma mark === 视频
- (void)videoPlayPause:(NSNotification *)nofi
{
    if (_htPlayer) {
        [
         _htPlayer stopPlay];
    }
}

- (void)videoStartPlay:(NSNotification *)nofi
{
    if (_htPlayer) {
        [_htPlayer play];
    }
}

#pragma mark === 跳转到上次播放的位置
- (void)handleUpdateVideoLastTime
{
    if (_audioPlayer) {
        [_audioPlayer seekTime:[[DBDataModel sharedDBDataModel] getCurrentTimeWithVideoId:_videoId]];
    }
    if (_htPlayer) {
        [_htPlayer seekTime:[[DBDataModel sharedDBDataModel] getCurrentTimeWithVideoId:_videoId]];
    }
}
#pragma mark === 音频
-(void)startPlay:(NSNotification *)nofi
{
    [self resumeRotateWithTime:@""];
}

-(void)playPause:(NSNotification *)nofi
{
    if (_audioPlayer) {
        [_audioPlayer stopPlay];
    }
    [self stopRotating];
}
// 开始旋转
-(void) startRotatingWithTime:(NSString *)time {
    CABasicAnimation* rotateAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotateAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    rotateAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2];   // 旋转一周
    rotateAnimation.duration = 15.0;                                 // 旋转时间20秒
    rotateAnimation.repeatCount = MAXFLOAT;                          // 重复次数，这里用最大次数
    rotateAnimation.removedOnCompletion = NO;                        //（ 确定动画是否在完成后从目标层的动画中移除。当YES，一旦其活动时间已经过去了，动画从目标层的动画删除。默认为yes。）
    [self.audioPlayHeader.newsImage.layer addAnimation:rotateAnimation forKey:nil];
    
}
// 停止旋转
-(void) stopRotating {
    
    CFTimeInterval pausedTime = [self.audioPlayHeader.newsImage.layer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.audioPlayHeader.newsImage.layer.speed = 0.0;                                          // 停止旋转
    self.audioPlayHeader.newsImage.layer.timeOffset = pausedTime;                              // 保存时间，恢复旋转需要用到
}

// 恢复旋转
-(void) resumeRotateWithTime:(NSString *)time {
    
    if (self.audioPlayHeader.newsImage.layer.timeOffset == 0) {
        [self startRotatingWithTime:time];
        return;
    }
    CFTimeInterval pausedTime = self.audioPlayHeader.newsImage.layer.timeOffset;
    self.audioPlayHeader.newsImage.layer.speed = 1.0;                                         // 开始旋转
    self.audioPlayHeader.newsImage.layer.timeOffset = 0.0;
    self.audioPlayHeader.newsImage.layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.audioPlayHeader.newsImage.layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;                                             // 恢复时间
    self.audioPlayHeader.newsImage.layer.beginTime = timeSincePause;                          // 从暂停的时间点开始旋转
}


#pragma mark - 处理中断事件
- (void)handleInterreption:(NSNotification *)sender {
    
    if(_audioPlayer){
        if (_audioPlayer.playBtn.selected) {
            [_audioPlayer stopPlay];
            [self stopRotating];
        }else{
            [_audioPlayer play];
            [self resumeRotateWithTime:@""];
        }
    }
}

- (void)dealloc {
    KRemoverNotifi(kHTPlayerFullScreenBtnNotificationKey);
    KRemoverNotifi(kAudioHTPlayerFinishedPlayNotificationKey);
    KRemoverNotifi(kHTPlayerFinishedPlayNotificationKey);
    KRemoverNotifi(kAudioHTPlayerPauseVideoNotificationKey);
    KRemoverNotifi(kAudioHTPlayerStartPlayNotificationKey);
    KRemoverNotifi(UIApplicationWillResignActiveNotification);
    KRemoverNotifi(UIApplicationDidBecomeActiveNotification);
    KRemoverNotifi(AVAudioSessionInterruptionNotification);
    [self releasePlayer];
}
- (void)releasePlayer
{
    if (_htPlayer) {
        [[DBDataModel sharedDBDataModel] addRowData_UserVideoProgressWithTime:[_htPlayer currentTime] videoId:_videoId];
        [_htPlayer releaseWMPlayer];
    }
    if (_audioPlayer) {
        [_audioPlayer stopPlay];
        [[DBDataModel sharedDBDataModel] addRowData_UserVideoProgressWithTime:[_audioPlayer currentTime] videoId:_videoId];
        [_audioPlayer releaseWMPlayer];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return  UIStatusBarStyleLightContent;
}


@end
