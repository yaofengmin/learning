//
//  TeacherInfoViewController.m
//  MicroClassroom
//
//  Created by fm on 2017/11/18.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "TeacherInfoViewController.h"

#import "MicroClassBannerSegmentModel.h"

@interface TeacherInfoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *teacherHead;
@property (weak, nonatomic) IBOutlet UILabel *teacherName;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *teacherDes;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHig;
@property (weak, nonatomic) IBOutlet UILabel *remark;


@property (nonatomic ,strong) MicroClassTeacherModel *infoModel;
@end

@implementation TeacherInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"个人主页";
    self.topHig.constant = KTopHeight + 20;
    self.teacherHead.layer.cornerRadius = 40 / 2.0;
    self.teacherHead.layer.masksToBounds = YES;
    [self requestInfo];
}

-(void)requestInfo
{
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{Service:GetTeachersById,@"teacherId":self.teacherId} andBlocks:^(NSDictionary *result) {
        if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
            self.infoModel = [MicroClassTeacherModel mj_objectWithKeyValues:result[REQUEST_INFO]];
            [self.teacherHead sd_setImageWithURL:[NSURL URLWithString:self.infoModel.pic] placeholderImage:[UIImage imageNamed:@"login_head"]];
            self.teacherName.text = self.infoModel.name;
            self.content.text = self.infoModel.position;
            self.teacherDes.text = self.infoModel.content;
//            self.remark.text = self.infoModel.remark;
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
