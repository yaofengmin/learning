//
//  TeacherInfoViewController.h
//  MicroClassroom
//
//  Created by fm on 2017/11/18.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"

@interface TeacherInfoViewController : YHBaseViewController
@property (nonatomic ,copy) NSString *teacherId;
@end
