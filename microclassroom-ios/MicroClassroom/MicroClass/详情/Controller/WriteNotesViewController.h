//
//  WriteNotesViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

#import "MyNotesListModel.h"
@interface WriteNotesViewController : YHBaseViewController
@property (nonatomic,copy) NSString *videoId;

-(instancetype)initWithModel:(MyNotesListModel *)model isWrite:(BOOL)isWirte;
@end
