//
//  WriteNotesTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WriteNotesTableViewCell : UITableViewCell<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *titleTipLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentTip;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;


@property (nonatomic,copy) void (^titleBlock)(NSString *text);
@property (nonatomic,copy) void (^contentBlock)(NSString *text);

@end
