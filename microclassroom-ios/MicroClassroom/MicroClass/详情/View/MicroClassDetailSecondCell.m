//
//  MicroClassDetailSecondCell.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MicroClassDetailSecondCell.h"

//#import "ArticleDisplayViewController.h"

@implementation MicroClassDetailSecondCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)readAll:(UIButton *)sender {
//    ArticleDisplayViewController *articleVC = [[ArticleDisplayViewController alloc]init];
//    articleVC.sc_title = self.model.videoTitle;
//    articleVC.content = self.model.videoDesc;
//    [self.viewController.navigationController pushViewController:articleVC animated:YES];
}

-(void)setModel:(MicroClassDetailModel *)model
{
    _model = model;
//    if ([model.isMp3 intValue] == 2) {
//        self.typeTitle.text = @"音频导读";
//    }else{
//        self.typeTitle.text = @"视频导读";
//    }
//    self.content.text = model.videoDesc;
//    //    CGFloat height = [self cacluteHeight:model.videoDesc];
//    //    if (height / 18 > 6) {
//    //        self.readBtn.hidden = NO;
//    //    }else{
//    //        self.readBtn.hidden = YES;
//    //    }
//    if (model.showAll) {
//        self.readBtn.hidden = NO;
//    }else{
//        self.readBtn.hidden = YES;
//    }
}
-(CGFloat)cacluteHeight:(NSString *)text
{
    if (text.length == 0) {
        return 0;
    }
    NSAttributedString *textStr = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    CGRect msgContentSize = [textStr boundingRectWithSize:CGSizeMake(KScreenWidth - 32, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    
    return msgContentSize.size.height;
}

@end
