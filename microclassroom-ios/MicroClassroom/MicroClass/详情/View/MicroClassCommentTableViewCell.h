//
//  MicroClassCommentTableViewCell.h
//  MicroClassroom
//
//  Created by fm on 2017/11/8.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MicroClassBannerSegmentModel.h"
#import "WFReplyBody.h"

@protocol deleteAction <NSObject>
@optional;
- (void)delectMsgWithIndext:(NSIndexPath *) indext;

- (void)seeAllCommentWithIndex:(NSIndexPath *) indexPath;


@end

@interface MicroClassCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *contentBg;
@property (weak, nonatomic) IBOutlet UIImageView *headImag;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *allBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;

@property (nonatomic,strong) NSIndexPath *stamp;

@property (nonatomic,weak) id<deleteAction> delegate;


@property(nonatomic ,copy) NSString *userId;

-(void)configWithCommentModel:(MicroClassCommentModel *)model;


-(void)configWithWeCommentModel:(WFReplyBody *)model;



@end
