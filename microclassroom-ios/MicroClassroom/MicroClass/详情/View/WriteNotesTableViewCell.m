//
//  WriteNotesTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "WriteNotesTableViewCell.h"

@implementation WriteNotesTableViewCell

- (void)awakeFromNib {
	[super awakeFromNib];
	self.titleTextView.delegate               = self;
	self.contentTextView.delegate             = self;
	self.titleTextView.layer.cornerRadius     = 4.0;
	self.titleTextView.layer.masksToBounds    = YES;
	self.contentTextView.layer.cornerRadius   = 4.0;
	self.contentTextView.layer.masksToBounds  = YES;
}

#pragma mark === UITextViewDelegate
-(void)textViewDidChange:(UITextView *)textView{
	if (textView.tag == 1000) {//问题名称
		if (textView.text.length != 0) {
			self.titleTipLabel.hidden = YES;
		}else{
			self.titleTipLabel.hidden = NO;
		}
        self.titleBlock(textView.text);
	}else if (textView.tag == 1001){//问题描述
		if (textView.text.length != 0) {
			self.contentTip.hidden = YES;
		}else{
			self.contentTip.hidden = NO;
		}
        self.contentBlock(textView.text);
	}
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

@end
