//
//  MicroClassThirdCell.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MicroClassBannerSegmentModel.h"

@interface MicroClassThirdCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftImage;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *topTitle;
@property (weak, nonatomic) IBOutlet UIImageView *typeImage;
@property (weak, nonatomic) IBOutlet UIImageView *watchIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typeLeft;

@property (nonatomic,strong) MicroClassVideoModel *model;

- (void)configWithModel:(MicroClassVideoModel *)model;
@end
