//
//  DIYHeadSearchButton.m
//  MicroClassroom
//
//  Created by fm on 2017/11/9.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "DIYHeadSearchButton.h"

@implementation DIYHeadSearchButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius  = 4.0;
    self.layer.masksToBounds = YES;
}
@end
