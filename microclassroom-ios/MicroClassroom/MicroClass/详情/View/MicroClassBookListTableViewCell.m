//
//  MicroClassBookListTableViewCell.m
//  yanshan
//
//  Created by fm on 2017/11/3.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MicroClassBookListTableViewCell.h"

@implementation MicroClassBookListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bookImage.backgroundColor = ImageBackColor;
    self.bookImage.contentMode = UIViewContentModeScaleAspectFill;
    self.bookImage.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)configDetailWithModel:(MicroClassVideoModel *)model
{
    self.authorLabel.text = [NSString stringWithFormat:@"%@",model.author];
    self.bookTitle.text = model.videoTitle;
    self.keyWordsLabel.text = model.videoKeyword;
    self.seeNum.text = model.videoLaud;
    self.sawIcon.hidden = [model.videoLaud intValue] == 0;
    self.seeNum.hidden = [model.videoLaud intValue] == 0;
    [self setImg:model.videoLogo];

}
- (void)setImg:(NSString *)urlStr {
    [self.bookImage sd_setImageWithURL:[NSURL URLWithString:urlStr]];
    
}

@end
