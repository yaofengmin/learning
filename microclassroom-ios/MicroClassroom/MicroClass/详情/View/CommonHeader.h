//
//  CommonHeader.h
//  yanshan
//
//  Created by fm on 2017/8/17.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
