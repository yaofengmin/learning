//
//  VideoLastTimeView.h
//  MicroClassroom
//
//  Created by yfm on 2018/2/28.
//  Copyright © 2018年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoUpdateTime <NSObject>

@optional
- (void)handleUpdateVideoLastTime;
@end

@interface VideoLastTimeView : UIView
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic,weak) id<VideoUpdateTime> delegate;

- (void)dismiss;
@end
