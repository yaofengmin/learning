//
//  MicroClassDetailFirstCell.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MicroClassDetailFirstCell.h"

#import "YHBaseWebViewController.h"

@implementation MicroClassDetailFirstCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.headImage.backgroundColor = ImageBackColor;
    self.headImage.layer.cornerRadius = 30 / 2.0;
    self.headImage.layer.masksToBounds = YES;
    
    self.keyWorkBg.layer.cornerRadius  = 4.0;
    self.keyWorkBg.layer.masksToBounds = YES;
    
    self.authorBg.layer.cornerRadius  = 4.0;
    self.authorBg.layer.masksToBounds = YES;
    UITapGestureRecognizer *headerTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTeacherHead)];
    [self.authorBg addGestureRecognizer:headerTap];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)handleSeeAction:(UIButton *)sender {
    YHBaseWebViewController *articleVC = [[YHBaseWebViewController alloc]initWithUrlStr:self.model.detailUrl andNavTitle:self.model.videoTitle];
    [self.viewController.navigationController pushViewController:articleVC animated:YES];
    
}

-(void)setModel:(MicroClassDetailModel *)model
{
    _model = model;
    self.topTitle.text = model.videoTitle;
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:model.authorLogo]];
    self.authorLabel.text = [NSString stringWithFormat:@"作者:  %@",model.author];
    self.authorAndTime.text = [NSString stringWithFormat:@"上传时间:  %@",model.addTime];;
    self.content.text = model.videoDesc;
    self.keyWordName.text = [NSString stringWithFormat:@"关键词:  %@",model.videoKeyword];
    self.seeNum.text = model.videoLaud;
    self.authorDes.text = model.authorDes;
    
    self.seeAllBtn.hidden = !model.showAll;
    self.authorBgTop.constant = model.showAll?40:10;
    NSMutableParagraphStyle  *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:9.0];
    if (model.videoDesc.length) {
        NSMutableAttributedString *videoDesc = [[NSMutableAttributedString alloc]initWithString:model.videoDesc];
        [videoDesc addAttributes:@{NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, model.videoDesc.length)];
        self.content.attributedText = videoDesc;
    }
    
    if (model.authorDes.length) {
        NSMutableAttributedString *authorDes = [[NSMutableAttributedString alloc]initWithString:model.authorDes];
        [authorDes addAttributes:@{NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, model.authorDes.length)];
        self.authorDes.attributedText = authorDes;
    }
}

#pragma mark === 讲师头像点击
-(void)handleTeacherHead
{
    if (self.model.teacherId.length) {
        if ([self.delegate respondsToSelector:@selector(handleTeacherHeadWithUserId:)]) {
            [self.delegate handleTeacherHeadWithUserId:self.model.teacherId];
        }
    }
}
@end
