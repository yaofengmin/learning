//
//  MicroClassBookListTableViewCell.h
//  yanshan
//
//  Created by fm on 2017/11/3.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MicroClassBannerSegmentModel.h"

@interface MicroClassBookListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;

@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *seeNum;
@property (weak, nonatomic) IBOutlet UIImageView *sawIcon;
@property (weak, nonatomic) IBOutlet UILabel *keyWordsLabel;

- (void)configDetailWithModel:(MicroClassVideoModel *)model;

@end
