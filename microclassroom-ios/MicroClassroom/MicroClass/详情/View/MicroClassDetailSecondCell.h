//
//  MicroClassDetailSecondCell.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MicroClassBannerSegmentModel.h"

@interface MicroClassDetailSecondCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeTitle;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UIButton *readBtn;

@property (nonatomic,strong) MicroClassDetailModel *model;

@end
