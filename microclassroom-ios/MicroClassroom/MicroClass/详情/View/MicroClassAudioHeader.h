//
//  MicroClassAudioHeader.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MicroClassAudioHeader : UIView
@property (weak, nonatomic) IBOutlet UIImageView *newsImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageBg;

@end
