//
//  MicroClassThirdCell.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MicroClassThirdCell.h"

@implementation MicroClassThirdCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.leftImage.backgroundColor = ImageBackColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithModel:(MicroClassVideoModel *)model {//// 1=视频 2=音频 3=图书 4=跳转web
	self.topTitle.text = model.videoTitle;
	self.numLabel.text = model.videoLaud;
	self.watchIcon.hidden = [model.videoLaud intValue] == 0;
	self.numLabel.hidden = [model.videoLaud intValue] == 0;
//    if ([model.isMp3 isEqualToString:@"3"]) {
//        self.typeImage.hidden = NO;
//        self.typeLabel.text = @"#书籍";
//        self.typeLeft.constant = 32;
//    }else{
//        self.typeImage.hidden = YES;
//        self.typeLabel.text = [NSString stringWithFormat:@"#%@",model.author];
//        self.typeLeft.constant = 18;
//    }
	
	[self setImg:model.videoLogo];
}

- (void)setImg:(NSString *)urlStr {
	[self.leftImage sd_setImageWithURL:[NSURL URLWithString:urlStr]];
	
}
@end
