//
//  MicroClassCommentTableViewCell.m
//  MicroClassroom
//
//  Created by fm on 2017/11/8.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "MicroClassCommentTableViewCell.h"

@implementation MicroClassCommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImag.layer.cornerRadius   = 30 / 2.0;
    self.headImag.layer.masksToBounds  = YES;
    self.headImag.userInteractionEnabled = YES;
    
    
    UITapGestureRecognizer *imageTap       = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(HeaderImageTap:)];
    [self.headImag addGestureRecognizer:imageTap];
    
    
    self.contentBg.layer.cornerRadius  = 4.0;
    self.contentBg.layer.masksToBounds = YES;
}
- (IBAction)deleteAction:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(delectMsgWithIndext:)]) {
        [self.delegate delectMsgWithIndext:self.stamp];
    }
}
- (IBAction)handleAllAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(seeAllCommentWithIndex:)]) {
        [self.delegate seeAllCommentWithIndex:self.stamp];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
#pragma mark === we+评论详情
-(void)configWithWeCommentModel:(WFReplyBody *)model
{
    _userId = model.userId;
    NSURL *photoUrl                    = [NSURL URLWithString:model.userPhoto];
    [self.headImag sd_setImageWithURL:photoUrl placeholderImage:[UIImage imageNamed:@"login_head"]];
    self.nameLabel.text                = model.userName;
    self.dateLabel.text                =  model.postTime;

    NSString *commetText;
    if (IsEmptyStr(model.toUserName)) {
        commetText                     = model.content;
    }else {
        if (model.toUserName.length) {
            commetText                     = [NSString stringWithFormat:@"回复 %@:%@",model.toUserName,model.content];
        }else {
            commetText                     = [NSString stringWithFormat:@"回复 %@:%@",model.toUserName,model.content];
            
        }
    }
    self.contentLabel.text             = commetText;
    self.contentHeight.constant = [self cacluteLabelHeight:commetText];
    self.allBtn.hidden = YES;
    self.deleteBtn.hidden = YES;
}

#pragma mark === 音视频
-(void)configWithCommentModel:(MicroClassCommentModel *)model
{
    _userId = [NSString stringWithFormat:@"%d",model.userId];
    [self.headImag sd_setImageWithURL:[NSURL URLWithString:model.photo] placeholderImage:[UIImage imageNamed:@"login_head"]];
    self.nameLabel.text = model.nickName;
    self.dateLabel.text = model.comTime;
    self.contentLabel.text = model.comcontent;
    if (model.replay_commnets.count == 0) {
        self.allBtn.hidden = YES;
    }else{
        self.allBtn.hidden = NO;
        [self.allBtn setTitle:[NSString stringWithFormat:@"查看全部 %ld 条评论回复",model.replay_commnets.count] forState:UIControlStateNormal];
    }
    if ([[NSString stringWithFormat:@"%d",model.userId] isEqualToString:KgetUserValueByParaName(USERID)]) {
        self.deleteBtn.hidden = NO;
    }else{
        self.deleteBtn.hidden = YES;
    }
    
    self.contentHeight.constant = [self cacluteLabelHeight:model.comcontent];
}

-(CGFloat)cacluteLabelHeight:(NSString *)text
{
    if (text.length == 0) {
        return 0;
    }
    NSAttributedString *textStr = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    CGRect msgContentSize = [textStr boundingRectWithSize:CGSizeMake(KScreenWidth - 92, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    
    return (msgContentSize.size.height + 30) < 35?35:(msgContentSize.size.height + 30);
}

#pragma mark --点击userHead

-(void)HeaderImageTap:(UITapGestureRecognizer*)tap{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"commentListHeadClick" object:@{@"userId":_userId}];
}
@end
