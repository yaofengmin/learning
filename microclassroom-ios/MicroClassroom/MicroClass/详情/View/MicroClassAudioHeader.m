//
//  MicroClassAudioHeader.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MicroClassAudioHeader.h"

@implementation MicroClassAudioHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
	[super awakeFromNib];
	self.newsImage.backgroundColor = ImageBackColor;
    self.newsImage.layer.cornerRadius = 100 / 2.0;
    self.newsImage.layer.masksToBounds = YES;
    self.newsImage.layer.borderWidth = 7.0;
    self.newsImage.layer.borderColor = BColor.CGColor;
    self.newsImage.contentMode = UIViewContentModeScaleAspectFill;
    self.newsImage.clipsToBounds = YES;
    
    self.imageBg.contentMode=UIViewContentModeScaleAspectFill;
    self.imageBg.clipsToBounds = YES;
}

@end
