//
//  VideoLastTimeView.m
//  MicroClassroom
//
//  Created by yfm on 2018/2/28.
//  Copyright © 2018年 Hanks. All rights reserved.
//

#import "VideoLastTimeView.h"

@implementation VideoLastTimeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor     = [UIColor colorWithWhite:0.0 alpha:0.9];
    self.layer.cornerRadius  = 4.0;
    self.layer.masksToBounds = YES;
}
- (IBAction)handleUpdateProAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(handleUpdateVideoLastTime)]) {
        [self.delegate handleUpdateVideoLastTime];
    }
    [self dismiss];
}
- (IBAction)handleCloseAction:(UIButton *)sender {
    [self dismiss];
}

- (void)dismiss
{
    [UIView animateWithDuration:2.0 animations:^{
        self.alpha = 0.0;
        [self removeFromSuperview];
    }];
}
@end
