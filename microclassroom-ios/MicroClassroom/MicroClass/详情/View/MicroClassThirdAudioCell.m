//
//  MicroClassThirdAudioCell.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MicroClassThirdAudioCell.h"

@implementation MicroClassThirdAudioCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.leftImage.backgroundColor = ImageBackColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
