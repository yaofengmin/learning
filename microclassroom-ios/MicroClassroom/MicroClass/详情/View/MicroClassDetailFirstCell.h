//
//  MicroClassDetailFirstCell.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MicroClassBannerSegmentModel.h"
@protocol teacherHeadTap <NSObject>

@optional
- (void)handleTeacherHeadWithUserId:(NSString *)userId;
@end


@interface MicroClassDetailFirstCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *topTitle;
@property (weak, nonatomic) IBOutlet UILabel *authorAndTime;
@property (weak, nonatomic) IBOutlet UILabel *keyWordName;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *authorDes;
@property (weak, nonatomic) IBOutlet UILabel *seeNum;
@property (weak, nonatomic) IBOutlet UIButton *seeAllBtn;
@property (weak, nonatomic) IBOutlet UIView *keyWorkBg;
@property (weak, nonatomic) IBOutlet UIView *authorBg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *authorBgTop;

@property (nonatomic,assign) id<teacherHeadTap> delegate;

@property (nonatomic,strong) MicroClassDetailModel *model;

@end
