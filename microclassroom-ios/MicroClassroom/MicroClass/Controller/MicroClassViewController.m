//
//  MicroClassViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/6.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MicroClassViewController.h"
#import "SDCycleScrollView.h"
#import "MicroClassListCell.h"
#import "MicroClassBannerSegmentModel.h"
#import "MicroClassSearchViewController.h"
#import "YHNavgationController.h"
#import "HomeGameItemModel.h"
#import <MJRefresh.h>
#import "MainMicroClassViewController.h"
#import "YHBaseWebViewController.h"

#import "MicroClassDetailViewController.h"
#import "BooksDetailViewController.h"
#import "MicroClassBookListTableViewCell.h"

//轮播(非全屏轮播)
#import "NewPagedFlowView.h"
#import "PGCustomBannerView.h"

static NSString * const kMicroClassListCellID = @"MicroClassListCell";
static NSString *const  KMicroClassBookListCellID = @"MicroClassBookListCellID";

//static CGFloat const kMicroClassListCellHeight = 120.0;
@interface MicroClassViewController ()
<SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource
//,NewPagedFlowViewDelegate, NewPagedFlowViewDataSource
>
{
    SDCycleScrollView * _bannerView;
    //    NewPagedFlowView  *_bannerView;
    MicroClassVideoListModel * _videoModel;
    NSInteger pageCount;
}

@property (strong, nonatomic) NSMutableArray<MicroClassVideoModel *> *resultModel;
@property (strong, nonatomic)  UITableView *tableView;
@property (strong, nonatomic)  NSArray<BannerModel *> *bannerArr;
@property (nonatomic ,strong) DIYTipView *tipView;

@property (nonatomic ,strong) NSArray *showArr;

@end

@implementation MicroClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = NCColor;
    self.resultModel = [NSMutableArray array];
    pageCount = 1;
    self.isRootVC = YES;
    [self setUpTableView];
    [self pullVideoListByType:self.homeItemModel.cateId];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpWhenViewWillAppearForTitle:(NSString *)title forIndex:(NSInteger)index firstTimeAppear:(BOOL)isFirstTime {
    
    
}


#pragma mark - 刷新
-(void)loadDataForHeader {
    pageCount = 1;
    [self.resultModel removeAllObjects];
    [self pullVideoListByType:self.homeItemModel.cateId];
    
}

#pragma mark - 加载
- (void)loadMoreData {
    pageCount ++ ;
    [self pullVideoListByType:self.homeItemModel.cateId];
}


- (void)pullVideoListByType:(NSString *)type {
    
    NSDictionary *paramDic = @{@"service": GetVideoListByClass,
                               @"cateId": type,
                               @"user_Id":KgetUserValueByParaName(USERID) ==nil ? @"":KgetUserValueByParaName(USERID),
                               @"pageIndex": @(pageCount),
                               @"pageSize": @20};
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (pageCount == 1) {
            [self.tableView.mj_header endRefreshing];
        }else {
            [self.tableView.mj_footer endRefreshing];
        }
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                _videoModel = [MicroClassVideoListModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                if (self.bannerArr.count == 0) {
                    self.bannerArr = [BannerModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST][@"display_list"]];
                    [self sortBanner:self.bannerArr];
                }
                [self.resultModel addObjectsFromArray: _videoModel.list];
                
                [self.tableView reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
                _videoModel = [MicroClassVideoListModel new];
                
                [self.tableView reloadData];
                
            }
            
        } else {
            _videoModel = [MicroClassVideoListModel new];
            
            [self.tableView reloadData];
            
        }
        
        
        if (self.resultModel.count==0) {
            if (!_tipView) {
                _tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                
            }
            [self.tableView addSubview:_tipView];
            [_tipView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.view);
                make.size.equalTo(CGSizeMake(120, 120));
                
            }];
        }else
        {
            if (_tipView.superview) {
                
                [_tipView removeFromSuperview];
            }
        }
        
        if (_videoModel.total == 0 || self.resultModel.count == _videoModel.total) {
            self.tableView.mj_footer.hidden = YES;
        }
    }];
}

- (void)sortBanner:(NSArray<BannerModel *>*)arr {
    [self layoutSubviews];
    UIView *header = [[UIView alloc]init];
    header.backgroundColor = NCColor;
    //    if (arr.count) {
    //        NSMutableArray *showArr = @[].mutableCopy;
    //        for (BannerModel * item in arr) {
    //            [showArr addObject:item.pic];
    //        }
    //        _bannerView.orginPageCount = showArr.count;
    //        self.showArr = [NSArray arrayWithArray:showArr];
    //        CGFloat imageH = (KScreenWidth - 40) * 30 / 67;
    //        if (arr.count == 1) {
    //            _bannerView.frame = CGRectMake(0, 10, KScreenWidth, imageH);
    //        }else{
    //            _bannerView.frame = CGRectMake(-20, 10, KScreenWidth + 20, imageH);
    //        }
    //        header.frame = CGRectMake(0, 0, KScreenWidth, imageH + 20);
    //    }else{
    //        _bannerView.frame = CGRectZero;
    //        header.frame = CGRectZero;
    //    }
    //    [header addSubview:_bannerView];
    //    _bannerView.delegate = self;
    //    _bannerView.dataSource = self;
    //    [_bannerView reloadData];
    //
    //    self.tableView.tableHeaderView = header;
    if (arr.count) {
        NSMutableArray *showArr = @[].mutableCopy;
        for (BannerModel * item in arr) {
            [showArr addObject:item.pic];
        }
        _bannerView.imageURLStringsGroup = showArr;
        
        CGFloat imageH = KScreenWidth/320 * arr[0].photoHeight.floatValue;
        _bannerView.frame = CGRectMake(0, 0, KScreenWidth, imageH);
        
    }else{
        _bannerView.frame = CGRectZero;
    }
    self.tableView.tableHeaderView = _bannerView;
}




#pragma mark - Layout
- (void)layoutSubviews {
    
    _bannerView = ({
        SDCycleScrollView * view = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:nil];
        view;
    });
    //    if (_bannerView == nil) {
    //        _bannerView = [[NewPagedFlowView alloc] initWithFrame:CGRectZero];
    //        _bannerView.backgroundColor = NCColor;
    //        _bannerView.minimumPageAlpha = 0.4;
    //
    //        _bannerView.leftRightMargin = 20;
    //        _bannerView.topBottomMargin = 0;
    //
    //        _bannerView.isOpenAutoScroll = YES;
    //    }
    
    //初始化pageControl0   0
    //    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, pageFlowView.frame.size.height - 24, KScreenWidth, 8)];
    //    pageFlowView.pageControl = pageControl;
    //    [pageFlowView addSubview:pageControl];
    //    [self.view addSubview:pageFlowView];
    
    
}

- (void)setUpTableView {
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 120;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    //    self.tableView.tableHeaderView = _bannerView;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 49, 0);
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight + kListBarH, 0, 0, 0));
    }];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassListCellID bundle:nil] forCellReuseIdentifier:kMicroClassListCellID];
    [self.tableView registerNib:[UINib nibWithNibName:@"MicroClassBookListTableViewCell" bundle:nil] forCellReuseIdentifier:KMicroClassBookListCellID];
    
    self.tableView.tableFooterView = [UIView new];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
    BannerModel *model = self.bannerArr[index];
    if ([model.itemType isEqualToString:@"1"]) {
        MicroClassDetailViewController * targetVC = [[MicroClassDetailViewController alloc] init];
        targetVC.videoId = model.outLinkOrId;
        [self.parentContr.navigationController pushViewController:targetVC animated:YES];
    }else{
        if (NOEmptyStr(model.outLinkOrId)) {
            YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
            [self.parentContr.navigationController pushViewController:web animated:YES];
        }
    }
}



#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _resultModel.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MicroClassVideoModel *model = self.resultModel[indexPath.row];
    if (model.isMp3 == MicClassMp3TypeBook) {
        MicroClassBookListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KMicroClassBookListCellID forIndexPath:indexPath];
        [cell configDetailWithModel:model];
        return cell;
    }else{
        MicroClassListCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassListCellID forIndexPath:indexPath];
        
        [cell configWithModel:_resultModel[indexPath.row]];
        return cell;
    }
}

#pragma mark - UITableViewDelegate
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    MicroClassVideoModel *model = self.resultModel[indexPath.row];
//    if (model.isMp3 == MicClassMp3TypeWeb){
//        return 110.0;
//    }
//    return kMicroClassListCellHeight;
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MicroClassVideoModel *model = _resultModel[indexPath.row];
    if (model.isVip) {//微课里面 如果 isVip ==1，才需要判断 用户是否vip。 如果 isVip ==0，则不管是不是vip，都能看
        if ([[UserInfoModel getInfoModel].grade integerValue] == 0) {
            @weakify(self);
            [YHJHelp aletWithTitle:@"提示" Message:@"您还不是VIP会员，不能观看" sureTitle:@"立即开通 " CancelTitle:@"忽略 " SureBlock:^{
                @strongify(self);
                BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                [self.parentContr.navigationController pushViewController:buy animated:YES];
                
            } andCancelBlock:nil andDelegate:self.parentContr];
            return;
        }
    }
    if (model.isMp3  == MicClassMp3TypeWeb) {//web
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.webUrl andNavTitle:model.videoTitle];
        [self.parentContr.navigationController pushViewController:web animated:YES];
    }else if (model.isMp3 == MicClassMp3TypeBook){//书籍详情
        BooksDetailViewController *detailVC = [[BooksDetailViewController alloc]init];
        detailVC.videoId = model.videoId;
        [self.parentContr.navigationController pushViewController:detailVC animated:YES];
    }else{
        MicroClassDetailViewController * targetVC = [[MicroClassDetailViewController alloc] init];
        targetVC.videoId = model.videoId;
        [self.parentContr.navigationController pushViewController:targetVC animated:YES];
    }
}

//- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
//
//    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
//    BannerModel *model = self.bannerArr[subIndex];
//    if ([model.itemType isEqualToString:@"1"]) {
//        MicroClassDetailViewController * targetVC = [[MicroClassDetailViewController alloc] init];
//        targetVC.videoId = model.outLinkOrId;
//        [self.parentContr.navigationController pushViewController:targetVC animated:YES];
//
//    }else{
//        if (NOEmptyStr(model.outLinkOrId)) {
//            YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
//            [self.parentContr.navigationController pushViewController:web animated:YES];
//        }
//    }
//}

//- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
//
////    NSLog(@"CustomViewController 滚动到了第%ld页",pageNumber);
//}

//- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
//    CGFloat imageH = 0;
//    if (self.showArr.count > 0) {
//        imageH = (KScreenWidth - 40) * 30 / 67;
//    }
//    CGFloat imageW = 0;
//    if (self.showArr.count > 1) {
//        imageW = KScreenWidth - 40;
//    }else{
//        imageW = KScreenWidth - 20;
//    }
//    return CGSizeMake(imageW, imageH);
//
//}

#pragma mark --NewPagedFlowView Datasource
//- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
//
//    return self.showArr.count;
//}
//
//- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
//
//    PGCustomBannerView *bannerView = (PGCustomBannerView *)[flowView dequeueReusableCell];
//    bannerView.backgroundColor = [UIColor whiteColor];
//    if (!bannerView) {
//        bannerView = [[PGCustomBannerView alloc] init];
//    }
//
//    //在这里下载网络图片
//    [bannerView.mainImageView sd_setImageWithURL:[NSURL URLWithString:self.showArr[index]] placeholderImage:[UIImage imageNamed:@""]];
//    return bannerView;
//}
//





@end
