//
//  RewardViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/15.
//  Copyright © 2016年 Hanks. All rights reserved.
//  打赏

#import "RewardViewController.h"
#import "InitConfigModel.h"
@interface RewardViewController ()

@property (nonatomic, strong) UILabel     *titleLabel;
@property (nonatomic, strong) UILabel     *contentLabel;
@property (nonatomic, strong) UIButton    *payBtn;
@property (nonatomic, strong) UIButton    *towBtn;
@property (nonatomic, strong) UIButton    *fiveBtn;
@property (nonatomic, strong) UIButton    *tenBtn;
@property (nonatomic, strong) UITextField *moneyF;
@property (nonatomic, strong) NSArray     *payNumberArr;
@property (nonatomic, strong) UIButton    *tempBtn;

@end

@implementation RewardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _payNumberArr = [InitConfigModel shareInstance].reward_moenys;
    [self initPop];
    [self setupUI];
    // Do any additional setup after loading the view.
}


- (void)initPop {
    self.view.backgroundColor = [UIColor whiteColor];
    self.contentSizeInPopup = (CGSize){270, 280};
    self.popupController.navigationBarHidden = YES;
    self.popupController.containerView.layer.cornerRadius = 7.f;
    self.popupController.containerView.backgroundColor = [UIColor clearColor];
    [self.popupController.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTap)]];
}

- (void)setupUI {

  
    // titleLabel
    [self.view addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.view.mas_top).offset(30);
    }];
    
    // contentLabel
    [self.view addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
    }];
    
    
    CGFloat btnW  = 70;
    CGFloat btnH  = 30;
    CGFloat distance = (self.view.viewWidth - btnW*3)/4;
    NSMutableArray *tempBtn = @[].mutableCopy;
    for (int i = 0; i < _payNumberArr.count; i++ ) {
        NSString *title = [_payNumberArr[i] stringValue];
        UIButton *btn = [self getBtnWithTitle:[NSString stringWithFormat:@"%@元",title]];
        [btn addTarget:self action:@selector(payNumberBtnDown:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        [tempBtn addObject:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentLabel.mas_bottom).offset(20);
            make.size.equalTo(CGSizeMake(btnW, btnH));
            if (i==0) {
                make.left.equalTo(distance);
            }else {
                make.left.equalTo([tempBtn[i -1] mas_right]).offset(distance);
            }
        }];
    }
    
    UILabel *label = [UILabel labelWithText:@"任性赞赏" andFont:13 andTextColor:KColorFromRGB(0xFB6300) andTextAlignment:0];
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(distance);
        make.top.equalTo([tempBtn[0] mas_bottom]).offset(40);
    }];
    
    [self.view addSubview:self.moneyF];
    [self.moneyF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_right).offset(5);
        make.centerY.equalTo(label);
        make.size.equalTo(CGSizeMake(150, 30));
    }];
    
    UILabel *unitLabel = [UILabel labelWithText:@"元" andFont:13 andTextColor:KColorFromRGB(0xFB6300) andTextAlignment:0];
    [self.view addSubview:unitLabel];
    [unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.moneyF.mas_right).offset(10);
        make.centerY.equalTo(label);
    }];
    
    [self.view addSubview:self.payBtn];
    [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.equalTo(self.moneyF.mas_bottom).offset(40);
        make.centerX.equalTo(0);
        make.size.equalTo(CGSizeMake(self.view.viewWidth*0.88, 35));
    }];
    
    @weakify(self);
    [self.payBtn handleEventTouchUpInsideWithBlock:^{
        @strongify(self);
        if ([self cheackParam]) {
            
            NSString *doMoney = @"";
            if ([self.moneyF.text integerValue] >0 ) {
                doMoney = self.moneyF.text;
            }else {
                doMoney = [_tenBtn.currentTitle componentsSeparatedByString:@"元"][0];
            }
            
            
            NSDictionary *paramDic = @{Service:AppDoReward,
                                       @"userId":KgetUserValueByParaName(USERID),
                                       @"videoId":self.videoId,
                                       @"doMoney":doMoney
                                       };
            
            SHOWHUD;
            [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                HIDENHUD;
                if (result) {
                    if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                        [WFHudView showMsg:@"打赏成功!谢谢您的支持" inView:nil];
                        [self backgroundTap];
                    }else{
                        [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    }
                }
            }];
        }
    }];
}


#pragma  mark -检查参数
-(BOOL)cheackParam {
    if (_tenBtn == nil && [self.moneyF.text integerValue]<= 0) {
        return NO;
    }
    
    if ([self.moneyF.text integerValue]== 0 && _tenBtn) {
        return YES;
    }
    
    if (_tenBtn == nil && [self.moneyF.text integerValue] > 0 ) {
        return YES;
    }
    
    if (_tenBtn && [self.moneyF.text integerValue] > 0) {
        return YES;
    }
    
    return YES;
}



- (void)payNumberBtnDown:(UIButton *)sender {
    
    if ([sender isEqual:_tenBtn]) {
        sender.selected = !sender.selected;
        if (sender.selected) {
            sender.layer.borderColor = KColorFromRGB(0xFB6300).CGColor;
            _tenBtn = sender;

        }else{
            sender.layer.borderColor = DColor.CGColor;
            _tenBtn = nil;

        }
    }else {
        sender.selected = YES;
        sender.layer.borderColor = KColorFromRGB(0xFB6300).CGColor;
        _tenBtn.selected = NO;
        _tenBtn.layer.borderColor = DColor.CGColor;
        _tenBtn = sender;
    }
    
    if (_tenBtn) {
        self.payBtn.backgroundColor = kNavigationBarColor;
    }else{
        if ([self.moneyF.text intValue] > 0) {
            return;
        }
        self.payBtn.backgroundColor = FColor;
    }
}

#pragma  mark -输入
-(void)textDidChange:(UITextField *)tf{
    if ([self.moneyF.text intValue] > 0) {
          self.payBtn.backgroundColor = kNavigationBarColor;
    }else {
        if (_tenBtn) {return;}
        self.payBtn.backgroundColor = FColor;
    }
}



- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        UILabel *label = [[UILabel alloc] init];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont boldSystemFontOfSize:14];
        [label sizeToFit];
        label.text = @"";
        _titleLabel = label;
    }
    return _titleLabel;
}

- (UILabel *)contentLabel {
    if (_contentLabel == nil) {
        UILabel *label = [[UILabel alloc] init];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = KColorRGB(165, 167, 170);
        label.font = [UIFont systemFontOfSize:12];
        [label sizeToFit];
        label.text = @"(全部收入暂时归讲师所有)";
        _contentLabel = label;
    }
    return _contentLabel;
}

-(UIButton *)payBtn {
    if (!_payBtn) {
        _payBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _payBtn.titleLabel.font = Font(14);
        [_payBtn setTitle:@"支 付" forState:UIControlStateNormal];
        _payBtn.layer.cornerRadius = 4.0f;
        _payBtn.layer.masksToBounds = YES;
        _payBtn.backgroundColor = FColor;
    }
    return _payBtn;
}

-(UITextField *)moneyF {
    if (!_moneyF) {
        _moneyF =  [[UITextField alloc]init];
        _moneyF.layer.cornerRadius = 4.0;
        _moneyF.backgroundColor = LColor;
        _moneyF.placeholder = @"1 - 500";
        _moneyF.textColor = DColor;
        _moneyF.font = Font(12);
        _moneyF.leftViewMode = UITextFieldViewModeAlways;
        _moneyF.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 14, 35)];
        [_moneyF addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _moneyF;
}

-(UIButton *)getBtnWithTitle:(NSString *)title {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:DColor forState:UIControlStateNormal];
    [btn setTitleColor:KColorFromRGB(0xFB6300) forState:UIControlStateSelected];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = Font(12);
    btn.layer.cornerRadius = 3;
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = DColor.CGColor;
    btn.layer.masksToBounds = YES;
    return btn;
}



- (void)backgroundTap {
    [self.popupController dismiss];
}


@end
