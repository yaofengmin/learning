//
//  MicroClassViewController.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/6.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"
#import "ZJScrollPageViewDelegate.h"
@class MainMicroClassViewController;
@class HomeGameItemModel;
@interface MicroClassViewController : YHBaseViewController<ZJScrollPageViewChildVcDelegate>
@property(nonatomic, strong) HomeGameItemModel *homeItemModel;
@property(nonatomic, weak) MainMicroClassViewController *parentContr;
@end
