//
//  MicroClassSearchViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/16.
//  Copyright © 2016年 Hanks. All rights reserved.
//  微课搜索

#import "MicroClassSearchViewController.h"
#import "MicroClassBannerSegmentModel.h"
#import "MicroClassListCell.h"
#import <MJRefresh.h>

#import "YHBaseWebViewController.h"
#import "BooksDetailViewController.h"
#import "MicroClassDetailViewController.h"


static NSString * const kMicroClassListCellID = @"MicroClassListCell";
@interface MicroClassSearchViewController ()<UISearchBarDelegate,UITableViewDelegate,
UITableViewDataSource>{
    MicroClassVideoListModel * _videoModel;
    NSString *_keyWords;
    NSInteger pageCount;
}
@property (nonatomic, strong)  UISearchBar *searchBar;
@property (strong, nonatomic)  UITableView *tableView;
@property (strong, nonatomic) NSMutableArray<MicroClassVideoModel *> *resultModel;
@property (nonatomic ,strong) DIYTipView *tipView;
@end

@implementation MicroClassSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNav];
    pageCount = 1;
    _resultModel = [NSMutableArray array];
    [self creatTabelView];
}


#pragma mark - 刷新
-(void)loadDataForHeader {
    pageCount = 1;
    [self searchWithKeyWords:_keyWords];
}

#pragma mark - 加载
- (void)loadSearchMoreData {
    pageCount ++ ;
    [self searchWithKeyWords:_keyWords];
}


- (void)searchWithKeyWords:(NSString *)keyWords {
    _keyWords = keyWords;
    NSDictionary *paramDic = @{@"service": GetVideoListByClass,
                               @"cateId":@"0",
                               @"pageIndex": @(pageCount),
                               @"pageSize": @20,
                               @"user_Id":KgetUserValueByParaName(USERID) ==nil ? @"":KgetUserValueByParaName(USERID),
                               @"keyWord":keyWords.length == 0?@"":keyWords};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (pageCount == 1) {
            [self.tableView.mj_header endRefreshing];
            [self.resultModel removeAllObjects];
        }else {
            [self.tableView.mj_footer endRefreshing];
        }
        
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                _videoModel = [MicroClassVideoListModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                
                [self.resultModel addObjectsFromArray:_videoModel.list];
                
                [self.tableView reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
                _videoModel = [MicroClassVideoListModel new];
                
                [self.tableView reloadData];
                
            }
            
        } else {
            _videoModel = [MicroClassVideoListModel new];
            
            [self.tableView reloadData];
        }
        
        if (self.resultModel.count==0) {
            if (!_tipView) {
                _tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                
            }
            [self.tableView addSubview:_tipView];
            [_tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.view);
                make.size.equalTo(CGSizeMake(120, 120));
                
            }];
        }else
        {
            if (_tipView.superview) {
                
                [_tipView removeFromSuperview];
            }
        }
        
        if (_videoModel.total == 0 || self.resultModel.count == _videoModel.total) {
            self.tableView.mj_footer.hidden = YES;
        }else{
            self.tableView.mj_footer.hidden = NO;
        }
        
    }];
    
}


-(void)creatTabelView
{
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 120;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.tableHeaderView = self.searchBar;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassListCellID bundle:nil] forCellReuseIdentifier:kMicroClassListCellID];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadSearchMoreData)];
    self.tableView.mj_footer.hidden = YES;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
}


-(void)setupNav {
    
    self.view.backgroundColor = MainBackColor;
    self.yh_navigationItem.title = @"微课搜索";
    
}


-(UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
        _searchBar.delegate = self;
        _searchBar.placeholder = @"类别/名称";
        UIImage* searchBarBg = [YHJHelp GetImageWithColor:[UIColor whiteColor] andHeight:40.0];
        //设置背景图片
        [_searchBar setBackgroundImage:searchBarBg];
        //设置背景色
        [_searchBar setBackgroundColor:[UIColor whiteColor]];
        _searchBar.returnKeyType = UIReturnKeySearch;
        UITextField * searchField = [self sa_GetSearchTextFiled];
        searchField.font = Font(12);
        searchField.textColor = JColor;
        [self setSearchTextFieldBackgroundColor:NCColor];
    }
    return _searchBar;
}

- (UITextField *)sa_GetSearchTextFiled{
    if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
        return _searchBar.searchTextField;
    }else{
//        UITextField *searchTextField =  [self valueForKey:@"_searchField"];
//        return searchTextField;
        return self.searchBar.subviews.firstObject.subviews.lastObject;
    }
}

- (void)setSearchTextFieldBackgroundColor:(UIColor *)backgroundColor
{
    UIView *searchTextField = nil;
    if (IOS7) {
        searchTextField = [[[self.searchBar.subviews firstObject] subviews] lastObject];
    }else if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
        searchTextField = self.searchBar.searchTextField;
    }
    searchTextField.backgroundColor = backgroundColor;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.resultModel.count;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return [MicroClassListCell cellHeight];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MicroClassListCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassListCellID forIndexPath:indexPath];
    [cell configWithModel:_resultModel[indexPath.row]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MicroClassVideoModel *model = _resultModel[indexPath.row];
    if (model.isVip) {//微课里面 如果 isVip ==1，才需要判断 用户是否vip。 如果 isVip ==0，则不管是不是vip，都能看
        if ([[UserInfoModel getInfoModel].grade integerValue] == 0) {
            @weakify(self);
            [YHJHelp aletWithTitle:@"提示" Message:@"您还不是VIP会员，不能观看" sureTitle:@"立即开通 " CancelTitle:@"忽略 " SureBlock:^{
                @strongify(self);
                BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                [self.navigationController pushViewController:buy animated:YES];
                
            } andCancelBlock:nil andDelegate:self];
            return;
        }
    }
    if (model.isMp3  == MicClassMp3TypeWeb) {//web
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.webUrl andNavTitle:model.videoTitle];
        [self.navigationController pushViewController:web animated:YES];
    }else if (model.isMp3 == MicClassMp3TypeBook){//书籍详情
        BooksDetailViewController *detailVC = [[BooksDetailViewController alloc]init];
        detailVC.videoId = model.videoId;
        [self.navigationController pushViewController:detailVC animated:YES];
    }else{
        MicroClassDetailViewController * targetVC = [[MicroClassDetailViewController alloc] init];
        targetVC.videoId = model.videoId;
        [self.navigationController pushViewController:targetVC animated:YES];
    }
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
    [self.resultModel removeAllObjects];
    pageCount = 1;
    [self searchWithKeyWords:searchBar.text];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
