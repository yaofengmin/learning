//
//  MicroClassDetailVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/7.
//  Copyright © 2016年 Hanks. All rights reserved.
//
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "IQKeyboardManager.h"
#import "HTPlayer.h"
#import "MicroClassDetailVC.h"
#import "RewardViewController.h"
#import "PersonHomeViewController.h"
#import "SegmentTapView.h"
#import "MicroClassSectionView.h"
#import "MicroClassTitleImgBtn.h"
#import "MicroClassListCell.h"
#import "MicroClassAuthorCell.h"
#import "MicroClassDetailTableViewCell.h"
#import "MicroClassReplyCommentCell.h"
#import "CareCollectionModel.h"
#import "MicroClassBannerSegmentModel.h"
#import "DBDataModel.h"
   
static NSString * const kMicroClassReplyCommentCellID = @"MicroClassReplyCommentCell";
static NSString * const kMicroClassRelateVideoCellID = @"MicroClassListCell";
static NSString * const kMicroClassDetailCellID = @"MicroClassDetailTableViewCell";
static NSString * const kMicroClassAuthorCellID = @"MicroClassAuthorCell";
static NSString * const kMicroClassSectionViewID = @"MicroClassSectionView";

@interface MicroClassDetailVC ()
<
SegmentTapViewDelegate,
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate
>


{
    SegmentTapView * _segmentView;
  
    int _currentIndex;
    int _commentId;
    int _commentUserId;
    BOOL _slide;
}

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoViewHeight;

@property (weak, nonatomic) IBOutlet UIView *segmentContainerView;
@property (weak, nonatomic) IBOutlet UIView *btnToolView;
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UIView *videoPlayView;
@property (weak, nonatomic) IBOutlet UITextField *commentTxt;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MicroClassTitleImgBtn *collectBtn;
@property (weak, nonatomic) IBOutlet MicroClassTitleImgBtn *followBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (strong, nonatomic)  MicroClassDetailModel *mainModel;
@property (strong, nonatomic) UIImage *shareImage;


@property (nonatomic, copy) NSArray * sectionTitleArr;
@property (strong, nonatomic)HTPlayer *htPlayer;
@end

@implementation MicroClassDetailVC

#pragma mark - life cricle
- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kHTPlayerFullScreenBtnNotificationKey object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    KPostNotifiCation(@"InterfaceOrientationNotification", @(YES));
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[DBDataModel sharedDBDataModel] addRowData_UserVideoProgressWithTime:[_htPlayer currentTime] videoId:_videoId];
    
    KPostNotifiCation(@"InterfaceOrientationNotification", @(NO));
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpNav];
//
    [self layoutSubviews];
   
    _currentIndex = 0;
    _commentId = 0;
    _commentUserId = 0;
    _slide = NO;
    self.commentTxt.delegate  = self;
    [self selectedIndex:_currentIndex];
//
    [self setUpTableView];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textfeildString:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fullScreenBtnClick:)
                                                 name:kHTPlayerFullScreenBtnNotificationKey object:nil];
    
    [self pullData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        return YES;
    }
    return NO;
}
#pragma mark - lazy init

- (NSArray *)sectionTitleArr {
    
    if (!_sectionTitleArr) {
        _sectionTitleArr = @[@"课程导读", @"作者介绍"];
    }
    return _sectionTitleArr;
}


- (HTPlayer *)htPlayer {
    if (!_htPlayer) {
        [[DBDataModel sharedDBDataModel] createDataTable_UserVideoProgress];
        CGRect rect = CGRectMake(0, KTopHeight, KScreenWidth, _mainModel.isMp3 ? 40 :(KScreenHeight/3.0));
        NSString *videoUrl = self.mainModel.videoUrl;
        _htPlayer = [[HTPlayer alloc]initWithFrame:rect videoURLStr:videoUrl];
        _htPlayer.canPlay = self.mainModel.isVip.integerValue;
        [_htPlayer seekTime:[[DBDataModel sharedDBDataModel] getCurrentTimeWithVideoId:_videoId]];
        [self.view addSubview:_htPlayer];
    }
    return _htPlayer;
}

#pragma mark - tool

- (void)setUpTableView {
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 100)];
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassRelateVideoCellID bundle:nil]
         forCellReuseIdentifier:kMicroClassRelateVideoCellID];
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassDetailCellID bundle:nil]
         forCellReuseIdentifier:kMicroClassDetailCellID];
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassAuthorCellID bundle:nil]
         forCellReuseIdentifier:kMicroClassAuthorCellID];
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassReplyCommentCellID bundle:nil]
         forCellReuseIdentifier:kMicroClassReplyCommentCellID];
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassSectionViewID bundle:nil] forHeaderFooterViewReuseIdentifier:kMicroClassSectionViewID];
    
    
    @weakify(self);
//    [self.tableView.tableFooterView addGestureRecognizer:[[UITapGestureRecognizer alloc]bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//        @strongify(self);
//        [self.view endEditing:YES];
//    }]];
}

- (NSString *)timeFormatted:(int)totalSeconds {
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

- (void)pullData {
    NSMutableDictionary *paramDic = @{@"service": GetVideoInfo,
                                       @"videoId": self.videoId
                                      }.mutableCopy;
    
    if (KgetUserValueByParaName(USERID)) {
        [paramDic setObject:KgetUserValueByParaName(USERID) forKey:@"user_Id"];
    }
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
               
                _mainModel = [MicroClassDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                
                @weakify(self);
                dispatch_async(dispatch_get_main_queue(), ^{
                    @strongify(self);
                 self.shareImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_mainModel.videoLogo]]];
                });
                
                [self.tableView reloadData];
                [self setBtnToolSelect];
                self.htPlayer.alpha = 1;
                if (_mainModel.isMp3) {
                    self.videoViewHeight.constant = 40;
                    [self.segmentContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.top.equalTo(self.videoPlayView.mas_bottom);
                    }];
                }
                
                [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(_segmentView.mas_bottom);
                    make.left.width.equalTo(self.view);
                    make.bottom.equalTo(-49);
                }];
               
                self.yh_navigationItem.title = _mainModel.videoTitle;
                if (_mainModel.videoTitle.length > 10) {
                    self.yh_navigationItem.title = [_mainModel.videoTitle substringToIndex:10];
                }
                
                if (_mainModel.teacherInfo.teacherId.integerValue == 0) {
                    self.collectBtn.bttonW = KScreenWidth;
                    [self.collectBtn mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.center.equalTo(self.btnToolView);
                    }];
                    
                    [self.followBtn mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.size.equalTo(CGSizeMake(0, 49));
                    }];
                }
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];

}

#pragma mark - Button Functions
- (IBAction)collectVideo:(MicroClassTitleImgBtn *)sender {
    
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        NSString *service;
        
        if (sender.selected) { service = DeleteUserCollection; }
        else { service = AddCollectoins; }
        
        NSDictionary *paramDic = @{@"service": service,
                                   @"pId": self.videoId,
                                   @"ctype": @(1),
                                   @"userId": KgetUserValueByParaName(USERID)};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    
                    sender.selected = !sender.selected;
                    
                   
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    
                }
                
            }
        }];

    }

}

- (void)careAuthor:(UIButton *)sender {
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {

        NSString *service = sender.selected ? DeleteUserFollow : AddFollows;

        NSDictionary *paramDic = @{@"service": service,
                                   @"pId": _mainModel.teacherInfo.teacherId,
                                   @"ctype": @(1),
                                   @"userId": KgetUserValueByParaName(USERID)};

        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功

                    sender.selected = !sender.selected;
                    
                    [_mainModel.teacherInfo resetFollowState:sender.selected];
             
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息

                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];

                }
                
            }
        }];

    }
}

//打赏逻辑
- (IBAction)followAuthor:(MicroClassTitleImgBtn *)sender {
    
    RewardViewController *pop = [[RewardViewController alloc]init];
    pop.videoId = _mainModel.videoId;
    STPopupController *popVericodeController = [[STPopupController alloc] initWithRootViewController:pop];
    [popVericodeController presentInViewController:self];
   
}

- (IBAction)sendComment:(UIButton *)sender {

    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        [self.view endEditing:YES];
        
        NSDictionary *paramDic = @{@"service": AddComment,
                                   @"userId": KgetUserValueByParaName(USERID),
                                   @"toId": self.videoId,
                                   @"ctype": @(1),
                                   @"pId": @(_commentId),
                                   @"toUserId": @(_commentUserId),
                                   @"content": self.commentTxt.text};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    
                    _commentUserId = 0;
                    _commentId = 0;
                    
                    [self pullData];
                    self.commentTxt.text = @"";
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    
                }
                
            }
        }];
    }
    
}

- (void)setBtnToolSelect {
    self.collectBtn.selected = [_mainModel getCollectionState];
}

#pragma mark - Navigation
- (void)setUpNav {
    
  
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_share"]
                                                                                 style:0
                                                                               handler:^(id send) {
                                                                                   @strongify(self);
       [[YHJHelp shareInstance]  showShareInController:self andShareURL:[NSString stringWithFormat:@"%@?inviteCode=%@",APPSHARELINK,KgetUserValueByParaName(MYINVITECODE)] andTitle:self.yh_navigationItem.title andShareText:self.mainModel.videoDesc andShareImage:self.shareImage isShowStudyGroup:NO isDIYUrl:NO];
    }];
    
}

#pragma mark Notification

- (void)textfeildString:(NSNotification *)notification {
    
   if (self.commentTxt.text.length>0) {
        self.commentBtn.enabled = YES;
        [self.commentBtn setBackgroundColor:MainBtnColor];
    } else {
        self.commentBtn.enabled = NO;
        [self.commentBtn setBackgroundColor:MainTextGrayColor];
    }
}

- (void)fullScreenBtnClick:(NSNotification *)notice{

    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationLandscapeLeft: case UIDeviceOrientationLandscapeRight:
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationPortrait]
                                        forKey:@"orientation"];
            break;
        case UIDeviceOrientationPortrait: case UIDeviceOrientationPortraitUpsideDown:
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft]
                                        forKey:@"orientation"];
            break;
            
        default:
            break;
    }
    
}

#pragma mark - SegmentTapViewDelegate
-(void)selectedIndex:(NSInteger)index {
    
    switch (index) {
        case 0:
            self.btnToolView.hidden = NO;
            self.commentView.hidden = YES;
            [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(-49);
            }];
            break;
        case 1:
            self.btnToolView.hidden = YES;
            self.commentView.hidden = NO;
            [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(-40);
            }];
            break;
        default:
            self.btnToolView.hidden = YES;
            self.commentView.hidden = YES;
            [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(0);
            }];
            break;
    }
    
    _currentIndex = (int)index;
    [self.tableView reloadData];
}

#pragma mark - MicroClassReplyCommentCell
- (void)ReplyCommentCellToPersonHomeVC:(UIButton *)sender {
    
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        PersonHomeViewController * targetVC = [[PersonHomeViewController alloc] init];
        
        targetVC.userId = (int)sender.tag;
        
        [self.navigationController pushViewController:targetVC animated:YES];
    }
    
    
}

#pragma mark - Layout
- (void)layoutSubviews {
    
    _segmentView = ({
        
        SegmentTapView * view = [[SegmentTapView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, SEGMENTHEIGHT)];
        view.delegate = self;
        view.dataArray = @[@"详情", @"评论", @"相关视频"];
        view.textNomalColor = DColor;
        view.textSelectedColor = MainBtnColor;
        view.titleFont = 16.0;
        view.lineColor = [UIColor whiteColor];
        [self.segmentContainerView addSubview:view];
        view;
    });
 
    
    [self.collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.btnToolView);
        make.size.equalTo(CGSizeMake(KScreenWidth/2, 49));
        make.centerY.equalTo(self.btnToolView);
    }];
    
    
    [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.btnToolView);
        make.size.equalTo(CGSizeMake(KScreenWidth/2, 49));
        make.centerY.equalTo(self.btnToolView);
    }];
        
}

#pragma mark - Rotation
- (void)viewWillTransitionToSize:(CGSize)mySize withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         if (mySize.width > mySize.height) {

             [self sc_setNavigationBarHidden:YES animated:YES];
             [_htPlayer toFullScreenWithInterfaceOrientation:UIInterfaceOrientationLandscapeLeft];
             
         } else {

              [self sc_setNavigationBarHidden:NO animated:YES];
             [_htPlayer reductionWithInterfaceOrientation:self.videoPlayView];
         }
         
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
         
     }];
    
    [super viewWillTransitionToSize:mySize withTransitionCoordinator:coordinator];
}




#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    switch (_currentIndex) {
        case 0:
            return 2;
            break;
        case 1:
            return _mainModel.Comments.count;
            break;
        case 2:
            return 1;
            break;
            
        default:
            break;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (_currentIndex) {
        case 0:
            return 1;
            break;
        case 1:
        {
            return _mainModel.Comments[section].replay_commnets.count + 1;
            break;
        }
        case 2:
            return _mainModel.linkVideos.count;
            break;
            
        default:
            break;
    }
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_currentIndex == 0) {
        
        if (indexPath.section == 0) {
            MicroClassDetailTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassDetailCellID forIndexPath:indexPath];
            
            [cell configWithModel:_mainModel];
            
            return cell;
        }
        
        if (indexPath.section == 1) {
            MicroClassAuthorCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassAuthorCellID forIndexPath:indexPath];
            
            [cell configWithTeacherModel:_mainModel.teacherInfo];
            
            return cell;
        }
        
    }
    
    if (_currentIndex == 1) {
        
        if (indexPath.row == 0) {
            MicroClassAuthorCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassAuthorCellID forIndexPath:indexPath];
            [cell configWithCommentModel:_mainModel.Comments[indexPath.section]];
            return cell;
        }
        
        MicroClassReplyCommentCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassReplyCommentCellID forIndexPath:indexPath];
        
        MicroClassCommentModel * model = _mainModel.Comments[indexPath.section];
        
        [cell configWithModel:model.replay_commnets[indexPath.row - 1]];
        
        return cell;
    }
    
    if (_currentIndex == 2) {
        MicroClassListCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassRelateVideoCellID forIndexPath:indexPath];
        
        [cell configWithModel:_mainModel.linkVideos[indexPath.row]];
        
        return cell;
    }
    
    
    MicroClassListCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassRelateVideoCellID forIndexPath:indexPath];
        
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_currentIndex == 0) {
        
        if (indexPath.section == 0) {
            return [MicroClassDetailTableViewCell cellHeightWithModel:_mainModel];
        }
        
        if (indexPath.section == 1) {
            return [MicroClassAuthorCell cellHeightWithTeacherModel:_mainModel.teacherInfo];
        }
    
    }
    
    if (_currentIndex == 1) {
        
        if (indexPath.row == 0) {
            return [MicroClassAuthorCell cellHeightWithCommentModel:_mainModel.Comments[indexPath.section]];
        }
        
        return [MicroClassReplyCommentCell cellHeightWithReplyCommentModel:(MicroClassReplyCommentModel *)_mainModel.Comments[indexPath.section].replay_commnets[indexPath.row - 1]];

    }
    
    return [MicroClassListCell cellHeight];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_currentIndex == 1) {
        
        if (indexPath.row != _mainModel.Comments[indexPath.section].replay_commnets.count) {
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset: UIEdgeInsetsMake(0, 58, 0, 0)];
            }
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins: UIEdgeInsetsMake(0, 58, 0, 0)];
            }
            
            return;
        }
        
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsZero];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (_currentIndex != 0) { return 0.01f; }
    else { return 20; }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (_currentIndex != 0) { return nil; }
    
    MicroClassSectionView * view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kMicroClassSectionViewID];
    
    [view setTitle:self.sectionTitleArr[section]];
        
    return view;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_currentIndex == 1) {
        
        if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
            if (indexPath.row == 0) {
                _commentUserId = _mainModel.Comments[indexPath.section].userId;
            } else {
                _commentUserId = _mainModel.Comments[indexPath.section].replay_commnets[indexPath.row - 1].userId;
            }
            
            _commentId = _mainModel.Comments[indexPath.section].comContentId;
            
            [self.commentTxt becomeFirstResponder];
        }
    }
    
    if (_currentIndex == 2) {
        
        MicroClassDetailVC * targetVC = [[MicroClassDetailVC alloc] init];
        
        targetVC.videoId = _mainModel.linkVideos[indexPath.row].videoId;
        
        [self.navigationController pushViewController:targetVC animated:YES];
        
        return;
    }
}


-(BOOL)shouldAutorotate
{
    return YES;
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    
    return UIInterfaceOrientationPortraitUpsideDown;
}


@end
