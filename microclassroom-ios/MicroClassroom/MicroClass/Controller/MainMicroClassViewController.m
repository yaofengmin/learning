//
//  MainMicroClassViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/16.
//  Copyright © 2016年 Hanks. All rights reserved.
// 首页重构容器

#import "MainMicroClassViewController.h"
#import "ZJScrollPageView.h"
#import "ZQHomePullDownView.h"
#import "HomeGameItemTool.h"
#import "HomeGameItemModel.h"
#import "MicroClassSearchViewController.h"
#import "YHNavgationController.h"
#import "MicroClassBannerSegmentModel.h"
#import "MicroClassViewController.h"
#import "DIYHeadSearchButton.h"//搜索点击进入到下一个界面的
#import "AppDelegate.h"

@interface MainMicroClassViewController ()<UIScrollViewDelegate,ZJScrollPageViewDelegate,ZQHomePullDownViewDelegate> {
    MicroClassBannerSegmentModel * _mainModel;
}
@property (nonatomic,strong) HomeGameItemModel   *currentItem;
@property (nonatomic,strong) ZJScrollSegmentView *segmentView;
@property (nonatomic,strong) UIView              *segBackView;
@property (nonatomic,strong) ZJContentView       *contentView;
@property (nonatomic,strong) ZQHomePullDownView  *itemContentView;
@property (nonatomic,strong) NSMutableArray      *topItemList;
@property (nonatomic,strong) NSMutableArray      *allItemArr;
@property (nonatomic,strong) NSMutableArray      *showControllerArr;
@property (nonatomic,strong) UIButton            *pullDownBtn;

@property (nonatomic ,strong) DIYHeadSearchButton *searchV;


@end

@implementation MainMicroClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //    [self setUpNav];
    [self creatSearchView];
    [self updateLocation];
    // localData
    [self configData];
    // UI
    [self configSegMent];
    [self requestHomeListTitel];
    KAddobserverNotifiCation(@selector(requestHomeListTitel), NOTIGICATIONFINDVVIEWRELOAD);
    
}

#pragma mark === 搜索布局
-(void)creatSearchView
{
    _searchV = [[NSBundle mainBundle] loadNibNamed:@"DIYHeadSearchButton" owner:self options:nil].firstObject;
    [self.yh_navigationBar addSubview:_searchV];
    [_searchV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.yh_navigationBar.mas_bottom).offset(-5);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth - 20, 30));
        make.centerX.mas_equalTo(self.yh_navigationBar);
    }];
    
    UITapGestureRecognizer *searchTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(searchAction:)];
    [_searchV addGestureRecognizer:searchTap];
}
#pragma mark === 搜索界面
-(void)searchAction:(UITapGestureRecognizer *)tap
{
    MicroClassSearchViewController *search = [[MicroClassSearchViewController alloc]init];
    YHNavgationController *navsearch  =[[YHNavgationController alloc]initWithRootViewController:search];
    navsearch.modalPresentationStyle = 0;
    [self presentViewController:navsearch animated:YES completion:nil];
}
- (void)setUpNav {
    self.yh_navigationItem.titleView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nav_logo"]];
    self.yh_navigationItem.leftBarButtonItem = [[YHBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"search"] style:0 handler:^(id send) {
        
        MicroClassSearchViewController *search = [[MicroClassSearchViewController alloc]init];
        YHNavgationController *navsearch  =[[YHNavgationController alloc]initWithRootViewController:search];
        navsearch.modalPresentationStyle = 0;
        [self presentViewController:navsearch animated:YES completion:nil];
    }];
}

//MARK: 配置数据
- (void)configData {
    
    _allItemArr =@[].mutableCopy;
    //获取本地所的数据
    NSMutableArray *tempArr = [[HomeGameItemTool getItemAllList] objectForKey:KAlllistArrKey];
    
    _topItemList = [[HomeGameItemTool getItemAllList] objectForKey:KTopListArrKey]; //顶部数据
    if (_topItemList.count == 0) {
        
        _topItemList = [HomeGameItemTool getDefultDataWithAllList:tempArr].mutableCopy;
        [_allItemArr addObjectsFromArray:tempArr];
        
        //保存原始数据
        [HomeGameItemTool saveModelAllList:_allItemArr];
        [HomeGameItemTool saveModelTopList:_topItemList];
        //        [HomeGameItemTool saveModelTopList:newTopArr];
        
    }else{
        //全部数据
        [_allItemArr addObjectsFromArray:tempArr];
        //用新数据更新顶部旧的数据
        _topItemList = [self updateTopDataWithOldTopData:_topItemList andAllSourceArr:_allItemArr];
    }
}


#pragma mark === ZQHomePullDownViewDelegate ===
- (void)changedWithSelectedItem:(HomeGameItemModel *)selected andTopSourceArr:(NSArray<HomeGameItemModel*>*)sourceArr {
    
    if (![[self throughItemListBackName:sourceArr] isEqualToArray:[self throughItemListBackName:self.topItemList]]) { //改变和点击
        self.topItemList = [NSMutableArray arrayWithArray:sourceArr]; //改变外面显示的数据源
        [self.itemContentView itemRespondFromListBarClickWithItem:selected];
        self.segmentView.selectItemTitle = selected.cateName;
        [self.segmentView reloadTitlesWithNewTitles:[self throughItemListBackName:sourceArr]];
        [self.contentView reload];
        [self.segmentView setSelectedIndex:[self findIndexOfListsWithModel:selected andSourceArr:sourceArr] animated:YES];
        
        [HomeGameItemTool saveTopChangeList:sourceArr];
        
    }else {
        
        if (![_currentItem.cateId isEqualToString:selected.cateId]) { //单纯的点击
            
            [self updateSegSourceWithModel:selected andSourceArr:sourceArr];
        }
    }
    
}

#pragma mark ===== segement刷新方法 ====

- (void)updateSegSourceWithModel:(HomeGameItemModel *)model andSourceArr:(NSArray *)sourceArr {
    
    self.topItemList = [NSMutableArray arrayWithArray:sourceArr]; //改变外面显示的数据源
    [self.itemContentView itemRespondFromListBarClickWithItem:model];
    self.segmentView.selectItemTitle = model.cateName;
    [self.segmentView setSelectedIndex:[self findIndexOfListsWithModel:model andSourceArr:sourceArr] animated:YES];
}



-(void)requestHomeListTitel {
    NSDictionary *paramDic = @{@"service": GetIndexList,@"user_Id":KgetUserValueByParaName(USERID)};
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                _mainModel = [MicroClassBannerSegmentModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                if (_mainModel.video_class.count > 0) {
                    //更新网络数据
                    [HomeGameItemTool saveModelAllList:[HomeGameItemModel mj_objectArrayWithKeyValuesArray:_mainModel.video_class]];
                    //通过网络数据配置需要显示的数据
                    [self configData];
                    
                    //刷新下拉view 数据
                    [self.itemContentView upDateDatasWithAllTitleItem:_allItemArr andTopItem:_topItemList];
                    //选中第0个数据
                    [self.itemContentView itemRespondFromListBarClickWithItem:_topItemList[0]];
                    //刷新seg 数据
                    [self.segmentView reloadTitlesWithNewTitles:[self throughItemListBackName:self.topItemList]];
                    //刷新content
                    [self.contentView reload];
                }
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
}



- (UIView *)segBackView {
    if (!_segBackView) {
        _segBackView = [[UIView alloc]initWithFrame:CGRectMake(0, KTopHeight, KScreenWidth, kListBarH)];
        _segBackView.backgroundColor = [UIColor whiteColor];
    }
    return _segBackView;
}



#pragma mark ===ZJScrollPageViewDelegate===
- (NSInteger)numberOfChildViewControllers {
    
    return self.topItemList.count;
}

- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    
    HomeGameItemModel *model = [self.topItemList objectAtIndex:index];
    _currentItem = model;
    return [self throughItemBackController:model];
}


#pragma mark === 查找 或者创建controller ===
- (UIViewController<ZJScrollPageViewChildVcDelegate> *)throughItemBackController:(HomeGameItemModel *)homeModel {
    
    for (MicroClassViewController *contr in self.showControllerArr) {
        if ([contr.homeItemModel.cateId isEqualToString:homeModel.cateId]) {
            return contr;
        }
    }
    
    MicroClassViewController *viewContr = [[MicroClassViewController alloc]init];
    viewContr.homeItemModel = homeModel;
    viewContr.parentContr = self;
    [self.showControllerArr addObject:viewContr];
    return viewContr;
}


#pragma mark == 查找当然model 在目标数组的位置===

- (NSInteger)findIndexOfListsWithModel:(HomeGameItemModel *)model andSourceArr:(NSArray *)arr{
    for (int i =0; i < arr.count; i++) {
        if ([model.cateId isEqualToString:[arr[i] cateId]]) {
            return i;
        }
    }
    return 0;
}


#pragma mark  ===配置segement===
- (void)configSegMent {
    
    //MARK:  创建UI
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    //显示滚动条
    style.showLine = YES;
    // 颜色渐变
    style.gradualChangeTitleColor = YES;
    style.segmentHeight = kListBarH;
    style.scrollLineColor = kNavigationBarColor;
    style.selectedTitleColor = kNavigationBarColor;
    style.scrollLineHeight = 3.0f;
    style.titleFont        = [UIFont systemFontOfSize:16];
    style.titleMargin      = 35.0f;
    style.scrollLineExtraWidth = -15;
    //    style.scrollLineExtraWidth = style.titleMargin;
    style.overstriking    = YES;
    style.showExtraButton = YES;
    style.extraBtnBackgroundImageName = @"ic_quanbufenlei";
    
    __weak typeof(self) weakSelf = self;
    ZJScrollSegmentView *segment = [[ZJScrollSegmentView alloc] initWithFrame:CGRectMake(0, 0,style.segmentWidth, kListBarH) segmentStyle:style titles:[self throughItemListBackName:self.topItemList] titleDidClick:^(UILabel *label, NSInteger index) {
        
        [weakSelf.contentView setContentOffSet:CGPointMake(weakSelf.contentView.bounds.size.width * index, 0.0) animated:YES];
        
    }];
    
    self.segmentView = segment;
    segment.backgroundColor = [UIColor whiteColor];
    segment.extraBtnOnClick = ^(UIButton *extraBtn){
        [self.itemContentView itemRespondFromListBarClickWithItem:_currentItem];
        self.itemContentView.hidden = NO;
        self.itemContentView.arrow.selected = NO;
        [self.itemContentView arrowBtnClick];
    };
    
    ZJContentView *content = [[ZJContentView alloc] initWithFrame:CGRectMake(0.0, 0.0, KScreenWidth, KScreenHeight) segmentView:self.segmentView parentViewController:self delegate:self];
    self.contentView = content;
    
    [self.view addSubview:self.contentView];
    [self.view addSubview:self.segBackView];
    [self.segBackView addSubview:segment];
    
    UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth - kListBarH, 0, 3, kListBarH)];
    lineImage.image = [UIImage imageNamed:@"line"];
    [self.segBackView addSubview:lineImage];
    
    _itemContentView = [[ZQHomePullDownView alloc]initWithFrame:CGRectMake(0, KTopHeight, KScreenWidth, KScreenHeight -KTopHeight) topTotles:_topItemList andAllTitles:_allItemArr parentViewController:self delegate:self];
    _itemContentView.hidden = YES;
    [self.view addSubview:_itemContentView];
    [self.view bringSubviewToFront:_itemContentView];
    
}


#pragma mark === 更新顶部数据====
- (NSMutableArray<HomeGameItemModel*> *)updateTopDataWithOldTopData:(NSArray<HomeGameItemModel*> *)oldTop andAllSourceArr:(NSArray<HomeGameItemModel*> *)allArr {
    
    NSMutableArray *newTopArr = @[].mutableCopy;
    for (HomeGameItemModel *new in allArr) {//每次都将 < 0 的 加进去
        if ([new.cateId integerValue] < 0) {
            [newTopArr addObject:new];
        }
    }
    //之前加入到顶部的,并且大于0 的加进去,防止小于0 的加两遍
    for (HomeGameItemModel *old in oldTop) {
        for (HomeGameItemModel *new in allArr) {
            if ([old.cateId isEqualToString:new.cateId] && [new.cateId integerValue] > 0) {
                [newTopArr addObject:new];
            }
        }
    }

    return newTopArr;
}

#pragma mark === 判断数组里面是否包含目标对象===
- (BOOL)judgeArr:(NSArray<HomeGameItemModel*> *)itemArr includeModel:(HomeGameItemModel *)model {
    for (HomeGameItemModel *judgeModel in itemArr) {
        if ([judgeModel.cateId isEqualToString:model.cateId]) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark == 返回名字数组 ===
- (NSMutableArray *)throughItemListBackName:(NSArray<HomeGameItemModel*> *)itemListArr {
    
    NSMutableArray *muArr =@[].mutableCopy;
    for (HomeGameItemModel *model in itemListArr) {
        
        [muArr addObject:model.cateName];
    }
    
    return muArr;
}


- (NSMutableArray *)showControllerArr{
    if (!_showControllerArr) {
        _showControllerArr = @[].mutableCopy;
    }
    return _showControllerArr;
}

-(BOOL)shouldAutorotate {
    
    return  NO;
}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

- (void)dealloc
{
    KRemoverNotifi(NOTIGICATIONFINDVVIEWRELOAD);
}


@end
