//
//  MicroClassSectionView.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/7.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MicroClassSectionView.h"

@interface MicroClassSectionView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation MicroClassSectionView

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
}


@end
