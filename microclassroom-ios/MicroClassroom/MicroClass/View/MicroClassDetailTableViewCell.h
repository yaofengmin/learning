//
//  MicroClassDetailTableViewCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/7.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseTableViewCell.h"

@class MicroClassDetailModel;

@interface MicroClassDetailTableViewCell : YHBaseTableViewCell
- (void)configWithModel:(MicroClassDetailModel *)model;
+ (CGFloat)cellHeightWithModel:(MicroClassDetailModel *)model;
@end
