//
//  UserInfoNormalCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/16.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoNormalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *desLabel;


- (void)configureCellWithStr:(NSString *)str;

+ (CGFloat)cellHeightWithStr:(NSString *)str;
@end
