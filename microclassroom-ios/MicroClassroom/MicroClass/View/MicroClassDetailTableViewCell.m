//
//  MicroClassDetailTableViewCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/7.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MicroClassDetailTableViewCell.h"

#import "MicroClassBannerSegmentModel.h"
#import "NSString+Size.h"

@interface MicroClassDetailTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *watchIcon;
@property (weak, nonatomic) IBOutlet UILabel *keyWordLabel;

@end

@implementation MicroClassDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}

- (void)configWithModel:(MicroClassDetailModel *)model {
    self.titleLabel.text = model.videoTitle;
    if(IsEmptyStr(model.startTime)){
         self.dateLabel.text =  [NSString stringWithFormat:@"上传时间：%@",model.addTime];
    }else{
        self.dateLabel.text =  [NSString stringWithFormat:@"直播时间：%@ 至 %@",model.startTime,model.endTime];
    }

    self.watchIcon.hidden = [model.videoLaud intValue] == 0;
    self.countLabel.hidden = [model.videoLaud intValue] == 0;
    self.countLabel.text = model.videoLaud;
    self.contentLabel.text = model.videoDesc;
    self.keyWordLabel.text = model.videoKeyword;
    
}

+ (CGFloat)cellHeightWithModel:(MicroClassDetailModel *)model {
    return 73 + 20 + ceil([model.videoDesc sizeWithFontSize:14.0 andRectSize:CGSizeMake(KScreenWidth - 20, 0)].height);

}

@end
