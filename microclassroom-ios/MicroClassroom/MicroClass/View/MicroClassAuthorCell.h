//
//  MicroClassAuthorCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/9.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YHBaseTableViewCell.h"

@class MicroClassCommentModel;
@class MicroClassTeacherModel;
@class CareModel;
@interface MicroClassAuthorCell : YHBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIButton *careBtn;
- (void)configWithCommentModel:(MicroClassCommentModel *)model;
- (void)configWithTeacherModel:(MicroClassTeacherModel *)model;
- (void)configWithCareModel:(CareModel *)model;

+ (CGFloat)cellHeightWithTeacherModel:(MicroClassTeacherModel *)model;
+ (CGFloat)cellHeightWithCommentModel:(MicroClassCommentModel *)model;
@end
