//
//  MicroClassReplyCommentCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/11.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseTableViewCell.h"

@class MicroClassReplyCommentModel;

@interface MicroClassReplyCommentCell : YHBaseTableViewCell
+ (CGFloat)cellHeightWithReplyCommentModel:(MicroClassReplyCommentModel *)model;
@end
