//
//  MicroClassTitleImgBtn.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/15.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MicroClassTitleImgBtn.h"

@implementation MicroClassTitleImgBtn


-(CGRect)imageRectForContentRect:(CGRect)contentRect {
    return CGRectMake(0, 0, _bttonW == 0 ? KScreenWidth/2 :_bttonW, 49*2/3);
}


-(CGRect)titleRectForContentRect:(CGRect)contentRect {
    return CGRectMake(0, 49*2/3, _bttonW == 0 ? KScreenWidth/2 :_bttonW, 49/3);
}

- (void)awakeFromNib {
    
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.imageView.contentMode =  UIViewContentModeScaleAspectFit;
    [super awakeFromNib];
}


@end
