//
//  MicroClassReplyCommentCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/11.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MicroClassReplyCommentCell.h"

#import "MicroClassBannerSegmentModel.h"
#import "NSString+Size.h"

@interface MicroClassReplyCommentCell ()

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *fromBtn;
@property (weak, nonatomic) IBOutlet UIButton *toBtn;

@end

@implementation MicroClassReplyCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.fromBtn addTarget:nil
                     action:@selector(ReplyCommentCellToPersonHomeVC:)
           forControlEvents:UIControlEventTouchUpInside];
    
    [self.toBtn addTarget:nil
                   action:@selector(ReplyCommentCellToPersonHomeVC:)
         forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configWithModel:(MicroClassReplyCommentModel *)cellModel {
    
    [self.fromBtn setTitle:cellModel.nickName forState:UIControlStateNormal];
    [self.toBtn setTitle:[NSString stringWithFormat:@"%@ ：",cellModel.toNickName]
                forState:UIControlStateNormal];
    
    self.fromBtn.tag = cellModel.userId;
    self.toBtn.tag = cellModel.toUserId;
    
    NSString *commentStr = [NSString stringWithFormat:@"%@ 回复 %@ ：%@", cellModel.nickName, cellModel.toNickName, cellModel.comcontent];
    
    NSUInteger len1 = [[NSString stringWithFormat:@"%@ 回复 %@ ：", cellModel.nickName, cellModel.toNickName] length];
    NSUInteger len2 = (commentStr.length - len1);
                       
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:commentStr];
                       
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor whiteColor] range:NSMakeRange(0, len1)];
    
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor lightGrayColor] range:NSMakeRange(len1, len2)];

    self.contentLabel.attributedText = attributedStr;
}

+ (CGFloat)cellHeightWithReplyCommentModel:(MicroClassReplyCommentModel *)model {
    
    NSString * content = [NSString stringWithFormat:@"%@ 回复 %@:%@", model.nickName, model.toNickName, model.comcontent];
    
    CGFloat height = 40 + ceil([content sizeWithFontSize:13.0 andRectSize:CGSizeMake(KScreenWidth - 66, 0)].height);
    
    return height > 55 ? height: 55;
}

@end
