//
//  MicroClassAuthorCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/9.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MicroClassAuthorCell.h"

#import "MicroClassBannerSegmentModel.h"
#import "CareCollectionModel.h"
#import "NSString+Size.h"
#import "PersonHomeViewController.h"
#import "UIImageView+AFNetworking.h"

@interface MicroClassAuthorCell ()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (assign, nonatomic) int  userid;


@end

@implementation MicroClassAuthorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
    self.avatarImageView.userInteractionEnabled = YES;
 
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.careBtn addTarget:nil
                     action:@selector(careAuthor:)
           forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)configWithTeacherModel:(MicroClassTeacherModel *)model {
    _userid = model.userId;
    self.titleLabel.text = model.teacherName;
    self.contentLabel.text = model.remark;
    self.dateLabel.text = @"";
    self.avatarImageView.hidden = NO;
    
    self.careBtn.selected = [model getFollowState];
    self.careBtn.hidden = NO;
    
    [self setAvatar:model.photo];
    if (model.teacherId.integerValue == 0) {
        self.careBtn.hidden = YES;
       
    }else{
//        @weakify(self);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleAvar)];
        [self.avatarImageView addGestureRecognizer:tap];
        
//        [self.avatarImageView addGestureRecognizer: [[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//            @strongify(self);
//
//            PersonHomeViewController *per = [[PersonHomeViewController alloc]init];
//            per.userId = self.userid;
//            [self.viewController.navigationController pushViewController:per animated:YES];
//
//        }]];
    }

}
-(void)handleAvar
{
    
    PersonHomeViewController *per = [[PersonHomeViewController alloc]init];
    per.userId = self.userid;
    [self.viewController.navigationController pushViewController:per animated:YES];
}

- (void)configWithCommentModel:(MicroClassCommentModel *)model {
    _userid = model.userId;
    self.titleLabel.text = model.nickName;
    self.contentLabel.text = model.comcontent;
    self.dateLabel.text = model.comTime;
    
    self.avatarImageView.hidden = NO;
    self.careBtn.hidden = YES;
    [self setAvatar:model.photo];
   

}

- (void)configWithCareModel:(CareModel *)model {
    _userid = model.userId;
    self.titleLabel.text = model.userName;
    self.contentLabel.text = model.personalSign;
    self.dateLabel.text = @"";
    
    self.avatarImageView.hidden = NO;
    self.careBtn.hidden = YES;
    [self setAvatar:model.pic];
    
}

- (void)setAvatar:(NSString *)avatarStr {

    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:avatarStr]];
}


+ (CGFloat)cellHeightWithTeacherModel:(MicroClassTeacherModel *)model {
    return 80 + ceil([model.remark sizeWithFontSize:14.0 andRectSize:CGSizeMake(KScreenWidth - 66, 0)].height);
}

+ (CGFloat)cellHeightWithCommentModel:(MicroClassCommentModel *)model {
    
    CGFloat height = 60 + ceil([model.comcontent sizeWithFontSize:14.0 andRectSize:CGSizeMake(KScreenWidth - 66, 0)].height);
    
    return height > 80 ? height: 80;
}

@end
