//
//  MicroClassListCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/6.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YHBaseTableViewCell.h"

@class CollectionModel;
@interface MicroClassListCell : YHBaseTableViewCell
- (void)configWithCollectionModel:(CollectionModel *)model;
@end
