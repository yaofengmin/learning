//
//  ZQHomePullDownView.m
//  ZQNewHomeTopDemo
//
//  Created by Hanks on 16/8/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "ZQHomePullDownView.h"
#import "BYDeleteBar.h"
#import "BYDetailsList.h"
#import <RDVTabBarController.h>

@interface ZQHomePullDownView()<BYDetailsListViewDelegate>

@property (nonatomic,strong) BYDeleteBar               *deleteBar;
@property (strong,nonatomic) BYDetailsList             *detailListView;
@property (nonatomic,weak  ) UIViewController          *parentViewController;

/**
 *  所有能够显示的item
 */
@property (nonatomic,strong) NSMutableArray            *allTitleArr;
/**
 *  顶部ItemList
 */
@property (nonnull,strong  ) NSArray                   *topItemList;

/**
 *  底部itemList
 */
@property (nonatomic,strong) NSMutableArray            *bottomList;
@end

@implementation ZQHomePullDownView

- (instancetype)initWithFrame:(CGRect)frame topTotles:(NSArray<HomeGameItemModel *> *)topItems andAllTitles:(NSArray<HomeGameItemModel *>*) allItems parentViewController:(UIViewController *)parentViewController  delegate:(id<ZQHomePullDownViewDelegate>) delegate {
    if (self = [super initWithFrame:frame]) {
        
      self.topItemList          = topItems;
      self.allTitleArr          = allItems.mutableCopy;
      self.pullDelegate         = delegate;
      self.parentViewController = parentViewController;
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(TopClick:)];
        [self.parentViewController.navigationController.navigationBar addGestureRecognizer:tap];
        [self configData];
        [self commonInit];
    }
    
    return self;
}


- (void)upDateDatasWithAllTitleItem:(NSArray<HomeGameItemModel*>*)allItem andTopItem:(NSArray<HomeGameItemModel*>*) topItem {
    
    self.allTitleArr          = nil;
    self.topItemList          = topItem;
    self.allTitleArr          = allItem.mutableCopy;
    [self configData];
     self.detailListView.listAll = [NSMutableArray arrayWithObjects:self.topItemList,self.bottomList,nil];
}

#pragma mark === 分离数据 ==

- (void)configData {
    self.bottomList = nil;
     self.bottomList = [self initializeArr:self.allTitleArr.mutableCopy removeObjectsInArray:self.topItemList];
}

- (void)commonInit {
    
    [self addSubview:self.deleteBar];
    [self.deleteBar addSubview:self.arrow];
    [self addSubview:self.detailBackView];
    [self.detailBackView addSubview:self.detailListView];
}


#pragma mark == 承载itemView ===
- (BYDetailsList *)detailListView{
    if (!_detailListView) {
        _detailListView = [[BYDetailsList alloc]initWithFrame:CGRectMake(0, -KScreenHeight , KScreenWidth, KScreenHeight - KTopHeight -kListBarH)];
        _detailListView.listAll = [NSMutableArray arrayWithObjects:_topItemList,_bottomList,nil];
        _detailListView.detailDelegate = self;
    }
    return _detailListView;
}



#pragma mark ==detailList 背景view ===
- (UIView *)detailBackView {
    if (!_detailBackView) {
        _detailBackView = [[UIView alloc]initWithFrame:CGRectMake(0,kListBarH, KScreenWidth, self.viewHeight - kListBarH)];
        _detailBackView.clipsToBounds = YES;
    }
    return _detailBackView;
}



#pragma mark == 顶部操作栏 ==
- (BYDeleteBar *)deleteBar {
    if (!_deleteBar) {
        _deleteBar = [[BYDeleteBar alloc] initWithFrame:CGRectMake(0,0, KScreenWidth, kListBarH)];
    }
    return _deleteBar;
}

#pragma mark=== 叉叉按钮 ===
- (BYArrow *)arrow {
    __weak typeof(self) unself = self;
    if (!_arrow) {
        _arrow = [[BYArrow alloc] initWithFrame:CGRectMake(KScreenWidth - kListBarH,0, kListBarH, kListBarH)];
        _arrow.arrowBtnClick = ^(){
            [unself.detailListView detailArrowBtnClick]; //回传数据
            [unself arrowBtnClick]; //执行动画
        };
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 3, kListBarH)];
        lineImage.image = [UIImage imageNamed:@"line"];
        [_arrow addSubview:lineImage];
    }
    return _arrow;
}

#pragma mark == 顶部标题栏点击==
- (void)TopClick:(UITapGestureRecognizer *)sender {
    if (self.deleteBar.alpha == 1) {
        self.arrow.selected = NO;
        [self.detailListView detailArrowBtnClick]; //回传数据
        [self arrowBtnClick]; //执行动画
    }
}

- (void)arrowBtnClick {
    
    __weak typeof(self) unself = self;
    //判断编辑按钮的状态 如果是编辑状态 关闭编辑状态
    if (self.deleteBar.sortBtn.selected == YES) {
        [self.deleteBar sortBtnClick:self.deleteBar.sortBtn];
    }
    
    if (self.arrow.selected == NO) {
        
        if (self.hidden == YES) {  //下拉的背景view
            self.hidden = NO;
        }
        
        [UIView animateWithDuration: 0.6 animations:^{
            if (unself.deleteBar.alpha == 1.0) {
                
                unself.deleteBar.alpha = 0.0;
                unself.detailListView.transform = CGAffineTransformIdentity;
                [[unself.parentViewController rdv_tabBarController] setTabBarHidden:NO animated:YES];
//                unself.arrow.imageView.transform = CGAffineTransformIdentity;
                
            }else{
                
                unself.detailListView.transform = CGAffineTransformMakeTranslation(0, KScreenHeight);
                unself.parentViewController.tabBarController.tabBar.transform = CGAffineTransformMakeTranslation(0, 50);
                unself.deleteBar.alpha = 1.0;
                [[unself.parentViewController rdv_tabBarController] setTabBarHidden:YES animated:YES];
//                CGAffineTransform rotation = unself.arrow.imageView.transform;
//                unself.arrow.imageView.transform = CGAffineTransformRotate(rotation,M_PI_4);
            }
            
        } completion:^(BOOL finished) {
            
            unself.arrow.selected = YES;
            if (unself.deleteBar.alpha == 0.0) {
                 unself.hidden = YES;
                if ([unself.pullDelegate respondsToSelector:@selector(zqHomeUpAnimationDidFinish)]) {
                    [unself.pullDelegate zqHomeUpAnimationDidFinish];
                }

            }
        }];
    }
}



#pragma mark ====BYDetailsListViewDelegate===
/**
 *  点击item回传
 *
 *  @param itemName 点击的名字
 *  @param titleAr  回传的整个数组
 */
- (void)clickItemWithModel:(HomeGameItemModel *)itemModel andReturnSourceArr:(NSArray<HomeGameItemModel*> *)topArr {
    
    self.arrow.selected = NO;
    [self arrowBtnClick];
    if ([self.pullDelegate respondsToSelector:@selector(changedWithSelectedItem:andTopSourceArr:)]) {
        [self.pullDelegate changedWithSelectedItem:itemModel andTopSourceArr:topArr];
    }
}

/**
 *  点击关闭按钮回传  延时改变
 
 *
 *  @param itemName 当前选中item
 *  @param titleArr top数据源
 */
- (void)changeItemWithSelectModel:(HomeGameItemModel *)itemModel andReturnSourceArr:(NSArray<HomeGameItemModel *> *)topArr {

    if ([self.pullDelegate respondsToSelector:@selector(changedWithSelectedItem:andTopSourceArr:)]) {
        [self.pullDelegate changedWithSelectedItem:itemModel andTopSourceArr:topArr];
    }
    
}

/**
 *  长按
 */
- (void)itemLongPress {
    
    [self.deleteBar sortBtnClick:self.deleteBar.sortBtn];
}


- (void)itemRespondFromListBarClickWithItem:(HomeGameItemModel*)model {
    [self.detailListView itemRespondFromListBarClickWithItem:model];
}

//MARK: 剔除目标数组中的给定数组
- (NSMutableArray *)initializeArr:(NSArray<HomeGameItemModel*> *)inputArr removeObjectsInArray:(NSArray <HomeGameItemModel*>*)removeArr{
    
    NSMutableArray *resultArr = inputArr.mutableCopy;
    for (HomeGameItemModel *inputModel in inputArr) {
        for (HomeGameItemModel *removeModle in removeArr) {
            
            if ([inputModel.cateId isEqualToString:removeModle.cateId]) {
                
                [resultArr removeObject:inputModel];
            }
        }
    }
    
    return resultArr;
}

@end
