//
//  MicroClassSectionView.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/7.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MicroClassSectionView : UITableViewHeaderFooterView
- (void)setTitle:(NSString *)title;
@end
