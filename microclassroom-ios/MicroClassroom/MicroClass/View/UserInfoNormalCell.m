//
//  UserInfoNormalCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/16.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "UserInfoNormalCell.h"

#import "NSString+Size.h"

@interface UserInfoNormalCell ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation UserInfoNormalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithStr:(NSString *)str {

    self.infoLabel.text = str;
    
}

+ (CGFloat)cellHeightWithStr:(NSString *)str {
    
    CGFloat height = 28 + ceil([str sizeWithFontSize:15.0 andRectSize:CGSizeMake(KScreenWidth - 20, 0)].height);
    
    return height > 44 ? height: 44;
}

@end
