//
//  UserInfoContactCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/16.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserInfoContanctModel;

@interface UserInfoContactCell : UITableViewCell
- (void)configureCellWithModel:(UserInfoContanctModel *)model;
@end
