//
//  ZQHomePullDownView.h
//  ZQNewHomeTopDemo
//
//  Created by Hanks on 16/8/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeGameItemModel.h"
#import "ZJScrollPageViewDelegate.h"
#import "BYArrow.h"
@protocol ZQHomePullDownViewDelegate <NSObject>
@required
- (void)changedWithSelectedItem:(HomeGameItemModel *)selected andTopSourceArr:(NSArray<HomeGameItemModel*>*)sourceArr;
/**
 * 向上的动画结束时候的回调用
 */
@optional
-(void)zqHomeUpAnimationDidFinish;

@end

@interface ZQHomePullDownView : UIView

@property(nonatomic,weak) id<ZQHomePullDownViewDelegate> pullDelegate;

/**
 *  内部通过arrow 的状态来判断动画开始或者结束
 */
@property (nonatomic,strong) BYArrow                   *arrow;
@property (nonatomic,strong) UIView                    *detailBackView;

- (instancetype)initWithFrame:(CGRect)frame topTotles:(NSArray<HomeGameItemModel *> *)topItems andAllTitles:(NSArray<HomeGameItemModel *>*) allItems parentViewController:(UIViewController *)parentViewController  delegate:(id<ZQHomePullDownViewDelegate>) delegate;

/**
 *   改变选中的item
 *
 *  @param model <#model description#>
 */
- (void)itemRespondFromListBarClickWithItem:(HomeGameItemModel*)model;

/**
 *  外部调用方法前先设置arrow NO 的状态
 */
- (void)arrowBtnClick;

/**
 *  刷新内部数据的方法
 *
 *  @param allItem <#allItem description#>
 *  @param topItem <#topItem description#>
 */
- (void)upDateDatasWithAllTitleItem:(NSArray<HomeGameItemModel*>*)allItem andTopItem:(NSArray<HomeGameItemModel*>*) topItem;

@end
