//
//  UserInfoContactCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/16.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "UserInfoContactCell.h"
#import "PeopleModel.h"

@interface UserInfoContactCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation UserInfoContactCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithModel:(UserInfoContanctModel *)model {
    self.titleLabel.text = model.title;
    self.contentLabel.text = model.content;
}
@end
