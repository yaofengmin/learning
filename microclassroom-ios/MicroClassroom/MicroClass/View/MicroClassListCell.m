//
//  MicroClassListCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/6.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MicroClassListCell.h"

#import "MicroClassBannerSegmentModel.h"
#import "CareCollectionModel.h"

#import "UIImageView+AFNetworking.h"

@interface MicroClassListCell ()
@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIImageView *watchIcon;
@property (weak, nonatomic) IBOutlet UILabel *keyWordLabel;

@end

@implementation MicroClassListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.logoView.backgroundColor = ImageBackColor;
    self.logoView.layer.cornerRadius  = 4.0;
    self.logoView.layer.masksToBounds = YES;
    self.logoView.contentMode = UIViewContentModeScaleAspectFill;
    self.logoView.clipsToBounds = YES;
    
    self.timeLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    self.timeLabel.layer.cornerRadius  = 2.0;
    self.timeLabel.layer.masksToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithModel:(MicroClassVideoModel *)model {
   
    self.authorLabel.text = model.author;
    self.titleLabel.text = model.videoTitle;
//    self.timeLabel.text = [self timeFormatted:[model.videoLength intValue]];
    self.timeLabel.text = model.videoLength;
    if ([model.videoLength isEqualToString:@"00:00:00"] || model.videoLength.length == 0) {
        self.timeLabel.hidden = YES;
    }else{
        self.timeLabel.hidden = NO;
    }
    self.countLabel.text = model.videoLaud;
    self.keyWordLabel.text = model.videoKeyword;
    if (model.isMp3  == MicClassMp3TypeWeb) {//web
        self.watchIcon.hidden = YES;
        self.countLabel.hidden = YES;
    }else{
        self.watchIcon.hidden = [model.videoLaud intValue] == 0;
        self.countLabel.hidden = [model.videoLaud intValue] == 0;
    }
    [self setImg:model.videoLogo];
   
}

- (void)configWithCollectionModel:(CollectionModel *)model {
  
    self.authorLabel.text = model.teacherName;
    self.titleLabel.text = model.videoTitle;
    self.timeLabel.text = [self timeFormatted:[model.videoLength intValue]];
    self.countLabel.text = model.videoLaud;
    self.keyWordLabel.text = model.videoKeyWord;
    
    if (model.isMp3  == MicClassMp3TypeWeb) {//web
        self.watchIcon.hidden = YES;
        self.countLabel.hidden = YES;
    }else{
        self.watchIcon.hidden = [model.videoLaud intValue] == 0;
        self.countLabel.hidden = [model.videoLaud intValue] == 0;
    }
    [self setImg:model.videoLogo];
    
}

- (void)setImg:(NSString *)urlStr {
     [self.logoView sd_setImageWithURL:[NSURL URLWithString:urlStr]];

}


- (NSString *)timeFormatted:(int)totalSeconds {
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}


+ (CGFloat)cellHeight
{
    return 120.0;
}

@end
