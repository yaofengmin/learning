//
//  CatalogViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"
#import "MicroClassBannerSegmentModel.h"

@interface CatalogViewController : YHBaseViewController
@property (nonatomic, strong) NSArray<VideoMenuesModel *> *videoMenus;
@end
