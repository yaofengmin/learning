//
//  BooksDetailViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "BooksDetailViewController.h"

//Cell
#import "BooksDetailFirstTableCell.h"
#import "BooksDetailSecondTableCell.h"
#import "MicroClassCommentTableViewCell.h"
#import "BooksDetailThirdTableViewCell.h"


#import "WriteNotesViewController.h"
#import "CatalogViewController.h"
#import "AllReplyViewController.h"
#import "AllCommentsViewController.h"
#import "YHBaseWebViewController.h"

#import "CommentToolBar.h"
#import "CommonHeader.h"
#import "BooksDetailSecondHeader.h"
#import "MicroClassBannerSegmentModel.h"
#import "WFMessageBody.h"
#import "CirleRequstTool.h"

static NSString *bookDetailFirstCellID = @"BooksDetailFirstCellID";
static NSString *bookDetailSecondCellID = @"BooksDetailSecondCellID";
static NSString *bookDetailThirdCellID = @"BooksDetailThirdCellID";
static NSString *bookCommentCellID = @"MicroClassCommentCellID";


@interface BooksDetailViewController ()<UITableViewDelegate,UITableViewDataSource,CommentToolbarDelegate,deleteAction,isRead,CirleRequstToolDelegate>
@property (nonatomic,strong) CommentToolBar *toolBar;
@property (nonatomic,strong) UIButton *writeBtn;
@property (nonatomic,strong) MicroClassBooksDetailModel *mainModel;
@property (nonatomic,assign) NSInteger selectedIndexPath;

@property (nonatomic , strong)   CirleRequstTool      *tool;

@property (nonatomic ,strong) UITableView *tableV;
@end

@implementation BooksDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"图书详情";
    self.yh_navigationItem.leftBarButtonItem.button.hidden = YES;
    [self initalTable];
    [self creatWriteBtn];
    _tool = [[CirleRequstTool alloc]init];
    _tool.delegate = self;
    [self initalKeyBoard];
    [self pullData];
    
}
- (void)pullData {
    NSMutableDictionary *paramDic = @{@"service": GetVideoInfo,
                                      @"videoId": self.videoId
                                      }.mutableCopy;
    
    if (KgetUserValueByParaName(USERID)) {
        [paramDic setObject:KgetUserValueByParaName(USERID) forKey:@"user_Id"];
    }
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                _mainModel = [MicroClassBooksDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                
                self.toolBar.collectionBtn.selected = _mainModel.isCollection;
                self.toolBar.commentBadgeNum.text = [NSString stringWithFormat:@"%ld",_mainModel.Comments.count];
                [self.tableV reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}

-(void)initalTable
{
    self.tableV = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableV.backgroundColor = [UIColor whiteColor];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 100)];
    self.tableV.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    [self.tableV registerNib:[UINib nibWithNibName:@"BooksDetailFirstTableCell" bundle:nil] forCellReuseIdentifier:bookDetailFirstCellID];
    [self.tableV registerNib:[UINib nibWithNibName:@"BooksDetailSecondTableCell" bundle:nil] forCellReuseIdentifier:bookDetailSecondCellID];
    [self.tableV registerNib:[UINib nibWithNibName:@"BooksDetailThirdTableViewCell" bundle:nil] forCellReuseIdentifier:bookDetailThirdCellID];
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassCommentTableViewCell" bundle:nil] forCellReuseIdentifier:bookCommentCellID];
    
    
}
-(void)initalKeyBoard
{
    CGFloat chatbarHeight = [CommentToolBar defaultHeight];
    self.toolBar = [[CommentToolBar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - chatbarHeight, self.view.frame.size.width, chatbarHeight) buttonIsHidden:NO backIsHidden:NO];
    self.toolBar.delegate = self;
    self.toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:self.toolBar];
}
#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 2) {
        return _mainModel.videoMenus.count > 3?3:_mainModel.videoMenus.count;
    }else if (section == 3){
        return _mainModel.Comments.count > 5?5:_mainModel.Comments.count;
    }
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_mainModel == nil) {
        return 0;
    }
    return 4;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        BooksDetailFirstTableCell *cell = [tableView dequeueReusableCellWithIdentifier:bookDetailFirstCellID forIndexPath:indexPath];
        cell.model = _mainModel;
        return cell;
    }else if (indexPath.section == 1){
        BooksDetailSecondTableCell *cell = [tableView dequeueReusableCellWithIdentifier:bookDetailSecondCellID forIndexPath:indexPath];
        cell.model = _mainModel;
        return cell;
    }else if (indexPath.section == 2){
        BooksDetailThirdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:bookDetailThirdCellID forIndexPath:indexPath];
        cell.model = self.mainModel.videoMenus[indexPath.row];
        return cell;
    }else if (indexPath.section == 3){
        MicroClassCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:bookCommentCellID forIndexPath:indexPath];
        [cell configWithCommentModel:self.mainModel.Comments[indexPath.row]];
        cell.delegate = self;
        cell.stamp = indexPath;
        return cell;
    }
    return nil;
}
#pragma mark === UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 131;
    }else if (indexPath.section == 1){
        return _mainModel.DesHeight;
    }else if (indexPath.section == 2){
        return 40;
    }
    MicroClassCommentModel *model = _mainModel.Comments[indexPath.row];
    return [model commentCellHeight];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else if (section == 2){
        return _mainModel.videoMenus.count > 0?60:0.001;
    }else if (section == 3){
        return _mainModel.Comments.count > 0?60:0.001;
    }
    return 30;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return self.mainModel.videoMenus.count > 0?65:0.001;
    }else if (section == 3){
        return self.mainModel.Comments.count > 5?65:0.001;
    }
    return 0.001;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    if (section == 1) {
        BooksDetailSecondHeader *header = [[NSBundle  mainBundle] loadNibNamed:@"BooksDetailSecondHeader" owner:self options:nil].firstObject;
        header.readState.selected = !_mainModel.isReed;
        header.delegate = self;
        return header;
    }else if (section == 2){
        if ( _mainModel.videoMenus.count > 0) {
            CommonHeader *header = [[NSBundle  mainBundle] loadNibNamed:@"CommonHeader" owner:self options:nil].firstObject;
            header.titleLabel.text = @"目录";
            return header;
        }else{
            return nil;
        }
    }else if (section == 3){
        if (_mainModel.Comments.count) {
            CommonHeader *header = [[NSBundle  mainBundle] loadNibNamed:@"CommonHeader" owner:self options:nil].firstObject;
            header.titleLabel.text = @"评论";
            return header;
        }else{
            return nil;
        }
    }
    return nil;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 65)];
    view.backgroundColor = [UIColor whiteColor];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:NEColor forState:UIControlStateNormal];
    btn.titleLabel.font = Font(18);
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(view);
    }];
    CGRect sectionRect = [tableView rectForSection:section];
    CGRect newFrame = CGRectMake(CGRectGetMinX(view.frame), CGRectGetMinY(sectionRect), CGRectGetWidth(view.frame), CGRectGetHeight(view.frame));
    view.frame = newFrame;
    
    if (section == 2) {
        if ( _mainModel.videoMenus.count > 0) {
            [btn setTitle:@"查看全部目录" forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(allCatalogAction:) forControlEvents:UIControlEventTouchUpInside];
            return view;
        }
        return nil;
    }else if (section == 3){
        if (self.mainModel.Comments.count > 5) {
            [btn setTitle:@"查看全部评论" forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(allCommentAction:) forControlEvents:UIControlEventTouchUpInside];
            return view;
        }else{
            return nil;
        }
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 2) {
        VideoMenuesModel *model = self.mainModel.videoMenus[indexPath.row];
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.contentDetailUrl andNavTitle:model.titel];
        [self.navigationController pushViewController:web animated:YES];
    }else if (indexPath.section == 3) {
        MicroClassCommentModel *model = _mainModel.Comments[indexPath.row];
        AllReplyViewController *replyVC = [[AllReplyViewController alloc]init];
        replyVC.commentModel = model;
        replyVC.replay_commnets = _mainModel.Comments[indexPath.row].replay_commnets;
        replyVC.videoId = _mainModel.videoId;
        replyVC.pId = model.comContentId;
        replyVC.nickName = model.nickName;
        replyVC.cellIndex = indexPath.row;
        @weakify(self);
        replyVC.deletedMsg = ^(MicroClassCommentModel *ymd , NSInteger indext) {
            @strongify(self);
            [self.mainModel.Comments removeObjectAtIndex:indext];
            self.toolBar.commentBadgeNum.text = [NSString stringWithFormat:@"%ld",(unsigned long)_mainModel.Comments.count];
            [self.tableV reloadData];
        };
        [self.navigationController pushViewController:replyVC animated:YES];
    }
}
-(void)creatWriteBtn
{
    _writeBtn  =[UIButton buttonWithType:UIButtonTypeCustom];
    [_writeBtn setImage:[UIImage imageNamed:@"notes"] forState:UIControlStateNormal];
    [_writeBtn addTarget:self action:@selector(handleWriteNotesAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_writeBtn];
    [_writeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.bottom.equalTo(-70);
        make.size.equalTo(CGSizeMake(71, 71));
    }];
}

#pragma mark === 写
-(void)handleWriteNotesAction:(UIButton *)sender
{
    WriteNotesViewController *writeNotesVC = [[WriteNotesViewController alloc]initWithModel:nil isWrite:YES];
    writeNotesVC.videoId = self.mainModel.videoId;
    [self.navigationController pushViewController:writeNotesVC animated:YES];
}

#pragma mark === 查看所有目录
-(void)allCatalogAction:(UIButton *)sender
{
    CatalogViewController *catalogVC = [[CatalogViewController alloc]init];
    catalogVC.videoMenus = self.mainModel.videoMenus;
    [self.navigationController pushViewController:catalogVC animated:YES];
}
#pragma mark === 查看全部评论
-(void)allCommentAction:(UIButton *)sender
{
    AllCommentsViewController *commentVC = [[AllCommentsViewController alloc]init];
    commentVC.videoId = self.mainModel.videoId;
    [self.navigationController pushViewController:commentVC animated:YES];
}

#pragma mark === 键盘

#pragma mark === toolbar按钮点击
-(void)handleItemWithSender:(UIButton *)sender
{
    switch (sender.tag) {
        case 1://返回
        {
            [self backBtnAction];
        }
            break;
        case 2://评论
        {
            AllCommentsViewController *commentVC = [[AllCommentsViewController alloc]init];
            commentVC.videoId = self.mainModel.videoId;
            [self.navigationController pushViewController:commentVC animated:YES];
        }
            break;
        case 3://收藏
        {
            WFMessageBody *body = [[WFMessageBody alloc]init];
            body.msgId = _mainModel.videoId;
            if (sender.selected) {
                [self.tool collectionWith:body type:@"1" stamp:nil vc:self];
            }else{
                [self.tool cancelCollectionWith:body type:@"1" stamp:nil vc:self];
            }
        }
            break;
        case 4://分享
        {
            [[YHJHelp shareInstance]  showShareInController:self andShareURL:self.mainModel.videoUrl andTitle:self.mainModel.videoTitle andShareText:@"" andShareImage:nil isShowStudyGroup:NO isDIYUrl:NO];
        }
            break;
        default:
            break;
    }
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

-(void)didSendText:(NSString *)text
{
    [self sendComment:text];
}

#pragma mark - 发起评论
- (void)sendComment:(NSString *)text{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        [self.view endEditing:YES];
        
        NSDictionary *paramDic = @{@"service": AddComment,
                                   @"userId": KgetUserValueByParaName(USERID),
                                   @"toId": self.mainModel.videoId,
                                   @"toUserId":@0,
                                   @"ctype": @(1),
                                   @"pId": @(0),
                                   @"content": text};
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    [self pullData];
                    self.toolBar.inputTextView.text = @"";
                }
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
        }];
    }
}

#pragma mark === CirleRequstToolDelegate
-(void)addThumbWithBody:(MicroClassCommentModel *)body
{
    [self reloadOneRowsWithData:body andRow:self.selectedIndexPath];
}
-(void)cancelThumbWithBody:(MicroClassCommentModel *)body
{
    [self reloadOneRowsWithData:body andRow:self.selectedIndexPath];
}
#pragma mark === 刷新一个row ===
-(void)reloadOneRowsWithData:(MicroClassCommentModel *)ymData andRow:(NSInteger) row {
    
    [_mainModel.Comments replaceObjectAtIndex:row withObject:ymData];
    [self.tableV reloadData];
}
#pragma mark === 删除某条说说
-(void)delectMsgWithIndext:(NSIndexPath *)indext
{
    MicroClassCommentModel *model = self.mainModel.Comments[indext.row];
    [YHJHelp aletWithTitle:nil Message:@"确认删除这条信息吗" sureTitle:@"确定" CancelTitle:@"取消" SureBlock:^{
        NSDictionary *paramDic = @{Service:VideoDeleteCommentMessage,
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"commentId":@(model.comContentId)
                                   };
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    [self.mainModel.Comments removeObject:model];
                    self.toolBar.commentBadgeNum.text = [NSString stringWithFormat:@"%ld",(unsigned long)_mainModel.Comments.count];
                    [self.tableV reloadData];
                }else {
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];
    } andCancelBlock:nil andDelegate:self];
    
}


#pragma mark === isRead 在读

-(void)handleIsReadActionWithSender:(UIButton *)sender
{
    if (sender.selected) {
        [self.tool addMyBooks:self.mainModel vc:self tamp:nil];
    }else{
        [self.tool cancelMyBooks:self.mainModel vc:self tamp:nil];
    }
    sender.selected = !sender.selected;
}
#pragma mark === 点击查看全部回复
-(void)seeAllCommentWithIndex:(NSIndexPath *)indexPath
{
    MicroClassCommentModel *model = _mainModel.Comments[indexPath.row];
    AllReplyViewController *replyVC = [[AllReplyViewController alloc]init];
    replyVC.commentModel = model;
    replyVC.replay_commnets = _mainModel.Comments[indexPath.row].replay_commnets;
    replyVC.videoId = _mainModel.videoId;
    replyVC.pId = model.comContentId;
    replyVC.nickName = model.nickName;
    replyVC.cellIndex = indexPath.row;
    @weakify(self);
    replyVC.deletedMsg = ^(MicroClassCommentModel *ymd , NSInteger indext) {
        @strongify(self);
        [self.mainModel.Comments removeObjectAtIndex:indext];
        self.toolBar.commentBadgeNum.text = [NSString stringWithFormat:@"%ld",(unsigned long)_mainModel.Comments.count];
        [self.tableV reloadData];
    };
    [self.navigationController pushViewController:replyVC animated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField =0;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
