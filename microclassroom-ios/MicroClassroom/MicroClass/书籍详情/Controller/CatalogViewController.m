//
//  CatalogViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "CatalogViewController.h"
#import "YHBaseWebViewController.h"


@interface CatalogViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableV;
@end

@implementation CatalogViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.yh_navigationItem.title = @"目录";
	[self initalTable];
}
-(void)initalTable
{
	self.tableV = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
	self.tableV.backgroundColor = [UIColor whiteColor];
	self.tableV.delegate = self;
	self.tableV.dataSource = self;
	self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
	[self.view addSubview:self.tableV];
	[self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
		make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
	}];
	if ([self.tableV respondsToSelector:@selector(setSeparatorInset:)]) {
		[self.tableV  setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
	}
	if (IOS8) {
		if ([self.tableV  respondsToSelector:@selector(setLayoutMargins:)]) {
			[self.tableV  setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
		}
	}
}
#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return self.videoMenus.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    VideoMenuesModel *model = self.videoMenus[indexPath.row];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CatelogCellID"];
	if (!cell) {
		cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CatelogCellID"];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.textColor = DColor;
	cell.textLabel.font = Font(15);
	cell.textLabel.text = model.titel;
	return cell;
}
#pragma mark === UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    VideoMenuesModel *model = self.videoMenus[indexPath.row];
    YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.contentDetailUrl andNavTitle:model.titel];
    [self.navigationController pushViewController:web animated:YES];
}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


@end
