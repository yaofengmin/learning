//
//  BooksDetailSecondTableCell.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MicroClassBannerSegmentModel.h"

@interface BooksDetailSecondTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *videoDes;
@property (weak, nonatomic) IBOutlet UILabel *authorDes;
@property (weak, nonatomic) IBOutlet UILabel *author;
@property (weak, nonatomic) IBOutlet UIButton *seeAllBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seeAllTop;

@property (nonatomic,strong) MicroClassBooksDetailModel *model;

@end
