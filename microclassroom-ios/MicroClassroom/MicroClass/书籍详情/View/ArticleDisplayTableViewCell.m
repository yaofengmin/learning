//
//  ArticleDisplayTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "ArticleDisplayTableViewCell.h"

@implementation ArticleDisplayTableViewCell

- (void)awakeFromNib {
	[super awakeFromNib];
	// Initialization code
	//	[self setLabelLineParagraphStyleWithLabel:self.content];
	
}

-(void)setLabelLineParagraphStyleWithLabel:(UILabel *)label
{
	NSDictionary *dic = @{NSKernAttributeName:@2.f
						  
						  };
	
	NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:label.text attributes:dic];
	
	NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
	
	[paragraphStyle setLineSpacing:5];//行间距
	
	[attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text  length])];
	
	[label setAttributedText:attributedString];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

@end
