//
//  BooksDetailFirstTableCell.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "BooksDetailFirstTableCell.h"

@implementation BooksDetailFirstTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.bookImage.backgroundColor = ImageBackColor;
    self.bookImage.contentMode = UIViewContentModeScaleAspectFill;
    self.bookImage.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setModel:(MicroClassBooksDetailModel *)model
{
    [self.bookImage sd_setImageWithURL:[NSURL URLWithString:model.videoLogo]];
    self.bookTitle.text = [NSString stringWithFormat:@"%@",model.videoTitle];
    self.PressTime.text = model.publishDate;
    self.Press.text = model.publishHouse;
}
@end
