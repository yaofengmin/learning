//
//  BooksDetailThirdTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "BooksDetailThirdTableViewCell.h"

@implementation BooksDetailThirdTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(VideoMenuesModel *)model
{
    self.menuTitle.text = model.titel;
}

@end
