//
//  BooksDetailThirdTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MicroClassBannerSegmentModel.h"

@interface BooksDetailThirdTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuTitle;


@property (nonatomic,strong) VideoMenuesModel *model;
@end
