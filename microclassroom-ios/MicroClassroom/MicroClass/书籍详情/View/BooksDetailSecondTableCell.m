//
//  BooksDetailSecondTableCell.m
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "BooksDetailSecondTableCell.h"
#import "YHBaseWebViewController.h"

@implementation BooksDetailSecondTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setModel:(MicroClassBooksDetailModel *)model
{
    _model = model;
    self.videoDes.text = model.videoDesc;
    self.authorDes.text = model.authorDes;
    self.author.text = [NSString stringWithFormat:@"作者 : %@",model.author];
    self.seeAllBtn.hidden = !model.showAll;
    self.seeAllTop.constant = model.showAll?45:15;
}
- (IBAction)handleSeeAllAction:(UIButton *)sender {
    YHBaseWebViewController *articleVC = [[YHBaseWebViewController alloc]initWithUrlStr:self.model.detailUrl andNavTitle:self.model.videoTitle];
    [self.viewController.navigationController pushViewController:articleVC animated:YES];
}

@end
