//
//  BooksDetailSecondHeader.h
//  yanshan
//
//  Created by BPO on 2017/8/18.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol isRead <NSObject>

-(void)handleIsReadActionWithSender:(UIButton *)sender;

@end
@interface BooksDetailSecondHeader : UIView
@property (weak, nonatomic) IBOutlet UIButton *readState;

@property (nonatomic,weak) id<isRead> delegate;
@end
