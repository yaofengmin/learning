//
//  BooksDetailFirstTableCell.h
//  yanshan
//
//  Created by BPO on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MicroClassBannerSegmentModel.h"

@interface BooksDetailFirstTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;
@property (weak, nonatomic) IBOutlet UILabel *Press;
@property (weak, nonatomic) IBOutlet UILabel *PressTime;


@property (nonatomic,strong) MicroClassBooksDetailModel *model;
@end
