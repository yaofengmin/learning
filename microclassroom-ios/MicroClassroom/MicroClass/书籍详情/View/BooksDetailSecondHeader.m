
//
//  BooksDetailSecondHeader.m
//  yanshan
//
//  Created by BPO on 2017/8/18.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "BooksDetailSecondHeader.h"

@implementation BooksDetailSecondHeader

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


- (IBAction)handleReadAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(handleIsReadActionWithSender:)]) {
        [self.delegate handleIsReadActionWithSender:sender];
    }
}

@end
