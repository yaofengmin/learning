//
//  ClassGropModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/8.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface ClassGropModel : YHBaseModel

@property (nonatomic, copy) NSString *remark;

@property (nonatomic, copy) NSString *groupId;

@property (nonatomic, copy) NSString *tUserId;

@property (nonatomic, copy) NSString *className;

@property (nonatomic, copy) NSString *endTime;

@property (nonatomic, copy) NSString *teacherName;

@property (nonatomic, copy) NSString *stuClassId;

@property (nonatomic, copy) NSString *startTime;

@property (nonatomic, copy) NSString *classId;

@property (nonatomic, copy) NSString *logo;
@end
