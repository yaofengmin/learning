//
//  ClassGropCell.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/8.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "ClassGropCell.h"
#import "ClassGropModel.h"
#import <UIImageView+WebCache.h>

@implementation ClassGropCell


- (void)awakeFromNib {
    [super awakeFromNib];
    self.headImageView.backgroundColor = ImageBackColor;
    self.headImageView.layer.cornerRadius  = 4.0;
    self.headImageView.layer.masksToBounds = YES;
}

+ (CGFloat)cellHeight {
    return 100;
}

+ (instancetype)cellWithTableView:(UITableView *)table {
    ClassGropCell *cell = [table dequeueReusableCellWithIdentifier:@"ClassGropCell"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ClassGropCell" owner:nil options:nil]lastObject];
    }
    return cell;
}

- (void)configWithModel:(ClassGropModel *)cellModel {
  
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:cellModel.logo]];
    self.classNameLabel.text = cellModel.className;
    self.manageLabel.text   = [NSString stringWithFormat:@"管理员: %@",cellModel.teacherName];
    self.timeLabel.text  =[NSString stringWithFormat:@"%@ - %@",cellModel.startTime,cellModel.endTime];
}


@end
