//
//  ClassGropCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/8.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseTableViewCell.h"

@interface ClassGropCell : YHBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *manageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
+ (instancetype)cellWithTableView:(UITableView *)table;
@end
