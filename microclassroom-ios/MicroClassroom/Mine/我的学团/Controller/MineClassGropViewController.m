//
//  MineClassGropViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/8.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MineClassGropViewController.h"
#import "ClassDetailViewController.h"
#import "ClassGropCell.h"
#import "ClassGropModel.h"
#import "AddGropViewController.h"
@interface MineClassGropViewController()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) UITableView    *tableView;

@end

@implementation MineClassGropViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MainBackColor;
    self.yh_navigationItem.title = @"我的班级";
    //加入班级
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"add_group"] style:0 handler:^(id send) {
        @strongify(self);
        AddGropViewController *add  = [[AddGropViewController alloc]init];
        [self.navigationController pushViewController:add animated:YES];
    }];
    [self creatTabelView];
    [self requestList];
}


- (void)requestList {
    NSDictionary *paramDic = @{Service:UserGetUserClasses,
                               @"userId":KgetUserValueByParaName(USERID)
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.dataArr = [ClassGropModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.tableView reloadData];
            }
        }
    }];
}

-(void)creatTabelView {
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ClassGropCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ClassGropCell *cell = [ClassGropCell cellWithTableView:tableView];
    ClassGropModel *model = [self.dataArr objectAtIndex:indexPath.row];
    [cell configWithModel:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ClassDetailViewController *contr = [[ClassDetailViewController alloc]initWithClassId:[self.dataArr[indexPath.row] classId]];
    [self.navigationController pushViewController:contr animated:YES];
}



@end

