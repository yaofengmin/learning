//
//  AddGropInfoViewController.h
//  MicroClassroom
//
//  Created by Hanks on 2016/9/24.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"
@class ClassGropModel;
@interface AddGropInfoViewController : YHBaseViewController

- (instancetype)initWithClassGropModel:(ClassGropModel *)model;

@end
