//
//  ClassDetailViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/8.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "ClassDetailViewController.h"
#import "ClassGropModel.h"
#import "ClassGropCell.h"
#import "FriendModel.h"
#import "PeopleCell.h"
#import "PersonHomeViewController.h"
#import "ChatViewController.h"
#import <MJRefresh.h>

static NSString * const kPeopleCell = @"PeopleCell";
@interface ClassDetailViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) UITableView    *tableView;
@property (nonatomic, copy)  NSString  *classId;
@property (nonatomic, strong) ClassGropModel *classModel;
@end

@implementation ClassDetailViewController

- (instancetype)initWithClassId:(NSString *)classId{
    if (self = [super init]) {
        _classId = classId;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MainBackColor;
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_groupchat"] style:0 handler:^(id send) {  // MARK: 跳转群聊
        @strongify(self);
        
        ChatViewController *group = [[ChatViewController alloc]initWithConversationChatter:self.classModel.groupId conversationType:EMConversationTypeGroupChat andData:self.dataArr];
        group.groupName = self.classModel.className;
        group.groupUrl = self.classModel.logo;
        [self.navigationController pushViewController:group animated:YES];
    }];
    self.yh_navigationItem.title = _classModel.className;
    [self creatTabelView];
    [self requestList];
    
    // Do any additional setup after loading the view.
}



- (void)requestList {
    NSDictionary *paramDic = @{Service:UserGetUsersByClassId,
                               @"ClassId":_classId
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.dataArr = [FriendModel mj_objectArrayWithKeyValuesArray: result[REQUEST_LIST][REQUEST_LIST]];
                _classModel = [ClassGropModel mj_objectWithKeyValues:result[REQUEST_LIST][@"class_info"]];
                [self.tableView reloadData];
            }
        }
    }];
}

-(void)creatTabelView {
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:kPeopleCell bundle:nil]
         forCellReuseIdentifier:kPeopleCell];
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestList];
    }];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [ClassGropCell cellHeight];
    }else{
        //        return [PeopleCell cellHeight];
        FriendModel *model =  _dataArr[indexPath.row];
        if (model.label_have.count != 0 && model.personalSign.length != 0) {
            return 100;
        }else{
            return 70;
        }
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0 ) {
        return 1;
    }
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ClassGropCell *cell = [ClassGropCell cellWithTableView:tableView];
        [cell configWithModel:_classModel];
        return cell;
    }else {
        PeopleCell * cell = [tableView dequeueReusableCellWithIdentifier:kPeopleCell forIndexPath:indexPath];
        [cell configureCellWithFriendModel:self.dataArr[indexPath.row]];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        
        FriendModel *friend = self.dataArr[indexPath.row];
        PersonHomeViewController * targetVC = [[PersonHomeViewController alloc] init];
        targetVC.userId = [friend.userId intValue];
        [self.navigationController pushViewController:targetVC animated:YES];
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.001;
    }else{
        return 40;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
        view.backgroundColor = NCColor;
        UILabel *label = [UILabel labelWithText:@"班级成员" andFont:14 andTextColor:JColor andTextAlignment:1];
        label.backgroundColor = BColor;
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(10, 0, 0, 0));
        }];
        return view;
    }
    return nil;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

