//
//  AddGropViewController.m
//  MicroClassroom
//
//  Created by Hanks on 2016/9/24.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AddGropViewController.h"
#import "ClassGropCell.h"
#import "ClassGropModel.h"
#import "AddGropInfoViewController.h"
#import "ChineseInclude.h"
#import "PinYinForObjc.h"
#import <MJRefresh.h>

@interface AddGropViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UISearchBarDelegate
>
{
    NSInteger pageCount;
}
@property(strong, nonatomic) NSMutableArray *dataArr; // 所有城市
@property(strong, nonatomic)  UITableView *tableView;
@property(copy,nonatomic) NSString *keyWords;
@property(strong ,nonatomic) UISearchBar *searchBar;


@end

@implementation AddGropViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"加入班级";
    self.keyWords = @"";
    pageCount = 1;
    [self setUpTableView];
}

- (void)setUpTableView {
    
    self.view.backgroundColor = NCColor;
    [self.view addSubview:self.searchBar];
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight + 40, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefesh)];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footRefresh)];
    self.tableView.mj_footer.hidden = YES;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}


-(void)headRefesh {
    self.dataArr = @[].mutableCopy;
    pageCount = 1;
    [self requestListWithKeyWord:self.keyWords];
}


-(void)footRefresh{
    pageCount ++;
    [self requestListWithKeyWord:self.keyWords];
}


- (void)requestListWithKeyWord:(NSString *)keyword{
    NSDictionary *paramDic = @{Service:AppGetClasses,
                               @"user_Id":KgetUserValueByParaName(USERID),
                               @"pageIndex":@(pageCount),
                               @"pageSize":@(10),
                               @"keyWord":keyword
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (pageCount == 1) {
            [self.tableView.mj_header endRefreshing];
        }else{
            [self.tableView.mj_footer endRefreshing];
        }
        
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.dataArr = [ClassGropModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST][REQUEST_LIST]];
                [self.tableView reloadData];
            }
        }
    }];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ClassGropCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ClassGropCell *cell = [ClassGropCell cellWithTableView:tableView];
    ClassGropModel *model = [self.dataArr objectAtIndex:indexPath.row];
    [cell configWithModel:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.dataArr.count == 0) {return;}
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    AddGropInfoViewController *contr = [[AddGropInfoViewController alloc]initWithClassGropModel:_dataArr[indexPath.row]];
    [self.navigationController pushViewController:contr animated:YES];
}


- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, KTopHeight, KScreenWidth, 44)];
        _searchBar.delegate = self;
        _searchBar.placeholder = @"请输入班级名称";
        _searchBar.backgroundImage = [UIImage new];
        _searchBar.backgroundColor = BColor;
        UITextField * searchField = [self sa_GetSearchTextFiled];
        searchField.font = Font(12);
        searchField.textColor = JColor;
        [self setSearchTextFieldBackgroundColor:NCColor];
        
    }
    return _searchBar;
}

- (UITextField *)sa_GetSearchTextFiled{
    if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
        return _searchBar.searchTextField;
    }else{
//        UITextField *searchTextField =  [self valueForKey:@"_searchField"];
//        return searchTextField;
        return self.searchBar.subviews.firstObject.subviews.lastObject;
    }
}

- (void)setSearchTextFieldBackgroundColor:(UIColor *)backgroundColor
{
    UIView *searchTextField = nil;
    if (IOS7) {
        searchTextField = [[[self.searchBar.subviews firstObject] subviews] lastObject];
    }
    searchTextField.backgroundColor = backgroundColor;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.dataArr removeAllObjects];
    [self requestListWithKeyWord:searchText];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

@end
