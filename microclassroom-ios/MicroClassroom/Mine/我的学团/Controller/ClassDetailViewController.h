//
//  ClassDetailViewController.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/8.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"

@interface ClassDetailViewController : YHBaseViewController
- (instancetype)initWithClassId:(NSString *)classId;
@end
