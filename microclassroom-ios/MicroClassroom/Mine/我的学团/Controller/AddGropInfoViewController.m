//
//  AddGropInfoViewController.m
//  MicroClassroom
//
//  Created by Hanks on 2016/9/24.
//  Copyright © 2016年 Hanks. All rights reserved.
//  加入班级

#import "AddGropInfoViewController.h"
#import "ClassGropModel.h"
#import "ClassGropCell.h"

@interface AddGropInfoViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) UITableView    *tableView;
@property (nonatomic ,strong) ClassGropModel *mode;
@end

@implementation AddGropInfoViewController

- (instancetype)initWithClassGropModel:(ClassGropModel *)model {
    if (self = [super init]) {
        _mode = model;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = self.mode.className;
    [self creatTabelView];
    // Do any additional setup after loading the view.
}

-(void)creatTabelView {
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    DIYButton *btn = [DIYButton buttonWithImage:nil andTitel:@"加入班级" andBackColor:kNavigationBarColor];
    [btn addTarget:self action:@selector(ApplicationJoin:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(CGSizeMake(KScreenWidth *0.9, 40));
        make.bottom.equalTo(self.view).offset(-20);
        make.centerX.equalTo(self.view);
    }];
    
    
}

- (void)ApplicationJoin:(UIButton *)btn {
    NSDictionary *paramDic = @{Service:AppJoinClasses,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"classId":self.mode.classId
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                [btn setTitle:@"已申请" forState:UIControlStateNormal];
                btn.enabled = NO;
            }else {
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
    }];
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return [ClassGropCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        ClassGropCell *cell = [ClassGropCell cellWithTableView:tableView];
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        [cell configWithModel:self.mode];
        return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

@end
