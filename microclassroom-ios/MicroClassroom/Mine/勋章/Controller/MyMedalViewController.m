//
//  MyMedalViewController.m
//
//
//  Created by BPO on 2017/8/21.
//
//

#import "MyMedalViewController.h"

#import "MyMedalCollectionViewCell.h"

#import "MyMedalModel.h"

#import "MedalAlertPromptView.h"

#import "UserInfoModel.h"

@interface MyMedalViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic,strong) NSArray *dataArr;

@end

static CGFloat const kCellMargin = 25.0f;
static CGFloat const kCellColumns = 25.0f;
static NSInteger const cellNum = 3;

static NSString *MyMedalCellID = @"MyMedalCollectionViewCellID";

@implementation MyMedalViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.yh_navigationItem.title = @"我的勋章";
	[self layoutSubviews];
    [self requestList];
}

- (void)requestList {
    NSDictionary *paramDic = @{Service:GetUserHonorList,@"userId":KgetUserValueByParaName(USERID)};
    SHOWHUD;
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        @strongify(self);
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                NSDictionary *info = result[REQUEST_INFO];
                UserInfoModel * user = [UserInfoModel getInfoModel];
                user.integrate = info[@"integrate"];
                user.haveHonor = info[@"haveHonor"];
                user.weCount = info[@"weCount"];
                [UserInfoModel saveUserInfoModelWithModel:user];
                _dataArr = [MyMedalModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.collectionView reloadData];
            }else{
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
    }];
}

#pragma mark - collectionView
- (void)layoutSubviews
{
	UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
	//cell间距
	flowLayout.minimumInteritemSpacing = kCellMargin;
	//cell行距
	flowLayout.minimumLineSpacing = kCellColumns;
	
	CGFloat proportion = (KScreenWidth - kCellMargin * (cellNum + 1))/ cellNum;
	flowLayout.itemSize = CGSizeMake(proportion, 125);
	self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
	self.collectionView.delegate = self;
	self.collectionView.dataSource = self;
	self.collectionView.backgroundColor = BColor;
	self.collectionView.alwaysBounceVertical = YES;
	[self.collectionView registerNib:[UINib nibWithNibName:@"MyMedalCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MyMedalCellID];
	[self.view addSubview:self.collectionView];
	[self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight + 10, 0, 0, 0));
	}];
}

#pragma mark UICollectionView DataSource Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	return self.dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	MyMedalCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:MyMedalCellID forIndexPath:indexPath];
    cell.model = self.dataArr[indexPath.item];
	return cell;
}

//UICollectionView被选中时调用的方法
-(void)collectionView:( UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
	
    MedalAlertPromptView *promptView = [MedalAlertPromptView shareParkAlterView];
    [promptView loadViewWithPromptlyView:self.view.window andDataModel:self.dataArr[indexPath.item]];
	
}

#pragma mark === UICollectionViewDelegateFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
	return UIEdgeInsetsMake(kCellColumns, 10, kCellColumns, 10);
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


@end
