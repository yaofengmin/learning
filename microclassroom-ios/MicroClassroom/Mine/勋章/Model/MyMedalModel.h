//
//  MyMedalModel.h
//  MicroClassroom
//
//  Created by fm on 2017/11/14.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface MyMedalModel : YHBaseModel
@property (nonatomic,copy) NSString *integrate;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *photo;
@property (nonatomic,copy) NSString *weCount;

@end
