//
//  MedalAlertPromptView.h
//  yanshan
//
//  Created by fm on 2017/10/27.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyMedalModel.h"

@interface MedalAlertPromptView : UIView
{
    UIWindow      *MainWindow;
    /**
     *  弹出的背景框
     */
    UIView                      *_coverView; //
    UIView *_whiteView;
    
}


+(instancetype)shareParkAlterView;

- (void)loadViewWithPromptlyView:(UIWindow*)window andDataModel:(MyMedalModel *)data;

@end
