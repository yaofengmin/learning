//
//  MyMedalCollectionViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyMedalModel.h"

@interface MyMedalCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *medalImage;
@property (weak, nonatomic) IBOutlet UILabel *medalName;

@property (nonatomic,strong) MyMedalModel *model;
@end
