//
//  MedalAlertPromptView.m
//  yanshan
//
//  Created by fm on 2017/10/27.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MedalAlertPromptView.h"
#import "UserInfoModel.h"

static MedalAlertPromptView *promptView = nil;

@implementation MedalAlertPromptView

#pragma mark === 初始化单例
+(instancetype)shareParkAlterView
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        promptView = [[MedalAlertPromptView alloc]init];
    });
    return promptView;
}

#pragma mark === 弹出界面
-(void)loadViewWithPromptlyView:(UIWindow *)window andDataModel:(MyMedalModel *)data
{
    //主窗口
    MainWindow = window;
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    [MainWindow addSubview:self];
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    //显示的view
    _whiteView = [[UIView alloc]init];
    _whiteView.backgroundColor = [UIColor clearColor];
    [self addSubview:_whiteView];
    [_whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
        make.center.mas_equalTo(0);
    }];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissSelf)];
    [_whiteView addGestureRecognizer:tap];

    //勋章图片
    UIImageView *medalImage = [[UIImageView alloc]init];
    [medalImage sd_setImageWithURL:[NSURL URLWithString:data.photo] placeholderImage:[UIImage imageNamed:@"medal"]];
    [_whiteView addSubview:medalImage];
    [medalImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(78);
        make.centerX.mas_equalTo(_whiteView);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth * 160 / 375, KScreenHeight * 198 / 667));
    }];
    
    //勋章名字
    UILabel *medalName = [[UILabel alloc]init];
    medalName.text = data.name;
    medalName.textAlignment = NSTextAlignmentCenter;
    medalName.font = [UIFont fontWithName:@"PingFang SC" size:25];
    medalName.textColor = FColor;
    [_whiteView addSubview:medalName];
    [medalName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(medalImage.mas_bottom).offset(32);
        make.width.mas_equalTo(KScreenWidth - 30);
        make.centerX.mas_equalTo(_whiteView);
    }];
    
    UILabel *statusLabel = [[UILabel alloc]init];
    NSString *myIntegrate = [UserInfoModel getInfoModel].integrate;
    NSString *integrate = data.integrate;
    if ([myIntegrate integerValue] >= [integrate integerValue]) {
        statusLabel.text = @"(已获得)";
    }else{
        statusLabel.text = @"(未获得)";
    }
    statusLabel.textAlignment = NSTextAlignmentCenter;
    statusLabel.font = [UIFont fontWithName:@"PingFang SC" size:15];
    statusLabel.textColor = FColor;
    [_whiteView addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(medalName.mas_bottom).offset(16);
        make.width.mas_equalTo(KScreenWidth - 30);
        make.centerX.mas_equalTo(_whiteView);
    }];
    
    
    
    //积分
    UILabel *recordLabel = [[UILabel alloc]init];
    recordLabel.textAlignment = NSTextAlignmentCenter;
    recordLabel.text = [NSString stringWithFormat:@"积分(%@/%@)",myIntegrate,integrate];
    recordLabel.font = [UIFont fontWithName:@"PingFang SC" size:14];
    recordLabel.textColor = FColor;
    [_whiteView addSubview:recordLabel];
    [recordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(statusLabel.mas_bottom).offset(73);
        make.width.mas_equalTo(KScreenWidth - 30);
        make.centerX.mas_equalTo(_whiteView);
    }];
    
    
    //we+积分
    UILabel *weLabel = [[UILabel alloc]init];
    weLabel.textAlignment = NSTextAlignmentCenter;
    weLabel.text = [NSString stringWithFormat:@"发布文章%@/%@",[UserInfoModel getInfoModel].weCount,data.weCount];
//    @"we+发布文章(3/44)";
    weLabel.font = [UIFont fontWithName:@"PingFang SC" size:14];
    weLabel.textColor = FColor;
    [_whiteView addSubview:weLabel];
    [weLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(recordLabel.mas_bottom).offset(12);
        make.width.mas_equalTo(KScreenWidth - 30);
        make.centerX.mas_equalTo(_whiteView);
    }];
    
    
    
    
}


#pragma mark === 消失
-(void)dismissSelf
{
    [UIView animateWithDuration:0.3 animations:^{
        _whiteView.alpha = 0;
        _whiteView = nil;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


@end
