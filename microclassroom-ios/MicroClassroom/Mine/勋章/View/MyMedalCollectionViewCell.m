//
//  MyMedalCollectionViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyMedalCollectionViewCell.h"

#import "UserInfoModel.h"

@implementation MyMedalCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)setModel:(MyMedalModel *)model
{
    NSString *myIntegrate = [UserInfoModel getInfoModel].integrate;
    if ([myIntegrate integerValue] >= [model.integrate integerValue]) {
        self.medalName.textColor = CColor;
    }else{
        self.medalName.textColor = DColor;
    }
    self.medalName.text = model.name;
    [self.medalImage sd_setImageWithURL:[NSURL URLWithString:model.photo] placeholderImage:[UIImage imageNamed:@"medal"]];
}

@end
