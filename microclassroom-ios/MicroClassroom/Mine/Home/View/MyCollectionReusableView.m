//
//  MyCollectionReusableView.m
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "MyCollectionReusableView.h"

#import "UserInfoModel.h"
#import "MyInfoVC.h"


@implementation MyCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headImage.layer.cornerRadius   = 55 / 2.0;
    self.headImage.layer.masksToBounds  = YES;
    
    self.gradeLaebl.layer.cornerRadius  = 15 / 2.0;
    self.gradeLaebl.layer.masksToBounds = YES;
 
}

-(void)setValueWithModel
{
    
    UserInfoModel *model  = [UserInfoModel getInfoModel];
    self.gradeLaebl.hidden = YES;
    if (model == nil) {
        self.nameLabel.text = @"您还未登录";
        self.gradeLaebl.text = @"";
        self.desLabel.text = @"";
        self.headImage.image = [UIImage imageNamed:@"login_head"];
//        self.addVImageView.hidden = YES;
        self.signView.hidden = YES;
    }else{
//        self.gradeLaebl.hidden = NO;
        [self.headImage sd_setImageWithURL:[NSURL URLWithString:model.photo] placeholderImage:[UIImage imageNamed:@"login_head"]];
        self.nameLabel.text = model.nickname;
//        self.gradeLaebl.text = [NSString stringWithFormat:@" Lv. %@ ",model.grade];
        self.desLabel.text = model.personalSign;
//        self.addVImageView.hidden = !model.isCertified;
        self.signView.hidden = NO;
        if ([UserInfoModel getInfoModel].isSign) {
            self.signImg.image = [UIImage imageNamed:@"sign"];
            self.signLbl.text = @"已签到";
        }else{
             self.signImg.image = [UIImage imageNamed:@"no_sign"];
             self.signLbl.text = @"今日签到";
        }
    }
}
- (IBAction)handleDetailInfoAction:(UIButton *)sender {
    MyInfoVC * targetVC = [[MyInfoVC alloc] init];
    [self.viewController.navigationController pushViewController:targetVC animated:YES];
}


@end
