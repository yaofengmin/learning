//
//  MyCollectionReusableView.h
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *desLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradeLaebl;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UIImageView *signImg;
@property (weak, nonatomic) IBOutlet UILabel *signLbl;
@property (weak, nonatomic) IBOutlet UIButton *signBtn;
@property (weak, nonatomic) IBOutlet UIView *signView;

-(void)setValueWithModel;
@end
