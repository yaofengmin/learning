//
//  MyHomeCollectionViewCell.h
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyHomeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *HomeImage;
@property (weak, nonatomic) IBOutlet UILabel *homeKind;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTop;
//@property (weak, nonatomic) IBOutlet UIView *rightLine;

@end
