//
//  MyHomeCollectionViewCell.m
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "MyHomeCollectionViewCell.h"

@implementation MyHomeCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    self.badgeLabel.layer.cornerRadius  = 15 / 2.0;
//    self.badgeLabel.layer.masksToBounds = YES;
    self.homeKind.adjustsFontSizeToFitWidth = YES;
    if (IPHONE_5) {
        self.imageTop.constant = 15;
    }else{
        self.imageTop.constant = 25;
    }
}

@end
