//
//  MyHomeViewController.m
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "MyHomeViewController.h"
#import "MyHomeCollectionViewCell.h"
#import "MyCollectionReusableView.h"
#import "MyHomeCollectionSecondReusableView.h"

#import "YhjRootViewController.h"

#import "MyInfoVC.h"

#import "MineWalletViewController.h"
#import "MyOrderVC.h"
#import "MineClassGropViewController.h"
#import "LearningTasksViewController.h"


#import "MyCollectionVC.h"
#import "MyMainCollectionViewController.h"
#import "MyCareVC.h"
#import "MyZhiBoVC.h"
#import "MySharedViewController.h"

#import "DeskmateViewController.h"
#import "ConversationListController.h"
#import "UserNewsVC.h"
#import "MeMoreViewController.h"

#import "MyMessageViewController.h"
#import "MyMedalViewController.h"
#import "MyNotesViewController.h"
#import "MyBooksViewController.h"
#import "FeedBackViewCtrl.h"

#import "MyIntegraListViewController.h"

#import "YHBaseWebViewController.h"
#import "InitConfigModel.h"
#import "MyHomeInfoModel.h"

#import "MyHomeFlowLayout.h"

@interface MyHomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic ,strong) NSArray *titleArr;//第一分区名字
@property (nonatomic ,strong) NSArray *imageArr;//第一分区图片

@property (nonatomic ,strong) NSArray *menusTopList;//第一分区
@property (nonatomic ,strong) NSArray *menusCenterList;//第二分区

@property (nonatomic ,strong) MyHomeCollectionViewCell *cell;

@property (nonatomic ,strong) UserInfoModel *infoModel;

@property (nonatomic ,strong) UILabel *badgeLabel;
@end

static CGFloat     const kCellMargin  = 0.0f;
static CGFloat     const kCellColumns = 1.0f;
static NSInteger   const cellNum      = 4.0;
static NSString *MyHomeCollectionCellID = @"MyHomeCollectionCellID";
static NSString *MyHomeReusableView  = @"MyHomeReusableView";
static NSString *MyHomeSecondReusableView  = @"MyHomeSecondReusableView";


@implementation MyHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationBar.hidden = YES;
    [self configData];
    
    KAddobserverNotifiCation(@selector(refreshDataSource), @"refreshConversionDataSource");
    KAddobserverNotifiCation(@selector(setupUnreadMessageCount), @"setupUnreadMessageCount");
    KAddobserverNotifiCation(@selector(refreshMyHomeData:),
                             NOTIGICATIONFINDVVIEWRELOAD);
    KAddobserverNotifiCation(@selector(requestUserInfo), @"signWebRefresh");
    
}
#pragma mark === 获取个人基本信息
-(void)configData
{
    NSDictionary *dataDic = KgetUserValueByParaName(HCmyDyItems);
    if (dataDic == nil) {
        return;
    }
    _infoModel = [UserInfoModel mj_objectWithKeyValues:dataDic];
    if (!_collectionView.superview) {
        [self creatCollectionV];
    }else{
        [self.collectionView reloadData];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

-(void)creatCollectionV
{
    MyHomeFlowLayout *flowLayout = [[MyHomeFlowLayout alloc]init];
    //cell间距
    flowLayout.minimumInteritemSpacing = kCellMargin;
    //cell行距
    flowLayout.minimumLineSpacing = kCellColumns;
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = NCColor;
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    for (int i = 0; i < 2; i ++) {//防止两个分区复用,傻瓜式解决办法
        [self.collectionView registerNib:[UINib nibWithNibName:@"MyHomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:[NSString stringWithFormat:@"%@%d",MyHomeCollectionCellID,i]];
    }
    [self.collectionView registerNib:[UINib nibWithNibName:@"MyCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MyHomeReusableView];
    [self.collectionView registerClass:[MyHomeCollectionSecondReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MyHomeSecondReusableView];
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}
#pragma mark UICollectionView DataSource Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return _infoModel.menusTopList.count;
    }
    return _infoModel.menusCenterList.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MyHomeCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NSString stringWithFormat:@"%@%ld",MyHomeCollectionCellID,(long)indexPath.section] forIndexPath:indexPath];
    if (indexPath.section == 0) {
        cell.contentView.backgroundColor = NEColor;
        cell.homeKind.textColor = BColor;
    }else{
        cell.contentView.backgroundColor = BColor;
        cell.homeKind.textColor = JColor;
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = NCColor;
        [cell.contentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.top.and.bottom.mas_equalTo(0);
            make.width.mas_equalTo(1.1);
        }];
    }
    MyHomeInfoModel *firstModel;
    if (indexPath.section == 0) {
        firstModel = _infoModel.menusTopList[indexPath.item];
    }else{
        firstModel = _infoModel.menusCenterList[indexPath.item];
    }
    cell.homeKind.text = firstModel.name;
    [cell.HomeImage sd_setImageWithURL:[NSURL URLWithString:firstModel.pic]];
    if ([firstModel.keyWord isEqualToString:@"xiaoxi"]) {
        if (!_badgeLabel) {
            _badgeLabel = [UILabel labelWithText:@"0" andFont:10 andTextColor:BColor andTextAlignment:NSTextAlignmentCenter];
            _badgeLabel.backgroundColor = NEColor;
            self.badgeLabel.layer.cornerRadius  = 15 / 2.0;
            self.badgeLabel.layer.masksToBounds = YES;
        }
        [cell.contentView addSubview:_badgeLabel];
        [_badgeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(cell.HomeImage.mas_right).offset(15);
            make.top.mas_equalTo(cell.HomeImage.mas_top).offset(-7.5);
            make.size.mas_equalTo(CGSizeMake(25, 15));
        }];
        [self setupUnreadMessageCount];
    }
    
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MyCollectionReusableView *view = nil;
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
            view = (MyCollectionReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MyHomeReusableView forIndexPath:indexPath];
        }
        [view.signBtn addTarget:self action:@selector(handleSignAction) forControlEvents:UIControlEventTouchUpInside];
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleUserInfoAction:)];
        //        [view addGestureRecognizer:tap];
        [view setValueWithModel];
        return view;
    }else{
        MyHomeCollectionSecondReusableView *view = nil;
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
            view = (MyHomeCollectionSecondReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MyHomeSecondReusableView forIndexPath:indexPath];
        }
        return view;
    }
}
//UICollectionView被选中时调用的方法
-(void)collectionView:( UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    MyHomeInfoModel *firstModel;
    if (indexPath.section == 0) {
        firstModel = _infoModel.menusTopList[indexPath.item];
    }else if (indexPath.section == 1) {
        firstModel = _infoModel.menusCenterList[indexPath.item];
    }
    if (firstModel.webUrl.length != 0) {//如果weburl不为空就跳转对应的web
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:firstModel.webUrl andNavTitle:firstModel.name];
        [self.navigationController pushViewController:web animated:YES];
    }else{
        if ([firstModel.keyWord isEqualToString:@"xuexirenwu"]) {//学习任务
            LearningTasksViewController *task = [[LearningTasksViewController alloc]init];
            [self.navigationController pushViewController:task animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"wodexuetuan"] || [firstModel.keyWord isEqualToString:@"wodebanji"]){//我的班级
            MineClassGropViewController *group = [[MineClassGropViewController alloc]init];
            [self.navigationController pushViewController:group animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"wodedingdan"]){//我的订单
            MyOrderVC *targetVC = [[MyOrderVC alloc]init];
            [self.navigationController pushViewController:targetVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"wodexuebi"]){//我的学币
            MineWalletViewController *wallet = [[MineWalletViewController alloc]init];
            [self.navigationController pushViewController:wallet animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"biji"]){//笔记
            MyNotesViewController *myNotesVC = [[MyNotesViewController alloc]init];
            [self.navigationController pushViewController:myNotesVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"tushu"]){//图书
            MyBooksViewController *myBooksVC = [[MyBooksViewController alloc]init];
            [self.navigationController pushViewController:myBooksVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"xunzhang"]){//勋章
            MyMedalViewController *myMedalVC = [[MyMedalViewController alloc]init];
            [self.navigationController pushViewController:myMedalVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"shezi"]){//设置
            MeMoreViewController *more = [[MeMoreViewController alloc]init];
            [self.navigationController pushViewController:more animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"dongtai"]){//动态
            UserNewsVC *vc = [[UserNewsVC alloc]initWithShowMessageType:GetMessagesTypeGetMyCircle andClassId:@""];
            [self.navigationController pushViewController:vc animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"xiaoxi"]){//消息
            MyMessageViewController *targetVC = [[MyMessageViewController alloc]init];
            [self.navigationController pushViewController:targetVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"tongzuo"]){//同桌
            DeskmateViewController *targetVC = [[DeskmateViewController alloc]init];
            [self.navigationController pushViewController:targetVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"gongxiang"]){//共享
            MySharedViewController *mySharedVC = [[MySharedViewController alloc]init];
            [self.navigationController pushViewController:mySharedVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"zibo"]){//直播
            MyZhiBoVC * targetVC = [[MyZhiBoVC alloc] init];
            [self.navigationController pushViewController:targetVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"guanzhu"]){//关注
            MyCareVC * targetVC = [[MyCareVC alloc] init];
            [self.navigationController pushViewController:targetVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"shouchang"]){//收藏
            MyMainCollectionViewController * targetVC = [[MyMainCollectionViewController alloc] init];
            [self.navigationController pushViewController:targetVC animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"feedback"]){//意见反馈
            FeedBackViewCtrl *contrl =[FeedBackViewCtrl new];
            [self.navigationController pushViewController:contrl animated:YES];
        }else if ([firstModel.keyWord isEqualToString:@"jifen"]){
            MyIntegraListViewController *integarVC = [[MyIntegraListViewController alloc]init];
            [self.navigationController pushViewController:integarVC animated:YES];
        }
    }
    
}


#pragma mark === UICollectionViewDelegateFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(kCellMargin, 10.0, kCellMargin, 10.0);
    }
    return UIEdgeInsetsMake(kCellColumns, kCellMargin, kCellColumns, kCellMargin);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        CGFloat width = (KScreenWidth - 20.0 - kCellMargin *(cellNum + 1) ) / _infoModel.menusTopList.count  * 1.0;
        CGFloat height = width * 200 / 180;
        if (_infoModel.menusTopList.count < cellNum) {
            height = (KScreenWidth - 20.0 - kCellMargin *(cellNum + 1) ) / cellNum  * 1.0 * 200 / 180;
        }
        return CGSizeMake(width, height);
    }
    return CGSizeMake((KScreenWidth - kCellMargin *(cellNum + 1)) / cellNum, (KScreenWidth - kCellColumns *(cellNum + 1)) / cellNum);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(KScreenWidth, 150);
    }
    return CGSizeMake(KScreenWidth, 10);
}

-(void)setupUnreadMessageCount {
    [self.root setupUnreadMessageCount];
    self.badgeLabel.hidden = YES;
    __block int unreadCount = 0;
    NSArray * conversationArr = [[EMClient sharedClient].chatManager getAllConversations];
    
    [conversationArr enumerateObjectsUsingBlock:^(EMConversation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        unreadCount += obj.unreadMessagesCount;
        if (unreadCount >0) {
            self.badgeLabel.hidden = NO;
            if (unreadCount > 99) {
                self.badgeLabel.text = [NSString stringWithFormat:@"99+"];
            }else{
                self.badgeLabel.text = [NSString stringWithFormat:@"%d", unreadCount];
            }
        }
    }];
}

//#pragma mark === 个人信息
//-(void)handleUserInfoAction:(UITapGestureRecognizer *)tap
//{
//    MyInfoVC * targetVC = [[MyInfoVC alloc] init];
//    [self.navigationController pushViewController:targetVC animated:YES];
//}
#pragma mark - 签到
- (void)handleSignAction
{
    YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:[NSString stringWithFormat:@"%@%@",[InitConfigModel shareInstance].userSign_url,KgetUserValueByParaName(USERID)] andNavTitle:@"我的签到"];
    web.isSign = YES;
    [self.navigationController pushViewController:web animated:YES];
}

-(void)refreshDataSource
{
    [self setupUnreadMessageCount];
}

-(void)refreshMyHomeData:(NSNotification *)nofi
{
    [self configData];
}

#pragma mark === 获取个人信息,刷新签到按钮
-(void)requestUserInfo
{
    if (NOEmptyStr(KgetUserValueByParaName(USERID))) {
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{Service:GetBaseInfo,@"userId":KgetUserValueByParaName(USERID)} andBlocks:^(NSDictionary *result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                UserInfoModel * user = [UserInfoModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                [UserInfoModel saveUserInfoModelWithModel:user];
                NSDictionary *listDic = [result objectForSafeKey:REQUEST_INFO];
                KsetUserValueByParaName(listDic, HCmyDyItems);
                if ([[result objectForSafeKey:REQUEST_INFO] isKindOfClass:[NSDictionary class]]) {
                    //特殊学员(brand_id>0)个人信息中的资源 需求 来往城市 行业 不显示；链城小站不显示
                    KsetUserValueByParaName([listDic objectForSafeKey:@"brandId"], KBrandInfoId);
                }
            }
            [self.collectionView reloadData];
        }];
    }
}

- (void)dealloc
{
    KRemoverNotifi(@"refreshConversionDataSource");
    KRemoverNotifi(@"setupUnreadMessageCount");
    KRemoverNotifi(NOTIGICATIONFINDVVIEWRELOAD);
    KRemoverNotifi(@"signWebRefresh");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
