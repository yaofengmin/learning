//
//  MyHomeViewController.h
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"
@class YhjRootViewController;
@interface MyHomeViewController : YHBaseViewController
@property (nonatomic,weak) YhjRootViewController *root;
-(void)setupUnreadMessageCount;

@end
