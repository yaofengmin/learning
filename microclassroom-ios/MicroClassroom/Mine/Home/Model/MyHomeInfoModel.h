//
//  MyHomeInfoModel.h
//  MicroClassroom
//
//  Created by yfm on 2017/12/26.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface MyHomeInfoModel : YHBaseModel
@property (nonatomic ,copy) NSString *name;
@property (nonatomic ,copy) NSString *keyWord;
@property (nonatomic ,copy) NSString *pic;
@property (nonatomic ,copy) NSString *ctype;
@property (nonatomic ,copy) NSString *webUrl;//如果是web,直接跳转url


//启动页
@property (nonatomic ,copy) NSString *brandId;
@property (nonatomic ,copy) NSString *brandName;
@property (nonatomic ,copy) NSString *logo;
@end
