//
//  MyBooksTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyBooksTableViewCell.h"

@implementation MyBooksTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.bookImage.backgroundColor = ImageBackColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setModel:(MicroClassBooksDetailModel *)model
{
    [self.bookImage sd_setImageWithURL:[NSURL URLWithString:model.videoLogo]];
    self.bookTitle.text = model.videoTitle;
    self.author.text = [NSString stringWithFormat:@"作者 : %@",model.author];
    self.Press.text = [NSString stringWithFormat:@"出版社 : %@",model.publishHouse];
    self.introLabel.text = model.videoDesc;
    
}
@end
