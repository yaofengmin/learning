//
//  MyBooksDetailTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MicroClassBannerSegmentModel.h"

@interface MyBooksDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;
@property (weak, nonatomic) IBOutlet UILabel *Press;
@property (weak, nonatomic) IBOutlet UILabel *pressTime;
@property (weak, nonatomic) IBOutlet UILabel *Introduction;
@property (weak, nonatomic) IBOutlet UILabel *author;
@property (weak, nonatomic) IBOutlet UILabel *authorIntroduction;

@property (nonatomic,strong) MicroClassBooksDetailModel *model;
@end
