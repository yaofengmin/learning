//
//  MyBooksDetailTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyBooksDetailTableViewCell.h"

@implementation MyBooksDetailTableViewCell

- (void)awakeFromNib {
	[super awakeFromNib];
	self.bookImage.backgroundColor = ImageBackColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}


-(void)setModel:(MicroClassBooksDetailModel *)model
{
    [self.bookImage sd_setImageWithURL:[NSURL URLWithString:model.videoLogo]];
    self.bookTitle.text = model.videoTitle;
    self.Press.text = [NSString stringWithFormat:@"出版社 : %@",model.publishHouse];
    self.pressTime.text = [NSString stringWithFormat:@"出版时间 : %@",model.publishDate];
    self.Introduction.text = model.videoDesc;
    self.author.text = [NSString stringWithFormat:@"作者 : %@",model.author];
    self.authorIntroduction.text = model.authorDes;
    
    
}
@end
