//
//  MyBooksDetailViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyBooksDetailViewController.h"

#import "MyBooksDetailTableViewCell.h"

static NSString *MybooksDetailCellID = @"MyBooksDetailCellID";
@interface MyBooksDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) MicroClassBooksDetailModel *mainModel;
@property (nonatomic ,strong) UITableView *tableV;
@end

@implementation MyBooksDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = _sc_title;
    [self initalTable];
    [self pullData];
}
- (void)pullData {
    NSMutableDictionary *paramDic = @{@"service": GetVideoInfo,
                                      @"videoId": self.videoId
                                      }.mutableCopy;
    
    if (KgetUserValueByParaName(USERID)) {
        [paramDic setObject:KgetUserValueByParaName(USERID) forKey:@"user_Id"];
    }
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                _mainModel = [MicroClassBooksDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                [self.tableV reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 450;
    self.tableV.tableFooterView = [[UIView alloc]init];
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"MyBooksDetailTableViewCell" bundle:nil] forCellReuseIdentifier:MybooksDetailCellID];
    
}


#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyBooksDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MybooksDetailCellID forIndexPath:indexPath];
    cell.model = self.mainModel;
    return cell;
}

#pragma mark === UITableViewDelegate

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
