//
//  MyBooksViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyBooksViewController.h"

#import "MyBooksDetailViewController.h"

#import "MyBooksTableViewCell.h"

#import "MicroClassBannerSegmentModel.h"

#import "BooksDetailViewController.h"
@interface MyBooksViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSArray *dataArr;
@property (nonatomic ,strong) UITableView *tableV;
@property (nonatomic ,strong) DIYTipView *tipView;

@end

static NSString *MyBooksCellID = @"MyBooksCellID";
@implementation MyBooksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"我的图书";
    [self initalTable];
    [self requestList];
}
- (void)requestList {
    NSDictionary *paramDic = @{Service:GetMyReadVideoList,
                               @"user_Id":KgetUserValueByParaName(USERID)};
    SHOWHUD;
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        @strongify(self);
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.dataArr = [MicroClassBooksDetailModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.tableV reloadData];
            }
//            else{
//                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
//            }
            if (self.dataArr.count==0) {
                if (!self.tipView) {
                    self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                }
                [self.tableV addSubview:self.tipView];
                [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(self.view);
                    make.size.equalTo(CGSizeMake(120, 120));
                }];
            }else
            {
                if (self.tipView.superview) {
                    [self.tipView removeFromSuperview];
                }
            }
        }
    }];
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 200;
    self.tableV.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"MyBooksTableViewCell" bundle:nil] forCellReuseIdentifier:MyBooksCellID];
    
}


#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyBooksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyBooksCellID forIndexPath:indexPath];
    cell.model = self.dataArr[indexPath.row];
    return cell;
}

#pragma mark === UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MicroClassBooksDetailModel *model = self.dataArr[indexPath.row];
    BooksDetailViewController *myBooksDetailVC = [[BooksDetailViewController alloc]init];
//    myBooksDetailVC.sc_title = model.videoTitle;
    myBooksDetailVC.videoId = model.videoId;
    [self.navigationController pushViewController:myBooksDetailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
