//
//  MyBooksDetailViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

#import "MicroClassBannerSegmentModel.h"

@interface MyBooksDetailViewController : YHBaseViewController

@property (nonatomic,copy) NSString *videoId;
@property (nonatomic,copy) NSString *sc_title;
@end
