//
//  LevelModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface LevelModel : YHBaseModel
@property (nonatomic, copy) NSString *levelName;

@property (nonatomic, copy) NSString *money;

@property (nonatomic, copy) NSString *level;

@property (nonatomic, copy) NSString *photo;

@property (nonatomic, copy) NSString *remark;
@end
