//
//  BuyVIPViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "BuyVIPViewController.h"
#import "LevelInfoCell.h"
#import "LevelModel.h"
#import "MineWalletViewController.h"
@interface BuyVIPViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSArray     *dataSouce;
@end
static NSString *cellID        = @"LevelInfoCell";
@implementation BuyVIPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MainBackColor;
    self.yh_navigationItem.title = @"会员等级";
    [self creatTabelView];
    [self requestVIPPayList];
    // Do any additional setup after loading the view.
}

- (void)creatTabelView {
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    
}

- (void)requestVIPPayList {
    NSDictionary *paramDic = @{Service:AppGetUserLevelSet,
                               @"user_Id":KgetUserValueByParaName(USERID)
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
              
                _dataSouce = [LevelModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.tableView reloadData];
            }else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }

        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     LevelModel *model = [self.dataSouce objectAtIndex:indexPath.section];
    CGFloat textHight = [YHJHelp sizeWithWidth:KScreenWidth*0.9 andFont:Font(14) andString:model.remark].height;
    CGFloat cellH = 150 + textHight;
    return  cellH ;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSouce.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LevelInfoCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LevelInfoCell" owner:nil options:nil]lastObject];
    }

    if (self.dataSouce.count) {
        LevelModel *model = [self.dataSouce objectAtIndex:indexPath.section];
        [cell configWithModel:model];
        @weakify(self);
        cell.buySuccess = ^{  //购买成功刷新
            @strongify(self);
            [self requestVIPPayList];
        };
        
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.001;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
