//
//  LevelInfoCell.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "LevelInfoCell.h"
#import "LevelModel.h"
#import <UIImageView+WebCache.h>
#import "MineWalletViewController.h"
@interface LevelInfoCell()

@property(nonatomic,strong) LevelModel *model;

@end

@implementation LevelInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.buyBtn.layer.cornerRadius = 4;
    self.buyBtn.layer.masksToBounds = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.priceLabel.hidden = YES;
}

- (void)configWithModel:(LevelModel*)cellModel {
    
    _model     =cellModel;
    self.desLabel.text                 = cellModel.remark;
    self.VIPlabel.text                 = [NSString stringWithFormat:@"VIP%@",cellModel.level];
//    self.priceLabel.text               = cellModel.money;
    self.headImageView.backgroundColor = ImageBackColor;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:cellModel.photo]];
   
    @weakify(self);
    [self.buyBtn handleEventTouchUpInsideWithBlock:^{
        @strongify(self);
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"您确定要购买VIP%@吗?",_model.level] preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
//        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self pay];
//        }]];
//
//        [self.viewController presentViewController:alert animated:YES completion:nil];
        [self callAction];
    }];

}

- (void)pay {
    
    //配置productInfo
    NSDictionary *productInfo = @{@"1":@{@"userNote":@"",
                                        @"items":@[@{
                                        @"outItemId":_model.level,
                                        @"buyAmount":@1,
                                        @"outItemType":@3
                                        }
                                        ]}};
    
    NSDictionary *paramDic = @{@"service": PayForProducts,
                               @"userId": KgetUserValueByParaName(USERID),
                               @"productsInfo": productInfo.mj_JSONString};
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewController.view animated:YES];
    hud.labelText = @"前往购买中请稍候...";
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        [hud hide:YES];
        //业务逻辑层
        if (result) {
            [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                if (self.buySuccess) {
                    self.buySuccess();
                }
            }else if([result[REQUEST_CODE] isEqualToNumber:@2]){
                @weakify(self);
                [YHJHelp aletWithTitle:@"提示" Message:@"帐号学币不足，是否前往充值" sureTitle:@"充值" CancelTitle:@"取消" SureBlock:^{
                    @strongify(self);
                    MineWalletViewController *contr = [[MineWalletViewController alloc]init];
                    [self.viewController.navigationController pushViewController:contr animated:YES];
                } andCancelBlock:nil andDelegate:self.viewController];
                
            }else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
        }
    }];

}

- (void)callAction
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",[InitConfigModel shareInstance].userBuyTel]]];
}

@end
