//
//  LevelInfoCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseTableViewCell.h"

@interface LevelInfoCell : YHBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *VIPlabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *desLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (copy, nonatomic) void(^buySuccess)();

@end
