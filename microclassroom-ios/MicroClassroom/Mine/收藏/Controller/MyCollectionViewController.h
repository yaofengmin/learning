//
//  MyCollectionViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"
#import "MyMainCollectionViewController.h"

@interface MyCollectionViewController : YHBaseViewController
@property(nonatomic, weak) MyMainCollectionViewController *parentContrl;

@end
