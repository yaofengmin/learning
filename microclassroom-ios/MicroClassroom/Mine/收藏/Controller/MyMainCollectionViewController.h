//
//  AskAndShareViewController.h
//  yanshan
//
//  Created by fm on 2017/8/13.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "FJSlidingController.h"

@interface MyMainCollectionViewController : FJSlidingController
@property (nonatomic, copy) void(^sureBlock)(NSString *helpStatus,NSString *labelId);

@end
