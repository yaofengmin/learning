//
//  MyCollectionViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyCollectionViewController.h"

#import "MicroClassListCell.h"
#import "MicroClassBookListTableViewCell.h"

#import "MicroClassBannerSegmentModel.h"

#import "BooksDetailViewController.h"
#import "MicroClassDetailViewController.h"
#import "YHBaseWebViewController.h"


@interface MyCollectionViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSArray *dataArr;
@property (nonatomic ,strong)UITableView *tableV;

@property (nonatomic ,strong) DIYTipView *tipView;
@end
static NSString *MyCollectionCellID = @"MicroClassListCell";
static NSString *MyMicroClassBookListCellID = @"MicroClassBookListCellID";

@implementation MyCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initalTable];
    [self requestList];
    
}
- (void)requestList {
    NSDictionary *paramDic = @{Service:MyCollections,
                               @"ctype":@"1",
                               @"userId":KgetUserValueByParaName(USERID)};
    SHOWHUD;
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        @strongify(self);
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.dataArr = [MicroClassVideoModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.tableV reloadData];
            }
//            else{
//                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
//            }
            if (self.dataArr.count==0) {
                if (!self.tipView) {
                    self.tipView = [[DIYTipView alloc]initWithInfo:result[REQUEST_MESSAGE] andBtnTitle:nil andClickBlock:nil];
                }
                [self.tableV addSubview:self.tipView];
                [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                    
                    make.center.equalTo(self.view);
                    make.size.equalTo(CGSizeMake(120, 120));
                    
                }];
            }else
            {
                if (self.tipView.superview) {
                    [self.tipView removeFromSuperview];
                }
            }
            
        }
    }];
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 120.0;
    self.tableV.backgroundColor = NCColor;
    self.tableV.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassListCell" bundle:nil] forCellReuseIdentifier:MyCollectionCellID];
    [self.tableV registerNib:[UINib nibWithNibName:@"MicroClassBookListTableViewCell" bundle:nil] forCellReuseIdentifier:MyMicroClassBookListCellID];
    
}


#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MicroClassVideoModel *model = self.dataArr[indexPath.row];
    if (model.isMp3 == MicClassMp3TypeBook) {
        MicroClassBookListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyMicroClassBookListCellID forIndexPath:indexPath];
        [cell configDetailWithModel:model];
        return cell;
    }else{
        MicroClassListCell *cell = [tableView dequeueReusableCellWithIdentifier:MyCollectionCellID forIndexPath:indexPath];
        [cell configWithModel:self.dataArr[indexPath.row]];
        return cell;
    }
}

#pragma mark === UITableViewDelegate

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    //    MicroClassVideoModel *model = self.dataArr[indexPath.row];
//    //    if (model.isMp3 == MicClassMp3TypeBook) {
//    //        return 125.0;
//    //    }
//    return 110;
//
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MicroClassVideoModel *model = self.dataArr[indexPath.row];
    if (model.isVip) {//微课里面 如果 isVip ==1，才需要判断 用户是否vip。 如果 isVip ==0，则不管是不是vip，都能看
        if ([[UserInfoModel getInfoModel].grade integerValue] == 0) {
            @weakify(self);
            [YHJHelp aletWithTitle:@"提示" Message:@"您还不是VIP会员，不能观看" sureTitle:@"立即开通 " CancelTitle:@"忽略 " SureBlock:^{
                @strongify(self);
                BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                [self.parentContrl.navigationController pushViewController:buy animated:YES];
                
            } andCancelBlock:nil andDelegate:self.parentContrl];
            return;
        }
    }
    if (model.isMp3 == MicClassMp3TypeWeb) {//web
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.webUrl andNavTitle:model.videoTitle];
        [self.parentContrl.navigationController pushViewController:web animated:YES];
    }else if (model.isMp3 == MicClassMp3TypeBook){//书籍详情
        BooksDetailViewController *detailVC = [[BooksDetailViewController alloc]init];
        detailVC.videoId = model.videoId;
        [self.parentContrl.navigationController pushViewController:detailVC animated:YES];
    }else{
        MicroClassDetailViewController * targetVC = [[MicroClassDetailViewController alloc] init];
        targetVC.videoId = model.videoId;
        [self.parentContrl.navigationController pushViewController:targetVC animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
