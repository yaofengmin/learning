//
//  AskAndShareViewController.m
//  yanshan
//
//  Created by fm on 2017/8/13.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyMainCollectionViewController.h"

#import "MyCollectionViewController.h"//收藏
#import "MyCircleCollectionViewController.h"


@interface MyMainCollectionViewController ()<FJSlidingControllerDataSource,FJSlidingControllerDelegate>
@property (nonatomic, strong)NSArray *titles;
@property (nonatomic, strong)NSArray *controllers;

@end

@implementation MyMainCollectionViewController

- (void)viewDidLoad {
    self.isShowLine = YES;
    self.lineW = 20.0f;
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.yh_navigationItem.title = @"我的收藏";
    self.datasouce = self;
    self.delegate = self;
    [self configController];
}

-(void)configController{
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = KColorFromRGB(0xededed);
    [self.view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 1));
        make.top.mas_equalTo(KTopHeight + SEGMENTHEIGHT);
    }];
    
    MyCollectionViewController *collectionVC = [[MyCollectionViewController alloc]init];
    collectionVC.parentContrl = self;
    
    MyCircleCollectionViewController *CircleColle = [[MyCircleCollectionViewController alloc]initWithShowMessageType:GetMessagesTypeCollection andClassId:@""];
    CircleColle.parentContrl = self;
    self.titles      = @[@"微课",@"动态"];
    self.controllers = @[collectionVC,CircleColle];
    
    [self addChildViewController:collectionVC];
    [self addChildViewController:CircleColle];
    [self reloadData];
}



#pragma mark dataSouce
- (NSInteger)numberOfPageInFJSlidingController:(FJSlidingController *)fjSlidingController{
    return self.titles.count;
}
- (UIViewController *)fjSlidingController:(FJSlidingController *)fjSlidingController controllerAtIndex:(NSInteger)index{
    return self.controllers[index];
}
- (NSString *)fjSlidingController:(FJSlidingController *)fjSlidingController titleAtIndex:(NSInteger)index{
    return self.titles[index];
}


- (UIColor *)titleNomalColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return  DColor;
    
}
- (UIColor *)titleSelectedColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return kNavigationBarColor;
}
- (UIColor *)lineColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return kNavigationBarColor;
}
- (CGFloat)titleFontInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return 16.0;
}

#pragma mark delegate
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedIndex:(NSInteger)index{
    
    self.title = [self.titles objectAtIndex:index];
}

- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedController:(UIViewController *)controller{
    // presentController
}
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedTitle:(NSString *)title{
    // presentTitle
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
