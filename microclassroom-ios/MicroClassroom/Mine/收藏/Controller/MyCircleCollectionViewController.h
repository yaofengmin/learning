//
//  MyCircleCollectionViewController.h
//  yanshan
//
//  Created by fm on 2017/9/12.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHCirleBaseViewController.h"
#import "MyMainCollectionViewController.h"

@interface MyCircleCollectionViewController : YHCirleBaseViewController
@property(nonatomic, weak) MyMainCollectionViewController *parentContrl;

@end
