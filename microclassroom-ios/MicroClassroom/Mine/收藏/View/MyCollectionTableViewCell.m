//
//  MyCollectionTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyCollectionTableViewCell.h"

@implementation MyCollectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.newsImage.backgroundColor = ImageBackColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
