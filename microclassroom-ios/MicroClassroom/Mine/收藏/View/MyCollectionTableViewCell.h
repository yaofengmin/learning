//
//  MyCollectionTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyCollectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *newsImage;
@property (weak, nonatomic) IBOutlet UIImageView *typeImage;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsTitle;

@property (weak, nonatomic) IBOutlet UILabel *newsTime;
@property (weak, nonatomic) IBOutlet UILabel *seeNum;


@end
