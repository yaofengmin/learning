//
//  MySharedViewController.m
//  yanshan
//
//  Created by fm on 2017/8/20.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MySharedViewController.h"

#import "DownloadWebViewController.h"

#import "ShareDetailListTableViewCell.h"

static NSString *MyShareCellID = @"ShareDetailListCellID";

@interface MySharedViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) UITableView *tableV;
@property (nonatomic ,strong) DIYTipView *tipView;

@end

@implementation MySharedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = NCColor;
    _dataArr = [NSMutableArray array];
    self.yh_navigationItem.title = @"我的共享";
    [self initalTable];
    [self requestList];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)requestList {
    NSDictionary *paramDic = @{Service:MyCollections,
                               @"ctype":@"3",
                               @"userId":KgetUserValueByParaName(USERID)};
    SHOWHUD;
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        @strongify(self);
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.dataArr = [ShareDetailModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                if (self.tableV) {
                    [self.tableV reloadData];
                }
            }else{
                if (self.dataArr.count) {
                    [self.dataArr removeAllObjects];
                }
                if (self.tableV) {
                    [self.tableV reloadData];
                }
//                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
            if (self.dataArr.count==0) {
                if (!self.tipView) {
                    self.tipView = [[DIYTipView alloc]initWithInfo:result[REQUEST_MESSAGE] andBtnTitle:nil andClickBlock:nil];
                }
                [self.tableV addSubview:self.tipView];
                [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(self.view);
                    make.size.equalTo(CGSizeMake(120, 120));
                }];
            }else
            {
                if (self.tipView.superview) {
                    [self.tipView removeFromSuperview];
                }
            }
        }
    }];
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.backgroundColor = [UIColor clearColor];
    self.tableV.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"ShareDetailListTableViewCell" bundle:nil] forCellReuseIdentifier:MyShareCellID];
    
}
#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShareDetailListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyShareCellID forIndexPath:indexPath];
    cell.listModel = self.dataArr[indexPath.row];
    return cell;
}

#pragma mark === UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ShareDetailModel *model = self.dataArr[indexPath.row];
    [self requestDetailNewsWithNewId:model.newsId url:model.detailUrl];
    
}
#pragma mark === 详情
-(void)requestDetailNewsWithNewId:(NSString *)newsId url:(NSString *)detailUrl
{
    NSDictionary *paramDic = @{Service:GetNewsInfo,
                               @"newsId":newsId,
                               @"user_Id":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                ShareDetailModel *model = [ShareDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                DownloadWebViewController *detailVC = [[DownloadWebViewController alloc]initWithUrlStr:model.filePathShow andNavTitle:model.newsName];
                detailVC.detailModel = model;
                [self.navigationController pushViewController:detailVC animated:YES];
                
            }
        }
        
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
