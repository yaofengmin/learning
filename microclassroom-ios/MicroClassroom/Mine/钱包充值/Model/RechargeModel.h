//
//  RechargeModel.h
//  EnergyConservationPark
//
//  Created by Hanks on 16/6/14.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "YHBaseModel.h"

@interface RechargeModel : YHBaseModel

@property (nonatomic ,copy) NSString *balance;
@property (nonatomic ,copy) NSString *apiId;

@end
