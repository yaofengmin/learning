//
//  RecordsListModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/31.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface RecordsListModel : YHBaseModel
//"billType": "alipay",
//"cashAmount": "0.01",
//"virtualAmount": "0.00",
//"outId": "16083015483250177",
//"billTime": "2016-08-30 15:49:25"

@property(nonatomic,copy) NSString *billType;
@property(nonatomic,copy) NSString *cashAmount;
@property(nonatomic,copy) NSString *virtualAmount;
@property(nonatomic,copy) NSString *outId;
@property(nonatomic,copy) NSString *billTime;
@property(nonatomic,copy) NSString *note;
@property(nonatomic,copy) NSString *amountLog;

@end
