//
//  WalletModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface WalletModel : YHBaseModel

@property (nonatomic ,copy) NSString *cashBalance; //现金
@property (nonatomic ,copy) NSString *virtualBalance; //虚拟币
@property (nonatomic ,copy) NSString *withdrawMinMoney; //体现金额

@end
