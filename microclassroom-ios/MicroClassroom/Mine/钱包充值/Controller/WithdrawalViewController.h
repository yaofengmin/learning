//
//  WithdrawalViewController.h
//  pinnc
//
//  Created by Hanks on 16/4/13.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "YHBaseViewController.h"


@interface WithdrawalViewController : YHBaseViewController
-(instancetype) initWithWithdrawalMoney:(NSString *)money andWithdrawType:(PayType)type;
@property (nonatomic ,copy) NSString *withdrawMinMoney;
@end
