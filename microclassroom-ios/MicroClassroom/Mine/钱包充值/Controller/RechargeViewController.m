//
//  MyWalletViewController.m
//  EnergyConservationPark
//
//  Created by Hanks on 16/6/7.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "RechargeViewController.h"
#import "RechargeModel.h"
#import "InitConfigModel.h"
#import "YHBaseWebViewController.h"
#import "IAPTool.h"
#import "GTMBase64.h"
#import "RechargeHeaderView.h"

@class PayItemBtn ;

@protocol PayItemBtnDelegate <NSObject>

-(void)chooseItmeWithItem:(PayItemBtn *)item  andModel:(RechargeModel *)model;

@end

static NSInteger   const cellNum      = 3;


@interface PayItemBtn : UIView

@property(nonatomic , strong)  UILabel *freePriceLabel;
@property(nonatomic , strong)  UILabel *desLabel;
@property(nonatomic , assign)  id <PayItemBtnDelegate> delegate;

@property (nonatomic ,strong) RechargeModel *rechargeModel;

@property (nonatomic, assign , getter=isSelected)   BOOL select;
-(instancetype)initWithModel:(RechargeModel *)model;


@end

@implementation PayItemBtn


-(instancetype)initWithModel:(RechargeModel *)model {
    
    if (self = [super init]) {
        self.rechargeModel = model;
        self.userInteractionEnabled = YES;
        self.layer.cornerRadius = 4;
        self.layer.borderWidth  =1;
        self.layer.borderColor  = NEColor.CGColor;
        self.layer.masksToBounds = YES;
        [self addSubview:self.freePriceLabel];
        [self addSubview:self.desLabel];
        self.freePriceLabel.text  = [NSString stringWithFormat:@"¥ %@",model.balance];
        self.desLabel.text  = [NSString stringWithFormat:@"%@学币",model.balance];
        [self.freePriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(5);
            make.right.mas_equalTo(-6);
        }];
        
        [self.desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.mas_equalTo(12);
        }];
        
        //        @weakify(self);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapAction)];
        
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        //            @strongify(self);
        //
        //            if ([self.delegate respondsToSelector:@selector(chooseItmeWithItem:andModel:)]) {
        //                [self.delegate chooseItmeWithItem:self andModel:model];
        //            }
        //        }];
        
        [self addGestureRecognizer:tap];
        
    }
    
    
    return self;
}

-(void)handleTapAction
{
    if ([self.delegate respondsToSelector:@selector(chooseItmeWithItem:andModel:)]) {
        [self.delegate chooseItmeWithItem:self andModel:self.rechargeModel];
    }
}

-(void)setSelect:(BOOL)select {
    
    _select = select;
    if (select) {
        self.backgroundColor = NEColor;
        self.layer.borderColor  = NEColor.CGColor;
        self.freePriceLabel.textColor = BColor;
        self.desLabel.textColor = BColor;
        
    }else
    {
        self.backgroundColor = BColor;
        self.layer.borderColor  = NEColor.CGColor;
        self.freePriceLabel.textColor = NEColor;
        self.desLabel.textColor = NEColor;
    }
    
    
}

-(UILabel *)freePriceLabel
{
    if (!_freePriceLabel) {
        
        _freePriceLabel = [UILabel labelWithText:@"" andFont:12 andTextColor:NEColor andTextAlignment:0];
    }
    
    return _freePriceLabel;
}

-(UILabel *)desLabel
{
    if (!_desLabel) {
        
        _desLabel = [UILabel labelWithText:@"" andFont:16 andTextColor:NEColor andTextAlignment:0];
    }
    
    return _desLabel;
}


@end

static const CGFloat ItemDistance = 15;

@interface RechargeViewController ()<PayItemBtnDelegate>

@property (nonatomic ,strong) UIScrollView   *scrolView;
@property (nonatomic ,strong) UILabel        *balanceLabel;
@property (nonatomic ,strong) NSMutableArray *itemArr;
@property (nonatomic ,strong) RechargeModel  *tempModel;
@property (nonatomic ,assign) BOOL           isAgree;
@property (nonatomic ,strong) PayItemBtn     *lastItem;
@property (nonatomic ,strong) DIYButton       *payBtn;
@property (nonatomic ,strong) MBProgressHUD   *viewHud;
@property (nonatomic ,strong) RechargeHeaderView *headerV;
@property (nonatomic ,strong) UILabel     *sureBlanceLaebl;//支付金额

@end

@implementation RechargeViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.yh_navigationItem.title = @"充值中心";
    [self.view addSubview:self.scrolView];
    [_scrolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    [self configPayItem];
    [self readProtocolPart];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.scrolView.contentSize = CGSizeMake(KScreenWidth, 700);
}

#pragma mark === 中间按钮布局 ===
-(void)configPayItem
{
    
    _headerV = [[NSBundle mainBundle] loadNibNamed:@"RechargeHeaderView" owner:self options:nil].firstObject;
    _headerV.amountLabel.text = self.balanceMoney;
    [self.scrolView addSubview:_headerV];
    [_headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 100));
        make.top.mas_equalTo(0);
        make.centerX.mas_equalTo(0);
    }];
    
    UILabel *middleTitleLable = [UILabel labelWithText:@"充值" andFont:14 andTextColor:JColor andTextAlignment:0];
    [self.scrolView addSubview:middleTitleLable];
    
    [middleTitleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(15);
        make.top.equalTo(_headerV.mas_bottom).offset(15);
    }];
    
    //初始化在上面,为了刚进来的时候可以默认显示
    _sureBlanceLaebl = [UILabel labelWithText:@"支付金额 :" andFont:16 andTextColor:NEColor andTextAlignment:NSTextAlignmentCenter];
    
    
    NSArray<RechargeModel *> *showList = [InitConfigModel shareInstance].topup_virtual;
    
    _itemArr = @[].mutableCopy;
    
    for (int i = 0; i< showList.count; i++) {
        
        RechargeModel *model = showList[i];
        PayItemBtn *item = [[PayItemBtn alloc]initWithModel:model];
        item.delegate = self;
        [self.scrolView addSubview:item];
        [_itemArr addObject:item];
        if (i== 0) {
            [item.delegate chooseItmeWithItem:item andModel:model];
        }
    }
    __block int row_index=0;
    for (int i = 0; i< _itemArr.count; i++) {
        [_itemArr[i] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(CGSizeMake((KScreenWidth - (cellNum + 1) *ItemDistance) / cellNum, 60));
            if (i % cellNum == 0 ) {
                make.left.equalTo(ItemDistance);
                row_index ++;
            }else
            {
                make.left.mas_equalTo([_itemArr[i - 1]  mas_right]).offset(ItemDistance);
                //                make.right.equalTo(self.view.mas_right).offset(-ItemDistance);
            }
            if (i < cellNum) {
                make.top.equalTo(middleTitleLable.mas_bottom).offset(ItemDistance);
            }else{
                make.top.equalTo([_itemArr[i -cellNum] mas_bottom]).offset(ItemDistance);
            }
        }];
        
        
        _lastItem = _itemArr[i];
    }
    
    [self.scrolView addSubview:_sureBlanceLaebl];
    [_sureBlanceLaebl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_lastItem.mas_bottom).offset(30);
        make.centerX.mas_equalTo(0);
    }];
    [self.scrolView layoutIfNeeded];
}


#pragma mark ==阅读协议==
-(void)readProtocolPart {
    UIButton *agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [agreeBtn setImage:[UIImage imageNamed:@"protical_on"] forState:UIControlStateSelected];
    [agreeBtn setImage:[UIImage imageNamed:@"protical_off"] forState:UIControlStateNormal];
    [agreeBtn addTarget:self action:@selector(agreeBtnDown:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrolView addSubview:agreeBtn];
    [self agreeBtnDown:agreeBtn];
    
    [agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.size.equalTo(CGSizeMake(44, 44));
        //        make.top.equalTo(CGRectGetMaxY(_lastItem.frame) + 40);
        make.top.equalTo(_sureBlanceLaebl.mas_bottom).offset(11);
    }];
    
    UIButton *protocolbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [protocolbtn setTitle:@"我已阅读《平台充值服务协议》" forState:UIControlStateNormal];
    [protocolbtn setTitleColor:KColor forState:UIControlStateNormal];
    protocolbtn.titleLabel.font = Font(11);
    [protocolbtn sizeToFit];
    
    [protocolbtn addTarget:self action:@selector(showProtocol) forControlEvents:UIControlEventTouchUpInside];
    [self.scrolView addSubview:protocolbtn];
    
    [protocolbtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(agreeBtn.mas_centerY);
        make.left.equalTo(agreeBtn.mas_right).offset(0);
        make.height.mas_equalTo(44);
        
    }];
    
    
    //    UILabel *noticeLable = [UILabel labelWithText:@"注意事项" andFont:16 andTextColor:EColor andTextAlignment:0];
    //    [self.scrolView addSubview:noticeLable];
    //
    //    [noticeLable mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.left.equalTo(25);
    //        make.top.equalTo(agreeBtn.mas_bottom).offset(30);
    //
    //    }];
    
    
    // 注意事项说明
    
    
    //    UILabel *illustrateLabel = [UILabel new];
    //    illustrateLabel.font = Font(11);
    //    illustrateLabel.numberOfLines = 0;
    //    illustrateLabel.textColor = kNavigationBarColor;
    //    illustrateLabel.text  = [InitConfigModel shareInstance].topup_meno;
    //    [self.scrolView addSubview:illustrateLabel];
    //
    //    [illustrateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //        make.left.equalTo(noticeLable.mas_left).offset(10);
    //        make.top.equalTo(noticeLable.mas_bottom).offset(15);
    //        make.width.lessThanOrEqualTo(KScreenWidth * 0.7);
    //        make.height.lessThanOrEqualTo(100);
    //    }];
    
    
    _payBtn = [DIYButton buttonWithImage:nil andTitel:@"立即充值" andBackColor:NEColor];
    
    [_payBtn addTarget:self action:@selector(payBtnDown) forControlEvents:UIControlEventTouchUpInside];
    [self.scrolView addSubview:_payBtn];
    
    [_payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.size.equalTo(CGSizeMake(KScreenWidth *0.9, 40));
        make.top.equalTo(protocolbtn.mas_bottom).offset(0);
        make.centerX.equalTo(0);
        
    }];
}


#pragma mark ===发起支付==

-(void)payBtnDown {
    
    if (_isAgree) {
        [self pay];
    }else {
        [WFHudView showMsg:@"请先同意充值协议" inView:self.view];
    }
    
}


-(void)pay {
    // 获取订单号
    NSDictionary *param = @{Service:UserTopupByThird,
                            @"userId":KgetUserValueByParaName(USERID),
                            @"appleProductId":_tempModel.apiId,
                            @"topupAmount":_tempModel.balance,
                            @"otherType":@"appleiap"};
    
    [self showHint:@"正在获取订单号..."];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:param andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                SKProduct *product = [[IAPTool sharedIAPTool] getSKProductByProductId:_tempModel.apiId];
                [[IAPTool sharedIAPTool].iap buyProduct:product onCompletion:^(SKPaymentTransaction *transcation) {
                    [self hideHud];
                    if (transcation.transactionState == SKPaymentTransactionStatePurchasing) {
                        if (transcation.error) {
                            [self showHint:[transcation.error localizedDescription]];
                        } else {
                            [self showHint:@"正在进行充值..."];
                        }
                    } else if (transcation.transactionState == SKPaymentTransactionStatePurchased) {
                        [self showHint:@"正在验证收据..."];
                        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
                        NSData *transactionReceipt = transcation.transactionReceipt;
#pragma clang diagnostic pop
                        NSString *tradeNo = result[REQUEST_INFO][@"tradeNo"];
                        NSString *receiptString = [[NSString alloc] initWithData:transactionReceipt encoding:NSUTF8StringEncoding];
                        NSString *encodeReceipt = [GTMBase64 stringByEncodingBytes:[receiptString UTF8String] length:[receiptString length]];
                        
                        NSDictionary *paramDic = @{Service :PinncApplePayForOk,
                                                   @"userId":KgetUserValueByParaName(USERID),
                                                   @"tradeNo":tradeNo,
                                                   @"totalFee":_tempModel.balance,
                                                   @"appleNo":encodeReceipt};
                        
                        
                        SHOWHUD;
                        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                            HIDENHUD;
                            if (result) {
                                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
                            }
                        }];
                        
                    } else if (transcation.transactionState == SKPaymentTransactionStateFailed) {
                        if (transcation.error) {
                            alertContent(@"购买失败");
                        }
                    }
                }];
                
            }
        }
    }];
}



#pragma mark ==跳转协议==
-(void)showProtocol {
    NSString *urlStr = [InitConfigModel shareInstance].topup_url;
    
    YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:urlStr andNavTitle:@"平台充值服务协议"];
    [self.navigationController pushViewController:web animated:YES];
    
}



-(void)agreeBtnDown:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    _isAgree = sender.selected;
    
}


-(void)chooseItmeWithItem:(PayItemBtn *)item andModel:(RechargeModel *)model {
    
    if ([_tempModel isEqual:model]) {
        return;
    }
    
    for (PayItemBtn *ietm in _itemArr) {
        
        ietm.select = NO;
    }
    
    [item setSelect:YES] ;
    _tempModel = model;
    self.sureBlanceLaebl.text = [NSString stringWithFormat:@"支付金额:  %@元",_tempModel.balance];
    
}


-(UIScrollView *)scrolView
{
    if (!_scrolView) {
        
        _scrolView = [UIScrollView new];
        _scrolView.backgroundColor = [UIColor whiteColor];
        _scrolView.showsVerticalScrollIndicator =  NO;
    }
    
    return _scrolView;
}





@end
