
//
//  MineWalletViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MineWalletViewController.h"
#import "WalletModel.h"
#import "WalletPayListCell.h"
#import "RecordsListModel.h"
#import "WithdrawalViewController.h"
#import "RechargeViewController.h"
#import "MyWalletFirstTableViewCell.h"


@interface MineWalletViewController()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) WalletModel *model;
@property (nonatomic,strong) NSArray *dataSource;
@property (nonatomic,strong) UIView *bottomView;
@property (nonatomic,strong) YHBarButtonItem *rightBtn;
@property (nonatomic,strong) UILabel *blanceLabel;
@property (nonatomic,strong) UIButton *RechargeBtn;

@end

@implementation MineWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"我的学币";
    self.view.backgroundColor = MainBackColor;
    [self creatTabelView];
    // [self creatBottomUI];
    
    //    @weakify(self);
    //    _rightBtn = [[YHBarButtonItem alloc]initWithTitle:@"提现" style:0 handler:^(id send) {
    //        @strongify(self);
    //
    //        UIActionSheet *testSheet = [UIActionSheet bk_actionSheetWithTitle:@"选择提现方式"];
    //        [testSheet bk_addButtonWithTitle:@"支付宝提现" handler:^{
    //
    //            WithdrawalViewController *pay = [[WithdrawalViewController alloc]initWithWithdrawalMoney:self.model.cashBalance andWithdrawType:PayTpyeAlipy];
    //            pay.withdrawMinMoney = self.model.withdrawMinMoney;
    //            [self.navigationController pushViewController:pay animated:YES];
    //        }];
    //
    //        [testSheet bk_addButtonWithTitle:@"微信提现" handler:^{
    //
    //            WithdrawalViewController *pay = [[WithdrawalViewController alloc]initWithWithdrawalMoney:self.model.cashBalance andWithdrawType:PayTypeWeiXinPay];
    //            pay.withdrawMinMoney = self.model.withdrawMinMoney;
    //            [self.navigationController  pushViewController:pay animated:YES];
    //        }];
    //
    //        [testSheet bk_setCancelButtonWithTitle:@"取消" handler:nil];
    //
    //        [testSheet showInView:self.view];
    //    }];
    //
    //    self.yh_navigationItem.rightBarButtonItem = _rightBtn;
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestWallet];
    [self requestWalletList];
}


- (void)requestWallet {
    
    NSDictionary *paramDic = @{Service:UserGetBalance,
                               @"userId":KgetUserValueByParaName(USERID)
                               };
    
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _model = [WalletModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                if ([_model.cashBalance floatValue] - [_model.withdrawMinMoney floatValue] > 0 ) {
                    _rightBtn.button.hidden = NO;
                }else {
                    _rightBtn.button.hidden = YES;
                }
                [self.tableView reloadData];
            }
        }
    }];
}


- (void)requestWalletList {
    
    NSDictionary *paramDic = @{Service:UserGetBalanceAndRecords,
                               @"userId":KgetUserValueByParaName(USERID)
                               };
    
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _dataSource = [RecordsListModel mj_objectArrayWithKeyValuesArray:result[REQUEST_INFO][@"records"]];
                if (_dataSource.count == 0) {
                    
                }else {
                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:0];
                }
            }
        }
    }];
}

- (void)creatTabelView {
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 60, 0);
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}




#pragma mark == tableViewAbout===

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else {
        return self.dataSource.count;
        return 5;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentif;
    if (indexPath.section == 0) {
        cellIdentif = @"MyWalletFirstCellID";
    }else{
        cellIdentif = @"WalletPayListCell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentif];
    if (cell == nil) {
        if (indexPath.section == 0) {
            cell  = [[[NSBundle mainBundle]loadNibNamed:@"MyWalletFirstTableViewCell" owner:nil options:nil]lastObject];
        }else {
            
            cell  = [[[NSBundle mainBundle]loadNibNamed:@"WalletPayListCell" owner:nil options:nil]lastObject];
        }
    }
    
    if (indexPath.section == 0) {
        [(MyWalletFirstTableViewCell*)cell configWithModel:_model];
        
        //        if (!_blanceLabel) {
        //            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //            _blanceLabel = [UILabel labelWithText:@"" andFont:18 andTextColor:kNavigationBarColor andTextAlignment:0];
        //            [cell.contentView addSubview:_blanceLabel];
        //            [_blanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        //                make.centerY.equalTo(cell.contentView);
        //                make.left.equalTo(@100);
        //            }];
        //
        //            [cell.contentView addSubview:self.RechargeBtn];
        //            [self.RechargeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        //                make.centerY.equalTo(cell.contentView);
        //                make.right.equalTo(@-20);
        //                make.size.mas_equalTo(CGSizeMake(40, 30));
        //            }];
        //        }
        //        cell.textLabel.font = Font(18);
        //        cell.textLabel.textColor = DColor;
        //        cell.textLabel.text = @"我的学币:";
        //        _blanceLabel.text = [NSString stringWithFormat:@"%@",_model.cashBalance];
    }else {
        
        RecordsListModel *model = [_dataSource objectAtIndex:indexPath.row];
        [(WalletPayListCell*)cell configWithModel:model];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 100;
    }
    return [WalletPayListCell cellHeight];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.0001;
    }else {
        return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0001;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
        view.backgroundColor = NCColor;
        UILabel *lable = [UILabel labelWithText:@"消费记录" andFont:14 andTextColor:JColor andTextAlignment:NSTextAlignmentCenter];
        lable.backgroundColor = BColor;
        [view addSubview:lable];
        [lable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(10, 0, 0, 0));
        }];
        return view;
    }
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


//- (UIButton *)RechargeBtn {
//    if (!_RechargeBtn) {
//        _RechargeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_RechargeBtn setTitle:@"充值" forState:UIControlStateNormal];
//        [_RechargeBtn setTitleColor:KColorFromRGB(0xffa7a6) forState:UIControlStateNormal];
//        _RechargeBtn.titleLabel.font = [UIFont systemFontOfSize:13];
//        _RechargeBtn.layer.cornerRadius = 4.0;
//        _RechargeBtn.layer.borderColor = KColorFromRGB(0xffa7a6) .CGColor;
//        _RechargeBtn.layer.borderWidth = 1;
//        @weakify(self);
//        [_RechargeBtn handleEventTouchUpInsideWithBlock:^{
//            @strongify(self);
//            RechargeViewController *recharge = [[RechargeViewController alloc]init];
//            [self.navigationController pushViewController:recharge animated:YES];
//        }];
//    }
//    return _RechargeBtn;
//}


@end
