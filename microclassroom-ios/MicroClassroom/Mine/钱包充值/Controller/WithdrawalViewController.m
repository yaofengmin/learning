//
//  WithdrawalViewController.m
//  pinnc
//
//  Created by Hanks on 16/4/13.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "WithdrawalViewController.h"
#import "InitConfigModel.h"


@interface WithdrawalViewController()

@property (nonatomic ,copy    ) NSString     *money;
@property (nonatomic ,assign  ) PayType type;
@property (nonatomic ,strong  ) UITextField  *nameF;
@property (nonatomic ,strong  ) UITextField  *carNumbF;
@property (nonatomic ,strong  ) UITextField  *moneyNumbF;



@end

@implementation WithdrawalViewController

-(instancetype) initWithWithdrawalMoney:(NSString *)money andWithdrawType:(PayType)type
{
    if (self = [super init]) {
      
        _money = money  ;
        _type = type;
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    NSString *navTitle ;
    NSString *cardViewHold;
    NSString *nameViewHold;
    
    if (_type == PayTpyeAlipy) {
        
        navTitle     = @"支付宝提现";
        cardViewHold = @"支付宝帐号(手机或邮箱)";
        nameViewHold = @"绑定支付宝帐号的真实姓名";
        
    }else
    {
        navTitle     = @"微信提现";
        cardViewHold = @"微信号";
        nameViewHold = @"绑定微信帐号的真实姓名";
 
    }
    
    
    self.yh_navigationItem.title = navTitle;
  
    _carNumbF = [[UITextField alloc]init];
    _nameF = [[UITextField alloc]init];
    _moneyNumbF = [[UITextField alloc]init];
    
    
    UIView  *cardView = [self viewWithTitle:@"帐号" andField:_carNumbF andPlaceHold:cardViewHold];
    
    [cardView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.equalTo(KTopHeight +10);
        make.size.equalTo(CGSizeMake(KScreenWidth, 50));
        make.centerX.equalTo(0);
        
    }];
    
    
    UIView  *nameView = [self viewWithTitle:@"姓名" andField:_nameF andPlaceHold:nameViewHold];
    
    [nameView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(cardView.mas_bottom).offset(5);
        make.size.equalTo(CGSizeMake(KScreenWidth, 50));
        make.centerX.equalTo(0);
        
    }];
    
    
    UIView  *moneyView = [self viewWithTitle:@"金额" andField:_moneyNumbF andPlaceHold:[NSString stringWithFormat:@"最大可提现金额%@  ",_money]];
    
    [moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(nameView.mas_bottom).offset(5);
        make.size.equalTo(CGSizeMake(KScreenWidth, 50));
        make.centerX.equalTo(0);
        
    }];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"全部提现" forState:UIControlStateNormal];
    [btn setTitleColor:kNavigationBarColor forState:UIControlStateNormal];
    btn.titleLabel.font = Font(13);
    [btn addTarget:self action:@selector(btnDown) forControlEvents:UIControlEventTouchUpInside];
    [moneyView addSubview:btn];
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.right.equalTo(0);
        make.centerY.equalTo(0);
        make.size.equalTo(CGSizeMake(70, 40));
    }];
    
    [self.view layoutIfNeeded];
    
     UILabel *desLabel= [[UILabel alloc]init];
    desLabel.font = Font(13);
    desLabel.textColor = EColor;
    desLabel.numberOfLines = 0;
    desLabel.lineBreakMode = NSLineBreakByWordWrapping;
    desLabel.text =[NSString stringWithFormat:@"最低底可提现金额:%@  ",[InitConfigModel  shareInstance].user_withdraw_min_money] ;
    [self.view addSubview:desLabel];
    
    CGFloat heigt = [YHJHelp sizeWithWidth:KScreenWidth - 30 andFont:desLabel.font andString:desLabel.text].height;
    desLabel.frame = CGRectMake(10, CGRectGetMaxY(moneyView.frame) + 20, KScreenWidth -30, heigt+15);
   
    
    UIButton *sendBtn  = [DIYButton buttonWithImage:nil andTitel:@"提交" andBackColor:MainBtnColor];
    [sendBtn addTarget:self action:@selector(sendBtnDown) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sendBtn];
    
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.size.equalTo(CGSizeMake(KScreenWidth *0.9, 40));
        make.centerX.equalTo(0);
        make.top.equalTo(CGRectGetMaxY(desLabel.frame) + 10);
    }];
    
}


-(void)btnDown
{
    _moneyNumbF.text = _money;
}

#pragma mark ===发起申请==
-(void)sendBtnDown
{
    if ([self checkParam]) {
        
        NSString *withdrawType ;
        if (_type ==PayTpyeAlipy) {
            withdrawType = @"alipay";
        }else
        {
            withdrawType =@"wxpay";
        }
        
        NSDictionary *paramDic = @{Service:UserApplyWithdraw,
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"amount":_moneyNumbF.text,
                                   @"toAccount":_carNumbF.text,
                                   @"name":_nameF.text,
                                   @"type":withdrawType};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    
                    DELAYEXECUTE(0.5, [self backBtnAction]);
                }
                
                 [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
            }
        }];

    }
    
    
    
}


-(BOOL)checkParam
{
    
    
    if (IsEmptyStr(_carNumbF.text)) {
        [self popView:@"请输入有效的帐号信息"];
        return NO;
    }
    
    if (IsEmptyStr(_nameF.text)) {
        
        [self popView:@"请输入有效的姓名信息"];
        return NO;
    }
    
    
    if ([_moneyNumbF.text floatValue]<[self.withdrawMinMoney floatValue]) {
        [WFHudView showMsg:[NSString stringWithFormat:@"最小提现金额: %@",self.withdrawMinMoney] inView:self.view];
        return NO;
    }
    
    
    return YES;
}



-(UIView *)viewWithTitle:(NSString *)title andField:(UITextField *)field andPlaceHold:(NSString *)holdStr
{
    UIView *view             = [[UIView alloc]init];
    view.backgroundColor     = [UIColor whiteColor];
    view.layer.borderColor   = FColor.CGColor;
    view.layer.borderWidth   = 0.5;
    view.layer.masksToBounds = YES;
    
    UILabel *label = [UILabel labelWithText:title andFont:16 andTextColor:CColor andTextAlignment:0];
    [view addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.equalTo(15);
        make.centerY.equalTo(0);
    }];
    
    
    field.placeholder = holdStr;
    field.font = Font(16);
    [view addSubview:field];
    
    [field mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.left.equalTo(65);
        make.centerY.equalTo(0);
        make.right.equalTo(-20);
        make.height.equalTo(45);
        
    }];
    
    [self.view addSubview:view];

    return view;
    
}





@end
