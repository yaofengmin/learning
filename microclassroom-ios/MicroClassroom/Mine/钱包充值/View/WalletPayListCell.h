//
//  WalletPayListCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseTableViewCell.h"

@interface WalletPayListCell : YHBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *payTiemeLabel;
@property (weak, nonatomic) IBOutlet UILabel *payTypeLable;
@property (weak, nonatomic) IBOutlet UILabel *payCrashLabel;
@property (weak, nonatomic) IBOutlet UILabel *SurplusPrice;

@end
