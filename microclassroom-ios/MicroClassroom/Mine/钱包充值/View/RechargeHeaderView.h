//
//  RechargeHeaderView.h
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RechargeHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end
