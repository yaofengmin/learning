//
//  MyWalletFirstTableViewCell.m
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "MyWalletFirstTableViewCell.h"

#import "WalletModel.h"

#import "RechargeViewController.h"

@implementation MyWalletFirstTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.rechargeBtn.layer.cornerRadius = 4.0;
    self.rechargeBtn.layer.masksToBounds = YES;
}
- (IBAction)handleRechargeAction:(UIButton *)sender {
    RechargeViewController *recharge = [[RechargeViewController alloc]init];
    recharge.balanceMoney = _cellModel.cashBalance;
    [self.viewController.navigationController pushViewController:recharge animated:YES];
}


-(void)configWithModel:(WalletModel *)cellModel
{
    _cellModel = cellModel;
    self.moneyLaebl.text = [NSString stringWithFormat:@"%@",cellModel.cashBalance];

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
