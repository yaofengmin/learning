//
//  WalletPayListCell.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "WalletPayListCell.h"
#import "RecordsListModel.h"
@implementation WalletPayListCell


+ (CGFloat)cellHeight {
    return 60;
}


- (void)configWithModel:(RecordsListModel *)cellModel {
    
    self.payTiemeLabel.text = cellModel.billTime;
   
    self.payCrashLabel.text = [NSString stringWithFormat:@"%@",cellModel.cashAmount];
        
//    if ([cellModel.billType isEqualToString:@"alipay"]) {
//        self.payTypeLable.text = @"支付宝";
//    }else if ([cellModel.billType isEqualToString:@"order"]){
//        self.payTypeLable.text = @"订单";
//    }else if ([cellModel.billType isEqualToString:@"wxpay"]){
    self.payTypeLable.text = cellModel.note;;
    self.SurplusPrice.text = [NSString stringWithFormat:@"余额: %@",cellModel.amountLog];

//    }
}

@end
