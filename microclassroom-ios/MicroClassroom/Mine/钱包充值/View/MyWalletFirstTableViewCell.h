//
//  MyWalletFirstTableViewCell.h
//  MicroClassroom
//
//  Created by fm on 2017/11/10.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "YHBaseTableViewCell.h"

@class WalletModel;
@interface MyWalletFirstTableViewCell : YHBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *moneyLaebl;
@property (weak, nonatomic) IBOutlet UIButton *rechargeBtn;

@property (nonatomic ,strong) WalletModel *cellModel;
@end
