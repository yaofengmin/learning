//
//  MyOrderCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MyOrderModel;


@interface MyOrderCell : UITableViewCell
- (void)configureCellWithMyOrderModel:(MyOrderModel *)model Index:(NSIndexPath *)index;
@property(nonatomic ,copy) void(^commentBtnBlock)(MyOrderModel *model);
@end
