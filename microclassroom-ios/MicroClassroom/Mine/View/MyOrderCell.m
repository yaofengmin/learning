//
//  MyOrderCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyOrderCell.h"
#import "MyOrderModel.h"

static NSString * kOrderPayedTitle = @"完成";
static NSString * kOrderCancelTitle = @"订单已取消";
static NSString * kOrderSuccessTitle = @"预约成功";
@interface MyOrderCell ()
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet UIImageView *logoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *payStatusLabel;
@property (nonatomic, strong) NSIndexPath *index;
@property (nonatomic, strong) MyOrderModel *model;
@end

@implementation MyOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.logoImg.backgroundColor = ImageBackColor;
    self.commentBtn.hidden = YES;
    self.commentBtn.layer.cornerRadius = 4;
    self.commentBtn.layer.masksToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)configureCellWithMyOrderModel:(MyOrderModel *)model Index:(NSIndexPath *)index{
    _model = model;
    self.index = index;
    self.orderIdLabel.text = [NSString stringWithFormat:@"订单号：%@", model.orderId];
    if ([model.totalFee floatValue] == 0.00) {
        self.priceLabel.hidden = YES;
    }else{
        self.priceLabel.hidden = NO;
        self.priceLabel.text = [NSString stringWithFormat:@"￥%@", model.totalFee];
    }
    self.dateLabel.text = model.orderTime;
    
    if (model.items.count > 0) {
        OrderItemModel * item = model.items[0];
        
        [self.logoImg sd_setImageWithURL:[NSURL URLWithString:item.itemCover]];
        self.titleLabel.text = item.itemTitle;
        self.timeLabel.text = item.fromTime;
    } else {
        [self.logoImg sd_setImageWithURL:[NSURL URLWithString:@""]];
    }
    
    if (model.items[0].outItemType == MCMyOrderTypeSite) {
        if ([model.totalFee floatValue] == 0.00) {
            self.payStatusLabel.text = @"预约成功";
            self.commentBtn.hidden = YES;
        }else{
            if (model.isComment.intValue == 0) {
                self.commentBtn.hidden = NO;
            }else{
                //显示状态
                self.commentBtn.hidden = YES;
                switch (model.orderStatus) {
                    case MCMyOrderStatusWaitPay:
                        
                        self.payStatusLabel.text = @"";
                        break;
                    case MCMyOrderStatusCancel:
                        
                        self.payStatusLabel.text = kOrderCancelTitle;
                        break;
                    case MCMyOrderStatusPayed:
                        
                        self.payStatusLabel.text = kOrderPayedTitle;
                        break;
                    default:
                        break;
                }
                
            }
        }
    }else {
        self.commentBtn.hidden = YES;
        switch (model.orderStatus) {
            case MCMyOrderStatusWaitPay:
                
                self.payStatusLabel.text = @"";
                break;
            case MCMyOrderStatusCancel:
                
                self.payStatusLabel.text = kOrderCancelTitle;
                break;
            case MCMyOrderStatusPayed:
                
                self.payStatusLabel.text = kOrderPayedTitle;
                break;
            default:
                break;
        }
        
        
    }
    
    
}
- (IBAction)commentBtnAction:(id)sender {
    if (self.commentBtnBlock) {
        self.commentBtnBlock(_model);
    }
}

@end
