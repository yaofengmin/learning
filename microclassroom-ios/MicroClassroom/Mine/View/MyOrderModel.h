//
//  MyOrderModel.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface OrderItemModel: YHBaseModel
@property (nonatomic, strong) NSString *outItemId;
@property (nonatomic, assign) MCMyOrderType outItemType;
@property (nonatomic, strong) NSString *itemTitle;
@property (nonatomic, strong) NSString *itemCover;
@property (nonatomic, strong) NSString *itemBrief;
@property (nonatomic, strong) NSString *unitPrice;
@property (nonatomic, assign) int buyAmount;
@property (nonatomic, strong) NSString *fromTime;
@property (nonatomic, strong) NSString *toTime;
@property (nonatomic, strong) NSString *site_detail_url;
@end

@interface MyOrderModel : YHBaseModel
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *totalFee;
@property (nonatomic, strong) NSString *cashFee;
@property (nonatomic, strong) NSString *virtualFee;
@property (nonatomic, strong) NSString *shouldPay;
@property (nonatomic, assign) int orderStatus;
@property (nonatomic, strong) NSString *orderTime;
@property (nonatomic, strong) NSString *userNote;
@property (nonatomic, strong) NSString *isComment;
@property (nonatomic, strong) NSArray<OrderItemModel *> *items;
@end


