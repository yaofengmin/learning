//
//  MineHeadCell.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/7.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MineHeadCell.h"
#import "UserInfoModel.h"
#import <UIImageView+WebCache.h>

@implementation MineHeadCell


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.nameLabel.textColor = CColor;
    self.Lvlabel.textColor   = DColor;
    self.sigeLabel.textColor = DColor;
    self.headView.backgroundColor = ImageBackColor;
    self.headView.layer.cornerRadius = 30.0f;
    self.headView.layer.masksToBounds = YES;
    
    
}

+(CGFloat)cellHeight {
    return 90;
}


-(void)configWithModel:(id)cellModel {
    
    UserInfoModel *model  = [UserInfoModel getInfoModel];
    if (model == nil) {
        self.nameLabel.text = @"您还未登录";
        self.Lvlabel.text = @"";
        self.sigeLabel.text = @"";
        self.headView.image = nil;
        self.addVImageView.hidden = YES;
    }else{
        [self.headView sd_setImageWithURL:[NSURL URLWithString:model.photo]];
        self.nameLabel.text = model.nickname;
        self.Lvlabel.text = [NSString stringWithFormat:@"Lv:%@",model.grade];
        self.sigeLabel.text = model.personalSign;
        self.addVImageView.hidden = !model.isCertified;
        
    }
    
}

@end
