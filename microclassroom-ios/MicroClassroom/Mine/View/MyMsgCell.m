//
//  MyMsgCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/10.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyMsgCell.h"
#import "FriendModel.h"
@interface MyMsgCell ()

@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation MyMsgCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.badgeLabel.layer.masksToBounds = YES;
    self.HeadImage.layer.cornerRadius = 20.0f;
    self.HeadImage.layer.masksToBounds = YES;
    self.HeadImage.contentMode = UIViewContentModeScaleAspectFit;
//    self.badgeView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/**
 devId = "developuser_30000000000";
 nickName = "\U52a8\U6001\U901a\U77e5";
 noteName = "";
 photo = "http://go2study8.pinnc.com/u/system/dangdang.png";
 userId = 30000000000;
 */


- (void)configureCellWithConversation:(EMConversation *)conversation {
    int unreadCount = conversation.unreadMessagesCount;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.badgeLabel.hidden = YES;
        self.badgeLabel.hidden = unreadCount == 0 ? YES : NO;
        self.badgeLabel.layer.cornerRadius = 7 ;
        self.badgeLabel.text = [NSString stringWithFormat:@"%d", unreadCount];
        self.titleLabel.text = [conversation.ext objectForKey:@"nickname"];
        [self.HeadImage sd_setImageWithURL:[NSURL URLWithString:[conversation.ext objectForKey:@"photo"]]];
    });
}


@end
