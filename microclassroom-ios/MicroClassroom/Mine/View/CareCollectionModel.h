//
//  CareCollectionModel.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface CareModel : YHBaseModel
@property (nonatomic, assign) int userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *personalSign;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *pic;
@end

@interface CollectionModel : YHBaseModel
@property (nonatomic, assign) int collectoinId;
@property (nonatomic, assign) int isMp3;
@property (nonatomic, copy) NSString *videoId;
@property (nonatomic, copy) NSString *videoKeyWord;
@property (nonatomic, copy) NSString *videoTitle;
@property (nonatomic, copy) NSString *videoLength;
@property (nonatomic, copy) NSString *videoLaud;
@property (nonatomic, copy) NSString *videoLogo;
@property (nonatomic, copy) NSString *videoUrl;
@property (nonatomic, copy) NSString *teacherName;
@property (nonatomic, copy) NSString *pic;

@end

