//
//  MyInfoHeaderView.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyInfoHeaderView.h"

#import "UserInfoModel.h"

#import "UIImageView+AFNetworking.h"
@interface MyInfoHeaderView ()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeAvatarBtn;
@property (weak, nonatomic) IBOutlet UIButton *changeAccountBtn;
@property (weak, nonatomic) IBOutlet UILabel *NoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addVImageView;

@end

@implementation MyInfoHeaderView

- (void)awakeFromNib {

    [self.changeAvatarBtn addTarget:nil
                       action:@selector(changeAvatar:)
             forControlEvents:UIControlEventTouchUpInside];
    [self.changeAccountBtn addTarget:nil
                             action:@selector(changeAccount:)
                   forControlEvents:UIControlEventTouchUpInside];
    self.avatarImg.layer.borderColor = [UIColor whiteColor].CGColor;
    [super awakeFromNib];
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MyInfoHeaderView" owner:self options: nil];
        
        if(arrayOfViews.count < 1){
            return nil;
        }
        
        if(![[arrayOfViews objectAtIndex:0] isKindOfClass:[UIView class]]){
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
        self.frame = frame;
    }
    return self;
}

- (void)configureViewWithModel:(UserInfoModel *)model {
    
    self.nameLabel.text = model.nickname;
    self.accountLabel.text = model.email.length > 0 ? model.email : @"未设置邮箱";
    [self.avatarImg sd_setImageWithURL:[NSURL URLWithString:model.photo] placeholderImage:[UIImage imageNamed:@"login_head"]];
    if (NOEmptyStr(model.userCode)) {
        self.NoLabel.text = [NSString stringWithFormat:@"学号: %@",model.userCode];
    }else{
        self.NoLabel.text = @"";
    }
    self.addVImageView.hidden = YES;
//    self.addVImageView.hidden = !model.isCertified;
}

@end
