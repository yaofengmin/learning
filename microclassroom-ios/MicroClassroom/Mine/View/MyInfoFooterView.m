//
//  MyInfoFooterView.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyInfoFooterView.h"

@interface MyInfoFooterView ()
@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;

@end

@implementation MyInfoFooterView

- (void)awakeFromNib {
    
      [super awakeFromNib];
    [self.logoutBtn addTarget:nil
                       action:@selector(logoutAccount:)
             forControlEvents:UIControlEventTouchUpInside];
  
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MyInfoFooterView" owner:self options: nil];
        
        if(arrayOfViews.count < 1){
            return nil;
        }
        
        if(![[arrayOfViews objectAtIndex:0] isKindOfClass:[UIView class]]){
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
        self.frame = frame;
        
    }
    
    return self;
    
}



@end
