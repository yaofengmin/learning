//
//  MyOrderModel.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyOrderModel.h"

@implementation OrderItemModel

@end

@implementation MyOrderModel
+ (NSDictionary *)objectClassInArray{
    return @{
             @"items" : @"OrderItemModel"
             };
}
@end
