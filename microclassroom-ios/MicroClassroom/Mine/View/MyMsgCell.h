//
//  MyMsgCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/10.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FriendModel;
@interface MyMsgCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *HeadImage;
//- (void)configureCell;
- (void)configureCellWithConversation:(EMConversation *)conversation;
@property(nonatomic , strong) FriendModel *friendModel;
@end
