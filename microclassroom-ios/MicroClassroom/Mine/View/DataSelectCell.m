//
//  DataSelectCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//
static NSString * const kSelectedImgName = @"ic_Selected";
static NSString * const kUnSelectImgName = @"ic_selectcircle";
#import "DataSelectCell.h"
#import "CodeListModel.h"
@interface DataSelectCell ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectImg;
@property (weak, nonatomic) IBOutlet UITextField *remarkField;
@property (strong, nonatomic) CodeListModel *model;

@end

@implementation DataSelectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.remarkField.delegate  = self;
    self.remarkField.hidden = YES;
    // Initialization code
    [self.remarkField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithModel:(CodeListModel *)cellModel {
    _model = cellModel;
    if (cellModel.isSelect) {
        if (self.editType != MCEditTypeIndustry) {
             self.remarkField.hidden = NO;
        }
        self.selectImg.image = [UIImage imageNamed:kSelectedImgName];
        self.remarkField.text = cellModel.meno;
    } else {
        
        self.remarkField.hidden = YES;
        self.selectImg.image = [UIImage imageNamed:kUnSelectImgName];
        self.remarkField.text = @"";
    }
    
    self.titleLabel.text = cellModel.codeName;
}

//资源和需求备注， 限制字数20
-(void)textFieldDidChange:(UITextField *)textField
{
    MyLog(@"%@",textField.text);
    if (textField.text.length > 20) {
        [WFHudView showMsg:@"字数超出限制!" inView:self.viewController.view];
        textField.text = [textField.text substringToIndex:20];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    _model.meno = textField.text;
}


@end
