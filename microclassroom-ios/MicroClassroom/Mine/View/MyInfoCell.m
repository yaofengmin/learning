//
//  MyInfoCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyInfoCell.h"

@interface MyInfoCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation MyInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configWithModel:(NSDictionary *)cellModel {
    self.titleLabel.text = cellModel[@"title"];
    self.contentLabel.text = cellModel[@"content"];
    
    if (cellModel[@"color"]) {
        self.contentLabel.textColor = cellModel[@"color"];
    } else {
        self.contentLabel.textColor = [UIColor colorWithRed:88/255.0 green:88/255.0 blue:88/255.0 alpha:1];
    }
    
}

@end
