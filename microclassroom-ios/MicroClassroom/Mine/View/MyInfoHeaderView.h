//
//  MyInfoHeaderView.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserInfoModel;
@interface MyInfoHeaderView : UIView
- (void)configureViewWithModel:(UserInfoModel *)model;
@end
