//
//  MeTableViewCell1.m
//  EnergyConservationPark
//
//  Created by keanzhu on 16/5/31.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "MeTableViewCell1.h"

@interface MeTableViewCell1()

@property (nonatomic ,strong) UIImageView *leftImageView;
@property (nonatomic ,strong) UILabel     *infoLabel;

@end

@implementation MeTableViewCell1

//-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    if (self =[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//      //  [self creatImage];
////        [self creatTitleLabel];
//       
//        
//    }
//    return self;
//}
//-(void)creatImage
//{
//    _leftImageView = [[UIImageView alloc]init];
//    [self.contentView addSubview:_leftImageView];
//    [_leftImageView  mas_makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.left.equalTo(10);
//        make.centerY.equalTo(0);
//        make.size.equalTo(CGSizeMake(35, 35));
//        
//    }];
//}
//
//-(void)creatTitleLabel
//{
//    _infoLabel = [[UILabel alloc]init];
//    _infoLabel.font = [UIFont systemFontOfSize:13];
//    _infoLabel.textColor = FColor;
//    [self.contentView addSubview:_infoLabel];
//    
//    [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_leftImageView.mas_right).offset(10);
//        make.top.bottom.equalTo(0);
//        make.width.lessThanOrEqualTo(100);
//    }];
//    
//}
//-(void)configeViewDic:(NSDictionary *)dic
//{
//    _infoLabel.text = [dic objectForKey:@"cellTitle"];
//    _leftImageView.image = [UIImage imageNamed:[dic objectForKey:@"cellPic"]];
//}

-(void)configeViewTitelArr:(NSString *)title{
    self.accessoryType = UITableViewCellAccessoryNone;
    self.textLabel.font = Font(15);
    self.textLabel.textColor = HColor;
    self.textLabel.text = title;
}

@end
