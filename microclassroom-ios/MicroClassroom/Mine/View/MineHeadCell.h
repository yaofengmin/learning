//
//  MineHeadCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/7.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseTableViewCell.h"

@interface MineHeadCell : YHBaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *Lvlabel;
@property (weak, nonatomic) IBOutlet UILabel *sigeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addVImageView;

@end
