//
//  MyNotesDetailFirstTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyNotesDetailFirstTableViewCell.h"

@implementation MyNotesDetailFirstTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bookImage.backgroundColor = ImageBackColor;
    self.bookImage.contentMode = UIViewContentModeScaleAspectFill;
    self.bookImage.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setModel:(MyNotesListModel *)model
{
    [self.bookImage sd_setImageWithURL:[NSURL URLWithString:model.videoLogo]];
    self.bookTitle.text = [NSString stringWithFormat:@"%@",model.videoTitle];
    if (model.author.length == 0) {
        self.bookAuthor.text = [NSString stringWithFormat:@"作者 :"];
    }else{
        self.bookAuthor.text = [NSString stringWithFormat:@"作者 : %@",model.author];
    }
    self.notesTitle.text = model.titel;
    self.noteContent.text = model.content;
    
}

@end
