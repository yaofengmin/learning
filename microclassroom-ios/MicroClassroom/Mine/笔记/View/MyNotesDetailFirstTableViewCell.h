//
//  MyNotesDetailFirstTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyNotesListModel.h"
@interface MyNotesDetailFirstTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;
@property (weak, nonatomic) IBOutlet UILabel *bookAuthor;
@property (weak, nonatomic) IBOutlet UILabel *notesTitle;
@property (weak, nonatomic) IBOutlet UILabel *noteContent;

@property (nonatomic,strong) MyNotesListModel *model;
@end
