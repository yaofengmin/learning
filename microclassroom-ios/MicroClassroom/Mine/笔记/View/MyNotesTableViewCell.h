//
//  MyNotesTableViewCell.h
//  yanshan
//
//  Created by fm on 2017/8/20.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyNotesListModel.h"

@interface MyNotesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookImage;
@property (weak, nonatomic) IBOutlet UILabel *bookTitle;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numOfNotes;
@property (weak, nonatomic) IBOutlet UILabel *introLabel;


@property (nonatomic,strong) MyNotesListModel *model;
@end
