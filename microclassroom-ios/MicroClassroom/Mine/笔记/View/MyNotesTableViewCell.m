//
//  MyNotesTableViewCell.m
//  yanshan
//
//  Created by fm on 2017/8/20.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyNotesTableViewCell.h"

@implementation MyNotesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bookImage.backgroundColor = ImageBackColor;
    self.bookImage.contentMode = UIViewContentModeScaleAspectFill;
    self.bookImage.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setModel:(MyNotesListModel *)model
{
    [self.bookImage sd_setImageWithURL:[NSURL URLWithString:model.videoLogo]];
    self.bookTitle.text = [NSString stringWithFormat:@"%@",model.videoTitle];
    self.timeLabel.text = model.addTime;
    self.numOfNotes.text = model.titel;
    self.introLabel.text = model.content;
}
@end
