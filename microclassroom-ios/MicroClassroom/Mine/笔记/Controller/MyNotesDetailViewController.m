//
//  MyNotesDetailViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/22.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyNotesDetailViewController.h"

#import "WriteNotesViewController.h"

#import "MyNotesDetailFirstTableViewCell.h"

static NSString *MyNotesDetailFirstCellID = @"MyNotesDetailFirstCellID";
@interface MyNotesDetailViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (nonatomic, strong) UITableView *tableV;
@end

@implementation MyNotesDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"笔记详情";
    [self setupNav];
    [self initalTable];
}


-(void)setupNav
{
    //删除
    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [deleteBtn setImage:[UIImage imageNamed:@"note_delete"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.yh_navigationBar addSubview:deleteBtn];
    [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.equalTo(self.yh_navigationBar.mas_centerY).
        offset(KStatusBarHeight / 2);
    }];
//    //分享
//    UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [shareBtn setImage:[UIImage imageNamed:@"note_share"] forState:UIControlStateNormal];
//    [shareBtn addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
//    [self.yh_navigationBar addSubview:shareBtn];
//    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(deleteBtn.mas_left).offset(-10);
//        make.centerY.equalTo(self.yh_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
//    }];
//
    //编辑
    UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [editBtn setImage:[UIImage imageNamed:@"note_edit"] forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.yh_navigationBar addSubview:editBtn];
    [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(deleteBtn.mas_left).offset(-10);
        make.centerY.equalTo(self.yh_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
    }];
    
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 250;
    self.tableV.tableFooterView = [[UIView alloc]init];
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"MyNotesDetailFirstTableViewCell" bundle:nil] forCellReuseIdentifier:MyNotesDetailFirstCellID];
    
}


#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyNotesDetailFirstTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyNotesDetailFirstCellID forIndexPath:indexPath];
    cell.model = self.model;
    return cell;
}

#pragma mark === UITableViewDelegate



#pragma mark === 删除
-(void)deleteAction:(UIButton *)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"确定删除当前笔记?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
}

#pragma mark === UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSDictionary *paramDic = @{@"service": DelMyNotes,
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"noteId":self.model.noteId};
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
            }
        }];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark === 分享
-(void)shareAction:(UIButton *)sender
{
    [[YHJHelp shareInstance]   showShareInController:self andShareURL:self.model.titel andTitle:self.model.videoTitle andShareText:@"" andShareImage:nil isShowStudyGroup:NO isDIYUrl:NO];
}
#pragma mark === 写
-(void)editAction:(UIButton *)sender
{
    WriteNotesViewController *writeNotesVC = [[WriteNotesViewController alloc]initWithModel:self.model isWrite:NO];
    [self.navigationController pushViewController:writeNotesVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
