//
//  MyNotesDetailViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/22.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

#import "MyNotesListModel.h"

@interface MyNotesDetailViewController : YHBaseViewController
@property (nonatomic,strong) MyNotesListModel *model;

@end
