//
//  MyNotesViewController.m
//  yanshan
//
//  Created by fm on 2017/8/20.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyNotesViewController.h"

#import "MyNotesDetailViewController.h"

#import "MyNotesTableViewCell.h"

#import "MyNotesListModel.h"


static NSString *myNotesCellID = @"MyNotesCellID";
@interface MyNotesViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger pageCount;
}
@property (nonatomic,strong) NSMutableArray *resultModel;

@property (nonatomic,strong) myNoteModel *model;
@property (nonatomic, assign)  NSInteger   total;;

@property (nonatomic ,strong) UITableView *tableV;

@property (nonatomic ,strong) DIYTipView *tipView;
@end

@implementation MyNotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"我的笔记";
    [self initalTable];
    pageCount = 1;
    self.resultModel = [NSMutableArray array];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.resultModel removeAllObjects];
    pageCount = 1;
    [self requestData];
}
#pragma mark - 刷新
-(void)loadDataForHeader {
    pageCount = 1;
    [self requestData];
    
}

#pragma mark - 加载
- (void)loadMoreData {
    pageCount ++ ;
    [self requestData];
}

-(void)requestData
{
    NSDictionary *paramDic = @{@"service": GetGetMyNotes,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"keyWord":@"",
                               @"pageIndex": @(pageCount),
                               @"pageSize":@20};
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
            if (pageCount == 1) {
                [self.tableV.mj_header endRefreshing];
                [self.resultModel removeAllObjects];
            }else {
                [self.tableV.mj_footer endRefreshing];
            }
            _model = [myNoteModel mj_objectWithKeyValues:result[REQUEST_LIST]];
            [self.resultModel addObjectsFromArray:_model.list];
            _total = _model.total;
            [self.tableV reloadData];
            if (_model.total == 0 || self.resultModel.count == _model.total) {
                self.tableV.mj_footer.hidden = YES;
            }
        }
        if (self.resultModel.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                
            }
            [self.tableV addSubview:self.tipView];
            [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.view);
                make.size.equalTo(CGSizeMake(120, 120));
                
            }];
            self.tableV.mj_header.hidden = YES;
            self.tableV.mj_footer.hidden = YES;
        }else
        {
            if (self.tipView.superview) {
                
                [self.tipView removeFromSuperview];
            }
        }
    }];
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 155;
    self.tableV.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"MyNotesTableViewCell" bundle:nil] forCellReuseIdentifier:myNotesCellID];
    
}


#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.resultModel.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyNotesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:myNotesCellID forIndexPath:indexPath];
    cell.model = self.resultModel[indexPath.row];
    return cell;
}

#pragma mark === UITableViewDelegate
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 155;
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MyNotesDetailViewController *notesDetailVC = [[MyNotesDetailViewController alloc]init];
    notesDetailVC.model = self.resultModel[indexPath.row];
    [self.navigationController pushViewController:notesDetailVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
