//
//  MyNotesListModel.h
//  yanshan
//
//  Created by fm on 2017/9/6.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseModel.h"


@interface MyNotesListModel : YHBaseModel
@property (nonatomic,copy) NSString *videoId;
@property (nonatomic,copy) NSString *videoTitle;
@property (nonatomic,copy) NSString *videoLogo;
@property (nonatomic,copy) NSString *titel;
@property (nonatomic,copy) NSString *noteId;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *addTime;
@property (nonatomic,copy) NSString *author;

@end

@interface myNoteModel : YHBaseModel
@property (nonatomic,assign) int total;
@property (nonatomic,strong) NSMutableArray <MyNotesListModel *>*list;
@end


