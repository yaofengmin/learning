//
//  UserStudyListModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/10.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface UserStudyListModel : YHBaseModel

@property(nonatomic, copy) NSString *videoId;
@property(nonatomic, copy) NSString *videoTitle;
@property(nonatomic, copy) NSString *videoLength;
@property(nonatomic, copy) NSString *videoLaud;
@property(nonatomic, copy) NSString *videoLogo;
@property(nonatomic, copy) NSString *videoUrl;
@property(nonatomic, copy) NSString *teacherName;

@end
