//
//  LearningTasksViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/8.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "LearningTasksViewController.h"
#import "WHUCalendarView.h"
#import "MicroClassBannerSegmentModel.h"
#import "MicroClassDetailViewController.h"

@interface LearningTasksViewController ()<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) NSArray         *dataArr;
@property (nonatomic, strong) NSArray         *dateList;
@property (nonatomic, strong) UITableView     *tableView;
@property (nonatomic, strong) WHUCalendarView *calview;
@property (nonatomic, copy  ) NSString        *yearInt;
@property (nonatomic, copy  ) NSString        *monthInt;
@property (nonatomic ,copy  ) NSString        *currentDate;

@property (nonatomic ,strong) DIYTipView *tipView;
@end

@implementation LearningTasksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"学习任务";
    [self requestStudyTime];
    
    // Do any additional setup after loading the view.
}


- (void)requestStudyTime {
    NSDictionary *paramDic = @{Service:AppGetUserStudyTime,
                               @"userId":KgetUserValueByParaName(USERID)
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.dataArr = [self getTaskArr:result[@"list"]];
                [self creatUI];
                [self creatTabelView];
            }
//            else {
//                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
//            }
            
            if (self.dataArr.count==0) {
                if (!self.tipView) {
                    self.tipView = [[DIYTipView alloc]initWithInfo:result[REQUEST_MESSAGE] andBtnTitle:nil andClickBlock:nil];
                    
                }
                [self.view addSubview:self.tipView];
                [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                    
                    make.center.equalTo(self.view);
                    make.size.equalTo(CGSizeMake(120, 120));
                    
                }];
                
            }else
            {
                if (self.tipView.superview) {
                    
                    [self.tipView removeFromSuperview];
                }
            }
        }
    }];
    
}


-(void)creatTabelView
{
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(CGRectGetMaxY(self.calview.frame), 0, 0, 0));
    }];
}




- (void)creatUI {
    
    [self.view addSubview:self.calview];
    @weakify(self);
    self.calview.tagStringOfDate=^NSString*(NSArray* calm,NSArray* itemDateArray){
        @strongify(self);
        
        for (NSArray *arr in self.dataArr) {
            if ([arr isEqualToArray:itemDateArray]) {
                return @"学";
            }
        }
        return nil;
    };
    
    _calview.onDateSelectBlk=^(NSDate* date){
        @strongify(self);
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-M-d"];
        NSString *dateString = [format stringFromDate:date];
        [self requestUserStudyWithDate:dateString];
    };
    
    
    
}

- (void)requestUserStudyWithDate:(NSString *)dateStr {
    NSDictionary *paramDic = @{Service:AppGetUserStudy,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"currentDate":dateStr
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                
                self.dateList = [MicroClassVideoModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
            }else{
                self.dateList = @[];
            }
            [self.tableView reloadData];
        }
    }];
}


- (WHUCalendarView *)calview {
    if (!_calview) {
        _calview = [[WHUCalendarView alloc]initWithFrame:CGRectMake(0, KTopHeight, KScreenWidth, KScreenWidth)];
        
    }
    return _calview;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dateList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
        cell.textLabel.textColor = DColor;
        cell.textLabel.font = Font(14);
    }
    
    MicroClassVideoModel *model = [self.dateList objectAtIndex:indexPath.row];
    cell.textLabel.text = model.videoTitle;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MicroClassVideoModel *model = [self.dateList objectAtIndex:indexPath.row];
    MicroClassDetailViewController *vc = [[MicroClassDetailViewController alloc]init];
    vc.videoId = model.videoId;
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - 获取时间

- (NSArray *)getTimeDate {
    NSDate *date = [NSDate date];
    NSDateFormatter *formater = [[NSDateFormatter alloc]init];
    formater.dateFormat = @"YYYY-M-d";
    NSString *dateStr = [formater stringFromDate:date];
    NSArray *arr = [dateStr componentsSeparatedByString:@"-"];
    return arr;
}

#pragma mark - 处理数据

-(NSArray *)getTaskArr:(NSArray *)dateArr {
    NSMutableArray *arr = @[].mutableCopy;
    for (NSDictionary *dic in dateArr) {
        NSArray *dateStrArr = [dic[@"workTime"] componentsSeparatedByString:@"-"];
        [arr addObject:dateStrArr];
    }
    return arr;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
