//
//  NotifiListModel.h
//  yanshan
//
//  Created by fm on 2017/10/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseModel.h"

@interface NotifiListModel : YHBaseModel
@property (nonatomic ,copy) NSString *ids;
@property (nonatomic ,copy) NSString *title;
@property (nonatomic ,copy) NSString *contents;
@property (nonatomic ,copy) NSString *addTime;


@end


@interface NotifiModel : YHBaseModel
@property (nonatomic, assign) int total;
@property (nonatomic ,strong) NSArray *list;
@end
