//
//  NotifiTableViewCell.m
//  yanshan
//
//  Created by fm on 2017/10/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "NotifiTableViewCell.h"

#import "NotifiListModel.h"

@implementation NotifiTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NotifiListModel *)model
{
    self.titleLabel.text = model.title;
    self.contentLabel.text = model.contents;
    self.timeLabel.text = model.addTime;
}
@end
