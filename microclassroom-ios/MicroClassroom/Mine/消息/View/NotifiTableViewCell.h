//
//  NotifiTableViewCell.h
//  yanshan
//
//  Created by fm on 2017/10/23.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NotifiListModel;
@interface NotifiTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic ,strong) NotifiListModel *model;
@end
