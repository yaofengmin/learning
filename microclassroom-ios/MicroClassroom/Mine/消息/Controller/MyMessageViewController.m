//
//  AskAndShareViewController.m
//  yanshan
//
//  Created by fm on 2017/8/13.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "MyMessageViewController.h"

#import "ConversationListController.h"//会话列表
#import "NotificationViewController.h"//通知

#import "CategoryScreenVC.h"

@interface MyMessageViewController ()<FJSlidingControllerDataSource,FJSlidingControllerDelegate>
@property (nonatomic, strong)NSArray *titles;
@property (nonatomic, strong)NSArray *controllers;

@end

@implementation MyMessageViewController

- (void)viewDidLoad {
    self.isShowLine = YES;
    self.lineW = 20.0f;
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.pageController.dataSource = nil;
    self.yh_navigationItem.title = @"我的消息";
    self.datasouce = self;
    self.delegate = self;
    [self configController];
    [self configRightBarButtonItem];
}
-(void)configRightBarButtonItem
{
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithTitle:@"忽略未读" style:YHBarButtonItemStylePlain handler:^(id send) {
        NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
        for (EMConversation *conversation in conversations) {
            [conversation markAllMessagesAsRead:nil];
        }
        KPostNotifiCation(@"refreshConversionDataSource", nil);
    }];
}
-(void)configController{
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = KColorFromRGB(0xededed);
    [self.view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 1));
        make.top.mas_equalTo(KTopHeight + SEGMENTHEIGHT);
    }];
    
    ConversationListController *conversationVC = [[ConversationListController alloc]init];
    conversationVC.parentContrl = self;
    [ChatDemoHelper shareHelper].conversationListVC = conversationVC;
    
    NotificationViewController *nofiVC = [[NotificationViewController alloc]init];
    nofiVC.parentContrl = self;
    
    self.titles      = @[@"消息",@"通知"];
    self.controllers = @[conversationVC,nofiVC];
    
    [self addChildViewController:conversationVC];
    [self addChildViewController:nofiVC];
    [self reloadData];
}



#pragma mark dataSouce
- (NSInteger)numberOfPageInFJSlidingController:(FJSlidingController *)fjSlidingController{
    return self.titles.count;
}
- (UIViewController *)fjSlidingController:(FJSlidingController *)fjSlidingController controllerAtIndex:(NSInteger)index{
    return self.controllers[index];
}
- (NSString *)fjSlidingController:(FJSlidingController *)fjSlidingController titleAtIndex:(NSInteger)index{
    return self.titles[index];
}


- (UIColor *)titleNomalColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return JColor;

}
- (UIColor *)titleSelectedColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return NEColor;
}
- (UIColor *)lineColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return NEColor;
}
- (CGFloat)titleFontInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return 14.0;
}

#pragma mark delegate
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedIndex:(NSInteger)index{
    if (index == 0) {
        [self configRightBarButtonItem];
    }else{
        self.yh_navigationItem.rightBarButtonItem = nil;
    }
    self.title = [self.titles objectAtIndex:index];
}

- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedController:(UIViewController *)controller{
    // presentController
}
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedTitle:(NSString *)title{
    // presentTitle
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
