//
//  NotificationViewController.h
//  yanshan
//
//  Created by fm on 2017/9/9.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"
#import "MyMessageViewController.h"

@interface NotificationViewController : YHBaseViewController
@property(nonatomic, weak) MyMessageViewController *parentContrl;

@end
