//
//  NotificationViewController.m
//  yanshan
//
//  Created by fm on 2017/9/9.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "NotificationViewController.h"

#import "NotifiListModel.h"

#import "NotifiTableViewCell.h"

static NSString *NotifiCellID = @"NotifiCellID";
@interface NotificationViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,assign)  NSInteger pageIndex;
@property (nonatomic ,strong)  NSMutableArray *dataArr;
@property (nonatomic, assign)  NSInteger   total;;
@property (nonatomic ,strong)  NotifiModel *model;
@property (nonatomic ,strong)  UITableView *tableV;
@property (nonatomic ,strong)  DIYTipView *tipView;


@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = NCColor;
    _dataArr = [NSMutableArray array];
    [self initalTable];
    [self loadDataForHeader];
}
#pragma mark === 刷新
-(void)loadDataForHeader
{
    _pageIndex = 1;
    [self requestMessageList];
}

#pragma mark === 加载更多
-(void)loadMoreData
{
    _pageIndex ++;
    [self requestMessageList];
}
-(void)requestMessageList
{
    NSDictionary *dic = @{Service:GetNotifiMessageList,@"pageIndex":@(_pageIndex),@"pageSize":@20,@"user_Id":KgetUserValueByParaName(USERID)};
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:dic andBlocks:^(NSDictionary *result) {
        if (_pageIndex == 1) {
            [self.tableV.mj_header endRefreshing];
            [self.dataArr removeAllObjects];
        }else {
            [self.tableV.mj_footer endRefreshing];
        }
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _model = [NotifiModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                [self.dataArr addObjectsFromArray:_model.list];
                _total = _model.total;
                [self.tableV reloadData];
                if (self.dataArr.count == _total) {
                    self.tableV.mj_footer.hidden = YES;
                }else {
                    self.tableV.mj_footer.hidden = NO;
                }
            }
        }
        
        if (self.dataArr.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                
            }
            [self.tableV addSubview:self.tipView];
            [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.view);
                make.size.equalTo(CGSizeMake(120, 120));
                
            }];
//            self.tableV.mj_header.hidden = YES;
            self.tableV.mj_footer.hidden = YES;
        }else
        {
            if (self.tipView.superview) {
                
                [self.tipView removeFromSuperview];
            }
        }
    }];
}
-(void)initalTable
{
    
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 60;
    self.tableV.backgroundColor = [UIColor clearColor];
    self.tableV.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self.tableV registerNib:[UINib nibWithNibName:@"NotifiTableViewCell" bundle:nil] forCellReuseIdentifier:NotifiCellID];
    
    self.tableV.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.tableV.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
}

#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotifiTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NotifiCellID forIndexPath:indexPath];
    cell.model = self.dataArr[indexPath.row];
    return cell;
}


#pragma mark === UITableViewDelegate

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
