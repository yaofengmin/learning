//
//  MyZhiBoVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyZhiBoVC.h"
#import "ZhiBoCell.h"
#import "ZhiBoModel.h"
//#import "WatchLiveVC.h"
#import "YHNavgationController.h"
#import "UserInfoModel.h"
//#import "ConfigLaunchViewController.h"
#import "LiveRadioDetailViewController.h"
#import "LiveRadioHomeTableViewCell.h"

static NSString *liveRadioCellID = @"LiveRadioHomeCellID";
static NSString * kMyZhiBoNavTitle = @"我的直播";
@interface MyZhiBoVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (strong, nonatomic) NSMutableArray<ZhiBoModel *> *dataModel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) ZhiBoModel *model;
@property (nonatomic ,strong) DIYTipView *tipView;

@end

@implementation MyZhiBoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configNav];
    [self setUpTableView];
    [self pullData];
    
}

- (void)configNav {
    self.yh_navigationItem.title = kMyZhiBoNavTitle;
     //-----huwb 2016-11-11 del
    /*if ([UserInfoModel getInfoModel].teacherId > 0) {
        self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithTitle:@"发起直播" style:0 handler:^(id send) {
            ConfigLaunchViewController *launch = [[ConfigLaunchViewController alloc]init];
            YHNavgationController *nav = [[YHNavgationController alloc]initWithRootViewController:launch];
            [self presentViewController:nav animated:YES completion:nil];
        }];
    }*/
    //-------
}

- (NSMutableArray<ZhiBoModel *> *)dataModel {
    
    if (!_dataModel) {
        _dataModel = [NSMutableArray arrayWithCapacity:1];
    }
    
    return _dataModel;
    
}

- (void)setUpTableView {
    
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LiveRadioHomeTableViewCell" bundle:nil] forCellReuseIdentifier:liveRadioCellID];

}

- (void)pullData {
    
    NSDictionary *paramDic = @{@"service": AppGetMyVideo,
                               @"userId": KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                for (id item in result[REQUEST_LIST]) {
                    [self.dataModel addObject:[ZhiBoModel mj_objectWithKeyValues:item]];
                }
                
                [self.tableView reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
              
            }
            if (self.dataModel.count==0) {
                if (!self.tipView) {
                    self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                }
                [self.tableView addSubview:self.tipView];
                [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(self.view);
                    make.size.equalTo(CGSizeMake(120, 120));
                }];
            }else
            {
                if (self.tipView.superview) {
                    [self.tipView removeFromSuperview];
                }
            }
            
        }
    }];

}

# pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsZero];
    }
}


# pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    ZhiBoCell * cell = [tableView dequeueReusableCellWithIdentifier:kMyZhiBoCell forIndexPath:indexPath];
//    [cell configureCellWithUserZhiBoModel:self.dataModel[indexPath.row]];
    LiveRadioHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:liveRadioCellID forIndexPath:indexPath];
    [cell configureCellWithZhiBoModel:self.dataModel[indexPath.row]];
    return cell;
}


/**
 返回说明 1=直播进行中, 参加者可以进入观看直播
 2=预约中 , 活动预约中,尚未开始
 3=结束 , 活动已结束
 4=录播已上线, 参加者可以观看录播回放
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    
    
    
    ZhiBoModel *model = self.dataModel[indexPath.row];
    _model = model;
    
    
  //-----huwb 2016-11-11 add
    LiveRadioDetailViewController *detail = [[LiveRadioDetailViewController alloc]initWithZhiBoid:model.videoId];
    [self.navigationController pushViewController:detail animated:YES];
    return;
  //-----
 
    
    
    
    
//    switch (_model.status) {
//        case 1: { // 获取token

//                if([VHallApi isLoggedIn]) {  // 跳转
//                    [self pushZhiboLiveContr];
//                }else {
//                    @weakify(self);
//
//                    [VHallApi loginWithAccount:DEMO_Setting.account password:DEMO_Setting.password success:^{
//                        @strongify(self);
//                        [self pushZhiboLiveContr];
//                    } failure:^(NSError * error) {}];
//                }
//   }
//                break;
//            case 2:
//            {
//                [YHJHelp aletWithTitle:@"提示" Message:@"活动预约中,尚未开始" sureTitle:nil CancelTitle:@"确定" SureBlock:nil andCancelBlock:nil andDelegate:self];
//            }
//                break;
//            case 3:
//            {
//                [YHJHelp aletWithTitle:@"提示" Message:@"结束 , 活动已结束" sureTitle:nil CancelTitle:@"确定" SureBlock:nil andCancelBlock:nil andDelegate:self];
//            }
//                break;
//            case 4:  //录播
//            {
//                
//            }
//                break;
//                
//            default:
//                break;
//        }
}

-(void)pushZhiboLiveContr {
   
//
//        WatchLiveVC * watchVC  =[[WatchLiveVC alloc]init];
////        WatchLiveViewController * watchVC  =[[WatchLiveViewController alloc]init];
//        watchVC.roomId = _model.webinarId;
//        watchVC.kValue = DEMO_Setting.kValue;
//        watchVC.bufferTimes = DEMO_Setting.bufferTimes;
//        watchVC.watchVideoType = kWatchVideoRTMP;
//    YHNavgationController *waNav = [[YHNavgationController alloc]initWithRootViewController:watchVC];
//        [self presentViewController:waNav animated:YES completion:nil];
    
    }

@end
