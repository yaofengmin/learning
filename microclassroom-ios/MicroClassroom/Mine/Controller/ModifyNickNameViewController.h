//
//  ModifyNickNameViewController.h
//  yunbo2016
//
//  Created by Hanks on 16/1/11.
//  Copyright © 2016年 apple. All rights reserved.
//

#import "YHBaseViewController.h"

@interface ModifyNickNameViewController : YHBaseViewController
@property(nonatomic,copy)NSString * titleStr;
@property(nonatomic,copy)NSString *titleLbText;
@property(nonatomic,copy)NSString * placeText;


@property(nonatomic,assign)BOOL nickRequest;
@property(nonatomic,copy)void(^backSetBlock)(NSString*str);

@end
