//
//  MineViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MineViewController.h"
#import "MineHeadCell.h"
#import "MyMsgCell.h"
#import "MeMoreViewController.h"
#import "MyInfoVC.h"
#import "MyCareVC.h"
#import "MyCollectionVC.h"
#import "DeskmateViewController.h"
#import <MJExtension.h>
//#import "OnLinePayTool.h"
#import "MineWalletViewController.h"
#import "MyZhiBoVC.h"
#import "ConversationListController.h"
#import "MyOrderVC.h"
#import "MineClassGropViewController.h"
#import "UserNewsVC.h"
#import "ChatDemoHelper.h"
#import "LearningTasksViewController.h"
#import "YhjRootViewController.h"
@interface MineViewController()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *_titleArr;
    NSArray *_imageArr;
}

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong)  UILabel *badgeLabel;

@end

@implementation MineViewController


-(void)viewDidLoad {

    [super viewDidLoad];
    self.view.backgroundColor = MainBackColor ;
    self.yh_navigationItem.titleView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nav_logo"]];
    _titleArr = @[@[@"我的学币",@"我的订单",@"我的班级",@"学习任务"],@[@"我的收藏",@"我的关注",@"我的直播",@"我的江湖"],@[@"我的同学",@"我的消息",@"我的动态"],@[@"设置"]];
    _imageArr =@[@[@"me_money",@"me_list",@"me_class",@"me_task"],@[@"me_collection",@"me_care",@"me_online",@"me_jianghu"],@[@"me_frineds",@"me_message",@"me_moments"],@[@"me_setting"]];
    [self creatTabelView];
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}
-(void)creatTabelView {

    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"MyMsgCell" bundle:nil]
         forCellReuseIdentifier:@"MyMsgCell"];
    
    [self.view addSubview:self.tableView];

    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {

        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _titleArr.count + 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [MineHeadCell cellHeight];
    }else{
        return 44;
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return [_titleArr[section - 1] count];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 ) {
        return 0.0001;
    }else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0001;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentfi ;
    if (indexPath.section == 0) {
        cellIdentfi = @"MineHeadCell";
    }
    else {
        cellIdentfi = @"cellId";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentfi];

    if (nil == cell) {

        if (indexPath.section == 0) {

            cell = [[[NSBundle mainBundle] loadNibNamed:@"MineHeadCell" owner:nil options:nil]lastObject];

        } else {
            cell = [[UITableViewCell alloc]initWithStyle:1 reuseIdentifier:cellIdentfi];
            cell.textLabel.font = Font(15);
            cell.textLabel.textColor = CColor;
        }
    }


    if (indexPath.section != 0) {
        
        cell.textLabel.text = _titleArr[indexPath.section - 1][indexPath.row];
        cell.imageView.image = [UIImage imageNamed: _imageArr[indexPath.section - 1][indexPath.row]];
        if (indexPath.section == 3 && indexPath.row == 1) {
            if (!_badgeLabel) {
                _badgeLabel = [[UILabel alloc]init];
                _badgeLabel.layer.cornerRadius = 7;
                _badgeLabel.textAlignment = NSTextAlignmentCenter;
                _badgeLabel.layer.masksToBounds = YES;
                _badgeLabel.backgroundColor = kNavigationBarColor;
                _badgeLabel.textColor = [UIColor whiteColor];
                _badgeLabel.font = Font(10);
                [cell.contentView addSubview:_badgeLabel];
                
                [_badgeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(cell.contentView);
                    make.width.greaterThanOrEqualTo(20);
                    make.height.greaterThanOrEqualTo(15);
                    make.right.equalTo(cell.contentView).offset(-10);
                }];
            }
                 [self setupUnreadMessageCount];
            
            return cell;
        }
    }else {
        [(MineHeadCell*)cell configWithModel:nil];
        }
  
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        if (indexPath.section == 4) {
            
            MeMoreViewController *more = [[MeMoreViewController alloc]init];
            [self.navigationController pushViewController:more animated:YES];
            
        } else if (indexPath.section == 1){
            if (indexPath.row == 0) {
                
                MineWalletViewController *wallet = [[MineWalletViewController alloc]init];
                [self.navigationController pushViewController:wallet animated:YES];
                
            }else if (indexPath.row == 2) {
                MineClassGropViewController *group = [[MineClassGropViewController alloc]init];
                [self.navigationController pushViewController:group animated:YES];
            }else if (indexPath.row == 1) {
                
                MyOrderVC *targetVC = [[MyOrderVC alloc]init];
                [self.navigationController pushViewController:targetVC animated:YES];
                
            }else if (indexPath.row == 3){
                LearningTasksViewController *task = [[LearningTasksViewController alloc]init];
                [self.navigationController pushViewController:task animated:YES];
            }
        } else if (indexPath.section == 2) {
            switch (indexPath.row) {
                case 0:
                {
                    MyCollectionVC * targetVC = [[MyCollectionVC alloc] init];
                    [self.navigationController pushViewController:targetVC animated:YES];
                }
                    break;
                case 1:
                {
                    MyCareVC * targetVC = [[MyCareVC alloc] init];
                    [self.navigationController pushViewController:targetVC animated:YES];
                }
                    break;
                case 2:
                {
                    MyZhiBoVC * targetVC = [[MyZhiBoVC alloc] init];
                    [self.navigationController pushViewController:targetVC animated:YES];
                }
                    break;
                case 3:{
                    UserNewsVC *vc = [[UserNewsVC alloc]initWithShowMessageType:GetMessagesTypeGetMyCircle andClassId:@""];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                    break;
                default:
                    break;
            }
        } else if (indexPath.section == 0) {
            MyInfoVC * targetVC = [[MyInfoVC alloc] init];
            [self.navigationController pushViewController:targetVC animated:YES];
        } else if (indexPath.section == 3) {
            switch (indexPath.row) {
                case 0:
                {
                    DeskmateViewController *targetVC = [[DeskmateViewController alloc]init];
                    [self.navigationController pushViewController:targetVC animated:YES];
                }
                    break;
                case 1:
                {
                    ConversationListController *targetVC = [[ConversationListController alloc]init];
                    
                    [ChatDemoHelper shareHelper].conversationListVC = targetVC;
                    [self.navigationController pushViewController:targetVC animated:YES];
                }
                    break;
                case 2:
                {
                    UserNewsVC *vc = [[UserNewsVC alloc]initWithShowMessageType:GetMessagesTypeGetMyCircle andClassId:@""];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
    }
}


-(void)setupUnreadMessageCount {
    [self.root setupUnreadMessageCount];
    _badgeLabel.hidden = YES;
    __block int unreadCount = 0;
    NSArray * conversationArr = [[EMClient sharedClient].chatManager getAllConversations];
    
    [conversationArr enumerateObjectsUsingBlock:^(EMConversation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        unreadCount += obj.unreadMessagesCount;
        if (unreadCount >0) {
            _badgeLabel.hidden = NO;
        }
        _badgeLabel.text = [NSString stringWithFormat:@"%d", unreadCount];
    }];

}

@end
