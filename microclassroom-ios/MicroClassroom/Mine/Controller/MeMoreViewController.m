//
//  MeMoreViewController.m
//  EnergyConservationPark
//
//  Created by Hanks on 16/6/18.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "MeMoreViewController.h"
#import "MeTableViewCell1.h"
#import "InitConfigModel.h"
#import <SDImageCache.h>
#import "ModifyPassWordViewController.h"
#import "YHBaseWebViewController.h"

@interface MeMoreViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@property (strong ,nonatomic) UITableView *tableView;
@property (strong ,nonatomic) NSArray     *dataSource;

@end

@implementation MeMoreViewController


-(void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = NCColor;
    self.yh_navigationItem.title  = @"更多";
    
    _dataSource =@[@[@"清除缓存",
                     @"修改密码"],
                   @[@"关于我们"]];
    
    [self creatTableView];
}


-(void)creatTableView
{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorColor = KColorFromRGB(0xededed);
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dataSource.count;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_dataSource objectAtIndex:section] count];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 10;
    }
    
    return 0.01;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 10)];
        view.backgroundColor = [UIColor clearColor];
        return view;
    }
    return nil;
    
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* indentifitr = @"cell";
    
    MeTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:indentifitr];
    if (cell == nil) {
        
        cell = [[MeTableViewCell1 alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:indentifitr];
        
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.detailTextLabel.font = Font(14);
    [(MeTableViewCell1 *)cell configeViewTitelArr:_dataSource[indexPath.section][indexPath.row]];
    
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {  // 清除缓存
            
            CGFloat size =   [[SDImageCache sharedImageCache] getSize]/1024/1024.0 ;
            NSString *clearCacheName = size>= 1 ? [NSString stringWithFormat:@"清理缓存(%.2fM)",size] : [NSString stringWithFormat:@"缓存(%.2fK)",size * 1024];
            cell.detailTextLabel.text =clearCacheName;
            cell.detailTextLabel.textColor = JColor;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }else
    {
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        cell.detailTextLabel.textColor = kNavigationBarColor;
        cell.detailTextLabel.text =[NSString stringWithFormat:@"V %@",version];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0){ // 清除缓存
            
            CGFloat size =   [[SDImageCache sharedImageCache] getSize]/1024/1024.0 ;
            if (size == 0) {
                return;
            }
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"确定要清除所有缓存吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert showAlertWithCompletionHandler:^(NSInteger tag) {
                if (tag == 1) {
                    [[SDImageCache sharedImageCache] clearDisk];
                    [self.tableView reloadData];
                }
            }];
            
        }else if (indexPath.row == 1){ //修改密码
            
            ModifyPassWordViewController *mod = [ModifyPassWordViewController new];
            
            [self.navigationController pushViewController:mod animated:YES];
            
        }
        
    }else
    {
        
        YHBaseWebViewController *about = [[YHBaseWebViewController alloc]initWithUrlStr:[InitConfigModel shareInstance].aboutme_url andNavTitle:@"关于我们"];
        [self.navigationController pushViewController:about animated:YES];
    }
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [[SDImageCache sharedImageCache] clearDisk];
        [self.tableView reloadData];
    }
}

@end
