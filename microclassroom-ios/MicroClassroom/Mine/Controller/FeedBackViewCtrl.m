//
//  FeedBackViewCtrl.m
//  CarService
//
//  Created by dqf on 7/17/14.
//  Copyright (c) 2014 fenglun. All rights reserved.
//  问题反馈

#import "FeedBackViewCtrl.h"

@interface FeedBackViewCtrl ()<UITextViewDelegate> {
    UITextView *myTextView;
    UILabel  *_textViewplaceholder;
}

@end

@implementation FeedBackViewCtrl

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = NCColor;
    self.yh_navigationItem.title = NSLocalizedString(@"意见反馈", @"");
    
    [self topView];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
}


- (void)topView
{
    
    myTextView = [[UITextView alloc] init];
    //    myTextView.layer.borderColor = EColor.CGColor;
    myTextView.font  = Font(14);
    myTextView.delegate = self;
    //    myTextView.layer.borderWidth = 0.8;
    myTextView.textColor = JColor;
    [self.view addSubview:myTextView];
    
    [myTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(15);
        make.right.equalTo(self.view).with.offset(-15);
        make.height.mas_equalTo(@200);
        make.top.equalTo(self.view).with.offset(20 + KTopHeight);
    }];
    
    
    [myTextView layoutIfNeeded];
    _textViewplaceholder               = [[UILabel  alloc]initWithFrame:CGRectMake(10, 7, 0, 0)];
    _textViewplaceholder.textColor     = JColor;
    _textViewplaceholder.text          = @"请输入您的反馈意见...";
    _textViewplaceholder.font          = [UIFont systemFontOfSize:14];
    _textViewplaceholder.textAlignment = NSTextAlignmentLeft;
    [_textViewplaceholder sizeToFit];
    [myTextView addSubview:_textViewplaceholder];
    
    
    
    UIButton *submitBtn = [DIYButton buttonWithImage:nil andTitel:@"提交" andBackColor:kNavigationBarColor];
    [submitBtn addTarget:self action:@selector(postRequest) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
    [submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(myTextView.mas_bottom).offset(30);
        make.size.equalTo(CGSizeMake(KScreenWidth * 0.9, 40));
        make.centerX.equalTo(0);
        
    }];
}



- (void)postRequest
{
    
    [self.view endEditing:YES];
    if (NOEmptyStr(myTextView.text)) {
        
        if (myTextView.text.length <= 140) {
            NSDictionary   *sendDic = @{Service:Feedback,
                                        @"userId":KgetUserValueByParaName(USERID),
                                        @"content":myTextView.text
                                        };
            
            [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
                    
                    myTextView.text=@"";
                    [myTextView addSubview:_textViewplaceholder];
                    
                }else
                {
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
                }
            }];
        }else {
            [self popView:@"字数超出限制!"];
        }
        
    }else{
        [self popView:@"亲，评论内容为空!"];
    }
    
}


#pragma mark -------textViewDelegate

-(void)textViewDidChange:(UITextView *)textView{
    if (textView.text.length==0) {
        [myTextView addSubview:_textViewplaceholder];
    }else {
        [_textViewplaceholder removeFromSuperview];
    }
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end



