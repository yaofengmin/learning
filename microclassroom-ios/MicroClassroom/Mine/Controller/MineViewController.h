//
//  MineViewController.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"
@class YhjRootViewController;
@interface MineViewController : YHBaseViewController
@property (nonatomic,weak) YhjRootViewController *root;
-(void)setupUnreadMessageCount;
@end
