//
//  EditGenderVC.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"

@interface EditGenderVC : YHBaseViewController
@property (nonatomic, copy) void(^block)(NSString *str);
@property (nonatomic, copy) NSString *defaultText;
@end
