//
//  CommonEditInfoVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/1.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "CommonEditInfoVC.h"

#import "IQKeyboardManager.h"
@interface CommonEditInfoVC ()
@property (weak, nonatomic) IBOutlet UITextField *editTxt;
@property (weak, nonatomic) IBOutlet UIButton *comfirmBtn;
@property (strong, nonatomic) UITextView *editView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topheight;

@end

@implementation CommonEditInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = NCColor;
//    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    self.topheight.constant = KTopHeight + 20;
    [self setUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUI {

    switch (_editAction) {
        case MCEditActionSign:
            self.yh_navigationItem.title = @"编辑个性签名";
            self.editTxt.placeholder = @"请输入个性签名";
            break;
        case MCEditActionBrief:{
            self.yh_navigationItem.title = @"编辑个人介绍";
            [self.view addSubview:self.editView];
            self.editTxt.hidden = YES;
            self.lineView.hidden = YES;
            [self.editView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(self.view).multipliedBy(1.0);
                make.centerX.equalTo(self.view);
                make.height.equalTo(@300);
                make.top.mas_equalTo(KTopHeight + 20);
            }];
        }
            break;
        case MCEditActionEmail:
            self.yh_navigationItem.title = @"编辑邮箱";
            self.editTxt.placeholder = @"请输入邮箱";
            break;
        case MCEditActionNickName:
            self.yh_navigationItem.title = @"编辑学名";
            self.editTxt.placeholder = @"请输入昵称";
            break;
        case MCEditActionRemarkName:
            self.yh_navigationItem.title = @"编辑备注";
            self.editTxt.placeholder = @"请输入备注";
            break;
//        case MCEditActionCompany:
//            self.yh_navigationItem.title = @"编辑所属单位";
//            self.editTxt.placeholder = @"请输入所属单位";
//            break;
        case MCEditActionTitle:
            self.yh_navigationItem.title = @"编辑单位职务";
            self.editTxt.placeholder = @"请输入单位职务";
            break;
        case MCEditActionLinkTel:
            self.yh_navigationItem.title = @"编辑联系方式";
            self.editTxt.placeholder = @"请输入联系方式";
            break;
        case MCEditActionHobby:
            self.yh_navigationItem.title = @"编辑爱好";
            self.editTxt.placeholder = @"编辑爱好";
            break;
        case MCEditActionKnowledge:
            self.yh_navigationItem.title = @"编辑领域";
            self.editTxt.placeholder = @"编辑熟悉领域";
            break;
        case MCEditActionRuningCity:
            self.yh_navigationItem.title = @"编辑来往城市";
            self.editTxt.placeholder = @"编辑来往城市";
            break;
            case MCEditChangeArea:
            self.yh_navigationItem.title = @"编辑所在地区";
            self.editTxt.placeholder = @"编辑所在地区";
        default:
            break;
    }
    
    if (_editAction == MCEditActionBrief) {
        self.editView.text = self.defaultText;
    }else {
        self.editTxt.text = self.defaultText;
    }
  
}

- (IBAction)comfirm:(UIButton *)sender {
    switch (_editAction) {
        case MCEditActionRemarkName:
            [self editRemarkName];
            break;
        case MCEditActionEmail:
            [self editMoreInfo:@"email"];
            break;
        case MCEditActionBrief:
            [self editMoreInfo:@"brief"];
            break;
        case MCEditActionLinkTel:
            [self editMoreInfo:@"link_tel"];
            break;
        case MCEditActionHobby:
            [self editMoreInfo:@"hobby"];
            break;
        case MCEditActionKnowledge:
            [self editMoreInfo:@"knowledge"];
            break;
        case MCEditActionRuningCity:
            [self editMoreInfo:@"runing_city"];
            break;
            case MCEditChangeArea:
            [self editMoreInfo:@"area_name"];
        default:
            [self editSelfInfo];
            break;
    }
    
}

- (void)editMoreInfo:(NSString *)key {
    
    NSDictionary *paramDic = @{@"service": UserSetMoreInfo,
                               @"userId": KgetUserValueByParaName(USERID),
                               @"fildValue": key,
                               @"setValue": (_editAction == MCEditActionBrief)?self.editView.text : self.editTxt.text};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
    
                self.block((_editAction == MCEditActionBrief)?self.editView.text : self.editTxt.text);
                
                [self.navigationController popViewControllerAnimated:YES];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
}

- (void)editSelfInfo {
    
    NSString * service;
    NSString * editName;
    
    switch (_editAction) {
        case MCEditActionSign:
            service = SetPersonalSign;
            editName = @"personalSign";
            break;
        case MCEditActionNickName:
            service = SetNickname;
            editName = @"nickname";
            break;
//        case MCEditActionCompany:
//            service = SetCompany;
//            editName = @"company";
//            break;
        case MCEditActionTitle:
            service = SetTitle;
            editName = @"Title";
            break;
        default:
            return;
    }
    
    NSDictionary *paramDic = @{@"service": service,
                               @"userId": KgetUserValueByParaName(USERID),
                               editName :(_editAction == MCEditActionBrief)?self.editView.text : self.editTxt.text};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                if (self.editAction == MCEditActionNickName) {
                    UserInfoModel *model = [UserInfoModel getInfoModel];
                    model.nickname = self.editTxt.text;
                    [UserInfoModel saveUserInfoModelWithModel:model];
                    KPostNotifiCation(NOTIGICATIONFINDVVIEWRELOAD, nil);
                }
                self.block(self.editTxt.text);
                
                [self.navigationController popViewControllerAnimated:YES];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}

- (void)editRemarkName {
    
    NSDictionary *paramDic = @{@"service": CircleSetFriendNoteName,
                               @"userId": KgetUserValueByParaName(USERID),
                               @"friendId": self.friendId,
                               @"noteName": self.editTxt.text};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功

                self.block(self.editTxt.text);
                
                [self.navigationController popViewControllerAnimated:YES];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];

}

- (UITextView *)editView {
    if (!_editView) {
        _editView = [[UITextView alloc]init];
        _editView.font = Font(14);
        _editView.textColor = JColor;
//        _editView.layer.borderColor = CColor.CGColor;
//        _editView.layer.borderWidth = 0.5;
    }
    
  return _editView;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField =0;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}

@end
