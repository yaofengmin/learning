//
//  MyCollectionVC.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"
#import "MyMainCollectionViewController.h"

@interface MyCollectionVC : YHBaseViewController
@property(nonatomic, weak) MyMainCollectionViewController *parentContrl;

@end
