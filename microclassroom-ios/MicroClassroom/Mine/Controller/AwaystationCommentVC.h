//
//  AwaystationCommentVC.h
//  MicroClassroom
//
//  Created by Hanks on 2016/10/9.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"

@interface AwaystationCommentVC : YHBaseViewController

- (instancetype)initWithOrderId:(NSString *)orderId;
@property(nonatomic ,copy)void(^commentSuccess)();
@end
