//
//  MyMoreInfoVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyMoreInfoVC.h"
#import "CommonEditInfoVC.h"
#import "EditMultipleSelectVC.h"

#import "MyInfoCell.h"
#import "CodeListModel.h"
#import "UserInfoModel.h"

static NSString * const kMyMoreInfoVCTitle = @"更多信息";
static NSString * const kMyMoreInfoCellID = @"MyInfoCell";

@interface MyMoreInfoVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (strong, nonatomic) NSMutableArray * tableModel;
@property (strong, nonatomic) UserInfoModel * userInfoModel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;

@end

@implementation MyMoreInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.yh_navigationItem.title = kMyMoreInfoVCTitle;
    self.topHeight.constant = KTopHeight;
    [self setUpTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//兴趣爱好hobby
//领域：knowledge
//来往城市 runing_city

- (NSMutableArray *)tableModel {
    if (!_tableModel) {
        NSString *brandId = KgetUserValueByParaName(KBrandInfoId);
        if ([brandId integerValue] > 0) {
            _tableModel = @[@{@"title": @"个人介绍", @"content": self.userInfoModel.brief == nil ? @"" : self.userInfoModel.brief}.mutableCopy,
                            @{@"title": @"联系方式", @"content": self.userInfoModel.linkTel == nil ? @"" :self.userInfoModel.linkTel}.mutableCopy,
                            @{@"title":@"兴趣爱好",@"content":self.userInfoModel.hobby == nil ? @"" : self.userInfoModel.hobby}.mutableCopy,
                            @{@"title":@"熟悉领域",@"content":self.userInfoModel.knowledge == nil ? @"" : self.userInfoModel.knowledge}.mutableCopy,
                            ].mutableCopy;
        }else{
            _tableModel = @[@{@"title": @"个人介绍", @"content": self.userInfoModel.brief == nil ? @"" : self.userInfoModel.brief}.mutableCopy,
                            @{@"title": @"资源", @"content": [self arrToStr:self.userInfoModel.label_have]}.mutableCopy,
                            @{@"title": @"需求", @"content": [self arrToStr:self.userInfoModel.label_need]}.mutableCopy,
                            @{@"title": @"联系方式", @"content": self.userInfoModel.linkTel == nil ? @"" :self.userInfoModel.linkTel}.mutableCopy,
                            @{@"title":@"兴趣爱好",@"content":self.userInfoModel.hobby == nil ? @"" : self.userInfoModel.hobby}.mutableCopy,
                            @{@"title":@"熟悉领域",@"content":self.userInfoModel.knowledge == nil ? @"" : self.userInfoModel.knowledge}.mutableCopy,
                            @{@"title":@"来往城市",@"content":self.userInfoModel.runingCity == nil ? @"" : self.userInfoModel.runingCity }.mutableCopy,
                            @{@"title":@"行业",@"content":self.userInfoModel.industryName == nil ? @"" : self.userInfoModel.industryName}.mutableCopy].mutableCopy;
        }
    }
    
    return _tableModel;
}

- (UserInfoModel *)userInfoModel {
    if (!_userInfoModel) {
        _userInfoModel = [UserInfoModel getInfoModel];
    }
    
    return _userInfoModel;
}

- (NSString *)arrToStr:(NSArray *)arr {
    if(arr.count==0){return @"";}
    
    NSMutableArray *nameArr = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        [nameArr addObject:dic[@"codeName"]];
    }
    
    return [nameArr componentsJoinedByString:@"，"];
}

- (void)setUpTable {
    
    self.tableView.tableFooterView = [UIView new];
    
    self.tableView.estimatedRowHeight = 50.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.tableView registerNib:[UINib nibWithNibName:kMyMoreInfoCellID bundle:nil]
         forCellReuseIdentifier:kMyMoreInfoCellID];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins: UIEdgeInsetsZero];
    }
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.tableModel.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyInfoCell * cell = [tableView dequeueReusableCellWithIdentifier:kMyMoreInfoCellID
                                                        forIndexPath:indexPath];
    
    [cell configWithModel:self.tableModel[indexPath.row]];
    
    return cell;
    
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *brandId = KgetUserValueByParaName(KBrandInfoId);
    if (indexPath.row == 0) {
        @weakify(self);
        CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
        targetVC.editAction = MCEditActionBrief;
        targetVC.defaultText = self.userInfoModel.brief;
        targetVC.block = ^(NSString *str){
            @strongify(self);
            self.userInfoModel.brief = str;
            [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
            self.tableModel[indexPath.row][@"content"] = self.userInfoModel.brief;
            [self.tableView reloadData];
        };
        [self.navigationController pushViewController:targetVC animated:YES];
    } else if (indexPath.row == 3) {
        @weakify(self);
        if ([brandId integerValue] > 0) {
            CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
            targetVC.editAction = MCEditActionKnowledge;
            targetVC.defaultText = self.userInfoModel.knowledge;
            targetVC.block = ^(NSString *str){
                @strongify(self);
                self.userInfoModel.knowledge = str;
                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                self.tableModel[indexPath.row][@"content"] = self.userInfoModel.knowledge;
                [self.tableView reloadData];
            };
            [self.navigationController pushViewController:targetVC animated:YES];
        }else{
            CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
            targetVC.editAction = MCEditActionLinkTel;
            targetVC.defaultText = self.userInfoModel.linkTel;
            targetVC.block = ^(NSString *str){
                @strongify(self);
                self.userInfoModel.linkTel = str;
                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                self.tableModel[indexPath.row][@"content"] = self.userInfoModel.linkTel;
                [self.tableView reloadData];
            };
            [self.navigationController pushViewController:targetVC animated:YES];
        }
    } else if (indexPath.row == 4) {
        @weakify(self);
        CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
        targetVC.editAction = MCEditActionHobby;
        targetVC.defaultText = self.userInfoModel.hobby;
        targetVC.block = ^(NSString *str){
            @strongify(self);
            self.userInfoModel.hobby = str;
            [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
            self.tableModel[indexPath.row][@"content"] = self.userInfoModel.hobby;
            [self.tableView reloadData];
        };
        [self.navigationController pushViewController:targetVC animated:YES];
    } else if (indexPath.row == 5) {
        @weakify(self);
        CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
        targetVC.editAction = MCEditActionKnowledge;
        targetVC.defaultText = self.userInfoModel.knowledge;
        targetVC.block = ^(NSString *str){
            @strongify(self);
            self.userInfoModel.knowledge = str;
            [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
            self.tableModel[indexPath.row][@"content"] = self.userInfoModel.knowledge;
            [self.tableView reloadData];
        };
        [self.navigationController pushViewController:targetVC animated:YES];
    } else if (indexPath.row == 6) {
        @weakify(self);
        CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
        targetVC.editAction = MCEditActionRuningCity;
        targetVC.defaultText = self.userInfoModel.runingCity;
        targetVC.block = ^(NSString *str){
            @strongify(self);
            self.userInfoModel.runingCity = str;
            [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
            self.tableModel[indexPath.row][@"content"] = self.userInfoModel.runingCity;
            [self.tableView reloadData];
        };
        [self.navigationController pushViewController:targetVC animated:YES];
    }else {
        EditMultipleSelectVC * targetVC = [[EditMultipleSelectVC alloc] init];
        if (indexPath.row == 1) {
            if ([brandId integerValue] > 0) {
                @weakify(self);
                CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
                targetVC.editAction = MCEditActionLinkTel;
                targetVC.defaultText = self.userInfoModel.linkTel;
                targetVC.block = ^(NSString *str){
                    @strongify(self);
                    self.userInfoModel.linkTel = str;
                    [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                    self.tableModel[indexPath.row][@"content"] = self.userInfoModel.linkTel;
                    [self.tableView reloadData];
                };
                [self.navigationController pushViewController:targetVC animated:YES];
                return;
            }else{
                targetVC.editType = MCEditTypeHave;
                targetVC.defaultSelectArr = self.userInfoModel.label_have;
                @weakify(self);
                targetVC.block = ^(NSArray *selectArr, NSString *selectStr){
                    @strongify(self);
                    self.userInfoModel.label_have = [CodeListModel mj_keyValuesArrayWithObjectArray:selectArr];
                    [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                    self.tableModel[indexPath.row][@"content"] = selectStr;
                    [self.tableView reloadData];
                };
            }
        } else if (indexPath.row == 2){
            if ([brandId integerValue] > 0) {
                @weakify(self);
                CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
                targetVC.editAction = MCEditActionHobby;
                targetVC.defaultText = self.userInfoModel.hobby;
                targetVC.block = ^(NSString *str){
                    @strongify(self);
                    self.userInfoModel.hobby = str;
                    [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                    self.tableModel[indexPath.row][@"content"] = self.userInfoModel.hobby;
                    [self.tableView reloadData];
                };
                [self.navigationController pushViewController:targetVC animated:YES];
                return;
            }else{
                targetVC.editType = MCEditTypeNeed;
                targetVC.defaultSelectArr = self.userInfoModel.label_need;
                @weakify(self);
                targetVC.block = ^(NSArray *selectArr, NSString *selectStr){
                    @strongify(self);
                    self.userInfoModel.label_need = [CodeListModel mj_keyValuesArrayWithObjectArray:selectArr];
                    [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                    self.tableModel[indexPath.row][@"content"] = selectStr;
                    [self.tableView reloadData];
                    
                };
            }
        }else {  //行业
            targetVC.editType = MCEditTypeIndustry;
            CodeListModel *industrry = [[CodeListModel alloc]init];
            industrry.codeId = self.userInfoModel.industryId;
            industrry.codeName = self.userInfoModel.industryName;
            targetVC.defaultSelectArr = @[industrry];
            @weakify(self);
            targetVC.block = ^(NSArray *selectArr, NSString *selectStr){
                @strongify(self);
                self.userInfoModel.industryName = [selectArr[0] codeName];
                self.userInfoModel.industryId = [selectArr[0] codeId];
                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                self.tableModel[indexPath.row][@"content"] = selectStr;
                [self.tableView reloadData];
                
            };
            
        }
        
        [self.navigationController pushViewController:targetVC animated:YES];
    }
    
}

@end
