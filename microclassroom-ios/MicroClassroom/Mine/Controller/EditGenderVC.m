//
//  EditGenderVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "EditGenderVC.h"

static NSString * const kEditGenderNavTitle = @"编辑性别";

@interface EditGenderVC ()
<
UIPickerViewDelegate,
UIPickerViewDataSource
>
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (nonatomic, copy) NSArray *pickerData;

@end

@implementation EditGenderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.yh_navigationItem.title = kEditGenderNavTitle;
    self.genderLabel.text = self.defaultText;
    
    [self.pickerData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([(NSString *)obj isEqualToString:self.defaultText]) {
            [self.picker selectRow:idx inComponent:0 animated:NO];
            *stop = YES;
        }
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)pickerData {
    
    if (!_pickerData) {
        _pickerData = @[@"男", @"女"];
    }
    
    return _pickerData;
    
}

- (IBAction)comfirm:(UIButton *)sender {
    NSDictionary *paramDic = @{@"service": SetSex,
                               @"userId": KgetUserValueByParaName(USERID),
                               @"sex" :self.genderLabel.text};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                self.block(self.genderLabel.text);
                
                [self.navigationController popViewControllerAnimated:YES];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];

}

#pragma nark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.pickerData.count;
}

#pragma nark - UIPickerViewDelegate
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.pickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.genderLabel.text = self.pickerData[row];
}

@end
