//
//  MyCareVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyCareVC.h"
#import "PeopleCell.h"
#import "FriendModel.h"
#import "PersonHomeViewController.h"
static NSString * const kMyCareVCTitle = @"我的关注";
static NSString * const kMyCareCellID = @"PeopleCell";

@interface MyCareVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (strong, nonatomic) NSMutableArray<FriendModel *> * model;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic ,strong) DIYTipView *tipView;
@end

@implementation MyCareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.yh_navigationItem.title = kMyCareVCTitle;
    
    [self setUpTable];
    [self pullCareData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpTable {
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:kMyCareCellID bundle:nil]
         forCellReuseIdentifier:kMyCareCellID];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins: UIEdgeInsetsZero];
    }
    
}

- (void)pullCareData {
    
    
    NSDictionary *paramDic = @{@"service": CircleGetFollowsList,
                               @"pageIndex": @"1",
                               @"pageSize": @"9999",
                               @"userId": KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层x
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                NSArray * arr = result[REQUEST_LIST][REQUEST_LIST];
                
                if (arr.count > 0) {
                    self.model = [NSMutableArray arrayWithCapacity:arr.count];
                    
                    for (id item in arr) {
                        [self.model addObject:[FriendModel mj_objectWithKeyValues:item]];
                    }
                    
                    [self.tableView reloadData];
                    
                }
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
            if (self.model.count==0) {
                if (!self.tipView) {
                    self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                    
                }
                [self.tableView addSubview:self.tipView];
                [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                    
                    make.center.equalTo(self.view);
                    make.size.equalTo(CGSizeMake(120, 120));
                    
                }];
                
            }else
            {
                if (self.tipView.superview) {
                    
                    [self.tipView removeFromSuperview];
                }
            }
            
        }
    }];
    
}


- (void)deleteCare:(NSIndexPath *)index {
    
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        NSDictionary *paramDic = @{@"service": DeleteUserFollow,
                                   @"pId": self.model[index.row].userId,
                                   @"ctype": @(1),
                                   @"userId": KgetUserValueByParaName(USERID)};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    
                    [self.model removeObjectAtIndex:index.row];
                    [self.tableView deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationTop];
                    
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    
                }
                
            }
        }];
    }
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.model.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PeopleCell * cell = [tableView dequeueReusableCellWithIdentifier:kMyCareCellID
                                                        forIndexPath:indexPath];
    [cell configureCellWithFriendModel:self.model[indexPath.row]];
    
    return cell;
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive
                                                                      title:@"取消关注"
                                                                    handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                                                        
                                                                        [self deleteCare:indexPath];
                                                                        
                                                                    }];
    
    
    return @[delete];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 60, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 60, 0, 0)];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FriendModel *model = _model[indexPath.row];
    PersonHomeViewController *per  = [[PersonHomeViewController alloc]init];
    per.userId = model.userId.intValue;
    [self.navigationController pushViewController:per animated:YES];
    
}



@end
