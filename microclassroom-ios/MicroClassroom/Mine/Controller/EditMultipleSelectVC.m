//
//  EditMultipleSelectVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "EditMultipleSelectVC.h"
#import "DataSelectCell.h"
#import "CodeListModel.h"
#import "UserInfoModel.h"
static NSString * const kEditMultipleCell = @"DataSelectCell";
static NSString * const kEditHaveNavTitle = @"编辑资源";
static NSString * const kEditNeedNavTitle = @"编辑需求";
@interface EditMultipleSelectVC ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSArray<CodeListModel *> *dataArr;
@end

@implementation EditMultipleSelectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    [self setNav];
    
    [self pullData];
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:kEditMultipleCell bundle:nil] forCellReuseIdentifier:kEditMultipleCell];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView  setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if (IOS8) {
        if ([self.tableView  respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableView  setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setNav {
    
    switch (self.editType) {
        case MCEditTypeHave:
            self.yh_navigationItem.title = kEditHaveNavTitle;
            break;
        case MCEditTypeNeed:
            self.yh_navigationItem.title = kEditNeedNavTitle;
            break;
            case MCEditTypeIndustry:
            self.yh_navigationItem.title = @"编辑行业";
            break;
        default:
            break;
    }
    
    if (self.editType == MCEditTypeIndustry){
        return;
    }
    
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc] initWithTitle:@"保存" style:0 handler:^(id send) {
        @strongify(self);
        
        [self saveEdit];
        
    }];
}

- (void)saveEdit {
    [self.view endEditing:YES];
    NSString *key;
    NSMutableString *value = [[NSMutableString alloc] init];
    NSMutableString *code = [[NSMutableString alloc] init];
    NSMutableArray *valueArr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *selectDataArr = [NSMutableArray array];
    
    switch (self.editType) {
        case MCEditTypeHave:
            key = @"labelHave";
            break;
        case MCEditTypeNeed:
            key = @"labelNeed";
            break;
        default:
            break;
    }
    
    [self.dataArr enumerateObjectsUsingBlock:^(CodeListModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.isSelect) {
            if (value.length == 0) {
                [value appendString:obj.codeName];
                [code appendString:obj.codeId];
     
            } else {
                [value appendString:[NSString stringWithFormat:@"、%@",obj.codeName]];
                [code appendString:[NSString stringWithFormat:@",%@",obj.codeId]];
            }
             [code appendFormat:@"@"];
          if (NOEmptyStr(obj.meno)){
             [code appendString:obj.meno];
           }
            [valueArr addObject:obj.codeName];
            [selectDataArr addObject:obj];
        }
    }];
    
    NSDictionary *paramDic = @{@"service": UserSetMoreInfo,
                               @"userId": KgetUserValueByParaName(USERID),
                               @"fildValue": key,
                               @"setValue": code};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
       if (self.block){
         self.block(selectDataArr, value);
        }
        [self.navigationController popViewControllerAnimated:YES];
                
    } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
}

- (void)pullData {
    
    NSDictionary *paramDic = @{Service: AppGetCodeList,
                               @"ctype": @(self.editType)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                
                self.dataArr = [CodeListModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
       // 配置选中状态
     switch (self.editType) {
           case MCEditTypeHave:{
                 for (CodeListModel *model in self.dataArr){
                   for (NSDictionary *dic in [UserInfoModel getInfoModel].label_have){
                      if ([model.codeId isEqualToString:dic[@"codeId"]]){
                           model.isSelect = YES;
                           model.meno = dic[@"meno"];
                    }else{
                        model.isSelect = NO;
                  }
               }
            }
         }
     break;
           case MCEditTypeNeed:{
                 for (CodeListModel *model in self.dataArr){
                    for (NSDictionary *dic in [UserInfoModel getInfoModel].label_need){
                         if ([model.codeId isEqualToString:dic[@"codeId"]]){
                                model.isSelect = YES;
                         }else{
                               model.isSelect = NO;
                        }
                    }
                }
         }
     break;
     case MCEditTypeIndustry:{
            for (CodeListModel *model in self.dataArr){
                 for (CodeListModel *inModel in self.defaultSelectArr){
                     if ([model.codeId isEqualToString:inModel.codeId]){
                         model.isSelect = YES;
                     }else{
                       model.isSelect = NO;
                   }
                }
            }
        }
     break;
     default:
     break;
     }
                [self.tableView reloadData];
            }
        }
     }];
}


#pragma mark - 设置行业
- (void)setUserSetIndustryId:(CodeListModel *)model {
    
    NSDictionary *paramDic = @{Service:UserSetIndustryId,
        @"userId":KgetUserValueByParaName(USERID),
        @"industryId":model.codeId
    };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
     HIDENHUD;
     if (result) {
     if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
     if(self.block){
     self.block (@[model], model.codeName);
     [self backBtnAction];
     }
     }else {
     [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
     }
    }
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DataSelectCell * cell = [tableView dequeueReusableCellWithIdentifier:kEditMultipleCell forIndexPath:indexPath];
    cell.editType = self.editType;
    [cell configWithModel:self.dataArr[indexPath.row]];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CodeListModel *model = self.dataArr[indexPath.row];
    if(self.editType == MCEditTypeIndustry) {
        [self setUserSetIndustryId:model];
    }else {
        model.isSelect = !model.isSelect;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
@end
