//
//  MyCollectionVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyCollectionVC.h"
#import "MicroClassDetailVC.h"

#import "MicroClassListCell.h"

#import "CareCollectionModel.h"

#import "YHBaseWebViewController.h"
#import "MicroClassDetailViewController.h"
#import "BooksDetailViewController.h"

static NSString * const kMyCollectionVCTitle = @"我的收藏";
static NSString * const kMyCollectionCellID = @"MicroClassListCell";
@interface MyCollectionVC ()
<
UITableViewDataSource,
UITableViewDelegate
>
@property (strong, nonatomic) NSMutableArray<CollectionModel *> * model;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MyCollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.yh_navigationItem.title = kMyCollectionVCTitle;
    [self setUpTable];
    [self pullCollectionData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpTable {
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:kMyCollectionCellID bundle:nil]
         forCellReuseIdentifier:kMyCollectionCellID];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins: UIEdgeInsetsZero];
    }
    
}

- (void)pullCollectionData {
    
    NSDictionary *paramDic = @{@"service": GetUserCollections,
                               @"ctype": @"1",
                               @"userId": KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                NSArray * arr = result[REQUEST_LIST];
                
                if (arr.count > 0) {
                    self.model = [NSMutableArray arrayWithCapacity:arr.count];
                    
                    for (id item in arr) {
                        [self.model addObject: [CollectionModel mj_objectWithKeyValues:item]];
                    }
                    
                    [self.tableView reloadData];
                    
                }
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}

- (void)deleteCollection:(NSIndexPath *)index {
    
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        NSDictionary *paramDic = @{@"service": DeleteUserCollection,
                                   @"pId": self.model[index.row].videoId,
                                   @"ctype": @(1),
                                   @"userId": KgetUserValueByParaName(USERID)};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    
                    [self.model removeObjectAtIndex:index.row];
                    [self.tableView deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationTop];
                    
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    
                }
                
            }
        }];
        
    }
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.model.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MicroClassListCell * cell = [tableView dequeueReusableCellWithIdentifier:kMyCollectionCellID forIndexPath:indexPath];
    
    [cell configWithCollectionModel:self.model[indexPath.row]];
    
    return cell;
    
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive
                                                                      title:@"取消收藏"
                                                                    handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                                                        
                                                                        [self deleteCollection:indexPath];
                                                                        
                                                                    }];
    
    
    return @[delete];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 103;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CollectionModel *model = self.model[indexPath.row];;
    if (model.isMp3  == MicClassMp3TypeWeb) {//web
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.videoUrl andNavTitle:model.videoTitle];
        [self.navigationController pushViewController:web animated:YES];
    }else if (model.isMp3 == MicClassMp3TypeBook){//书籍详情
        BooksDetailViewController *detailVC = [[BooksDetailViewController alloc]init];
        detailVC.videoId = model.videoId;
        [self.navigationController pushViewController:detailVC animated:YES];
    }else{
        MicroClassDetailViewController * targetVC = [[MicroClassDetailViewController alloc] init];
        targetVC.videoId = model.videoId;
        [self.navigationController pushViewController:targetVC animated:YES];
    }
    
}

@end
