//
//  ModifyNickNameViewController.m
//  yunbo2016
//
//  Created by Hanks on 16/1/11.
//  Copyright © 2016年 apple. All rights reserved.
//

#import "ModifyNickNameViewController.h"
#import "UserInfoModel.h"

@interface ModifyNickNameViewController ()<UITextFieldDelegate,UIAlertViewDelegate>

@property (nonatomic , strong) UITextField *myTextField;
@property (nonatomic,   assign)  BOOL  isSave;
@property (nonatomic ,strong) UserInfoModel *model;

@end

@implementation ModifyNickNameViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    if (self.nickRequest) {
        _model = [UserInfoModel getInfoModel];
    }else{
        _model = nil;
    }
    
    [self configNav];
    [self topView];
    
    
}


-(void)configNav
{
    self.yh_navigationItem.title = self.titleStr;
    
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithTitle:(self.nickRequest == YES ? @"提交" :@"确定") style:YHBarButtonItemStylePlain handler:^(id sender) {
        @strongify(self);
        if (self.nickRequest) {
            [self requestModify];
        }else{
            if (self.backSetBlock) {
                self.backSetBlock(_myTextField.text);
                 DELAYEXECUTE(0.5, [self backBtnAction]);
            }
        }
    }];
    
    self.yh_navigationItem.rightBarButtonItem.enabled= NO;
}


- (void)topView
{
    UIView *bgView = [[UIView alloc] init];
    [bgView setFrame:CGRectMake(0, KTopHeight + 20, CGRectGetWidth(self.view.bounds), 44)];
    [bgView setBackgroundColor:[UIColor whiteColor]];
//    [SeparatorHelper separatorForView:bgView type:kSeparatorTopType color:KColorRGB(191, 191, 191)];
//    [SeparatorHelper separatorForView:bgView type:kSeparatorBottomType color:KColorRGB(191, 191, 191)];
    
    UILabel *info = [[UILabel alloc] init];
    [info setFrame:CGRectMake(10, 6, 40, 32)];
    [info setBackgroundColor:[UIColor clearColor]];
    [info setTextColor:KColorRGB(51, 51, 51)];
    [info setTextAlignment:NSTextAlignmentRight];
    [info setFont:[UIFont systemFontOfSize:13]];
    [info setText:self.titleLbText];
    [bgView addSubview:info];
    
    UIView *textView = [UIView new];
    textView.frame = CGRectMake(75, 6, KScreenWidth-75-15-10, 32);
    [textView setBackgroundColor:[UIColor whiteColor]];
    [textView.layer setCornerRadius:3];
    [textView.layer setBorderWidth:0.5];
    [textView.layer setBorderColor:KColorRGB(200, 200, 200).CGColor];
    
 
    _myTextField = [[UITextField alloc] init];
    _myTextField.delegate  = self;
    _myTextField.textColor = KColorRGB(130, 128, 128);
    _myTextField.font = [UIFont systemFontOfSize:17];
    [_myTextField setFrame:CGRectMake(10, 0, CGRectGetWidth(textView.frame)-20, 32)];
    [_myTextField setBackgroundColor:[UIColor clearColor]];
    _myTextField.returnKeyType = UIReturnKeyDone;
    if (self.nickRequest == YES) {
        [_myTextField setText:_model.nickname];
    }else{
        [_myTextField setText:self.placeText];

        }
   
    [textView addSubview:_myTextField];
    [bgView addSubview:textView];
    
    [self.view addSubview:bgView];
}


-(void)requestModify
{
    if ([_model.nickname isEqualToString:_myTextField.text]) {
        
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    NSDictionary *sendDic = @{@"service":@"User.setNickname",
                              @"userId":KgetUserValueByParaName(USERID),
                              @"nickname":_myTextField.text};
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                
                _model.nickname = _myTextField.text;
                [UserInfoModel saveUserInfoModelWithModel:_model];
                [WFHudView showMsg:@"修改成功" inView:nil];
                _isSave  = YES;
                DELAYEXECUTE(0.5, [self backBtnAction]);
                
            }else
            {
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }
        
    }];
}


-(BOOL)checkParam
{
    
    if (NOEmptyStr(_myTextField.text)) {
        
        return YES;
        
    }else
    {
        [self popView:@"学名不能为空"];
        
        return NO;
    }
    
    return YES;
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.yh_navigationItem.rightBarButtonItem.enabled = YES;
}

-(void)backBtnAction
{
    if (!self.nickRequest) {
        [super backBtnAction];
        return;
    }
    
    if (![_model.nickname isEqualToString:_myTextField.text]&&_isSave==NO) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"修改尚未保存确认返回吗" delegate:self cancelButtonTitle:@"返回" otherButtonTitles:@"保存",nil];
        [alertView show];
    }else
    {
        if (self.backSetBlock) {
            self.backSetBlock(_model.nickname);
        }
        [super backBtnAction];
    }
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        [super backBtnAction];
    }else
    {
        [self requestModify];
    }
}


@end
