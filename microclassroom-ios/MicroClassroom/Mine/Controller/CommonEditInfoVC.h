//
//  CommonEditInfoVC.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/1.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"

@interface CommonEditInfoVC : YHBaseViewController
@property (nonatomic, assign) MCEditAction editAction;
@property (nonatomic, copy) NSString *defaultText;
@property (nonatomic, copy) NSString *friendId;

@property (nonatomic, copy) void(^block)(NSString *str);
@end
