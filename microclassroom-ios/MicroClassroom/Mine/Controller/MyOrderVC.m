//
//  MyOrderVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyOrderVC.h"
#import "AwaystationDetailVC.h"
#import "MyOrderCell.h"
#import "MineWalletViewController.h"
#import "MyOrderModel.h"
#import "AwaystationCommentVC.h"
#import <MJRefresh.h>

static NSString * const kMyOrderNavTitle = @"我的订单";
static NSString * const kMyOrderCell = @"MyOrderCell";
static NSInteger pagCount = 1;
@interface MyOrderVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray<MyOrderModel *> *dataArr;
@property (nonatomic ,strong) DIYTipView *tipView;

@end

@implementation MyOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.yh_navigationItem.title = kMyOrderNavTitle;
    
    [self setUpTableView];
    [self pullData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpTableView {
    
    self.tableView.tableFooterView = [UIView new];
    
    [self.tableView registerNib:[UINib nibWithNibName:kMyOrderCell bundle:nil]
         forCellReuseIdentifier:kMyOrderCell];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins: UIEdgeInsetsZero];
    }
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefesh)];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footRefresh)];
    self.tableView.mj_footer.hidden = YES;

}

- (void)headRefesh {
    [self.dataArr removeAllObjects];
    pagCount = 1;
    [self pullData];
}


-(void)footRefresh {
    pagCount ++ ;
    [self pullData];
}

- (void)pullData {
    
    
    NSDictionary *paramDic = @{@"service": GetOrderList,
                               @"pageIndex": @(pagCount),
                               @"pageSize": @"20",
                               @"type": @"0",
                               @"userId": KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (pagCount == 1) {
            [self.tableView.mj_header endRefreshing];
        }else{
            [self.tableView.mj_footer endRefreshing];
        }
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                NSArray * arr = result[REQUEST_LIST][REQUEST_LIST];
                
                if (arr.count > 0) {
                    
                    for (id item in arr) {
                        [self.dataArr addObject:[MyOrderModel mj_objectWithKeyValues:item]];
                    }
                    
                    [self.tableView reloadData];
                }
                
            }
//            else { //其他代表失败  如果没有详细说明 直接提示错误信息
//
//                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
//            }
            if (self.dataArr.count==0) {
                if (!self.tipView) {
                    self.tipView = [[DIYTipView alloc]initWithInfo:result[REQUEST_MESSAGE] andBtnTitle:nil andClickBlock:nil];
                }
                [self.tableView addSubview:self.tipView];
                [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.center.equalTo(self.view);
                    make.size.equalTo(CGSizeMake(120, 120));
                }];
            }else
            {
                if (self.tipView.superview) {
                    [self.tipView removeFromSuperview];
                }
            }
        }
    }];
}

- (NSMutableArray<MyOrderModel *> *)dataArr {
    
    if (!_dataArr) {
        _dataArr = [NSMutableArray arrayWithCapacity:1];
    }
    
    return _dataArr;
}

# pragma mark - MyOrderCellDelagate
- (void)payFroOrder:(NSIndexPath *)index {
//    MyOrderModel *model = self.dataArr[index.row];
//    NSDictionary *paramDic = @{Service:PinncPayForOrder,
//                               @"userId":KgetUserValueByParaName(USERID),
//                               @"orderId":model.orderId,
//                               };
//    
//    SHOWHUD;
//    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
//        HIDENHUD;
//        if (result) {
//            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
//                [WFHudView showMsg:@"支付成功!" inView:nil];
//            }else if([result[REQUEST_CODE] isEqualToNumber:@2]){
//                @weakify(self);
//                [YHJHelp aletWithTitle:@"提示" Message:@"帐号余额不足，是否前往充值" sureTitle:@"充值" CancelTitle:@"取消" SureBlock:^{
//                    @strongify(self);
//                    MineWalletViewController *contr = [[MineWalletViewController alloc]init];
//                    [self.navigationController pushViewController:contr animated:YES];
//                } andCancelBlock:nil andDelegate:self];
//            }
//        }
//    }];
}

# pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsZero];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (self.dataArr[indexPath.row].items.count > 0) {
//        switch (self.dataArr[indexPath.row].items[0].outItemType) {
//            case MCMyOrderTypeSite:
//            {
//                AwaystationDetailVC * targetVC = [[AwaystationDetailVC alloc] init];
//                targetVC.webUrlStr = self.dataArr[indexPath.row].items[0].site_detail_url;
//                targetVC.SiteId = [self.dataArr[indexPath.row].items[0].outItemId intValue];
//                [self.navigationController pushViewController:targetVC animated:YES];
//
//            }
//                break;
//            case MCMyOrderTypeZhiBo:
//
//                break;
//            default:
//                break;
//        }
//    }
}

# pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyOrderCell * cell = [tableView dequeueReusableCellWithIdentifier:kMyOrderCell forIndexPath:indexPath];
    [cell configureCellWithMyOrderModel:self.dataArr[indexPath.row] Index:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    @weakify(self);
    cell.commentBtnBlock = ^(MyOrderModel *model){
        @strongify(self);
        AwaystationCommentVC *vc = [[AwaystationCommentVC alloc]initWithOrderId:model.orderId];
        [self.navigationController pushViewController:vc animated:YES];
  
        vc.commentSuccess = ^{
            [self.tableView.mj_header beginRefreshing];
        };
    };
    
    return cell;

}


@end
