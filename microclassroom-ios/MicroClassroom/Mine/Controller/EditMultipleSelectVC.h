//
//  EditMultipleSelectVC.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"


@interface EditMultipleSelectVC : YHBaseViewController
@property (nonatomic, copy) NSArray *defaultSelectArr;
@property (nonatomic, copy) void(^block)(NSArray *selectArr, NSString *selectStr);
@property (nonatomic, assign) MCEditType editType;
@end
