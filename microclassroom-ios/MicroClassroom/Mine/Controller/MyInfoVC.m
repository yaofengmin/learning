//
//  MyInfoVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "MyInfoVC.h"
#import "MyMoreInfoVC.h"
#import "LoginViewController.h"
#import "YHNavgationController.h"
#import "CameraTakeManager.h"
#import "CommonEditInfoVC.h"
#import "EditGenderVC.h"
#import "MyInfoHeaderView.h"
#import "MyInfoFooterView.h"
#import "MyInfoCell.h"
#import "BuyVIPViewController.h"
#import "UserInfoModel.h"

static NSString * const kMyInfoVCTitle = @"我的信息";
static NSString * const kMyInfoCellID = @"MyInfoCell";

@interface MyInfoVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (copy, nonatomic) NSArray<NSArray *> * titleArr;
@property (strong, nonatomic) NSMutableArray * tableModel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) MyInfoHeaderView *tableHeader;
@property (strong, nonatomic) MyInfoFooterView *tableFooter;
@property (strong, nonatomic) UserInfoModel * userInfoModel;
@end

@implementation MyInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.yh_navigationItem.title = kMyInfoVCTitle;
    [self setUpTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableHeader configureViewWithModel:self.userInfoModel];
    _titleArr = @[@[@"学名", @"个性签名", @"所在地区",@"单位职务"],@[@"更多信息"]];//@"性别",
//    if ([KgetUserValueByParaName(KBrandInfoId) integerValue] > 0) {
//    }else{
//        _titleArr = @[@[@"学名", @"性别", @"个性签名", @"所在地区",@"单位职务", @"会员等级"],@[@"更多信息"]];
//    }
    [self.tableView reloadData];
}

//- (NSArray<NSArray *> *)titleArr {
//
//    if (!_titleArr) {
//        if ([KgetUserValueByParaName(KBrandInfoId) integerValue] > 0) {
//            _titleArr = @[@[@"学名", @"性别", @"个性签名", @"所在地区", @"单位职务"],@[@"更多信息"]];
//        }else{
//            _titleArr = @[@[@"学名", @"性别", @"个性签名", @"所在地区", @"单位职务", @"会员等级"],@[@"更多信息"]];
//        }
//    }
//    return _titleArr;
//}

- (NSMutableArray *)tableModel {
    
    NSMutableArray *dataArr = [NSMutableArray array];
    if (!_tableModel) {
        dataArr = @[@{@"title": @"学名", @"content": self.userInfoModel.nickname}.mutableCopy,
//                    @{@"title": @"性别", @"content": self.userInfoModel.sex}.mutableCopy,
                    @{@"title": @"个性签名", @"content": self.userInfoModel.personalSign}.mutableCopy,
                    @{@"title": @"所在地区", @"content": self.userInfoModel.areaName}.mutableCopy,
//                    @{@"title": @"所属单位", @"content": self.userInfoModel.company}.mutableCopy,
                    @{@"title": @"单位职务", @"content": self.userInfoModel.title}.mutableCopy,
                    ].mutableCopy;
//        if ([KgetUserValueByParaName(KBrandInfoId) integerValue] == 0) {
//            [dataArr addObject:@{@"title": @"会员等级", @"content": self.userInfoModel.grade_name, @"color": [UIColor redColor]}.mutableCopy];
//        }
        _tableModel = dataArr.mutableCopy;
    }
    return _tableModel;
}

- (UserInfoModel *)userInfoModel {
    if (!_userInfoModel) {
        _userInfoModel = [UserInfoModel getInfoModel];
    }
    
    return _userInfoModel;
}

- (MyInfoHeaderView *)tableHeader {
    if (!_tableHeader) {
        _tableHeader = [[MyInfoHeaderView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 180)];
    }
    
    return _tableHeader;
}

- (MyInfoFooterView *)tableFooter {
    if (!_tableFooter) {
        _tableFooter = [[MyInfoFooterView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 110)];
    }
    
    return _tableFooter;
}

- (void)setUpTable {
    
    [self.tableView registerNib:[UINib nibWithNibName:kMyInfoCellID bundle:nil]
         forCellReuseIdentifier:kMyInfoCellID];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins: UIEdgeInsetsZero];
    }
}

- (void)setAvatar:(UIImage *)image {
    
    NSDictionary *paramDic = @{@"service": SetAvatar,
                               @"userId": KgetUserValueByParaName(USERID)};
    
    NSDictionary *imageDic = @{@"pics":@[UIImageJPEGRepresentation(image, 0.5)],
                               @"fileLoc":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager publishCarCirclWithUrl:MainAPI andPostParameters:paramDic andImageDic:imageDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                self.userInfoModel.photo = result[@"url"];
                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                [self.tableHeader configureViewWithModel:self.userInfoModel];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
    }];
    
}

- (void)editInfo:(NSIndexPath *)index {
    switch (index.row) {
        case 0:
        {
            @weakify(self);
            CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
            targetVC.editAction = MCEditActionNickName;
            targetVC.defaultText = self.userInfoModel.nickname;
            targetVC.block = ^(NSString *str){
                @strongify(self);
                self.userInfoModel.nickname = str;
                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                self.tableModel[index.row][@"content"] = self.userInfoModel.nickname;
                [self.tableView reloadData];
            };
            [self.navigationController pushViewController:targetVC animated:YES];
        }
            break;
//        case 1:
//        {
//            @weakify(self);
//            EditGenderVC * targetVC = [[EditGenderVC alloc] init];
//            targetVC.defaultText = self.userInfoModel.sex;
//            targetVC.block = ^(NSString *str){
//                @strongify(self);
//                self.userInfoModel.sex = str;
//                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
//                self.tableModel[index.row][@"content"] = self.userInfoModel.sex;
//                [self.tableView reloadData];
//            };
//            [self.navigationController pushViewController:targetVC animated:YES];
//        }
//            break;
        case 1:
        {
            @weakify(self);
            CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
            targetVC.editAction = MCEditActionSign;
            targetVC.defaultText = self.userInfoModel.personalSign;
            targetVC.block = ^(NSString *str){
                @strongify(self);
                self.userInfoModel.personalSign = str;
                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                self.tableModel[index.row][@"content"] = self.userInfoModel.personalSign;
                [self.tableView reloadData];
            };
            [self.navigationController pushViewController:targetVC animated:YES];
        }
            break;
        case 2:
        {
            @weakify(self);
            CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
            targetVC.editAction = MCEditChangeArea;
            targetVC.defaultText = self.userInfoModel.areaName;
            targetVC.block = ^(NSString *str){
                @strongify(self);
                self.userInfoModel.areaName = str;
                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                self.tableModel[index.row][@"content"] = self.userInfoModel.areaName;
                [self.tableView reloadData];
            };
            [self.navigationController pushViewController:targetVC animated:YES];
        }
            
            break;
//        case 4:
//        {
//            @weakify(self);
//            CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
//            targetVC.editAction = MCEditActionCompany;
//            targetVC.defaultText = self.userInfoModel.company;
//            targetVC.block = ^(NSString *str){
//                @strongify(self);
//                self.userInfoModel.company = str;
//                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
//                self.tableModel[index.row][@"content"] = self.userInfoModel.company;
//                [self.tableView reloadData];
//            };
//            [self.navigationController pushViewController:targetVC animated:YES];
//        }
//            break;
        case 3:
        {
            @weakify(self);
            CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
            targetVC.editAction = MCEditActionTitle;
            targetVC.defaultText = self.userInfoModel.title;
            targetVC.block = ^(NSString *str){
                @strongify(self);
                self.userInfoModel.title = str;
                [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
                self.tableModel[index.row][@"content"] = self.userInfoModel.title;
                [self.tableView reloadData];
            };
            [self.navigationController pushViewController:targetVC animated:YES];
        }
            break;
        case 4:
        {
            BuyVIPViewController *vip = [[BuyVIPViewController alloc]init];
            [self.navigationController pushViewController:vip animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UIButton

- (void)changeAvatar:(UIButton *)sender {
    
    [self.view endEditing:YES];
    @weakify(self);
    [[CameraTakeManager sharedInstance] cameraSheetInController:self handler:^(UIImage *image, NSString *imagePath) {
        @strongify(self);
        
        [self setAvatar:image];
        
    } cancelHandler:^{}];
    
}

- (void)changeAccount:(UIButton *)sender {
    
    [self.view endEditing:YES];
    @weakify(self);
    CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
    targetVC.editAction = MCEditActionEmail;
    targetVC.defaultText = self.userInfoModel.email;
    targetVC.block = ^(NSString *str){
        @strongify(self);
        self.userInfoModel.email = str;
        [UserInfoModel saveUserInfoModelWithModel:self.userInfoModel];
        [self.tableHeader configureViewWithModel:self.userInfoModel];
        [self.tableView reloadData];
    };
    [self.navigationController pushViewController:targetVC animated:YES];
    
}

- (void)logoutAccount:(UIButton *)sender {
    
    NSDictionary   *sendDic = @{@"service":Logout,
                                @"userId":KgetUserValueByParaName(USERID)
                                };
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
            [UserInfoModel clearModel];
            _userInfoModel = nil;
            _tableModel = nil;
            /**
             *  环信 推出登录
             */
            [[EMClient sharedClient] logout:YES];
            LoginViewController *login = [[LoginViewController alloc]init];
            YHNavgationController *nav = [[YHNavgationController alloc]initWithRootViewController:login];
            nav.modalPresentationStyle = 0;
            [self presentViewController:nav animated:YES completion:nil];
        }else{
            [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
        }
    }];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.titleArr[section].count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyInfoCell * cell = [tableView dequeueReusableCellWithIdentifier:kMyInfoCellID
                                                        forIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
            [cell configWithModel:self.tableModel[indexPath.row]];
            break;
        case 1:
            [cell configWithModel:@{@"title": self.titleArr[indexPath.section][indexPath.row],
                                    @"content": @""}];
        default:
            break;
    }
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return 110;
    }
    
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 180;
    }
    
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if (section == 1) {
        return self.tableFooter;
    }
    
    return [UIView new];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return self.tableHeader;
    }
    
    return [UIView new];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        MyMoreInfoVC * targetVC = [[MyMoreInfoVC alloc] init];
        [self.navigationController pushViewController:targetVC animated:YES];
    } else {
        [self editInfo:indexPath];
    }
    
}

@end
