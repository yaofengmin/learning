

//
//  modifyPassWordViewController.m
//  yunbo2016
//
//  Created by Hanks on 16/1/11.
//  Copyright © 2016年 apple. All rights reserved.
//

#import "ModifyPassWordViewController.h"
#import "WFHudView.h"

@interface ModifyPassWordViewController ()<UITextFieldDelegate>

@property(nonatomic ,strong) UITextField *oldPasswF;
@property(nonatomic ,strong) UITextField *xinPasswF;
@property(nonatomic ,strong) UITextField *xinPasswF2;
@end

@implementation ModifyPassWordViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = BColor;
    [self configNav];
    [self creatUI];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapEndEditing:)];
    [self.view addGestureRecognizer:tap];
}

-(void)configNav
{
    self.yh_navigationItem.title = @"修改密码";
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithTitle:@"提交" style:YHBarButtonItemStylePlain handler:^(id sender) {
        @strongify(self);
        
        [self requestModify];
    }];
}

-(void)creatUI
{
    
    
    UIImageView *pawImage = [self getImageWithName:@"ic_password"];
    
    _oldPasswF = [self getField];
    _oldPasswF.placeholder = @"请输入原密码";
    
    UIView *firstRowView =[self filed:_oldPasswF WithImage:pawImage];
    [self.view addSubview:firstRowView];
    
    [firstRowView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(KTopHeight + 25);
        make.left.mas_equalTo(22);
        make.right.mas_equalTo(-22);
        make.height.equalTo(44);
        
    }];
    UIImageView *newPawImage = [self getImageWithName:@"ic_password"];
    
    _xinPasswF = [self getField];
    _xinPasswF.placeholder = @"请输入新密码";
    UIView *secondView = [self filed:_xinPasswF WithImage:newPawImage];
    [self.view addSubview:secondView];
    
    [secondView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(firstRowView.mas_bottom).offset(10);
        make.left.mas_equalTo(22);
        make.right.mas_equalTo(-22);
        make.height.equalTo(44);
        
    }];
    
    
    UIImageView *surePawImage = [self getImageWithName:@"ic_confirmpassword"];
    
    _xinPasswF2 = [self getField];
    _xinPasswF2.placeholder = @"再次请输入新密码";
    UIView *secondView2 = [self filed:_xinPasswF2 WithImage:surePawImage];
    [self.view addSubview:secondView2];
    
    [secondView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(secondView.mas_bottom).offset(10);
        make.left.mas_equalTo(22);
        make.right.mas_equalTo(-22);
        make.height.equalTo(44);
        
    }];
    
}

#pragma mark === 修改密码 ===
-(void)requestModify
{
    if ([self checkParam]) {
        
        NSDictionary *paramDic = @{@"service":@"User.setPwd",
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"oldPwd":[NSString md5:_oldPasswF.text],
                                   @"newPwd":[NSString md5:_xinPasswF.text]};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    
                    [self.view endEditing:YES];
                    self.oldPasswF.text  = @"";
                    self.xinPasswF.text  = @"";
                    self.xinPasswF2.text = @"";
                }
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
        }];
    }
    
    
}

-(BOOL)checkParam
{
    if (_oldPasswF.text.length==0) {
        
        [self popView:@"请输入原始密码"];
        return NO;
    }
    
    if (_xinPasswF.text.length<6) {
        
        alertContent(@"新密码长度不小于6位");
        
        return NO;
    }
    
    
    if (![_xinPasswF.text isEqualToString:_xinPasswF2.text]) {
        
        [WFHudView showMsg:@"两次密码不一致" inView:nil];
        return NO;
    }
    
    return YES;
}




-(UITextField *)getField
{
    UITextField *field = [[UITextField alloc] init];
    [field setDelegate:self];
    field.textColor = KColorRGB(130, 128, 128);
    field.font = [UIFont systemFontOfSize:14];
    field.secureTextEntry = YES;
    
    return field;
}



-(UIView*)filed:(UITextField *)filed WithImage:(UIImageView *)imageV
{
    UIView *showView = [UIView new];
    [showView setBackgroundColor:NCColor];
    [showView.layer setCornerRadius:4.0];
    showView.layer.masksToBounds = YES;
    //    [showView.layer setBorderWidth:0.5];
    //    [showView.layer setBorderColor:KColorRGB(200, 200, 200).CGColor];
    [showView addSubview:imageV];
    [showView addSubview:filed];
    
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(44, 44));
    }];
    
    [filed mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageV.mas_right).offset(0);
        make.centerY.equalTo(imageV);
        make.height.equalTo(30);
        make.right.mas_equalTo(-10);
    }];
    
    
    return showView;
}
-(UIImageView *)getImageWithName:(NSString *)name
{
    UIImageView *pawImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:name]];
    return pawImage;
}


#pragma mark === tapEndEditing
-(void)tapEndEditing:(UITapGestureRecognizer *)tap
{
    [self.view endEditing:YES];
}


@end
