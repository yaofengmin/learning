//
//  MyIntegraListViewController.m
//  MicroClassroom
//
//  Created by yfm on 2018/3/2.
//  Copyright © 2018年 Hanks. All rights reserved.
//

#import "MyIntegraListViewController.h"

#import "IntegralRecordTableViewCell.h"

#import "MyIntegrateRecordModel.h"

#import "MyIntegraHeader.h"
#import "IntegraSectionHeader.h"

static NSString *kIntegralRecordTableViewCell = @"IntegralRecordTableViewCell";
@interface MyIntegraListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableV;

@property (nonatomic,assign)   NSInteger    pageCount;

@property (nonatomic,assign)   NSInteger    total;;

@property (nonatomic,strong)   NSMutableArray *dataArr;

@property (nonatomic,strong)   NSMutableArray *listAllArr;//专门判断是否有更多数据

@property (nonatomic,strong) MyIntegrateModel *listModel;

@property (nonatomic,strong) DIYTipView *tipView;

@property (nonatomic,strong) MyIntegraHeader *integraHeader;



@end

@implementation MyIntegraListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationBar.hidden = YES;
    _pageCount = 1;
    _dataArr = [NSMutableArray array];
    _listAllArr = [NSMutableArray array];
    [self initalTable];
    [self requestList];
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:kIntegralRecordTableViewCell bundle:nil] forCellReuseIdentifier:kIntegralRecordTableViewCell];
    if (@available(iOS 11.0, *)) {
        self.tableV.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableV.estimatedRowHeight = 0;
        self.tableV.estimatedSectionFooterHeight = 0;
        self.tableV.estimatedSectionHeaderHeight = 0;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.tableV.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.tableV.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    [self creatHeader];
    
}

- (void)creatHeader
{
    UIView *headerBg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 210)];
    _integraHeader = [[NSBundle mainBundle] loadNibNamed:@"MyIntegraHeader" owner:self options:nil].firstObject;
    [headerBg addSubview:_integraHeader];
    [_integraHeader mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    self.tableV.tableHeaderView = headerBg;
}
#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    MyIntergrateListModel *model = self.dataArr[section];
    return model.ListRecord.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IntegralRecordTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kIntegralRecordTableViewCell forIndexPath:indexPath];
    MyIntergrateListModel *model = self.dataArr[indexPath.section];
    cell.model = model.ListRecord[indexPath.row];
    return cell;
}

#pragma mark === UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 43)];
    IntegraSectionHeader *sectionHeader = [[NSBundle mainBundle] loadNibNamed:@"IntegraSectionHeader" owner:self options:nil].firstObject;
    [header addSubview:sectionHeader];
    [sectionHeader mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    MyIntergrateListModel *model = self.dataArr[section];
    sectionHeader.dateLabel.text = model.createTime;
    sectionHeader.topLine.hidden = !section;
    return header;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 43;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

#pragma mark - 刷新
-(void)loadDataForHeader {
    _pageCount = 1;
    [self requestList];
    
}

#pragma mark - 加载
- (void)loadMoreData {
    _pageCount ++ ;
    [self requestList];
}
- (void)requestList {
    NSDictionary *paramDic = @{Service:GetIntegrateRecord,
                               @"pageIndex":@(_pageCount),
                               @"pageSize":@20,
                               @"userId":KgetUserValueByParaName(USERID)};
    SHOWHUD;
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        @strongify(self);
        if (_pageCount == 1) {
            [self.tableV.mj_header endRefreshing];
            [self.dataArr removeAllObjects];
        }else {
            [self.tableV.mj_footer endRefreshing];
        }
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _listModel = [MyIntegrateModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                [self.dataArr addObjectsFromArray:_listModel.list];
                _total = _listModel.total;
                
                _integraHeader.totalIntegraLabel.text = [NSString stringWithFormat:@"%d",_listModel.totalIntegrate];
                
                for (MyIntergrateListModel *listModel in _listModel.list) {
                    for (MyIntegrateRecordModel *recordMode in listModel.ListRecord) {
                        [self.listAllArr addObject:recordMode];
                    }
                }
            }else{
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            [self.tableV reloadData];
            
            self.tableV.mj_footer.hidden = self.listAllArr.count == _listModel.total?YES:NO;
        }
        if (self.dataArr.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关记录" andBtnTitle:nil andClickBlock:nil];
            }
            [self.tableV addSubview:self.tipView];
            [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(self.tableV);
                make.size.equalTo(CGSizeMake(120, 120));
            }];
            self.tableV.mj_header.hidden = YES;
            self.tableV.mj_footer.hidden = YES;
        }else
        {
            if (self.tipView.superview) {
                
                [self.tipView removeFromSuperview];
            }
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
