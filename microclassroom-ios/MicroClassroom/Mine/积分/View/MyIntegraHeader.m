//
//  MyIntegraHeader.m
//  MicroClassroom
//
//  Created by yfm on 2018/3/2.
//  Copyright © 2018年 Hanks. All rights reserved.
//

#import "MyIntegraHeader.h"

@implementation MyIntegraHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.topVHeight.constant = KTopHeight;
    self.titleTop.constant = KStatusBarHeight / 2.0;
}
- (IBAction)handleBackAction:(UIButton *)sender {
    [self.viewController backBtnAction];
}

@end
