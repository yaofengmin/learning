//
//  MyIntegraHeader.h
//  MicroClassroom
//
//  Created by yfm on 2018/3/2.
//  Copyright © 2018年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyIntegraHeader : UIView
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topVHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTop;
@property (weak, nonatomic) IBOutlet UILabel *totalIntegraLabel;

@end
