//
//  IntegraSectionHeader.h
//  MicroClassroom
//
//  Created by yfm on 2018/3/2.
//  Copyright © 2018年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntegraSectionHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *topLine;

@end
