//
//  IntegralRecordTableViewCell.m
//  yanshan
//
//  Created by fm on 2017/8/20.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "IntegralRecordTableViewCell.h"

@implementation IntegralRecordTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setModel:(MyIntegrateRecordModel *)model//integrateType 1.视频 2音频 3图书
{
    self.typeTitle.text = model.note;
    self.record.text = [NSString stringWithFormat:@"+%@",model.integrateAmount];
//    [self.typeImage sd_setImageWithURL:[NSURL URLWithString:model.integrateTypeIcon]];
}

@end
