//
//  IntegralRecordTableViewCell.h
//  yanshan
//
//  Created by fm on 2017/8/20.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyIntegrateRecordModel.h"

@interface IntegralRecordTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeTitle;
@property (weak, nonatomic) IBOutlet UILabel *record;
//@property (weak, nonatomic) IBOutlet UIImageView *typeImage;
@property (nonatomic,strong) MyIntegrateRecordModel *model;
@end
