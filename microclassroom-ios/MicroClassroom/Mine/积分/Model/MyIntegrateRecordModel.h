//
//  MyIntegrateRecordModel.h
//  yanshan
//
//  Created by fm on 2017/9/8.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseModel.h"

@interface MyIntegrateRecordModel : YHBaseModel
@property (nonatomic,assign) int integrateType;
@property (nonatomic,copy) NSString *integrateAmount;
@property (nonatomic,copy) NSString *note;
@property (nonatomic,copy) NSString *integrateTypeIcon;
@end


@interface MyIntergrateListModel : YHBaseModel
@property (nonatomic,copy) NSString *createTime;
@property (nonatomic,strong) NSArray <MyIntegrateRecordModel *>*ListRecord;
@end


@interface MyIntegrateModel : YHBaseModel
@property (nonatomic,assign) int totalIntegrate;
@property (nonatomic,assign) int total;
@property (nonatomic,strong) NSArray <MyIntergrateListModel *>*list;


@end



