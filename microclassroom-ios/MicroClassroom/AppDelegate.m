//
//  AppDelegate.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+thirdPathInit.h"
#import "AppDelegate+EaseMob.h"
#import "YhjRootViewController.h"
#import "YHNavgationController.h"
#import "LoginViewController.h"
#import "ChatViewController.h"
#import "InitConfigModel.h"
#import "IAPTool.h"
#import "HTPlayer.h"
#import "HTAudioPlayer.h"
#import "EAintroViewController.h"
#import "SecondLaunchImageViewController.h"


static NSString * const kEMSDKAppKey = @"91330108ma27wm1u3r#go2study8xingye";
static NSString *kConversationChatter = @"ConversationChatter";
static NSString *kMessageType = @"MessageType";
static NSString *kMessageExt = @"MessageExt";

@interface AppDelegate ()
<
EMClientDelegate,
EMChatManagerDelegate
>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
//    [self aFNetworkStatus];
    [self easemobApplication:application
didFinishLaunchingWithOptions:launchOptions
                      appkey:kEMSDKAppKey
                apnsCertName:ApnsCertName
                 otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES]}];
    
    [self setUMSDK];
    [self homeRequestWithFinishBlock:nil];
    [self registerForRemoteNotification];
    [self setKeyBoardManager];
    [self getInitConfig];
    [self setUpRootViewController];
    [self customLaunchImageView];
    [self setKeyValueWithLabelHave];

    return YES;
}

/**
 *   获取系统配置信息
 */
-(void)getInitConfig{
    NSString *appVersion = APPversion;
    NSDictionary *dic = @{Service:GetInitConfig,@"version":appVersion};
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:dic andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                [[InitConfigModel shareInstance] mj_setKeyValues:result[REQUEST_LIST]];
//                [[IAPTool sharedIAPTool] initProducts];
                KPostNotifiCation(KLOGINVIEWISSHOWTOURISTS, nil);
            }
        }
    }];
}

-(void)setUpRootViewController {
    
    NSString *userid = KgetUserValueByParaName(USERID);
    YHNavgationController *rootNav = nil;
    if ([YHJHelp isFirstLoad]) {
        EAintroViewController *introPage = [[EAintroViewController alloc]init];
        rootNav = [[YHNavgationController alloc]initWithRootViewController:introPage];
        self.window.rootViewController  = rootNav;
    }else{
        if (IsEmptyStr(userid)) {
            LoginViewController *login = [[LoginViewController alloc]init];
            rootNav = [[YHNavgationController alloc]initWithRootViewController:login];
            rootNav.modalPresentationStyle = 0;
            self.window.rootViewController  = rootNav;
        }else {
            [self loginEMSDK];
            YhjRootViewController  *rootVc = [[YhjRootViewController alloc]init];
            rootNav.modalPresentationStyle = 0;
            self.window.rootViewController  = rootVc;
        }
    }
    [self.window makeKeyAndVisible];
}

-(void)setLauchImage
{
    YHNavgationController *rootNav = nil;
    NSDictionary *infoDic = KgetUserValueByParaName(HCmyDyItems);
    if ([[infoDic objectForSafeKey:@"brandInfo"] isKindOfClass:[NSDictionary class]]) {
        SecondLaunchImageViewController  *lauchImage = [[SecondLaunchImageViewController alloc]init];
        rootNav = [[YHNavgationController alloc]initWithRootViewController:lauchImage];
    }else{
        [self loginEMSDK];
        YhjRootViewController  *rootVc = [[YhjRootViewController alloc]init];
        rootNav = [[YHNavgationController alloc]initWithRootViewController:rootVc];
    }
    self.window.rootViewController  = rootNav;
    
}
//#pragma mark ===收到的环信消息 ==


/*!
 *  \~chinese
 *  收到消息
 *
 *  @param aMessages  消息列表<EMMessage>
 *
 *  \~english
 *  Received messages
 *
 *  @param aMessages  Message list<EMMessage>
 */
- (void)didReceiveMessages:(NSArray *)aMessages {
    
    switch ([UIApplication sharedApplication].applicationState) {
            
        case UIApplicationStateBackground:
        {
            
            NSString *msgText = [self getMessageStr:aMessages[0]];
            
            //发送本地推送
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = [NSDate date]; //触发通知的时间
            notification.alertBody = msgText;
            notification.alertAction = NSLocalizedString(@"open", @"Open");
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.soundName = UILocalNotificationDefaultSoundName;
            
            //发送通知
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            [UIApplication sharedApplication].applicationIconBadgeNumber += 1;
            
            
        }
            break;
        case UIApplicationStateActive:
        {
            
            KPostNotifiCation(NewMsgNotification, @"");
            
        }
            break;
        default:
            break;
    }
    
}

- (void)didReceiveCmdMessages:(NSArray *)aCmdMessages {
    
    //发送本地推送
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate date]; //触发通知的时间
    
    notification.alertBody = [self getMessageStr:aCmdMessages[0]];;
    notification.alertAction = NSLocalizedString(@"open", @"Open");
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    //发送通知
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    [UIApplication sharedApplication].applicationIconBadgeNumber += 1;
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    //开启后台处理多媒体事件
//    _bgTaskId=[AppDelegate backgroundPlayerID:_bgTaskId];
  
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[EMClient sharedClient] applicationDidEnterBackground:application];

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[EMClient sharedClient] applicationWillEnterForeground:application];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    KPostNotifiCation(NewMsgNotification, @"");
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)loginEMSDK {
    [YHJHelp logHuanXinAcount];
}

- (void)logoutEMSDK {
    
    EMError *error = [[EMClient sharedClient] logout:YES];
    if (!error) {
        NSLog(@"退出成功");
    }
    
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    [WXApi handleOpenURL:url delegate:self];
    
    return YES;
}

//独立客户端回调函数
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [WXApi handleOpenURL:url delegate:self];
}

#ifdef IOS9

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    
    [WXApi handleOpenURL:url delegate:self];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable restorableObjects))restorationHandler {
    return [WXApi handleOpenUniversalLink:userActivity delegate:self];
}

#endif

#pragma mark === 个人信息
-(void)homeRequestWithFinishBlock:(UserInfoFinishBlock) getInitfinishBlock
{
    _userInitBlock = [getInitfinishBlock copy];
    if (NOEmptyStr(KgetUserValueByParaName(USERID))) {
        dispatch_async(self.homeVCQueue, ^{
            [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{Service:GetBaseInfo,@"userId":KgetUserValueByParaName(USERID)} andBlocks:^(NSDictionary *result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    UserInfoModel * user = [UserInfoModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                    [UserInfoModel saveUserInfoModelWithModel:user];
                    NSDictionary *listDic = [result objectForSafeKey:REQUEST_INFO];
                    KsetUserValueByParaName(listDic, HCmyDyItems);
                    if ([[result objectForSafeKey:REQUEST_INFO] isKindOfClass:[NSDictionary class]]) {
                        //特殊学员(brand_id>0)个人信息中的资源 需求 来往城市 行业 不显示；链城小站不显示
                        KsetUserValueByParaName([listDic objectForSafeKey:@"brandId"], KBrandInfoId);
                    }
                    KPostNotifiCation(NOTIGICATIONFINDFITERBUTTON, nil);
                }
                if (_userInitBlock) {
                    _userInitBlock();
                }
            }];
        });
    }
}
- (dispatch_queue_t)homeVCQueue {
    if (!_homeVCQueue) {
        _homeVCQueue = dispatch_queue_create("homeVCQueue", NULL);
    }
    return _homeVCQueue;
}


@end
