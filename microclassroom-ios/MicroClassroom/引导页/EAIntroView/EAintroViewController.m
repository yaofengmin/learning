//
//  EAintroViewController.m
//  MicroClassroom
//
//  Created by yfm on 2017/12/11.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "EAintroViewController.h"
#import "EAIntroView.h"
#import "YHNavgationController.h"
#import "LoginViewController.h"
#import "SecondLaunchImageViewController.h"
@interface EAintroViewController ()<EAIntroDelegate>

@end

@implementation EAintroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showIntroWithCrossDissolve];
}

- (void)showIntroWithCrossDissolve
{
    /**
     聊天这个版本清空登录状态
     */
    NSString *pic1;
    NSString *pic2 ;
    NSString *pic3;
    NSString *pic4;
    NSString *pic5;
    NSString *pic6;
    
    //    if (IPHONE3_5INCH) {
    //
    //        pic2 = @"l44.gif";
    //        pic3 = @"l33.gif";
    //        pic4 = @"l22.gif";
    //        pic1 = @"l11.gif";
    //
    //    }else
    //    {
    pic1 = @"1.jpg";
    pic2 = @"2.jpg";
    pic3 = @"3.jpg";
    pic4 = @"4.jpg";
    pic5 = @"5.jpg";
//    pic6 = @"6.jpg";
    
    //    }
    
    EAIntroPage* page1 = [EAIntroPage page];
    page1.title = @"";
    page1.desc = @"";
    page1.titleImage = [UIImage imageNamed:pic1];
    
    EAIntroPage* page2 = [EAIntroPage page];
    page2.title = @"";
    page2.desc = @"";
    page2.titleImage = [UIImage imageNamed:pic2];
    
    EAIntroPage* page3 = [EAIntroPage page];
    page3.title = @"";
    page3.desc = @"";
    page3.titleImage = [UIImage imageNamed:pic3];
    
    EAIntroPage* page4 = [EAIntroPage page];
    page4.title = @"";
    page4.desc = @"";
    page4.titleImage = [UIImage imageNamed:pic4];
    
    EAIntroPage* page5 = [EAIntroPage page];
    page5.title = @"";
    page5.desc = @"";
    page5.titleImage = [UIImage imageNamed:pic5];
    
//    EAIntroPage* page6 = [EAIntroPage page];
//    page6.title = @"";
//    page6.desc = @"";
//    page6.titleImage = [UIImage imageNamed:pic6];
    EAIntroView* intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2, page3 ,page4,page5]];
    intro.pageControl.hidden = YES;
    intro.delegate = self;
    [intro showInView:self.view animateDuration:0.0];
    
}

#pragma mark === EAIntroDelegate
-(void)introDidFinish
{
    NSString *userid = KgetUserValueByParaName(USERID);
    YHNavgationController *rootNav = nil;
    if (IsEmptyStr(userid)) {
        LoginViewController *login = [[LoginViewController alloc]init];
        rootNav = [[YHNavgationController alloc]initWithRootViewController:login];
        rootNav.modalPresentationStyle = 0;
    }else {
        NSDictionary *infoDic = KgetUserValueByParaName(HCmyDyItems);
        if ([[infoDic objectForSafeKey:@"brandInfo"] isKindOfClass:[NSDictionary class]]) {
            SecondLaunchImageViewController  *lauchImage = [[SecondLaunchImageViewController alloc]init];
            rootNav = [[YHNavgationController alloc]initWithRootViewController:lauchImage];
        }else{
            [YHJHelp logHuanXinAcount];
            YhjRootViewController  *rootVc = [[YhjRootViewController alloc]init];
            rootNav = [[YHNavgationController alloc]initWithRootViewController:rootVc];
            rootNav.modalPresentationStyle = 0;
        }
    }
    [UIApplication sharedApplication].keyWindow.rootViewController = rootNav;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
