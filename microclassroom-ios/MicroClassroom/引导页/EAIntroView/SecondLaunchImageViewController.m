//
//  SecondLaunchImageViewController.m
//  MicroClassroom
//
//  Created by yfm on 2017/12/27.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "SecondLaunchImageViewController.h"
#import "YHNavgationController.h"

@interface SecondLaunchImageViewController ()
@property (nonatomic ,strong) UIImageView *bgImage;
@end

@implementation SecondLaunchImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setUpRootViewController];
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self showLauchImage];
}
-(void)showLauchImage
{
    _bgImage = [[UIImageView alloc]init];
//    _bgImage.contentMode = UIViewContentModeScaleAspectFit;
    NSDictionary *infoDic = KgetUserValueByParaName(HCmyDyItems);
    if ([[infoDic objectForSafeKey:@"brandInfo"] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *infoModel = [infoDic objectForSafeKey:@"brandInfo"];
        [_bgImage sd_setImageWithURL:[infoModel objectForSafeKey:@"logo"]];
    }
    [self.view addSubview:_bgImage];
    [_bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

-(void)setUpRootViewController
{
    YHNavgationController *rootNav = nil;
    [YHJHelp logHuanXinAcount];
    YhjRootViewController  *rootVc = [[YhjRootViewController alloc]init];
    rootNav = [[YHNavgationController alloc]initWithRootViewController:rootVc];
    [UIApplication sharedApplication].keyWindow.rootViewController = rootNav;
    [self removeFromParentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
