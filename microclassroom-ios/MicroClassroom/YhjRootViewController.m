//
//  YhjRootViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YhjRootViewController.h"
#import "RDVTabBarItem.h"
#import "YHNavgationController.h"
#import "MainMicroClassViewController.h"
#import "WeViewController.h"
#import "FindViewController.h"
#import "LiveRadioViewController.h"
#import "MineViewController.h"
#import "ChatDemoHelper.h"
#import "LoginViewController.h"
#import "UserInfoModel.h"

#import "MyHomeViewController.h"
#import "LiveRadioHomeViewController.h"

@interface YhjRootViewController ()<RDVTabBarControllerDelegate>

{
    __block NSInteger _currentIndex;
    BOOL shouldAutorotate;
}


@property (nonatomic, assign) NSInteger currentIndex;


@end

@implementation YhjRootViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    //    设置委托
    id<RDVTabBarControllerDelegate> __unsafe_unretained delegate = self;
    self.delegate = delegate;
    [self setupViewControllers];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(autorotateInterface:) name:@"InterfaceOrientationNotification" object:nil];
    KAddobserverNotifiCation(@selector(setMsgBadge:), NewMsgNotification);
    KAddobserverNotifiCation(@selector(loginStateChange:), KNOTIFICATION_LOGINCHANGE);
    
    [self setMsgBadge:nil];
}


-(void)setupViewControllers
{
    /**
     微课
     */
    MainMicroClassViewController *microClass = [[MainMicroClassViewController alloc]init];
    microClass.isRootVC = YES;
    YHNavgationController *microClassNav  = [[YHNavgationController alloc]initWithRootViewController:microClass];
    
    /**
     *  WE+
     */
    
    WeViewController *we = [[WeViewController alloc]init];
    we.isRootVC = YES;
    YHNavgationController *weNav = [[YHNavgationController alloc]initWithRootViewController:we];
    
    
    /**
     * 发现
     */
    
    FindViewController *find = [[FindViewController alloc]init];
    find.isRootVC  = YES;
    YHNavgationController *findNav = [[YHNavgationController alloc]initWithRootViewController:find];
    
    /**
     * 直播
     */
    
    //    LiveRadioViewController *liveRadio = [[LiveRadioViewController alloc]init];
    LiveRadioHomeViewController *liveRadio = [[LiveRadioHomeViewController alloc]init];
    liveRadio.isRootVC  = YES;
    YHNavgationController *liveRadioNav = [[YHNavgationController alloc]initWithRootViewController:liveRadio];
    
    /**
     *  我的
     */
    
    //    MineViewController *mine = [[MineViewController alloc]init];
    MyHomeViewController *mine = [[MyHomeViewController alloc]init];
    mine.isRootVC = YES;
    mine.root = self;
    YHNavgationController *mineNav = [[YHNavgationController alloc]initWithRootViewController:mine];
    
//    [self setViewControllers:@[microClassNav,weNav,findNav,mineNav]];
    if ([InitConfigModel shareInstance].isShowCircle) {
        [self setViewControllers:@[microClassNav,weNav,findNav,mineNav]];
    }else{
        [self setViewControllers:@[microClassNav,findNav,mineNav]];
    }
    [self configTableBarController];
    
    [ChatDemoHelper shareHelper].mineVC = mine;
    [ChatDemoHelper shareHelper].mainVC = self;
    self.selectedIndex = 0;
}

-(void)configTableBarController
{
    
    int index = 0;
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] =KColorRGB(123, 123, 123);
    textAttrs[NSFontAttributeName] = Font(13);
    NSMutableDictionary *selectTextAttrs = [NSMutableDictionary dictionary];
    selectTextAttrs[NSForegroundColorAttributeName] = kNavigationBarColor;
    selectTextAttrs[NSFontAttributeName] = Font(13);
    NSMutableArray *titleArr = [NSMutableArray arrayWithArray:@[@"微课",@"共享",@"我的"]];
    NSMutableArray *disImageArr = [NSMutableArray arrayWithArray:@[@"tab_weike_n",@"tab_discover_n",@"tab_me_n"]];
    NSMutableArray *imageArr = [NSMutableArray arrayWithArray:@[@"tab_weike_nsel",@"tab_discover_nselected",@"tab_me_nselected"]];
    if ([InitConfigModel shareInstance].isShowCircle) {
        [titleArr insertObject:@"发现" atIndex:1];
        [disImageArr insertObject:@"tab_w+_n" atIndex:1];
        [imageArr insertObject:@"tab_w+_nselected" atIndex:1];
    }
    
    for (RDVTabBarItem *item in [[self tabBar] items]) {
        item.unselectedTitleAttributes = textAttrs;
        item.selectedTitleAttributes = selectTextAttrs;
        item.title = titleArr[index];
        UIImage *selectedimage = [UIImage imageNamed:imageArr[index]];
        UIImage *unselectedimage = [UIImage imageNamed:disImageArr[index]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        
        index++;
    }
    
}

- (void)setMsgBadge:(NSNotification *)sender {
    
    RDVTabBarItem * me = [[[self tabBar] items] objectAtIndex:self.viewControllers.count - 1];
    
    __block int unreadCount = 0;
    
    NSArray * conversationArr = [[EMClient sharedClient].chatManager getAllConversations];
    
    [conversationArr enumerateObjectsUsingBlock:^(EMConversation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        unreadCount += obj.unreadMessagesCount;
        
    }];
    
    NSString * newCount;
    
    if (unreadCount == 0) {
        newCount = @"";
    } else {
        newCount = [NSString stringWithFormat:@"%d", unreadCount];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [me setBadgeValue:newCount];
    });
}

#pragma mark --RDVTabBarDelegate
- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index {
    
    if (self.selectedIndex ==index) {
        return NO;
    }
    
    _currentIndex = index;
    
    return YES;
}


// 统计未读消息数
-(void)setupUnreadMessageCount
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    
    if(unreadCount>0){
        RDVTabBarItem *tabBarItem =self.tabBar.items[self.viewControllers.count - 1];
        if (unreadCount > 99) {
            tabBarItem.badgeValue=@"99+";
        }else{
            tabBarItem.badgeValue=[NSString stringWithFormat:@"%ld",(long)unreadCount];
        }
    }else{
        RDVTabBarItem *tabBarItem =self.tabBar.items[self.viewControllers.count - 1];
        tabBarItem.badgeValue=nil;
    }
    
    
}


#pragma mark - 退出登录
- (void)loginStateChange:(NSNotification *)notification {
    BOOL loginSuccess = [notification.object boolValue]; //注销环信
    if (!loginSuccess) {
        [UserInfoModel clearModel];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"对不起，您的登录信息已过期" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
        [[EMClient sharedClient] logout:YES completion:nil];
        LoginViewController *log = [[LoginViewController alloc]init];
        YHNavgationController *logNav = [[YHNavgationController alloc]initWithRootViewController:log];
        logNav.modalPresentationStyle = 0;
        [self presentViewController:logNav animated:YES completion:nil];
        
    }
}



-(void)autorotateInterface:(NSNotification *)notifition
{
    shouldAutorotate = [notifition.object boolValue];
}

/**
 *  适配旋转的类型
 *
 *  @return 类型
 */
-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if (!shouldAutorotate) {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskAllButUpsideDown;
}
@end
