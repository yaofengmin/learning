//
//  AddVoteCollectionViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/31.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "AddVoteCollectionViewCell.h"

@implementation AddVoteCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.addBtn.layer.cornerRadius = 4.0;
	self.addBtn.layer.masksToBounds = YES;
	self.addBtn.layer.borderWidth = 1;
	self.addBtn.layer.borderColor = KColorFromRGB(0xfbbcbf).CGColor;
}
- (IBAction)addAction:(UIButton *)sender {
	!_ACMediaClickAddButton ?  : _ACMediaClickAddButton();

}

@end
