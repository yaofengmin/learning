//
//  SendVoteInputView.h
//  yanshan
//
//  Created by fm on 2017/8/30.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SendVoteManager.h"
@class SendVoteInputView;

@protocol SendVoteDelegate <NSObject>

@optional
- (void)InputView:(SendVoteInputView *)photoView updateFrame:(CGRect)frame;


@end
@interface SendVoteInputView : UIView
/**
 * 最大资源选择个数,（包括 preShowMedias 预先展示数据）. default is 9
 */
@property (nonatomic, assign) NSInteger maxItemSelected;

/**
 * 是否显示删除按钮. Defaults is YES
 */
@property (nonatomic, assign) BOOL showDelete;

/**
 * 是否需要显示添加按钮. Defaults is YES
 */
@property (nonatomic, assign) BOOL showAddButton;

/**
 * 底部collectionView的 backgroundColor
 */
@property (nonatomic, strong) UIColor *backgroundColor;

typedef void(^ACMediaHeightBlock)(CGFloat mediaHeight);


@property (nonatomic, copy) void(^fileBlock)(NSArray<SendVoteManager *> *list);

@property (nonatomic,weak) id<SendVoteDelegate> delegate;


#pragma mark - methods

/**
 * 监控view的高度变化，如果不和其他控件一起使用，则可以不用监控高度变化
 */
- (void)observeViewHeight: (ACMediaHeightBlock)value;


/**
 * 视图一开始默认高度
 */
+ (CGFloat)defaultViewHeight;

/**
 * 刷新
 */
- (void)reload;
@end
