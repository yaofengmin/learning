//
//  EnclosureDetailTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/31.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnclosureDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *fileTymeImage;
@property (weak, nonatomic) IBOutlet UILabel *fileType;
@property (weak, nonatomic) IBOutlet UILabel *kbLabel;
@property (weak, nonatomic) IBOutlet UIButton *beginDowload;
@property (weak, nonatomic) IBOutlet UIView *bottomN;

@end
