//
//  SendVoteInputView.m
//  yanshan
//
//  Created by fm on 2017/8/30.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "SendVoteInputView.h"

#import "SendVoteCollectionViewCell.h"
#import "AddVoteCollectionViewCell.h"

#define fileNum 8
@interface SendVoteInputView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, copy) ACMediaHeightBlock block;

/** 总的媒体数组 */
@property (nonatomic, strong) NSMutableArray *mediaArray;
@end
@implementation SendVoteInputView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _setup];
    }
    return self;
}
- (void)_setup {
    _mediaArray = [NSMutableArray array];
    _showDelete = YES;
    _showAddButton = YES;
    _maxItemSelected = 9;
    _backgroundColor = [UIColor whiteColor];
    [self configureCollectionView];
}

- (void)configureCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(self.frame.size.width, 40);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(10, 0, 10, 0);
    _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
    [_collectionView registerNib:[UINib nibWithNibName:@"SendVoteCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SendVoteCollectionCellID"];
    [_collectionView registerNib:[UINib nibWithNibName:@"AddVoteCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"AddVoteCellID"];
    _collectionView.scrollEnabled = NO;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = _backgroundColor;
    [self addSubview:_collectionView];
}
#pragma mark - setter

- (void)setShowDelete:(BOOL)showDelete {
    _showDelete = showDelete;
}

- (void)setShowAddButton:(BOOL)showAddButton {
    _showAddButton = showAddButton;
    if (_mediaArray.count > 3 || _mediaArray.count == 0) {
        [self layoutCollection];
    }
}
- (void)setBackgroundColor:(UIColor *)backgroundColor {
    _backgroundColor = backgroundColor;
    [_collectionView setBackgroundColor:backgroundColor];
}
#pragma mark - 布局

///重新布局collectionview
- (void)layoutCollection {
    
    NSInteger allImageCount = _showAddButton ? _mediaArray.count + 1 : _mediaArray.count;
    NSInteger maxRow = allImageCount > self.maxItemSelected?self.maxItemSelected:allImageCount;
    _collectionView.viewHeight = allImageCount == 0 ? 0 : maxRow * 40 + allImageCount *10 + 5;
    self.viewHeight = _collectionView.viewHeight;
    [self reload];
    
    if ([self.delegate respondsToSelector:@selector(InputView:updateFrame:)]) {
        [self.delegate InputView:self updateFrame:_collectionView.frame];
    }
}
#pragma mark - public method

- (void)observeViewHeight:(ACMediaHeightBlock)value {
    _block = value;
    //预防先加载数据源的情况
    _block(_collectionView.viewHeight);
}



+ (CGFloat)defaultViewHeight {
    return 50;
}

- (void)reload {
    [self.collectionView reloadData];
}

#pragma mark -  Collection View DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger num = self.mediaArray.count < _maxItemSelected ? self.mediaArray.count : _maxItemSelected;
    if (num == _maxItemSelected) {
        return _maxItemSelected;
    }
    return _showAddButton ? num + 1 : num;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _mediaArray.count) {
        
        AddVoteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AddVoteCellID" forIndexPath:indexPath];
        [cell setACMediaClickAddButton:^{
            [self.viewController.view endEditing:YES];
            SendVoteManager *manager = [[SendVoteManager alloc]init];
            manager.fileName = @"";
            [self.mediaArray addObject:manager];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self layoutCollection];
            });
            self.fileBlock(_mediaArray);
        }];
        [cell.addBtn setTitle:[NSString stringWithFormat:@"+增加选项(%ld/8)",self.mediaArray.count] forState:UIControlStateNormal];
        return cell;
    }else{
        
        SendVoteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SendVoteCollectionCellID" forIndexPath:indexPath];
        cell.indexPath = indexPath;
        cell.model = _mediaArray[indexPath.row];
        cell.deleteBtn.hidden = !_showDelete;
        [cell setACMediaClickDeleteButton:^{
            
            [_mediaArray removeObjectAtIndex:indexPath.row];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self layoutCollection];
            });
            self.fileBlock(_mediaArray);
        }];
        
        cell.inputTextField = ^(NSString *text, NSIndexPath *indexPath) {
            SendVoteManager *manager = _mediaArray[indexPath.row];
            manager.fileName = text;
            self.fileBlock(_mediaArray);
        };
        
        cell.chooseImage = ^(UIImage *image, NSIndexPath *indexPath) {
            SendVoteManager *manager = _mediaArray[indexPath.row];
            manager.image = image;
            self.fileBlock(_mediaArray);
        };
        return cell;
    }
}

@end
