//
//  SendVoteCollectionViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/31.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SendVoteManager.h"

@interface SendVoteCollectionViewCell : UICollectionViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *inputTf;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (nonatomic,strong) NSIndexPath *indexPath;

@property (weak, nonatomic) IBOutlet UIButton *imageV;

/** 点击删除按钮的回调block */
@property (nonatomic, copy) void(^ACMediaClickDeleteButton)();

@property (nonatomic, copy) void(^inputTextField)(NSString *text,NSIndexPath *indexPath);
@property (nonatomic, copy) void(^chooseImage)(UIImage *image,NSIndexPath *indexPath);


@property (nonatomic,strong) SendVoteManager *model;

@end
