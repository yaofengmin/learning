//
//  EnclosureDetailTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/31.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "EnclosureDetailTableViewCell.h"

@implementation EnclosureDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.beginDowload.layer.borderWidth = 1.0;
	self.beginDowload.layer.borderColor = DColor.CGColor;
	self.beginDowload.layer.cornerRadius = 2.0;
	self.beginDowload.layer.masksToBounds = YES;
	self.bottomN.layer.cornerRadius = 4.0;
	self.bottomN.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)dowloadAction:(UIButton *)sender {
}

@end
