//
//  SendVoteAddTableViewCell.m
//  yanshan
//
//  Created by fm on 2017/8/30.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "SendVoteAddTableViewCell.h"

#import "SendVoteInputView.h"

#import "DateTimePickerView.h"

@interface SendVoteAddTableViewCell ()<SendVoteDelegate,DateTimePickerViewDelegate>
@property (nonatomic,strong) UILabel *dateLabel;
@property (nonatomic,strong) UIButton *chooseDateBtn;
@property (nonatomic, strong) DateTimePickerView *datePickerView;

@end

@implementation SendVoteAddTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self creatSub];
    }
    return self;
}

-(void)creatSub
{
    CGFloat height = [SendVoteInputView defaultViewHeight];
    SendVoteInputView *mediaView = [[SendVoteInputView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, height)];
    mediaView.maxItemSelected = 8;
    mediaView.delegate = self;
    mediaView.showDelete = YES;
    mediaView.fileBlock = ^(NSArray<SendVoteManager *> *list) {
        self.listModel(list);
    };
    [self.contentView addSubview:mediaView];
    [mediaView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(70);
    }];
    
    self.dateLabel = [UILabel labelWithText:@"截止日期 :" andFont:14 andTextColor:JColor andTextAlignment:NSTextAlignmentCenter];
    [self.contentView addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset(25);
        make.size.mas_equalTo(CGSizeMake(65, 30));
        make.bottom.mas_equalTo(-10);
    }];
    
    self.chooseDateBtn = [DIYButton buttonWithImage:nil andTitel:@"选择日期" andBackColor:NCColor];
    [self.chooseDateBtn setTitleColor:JColor forState:UIControlStateNormal];
    self.chooseDateBtn.titleLabel.font = Font(12);
    [self.chooseDateBtn addTarget:self action:@selector(chooseDate:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.chooseDateBtn];
    [self.chooseDateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.dateLabel.mas_right).offset(8);
        make.centerY.mas_equalTo(self.dateLabel);
        make.right.mas_equalTo(-59);
        make.height.mas_equalTo(35);
    }];
    
}

-(void)InputView:(SendVoteInputView *)photoView updateFrame:(CGRect)frame
{
    _currentModel.fileHeight = frame.size.height;
    [self.tableV beginUpdates];
    [self.tableV endUpdates];
}

-(void)chooseDate:(UIButton *)sender
{
    [self.viewController.view endEditing:YES];
    DateTimePickerView *pickerView = [[DateTimePickerView alloc] init];
    self.datePickerView = pickerView;
    pickerView.delegate = self;
    pickerView.pickerViewMode = DatePickerViewDateTimeMode;
    [self.viewController.view addSubview:pickerView];
    [pickerView showDateTimePickerView];
    
}
#pragma mark - delegate

- (void)didClickFinishDateTimePickerView:(NSString *)date{
    [self.chooseDateBtn setTitle:date forState:UIControlStateNormal];
    self.timeBlock(date);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
