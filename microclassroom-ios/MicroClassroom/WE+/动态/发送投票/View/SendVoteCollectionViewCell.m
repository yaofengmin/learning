//
//  SendVoteCollectionViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/31.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "SendVoteCollectionViewCell.h"

#import "CameraTakeManager.h"

@implementation SendVoteCollectionViewCell

- (void)awakeFromNib {
	[super awakeFromNib];
	[super awakeFromNib];
	self.inputTf.delegate = self;
	self.bottomView.layer.borderWidth = 1.0;
	self.bottomView.layer.borderColor = KColorFromRGB(0xfbbcbf).CGColor;
}
- (IBAction)deleteAction:(UIButton *)sender {
	!_ACMediaClickDeleteButton ?  : _ACMediaClickDeleteButton();
	
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
	self.inputTextField(textField.text, self.indexPath);
}

-(void)setModel:(SendVoteManager *)model
{
	if (model.fileName) {
		self.inputTf.text = model.fileName;
	}else{
		self.inputTf.text = @"";
		self.inputTf.placeholder = @"输入选项内容";
	}
	if (model.image) {
		[self.imageV setImage:model.image forState:UIControlStateNormal];
	}else{
		[self.imageV setImage:[UIImage imageNamed:@"+pic"] forState:UIControlStateNormal];
	}
}

- (IBAction)chooseImage:(UIButton *)sender {
	[self endEditing:YES];
	@weakify(self);
	[[CameraTakeManager sharedInstance] cameraSheetInController:self.viewController handler:^(UIImage *image, NSString *imagePath) {
		@strongify(self);
		[sender setImage:image forState:UIControlStateNormal];
		self.chooseImage(image, self.indexPath);
	} cancelHandler:^{}];
}

@end
