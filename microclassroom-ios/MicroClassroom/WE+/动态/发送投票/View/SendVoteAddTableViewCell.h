//
//  SendVoteAddTableViewCell.h
//  yanshan
//
//  Created by fm on 2017/8/30.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AssignmentModel.h"
#import "SendVoteManager.h"

@interface SendVoteAddTableViewCell : UITableViewCell

@property (nonatomic,assign) UITableView *tableV;

@property (nonatomic,strong) AssignmentModel *currentModel;

@property (nonatomic,copy) void(^listModel)(NSArray<SendVoteManager *> *list);

@property (nonatomic,copy) void(^timeBlock)(NSString *timeDate);

@end
