//
//  SendVoteManager.h
//  yanshan
//
//  Created by BPO on 2017/8/31.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseModel.h"

@interface SendVoteManager : YHBaseModel
@property (nonatomic,copy) NSString *fileName;
@property (nonatomic,strong) UIImage *image;
@property (nonatomic,assign) BOOL state;


//标签
@property (nonatomic,copy) NSString *cateId;
@property (nonatomic,copy) NSString *cateName;

@end
