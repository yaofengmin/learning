//
//  VoteListModel.h
//  yanshan
//
//  Created by BPO on 2017/9/1.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseModel.h"

@interface VoteListModel : YHBaseModel
@property (nonatomic,copy) NSString *qusId;//qusId  是未过期的投票作为是否投票的标准, 0— 是未过期中的未投票, 其他值是投票的那个id,判断是投的哪个票
@property (nonatomic,copy) NSString *question;
@property (nonatomic,copy) NSString *voteCount;
@property (nonatomic,copy) NSString *voteLI;

@end
