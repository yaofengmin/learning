//
//  SendVoteViewController.m
//  yanshan
//
//  Created by fm on 2017/8/29.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "SendVoteViewController.h"
#import "HelpPublishInputTableViewCell.h"

#import "SendVoteAddTableViewCell.h"

#import "AssignmentModel.h"
#import "SendVoteManager.h"

#import "UserInfoModel.h"
#import "ClassModel.h"

static NSString *HelpPublishInputCellID = @"HelpPublishInputCellID";

@interface SendVoteViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,copy) NSString *content;
@property (nonatomic,strong) AssignmentModel *imageModel;

@property (nonatomic,strong) NSArray *listArr;
@property (nonatomic,copy) NSString *endTime;

@property (nonatomic,strong) UITableView *tableV;

@property (nonatomic,assign) GetMessagesType type;

/**
 是否置顶
 */
@property (nonatomic ,assign) BOOL isTop;
/**
 班级id
 */
@property (nonatomic,strong) ClassModel *classmodel;

@end

@implementation SendVoteViewController
-(instancetype)initWithPubilshType:(GetMessagesType)publishType
{
    if (self = [super init]) {
        self.type = publishType;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"发送投票";
    _isTop = NO;
    _imageModel = [[AssignmentModel alloc]init];
    [self initalTable];
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithTitle:@"发送" style:YHBarButtonItemStylePlain handler:^(id send) {
        @strongify(self);
        [self publishVote];
        
    }];
}
-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.backgroundColor = [UIColor clearColor];
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"HelpPublishInputTableViewCell" bundle:nil] forCellReuseIdentifier:HelpPublishInputCellID];
}
#pragma mark === UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  2;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        HelpPublishInputTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HelpPublishInputCellID forIndexPath:indexPath];
        if ([[UserInfoModel getInfoModel].grade isEqualToString:@"99"]) {//超级会员.显示是否置顶按钮
            cell.isTopView.hidden = NO;
        }else{
            cell.isTopView.hidden = YES;
        }
        if (self.type == GetMessagesTypeStudyGroup) {  // 班级
            cell.classBottomV.hidden = NO;
            cell.tipLabel.text = @"输入内容";
        }else{
            cell.classBottomV.hidden = YES;
            cell.tipLabel.text = @"同学,感谢分享,您的智慧在审核通过后将分享给所有人";
        }
        cell.textBlock = ^(NSString *text) {
            self.content = text;
        };
        cell.isTopBlock = ^(BOOL isTop) {
            self.isTop = isTop;
        };
        cell.changeGroupId = ^(ClassModel *model) {
            self.classmodel = model;
        };
        return cell;
    }else if (indexPath.row == 1){
        SendVoteAddTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sendVoteCellID"];
        if (!cell) {
            cell = [[SendVoteAddTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"sendVoteCellID"];
        }
        cell.tableV = self.tableV;
        cell.currentModel = _imageModel;
        @weakify(self);
        cell.listModel = ^(NSArray<SendVoteManager *> *list) {
            @strongify(self);
            self.listArr = list;
        };
        cell.timeBlock = ^(NSString *timeDate) {
            self.endTime = timeDate;
        };
        return cell;
    }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        CGFloat cellH = 180;//正常High
        if (self.type == GetMessagesTypeStudyGroup) {//班级成立,需要显示参加的班级
            cellH = cellH + 50;
        }
        if ([[UserInfoModel getInfoModel].grade isEqualToString:@"99"]) {//超级会员.显示是否置顶按钮
            cellH = cellH + 50;
        }
        return cellH;
    }
    return _imageModel.returnFileHeight;
}
-(void)publishVote
{
    [self.view endEditing:YES];
    NSString *classId = @"0";
    if (_classmodel) {
        classId = self.classmodel.classId;
    }
    NSMutableArray *fileName = [NSMutableArray array];
    NSMutableArray *imageArr = [NSMutableArray array];
    //we+发布 需要至少vip1
    if ([[UserInfoModel getInfoModel].grade integerValue] < 1) {
        @weakify(self);
        [YHJHelp aletWithTitle:@"提示" Message:@"您还不是VIP会员，不能发布" sureTitle:@"立即购买" CancelTitle:@"忽略 " SureBlock:^{
            @strongify(self);
            BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
            [self.navigationController pushViewController:buy animated:YES];
        } andCancelBlock:nil andDelegate:self];
        return;
    }
    if (self.content.length == 0) {
        [WFHudView showMsg:@"请输入发布内容" inView:self.view];
        return;
    }
    if (self.listArr.count == 0) {
        [WFHudView showMsg:@"请添加投票选项" inView:self.view];
        return;
    }
    for (SendVoteManager *model in self.listArr) {
        if (model.fileName.length == 0) {
            [WFHudView showMsg:@"请输入选项名称" inView:self.view];
            return;
        }
        [fileName addObject:model.fileName];
        if (model.image) {
            NSData *imageData = UIImageJPEGRepresentation(model.image, 0.5);
            [imageArr addObject:imageData];
        }else{
            NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"We_option"], 0.5);
            [imageArr addObject:imageData];
            //            [imageArr addObject:@""];
        }
    }if (fileName.count < 2) {
        [WFHudView showMsg:@"投票选项不得少于两项" inView:self.view];
        return;
    }
    if (self.endTime.length == 0) {
        [WFHudView showMsg:@"请选择截止日期" inView:self.view];
        return;
    }
    NSDictionary *paramDic = @{Service:CirclePublishMessage,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"ctype":@"2",
                               @"content":self.content.length == 0?@"":self.content,
                               @"questions":fileName.mj_JSONString,
                               @"endTime":self.endTime,
                               @"classesId":classId,
                               @"ifTop":@"0",
                               };
    SHOWHUD;
    [HTTPManager publishCarCirclWithUrl:MainAPI andPostParameters:paramDic andImageDic:@{@"pics":imageArr} andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                [self backBtnAction];
                if ([_delegate respondsToSelector:@selector(finishPublish)]) {
                    [_delegate finishPublish];
                }
            }else{
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
    }];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 60;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
