//
//  SendVoteViewController.h
//  yanshan
//
//  Created by fm on 2017/8/29.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

@protocol PublishFileDelegate <NSObject>

@required

-(void)finishPublish;

@end

@interface SendVoteViewController : YHBaseViewController
@property (assign ,nonatomic) id <PublishFileDelegate> delegate ;


-(instancetype)initWithPubilshType:(GetMessagesType) publishType;


@end
