//
//  EnclosureDetailViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/31.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "EnclosureDetailViewController.h"

#import "EnclosureDetailTableViewCell.h"

static NSString *EnclosureDetailCellID = @"EnclosureDetailCellID";
@interface EnclosureDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableV;
@end

@implementation EnclosureDetailViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.yh_navigationItem.title = @"附件详情";
	[self initalTable];
	
}
-(void)initalTable
{
	self.tableV = [[UITableView alloc]init];
	self.tableV.backgroundColor = [UIColor clearColor];
	self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
	self.tableV.delegate = self;
	self.tableV.dataSource = self;
	self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
	[self.view addSubview:self.tableV];
	[self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
		make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight + 16, 0, 0, 0));
	}];
	
	[self.tableV registerNib:[UINib nibWithNibName:@"EnclosureDetailTableViewCell" bundle:nil] forCellReuseIdentifier:EnclosureDetailCellID];
}
#pragma mark === UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return  2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	EnclosureDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:EnclosureDetailCellID forIndexPath:indexPath];
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return 160;
}



- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}



@end
