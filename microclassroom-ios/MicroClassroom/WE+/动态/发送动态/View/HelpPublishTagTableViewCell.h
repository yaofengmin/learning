//
//  HelpPublishTagTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/30.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpPublishTagTableViewCell : UITableViewCell
@property (nonatomic,strong) NSArray *topArr;
@property (nonatomic,copy) void(^seletedLabel)(NSArray *seletedArr);
@end
