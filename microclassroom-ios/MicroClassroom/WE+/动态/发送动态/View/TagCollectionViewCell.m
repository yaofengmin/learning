//
//  TagCollectionViewCell.m
//  yanshan
//
//  Created by fm on 2017/9/2.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "TagCollectionViewCell.h"

@implementation TagCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)deleteAction:(UIButton *)sender {
    self.deleteBlock(self.indexPath);
}

@end
