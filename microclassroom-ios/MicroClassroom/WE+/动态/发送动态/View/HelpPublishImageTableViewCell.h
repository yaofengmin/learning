//
//  HelpPublishImageTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/28.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssignmentModel.h"

@protocol uploadImage <NSObject>

-(void)getUpLoadPhotoArr:(NSArray *)photoArr atIndexPath:(NSIndexPath *)indexPath;

@end
@interface HelpPublishImageTableViewCell : UITableViewCell
@property (nonatomic,strong) AssignmentModel *currentModel;
@property (nonatomic,strong) NSIndexPath * indexPath;
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,assign)id<uploadImage> delegate;

@end
