//
//  HelpPublishInputTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/28.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "HelpPublishInputTableViewCell.h"

@implementation HelpPublishInputTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.textView.layer.cornerRadius  = 2.0;
	self.textView.layer.masksToBounds = YES;
    self.classBottomV.delegate = self;
}

-(void)textViewDidChange:(UITextView *)textView
{
	if (textView.text.length == 0) {
		self.tipLabel.hidden = NO;
	}else{
		self.tipLabel.hidden = YES;
	}
    self.textBlock(textView.text);
}
- (IBAction)handleSwitchAction:(UISwitch *)sender {
    self.isTopBlock(sender.isOn);
}

-(void)publicWithClassModel:(ClassModel *)model
{
//    _classModel = model;
    self.changeGroupId(model);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
