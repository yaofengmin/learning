//
//  HelpPublishAddCollectionViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/31.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpPublishAddCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
/** 点击删除按钮的回调block */
@property (nonatomic, copy) void(^ACMediaClickAddLabelButton)();
@end
