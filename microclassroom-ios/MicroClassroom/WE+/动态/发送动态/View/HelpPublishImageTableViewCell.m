//
//  HelpPublishImageTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/28.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "HelpPublishImageTableViewCell.h"

#import "HXPhotoViewController.h"
#import "HXPhotoView.h"

@interface HelpPublishImageTableViewCell ()<HXPhotoViewDelegate>
@property (strong, nonatomic) HXPhotoManager *manager;
@property (nonatomic,strong) HXPhotoView *photoView;
@property (nonatomic,strong) UILabel *titleLabel;

@end
@implementation HelpPublishImageTableViewCell

- (HXPhotoManager *)manager
{
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.openCamera = YES;
        _manager.outerCamera = YES;
        _manager.showFullScreenCamera = NO;
        _manager.lookLivePhoto = NO;
        _manager.photoMaxNum = 8;
    }
    return _manager;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"添加图片";
        _titleLabel.font = Font(14);
        _titleLabel.textColor = DColor;
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(25);
            make.top.mas_equalTo(10);
        }];
        _photoView = [HXPhotoView photoManager:self.manager];
        _photoView.delegate = self;
        _photoView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_photoView];
        [_photoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
            make.width.mas_equalTo(KScreenWidth - 50);
            make.centerX.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView.mas_bottom).mas_offset(-5);
        }];
    }
    return self;
}

- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal
{
    if (allList.count) {
        [HXPhotoTools getImageForSelectedPhoto:photos type:HXPhotoToolsFetchOriginalImageTpe completion:^(NSArray<UIImage *> *images) {
            if ([self.delegate respondsToSelector:@selector(getUpLoadPhotoArr:atIndexPath:)]) {
                [self.delegate getUpLoadPhotoArr:images atIndexPath:self.indexPath];
            }
        }];
    }else{
        if ([self.delegate respondsToSelector:@selector(getUpLoadPhotoArr:atIndexPath:)]) {
            [self.delegate getUpLoadPhotoArr:@[] atIndexPath:_indexPath];
        }
    }
}

- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame{
    if (frame.size.height < (KScreenWidth - 77) / 4.0) {
        _currentModel.photoHeight = (KScreenWidth - 77) / 4.0;
    }else{
        _currentModel.photoHeight = frame.size.height;
    }
    [_tableView beginUpdates];
    [_tableView endUpdates];
}

#pragma mark === 打开相册
-(void)openCamera
{
    [_photoView goPhotoViewController];
}
@end
