//
//  HelpPublishInputTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/28.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassPublishViewBottom.h"

@interface HelpPublishInputTableViewCell : UITableViewCell<UITextViewDelegate,ClassPublishViewBottomDeleage>
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;

/**
 内容
 */
@property (nonatomic,copy) void (^textBlock) (NSString *text);
@property (weak, nonatomic) IBOutlet UIView *isTopView;
@property (weak, nonatomic) IBOutlet UISwitch *isTopSwitch;

/**
 是否置顶
 */
@property (nonatomic,copy) void (^isTopBlock) (BOOL isTop);

@property (weak, nonatomic) IBOutlet ClassPublishViewBottom *classBottomV;

/**
 班级修改
 */
@property (nonatomic,copy) void (^changeGroupId) (ClassModel *model);

@end
