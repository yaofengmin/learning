//
//  HelpPublishTagTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/30.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "HelpPublishTagTableViewCell.h"

#import "TagCollectionViewCell.h"
#import "SQActionSheetView.h"

#import "SeacherTipListModel.h"

#define itemLine 10

static NSString *tagCollectionID = @"TagCollectionCellID";
@interface HelpPublishTagTableViewCell ()<UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UICollectionViewFlowLayout *flowLayout;

@property (nonatomic,strong) UICollectionView *collectionV;

@property (nonatomic,strong)NSMutableArray *addContenArr;

@property (nonatomic,assign)NSInteger i;

@property (nonatomic,assign)CGSize size;

@property (nonatomic,strong) NSArray <SeacherItemModel *>*tipArr;

@end
@implementation HelpPublishTagTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _addContenArr = [NSMutableArray array];
        
        [self creatSub];
    }
    return self;
}

-(void)creatSub
{
    UILabel *tipLabel = [[UILabel alloc]init];
    tipLabel.textColor = DColor;
    tipLabel.text = @"添加标签";
    tipLabel.font = Font(14);
    [self.contentView addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(25);
    }];
    
    
    _flowLayout = [[UICollectionViewFlowLayout alloc]init];
    _flowLayout.minimumLineSpacing = itemLine;
    _flowLayout.minimumInteritemSpacing = itemLine;
    _collectionV = [[UICollectionView alloc]initWithFrame:CGRectMake(25, 30, KScreenWidth - 36 - 15, 70) collectionViewLayout:_flowLayout];
    _collectionV.scrollEnabled = NO;
    _collectionV.delegate = self;
    _collectionV.dataSource = self;
    
    _collectionV.backgroundColor = [UIColor clearColor];
    
    [_collectionV registerNib:[UINib nibWithNibName:@"TagCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:tagCollectionID];
    
    
    [self.contentView addSubview:_collectionV];
    
    
}
#pragma mark === UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.addContenArr.count + 1;
    
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    TagCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:tagCollectionID forIndexPath:indexPath];
    cell.indexPath = indexPath;
    if (indexPath.item == self.addContenArr.count) {
        cell.deleteBtn.hidden = YES;
        [cell.label setBackgroundImage:[UIImage imageNamed:@"add_label"] forState:UIControlStateNormal];
        [cell.label setTitle:@"" forState:UIControlStateNormal];
        cell.labelRight.constant = 0;
    }else{
        if (self.addContenArr.count) {
            SeacherItemModel *model = self.addContenArr[indexPath.item];
            cell.deleteBtn.hidden = NO;
            [cell.label setBackgroundImage:[UIImage imageNamed:@"lable_Sel"] forState:UIControlStateNormal];
            [cell.label setTitle:model.codeName forState:UIControlStateNormal];
            cell.labelRight.constant = 25;
        }
    }
    cell.deleteBlock = ^(NSIndexPath *indexPath) {
        [self.addContenArr removeObjectAtIndex:indexPath.item];
        [self.collectionV reloadData];
    };
    return cell;
}

#pragma mark === UICollectionViewDelegateFlowLayout
/**
 每个cell 的大小，随着字体大小改变width
 */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.item == _addContenArr.count) {
        return CGSizeMake(65,20);
    }else{
        if (self.addContenArr.count) {
            SeacherItemModel *model = self.addContenArr[indexPath.item];
            _size = [model.codeName sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]}];
            return CGSizeMake(_size.width + 45,20);
        }
    }
    return CGSizeZero;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    
    return UIEdgeInsetsMake(itemLine, 0, itemLine, itemLine);
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == _addContenArr.count) {
        [self.viewController.view endEditing:YES];
        [self requestTips];
    }
}

#pragma mark === 标签数据源
-(void)requestTips {
    NSDictionary *paramDic = @{Service:AppGetCodeList,
                               @"ctype":@(102)};
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _tipArr = [SeacherItemModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                SQActionSheetView *sheetView = [[SQActionSheetView alloc]initWithTitle:@"" data:_tipArr addContentArr:self.addContenArr buttonClick:^(SQActionSheetView *sheetView, NSMutableArray *addContentArr) {
                    if (self.addContenArr.count) {
                        [self.addContenArr removeAllObjects];
                    }
                    [self.addContenArr addObjectsFromArray:addContentArr];
                    [self.collectionV reloadData];
                    self.seletedLabel(self.addContenArr);
                }];
                [sheetView showView];
            }
        }
    }];
}
@end
