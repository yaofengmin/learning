//
//  TagCollectionViewCell.h
//  yanshan
//
//  Created by fm on 2017/9/2.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TagCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *label;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelRight;

@property (nonatomic,strong) NSIndexPath *indexPath;
@property (nonatomic,copy) void(^deleteBlock)(NSIndexPath *indexPath);
@end
