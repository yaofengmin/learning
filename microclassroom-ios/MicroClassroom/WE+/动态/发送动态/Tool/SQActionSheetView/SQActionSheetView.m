//
//  SQActionSheetView.m
//  JHTDoctor
//
//  Created by yangsq on 2017/5/23.
//  Copyright © 2017年 yangsq. All rights reserved.
//

#import "SQActionSheetView.h"

#import "HXTagsView.h"

#import "SeacherTipListModel.h"

#define Margin  6
#define ButtonHeight  45
#define TitleHeight   30
#define LineHeight    0.5

#define itemLine 15


#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)


@interface SQActionSheetView ()<HXTagsViewDelegate>

@property (nonatomic,strong)NSArray *ContenArr;

@property (nonatomic,assign)NSInteger i;

@property (nonatomic,assign)CGSize size;

//@property (nonatomic, strong) UIToolbar *containerToolBar;
@property (nonatomic, strong) UIView *bottomV;
@property (nonatomic, assign) CGFloat toolbarH;

@property (nonatomic,strong) NSMutableArray *addContentArr;

@end

@implementation SQActionSheetView

- (id)initWithTitle:(NSString *)title data:(NSArray *)dataSource addContentArr:(NSArray *)addContentArr buttonClick:(void (^)(SQActionSheetView *, NSMutableArray *))block{
    
    if (self = [super init]) {
        self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.3];
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        //多行不滚动,则计算出全部展示的高度,让maxHeight等于计算出的高度即可,初始化不需要设置高度
        HXTagsView* tagsView = [[HXTagsView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0)];
        tagsView.type = 0;
        tagsView.tagOriginX = 10;
        tagsView.tagOriginY = 20;
        tagsView.tagHorizontalSpace = 22;
        tagsView.normalBackgroundImage = [UIImage imageNamed:@"label_normal"];
        tagsView.seletedBackgroundImage = [UIImage imageNamed:@"lable_Sel"];
        tagsView.titleColor = JColor;
        tagsView.titleSize = 12;
        NSMutableArray *labelArr = [NSMutableArray array];
        for (SeacherItemModel *dic in dataSource) {
            [labelArr addObject:dic.codeName];
        }
        tagsView.seletedBtnArr = addContentArr;
        [tagsView setTagAry:labelArr delegate:self];
        
        
        _toolbarH = ButtonHeight + Margin + tagsView.frame.size.height + 10;
        
        
        
        //        _containerToolBar = [[UIToolbar alloc]initWithFrame:(CGRect){0,CGRectGetHeight(self.frame),CGRectGetWidth(self.frame),_toolbarH}];
        //        _containerToolBar.clipsToBounds = YES;
        
        _bottomV = [[UIView alloc]initWithFrame:(CGRect){0,CGRectGetHeight(self.frame),CGRectGetWidth(self.frame),_toolbarH}];
        [_bottomV addSubview:tagsView];
        
        //        [_containerToolBar layoutIfNeeded];
        [tagsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.mas_equalTo(0);
            make.width.mas_equalTo(KScreenWidth);
            make.height.mas_equalTo(tagsView.frame.size.height + 10);
        }];
        
        _ContenArr = dataSource;
        _addContentArr = [NSMutableArray array];
        [_addContentArr addObjectsFromArray:addContentArr];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.8];
        [button setTitle:@"确定" forState:UIControlStateNormal];
        [button setTitleColor:JColor forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:17]];
        button.tag = 101;
        [button addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = (CGRect){0,_toolbarH - ButtonHeight,CGRectGetWidth(self.frame),ButtonHeight};
        
        [_bottomV addSubview:button];
        
        self.buttonClick = block;
    }
    
    return self;
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismissView];
}

- (void)buttonTouch:(UIButton *)button{
    
    if (self.buttonClick) {
        self.buttonClick(self, self.addContentArr);
    }
    [self dismissView];
    
}


- (void)showView{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [[UIApplication sharedApplication].keyWindow addSubview:_bottomV];
    
    self.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        _bottomV.transform = CGAffineTransformMakeTranslation(0, -_toolbarH - kBottomOffSet);
        self.alpha = 1;
        
    } completion:^(BOOL finished) {
        
    }];
    
}

- (void)dismissView{
    [UIView animateWithDuration:0.3 animations:^{
        _bottomV.transform = CGAffineTransformIdentity;
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [_bottomV removeFromSuperview];
    }];
}

#pragma mark === HXTagsViewDelegate
-(void)tagsViewButtonAction:(HXTagsView *)tagsView button:(UIButton *)sender
{
    NSInteger tag = sender.tag;
    NSLog(@"tag:%@ index:%ld",sender.titleLabel.text,(long)sender.tag);
    SeacherItemModel *model = self.ContenArr[tag];
    if (!sender.selected) {
        if (self.addContentArr.count >= 3) {
            [WFHudView showMsg:@"最多选择3个" inView:self];
            return;
        }
    }
    sender.selected = !sender.isSelected;
    if (sender.selected) {
        [self.addContentArr addObject:self.ContenArr[tag]];
    }else{
        for (SeacherItemModel *selModel in self.addContentArr.mutableCopy) {
            if ([model.codeId isEqualToString:selModel.codeId]) {
                [self.addContentArr removeObject:selModel];
            }
        }
    }
    
}



@end
