//
//  SQActionSheetView.h
//  JHTDoctor
//
//  Created by yangsq on 2017/5/23.
//  Copyright © 2017年 yangsq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQActionSheetView : UIView

@property (nonatomic, copy) void(^buttonClick)(SQActionSheetView *sheetView,NSMutableArray *addContentArr);

- (id)initWithTitle:(NSString *)title data:(NSArray *)dataSource  addContentArr:(NSArray *)addContentArr buttonClick:(void (^)(SQActionSheetView *, NSMutableArray *))block;
- (void)showView;
@end
