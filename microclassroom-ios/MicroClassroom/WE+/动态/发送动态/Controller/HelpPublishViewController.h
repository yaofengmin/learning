//
//  HelpPublishViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/28.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

@protocol PublishCircleDelegate <NSObject>

@required

-(void)finishPublish;

@end
@interface HelpPublishViewController : YHBaseViewController
@property (assign ,nonatomic) id <PublishCircleDelegate> delegate ;


-(instancetype)initWithPubilshType:(GetMessagesType) publishType;

@end
