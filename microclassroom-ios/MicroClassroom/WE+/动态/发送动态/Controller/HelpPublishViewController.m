//
//  HelpPublishViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/28.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "HelpPublishViewController.h"

#import "HelpPublishInputTableViewCell.h"
#import "HelpPublishImageTableViewCell.h"

#import "HelpPublishTagTableViewCell.h"

#import "SeacherTipListModel.h"

#import "UserInfoModel.h"
#import "ClassModel.h"

//班级选择
#import "ClassPublishViewBottom.h"


static NSString *HelpPublishInputCellID = @"HelpPublishInputCellID";
static NSString *HelpPublishImageCellID = @"HelpPublishImageCellID";

@interface HelpPublishViewController ()<UITableViewDelegate,UITableViewDataSource,uploadImage>
@property (nonatomic,strong) AssignmentModel *imageModel;
@property (nonatomic,strong) NSArray *imageArr;
@property (nonatomic,copy) NSString *content;//发表内容
@property (nonatomic,strong) NSArray *labelArr;//标签数据

@property (nonatomic ,strong) UITableView *tableV;

/**
 是否置顶
 */
@property (nonatomic ,assign) BOOL isTop;

/**
 班级选择
 */
@property (nonatomic , strong) ClassPublishViewBottom *BottomView;

/**
 发布类型
 */
@property (nonatomic,assign) GetMessagesType type;

/**
 班级id
 */
@property (nonatomic,strong) ClassModel *classmodel;


@end

@implementation HelpPublishViewController
-(instancetype)initWithPubilshType:(GetMessagesType)publishType
{
    if (self = [super init]) {
        self.type = publishType;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"发送动态";
    _imageModel = [[AssignmentModel alloc]init];
    _isTop = NO;
    [self initalTable];
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithTitle:@"发送" style:YHBarButtonItemStylePlain handler:^(id send) {
        @strongify(self);
        [self publishMessage];
    }];
}

-(void)initalTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.backgroundColor = [UIColor clearColor];
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"HelpPublishInputTableViewCell" bundle:nil] forCellReuseIdentifier:HelpPublishInputCellID];
}
#pragma mark === UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        CGFloat cellH = 180;//正常High
        if (self.type == GetMessagesTypeStudyGroup) {//班级成立,需要显示参加的班级
            cellH = cellH + 50;
        }
        if ([[UserInfoModel getInfoModel].grade isEqualToString:@"99"]) {//超级会员.显示是否置顶按钮
            cellH = cellH + 50;
        }
        return cellH;
    }else if (indexPath.row == 1){
        return 100;
    }
    return _imageModel.returnCellHeight;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        HelpPublishInputTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HelpPublishInputCellID forIndexPath:indexPath];
        if ([[UserInfoModel getInfoModel].grade isEqualToString:@"99"]) {//超级会员.显示是否置顶按钮
            cell.isTopView.hidden = NO;
        }else{
            cell.isTopView.hidden = YES;
        }
        if (self.type == GetMessagesTypeStudyGroup) {  // 班级
            cell.classBottomV.hidden = NO;
            cell.tipLabel.text = @"输入内容";
        }else{
            cell.classBottomV.hidden = YES;
            cell.tipLabel.text = @"同学,感谢分享,您的智慧在审核通过后将分享给所有人";
        }
        @weakify(self);
        cell.textBlock = ^(NSString *text) {
            @strongify(self);
            self.content = text;
        };
        
        cell.isTopBlock = ^(BOOL isTop) {
            @strongify(self);
            self.isTop = isTop;
        };
        cell.changeGroupId = ^(ClassModel *model) {
            if (self.type == GetMessagesTypeStudyGroup) {
                self.classmodel = model;
            }
        };
        return cell;
    }else if (indexPath.row == 1){
        HelpPublishTagTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HelpPublishTagCellID"];
        if (!cell) {
            cell = [[HelpPublishTagTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HelpPublishTagCellID"];
        }
        cell.seletedLabel = ^(NSArray *seletedArr) {
            self.labelArr = seletedArr;
        };
        return cell;
    }else {
        HelpPublishImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HelpPublishImageCellID];
        if (!cell) {
            cell = [[HelpPublishImageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HelpPublishImageCellID];
        }
        cell.indexPath = indexPath;
        cell.tableView = self.tableV;
        cell.delegate = self;
        cell.currentModel = _imageModel;
        return cell;
    }
}

#pragma mark === UITableViewDelegate

#pragma mark === ZSTextViewCellDelegate

-(void)getUpLoadPhotoArr:(NSArray *)photoArr atIndexPath:(NSIndexPath *)indexPath
{
    self.imageArr = photoArr;
}

-(void)publishMessage
{
    if ([self chectPram]) {
        NSMutableArray *imageMu = [NSMutableArray array];
        for (UIImage *image in self.imageArr) {
            NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
            [imageMu addObject:imageData];
            
        }
        NSString *labelID = @"";
        for (SeacherItemModel *selModel in self.labelArr) {
            if (labelID.length == 0) {
                labelID = selModel.codeId;
            }else{
                labelID = [NSString stringWithFormat:@"%@,%@",labelID,selModel.codeId];
            }
        }
        NSString *classId = @"0";
        if (_classmodel) {
            classId = self.classmodel.classId;
        }
        NSDictionary *paramDic = @{Service:CirclePublishMessage,
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"ctype":@"1",
                                   @"content":self.content.length == 0?@"":self.content,
                                   //                                   @"isClass":@(self.type),
                                   @"labelId":labelID.length == 0?@"":labelID,
                                   @"classesId":classId,
                                   @"ifTop":@(self.isTop),
                                   };
        SHOWHUD;
        [HTTPManager publishCarCirclWithUrl:MainAPI andPostParameters:paramDic andImageDic:@{@"pics":imageMu} andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    [self backBtnAction];
                    if ([_delegate respondsToSelector:@selector(finishPublish)]) {
                        [_delegate finishPublish];
                    }
                }else{
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];
    }
}

-(BOOL)chectPram
{
    //we+发布 需要至少vip1
    if ([[UserInfoModel getInfoModel].grade integerValue] < 1) {
        @weakify(self);
        [YHJHelp aletWithTitle:@"提示" Message:@"您还不是VIP会员，不能发布" sureTitle:@"立即购买" CancelTitle:@"忽略 " SureBlock:^{
            @strongify(self);
            BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
            [self.navigationController pushViewController:buy animated:YES];
        } andCancelBlock:nil andDelegate:self];
        return NO;
    }
    if (self.content.length == 0) {
        [WFHudView showMsg:@"内容不能为空" inView:self.view];
        return NO;
    }
    
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
