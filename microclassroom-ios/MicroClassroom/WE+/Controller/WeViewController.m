//
//  WeViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "WeViewController.h"
#import "DuringClassViewController.h"
#import "SchoolmateViewController.h"
#import "DeskmateViewController.h"
#import "StudyGroupViewController.h"


@interface WeViewController ()
<
FJSlidingControllerDataSource,
FJSlidingControllerDelegate
>

@property (nonatomic, strong)NSArray *titles;
@property (nonatomic, strong)NSArray *controllers;
@property (nonatomic, assign)NSInteger index;

@end

@implementation WeViewController


-(void)viewDidLoad
{
    self.isShowLine = YES;
    self.lineW = 20.0f;
    [super viewDidLoad];
    
    [self initalTopSegment];
    [self configController];
    //    KAddobserverNotifiCation(@selector(reloadDataWithBrandId), NOTIGICATIONFINDVVIEWRELOAD);
    
    
}
//-(void)reloadDataWithBrandId
//{
//    [self configController];
//    [self reloadData];
//}
#pragma mark === 重新布局
-(void)initalTopSegment
{
    self.segTop = KStatusBarHeight;
    [self.segmentTapView removeFromSuperview];
    [self.yh_navigationBar addSubview:self.segmentTapView];
    [self.segmentTapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.bottom.equalTo(self.yh_navigationBar);
        make.height.mas_equalTo(SEGMENTHEIGHT);
    }];
    self.segmentTapView.backgroundColor = [UIColor clearColor];
    self.datasouce = self;
    self.delegate = self;
    
}

-(void)configController{
    
    DuringClassViewController *duringClass = [[DuringClassViewController alloc]initWithShowMessageType:GetMessagesTypeDuringClass andClassId:@"0"];
    duringClass.parentContrl = self;

    
    DeskmateViewController *deskmate = [[DeskmateViewController alloc]init];
    deskmate.parentContrl = self;
    SchoolmateViewController *schoolmate = [[SchoolmateViewController alloc]init];
    schoolmate.parentContrl = self;
    self.titles      = @[@"发布",@"通讯录",@"链接"];
    self.controllers = @[duringClass,deskmate,schoolmate];
    [self addChildViewController:duringClass];
    [self addChildViewController:deskmate];
    [self addChildViewController:schoolmate];
    self.title = self.titles[0];
    [self reloadData];
}


#pragma mark dataSouce
- (NSInteger)numberOfPageInFJSlidingController:(FJSlidingController *)fjSlidingController{
    return self.titles.count;
}
- (UIViewController *)fjSlidingController:(FJSlidingController *)fjSlidingController controllerAtIndex:(NSInteger)index{
    return self.controllers[index];
}
- (NSString *)fjSlidingController:(FJSlidingController *)fjSlidingController titleAtIndex:(NSInteger)index{
    return self.titles[index];
}


- (UIColor *)titleNomalColorInFJSlidingController:(FJSlidingController *)fjSlidingController
{
    return JColor;
    
}
- (UIColor *)titleSelectedColorInFJSlidingController:(FJSlidingController *)fjSlidingController
{
    return kNavigationBarColor;
}
- (UIColor *)lineColorInFJSlidingController:(FJSlidingController *)fjSlidingController
{
    return kNavigationBarColor;
}
- (CGFloat)titleFontInFJSlidingController:(FJSlidingController *)fjSlidingController
{
    return 16.0;
}

#pragma mark delegate
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedIndex:(NSInteger)index{
    // presentIndex
    
    self.title = [self.titles objectAtIndex:index];
}

- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedController:(UIViewController *)controller{
    // presentController
}
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedTitle:(NSString *)title{
    // presentTitle
}

//- (void)dealloc
//{
//    KRemoverNotifi(NOTIGICATIONFINDVVIEWRELOAD);
//}
@end
