//
//  UserNewsViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "UserNewsVC.h"
#import "MWPhotoBrowser.h"
#import "CommentdetailViewController.h"
#import "PersonHomeViewController.h"
#import "RDVTabBarController.h"
#import "YHNavgationController.h"


#import "YHBaseWebViewController.h"
#import "ShareDetailListViewController.h"
#import "DownloadWebViewController.h"
#import "ShareListModel.h"


@interface UserNewsVC ()

<
MWPhotoBrowserDelegate
>

@end

@implementation UserNewsVC


-(void)viewDidAppear:(BOOL)animated
{
    if (self.ymData) {
        
        //        [self reloadOneRowsWithData:self.ymData andRow:self.row];
    }
    [super viewDidAppear:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configData];
    self.mainTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.mainTable.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    self.mainTable.mj_footer.hidden = YES;
    CGFloat height = 0.0;
    if (!self.parentContrl) {
        if (self.messageType == GetMessagesTypeGetMyStudyGroup) {
            self.yh_navigationItem.title  = @"我的江湖";
        }else{
            self.yh_navigationItem.title  = @"我的动态";
        }
        
        [self.mainTable mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
        }];
    }
    if (self.friendId == [KgetUserValueByParaName(USERID) intValue]) {
        height = KScreenHeight - 200;
    }else {
        height = KScreenHeight - 260;
    }
    self.mainTable.frame = CGRectMake(0, 0, KScreenWidth,height);
    [self loadDataForHeader];
    
}


-(void)configData
{
    self.photos            = @[].mutableCopy;
    self.thumbs            = @[].mutableCopy;
}


#pragma mark - 图片点击事件回调
- (void)showImageViewWithImageViews:(NSArray *)imageViews byClickWhich:(NSInteger)clickTag{
    
    [[self.parentContrl rdv_tabBarController] setTabBarHidden:YES animated:YES];
    
    [self.photos removeAllObjects];
    [self.thumbs removeAllObjects];
    
    for ( NSString *thumbUrl in imageViews) {
        
        [self.thumbs addObject:[MWPhoto photoWithURL:[NSURL URLWithString:thumbUrl]]];
        [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[thumbUrl  stringByReplacingOccurrencesOfString:@"_thumb" withString:@""]]]];
    }
    
    BOOL displayActionButton = YES; //分享按钮,默认是
    BOOL displaySelectionButtons = NO; //是否显示选择按钮在图片上,默认否
    BOOL displayNavArrows = NO;  //左右分页切换,默认否
    BOOL enableGrid = NO; //是否允许用网格查看所有图片,默认是
    BOOL startOnGrid = NO; //是否第一张,默认否
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = displayActionButton;
    browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = displaySelectionButtons; //控制条件控件 是否显示,默认否
    browser.zoomPhotosToFill = NO; //是否全屏,默认是
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = YES;
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    browser.wantsFullScreenLayout = YES;//是否全屏
#endif
    
    [browser setCurrentPhotoIndex:clickTag];
    
    if (displaySelectionButtons) {
        self.selections = [NSMutableArray new];
        for (int i = 0; i < self.photos.count; i++) {
            [self.selections addObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        // Push
        if (self.parentContrl) {
            [self.parentContrl.navigationController pushViewController:browser animated:YES];
        }else{
            [self.navigationController pushViewController:browser animated:YES];
        }
        
    } else {
        // Modal
        YHNavgationController *nc = [[YHNavgationController alloc] initWithRootViewController:browser];
        nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        if (self.parentContrl) {
            [self.parentContrl.navigationController pushViewController:nc animated:YES];
        }else{
            [self.navigationController pushViewController:nc animated:YES];
        }
    }
    
}


#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < self.thumbs.count)
        return [self.thumbs objectAtIndex:index];
    return nil;
}


- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return [[self.selections objectAtIndex:index] boolValue];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.row = indexPath.row;
    self.ymData = (YMTextData *)[self.tableDataSource objectAtIndex:indexPath.row];
    [self pushDetailActionWithindexPath:indexPath];

}
#pragma mark === 详情请求
-(void)pushDetailActionWithindexPath:(NSIndexPath *)indexPath
{
    CommentdetailViewController *detailC = [[CommentdetailViewController alloc]initWithShowMessageType:GetMessagesTypeDuringClass andClassId:@""];
    detailC.ymData = self.ymData;
    @weakify(self);
    detailC.deletedMsg = ^(YMTextData *ymd , NSInteger indext) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableDataSource removeObjectAtIndex:indexPath.row];
            [self.mainTable deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationLeft];
        });
    };
    [self.parentContrl.navigationController pushViewController:detailC animated:YES];
}


-(void)loadDataForHeader
{
    [super loadDataForHeader];
}

-(void)loadMoreData
{
    [super loadMoreData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --- 头像点击事件    跳转详情
-(void)userPhotoImageClick:(NSInteger)index adnUsrId:(NSInteger)userId{
    
    PersonHomeViewController *userHome = [[PersonHomeViewController alloc]init];
    userHome.userId = (int)userId;
    if (self.parentContrl) {
        [self.parentContrl.navigationController pushViewController:userHome animated:YES];
    }else{
        [self.navigationController pushViewController:userHome animated:YES];
    }
}

#pragma mark === 共享详情
-(void)requestDetailNewsWithNewId:(NSString *)newsId
{
    NSDictionary *paramDic = @{Service:GetNewsInfo,
                               @"newsId":newsId,
                               @"user_Id":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                ShareDetailModel *model = [ShareDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                DownloadWebViewController *detailVC = [[DownloadWebViewController alloc]initWithUrlStr:model.filePathShow andNavTitle:model.newsName];
                detailVC.detailModel = model;
                if (self.parentContrl) {
                    [self.parentContrl.navigationController pushViewController:detailVC animated:YES];
                }else{
                    [self.navigationController pushViewController:detailVC animated:YES];
                }
            }
        }
        
        
    }];
}
-(void)showFileViewWithFiles:(NSArray *)files byClickWhich:(NSInteger)clickTag
{
    NSArray *fileNameArr = [files[clickTag] componentsSeparatedByString:@"_"];
    NSString *fileName = fileNameArr.lastObject;
    FileWebViewController *web = [[FileWebViewController alloc]initWithUrlStr:files[clickTag] andNavTitle:fileName];
    if (self.parentContrl) {
        [self.parentContrl.navigationController pushViewController:web animated:YES];
    }else{
        [self.navigationController pushViewController:web animated:YES];
    }
}

#pragma mark === 展示web
-(void)showWebViewWithlink:(NSString *)linkUrl title:(NSString *)title index:(NSInteger)stamp
{
    if ([linkUrl hasPrefix:@"http"]) {
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:linkUrl andNavTitle:title];
        if (self.parentContrl) {
            [self.parentContrl.navigationController pushViewController:web animated:YES];
        }else{
            [self.navigationController pushViewController:web animated:YES];
        }
    }else{
        if ([[UserInfoModel getInfoModel].grade integerValue] < 2) {//共享文件 需要 grade >= 2
            @weakify(self);
            [YHJHelp aletWithTitle:@"提示" Message:@"VIP会员等级不足，不能查看" sureTitle:@"立即升级" CancelTitle:@"忽略 " SureBlock:^{
                @strongify(self);
                BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                if (self.parentContrl) {
                    [self.parentContrl.navigationController pushViewController:buy animated:YES];
                }else{
                    [self.navigationController pushViewController:buy animated:YES];
                }
            } andCancelBlock:nil andDelegate:self];
            return;
        }
        [self requestDetailNewsWithNewId:linkUrl];
    }
    [self requestCircleInfoWithIndex:stamp];
}
#pragma mark === 获取当前动态详情
-(void)requestCircleInfoWithIndex:(NSInteger )stamp
{
    YMTextData *data = (YMTextData *)[self.tableDataSource objectAtIndex:stamp];
    NSMutableDictionary *sendDic  =  [[NSMutableDictionary alloc]init];
    [sendDic setObject:CircleGetMessageInfo forKey:Service];
    [sendDic setObject:data.messageBody.msgId forKey:@"msgId"];
    [sendDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        data.messageBody.viewCount = result[REQUEST_INFO][@"viewCount"];
        [self reloadOneRowsWithData:data andRow:stamp];
    }];
}


@end
