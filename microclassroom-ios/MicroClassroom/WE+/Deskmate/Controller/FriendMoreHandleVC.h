//
//  FriendMoreHandleVC.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/31.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"
@class OtherUserInfoModel;

@interface FriendMoreHandleVC : YHBaseViewController
@property (nonatomic, strong) OtherUserInfoModel * userModel;
@end
