
//
//  CheckListViewController.m
//  MicroClassroom
//
//  Created by 姚凤敏 on 2019/12/17.
//  Copyright © 2019 Hanks. All rights reserved.
//

#import "CheckListViewController.h"

#import "PersonHomeViewController.h"

#import "PeopleCell.h"
#import "PeopleModel.h"
static NSString * const kPeopleCell = @"PeopleCell";

@interface CheckListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableV;

@property (nonatomic,strong) NSArray *checkList;
@end

@implementation CheckListViewController

- (instancetype)initCheckList:(NSArray *)checkList
{
    self = [super init];
    if (self) {
        self.checkList = checkList;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"认证节点";
    [self creatTable];
}

- (void)creatTable
{
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.tableFooterView = [UIView new];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    [self.tableV registerNib:[UINib nibWithNibName:kPeopleCell bundle:nil]
      forCellReuseIdentifier:kPeopleCell];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.checkList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CheckCellID"];
    //    if (!cell) {
    //        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CheckCellID"];
    //    }
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //    cell.textLabel.textColor = KColorFromRGB(0x646466);
    //    cell.textLabel.font = Font(14);
    //    UserInfoModel *user = self.checkList[indexPath.row];
    //    cell.textLabel.text = user.userName;
    PeopleCell * cell = [tableView dequeueReusableCellWithIdentifier:kPeopleCell forIndexPath:indexPath];
    [cell configureCellWithSchoolMateModel:self.checkList[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    SchoolMateModel *model =  self.checkList[indexPath.row];
    if (model.label_have.count != 0 && model.personalSign.length != 0) {
        return 100;
    }else{
        return 70;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SchoolMateModel *model =  self.checkList[indexPath.row];
    PersonHomeViewController *vc = [[PersonHomeViewController alloc]init];
    vc.userId = model.userId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 60, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 60, 0, 0)];
    }
    
}
@end
