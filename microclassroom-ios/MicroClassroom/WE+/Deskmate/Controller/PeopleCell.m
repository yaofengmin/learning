//
//  PeopleCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/14.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "PeopleCell.h"
#import "PeopleModel.h"

#import "UIImageView+AFNetworking.h"

static NSString * const kMaleImageName = @"login_male_selected";
static NSString * const kFemaleImageName = @"login_female_selected";

@interface PeopleCell ()

@property (weak, nonatomic) IBOutlet UILabel *chengweiLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property(nonatomic ,strong) NSMutableArray *categoryBtnArry;
@property (weak, nonatomic) IBOutlet UIImageView *addVImageView;

@end

@implementation PeopleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.avatarImage.backgroundColor = ImageBackColor;
    self.avatarImage.layer.cornerRadius = self.avatarImage.viewHeight/2;
    self.avatarImage.layer.masksToBounds = YES;
    _categoryBtnArry = @[].mutableCopy;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+ (CGFloat)cellHeight {
    return  75.0;
}

- (void)configureCellWithFriendModel:(FriendModel *)model {
    
    self.addVImageView.hidden = !model.isCertified;
    [self.avatarImage sd_setImageWithURL:[NSURL URLWithString:model.photo] placeholderImage:[UIImage imageNamed:@"login_head"]];
    self.nameLabel.text = model.noteName.length > 0 ? model.noteName : model.userName;
    self.descLabel.text = model.personalSign;
    
    self.chengweiLabel.text = model.title;
    for (UIButton *btn in _categoryBtnArry) {
        [btn removeFromSuperview];
    }
    
    [_categoryBtnArry removeAllObjects];
    
    for (int i = 0; i< model.label_have.count; i++) {
        
        UILabel *titleLable = [self tipLabelWithTitle:[model.label_have[i] objectForKey:@"codeName"]];
        if (i == 0) {
            titleLable.text = [model.label_have[i] objectForKey:@"codeName"];
        }else{
            titleLable.text = [NSString stringWithFormat:@"| %@",[model.label_have[i] objectForKey:@"codeName"]];
        }
        if (titleLable) {
            [self.contentView addSubview:titleLable];
            [_categoryBtnArry addObject:titleLable];
        }
    }
    
    for (int i = 0 ; i < _categoryBtnArry.count; i++) {
        
        [_categoryBtnArry[i] mas_makeConstraints:^(MASConstraintMaker *make) {
            
            if (i==0) {
                make.left.equalTo(self.avatarImage.mas_right).offset(10);
            }
            else {
                make.left.greaterThanOrEqualTo([_categoryBtnArry[i-1] mas_right]).offset(3);
            }
            
            NSString *title = [_categoryBtnArry[i] text];
            
            CGFloat width = [YHJHelp sizeWithWidth:100 andFont:Font(10) andString:title].width;
            
            make.size.equalTo(CGSizeMake((width>20?(width +2):20),15));
            make.top.equalTo(self.nameLabel.mas_bottom).offset(8);
        }];
    }
    
}

- (void)configureCellWithSchoolMateModel:(SchoolMateModel *)model {
    
    self.addVImageView.hidden = !model.isCertified;
    [self.avatarImage sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"login_head"]];
    self.nameLabel.text = model.userName;
    self.descLabel.text = model.personalSign;
    self.chengweiLabel.text = model.title;
    
    for (UIButton *btn in _categoryBtnArry) {
        [btn removeFromSuperview];
    }
    
    [_categoryBtnArry removeAllObjects];
    
    for (int i = 0; i< model.label_have.count; i++) {
        
        UILabel *titleLable = [self tipLabelWithTitle:[model.label_have[i] objectForKey:@"codeName"]];
        if (i == 0) {
            titleLable.text = [model.label_have[i] objectForKey:@"codeName"];
        }else{
            titleLable.text = [NSString stringWithFormat:@"| %@",[model.label_have[i] objectForKey:@"codeName"]];
        }
        if (titleLable) {
            [self.contentView addSubview:titleLable];
            [_categoryBtnArry addObject:titleLable];
        }
        
    }
    
    for (int i = 0 ; i < _categoryBtnArry.count; i++) {
        
        [_categoryBtnArry[i] mas_makeConstraints:^(MASConstraintMaker *make) {
            
            if (i==0) {
                make.left.equalTo(self.nameLabel.mas_left);
            }
            else {
                make.left.greaterThanOrEqualTo([_categoryBtnArry[i-1] mas_right]).offset(0);
            }
            
            NSString *title = [_categoryBtnArry[i] text];
            
            CGFloat width = [YHJHelp sizeWithWidth:100 andFont:Font(10) andString:title].width;
            
            make.size.equalTo(CGSizeMake((width>20?(width +2):20),15));
            //            make.size.equalTo(CGSizeMake((width>20?(width +5):20),15));
            make.top.equalTo(self.nameLabel.mas_bottom).offset(10);
        }];
    }
    
}


-(UILabel *)tipLabelWithTitle:(NSString *)title
{
    if (IsEmptyStr(title)) {
        return nil;
    }
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentCenter;
    //    label.backgroundColor = KColorFromRGB(0xffeeee);
    label.textColor = IColor;
    //    label.layer.cornerRadius = 4.0;
    //    label.layer.borderColor = KColorFromRGB(0xffa7a6) .CGColor;
    //    label.layer.borderWidth = 0.5;
    label.layer.masksToBounds = YES;
    label.font = Font(10);
    
    return label;
    
}


@end

