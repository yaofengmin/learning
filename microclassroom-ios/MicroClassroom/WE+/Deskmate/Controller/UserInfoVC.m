//
//  UserViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/14.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "UserInfoVC.h"
#import "MicroClassSectionView.h"
#import "UserInfoContactCell.h"
#import "UserInfoNormalCell.h"
#import "SegmentTapView.h"
#import "PeopleModel.h"
#import "UIImageView+AFNetworking.h"
#import "CheckListViewController.h"
#import "CheckListTableViewCell.h"

static NSString * const kUserInfoSectionViewID = @"MicroClassSectionView";
static NSString * const kUserInfoNormalCellID = @"UserInfoNormalCell";
static NSString * const kUserInfoContactCellID = @"UserInfoContactCell";
static NSString * const kCheckListCellID       = @"CheckListTableViewCell";

@interface UserInfoVC ()
<
SegmentTapViewDelegate,
UITableViewDelegate,
UITableViewDataSource
>

{
    NSMutableArray * _contactArr;
}

@property (nonatomic, strong) NSArray * tableSectionTitle;
@property (weak, nonatomic) IBOutlet UITableView *infoTableView;

@end

@implementation UserInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTable];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setTable {
    self.infoTableView.separatorColor = KColorFromRGB(0xededed);
    [self.infoTableView registerNib:[UINib nibWithNibName:kUserInfoContactCellID bundle:nil] forCellReuseIdentifier:kUserInfoContactCellID];
    [self.infoTableView registerNib:[UINib nibWithNibName:kUserInfoNormalCellID bundle:nil] forCellReuseIdentifier:kUserInfoNormalCellID];
    [self.infoTableView registerNib:[UINib nibWithNibName:kUserInfoSectionViewID bundle:nil] forHeaderFooterViewReuseIdentifier:kUserInfoSectionViewID];
    [self.infoTableView registerClass:[CheckListTableViewCell class] forCellReuseIdentifier:kCheckListCellID];
    self.infoTableView.tableFooterView = [UIView new];
}

- (void)setUserModel:(OtherUserInfoModel *)userModel {
    
    _userModel = userModel;
    _contactArr = [NSMutableArray arrayWithCapacity:2];
    
    switch (self.userModel.status) {
        case MCRelationTypeFriend: case MCRelationTypeSelf:{
            if ([KgetUserValueByParaName(KBrandInfoId) integerValue] > 0) {
                self.tableSectionTitle = @[@"简介",@"认证节点", @"联系方式",@"更多"];
            }else{
                self.tableSectionTitle = @[@"简介",@"认证节点", @"资源", @"需求", @"联系方式",@"更多"];
            }
            NSArray *linkArr = @[[UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"Tel",@"content" : _userModel.linkTel}],[UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"Mail",
                                                                                                                                                                                    @"content" : _userModel.email}]];
            NSArray *moreArr;
            if ([KgetUserValueByParaName(KBrandInfoId) integerValue]  > 0) {//特殊
                moreArr = @[[UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"兴趣爱好",@"content" : _userModel.hobby == nil ? @"" : _userModel.hobby}],
                            [UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"熟悉领域",@"content" : _userModel.knowledge == nil ? @"" : _userModel.knowledge}]];
            }else{
                moreArr = @[[UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"兴趣爱好",@"content" : _userModel.hobby == nil ? @"" : _userModel.hobby}],
                            [UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"熟悉领域",@"content" : _userModel.knowledge == nil ? @"" : _userModel.knowledge}],
                            [UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"来往城市",@"content" : _userModel.runingCity  == nil ? @"" : _userModel.runingCity }],
                            [UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"行业",@"content" : _userModel.industryName  == nil ? @"" : _userModel.industryName }]];
            }
            [_contactArr addObject:linkArr];
            [_contactArr addObject:moreArr];
            
        }
            break;
        default:
        {
            if ([KgetUserValueByParaName(KBrandInfoId) integerValue] > 0) {
                self.tableSectionTitle = @[@"简介",@"认证节点", @"联系方式"];
                NSArray *linkArr = @[[UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"Tel",@"content" : _userModel.linkTel}],[UserInfoContanctModel mj_objectWithKeyValues:@{@"title": @"Mail",
                                                                                                                                                                                        @"content" : _userModel.email}]];
                if (_contactArr.count) {
                    [_contactArr removeAllObjects];
                }
                [_contactArr addObject:linkArr];
            }else{
                self.tableSectionTitle = @[@"简介",@"认证节点", @"资源", @"需求"];
            }
        }
            break;
    }
    
    [self.infoTableView reloadData];
    
}

- (void)pullBothFriend {
    
    NSLog(@"%s, 获取共同好友",__func__);
    
    NSDictionary *paramDic = @{@"service": @""};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.tableSectionTitle.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (self.userModel.status) {
        case MCRelationTypeFriend: case MCRelationTypeSelf:{
            if ([KgetUserValueByParaName(KBrandInfoId) integerValue] > 0) {
                switch (section) {
                    case 1:
                        return [_contactArr[0] count];
                        break;
                    case 2:
                        return [_contactArr[1] count];
                        break;
                    default:
                        break;
                }
            }else{
                switch (section) {
                    case 1:
                        return _userModel.cheakUserList.count > 0?1:0;
                        break;
                    case 2:
                        return _userModel.label_have.count;
                        break;
                    case 3:
                        return _userModel.label_need.count;
                        break;
                    case 4:
                        return [_contactArr[0] count];
                    case 5:
                        return [_contactArr[1] count];
                    default:
                        break;
                }
            }
        }
            break;
        default:
        {
            if ([KgetUserValueByParaName(KBrandInfoId) integerValue] > 0) {
                switch (section) {
                    case 1:
                        return [_contactArr[0] count];
                        break;
                    default:
                        break;
                }
            }else{
                switch (section) {
                    case 1:
                        return _userModel.cheakUserList.count > 0?1:0;
                        break;
                    case 2:
                        return _userModel.label_have.count;
                        break;
                    case 3:
                        return _userModel.label_need.count;
                        break;
                    default:
                        break;
                }
            }
        }
            break;
    }
    
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UserInfoNormalCell * cell = [tableView dequeueReusableCellWithIdentifier:kUserInfoNormalCellID forIndexPath:indexPath];
        [cell configureCellWithStr:_userModel.brief];
        cell.desLabel.text = @"";
        return cell;
    }
    switch (self.userModel.status) {
        case MCRelationTypeFriend: case MCRelationTypeSelf:{
            if ([KgetUserValueByParaName(KBrandInfoId) integerValue] > 0) {
                //简介 联系方式 更多
                UserInfoContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kUserInfoContactCellID
                                                                             forIndexPath:indexPath];
                
                [cell configureCellWithModel:_contactArr[indexPath.section - 1][indexPath.row]];
                return cell;
                
            }else{
                if (indexPath.section == 4||indexPath.section == 5) {
                    UserInfoContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kUserInfoContactCellID
                                                                                 forIndexPath:indexPath];
                    [cell configureCellWithModel:_contactArr[indexPath.section - 4][indexPath.row]];
                    
                    return cell;
                }else {
                    if (indexPath.section == 1) {
                        CheckListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCheckListCellID];
                        [cell configWithModel:_userModel.cheakUserList];
//                        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"checkCellID"];
//                        if (!cell) {
//                            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"checkCellID"];
//                        }
//                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//                        cell.textLabel.textColor = KColorFromRGB(0x646466);
//                        cell.textLabel.font = Font(12);
//                        NSString *str = @"";
//                        for (SchoolMateModel *user in _userModel.cheakUserList) {
//                            str = [NSString stringWithFormat:@"%@ %@",str,user.userName];
//                        }
//                        cell.textLabel.text = str;
                        return cell;
                    }else{
                        UserInfoNormalCell * cell = [tableView dequeueReusableCellWithIdentifier:kUserInfoNormalCellID
                                                                                    forIndexPath:indexPath];
                        switch (indexPath.section) {
                            case 2:{
                                NSMutableString *str = _userModel.label_have[indexPath.row].codeName.mutableCopy;
                                if (NOEmptyStr(_userModel.label_have[indexPath.row].meno)) {
                                    //                    [str appendString:[NSString stringWithFormat:@"  [%@]",_userModel.label_have[indexPath.row].meno]];
                                    cell.desLabel.text = _userModel.label_have[indexPath.row].meno;
                                }
                                NSLog(@"%@",_userModel.label_have[indexPath.row].meno);
                                [cell configureCellWithStr:str];
                                
                            }
                                break;
                            case 3:{
                                
                                //                NSMutableString *str = _userModel.label_need[indexPath.row].codeName.mutableCopy;
                                if (NOEmptyStr(_userModel.label_need[indexPath.row].meno)) {
                                    //                    [str appendString:[NSString stringWithFormat:@"[%@]",_userModel.label_need[indexPath.row].meno]];
                                    cell.desLabel.text = _userModel.label_need[indexPath.row].meno;
                                }
                                [cell configureCellWithStr:_userModel.label_need[indexPath.row].codeName];
                                
                            }
                                break;
                            default:
                                break;
                        }
                        
                        return cell;
                    }
                }
            }
        }
            break;
        default:
        {
            if ([KgetUserValueByParaName(KBrandInfoId) integerValue] > 0) {//@"简介",认证节点 @"联系方式"
                //简介 认证节点 联系方式 更多
                UserInfoContactCell * cell = [tableView dequeueReusableCellWithIdentifier:kUserInfoContactCellID
                                                                             forIndexPath:indexPath];
                
                [cell configureCellWithModel:_contactArr[indexPath.section - 1][indexPath.row]];
                return cell;
                
            }else{//@"简介", @"认证节点",@"资源", @"需求"
                if (indexPath.section == 1) {
                    CheckListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCheckListCellID];
                    [cell configWithModel:_userModel.cheakUserList];
//                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"checkCellID"];
//                    if (!cell) {
//                        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"checkCellID"];
//                    }
//                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//                    cell.textLabel.textColor = KColorFromRGB(0x646466);
//                    cell.textLabel.font = Font(12);
//                    NSString *str = @"";
//                    for (SchoolMateModel *user in _userModel.cheakUserList) {
//                        str = [NSString stringWithFormat:@"%@ %@",str,user.userName];
//                    }
//                    cell.textLabel.text = str;
                    return cell;
                }else{
                    UserInfoNormalCell * cell = [tableView dequeueReusableCellWithIdentifier:kUserInfoNormalCellID forIndexPath:indexPath];
                    
                    switch (indexPath.section) {
                        case 2:{
                            NSMutableString *str = _userModel.label_have[indexPath.row].codeName.mutableCopy;
                            if (NOEmptyStr(_userModel.label_have[indexPath.row].meno)) {
                                //                    [str appendString:[NSString stringWithFormat:@"  [%@]",_userModel.label_have[indexPath.row].meno]];
                                cell.desLabel.text = _userModel.label_have[indexPath.row].meno;
                            }
                            NSLog(@"%@",_userModel.label_have[indexPath.row].meno);
                            [cell configureCellWithStr:str];
                            
                        }
                            break;
                        case 3:{
                            
                            //                NSMutableString *str = _userModel.label_need[indexPath.row].codeName.mutableCopy;
                            if (NOEmptyStr(_userModel.label_need[indexPath.row].meno)) {
                                //                    [str appendString:[NSString stringWithFormat:@"[%@]",_userModel.label_need[indexPath.row].meno]];
                                cell.desLabel.text = _userModel.label_need[indexPath.row].meno;
                            }
                            [cell configureCellWithStr:_userModel.label_need[indexPath.row].codeName];
                            
                        }
                            break;
                        default:
                            break;
                    }
                    return cell;
                }
            }
        }
            break;
    }
    
    return nil;
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return [UserInfoNormalCell cellHeightWithStr:_userModel.brief];
    }
    
    return 50;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (_userModel.cheakUserList.count > 0 && section == 1) {
        UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
        header.backgroundColor = [UIColor whiteColor];
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = MainBtnColor;
        [header addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.centerY.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(5, 20));
        }];
        UILabel *titleLbl = [UILabel labelWithText:@"认证节点" andFont:13 andTextColor:[UIColor blackColor] andTextAlignment:0];
        titleLbl.font = [UIFont boldSystemFontOfSize:13];
        [header addSubview:titleLbl];
        [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(line.mas_right).offset(10);
            make.centerY.mas_equalTo(line.mas_centerY);
        }];
        UIButton *arrowImg = [UIButton buttonWithType:UIButtonTypeCustom];
        [arrowImg setImage:[UIImage imageNamed:@"ic_enter"] forState:UIControlStateNormal];
        [header addSubview:arrowImg];
        [arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.centerY.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(40, 40));
        }];
        [arrowImg addTarget:self action:@selector(moreAction) forControlEvents:UIControlEventTouchUpInside];
        return header;
    }else{
        if (section == 1) {
            return nil;
        }else{
            MicroClassSectionView * view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kUserInfoSectionViewID];
            
            [view setTitle:self.tableSectionTitle[section]];
            
            return view;
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_userModel.cheakUserList.count > 0 && section == 1) {
        return 40;
    }else{
        if (section == 1) {
            return 0.01;
        }else{
            return 40;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0 && _userModel.cheakUserList.count > 0) {
        return 10;
    }
    return 0.01;
}

- (void)moreAction
{
    CheckListViewController *vc = [[CheckListViewController alloc]initCheckList:_userModel.cheakUserList];
    [self.parentContrl.parentCotr.navigationController pushViewController:vc animated:YES];
}

@end

