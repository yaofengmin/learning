//
//  PersonHomeViewController.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"
@interface PersonHomeViewController : YHBaseViewController
@property (nonatomic, assign) int userId;

/**
 0=自己的用户id 1=对应微吼的用户userId
 */
@property (nonatomic,copy) NSString *userIdCtype;

@end
