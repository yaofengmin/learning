//
//  AwaystationBookView.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AwaystationBookView : UIView
//@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextView *remarkTxt;
@property (weak, nonatomic) IBOutlet UITextField *startDateTxt;
@property (weak, nonatomic) IBOutlet UITextField *endDateTxt;
@property (weak, nonatomic) IBOutlet UIView *startDateView;
@property (weak, nonatomic) IBOutlet UIView *endDateView;

@property (assign, nonatomic) double price;
@property (assign, nonatomic) int count;
@property (copy, nonatomic) NSString *unit;

@end
