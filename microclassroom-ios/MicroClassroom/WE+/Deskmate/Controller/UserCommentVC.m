//
//  UserCommentViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "UserCommentVC.h"
#import "PeopleModel.h"
#import "NSString+Size.h"
@interface UserCommentVC ()
@property (nonatomic ,strong) UIScrollView *contentView;
@property (nonatomic ,strong) UILabel *commentLabel;
@end

@implementation UserCommentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)setParentContrl:(PersonSegViewController *)parentContrl {
    [self.view addSubview:self.contentView];
    [self.contentView addSubview:self.commentLabel];
    if (IsEmptyStr(parentContrl.userModel.comment)) {
        self.commentLabel.text = @"他还没有任何评价";
        [self.commentLabel sizeToFit];
        [self.commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.view);
        }];
    }else {
        self.commentLabel.text = parentContrl.userModel.comment;
    CGSize size = [self.commentLabel.text sizeWithFontSize:15 andRectSize:CGSizeMake(KScreenWidth - 20, 10000)];
    self.commentLabel.frame =  CGRectMake(10, 20, KScreenWidth-20, size.height);
        self.contentView.contentSize = CGSizeMake(KScreenWidth, size.height + 50);
    }
}

- (UILabel *)commentLabel {
    if (!_commentLabel) {
        _commentLabel = [[UILabel alloc]init];
        _commentLabel.textColor = DColor;
        _commentLabel.numberOfLines = 0;
        _commentLabel.font = Font(15);
    }
    return _commentLabel;
}

- (UIScrollView *)contentView {
    if (!_contentView) {
        _contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight - KTopHeight - SEGMENTHEIGHT - 100 - 65)];
    }
    return _contentView;
}

@end
