//
//  PersonSegViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "PersonSegViewController.h"

#import "UserInfoVC.h"
#import "UserCommonFriendVC.h"
#import "UserNewsVC.h"
#import "UserCommentVC.h"

#import "PeopleModel.h"

@interface PersonSegViewController ()

<
FJSlidingControllerDataSource,
FJSlidingControllerDelegate
>
@property (nonatomic, strong)NSArray *titles;
@property (nonatomic, strong)NSArray *controllers;
@property (nonatomic, assign)NSInteger index;

@end

@implementation PersonSegViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.
    self.datasouce = self;
    self.delegate = self;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUserModel:(OtherUserInfoModel *)userModel {
    
    _userModel = userModel;
    
    UserInfoVC * info = [[UserInfoVC alloc] init];
    info.parentContrl = self;
    info.userModel = self.userModel;
    
    UserCommonFriendVC * friend = [[UserCommonFriendVC alloc]init];
    friend.parentContrl = self;
    
    
    UserNewsVC * news = [[UserNewsVC alloc] initWithShowMessageType:GetMessagesTypeGetMyCircle andClassId:@""];
    news.friendId = [_userModel.userId intValue];
    news.parentContrl = self.parentCotr;
    UserCommentVC * comment = [[UserCommentVC alloc] init];
    comment.parentContrl = self;
    
    if (_userModel.status == MCRelationTypeFriend) {
        self.titles      = @[@"个人信息", @"动态", @"评价", @"共同好友"];
//        self.titles      = @[@"个人信息", @"动态", @"共同好友"];
        self.controllers = @[info, news, comment, friend];
//        self.controllers = @[info, news,friend];
        
        [self addChildViewController:info];
        [self addChildViewController:news];
        [self addChildViewController:comment];
        [self addChildViewController:friend];
        
    } else {
        
        self.titles      = @[@"个人信息", @"动态"];
        self.controllers = @[info, news];
        
        [self addChildViewController:info];
        [self addChildViewController:news];

    }
    
    self.title = self.titles[0];
    [self reloadData];
    
}

#pragma mark - FJSlidingControllerDataSource
- (NSInteger)numberOfPageInFJSlidingController:(FJSlidingController *)fjSlidingController{
    return self.titles.count;
}
- (UIViewController *)fjSlidingController:(FJSlidingController *)fjSlidingController controllerAtIndex:(NSInteger)index{
    return self.controllers[index];
}
- (NSString *)fjSlidingController:(FJSlidingController *)fjSlidingController titleAtIndex:(NSInteger)index{
    return self.titles[index];
}


- (UIColor *)titleNomalColorInFJSlidingController:(FJSlidingController *)fjSlidingController
{
    return DColor;
    
}
- (UIColor *)titleSelectedColorInFJSlidingController:(FJSlidingController *)fjSlidingController
{
    return kNavigationBarColor;
}
- (UIColor *)lineColorInFJSlidingController:(FJSlidingController *)fjSlidingController
{
    return [UIColor clearColor];
}
- (CGFloat)titleFontInFJSlidingController:(FJSlidingController *)fjSlidingController
{
    return 15.0;
}

#pragma mark - FJSlidingControllerDelegate
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedIndex:(NSInteger)index{
    // presentIndex
    
    self.title = [self.titles objectAtIndex:index];
}

- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedController:(UIViewController *)controller{
    // presentController
}
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedTitle:(NSString *)title{
    // presentTitle
}


@end
