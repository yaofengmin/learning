//
//  PeopleModel.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/14.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"
#import "hxAccountModel.h"
#import "FriendModel.h"

@interface FriendMateModel : YHBaseModel
@property (nonatomic, assign) int userId;
@property (nonatomic, copy) NSString *noteName;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *personalSign;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, assign) int status;
@property (nonatomic, copy) NSString *hxAccount;
@end

@interface SchoolMateModel : YHBaseModel
@property (nonatomic, assign) int userId;
/**单位职务*/
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *personalSign;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, strong)NSArray *label_need;
@property (nonatomic, strong)NSArray *label_have;
@property (nonatomic, assign) NSInteger isCertified;

@end

@interface UserInfoSectionModel : YHBaseModel
@property (nonatomic, assign) int codeId;
@property (nonatomic, copy) NSString *codeName;
@property (nonatomic, copy) NSString *meno;
@end

@interface UserInfoContanctModel : YHBaseModel
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;


@end

@interface OtherUserInfoModel : YHBaseModel
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, assign) int status;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *personalSign;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *sex;
@property (nonatomic, copy) NSString *areaId;
@property (nonatomic, copy) NSString *areaName;
@property (nonatomic, copy) NSString *noteName;
@property (nonatomic, strong) NSArray *pics;
@property (nonatomic, strong) NSArray<UserInfoSectionModel *> *label_have;
@property (nonatomic, strong) NSArray<UserInfoSectionModel *> *label_need;
@property (nonatomic, strong) NSString *linkTel;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *brief;
@property (nonatomic, strong) hxAccountModel *hxAccount;
@property (nonatomic, copy) NSString  *hobby;
@property (nonatomic, copy) NSString  *knowledge;
@property (nonatomic, copy) NSString  *runingCity;
@property (nonatomic, copy) NSString  *industryName;
@property (nonatomic, copy) NSString  *comment;
@property (nonatomic, copy) NSString *userCode;
@property (nonatomic, assign) NSInteger isCertified;
@property (nonatomic, copy) NSString *brandId;//特殊身份
@property (nonatomic,copy) NSString *wxUserAppId;//跳转小程序要拼接的

@property (nonatomic,strong) NSArray <SchoolMateModel *>*cheakUserList;//认证节点

@end
