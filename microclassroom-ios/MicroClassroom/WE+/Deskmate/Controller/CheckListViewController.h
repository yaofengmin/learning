//
//  CheckListViewController.h
//  MicroClassroom
//
//  Created by 姚凤敏 on 2019/12/17.
//  Copyright © 2019 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CheckListViewController : YHBaseViewController

- (instancetype)initCheckList:(NSArray *)checkList;

@end

NS_ASSUME_NONNULL_END
