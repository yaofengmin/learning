//
//  PersonSegViewController.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "FJSlidingController.h"
#import "PersonHomeViewController.h"
@class OtherUserInfoModel;

@interface PersonSegViewController : FJSlidingController
@property (nonatomic, strong) OtherUserInfoModel * userModel;
@property (nonatomic,  weak)  PersonHomeViewController *parentCotr;
@end
