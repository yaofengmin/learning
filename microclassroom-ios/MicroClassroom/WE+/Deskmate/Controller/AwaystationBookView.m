//
//  AwaystationBookView.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/27.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AwaystationBookView.h"

@interface AwaystationBookView ()
<
UITextViewDelegate,
UITextFieldDelegate
>

@property (strong, nonatomic) NSDateFormatter * dateFormatter;
@property (strong, nonatomic) UIDatePicker *pickerViewStart;
@property (strong, nonatomic) UIDatePicker *pickerViewEnd;
@property (strong, nonatomic) UIBarButtonItem * doneButton;
@property (strong, nonatomic) UIToolbar *keyboardDoneButtonView;
@property (strong, nonatomic) UILabel *textViewplaceholder;


@end

@implementation AwaystationBookView

- (void)awakeFromNib {
    
    
    self.remarkTxt.delegate = self;
    self.startDateTxt.delegate = self;
    self.endDateTxt.delegate = self;
    
    self.startDateView.layer.cornerRadius  = 3.0;
    self.startDateView.layer.masksToBounds = YES;
    self.endDateView.layer.cornerRadius    = 3.0;
    self.endDateView.layer.masksToBounds   = YES;
    self.remarkTxt.layer.cornerRadius      = 4.0;
    self.remarkTxt.layer.masksToBounds     = YES;
    
    [self.remarkTxt addSubview:self.textViewplaceholder];
    self.startDateTxt.text = [self.dateFormatter stringFromDate:[NSDate new]];
    self.endDateTxt.text = [self.dateFormatter stringFromDate:[NSDate new]];
    
    self.count = 1;
    self.unit = @"元";
    self.price = 0.0f;
    [super awakeFromNib];
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"AwaystationBookView" owner:self options: nil];
        
        if(arrayOfViews.count < 1){
            return nil;
        }
        
        if(![[arrayOfViews objectAtIndex:0] isKindOfClass:[UIView class]]){
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
        self.frame = frame;
        
    }
    
    return self;
    
}

- (NSDateFormatter *)dateFormatter {
    
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy/MM/dd";
    }
    return _dateFormatter;
}

- (UIDatePicker *)pickerViewEnd {
    
    if (!_pickerViewEnd) {
        _pickerViewEnd = [UIDatePicker new];
        _pickerViewEnd.datePickerMode = UIDatePickerModeDate;
        _pickerViewEnd.minimumDate = [NSDate new];
        [_pickerViewEnd setBackgroundColor:[UIColor whiteColor]];
    }
    
    return _pickerViewEnd;
    
}

- (UIDatePicker *)pickerViewStart {
    
    if (!_pickerViewStart) {
        _pickerViewStart = [UIDatePicker new];
        _pickerViewStart.datePickerMode = UIDatePickerModeDate;
        _pickerViewStart.minimumDate = [NSDate new];
        [_pickerViewStart setBackgroundColor:[UIColor whiteColor]];
    }
    
    return _pickerViewStart;
    
}

- (UILabel *)textViewplaceholder {
    
    if (!_textViewplaceholder) {
        _textViewplaceholder               = [[UILabel  alloc]initWithFrame:CGRectMake(5, 7, 0, 0)];
        _textViewplaceholder.textColor     = IColor;
        _textViewplaceholder.text          = @"这里填写备注";
        _textViewplaceholder.font          = [UIFont systemFontOfSize:14];
        _textViewplaceholder.textAlignment = NSTextAlignmentLeft;
        [_textViewplaceholder sizeToFit];
    }
    
    return _textViewplaceholder;
    
}

- (UIToolbar *)keyboardDoneButtonView {
    
    if (!_keyboardDoneButtonView) {
        _keyboardDoneButtonView = [[UIToolbar alloc] init];
        _keyboardDoneButtonView.backgroundColor=[UIColor whiteColor];
        _keyboardDoneButtonView.translucent =YES;
        _keyboardDoneButtonView.tintColor =nil;
        [_keyboardDoneButtonView sizeToFit];
        UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"取消"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(pickerCancelClicked)];
        UIBarButtonItem * empty=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                              target:self
                                                                              action:nil];
        
        
        [_keyboardDoneButtonView setItems:[NSArray arrayWithObjects:cancelButton,empty,self.doneButton,nil]];
    }

    return _keyboardDoneButtonView;
}

- (UIBarButtonItem *)doneButton {
    
    if (!_doneButton) {
        _doneButton = [[UIBarButtonItem alloc] initWithTitle:@"确定"
                                                       style:UIBarButtonItemStylePlain
                                                      target:self
                                                      action:@selector(pickerDoneClicked:)];
    }
    
    return _doneButton;

}

- (void)setKeyBoardWithPickerView:(UIDatePicker *)pickerView TextField:(UITextField *)textField {
    
    textField.inputView = pickerView;
    self.doneButton.tag = pickerView.tag;
    textField.inputAccessoryView = self.keyboardDoneButtonView;
    
}

- (void)pickerDoneClicked:(UIBarButtonItem *)sender {
    
    NSTimeInterval time = [[self.pickerViewEnd date] timeIntervalSinceDate:[self.pickerViewStart date]];
    
    if (sender.tag == self.pickerViewEnd.tag) {
        
        self.endDateTxt.text = [self.dateFormatter stringFromDate:[self.pickerViewEnd date]];
        
        if (time < 0) {
            self.startDateTxt.text = [self.dateFormatter stringFromDate:[self.pickerViewEnd date]];
            self.pickerViewStart.date = self.pickerViewEnd.date;
        }
        
    }
    
    if (sender.tag == self.pickerViewStart.tag) {
        
        self.startDateTxt.text = [self.dateFormatter stringFromDate:[self.pickerViewStart date]];
        
        if (time < 0) {
            self.endDateTxt.text = [self.dateFormatter stringFromDate:[self.pickerViewStart date]];
            self.pickerViewEnd.date = self.pickerViewStart.date;
        } 

    }
    
    int timeCount = (int)[[self.dateFormatter dateFromString:self.endDateTxt.text] timeIntervalSinceDate:[self.dateFormatter dateFromString:self.startDateTxt.text]];
    
    int days = timeCount / (60*60*24);
    int dayCount = (timeCount % (60*60*24)) == 0 ? (days + 1) : days;
    
//    self.priceLabel.text = [NSString stringWithFormat:@"%.2f %@", dayCount * self.price, self.unit];
    self.count = dayCount;
    
    [self endEditing:YES];
}

-(void)pickerCancelClicked {
    [self endEditing:YES];
}
#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length==0) {
        [textView addSubview:self.textViewplaceholder];
    } else {
        [self.textViewplaceholder removeFromSuperview];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    if (textField == self.startDateTxt) {
        self.pickerViewStart.tag = 101;
        [self setKeyBoardWithPickerView:self.pickerViewStart TextField:textField];
    }
    
    if (textField == self.endDateTxt) {
        self.pickerViewEnd.tag = 102;
        [self setKeyBoardWithPickerView:self.pickerViewEnd TextField:textField];
    }
    
    return YES;
    
}

@end
