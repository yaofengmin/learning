//
//  PeopleCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/14.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseTableViewCell.h"

@class FriendModel;
@class SchoolMateModel;
@interface PeopleCell : YHBaseTableViewCell
- (void)configureCellWithFriendModel:(FriendModel *)model;
- (void)configureCellWithSchoolMateModel:(SchoolMateModel *)model;
@end
