//
//  FriendMoreHandleVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/31.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "FriendMoreHandleVC.h"
#import "CommonEditInfoVC.h"

#import "PeopleModel.h"
#import "DBDataModel.h"
static NSString * kMoreHandleVCTitle = @"好友设置";

@interface FriendMoreHandleVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSArray * titleArr;

@end

@implementation FriendMoreHandleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.yh_navigationItem.title = kMoreHandleVCTitle;
    self.tableView.tableFooterView = [UIView new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)titleArr {
    
    if (!_titleArr) {
        _titleArr = @[@"设置备注", @"地区"];
    }
    
    return _titleArr;
    
}

- (IBAction)deleteFriend:(UIButton *)sender {
    
    @weakify(self);
   [YHJHelp aletWithTitle:nil Message:@"确定删除该好友?" sureTitle:@"确定" CancelTitle:@"取消" SureBlock:^{
       @strongify(self);
       NSDictionary *paramDic = @{@"service": CircleDeleteFriend,
                                  @"userId": KgetUserValueByParaName(USERID),
                                  @"friendId": self.userModel.userId};
       
       SHOWHUD;
       [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
           HIDENHUD;
           //业务逻辑层
           if (result) {
               
               if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                   [WFHudView showMsg:@"删除成功" inView:nil];
                   KPostNotifiCation(DeskmateRefresh, nil);
                   [self.navigationController popToRootViewControllerAnimated:YES];
                   
               } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                   
                   [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                   
               }
               
           }
       }];

   } andCancelBlock:nil andDelegate:self];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.titleArr.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    
    cell.textLabel.text = self.titleArr[indexPath.section];
    
    switch (indexPath.section) {
        case 0:
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.detailTextLabel.text = self.userModel.noteName;
            break;
        case 1:
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.detailTextLabel.text = self.userModel.areaName;
            break;
        default:
            break;
    }
    
    return cell;
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        NSLog(@"设置备注");
        
        @weakify(self);
        CommonEditInfoVC * targetVC = [[CommonEditInfoVC alloc] init];
        targetVC.editAction = MCEditActionRemarkName;
        targetVC.defaultText = self.userModel.noteName;
        targetVC.friendId = self.userModel.userId;
        targetVC.block = ^(NSString *str){
            @strongify(self);
            self.userModel.noteName = str;
            [self.tableView reloadData];
        };
        [self.navigationController pushViewController:targetVC animated:YES];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}


@end
