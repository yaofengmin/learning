//
//  UserCommonFriendVC.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YHBaseViewController.h"
#import "PersonSegViewController.h"
@interface UserCommonFriendVC : YHBaseViewController
@property (nonatomic, weak) PersonSegViewController *parentContrl;
@end
