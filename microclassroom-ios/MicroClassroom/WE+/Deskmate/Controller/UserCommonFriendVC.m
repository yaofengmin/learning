//
//  UserCommonFriendVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "UserCommonFriendVC.h"
#import "PeopleModel.h"
#import "PeopleCell.h"
static NSString * const kPeopleCell = @"PeopleCell";
@interface UserCommonFriendVC ()
<
UITableViewDelegate,
UITableViewDataSource
>
{
    NSMutableArray <FriendModel *>* _friendModel;

}
@property (strong, nonatomic)  UITableView *tableView;
@end

@implementation UserCommonFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setUpTableView];
    [self pullData];
    // Do any additional setup after loading the view from its nib.
}

- (void)setUpTableView {
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
     [self.tableView registerNib:[UINib nibWithNibName:kPeopleCell bundle:nil] forCellReuseIdentifier:kPeopleCell];
}


- (void)pullData {
    
    NSDictionary *paramDic = @{@"service": CircleGetCommonFriendList,
                               @"otherId": self.parentContrl.userModel.userId,
                               @"userId": KgetUserValueByParaName(USERID)};
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
               
                _friendModel = [FriendModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.tableView reloadData];
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
               
            }
            
        } else {
            
        }
    }];
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return [PeopleCell cellHeight];
    FriendModel *model = _friendModel[indexPath.row];
    if (model.label_have.count != 0 && model.personalSign.length != 0) {
        return 100;
    }else{
        return 70;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   return _friendModel.count;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PersonHomeViewController * targetVC = [[PersonHomeViewController alloc] init];
    targetVC.userId = [_friendModel[indexPath.row].userId intValue];
    [self.parentContrl.parentCotr.navigationController pushViewController:targetVC animated:YES];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PeopleCell * cell = [tableView dequeueReusableCellWithIdentifier:kPeopleCell forIndexPath:indexPath];
    FriendModel *model = nil;
   
    model = _friendModel[indexPath.row];
   
    [cell configureCellWithFriendModel:model];
    
    return cell;
}


@end
