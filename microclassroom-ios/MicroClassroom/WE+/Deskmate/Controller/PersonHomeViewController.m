//
//  PersonHomeViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "PersonHomeViewController.h"
#import "PersonSegViewController.h"
#import "FriendMoreHandleVC.h"
#import "PeopleModel.h"
#import "UIImageView+AFNetworking.h"
#import "ChatViewController.h"
#import "BuyVIPViewController.h"
#import "UserInfoModel.h"

static NSString * const kUserVCTitle = @"个人主页";
static NSString * const kBtnTitleSendMsg = @"发送消息";
//static NSString * const kBtnTitleAgreeAdd = @"同意添加为同学";
static NSString * const kBtnTitleAgreeAdd = @"  同意添加";
//static NSString * const kBtnTitleAddFriend = @"请求添加为同学";
static NSString * const kBtnTitleAddFriend = @"  好友";
static NSString * const kUserVCMaleImageName = @"login_male_selected";
static NSString * const kUserVCFemaleImageName = @"login_female_selected";

@interface PersonHomeViewController ()
{
    OtherUserInfoModel * _userModel;
}
@property (weak, nonatomic) IBOutlet UIView *vcContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *genderImage;
@property (strong,nonatomic) UILabel *NoLabel;
@property (weak, nonatomic) IBOutlet UILabel *subNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *signLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelLeading;
@property (strong, nonatomic) PersonSegViewController * seg;
@property (nonatomic, assign) MCRelationType relationType;
@property (nonatomic, strong) NSArray * segmentTitleArr;
@property (nonatomic ,copy) NSString *addNoteStr;
@property (weak, nonatomic) IBOutlet UIButton *bChatBtn;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *minBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topH;

@end

@implementation PersonHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.yh_navigationItem.title = kUserVCTitle;
    //    self.NoLabel = [UILabel labelWithText:@"" andFont:14 andTextColor:DColor andTextAlignment:0];
    //    [self.view addSubview:self.NoLabel];
    //    [self.NoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.centerY.equalTo(self.nameLabel);
    //        make.left.equalTo(self.genderImage.mas_right).offset(10);
    //    }];
    
    self.bottomBtn.layer.cornerRadius = 4.0;
    self.bottomBtn.layer.masksToBounds = YES;
    self.signLabel.text = @"";
    self.subNameLabel.text = @"";
    [self setUpSegVC];
    // 删除好友重拉数据
    [self pullData];
//    self.genderImage.hidden = YES;
    self.bChatBtn.hidden = YES;
    self.topH.constant = KTopHeight;
}


- (void)setUpSegVC {
    
    self.seg = [[PersonSegViewController alloc] init];
    self.seg.parentCotr = self;
    [self.vcContainerView addSubview:self.seg.view];
    
    [self.seg.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.vcContainerView);
    }];
    
    self.seg.segTop = 0.0f;
    [self.view layoutIfNeeded];
    
}

- (void)setBaseUIByRelationType {
    
    switch (_relationType) {
        case MCRelationTypeFriend:
            self.bottomBtn.hidden = YES;
            self.chatBtn.hidden = YES;
            self.bChatBtn.hidden = NO;
//            [self.bottomBtn setTitle:kBtnTitleSendMsg forState:UIControlStateNormal];
            break;
        case MCRelationTypeWaitMe:
            self.bChatBtn.hidden = YES;
            self.bottomBtn.hidden = NO;
            self.chatBtn.hidden = NO;
            [self.bottomBtn setTitle:kBtnTitleAgreeAdd forState:UIControlStateNormal];
            break;
        case MCRelationTypeStranger: case MCRelationTypeWaitOther:
            self.bChatBtn.hidden = YES;
            self.bottomBtn.hidden = NO;
            self.chatBtn.hidden = NO;
            [self.bottomBtn setTitle:kBtnTitleAddFriend forState:UIControlStateNormal];
            break;
        default:
            self.bottomBtn.hidden = YES;
            self.chatBtn.hidden = YES;
            self.bChatBtn.hidden = YES;
            break;
    }
    
}

- (void)setTopViewData {
    
    switch (_relationType) {
        case MCRelationTypeFriend:
        {
            self.nameLabel.text = _userModel.noteName.length > 0 ? _userModel.noteName : _userModel.nickname;
            //            self.subNameLabel.text = _userModel.noteName.length > 0 ? _userModel.nickname : @"";
            self.titleLabel.text = _userModel.title;
            self.titleLabelLeading.constant = self.subNameLabel.text.length > 0 ? 10 : 0;
            //            if (NOEmptyStr(_userModel.userCode)) {
            //                self.NoLabel.text = [NSString stringWithFormat:@"NO.%@",_userModel.userCode];
            //            }
            
            @weakify(self);
            self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc] initWithTitle:@"..." style:0 handler:^(id send) {
                @strongify(self);
                
                FriendMoreHandleVC * targetVC = [[FriendMoreHandleVC alloc] init];
                targetVC.userModel = _userModel;
                
                [self.navigationController pushViewController:targetVC animated:YES];
                
            }];
            self.yh_navigationItem.rightBarButtonItem.button.titleLabel.font = [UIFont boldSystemFontOfSize:24];
            
        }
            break;
        case MCRelationTypeStranger: case MCRelationTypeWaitMe: case MCRelationTypeWaitOther:
            self.nameLabel.text = _userModel.nickname;
//            self.titleLabel.text = @"";
            self.titleLabel.text = _userModel.title;
            self.yh_navigationItem.rightBarButtonItem = nil;
            break;
        default:
            self.nameLabel.text = _userModel.nickname;
            self.titleLabel.text = _userModel.title;
            break;
    }
    
    self.signLabel.text = _userModel.personalSign;
    NSString *labelStr = @"";
    for (UserInfoSectionModel *model in _userModel.label_have) {
        if (labelStr.length == 0) {
            labelStr = model.codeName;
        }else{
            labelStr = [NSString stringWithFormat:@"%@ | %@",labelStr,model.codeName];
        }
    }
    if (labelStr.length != 0) {
        self.subNameLabel.text = labelStr;
    }else{
        self.subNameLabel.text = @"";
    }
    if (_userModel.isCertified == 0) {
        if ([_userModel.sex isEqualToString:@"男"]) {
            self.genderImage.image = [UIImage imageNamed:kUserVCMaleImageName];
        } else if ([_userModel.sex isEqualToString:@"女"]) {
            self.genderImage.image = [UIImage imageNamed:kUserVCFemaleImageName];
        } else {
            self.genderImage.image = nil;
        }
    }else{
        self.genderImage.image = [UIImage imageNamed:@"ic_vip"];
        
    }
    
    self.minBtn.hidden = !_userModel.wxUserAppId.length;
    
    __weak UIImageView *weak = self.avatarImage;
    [weak setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_userModel.photo]] placeholderImage:[UIImage imageNamed:@"login_head"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        weak.image=image;
        [weak setNeedsLayout];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
    }];
    
}

- (void)pullData {
    
    NSDictionary *paramDic = @{@"service": CircleGetUserInfo,
                               @"userId": KgetUserValueByParaName(USERID),
                               @"otherId": [NSString stringWithFormat:@"%d", self.userId],
                               @"userIdCtype":self.userIdCtype == nil ? @"0" : self.userIdCtype};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                _userModel = [OtherUserInfoModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                
                self.relationType = _userModel.status;
                
                self.seg.userModel = _userModel;
                
                [self setTopViewData];
                
                [self setBaseUIByRelationType];
                if ([_userModel.userId isEqualToString:KgetUserValueByParaName(USERID)]) {
                    self.bottomViewHeight.constant = 0;
                }
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}

- (void)addFriend {
    self.addNoteStr = [NSString stringWithFormat:@"我是%@",[UserInfoModel getInfoModel].nickname];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"添加验证" message:[NSString stringWithFormat:@"向%@发送好友请求",_userModel.nickname] preferredStyle:UIAlertControllerStyleAlert];
    
    @weakify(self)
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        @strongify(self)
        textField.text = self.addNoteStr;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTextFieldTextDidChangeNotification:) name:UITextFieldTextDidChangeNotification object:textField];
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:alert.textFields.firstObject];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:alert.textFields.firstObject];
        MyLog(@"备注信息 : %@", self.addNoteStr);
        NSDictionary *paramDic = @{@"service": CircleAddUserAsFriend,
                                   @"userId": KgetUserValueByParaName(USERID),
                                   @"toUserId": [NSString stringWithFormat:@"%d", self.userId],
                                   @"addNote":self.addNoteStr};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }else if ([result[REQUEST_CODE] isEqualToNumber:@30]) {
                    @weakify(self);
                    [YHJHelp aletWithTitle:@"提示" Message:result[REQUEST_MESSAGE] sureTitle:@"立即升级" CancelTitle:@"忽略" SureBlock:^{
                        @strongify(self);
                        BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                        [self.navigationController pushViewController:buy animated:YES];
                    } andCancelBlock:nil andDelegate:self];
                } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    
                }
                
            }
        }];
        
    }]];
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)agreeAddFriend {
    
    NSDictionary *paramDic = @{@"service": CircleConfirmAddAsFriend,
                               @"userId": KgetUserValueByParaName(USERID),
                               @"fromUserId": [NSString stringWithFormat:@"%d", self.userId]};
    
//    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
//        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                [WFHudView showMsg:@"添加成功" inView:nil];
                [self pullData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
}

- (void)handleTextFieldTextDidChangeNotification:(NSNotification *)notification {
    UITextField *textField = notification.object;
    self.addNoteStr = textField.text;
    if (IsEmptyStr(self.addNoteStr)) {
        self.addNoteStr = @"";
    }
    // Enforce a minimum length of >= 5 characters for secure text alerts.
}

#pragma mark - UIButton Functions
- (IBAction)bottomBtnAction:(UIButton *)sender {
    
    switch (_relationType) {
        case MCRelationTypeWaitMe:
            [self agreeAddFriend];
            return;
        case MCRelationTypeStranger: case MCRelationTypeWaitOther:
            [self addFriend];
            
            return;
        default:
            break;
    }
    
    FriendModel *model = [FriendModel new];
    model.userName = _userModel.nickname;
    ChatViewController * targetVC = [[ChatViewController alloc] initWithConversationChatter:_userModel.hxAccount.username conversationType:EMConversationTypeChat andData:model];
    [self.navigationController pushViewController:targetVC animated:YES];
    
}

- (IBAction)handleChatAction:(UIButton *)sender {
    FriendModel *model = [FriendModel new];
      model.userName = _userModel.nickname;
      ChatViewController * targetVC = [[ChatViewController alloc] initWithConversationChatter:_userModel.hxAccount.username conversationType:EMConversationTypeChat andData:model];
      [self.navigationController pushViewController:targetVC animated:YES];
}

- (IBAction)handleMinBtn:(UIButton *)sender {
    [[YHJHelp shareInstance] pushWXLaunchMiniProgramReq:_userModel.wxUserAppId];
}

@end

