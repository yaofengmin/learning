//
//  DeskmateViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/14.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "DeskmateViewController.h"
#import "PersonHomeViewController.h"
#import "UserInfoVC.h"
#import "PeopleCell.h"
#import "ChineseInclude.h"
#import "PinYinForObjc.h"
#import "PeopleModel.h"
#import "DBDataModel.h"
#import <MJRefresh.h>
#import "DIYSearchView.h"

static NSString * const kPeopleCell = @"PeopleCell";

@interface DeskmateViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UISearchBarDelegate,
UISearchDisplayDelegate,
filterAction>

{
    NSMutableArray <FriendModel *>* _friendModel;
    UISearchDisplayController *searchDisplayController;
    UISearchController        *searchController;
    NSMutableArray *searchResults;
}
@property (strong, nonatomic)  UITableView *tableView;
@property (strong, nonatomic)  NSLayoutConstraint *searchBarTop;
@property (strong, nonatomic)  UISearchBar *searchBar;
@property (nonatomic, strong)  DIYSearchView *headerView;
@property (nonatomic,assign) BOOL isSearch;

@end

@implementation DeskmateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    KAddobserverNotifiCation(@selector(pullData), DeskmateRefresh);
    self.view.backgroundColor = NCColor;
    [self setUpUI];
    
    [self initIvar];
    
    [self creatSearchV];
    
    [self setUpTableView];
    
    [self pullData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initIvar {
    
    _friendModel = [NSMutableArray arrayWithCapacity:1];
    
}

- (void)setUpUI {
    if (self.parentContrl) {
//        self.searchBarTop.constant = 0;
    } else {
//        self.searchBarTop.constant = KTopHeight;
        self.yh_navigationItem.title = @"我的同学";
    }
}

-(void)creatSearchV
{
    _headerView = [[DIYSearchView alloc]initWithSuperV:self filterHidden:YES];
    _headerView.placeholder = @"搜索";
    _headerView.delegate = self;
    _headerView.backgroundColor =  [UIColor whiteColor];
    [self.view addSubview:_headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.parentContrl) {
            make.top.mas_equalTo(0);
        }else{
            make.top.mas_equalTo(KTopHeight);
        }
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 50));
    }];
}

- (void)setUpTableView {
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableHeaderView = self.searchBar;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 1, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    if (self.parentContrl) {
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(50, 0, 0, 0));
        }];
    }else {
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(KTopHeight + 50, 0, 0, 0));
        }];
    }
    
    
    //    if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
    //        searchController = [[UISearchController alloc]initWithSearchResultsController:self];
    //    }else{
    //        searchDisplayController = [[UISearchDisplayController alloc]initWithSearchBar:self.searchBar contentsController:self];
    //        searchDisplayController.active = NO;
    //        searchDisplayController.searchResultsDataSource = self;
    //        searchDisplayController.searchResultsDelegate = self;
    //        searchDisplayController.delegate = self;
    //    }
    
    
    //    [self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:kPeopleCell bundle:nil]
    //                                              forCellReuseIdentifier:kPeopleCell];
    [self.tableView registerNib:[UINib nibWithNibName:kPeopleCell bundle:nil] forCellReuseIdentifier:kPeopleCell];
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader  headerWithRefreshingBlock:^{
        @strongify(self);
        [self pullData];
    }];
}

- (void)pullData {
    
    NSDictionary *paramDic = @{@"service": CircleGetFriendList,
                               @"type": @(1),
                               @"userId": KgetUserValueByParaName(USERID)};
    
//    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
//        HIDENHUD;
        [self initIvar];
        //业务逻辑层
        [self.tableView.mj_header endRefreshing];
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                for (id item in result[REQUEST_LIST]) {
                    [_friendModel addObject:[FriendModel mj_objectWithKeyValues:item]];
                }
                
                [[DBDataModel sharedDBDataModel] createDataTable_Friend];
                [[DBDataModel sharedDBDataModel] addAllRowData_Friend:_friendModel];
                
                [self.tableView reloadData];
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
//                [self pullDataFromDB];
            }
            
        } else {
            
            [self pullDataFromDB];
        }
    }];
    
}

- (void)deleteFriend:(NSIndexPath*)index {
    
    NSDictionary *paramDic = @{@"service": CircleDeleteFriend,
                               @"friendId": _friendModel[index.row].userId,
                               @"userId": KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                [[DBDataModel sharedDBDataModel] deleteFriend:_friendModel[index.row]];
                [_friendModel removeObjectAtIndex:index.row];
                [self.tableView deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationTop];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        } else {
            [WFHudView showMsg:@"删除失败，请重试" inView:nil];
        }
    }];
    
}


- (void)pullDataFromDB {
    
    NSArray * dbArr = [[DBDataModel sharedDBDataModel] readDataFromTableDistinctstatus][0];
    
    if (dbArr.count > 0) {
        
        for (id arr in dbArr) {
            
            for (id item in arr) {
                
                [_friendModel addObject:[FriendModel mj_objectWithKeyValues:item]];
            }
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.isSearch) {
        return searchResults.count;
    }else{
        return _friendModel.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PeopleCell * cell = [tableView dequeueReusableCellWithIdentifier:kPeopleCell forIndexPath:indexPath];
    FriendModel *model = nil;
    if (self.isSearch) {
        model = searchResults[indexPath.row];
    }else{
        model = _friendModel[indexPath.row];
    }
    
    [cell configureCellWithFriendModel:model];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendModel *model = nil;
    if (self.isSearch) {
        model = searchResults[indexPath.row];
    }else{
        model = _friendModel[indexPath.row];
    }
    if (model.label_have.count != 0 && model.personalSign.length != 0) {
        return 100;
    }else{
        return 70;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PersonHomeViewController * targetVC = [[PersonHomeViewController alloc] init];
    if (self.isSearch) {
        FriendModel *model = searchResults[indexPath.row];
        targetVC.userId = [model.userId intValue];
    }else{
        targetVC.userId = [_friendModel[indexPath.row].userId intValue];
    }
    if (self.parentContrl) {
        [self.parentContrl.navigationController pushViewController:targetVC animated:YES];
    } else {
        [self.navigationController pushViewController:targetVC animated:YES];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
}

- (void)searchEndWithText:(NSString *)searchText
{
    self.isSearch = YES;
    searchResults = @[].mutableCopy;
    if (searchText.length > 0 && ![ChineseInclude isIncludeChineseInString:searchText]) {
        for (int i = 0; i< _friendModel.count; i++) {
            if ([ChineseInclude isIncludeChineseInString:[_friendModel[i] userName]] || [ChineseInclude isIncludeChineseInString:[_friendModel[i] noteName]]||[[_friendModel[i] userName] rangeOfString:searchText].location !=NSNotFound) {
                NSString *tempPinYinStr = [PinYinForObjc chineseConvertToPinYin:[_friendModel[i] userName]];
                NSString *notePin = [PinYinForObjc chineseConvertToPinYin:[_friendModel[i] noteName]];
                NSRange titleResult=[tempPinYinStr rangeOfString:searchText options:NSCaseInsensitiveSearch];
                NSRange noteResult = [notePin rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if (titleResult.length>0 || noteResult.length >0) {
                    [searchResults addObject:_friendModel[i]];
                }
            }
        }
        
    }else if(searchText.length > 0 && [ChineseInclude isIncludeChineseInString:searchText]){
        
        for (FriendModel *model in _friendModel) {
            NSString *name = model.userName;
            NSString *note = model.noteName;
            NSRange resultName = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange resultNote = [note rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (resultName.length>0 || resultNote.length >0) {
                [searchResults addObject:model];
            }
        }
    }else if (searchText.length == 0){
        self.isSearch = NO;
    }
    [self.tableView reloadData];
}

- (void)cancelSearchAction{
    self.isSearch = NO;
    [self.tableView reloadData];
}

//- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
//{
//    controller.searchResultsTableView.contentInset = UIEdgeInsetsMake(0.0f, 0.f, 0.f, 0.f);
//    if (self.parentContrl) {
//         tableView.frame = CGRectMake(0,50,KScreenWidth,self.view.viewHeight - 50); //
//    }else{
//        tableView.frame = CGRectMake(0,KTopHeight + 50,KScreenWidth, KScreenHeight - KTopHeight - 50); //
//    }
//    tableView.tableFooterView = [[UIView alloc]init];
//}

//- (UISearchBar *)searchBar {
//    if (!_searchBar) {
//        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
//        _searchBar.delegate = self;
//        _searchBar.placeholder = @"搜索";
//        UIImage* searchBarBg = [YHJHelp GetImageWithColor:[UIColor whiteColor] andHeight:40];
//        //设置背景图片
//        [_searchBar setBackgroundImage:searchBarBg];
//        //设置背景色
//        [_searchBar setBackgroundColor:[UIColor whiteColor]];
//        //设置文本框背景
//        [self setSearchTextFieldBackgroundColor:NCColor];
//        UITextField * searchField = [self sa_GetSearchTextFiled];
//        searchField.font = Font(12);
//        searchField.textColor = JColor;
//    }
//    return _searchBar;
//}

//- (UITextField *)sa_GetSearchTextFiled{
//    if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
//        return _searchBar.searchTextField;
//    }else{
////        UITextField *searchTextField =  [self valueForKey:@"_searchField"];
////        return searchTextField;
//        return self.searchBar.subviews.firstObject.subviews.lastObject;
//    }
//}

//- (void)setSearchTextFieldBackgroundColor:(UIColor *)backgroundColor
//{
//    UIView *searchTextField = nil;
//    if (IOS7) {
//        searchTextField = [[[self.searchBar.subviews firstObject] subviews] lastObject];
//    }
//    searchTextField.backgroundColor = backgroundColor;
//}

@end
