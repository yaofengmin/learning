//
//  UserViewController.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/14.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"
#import "PersonSegViewController.h"

@class OtherUserInfoModel;
@interface UserInfoVC : YHBaseViewController
@property (nonatomic, assign) OtherUserInfoModel * userModel;
@property (nonatomic, weak) PersonSegViewController *parentContrl;

@end
