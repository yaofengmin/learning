//
//  OrgJoinCollectionViewCell.h
//  partyiOS
//
//  Created by 姚凤敏 on 2019/8/8.
//  Copyright © 2019 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrgJoinCollectionViewCell : UICollectionViewCell

- (void)configModel:(id )cellModel;

@end

NS_ASSUME_NONNULL_END
