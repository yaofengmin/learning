//
//  CheckListTableViewCell.m
//  MicroClassroom
//
//  Created by 姚凤敏 on 2019/12/19.
//  Copyright © 2019 Hanks. All rights reserved.
//

#import "CheckListTableViewCell.h"

#import "OrgJoinCollectionViewCell.h"

#import "PeopleModel.h"
#import "PersonHomeViewController.h"
#import "UserInfoVC.h"

static NSString *kHomeItemColCellID   = @"HomeItemCollectionViewCell";

@interface CheckListTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UICollectionView *collectionV;

@property (nonatomic,strong) NSArray *dataArr;

@end

@implementation CheckListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self creatSub];
    }
    return self;
}

- (void)configWithModel:(NSArray *)cellModel{
    self.dataArr = cellModel;
    [self.collectionV reloadData];
}

- (void)creatSub
{
    [self.contentView addSubview:self.collectionV];
    [self.collectionV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (UICollectionView *)collectionV{
    if (!_collectionV) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0.0;
        layout.minimumInteritemSpacing = 0.0;
        _collectionV = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionV.backgroundColor = [UIColor whiteColor];
        _collectionV.delegate = self;
        _collectionV.dataSource = self;
        _collectionV.scrollEnabled = NO;
        [_collectionV registerClass:[OrgJoinCollectionViewCell class] forCellWithReuseIdentifier:kHomeItemColCellID];
    }
    return _collectionV;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    OrgJoinCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kHomeItemColCellID forIndexPath:indexPath];
    [cell configModel:self.dataArr[indexPath.item]];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SchoolMateModel *model = self.dataArr[indexPath.item];
    PersonHomeViewController *vc = [[PersonHomeViewController alloc]init];
    vc.userId = model.userId;
    UserInfoVC *infoVC = (UserInfoVC *)self.viewController;
    [infoVC.parentContrl.parentCotr.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (IPHONE_6) {
        return CGSizeMake(KScreenWidth / 5.0, 50);
    }else{
        return CGSizeMake(KScreenWidth/ 6.0 ,50);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

+ (CGFloat)cellHeight{
    return 50;
}

@end
