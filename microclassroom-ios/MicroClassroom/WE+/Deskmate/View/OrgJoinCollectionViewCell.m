//
//  OrgJoinCollectionViewCell.m
//  partyiOS
//
//  Created by 姚凤敏 on 2019/8/8.
//  Copyright © 2019 iOS. All rights reserved.
//

#import "OrgJoinCollectionViewCell.h"

#import "PeopleModel.h"

@interface OrgJoinCollectionViewCell ()

@property (nonatomic,strong) UILabel *titleLbl;

@end

@implementation OrgJoinCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatSub];
    }
    return self;
}

- (void)configModel:(SchoolMateModel *)cellModel{
    self.titleLbl.text = cellModel.userName;
}

- (void)creatSub
{
    
    [self.contentView addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 3, 0, 3));
    }];

    
}

- (UILabel *)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel labelWithText:@"宁雨" andFont:14 andTextColor:KColorFromRGB(0x696969) andTextAlignment:1];
    }
    return _titleLbl;
}

@end
