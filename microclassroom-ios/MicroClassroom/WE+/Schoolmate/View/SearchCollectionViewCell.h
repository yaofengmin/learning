//
//  SearchCollectionViewCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchCollectionViewCellDelegate <NSObject>



@end

@interface SearchCollectionViewCell : UICollectionViewCell

@property (strong,nonatomic) NSIndexPath *indextPath;
@property (weak, nonatomic) IBOutlet UIButton *tipsBtn;
- (IBAction)tipsBtnDown:(UIButton *)sender;

@end
