//
//  SearchCategoryHeadView.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCategoryHeadView : UICollectionReusableView

@property(nonatomic, copy) NSString *title;

@end
