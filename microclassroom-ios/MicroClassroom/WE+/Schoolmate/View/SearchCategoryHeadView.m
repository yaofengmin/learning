//
//  SearchCategoryHeadView.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "SearchCategoryHeadView.h"

@interface SearchCategoryHeadView()

@property(nonatomic ,strong) UILabel *titleLabel;

@end

@implementation SearchCategoryHeadView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _titleLabel = [UILabel labelWithText:@"端头" andFont:12 andTextColor:CColor andTextAlignment:0];
        
        [self addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(20);
            make.centerY.equalTo(0);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = LColor;
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(0);
            make.height.equalTo(0.5);
        }];
    }
    return self;
}


-(void)setTitle:(NSString *)title {
    _titleLabel.text = title;
}


@end
