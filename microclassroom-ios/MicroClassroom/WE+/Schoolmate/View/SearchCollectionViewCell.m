//
//  SearchCollectionViewCell.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "SearchCollectionViewCell.h"

@implementation SearchCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    _tipsBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    _tipsBtn.userInteractionEnabled = NO;
    _tipsBtn.titleLabel.font = Font(kItemFont);
    _tipsBtn.layer.cornerRadius = 4.0;
    [_tipsBtn setTitleColor:BColor forState:UIControlStateSelected];
    // Initialization code
}

- (IBAction)tipsBtnDown:(UIButton *)sender {
   
}

@end
