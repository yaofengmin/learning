//
//  SeacherTipListModel.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/19.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "SeacherTipListModel.h"

@implementation SeacherItemModel

@end

@implementation SeacherTipListModel

+(NSDictionary*)mj_objectClassInArray {
    return @{@"list":@"SeacherItemModel"};
}

@end
