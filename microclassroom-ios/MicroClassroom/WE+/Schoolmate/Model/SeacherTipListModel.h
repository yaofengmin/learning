//
//  SeacherTipListModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/19.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"


@interface SeacherItemModel : YHBaseModel
@property(nonatomic, copy) NSString *codeId;
@property(nonatomic, copy) NSString *codeName;
@property (nonatomic,assign)BOOL    state;
@property(nonatomic ,copy)NSString  *pareName;
@end

@interface SeacherTipListModel : YHBaseModel
@property(nonatomic ,copy)NSString  *pareName;
@property(nonatomic ,copy)NSString  *pareLable;
@property(nonatomic ,strong)NSArray *list;
@end
