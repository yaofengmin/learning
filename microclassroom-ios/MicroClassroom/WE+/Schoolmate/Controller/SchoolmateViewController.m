//
//  SchoolmateViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/13.
//  Copyright © 2016年 Hanks. All rights reserved.
// 同窗
#import <MJRefresh.h>

#import "SchoolmateViewController.h"
#import "PersonHomeViewController.h"
#import "UserInfoVC.h"
#import "PeopleCell.h"
#import "PeopleModel.h"
#import "CategoryScreenVC.h"

#import "UserInfoModel.h"

static NSString * const kPeopleCell = @"PeopleCell";

@interface SchoolmateViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UISearchBarDelegate
>

{
    NSMutableArray <SchoolMateModel *>* _mateModel;
    int _pageIndex;
    int _totalCount;
    
}
@property (strong, nonatomic)  UIView *topView;
@property (strong, nonatomic)  UITableView *tableView;
@property (strong, nonatomic)  CategoryScreenVC *slidebarVC;
@property (strong, nonatomic)  UISearchBar *mySearchBar;
@property (copy ,nonatomic)    NSString *keyWords;
@property (strong, nonatomic)  NSMutableDictionary *paramDic;
@property (nonatomic ,copy)    NSString *label_haveObId;
@property (nonatomic ,copy)    NSString *label_needObId;

@property (nonatomic ,strong) UIButton *fitterBtn;
@end

@implementation SchoolmateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.label_haveObId = @"";
    self.label_needObId = @"";
    [self initIvar];
    [self setUpTableView];
    _paramDic = @{Service: CircleGetUserList,
                  @"userId":KgetUserValueByParaName(USERID),
                  @"pageSize": @(20)
                  }.mutableCopy;
    //    [self initalLabelId];//后期如果需要自动匹配用户的需求资源,可打开
    [self pullData];
    KAddobserverNotifiCation(@selector(isHiddenFitterBtn), NOTIGICATIONFINDFITERBUTTON);
}
#pragma mark === 初始化 标签默认选择
-(void)initalLabelId
{
    //资源
    NSArray *label_have = [UserInfoModel getInfoModel].label_have;
    
    for (NSDictionary *havcDic in label_have) {
        NSString *label_haveCode = [havcDic objectForSafeKey:@"codeId"];
        if (self.label_haveObId.length == 0) {
            self.label_haveObId = [NSString stringWithFormat:@"%@", KgetUserValueByParaName(label_haveCode)];
        }else{
            self.label_haveObId = [NSString stringWithFormat:@"%@,%@", self.label_haveObId, KgetUserValueByParaName(label_haveCode)];
        }
    }
    [self.paramDic setObject:self.label_haveObId forKey:@"labelNeedId"];//需求和资源是对立的,
    //    NSDictionary *havcDic = label_have.firstObject;
    //    NSString *label_haveCode = [havcDic objectForSafeKey:@"codeId"];
    //
    //    if (label_haveCode.length != 0) {
    //        MyLog(@"%@",KgetUserValueByParaName(@"81"));
    //        self.label_haveObId = [NSString stringWithFormat:@"%@", KgetUserValueByParaName(label_haveCode)];
    //        [self.paramDic setObject:self.label_haveObId forKey:@"labelNeedId"];//需求和资源是对立的,
    //    }else{
    //        self.label_haveObId = @"";
    //    }
    
    //需求
    NSArray *label_need = [UserInfoModel getInfoModel].label_need;
    for (NSDictionary *needDic in label_need) {
        NSString *label_needId = [needDic objectForSafeKey:@"codeId"];
        if (self.label_needObId.length == 0) {
            self.label_needObId = [NSString stringWithFormat:@"%@",KgetUserValueByParaName(label_needId)];
        }else{
            self.label_needObId = [NSString stringWithFormat:@"%@,%@",self.label_needObId, KgetUserValueByParaName(label_needId)];
        }
    }
    [self.paramDic setObject:self.label_needObId forKey:@"labelHaveId"];
    
    //    NSDictionary *needDic = label_need.firstObject;
    //    NSString *label_needId = [needDic objectForSafeKey:@"codeId"];
    //    if (label_needId.length != 0) {
    //        self.label_needObId = [NSString stringWithFormat:@"%@",KgetUserValueByParaName(label_needId)];
    //        [self.paramDic setObject:self.label_needObId forKey:@"labelHaveId"];
    //    }else{
    //        self.label_needObId = @"";
    //    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initIvar {
    _pageIndex = 1;
    _totalCount = 0;
    _mateModel = [NSMutableArray arrayWithCapacity:1];
}

- (void)setUpTableView {
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableHeaderView = self.topView;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefreshAction)];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self
                                                                    refreshingAction:@selector(footerRefresh)];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:kPeopleCell bundle:nil]
         forCellReuseIdentifier:kPeopleCell];
}

#pragma mark - 显示侧滑
- (void)screenOutBrnDown:(id)sender {
    [self.view endEditing:YES];
    if (!_slidebarVC) {
        _slidebarVC = [[CategoryScreenVC alloc] initWithSearchType:YHSearchTypeMate];
        _slidebarVC.view.frame  =  self.tableView.frame;
        [self.view addSubview:_slidebarVC.view];
        
        @weakify(self);
        _slidebarVC.clearClick = ^{
            @strongify(self);
            self.paramDic = @{Service: CircleGetUserList,
                              @"userId":KgetUserValueByParaName(USERID),
                              @"pageSize": @(20)
                              }.mutableCopy;
            [self pullData];
        };
        
        
        _slidebarVC.tipsFinishClick = ^(NSArray<SeacherItemModel *> * selectArr){
            @strongify(self);
            NSString *labelHaveId = @"";
            NSString *labelNeedId = @"";
            for (SeacherItemModel *mode in selectArr) {
                
                if ([mode.pareName isEqualToString:@"labelHaveId"]) {//labelHaveId
                    if (labelHaveId.length == 0) {
                        labelHaveId = mode.codeId;
                    }else{
                        labelHaveId = [NSString stringWithFormat:@"%@,%@",labelHaveId,mode.codeId];
                    }
                }else if ([mode.pareName isEqualToString:@"labelNeedId"]) {//labelNeedId
                    if (labelNeedId.length == 0) {
                        labelNeedId = mode.codeId;
                    }else{
                        labelNeedId = [NSString stringWithFormat:@"%@,%@",labelNeedId,mode.codeId];
                    }
                }else{
                    
                    [self.paramDic setObject:mode.codeId forKey:mode.pareName];
                }
                
            }
            if (labelHaveId.length != 0) {
                [self.paramDic setObject:labelHaveId forKey:@"labelHaveId"];
            }
            if (labelNeedId.length != 0) {
                [self.paramDic setObject:labelNeedId forKey:@"labelNeedId"];
            }
            [self pullData];
        };
        
        
        _slidebarVC.cancelTip = ^(SeacherItemModel *mode){
            @strongify(self);
            [self.paramDic removeObjectForKey:mode.pareName];
            [self pullData];
        };
        
    }
    
    [_slidebarVC showHideSidebar];
}

- (void)headRefreshAction {
    [_mateModel removeAllObjects];
    _pageIndex = 1;
    [self pullData];
}

- (void)footerRefresh {
    _pageIndex += 1;
    [self pullData];
}

- (void)pullData {
    
    [self.paramDic setObject:@(_pageIndex) forKey:@"pageIndex"];
    [self.paramDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:self.paramDic andBlocks:^(NSDictionary *result) {
        //业务逻辑层
        if (_pageIndex == 1) {
            [_mateModel removeAllObjects];
            [self.tableView.mj_header endRefreshing];
        }else{
            [self.tableView.mj_footer endRefreshing];
        }
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                self.tableView.mj_footer.hidden = NO;
                _totalCount = [result[REQUEST_LIST][@"total"] intValue];
                
                for (id item in result[REQUEST_LIST][REQUEST_LIST]) {
                    [_mateModel addObject:[SchoolMateModel mj_objectWithKeyValues:item]];
                }
                
                [self.tableView.mj_footer endRefreshing];
                
                self.tableView.mj_footer.hidden = _mateModel.count == _totalCount;
                
                [self.tableView reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
        
        if (_mateModel.count == _totalCount) {
            self.tableView.mj_footer.hidden = YES;
        }else{
            self.tableView.mj_footer.hidden = NO;
        }
    }];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mateModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PeopleCell * cell = [tableView dequeueReusableCellWithIdentifier:kPeopleCell forIndexPath:indexPath];
    
    [cell configureCellWithSchoolMateModel:_mateModel[indexPath.row]];
    
    return cell;
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    SchoolMateModel *model =  _mateModel[indexPath.row];
    
    //    if (IsEmptyStr(model.personalSign)) {
    //        return [PeopleCell cellHeight] - 10;
    //
    //    }else {
    //        return [PeopleCell cellHeight];
    //    }
    if (model.label_have.count != 0 && model.personalSign.length != 0) {
        return 100;
    }else{
        return 70;
    }
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsMake(0, 60, 0, 0)];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsMake(0, 60, 0, 0)];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PersonHomeViewController * targetVC = [[PersonHomeViewController alloc] init];
    targetVC.userId = _mateModel[indexPath.row].userId;
    [self.parentContrl.navigationController pushViewController:targetVC animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [_mateModel removeAllObjects];
    if (searchText == nil) {
        searchText = @"";
    }
    [self.paramDic setObject:searchText forKey:@"keyWord"];
    [self pullData];
}




- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 44)];
        NSString *brandId = KgetUserValueByParaName(KBrandInfoId);
        if ([brandId integerValue] > 0) {
            _mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 44)];
        }else{
            _mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth- 44, 44)];
        }
        _mySearchBar.showsCancelButton = NO;
        _mySearchBar.delegate = self;
        _mySearchBar.backgroundColor = BColor;
        _mySearchBar.showsCancelButton = NO;
        _mySearchBar.delegate = self;
        _mySearchBar.placeholder = @"搜索";
        UITextField * searchField = [self sa_GetSearchTextFiled];
        searchField.font = Font(12);
        searchField.textColor = JColor;
        [_topView addSubview:_mySearchBar];
        
        UIImage* searchBarBg = [YHJHelp GetImageWithColor:[UIColor clearColor] andHeight:40];
        //设置背景图片
        [_mySearchBar setBackgroundImage:searchBarBg];
        //设置背景色
        [_mySearchBar setBackgroundColor:[UIColor clearColor]];
        //设置文本框背景
        [self setSearchTextFieldBackgroundColor:NCColor];
        _fitterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_fitterBtn setImage:[UIImage imageNamed:@"ic_quanbufenlei"] forState:UIControlStateNormal];
        _fitterBtn.titleLabel.font = Font(14);
        [_fitterBtn addTarget:self action:@selector(screenOutBrnDown:) forControlEvents:UIControlEventTouchUpInside];
        [_fitterBtn setTitleColor:CColor forState:UIControlStateNormal];
        [_topView addSubview:_fitterBtn];
        _fitterBtn.frame = CGRectMake(KScreenWidth - 40, 0, 40, 44);
        if ([brandId integerValue] > 0) {
            _fitterBtn.hidden = YES;
        }else{
            _fitterBtn.hidden = NO;
        }
    }
    return _topView;
}

- (UITextField *)sa_GetSearchTextFiled{
    if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
        return _mySearchBar.searchTextField;
    }else{
//        UITextField *searchTextField =  [self valueForKey:@"_searchField"];
//        return searchTextField;
        return _mySearchBar.subviews.firstObject.subviews.lastObject;
    }
}


- (void)setSearchTextFieldBackgroundColor:(UIColor *)backgroundColor
{
    UIView *searchTextField = nil;
    if (IOS7) {
        searchTextField = [[[_mySearchBar.subviews firstObject] subviews] lastObject];
    }
    searchTextField.backgroundColor = backgroundColor;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

-(void)isHiddenFitterBtn
{
    NSString *brandId = KgetUserValueByParaName(KBrandInfoId);
    if (_mySearchBar != nil) {
        if ([brandId integerValue] > 0) {
            _mySearchBar.frame = CGRectMake(0, 0, KScreenWidth, 44);
        }else{
            _mySearchBar.frame = CGRectMake(0, 0, KScreenWidth- 44, 44);
        }
    }
    if (_fitterBtn != nil) {
        if ([brandId integerValue] > 0) {
            _fitterBtn.hidden = YES;
        }else{
            _fitterBtn.hidden = NO;
        }
    }
}
- (void)dealloc
{
    KRemoverNotifi(NOTIGICATIONFINDFITERBUTTON);
}

@end
