//
//  CategoryScreenVC.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "CategoryScreenVC.h"
#import "SearchCollectionViewCell.h"
#import "SearchCategoryHeadView.h"

#import "UserInfoModel.h"

@interface CategoryScreenVC ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout
>
@property (nonatomic ,strong) UICollectionView *collection;
@property (nonatomic ,assign) YHSearchType type;
@property (nonatomic ,strong) NSArray<SeacherTipListModel*> *resultArr;
@property (nonatomic ,strong) UIButton *clearBtn;

//选中的每个类别的Arr
@property   (nonatomic,strong)NSMutableArray    *selectArr;

@property (nonatomic ,copy) NSString *label_haveObName;//每次刚进入当前界面需要默认用户当前的标签
@property (nonatomic ,copy) NSString *label_needObName;//需求


@property (nonatomic ,strong) NSIndexPath *haveIndexPath;

@property (nonatomic ,strong) NSIndexPath *needIndexPath;

@end

@implementation CategoryScreenVC
{
    SeacherItemModel     *model;
}
-(instancetype)initWithSearchType:(YHSearchType)type {
    if (self = [super init]) {
        _type = type;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.label_haveObName = @"";
    self.label_needObName = @"";
    self.view.backgroundColor = [MainBackColor colorWithAlphaComponent:0.1];
    // self.contentView.frame = CGRectMake(0, 0, kSidebarWidth , KScreenHeight - KTopHeight  - KTabBarHeight - SEGMENTHEIGHT);
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.top.bottom.right.equalTo(self.view);
        make.width.equalTo(kSidebarWidth);
    }];
    _selectArr = [[NSMutableArray alloc]init];
    [self.contentView addSubview:self.collection];
    [self.collection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 50, 0));
    }];
    
    [self.contentView addSubview:self.clearBtn];
    [self.clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collection.mas_bottom);
        make.left.right.bottom.equalTo(self.contentView);
    }];
    [self requestTips];
    // Do any additional setup after loading the view.
}


-(void)requestTips {
    //ctype 1=江湖；2=课间；
    NSDictionary *paramDic = @{Service:AppGetSeacherCodes,
                               @"ctype":@(_type)
                               };
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _resultArr = [SeacherTipListModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST][@"seacher_list"]];
//                [self initalLabelId];
                [_collection reloadData];
            }
        }
    }];
}
#pragma mark === 初始化 标签默认选择
-(void)initalLabelId
{
    //资源
    NSArray *label_have = [UserInfoModel getInfoModel].label_have;
    for (NSDictionary *havcDic in label_have) {
        NSString *label_haveCode = [havcDic objectForSafeKey:@"codeId"];
        if (self.label_haveObName.length == 0) {
            self.label_haveObName = [NSString stringWithFormat:@"%@", KgetUserValueByParaName(label_haveCode)];
        }else{
            self.label_haveObName = [NSString stringWithFormat:@"%@,%@", self.label_haveObName, KgetUserValueByParaName(label_haveCode)];
        }
    }
    //    NSDictionary *havcDic = label_have.firstObject;
    //    NSString *label_haveCode = [havcDic objectForSafeKey:@"codeId"];
    //    if (label_haveCode.length != 0) {
    //        MyLog(@"%@",KgetUserValueByParaName(@"81"));
    //        self.label_haveObName = [NSString stringWithFormat:@"%@", KgetUserValueByParaName(label_haveCode)];
    //    }else{
    //
    //        self.label_haveObName = @"";
    //    }
    
    //需求
    NSArray *label_need = [UserInfoModel getInfoModel].label_need;
    for (NSDictionary *needDic in label_need) {
        NSString *label_needId = [needDic objectForSafeKey:@"codeId"];
        if (self.label_needObName.length == 0) {
            self.label_needObName = [NSString stringWithFormat:@"%@",KgetUserValueByParaName(label_needId)];
        }else{
            self.label_needObName = [NSString stringWithFormat:@"%@,%@",self.label_needObName, KgetUserValueByParaName(label_needId)];
            
        }
    }
    //    NSDictionary *needDic = label_need.firstObject;
    //    NSString *label_needId = [needDic objectForSafeKey:@"codeId"];
    //    if (label_needId.length != 0) {
    //        self.label_needObName = [NSString stringWithFormat:@"%@",KgetUserValueByParaName(label_needId)];
    //    }else{
    //        self.label_needObName = @"";
    //    }
    
    for (int i = 0; i < _resultArr.count; i ++) {
        SeacherTipListModel *clickSectionModel = _resultArr[i];
        for (int j = 0; j < clickSectionModel.list.count; j ++) {
            SeacherItemModel *clickModel = clickSectionModel.list[j];
            if (self.label_haveObName.length != 0) {
                NSArray *label_Have = [self.label_haveObName componentsSeparatedByString:@","];
                for (NSString *codeId in label_Have) {
                    if ([clickModel.codeId isEqualToString:codeId]) {
                        [self collectionView:self.collection didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]];
                    }
                }
            }
            if (self.label_needObName.length != 0) {
                NSArray *label_Need = [self.label_needObName componentsSeparatedByString:@","];
                for (NSString *codeId in label_Need) {
                    if ([clickModel.codeId isEqualToString:codeId]) {
                        [self collectionView:self.collection didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]];
                    }
                    
                }
            }
        }
        
    }
    
}

//required
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIDa = @"SearchCollectionViewCell";
    SearchCollectionViewCell  *collectionCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIDa forIndexPath:indexPath];
    collectionCell.tipsBtn.layer.cornerRadius = 4.0;
    collectionCell.tipsBtn.layer.borderColor = KColorFromRGB(0xffa7a6) .CGColor;
    collectionCell.tipsBtn.layer.borderWidth = 1;
    [collectionCell.tipsBtn setTitleColor:kNavigationBarColor forState:UIControlStateSelected];
    collectionCell.indextPath = indexPath;
    SeacherItemModel *newModel =_resultArr[indexPath.section].list[indexPath.row];
    [collectionCell.tipsBtn setTitle:newModel.codeName  forState:UIControlStateNormal];
    collectionCell.tipsBtn.selected = newModel.state;
    [collectionCell tipsBtnDown:collectionCell.tipsBtn];
    return collectionCell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _resultArr[section].list.count;
}

//optional
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _resultArr.count;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = @"header";
    SearchCategoryHeadView *cell = nil;
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        cell = (SearchCategoryHeadView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
    }
    cell.title = _resultArr[indexPath.section].pareLable;
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.contentView.viewWidth, 30);
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(60, 30);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SeacherTipListModel *clickSectionModel = _resultArr[indexPath.section];
    SeacherItemModel *clickModel = clickSectionModel.list[indexPath.row];
    clickModel.pareName = clickSectionModel.pareName;
    if (clickModel.state) {
        clickModel.state = !clickModel.state;
        [_selectArr removeObject:clickModel];
        if (self.cancelTip) {
            self.cancelTip(clickModel);
        }
    }else{
        if (indexPath.section == 0) {
            for (SeacherItemModel *newModel in clickSectionModel.list) {//遍历数组将所有model 选中状态置为no
                if (newModel.state == YES) {
                    [_selectArr removeObject:newModel];
                    newModel.state = NO;
                    break;
                }
            }
        }
        
        [_selectArr addObject:clickModel];
        clickModel.state = !clickModel.state;
        //传递值
        if (self.tipsFinishClick) {
            self.tipsFinishClick(_selectArr);
        }
    }
    
    [_collection reloadData];
}


- (UICollectionView *)collection {
    if (!_collection) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        //设置对齐方式
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        //cell间距
        layout.minimumInteritemSpacing = 10.0f;
        //cell行距
        layout.minimumLineSpacing = 10.0f;
        _collection = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collection.delegate = self;
        _collection.dataSource = self;
        _collection.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
        [_collection registerClass:[SearchCategoryHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
        [_collection registerNib:[UINib nibWithNibName:@"SearchCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SearchCollectionViewCell"];
    }
    return _collection;
}



- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)sectio{
    
    return UIEdgeInsetsMake(10, 10, 10, 10);// top left bottom right  Cell边界范围
}

- (UIButton *)clearBtn {
    if (!_clearBtn) {
        _clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_clearBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_clearBtn setTitle:@"重置" forState:UIControlStateNormal];
        _clearBtn.titleLabel.font = Font(14);
        _clearBtn.backgroundColor = kNavigationBarColor;
        [_clearBtn addTarget:self action:@selector(clearBtnDown) forControlEvents:UIControlEventTouchUpInside];
    }
    return _clearBtn;
}

-(void)clearBtnDown {
    if (self.clearClick) {
        self.clearClick();
    }
    
    for (SeacherItemModel *clickModel in _selectArr) {
        clickModel.state = NO;
    }
    [_selectArr removeAllObjects];
    [self.collection reloadData];
}


@end
