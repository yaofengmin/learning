//
//  CategoryScreenVC.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "CustomSlideBar.h"
#import "SeacherTipListModel.h"
@interface CategoryScreenVC : CustomSlideBar

-(instancetype)initWithSearchType:(YHSearchType)type;
//添加
@property (nonatomic ,copy) void(^tipsFinishClick)(NSArray<SeacherItemModel *> * selectArr);
//移除
@property (nonatomic, copy) void(^cancelTip)(SeacherItemModel *mode);
//取消
@property (nonatomic ,copy) void(^clearClick)();
@end
