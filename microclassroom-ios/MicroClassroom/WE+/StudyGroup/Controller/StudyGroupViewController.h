//
//  StudyGroupViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/18.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHCirleBaseViewController.h"

#import "WeViewController.h"

@interface StudyGroupViewController : YHCirleBaseViewController
@property(nonatomic, weak) WeViewController *parentContrl;
@property (nonatomic,assign) GetMessagesType type;

@end
