//
//  YHBaseWebViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/28.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "FileWebViewController.h"

@interface FileWebViewController ()<UIWebViewDelegate>

@property (nonatomic ,copy) NSString *NavTitle;


@end

@implementation FileWebViewController

-(instancetype)initWithUrlStr:(NSString *)urlStr andNavTitle:(NSString *)navTitle

{
    if (self= [super init]) {
        _urlStr = urlStr;
        _NavTitle = navTitle;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.yh_navigationItem.title = _NavTitle;
    
    _web = [[UIWebView alloc]init];
    _web.scrollView.bounces = NO;
    _web.delegate   = self;
    _web.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_web];
    [_web  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if([_urlStr hasSuffix:@"txt"]){
        NSString * htmlstr = [[NSString alloc]initWithContentsOfURL:[NSURL URLWithString:_urlStr] encoding:NSUTF8StringEncoding error:nil];
        if (!htmlstr) {
            htmlstr = [[NSString alloc]initWithContentsOfURL:[NSURL URLWithString:_urlStr] encoding:0x80000632 error:nil];
        }
        if (htmlstr) {
            htmlstr =[htmlstr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
        }
        [_web loadHTMLString:htmlstr baseURL:nil];
    }else{
        NSURLRequest *requset = [NSURLRequest requestWithURL:[NSURL URLWithString:[_urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
        [_web loadRequest:requset];
    }
    
    
  
    
}

#pragma mark --- 返回上一级
-(void)backBtnAction{
    if ([self.web canGoBack]) {
        [self.web goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [MBProgressHUD showHUDAddedTo:_web animated:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:_web animated:YES];
//    [self.web stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none'"];
//    [self.web stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none'"];
}

@end

