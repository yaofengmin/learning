//
//  FileWebViewController.h
//  yanshan
//
//  Created by yfm on 2017/12/8.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

@interface FileWebViewController : YHBaseViewController
@property (nonatomic ,strong) UIWebView *web;
@property (nonatomic ,copy) NSString *urlStr;
@property (nonatomic ,copy) NSDictionary *shareDic;


-(instancetype)initWithUrlStr:(NSString *)urlStr andNavTitle:(NSString *)navTitle;

@end
