//
//  DuringClassViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/13.
//  Copyright © 2016年 Hanks. All rights reserved.
// 课间

#import "StudyGroupViewController.h"
#import "HelpPublishViewController.h"
#import "SendVoteViewController.h"

#import "EnclosureDetailViewController.h"

#import "CommentdetailViewController.h"
#import "YHNavgationController.h"

#import "RDVTabBarController.h"
#import "PersonHomeViewController.h"

#import "DIYSearchView.h"
#import "CategoryTopVC.h"
#import "MWPhotoBrowser.h"

#import "AddGropViewController.h"

#import "YHBaseWebViewController.h"
#import "ShareDetailListViewController.h"
#import "DownloadWebViewController.h"
#import "ShareListModel.h"

@interface StudyGroupViewController ()<cellDelegate,filterAction,MWPhotoBrowserDelegate,PublishCircleDelegate,PublishFileDelegate>

@property (nonatomic, strong)  DIYSearchView *headerView;
@property (nonatomic, assign)  BOOL isFirter;//根据这个来判断是筛选还是搜索防止冲突

@property (nonatomic, strong)  CategoryTopVC *slidebarVC;


@end


@implementation StudyGroupViewController

-(void)viewDidAppear:(BOOL)animated
{
    if (self.ymData) {
        
        //        [self reloadOneRowsWithData:self.ymData andRow:self.row];
    }
    [super viewDidAppear:animated];
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self configData];
    self.isShowPublishBtn = YES;
    KAddobserverNotifiCation(@selector(loadDataForHeader), NOTIGICATIONFINDVVIEWRELOAD);
    [self creatSearchV];
    self.mainTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.mainTable.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    self.mainTable.mj_footer.hidden = YES;
    self.isShowPublishBtn = YES;
    self.isShowSendVoteBtn = YES;
    [self.publishBtn handleControlEvent:UIControlEventTouchUpInside withBlock:^{
        self.chooseView.hidden = !self.chooseView.hidden;
    }];
    [self loadDataForHeader];
    
    
}


-(void)configData
{
    self.photos            = @[].mutableCopy;
    self.thumbs            = @[].mutableCopy;
}


#pragma mark - 图片点击事件回调
#pragma mark - 图片点击事件回调
- (void)showImageViewWithImageViews:(NSArray *)imageViews byClickWhich:(NSInteger)clickTag{
    
    [[self.parentContrl rdv_tabBarController] setTabBarHidden:YES animated:YES];
    
    [self.photos removeAllObjects];
    [self.thumbs removeAllObjects];
    
    for ( NSString *thumbUrl in imageViews) {
        
        [self.thumbs addObject:[MWPhoto photoWithURL:[NSURL URLWithString:thumbUrl]]];
        [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[thumbUrl  stringByReplacingOccurrencesOfString:@"_thumb" withString:@""]]]];
    }
    
    BOOL displayActionButton = YES; //分享按钮,默认是
    BOOL displaySelectionButtons = NO; //是否显示选择按钮在图片上,默认否
    BOOL displayNavArrows = NO;  //左右分页切换,默认否
    BOOL enableGrid = NO; //是否允许用网格查看所有图片,默认是
    BOOL startOnGrid = NO; //是否第一张,默认否
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = displayActionButton;
    browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = displaySelectionButtons; //控制条件控件 是否显示,默认否
    browser.zoomPhotosToFill = NO; //是否全屏,默认是
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = YES;
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    browser.wantsFullScreenLayout = YES;//是否全屏
#endif
    
    [browser setCurrentPhotoIndex:clickTag];
    
    if (displaySelectionButtons) {
        self.selections = [NSMutableArray new];
        for (int i = 0; i < self.photos.count; i++) {
            [self.selections addObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        // Push
        [self.parentContrl.navigationController pushViewController:browser animated:YES];
        
    } else {
        // Modal
        YHNavgationController *nc = [[YHNavgationController alloc] initWithRootViewController:browser];
        nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.parentContrl presentViewController:nc animated:YES completion:nil];
    }
    
}


#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < self.thumbs.count)
        return [self.thumbs objectAtIndex:index];
    return nil;
}


- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return [[self.selections objectAtIndex:index] boolValue];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.row = indexPath.row;
    self.ymData = (YMTextData *)[self.tableDataSource objectAtIndex:indexPath.row];
    [self pushDetailActionWithindexPath:indexPath];
    
}
-(void)pushDetailActionWithindexPath:(NSIndexPath *)indexPath
{
    CommentdetailViewController *detailC = [[CommentdetailViewController alloc]initWithShowMessageType:GetMessagesTypeDuringClass andClassId:@""];
    detailC.ymData = self.ymData;
    [self.parentContrl.navigationController pushViewController:detailC animated:YES];
    @weakify(self);
    detailC.deletedMsg = ^(YMTextData *ymd , NSInteger indext) {
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableDataSource removeObjectAtIndex:indexPath.row];
            [self.mainTable deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationLeft];
        });
    };
}


#pragma mark --- 头像点击事件    跳转详情
-(void)userPhotoImageClick:(NSInteger)index adnUsrId:(NSInteger)userId{
    
    PersonHomeViewController *userHome = [[PersonHomeViewController alloc]init];
    userHome.userId = (int)userId;
    [self.parentContrl.navigationController pushViewController:userHome animated:YES];
}



-(void)loadDataForHeader
{
    [super loadDataForHeader];
}

-(void)loadMoreData
{
    [super loadMoreData];
}

#pragma mark === 完成发布delegate ==
-(void)finishPublish
{
    self.ymData = nil;
    [self loadDataForHeader];
}

#pragma mark === 投票成功
-(void)addMessagesQusAFinish
{
    self.ymData = nil;
    [self loadDataForHeader];
}
#pragma mark === 底部搜索筛选
-(void)creatSearchV
{
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
    _headerView = [[DIYSearchView alloc]initWithSuperV:self filterHidden:NO];
    _headerView.frame = CGRectMake(0, 0, KScreenWidth, 50);
    _headerView.delegate = self;
    _headerView.backgroundColor = BColor;
    _headerView.placeholder = @"搜索";
    [header addSubview:_headerView];
    self.mainTable.tableHeaderView = header;
    
}
#pragma mark === UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}


#pragma mark === 筛选
-(void)handleFilterActionWithTag:(NSInteger)tag
{
    if (tag == 1000) {
        [self showMenue];
        _isFirter = YES;
    }else{
        if (_isFirter == YES) {
            _slidebarVC.view.hidden = YES;
            _isFirter = NO;
        }
    }
}
#pragma mark === 弹出筛选视图
-(void)showMenue
{
    [self.headerView converClick];
    if (!_slidebarVC) {
        _slidebarVC = [[CategoryTopVC alloc] initWithSearchType:YHSearchTypeCircle];
        [self.view addSubview:_slidebarVC.view];
        [_slidebarVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_headerView.mas_bottom).offset(1);
            make.left.right.bottom.equalTo(self.view);
        }];
        @weakify(self);
        _slidebarVC.tipsFinishClick = ^(NSArray<SeacherItemModel *> * selectArr){
            @strongify(self);
            self.labelId = @"";//重置下,防止重置的时候依然是旧值
            for (SeacherItemModel *mode in selectArr) {//当前界面是单选,所以不用担心会覆盖
                self.labelId = mode.codeId;
            }
            [self loadDataForHeader];
        };
    }
    
    [_slidebarVC showHideSidebar];
}

#pragma mark === 搜索
-(void)searchEndWithText:(NSString *)text
{
    self.keyWord = text;
    [self loadDataForHeader];
}
-(void)publishTypeChooseWithSender:(UIButton *)sender
{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        self.chooseView.hidden = YES;
        [self vailGroupWithTag:sender.tag];
    }
    //    if (sender.tag == 101) {//发布动态
    //
    //    }else{//发布投票
    //
    //    }
}

#pragma mark === 是否加入班级
-(void)vailGroupWithTag:(NSInteger )tag
{
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{Service:UserGetUserClasses,USERID:KgetUserValueByParaName(USERID)} andBlocks:^(NSDictionary *result) {
        @strongify(self);
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                if (tag == 101) {//发布动态
                    HelpPublishViewController *publish = [[HelpPublishViewController alloc]initWithPubilshType:GetMessagesTypeStudyGroup];
                    publish.delegate = self;
                    YHNavgationController *publishNav = [[YHNavgationController alloc]initWithRootViewController:publish];
                    publishNav.modalPresentationStyle = 0;
                    if (self.parentContrl) {
                        [self.parentContrl presentViewController:publishNav animated:YES completion:nil];
                    }else{
                        [self presentViewController:publishNav animated:YES completion:nil];
                    }
                }else{//发布投票
                    SendVoteViewController *publish = [[SendVoteViewController alloc]initWithPubilshType:GetMessagesTypeStudyGroup];
                    publish.delegate = self;
                    YHNavgationController *publishNav = [[YHNavgationController alloc]initWithRootViewController:publish];
                    publishNav.modalPresentationStyle = 0;
                    if (self.parentContrl) {
                        [self.parentContrl presentViewController:publishNav animated:YES completion:nil];
                    }else{
                        [self presentViewController:publishNav animated:YES completion:nil];
                    }
                }
            }else {
                [YHJHelp aletWithTitle:@"提示" Message:@"您还没有加入任何班级,请前去申请" sureTitle:@"前往" CancelTitle:@"取消" SureBlock:^{
                    AddGropViewController *add = [[AddGropViewController alloc]init];
                    [self.parentContrl.navigationController pushViewController:add animated:YES];
                } andCancelBlock:nil andDelegate:self.parentContrl];
            }
        }
        
    }];
}
#pragma mark === 共享详情
-(void)requestDetailNewsWithNewId:(NSString *)newsId
{
    NSDictionary *paramDic = @{Service:GetNewsInfo,
                               @"newsId":newsId,
                               @"user_Id":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                ShareDetailModel *model = [ShareDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                DownloadWebViewController *detailVC = [[DownloadWebViewController alloc]initWithUrlStr:model.filePathShow andNavTitle:model.newsName];
                detailVC.detailModel = model;
                [self.parentContrl.navigationController pushViewController:detailVC animated:YES];
                
            }
        }
        
        
    }];
}
-(void)showFileViewWithFiles:(NSArray *)files byClickWhich:(NSInteger)clickTag
{
    NSArray *fileNameArr = [files[clickTag] componentsSeparatedByString:@"_"];
    NSString *fileName = fileNameArr.lastObject;
    FileWebViewController *web = [[FileWebViewController alloc]initWithUrlStr:files[clickTag] andNavTitle:fileName];
    [self.parentContrl.navigationController pushViewController:web animated:YES];
}

#pragma mark === 展示web
-(void)showWebViewWithlink:(NSString *)linkUrl title:(NSString *)title index:(NSInteger)stamp
{
    if ([linkUrl hasPrefix:@"http"]) {
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:linkUrl andNavTitle:title];
        [self.parentContrl.navigationController pushViewController:web animated:YES];
    }else{
        if ([[UserInfoModel getInfoModel].grade integerValue] < 2) {//共享文件 需要 grade >0
            @weakify(self);
            [YHJHelp aletWithTitle:@"提示" Message:@"VIP会员等级不足，不能查看" sureTitle:@"立即升级" CancelTitle:@"忽略 " SureBlock:^{
                @strongify(self);
                BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                [self.parentContrl.navigationController pushViewController:buy animated:YES];
                
            } andCancelBlock:nil andDelegate:self.parentContrl];
            return;
        }
        [self requestDetailNewsWithNewId:linkUrl];
    }
    [self requestCircleInfoWithIndex:stamp];
}
#pragma mark === 获取当前动态详情
-(void)requestCircleInfoWithIndex:(NSInteger )stamp
{
    YMTextData *data = (YMTextData *)[self.tableDataSource objectAtIndex:stamp];
    NSMutableDictionary *sendDic  =  [[NSMutableDictionary alloc]init];
    [sendDic setObject:CircleGetMessageInfo forKey:Service];
    [sendDic setObject:data.messageBody.msgId forKey:@"msgId"];
    [sendDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        data.messageBody.viewCount = result[REQUEST_INFO][@"viewCount"];
        [self reloadOneRowsWithData:data andRow:stamp];
    }];
}
-(void)dealloc
{
    KRemoverNotifi(NOTIGICATIONFINDVVIEWRELOAD);
}


@end
