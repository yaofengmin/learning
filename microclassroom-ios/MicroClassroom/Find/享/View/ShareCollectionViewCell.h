//
//  ShareCollectionViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShareCateModel.h"

@interface ShareCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *topImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic,strong) ShareCateModel *cateModel;

@end
