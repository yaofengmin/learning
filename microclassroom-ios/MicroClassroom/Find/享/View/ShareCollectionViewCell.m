//
//  ShareCollectionViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "ShareCollectionViewCell.h"

@implementation ShareCollectionViewCell

- (void)awakeFromNib {
	[super awakeFromNib];
	self.topImage.backgroundColor = ImageBackColor;
    self.topImage.layer.cornerRadius = 4.0;
    self.topImage.layer.masksToBounds = YES;
}


-(void)setCateModel:(ShareCateModel *)cateModel
{
	[self.topImage sd_setImageWithURL:[NSURL URLWithString:cateModel.coverPhoto]];
	self.titleLabel.text = cateModel.cateName;
}
@end
