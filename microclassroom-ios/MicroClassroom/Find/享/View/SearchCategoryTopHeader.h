//
//  SearchCategoryTopHeader.h
//  MicroClassroom
//
//  Created by fm on 2017/9/6.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCategoryTopHeader : UICollectionReusableView

@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *imageStr;

@end
