//
//  ShareChooseStudyGroupView.m
//  MicroClassroom
//
//  Created by yfm on 2017/12/11.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "ShareChooseStudyGroupView.h"

@interface ShareChooseStudyGroupView  ()<ClassPublishViewBottomDeleage>
@property (nonatomic ,strong) ClassModel *classModel;
@end
@implementation ShareChooseStudyGroupView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.chooseGroupV.delegate = self;
    self.chooseGroupV.backgroundColor = KColorFromRGB(0xf0f0f5);
    self.layer.cornerRadius = 4.0;
    self.layer.masksToBounds = YES;
}
- (IBAction)cancelAction:(UIButton *)sender {
    [self animatedOut];
}
- (IBAction)sureAction:(UIButton *)sender {
    self.shareChangeGroupId(self.classModel);
    [self animatedOut];
}

-(void)publicWithClassModel:(ClassModel *)model
{
    self.classModel = model;
}
- (void)show
{
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    keywindow.windowLevel = UIWindowLevelNormal;
    [keywindow addSubview:self];
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(keywindow);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 160));
    }];
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,
                              keywindow.bounds.size.height/2.0f);
    
    [self animatedIn];
}
#pragma mark - Animated Mthod
- (void)animatedIn
{
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)animatedOut
{
    [UIView animateWithDuration:.35 animations:^{
        [self removeFromSuperview];
    }];
}
@end
