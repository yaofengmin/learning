//
//  DIYSearchView.m
//  yanshan
//
//  Created by fm on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "DIYSearchView.h"

@interface DIYSearchView ()<UISearchBarDelegate>
{
    UIView *_conver;
    
}

@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,assign) UIViewController* superV;

@property (nonatomic ,assign) BOOL filterHidden;

@end
@implementation DIYSearchView

- (instancetype)initWithSuperV:(id)superV filterHidden:(BOOL)filterHidden
{
    self = [super init];
    if (self) {
        self.filterHidden = filterHidden;
        [self creatSub];
        _superV = superV;
    }
    return self;
}

-(void)creatSub
{
    if (!self.filterHidden) {
        //筛选按钮
        _filterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //    [_filterBtn setTitle:@"筛选" forState:UIControlStateNormal];
        [_filterBtn setImage:[UIImage imageNamed:@"btn_more"] forState:UIControlStateNormal];
        [_filterBtn setTitleColor:KColorFromRGB(0xe64f53) forState:UIControlStateNormal];
        [_filterBtn addTarget:self action:@selector(handleFilterAction:) forControlEvents:UIControlEventTouchUpInside];
        _filterBtn.titleLabel.font = Font(14);
        _filterBtn.tag = 1000;
        [self addSubview:self.filterBtn];
        [self.filterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.mas_right).offset(-10);
            make.centerY.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(44, 44));
        }];
        
    }
    
    
    _searchBar = [[UISearchBar alloc]init];
    _searchBar.delegate = self;
    _searchBar.backgroundColor = [UIColor whiteColor];
    _searchBar.placeholder = self.placeholder;
    _searchBar.showsCancelButton = NO;
    [_searchBar.layer setBorderWidth:8];
    _searchBar.layer.cornerRadius = 3.0;
    _searchBar.layer.masksToBounds = YES;
    _searchBar.tag = 1001;
    
    _searchBar.returnKeyType = UIReturnKeySearch;
    _searchBar.enablesReturnKeyAutomatically = NO;//再没有值得情况下也可以点击搜索
    [_searchBar.layer setBorderColor:KColorRGB(239, 239, 244).CGColor];
    [self setSearchTextFieldBackgroundColor:NCColor];
    [self addSubview:self.searchBar];
    [_searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).offset(10);
        if (_filterHidden) {
            make.right.mas_equalTo(-12);
        }else{
            make.right.mas_equalTo(_filterBtn.mas_left).offset(-12);
        }
        make.height.mas_equalTo(33);
        make.centerY.equalTo(self);
    }];
    
    UITextField * searchField = [self sa_GetSearchTextFiled];
    searchField.font = Font(12);
    searchField.textColor = JColor;
    searchField.backgroundColor = NCColor;
    
}

- (UITextField *)sa_GetSearchTextFiled{
    if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
        return self.searchBar.searchTextField;
    }else{
        return self.searchBar.subviews.firstObject.subviews.lastObject;
//        UITextField *searchTextField =  [self valueForKey:@"_searchField"];
//        return searchTextField;
    }
}

-(void)setPlaceholder:(NSString *)placeholder
{
    _searchBar.placeholder = placeholder;
}

#pragma mark - UISearchBarDelegate
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [self creatCover];
    if ([self.delegate respondsToSelector:@selector(handleFilterActionWithTag:)]) {
        [self.delegate handleFilterActionWithTag:searchBar.tag];
    }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if ([self.delegate respondsToSelector:@selector(searchEndWithText:)]) {
        [self.delegate searchEndWithText:searchBar.text];
    }
    [_conver removeFromSuperview];
    _conver =nil;
    [self endEditing:YES];
}
-(void)creatCover
{
    if (!_conver) {
        _conver = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.origin.y + 50, KScreenWidth, KScreenHeight)];
        [_superV.view addSubview:_conver];
        _conver.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        UIView *bgVWithTapView = [[UIView alloc] init];
        bgVWithTapView.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight);
        bgVWithTapView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        [_conver addSubview:bgVWithTapView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(converClick)];
        [bgVWithTapView addGestureRecognizer:tap];
    }
}
- (void)setSearchTextFieldBackgroundColor:(UIColor *)backgroundColor
{
    UIView *searchTextField = nil;
    if (IOS7) {
        searchTextField = [[[self.searchBar.subviews firstObject] subviews] lastObject];
    }
    searchTextField.backgroundColor = backgroundColor;
}
-(void)converClick
{
    _searchBar.text = @"";
    [_conver removeFromSuperview];
    _conver =nil;
    [self endEditing:YES];
    if ([self.delegate respondsToSelector:@selector(cancelSearchAction)]) {
          [self.delegate cancelSearchAction];
      }
}

#pragma mark === handleFilterAction
-(void)handleFilterAction:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(handleFilterActionWithTag:)]) {
        [self.delegate handleFilterActionWithTag:sender.tag];
    }
}
@end
