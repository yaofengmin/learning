//
//  ShareDetailListTableViewCell.h
//  yanshan
//
//  Created by BPO on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShareListModel.h"

@interface ShareDetailListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *FileType;
@property (weak, nonatomic) IBOutlet UILabel *downloadNum;

@property (weak, nonatomic) IBOutlet UILabel *author;

@property (nonatomic,strong) ShareDetailModel *listModel;

@end
