//
//  ShareDetailListTableViewCell.m
//  yanshan
//
//  Created by BPO on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "ShareDetailListTableViewCell.h"

@implementation ShareDetailListTableViewCell

- (void)awakeFromNib {
	[super awakeFromNib];
	self.imageV.backgroundColor = ImageBackColor;
    self.imageV.layer.cornerRadius = 4.0;
    self.imageV.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}


-(void)setListModel:(ShareDetailModel *)listModel
{
	self.titleLabel.text = listModel.newsName;
	[self.imageV sd_setImageWithURL:[NSURL URLWithString:listModel.coverPhoto]];
	self.downloadNum.text = listModel.downCount;
    self.author.text = [NSString stringWithFormat:@"作者: %@",listModel.author];
	[self.FileType sd_setImageWithURL:[NSURL URLWithString:listModel.fileExt]];
}

@end
