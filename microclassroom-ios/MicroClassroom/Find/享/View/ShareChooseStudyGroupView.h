//
//  ShareChooseStudyGroupView.h
//  MicroClassroom
//
//  Created by yfm on 2017/12/11.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ClassPublishViewBottom.h"

@interface ShareChooseStudyGroupView : UIView
@property (weak, nonatomic) IBOutlet ClassPublishViewBottom *chooseGroupV;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;


/**
 班级修改
 */
@property (nonatomic,copy) void (^shareChangeGroupId) (ClassModel *model);
- (void)show;
- (void)animatedOut;
@end
