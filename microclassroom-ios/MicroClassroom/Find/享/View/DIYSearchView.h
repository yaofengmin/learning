//
//  DIYSearchView.h
//  yanshan
//
//  Created by fm on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol filterAction <NSObject>
@optional
-(void)handleFilterActionWithTag:(NSInteger )tag;

-(void)searchEndWithText:(NSString *)text;

-(void)cancelSearchAction;

@end
@interface DIYSearchView : UIView
@property (nonatomic ,strong) UISearchBar *searchBar;
//提示语
@property (nonatomic,copy) NSString *placeholder;

@property(nonatomic ,strong) UIButton *filterBtn;

@property (nonatomic,weak) id<filterAction> delegate;




- (instancetype)initWithSuperV:(id)superV filterHidden:(BOOL)filterHidden;
-(void)converClick;

@end
