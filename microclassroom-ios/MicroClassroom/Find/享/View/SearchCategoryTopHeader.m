//
//  SearchCategoryTopHeader.m
//  MicroClassroom
//
//  Created by fm on 2017/9/6.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "SearchCategoryTopHeader.h"

@interface SearchCategoryTopHeader()

@property(nonatomic ,strong) UILabel *titleLabel;
@property (nonatomic,strong) UIImageView *headerImage;

@end

@implementation SearchCategoryTopHeader

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _headerImage = [[UIImageView alloc]init];
        [self addSubview:_headerImage];
        [_headerImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.top.mas_equalTo(25);
        }];
        
        _titleLabel = [UILabel labelWithText:@"端头" andFont:14 andTextColor:DColor andTextAlignment:0];
        [self addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_headerImage.mas_right).offset(7);
            make.centerY.equalTo(_headerImage);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = KColorFromRGB(0xededed);
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(0);
            make.height.equalTo(0.5);
        }];
    }
    return self;
}


-(void)setTitle:(NSString *)title {
    _titleLabel.text = title;
}

-(void)setImageStr:(NSString *)imageStr
{
    _headerImage.image = [UIImage imageNamed:imageStr];
}

@end
