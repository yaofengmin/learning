//
//  ShareCateModel.h
//  yanshan
//
//  Created by BPO on 2017/8/24.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseModel.h"

@interface ShareCateModel : YHBaseModel
@property (nonatomic,copy) NSString *cateId;
@property (nonatomic,copy) NSString *cateName;
@property (nonatomic,copy) NSString *coverPhoto;

@end
