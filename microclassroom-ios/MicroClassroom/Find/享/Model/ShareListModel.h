//
//  ShareListModel.h
//  yanshan
//
//  Created by BPO on 2017/8/24.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseModel.h"
@interface ShareDetailModel :YHBaseModel
@property (nonatomic,copy) NSString *newsId;
@property (nonatomic,copy) NSString *newsName;
@property (nonatomic,copy) NSString *brief;
@property (nonatomic,copy) NSString *newsTime;
@property (nonatomic,copy) NSString *coverPhoto;
@property (nonatomic,copy) NSString *detailUrl;
@property (nonatomic,copy) NSString *fileExt;//文件形式
@property (nonatomic,copy) NSString *downCount;//下载次数
@property (nonatomic,copy) NSString *filePath;//文件
@property (nonatomic,copy) NSString *filePathShow;
@property (nonatomic,copy) NSString *author;//作者
@property (nonatomic,assign) BOOL isCollection;//是否保存

@end

@interface ShareListModel : YHBaseModel

@property (nonatomic, assign) int total;
@property (nonatomic, copy) NSArray<ShareDetailModel *> *list;
@end

