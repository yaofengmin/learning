//
//  ShareListModel.m
//  yanshan
//
//  Created by BPO on 2017/8/24.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "ShareListModel.h"

@implementation ShareListModel

+ (NSDictionary *)objectClassInArray{
	return @{@"list" : @"ShareDetailModel"};
}

@end

@implementation ShareDetailModel

+(NSDictionary *)mj_replacedKeyFromPropertyName
{
	return @{@"newsName" : @"newName",@"newsTime":@"newTime"};
}

@end
