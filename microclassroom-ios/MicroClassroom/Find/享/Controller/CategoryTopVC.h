//
//  CategoryTopVC.h
//  MicroClassroom
//
//  Created by fm on 2017/9/6.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "TopSlideBar.h"
#import "SeacherTipListModel.h"
@interface CategoryTopVC : TopSlideBar

-(instancetype)initWithSearchType:(YHSearchTopType)type;
//添加
@property (nonatomic ,copy) void(^tipsFinishClick)(NSArray<SeacherItemModel *> * selectArr);
@property (nonatomic ,copy) void(^tipsStateFinishClick)(NSArray<SeacherItemModel *> * selectArr,NSArray<SeacherItemModel *> * stateSelectArr);

//移除
@property (nonatomic, copy) void(^cancelTip)(SeacherItemModel *mode);
//取消
@property (nonatomic ,copy) void(^clearClick)();
@end
