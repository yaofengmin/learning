//
//  ShareDetailListViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "ShareDetailListViewController.h"

#import "DownloadWebViewController.h"

#import "ShareDetailListTableViewCell.h"

#import "DIYSearchView.h"
#import "CategoryTopVC.h"

#import "ShareListModel.h"



static NSString *ShareDetailListCellID = @"ShareDetailListCellID";

@interface ShareDetailListViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIScrollViewDelegate,filterAction>

@property (nonatomic, strong)  DIYSearchView *headerView;
@property (nonatomic, strong)  CategoryTopVC *slidebarVC;
@property (nonatomic, strong)  UIView *sepLine;
@property (nonatomic, assign)  BOOL isFirter;//根据这个来判断是筛选还是搜索防止冲突
@property (nonatomic, strong)  NSMutableArray *dataArr;
@property (nonatomic, assign)  NSInteger   total;;

@property (nonatomic,strong) ShareListModel *model;
@property (nonatomic,assign) NSInteger pageCount;


@property (nonatomic,copy) NSString *keyWord;//搜索关键字
@property (nonatomic,copy) NSString *labelID;//标签的ID

@property (nonatomic,strong) UITableView *tableV;
@property (nonatomic,strong) DIYTipView *tipView;
@end

@implementation ShareDetailListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = _sc_title;
    _pageCount = 1;
    _dataArr = [NSMutableArray array];
    [self creatSearchV];
    [self initalTable];
    [self requestList];
}
#pragma mark - 刷新
-(void)loadDataForHeader {
    _pageCount = 1;
    [self requestList];
    
}

#pragma mark - 加载
- (void)loadMoreData {
    _pageCount ++ ;
    [self requestList];
}
- (void)requestList {
    NSDictionary *paramDic = @{Service:GetNewsList,
                               @"cateId":self.cateId,
                               @"labelId":self.labelID.length == 0?@"":self.labelID,
                               @"userId":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID),
                               @"pageIndex":@(_pageCount),
                               @"pageSize":@20,
                               @"keyWord":self.keyWord.length == 0?@"":self.keyWord};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (_pageCount == 1) {
            [self.dataArr removeAllObjects];
            [self.tableV.mj_header endRefreshing];
        }else {
            [self.tableV.mj_footer endRefreshing];
        }
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _model = [ShareListModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                [self.dataArr addObjectsFromArray:_model.list];
                _total = _model.total;
                [self.tableV reloadData];
                if (self.dataArr.count == _total) {
                    self.tableV.mj_footer.hidden = YES;
                }else {
                    self.tableV.mj_footer.hidden = NO;
                }
            }
        }
        
        if (self.dataArr.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                
            }
            [self.tableV addSubview:self.tipView];
            [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.view);
                make.size.equalTo(CGSizeMake(120, 120));
                
            }];
            self.tableV.mj_header.hidden = YES;
            self.tableV.mj_footer.hidden = YES;
        }else
        {
            if (self.tipView.superview) {
                
                [self.tipView removeFromSuperview];
            }
        }
        
    }];
}
-(void)creatSearchV
{
    _headerView = [[DIYSearchView alloc]initWithSuperV:self filterHidden:NO];
    _headerView.delegate = self;
    _headerView.placeholder = @"搜索";
    _headerView.backgroundColor =  [UIColor whiteColor];
    [self.view addSubview:_headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(KTopHeight);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 50));
    }];
}

-(void)initalTable
{
    _sepLine = [[UIView alloc]init];
    _sepLine.backgroundColor = KColorFromRGB(0xededed);
    [self.view addSubview:_sepLine];
    [_sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_headerView.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 1));
        make.centerX.equalTo(self.view);
    }];
    
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 235;
    self.tableV.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_sepLine.mas_bottom);
        make.left.and.right.and.bottom.mas_equalTo(0);
    }];
    if ([self.tableV respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableV  setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    if (IOS8) {
        if ([self.tableV  respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableV  setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 15)];
        }
    }
    [self.tableV registerNib:[UINib nibWithNibName:@"ShareDetailListTableViewCell" bundle:nil] forCellReuseIdentifier:ShareDetailListCellID];
    
    self.tableV.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.tableV.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
}


#pragma mark === UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShareDetailListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ShareDetailListCellID forIndexPath:indexPath];
    cell.listModel = self.dataArr[indexPath.row];
    return cell;
}


#pragma mark === UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[UserInfoModel getInfoModel].grade integerValue] < 2) {//共享文件 需要 grade >= 2
        @weakify(self);
        [YHJHelp aletWithTitle:@"提示" Message:@"VIP会员等级不足，不能查看" sureTitle:@"立即升级" CancelTitle:@"忽略 " SureBlock:^{
            @strongify(self);
            BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
            [self.navigationController pushViewController:buy animated:YES];
            
        } andCancelBlock:nil andDelegate:self];
        return;
    }
    ShareDetailModel *model = self.dataArr[indexPath.row];
    [self requestDetailNewsWithNewId:model.newsId url:model.detailUrl];
    
}
#pragma mark === 详情
-(void)requestDetailNewsWithNewId:(NSString *)newsId url:(NSString *)detailUrl
{
    NSDictionary *paramDic = @{Service:GetNewsInfo,
                               @"newsId":newsId,
                               @"user_Id":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                ShareDetailModel *model = [ShareDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                DownloadWebViewController *detailVC = [[DownloadWebViewController alloc]initWithUrlStr:model.filePathShow andNavTitle:model.newsName];
                detailVC.detailModel = model;
                [self.navigationController pushViewController:detailVC animated:YES];
                
            }
        }
        
        
    }];
}

#pragma mark === UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}


#pragma mark === 筛选
-(void)handleFilterActionWithTag:(NSInteger)tag
{
    if (tag == 1000) {
        [self showMenue];
        _isFirter = YES;
    }else{
        if (_isFirter == YES) {
            _slidebarVC.view.hidden = YES;
            _isFirter = NO;
        }
    }
}
#pragma mark === 弹出筛选视图
-(void)showMenue
{
    [self.headerView converClick];
    if (!_slidebarVC) {
        _slidebarVC = [[CategoryTopVC alloc] initWithSearchType:YHSearchTypeShreDetail];
        [self.view addSubview:_slidebarVC.view];
        [_slidebarVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_sepLine.mas_bottom);
            make.left.right.bottom.equalTo(self.view);
        }];
        @weakify(self);
        _slidebarVC.tipsFinishClick = ^(NSArray<SeacherItemModel *> * selectArr){
            @strongify(self);
            self.labelID = @"";//重置下,防止重置的时候依然是旧值
            for (SeacherItemModel *mode in selectArr) {//当前界面是单选,所以不用担心会覆盖
                self.labelID = mode.codeId;
            }
            self.pageCount = 1;
            [self requestList];
        };
    }
    
    [_slidebarVC showHideSidebar];
}

#pragma mark === 搜索
-(void)searchEndWithText:(NSString *)text
{
    self.keyWord = text;
    _pageCount = 1;
    [self requestList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
