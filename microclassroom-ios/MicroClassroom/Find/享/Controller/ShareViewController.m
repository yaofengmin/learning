//
//  ShareViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "ShareViewController.h"

#import "ShareDetailListViewController.h"

#import "ShareCollectionViewCell.h"

#import "ShareCateModel.h"


static NSString * const ShareCellID = @"ShareCollectionCellID";
static CGFloat const kCellMargin = 15.0f;
static CGFloat const kCellColumns = 2.0f;
static NSInteger const cellNum = 2;

@interface ShareViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSArray *dataArr;
@property (nonatomic,strong) DIYTipView *tipView;
@property (nonatomic,strong) UITableView *tableV;
@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //    self.view.backgroundColor = NDColor;
    self.view.backgroundColor = BColor;
    [self layoutSubviews];
    [self requestList];
}
- (void)requestList {
    NSDictionary *paramDic = @{Service:GetNewsCate,
                               @"userId":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                self.dataArr = [ShareCateModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.collectionView reloadData];
            }
        }
        
        if (self.dataArr.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                
            }
            [self.tableV addSubview:self.tipView];
            [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.view);
                make.size.equalTo(CGSizeMake(120, 120));
                
            }];
        }else
        {
            if (self.tipView.superview) {
                
                [self.tipView removeFromSuperview];
            }
        }
        
    }];
}

#pragma mark - collectionView
- (void)layoutSubviews
{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.minimumLineSpacing = kCellColumns;
    flowLayout.minimumInteritemSpacing = kCellMargin;
    CGFloat proportion = (KScreenWidth - kCellMargin * (cellNum + 1))/ cellNum;
    flowLayout.itemSize = CGSizeMake(proportion, 140);
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView registerNib:[UINib nibWithNibName:@"ShareCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:ShareCellID];
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(1, 0, 0, 0));
    }];
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
}

#pragma mark UICollectionView DataSource Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ShareCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:ShareCellID
                                                                               forIndexPath:indexPath];
    [cell setCateModel:self.dataArr[indexPath.item]];
    return cell;
}

//UICollectionView被选中时调用的方法
-(void)collectionView:( UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    ShareCateModel *model = self.dataArr[indexPath.item];
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    ShareDetailListViewController *shareListVC = [[ShareDetailListViewController alloc]init];
    shareListVC.cateId = model.cateId;
    shareListVC.sc_title = model.cateName;
    [self.parentContrl.navigationController pushViewController:shareListVC animated:YES];
    
}

//返回这个UICollectionViewCell是否可以被选择
-(BOOL)collectionView:( UICollectionView *)collectionView shouldSelectItemAtIndexPath:( NSIndexPath *)indexPath
{
    return YES ;
}
#pragma mark --UICollectionViewDelegateFlowLayout
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return kCellMargin;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(kCellMargin, kCellMargin, kCellMargin, kCellMargin);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
