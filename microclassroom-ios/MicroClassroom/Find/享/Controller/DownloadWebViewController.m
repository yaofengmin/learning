//
//  DownloadWebViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "DownloadWebViewController.h"

#import "ShareChooseStudyGroupView.h"
#import "ClassModel.h"

#import "AddGropViewController.h"

@interface DownloadWebViewController ()<UIWebViewDelegate>
@property (nonatomic ,strong) UIWebView *web;
@property (nonatomic ,copy) NSString *NavTitle;
@property (nonatomic,strong) UIButton *downloadBtn;

@end

@implementation DownloadWebViewController

-(instancetype)initWithUrlStr:(NSString *)urlStr andNavTitle:(NSString *)navTitle

{
    if (self= [super init]) {
        _urlStr = urlStr;
        _NavTitle = navTitle;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.yh_navigationItem.title = _NavTitle;
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"note_share"] style:YHBarButtonItemStylePlain handler:^(id send) {
        @strongify(self);
        [[YHJHelp shareInstance] showShareInController:self andShareURL:self.detailModel.filePath andTitle:self.NavTitle andShareText:self.detailModel.newsName andShareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.detailModel.coverPhoto]]] isShowStudyGroup:YES isDIYUrl:YES];
    }];
    
    _web = [[UIWebView alloc]init];
    _web.scrollView.bounces = NO;
    _web.delegate   = self;
    _web.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_web];
    [_web  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if([_urlStr hasSuffix:@"txt"]){
        NSString * htmlstr = [[NSString alloc]initWithContentsOfURL:[NSURL URLWithString:_urlStr] encoding:NSUTF8StringEncoding error:nil];
        if (!htmlstr) {
            htmlstr = [[NSString alloc]initWithContentsOfURL:[NSURL URLWithString:_urlStr] encoding:0x80000632 error:nil];
        }
        if (htmlstr) {
            htmlstr =[htmlstr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
        }
        [_web loadHTMLString:htmlstr baseURL:nil];
    }else{
        NSURLRequest *requset = [NSURLRequest requestWithURL:[NSURL URLWithString:[_urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
        [_web loadRequest:requset];
    }
    
    [self creatSolveBtn];
    
    KAddobserverNotifiCation(@selector(vailGroup), @"ShareFileToStudyGroup");
    
}

#pragma mark --- 返回上一级
-(void)backBtnAction{
    if ([self.web canGoBack]) {
        [self.web goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [MBProgressHUD showHUDAddedTo:_web animated:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:_web animated:YES];
    //禁止
//    [self.web stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none'"];
//    [self.web stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none'"];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [MBProgressHUD hideHUDForView:_web animated:YES];
}
-(void)creatSolveBtn
{
    _downloadBtn  =[UIButton buttonWithType:UIButtonTypeCustom];
    [_downloadBtn setImage:[UIImage imageNamed:@"save"] forState:UIControlStateNormal];
    [_downloadBtn addTarget:self action:@selector(handleDownloadAction:) forControlEvents:UIControlEventTouchUpInside];
    [_downloadBtn setImage:[UIImage imageNamed:@"save_nor"] forState:
     UIControlStateSelected];
    _downloadBtn.selected = self.detailModel.isCollection;
    [self.view addSubview:_downloadBtn];
    [_downloadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.bottom.equalTo(-47);
        make.size.equalTo(CGSizeMake(71, 71));
        
    }];
}
#pragma mark === 下载
-(void)handleDownloadAction:(UIButton *)sender
{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
        [mDic setObject:self.detailModel.newsId forKey:@"pId"];
        [mDic setObject:@"3" forKey:@"ctype"];
        [mDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
        if (sender.selected) {
            [mDic setObject:CircleCancelCollection forKey:Service];
        }else{
            [mDic setObject:CircleAddCollection forKey:Service];
        }
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:mDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            if (result) {
                sender.selected = !sender.selected;
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }];
    }
}

-(void)shareToStudyGroup
{
    ShareChooseStudyGroupView *groupView = [[NSBundle mainBundle] loadNibNamed:@"ShareChooseStudyGroupView" owner:self options:nil].firstObject;
    @weakify(self);
    groupView.shareChangeGroupId = ^(ClassModel *model) {
        @strongify(self);
        [self shareActionWithClassModel:model];
    };
    [groupView show];
}
-(void)shareActionWithClassModel:(ClassModel *)model
{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
        [mDic setObject:CircleCopyNewsToMessage forKey:Service];
        [mDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
        [mDic setObject:self.detailModel.newsId forKey:@"newsId"];
        [mDic setObject:model.classId forKey:@"classesId"];
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:mDic andBlocks:^(NSDictionary *result) {
            if (result) {
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:self.view];
            }
        }];
    }
}

#pragma mark === 是否加入班级
-(void)vailGroup
{
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{Service:UserGetUserClasses,USERID:KgetUserValueByParaName(USERID)} andBlocks:^(NSDictionary *result) {
        @strongify(self);
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                [self shareToStudyGroup];
            }else {
                [YHJHelp aletWithTitle:@"提示" Message:@"您还没有加入任何班级,请前去申请" sureTitle:@"前往" CancelTitle:@"取消" SureBlock:^{
                    AddGropViewController *add = [[AddGropViewController alloc]init];
                    [self.navigationController pushViewController:add animated:YES];
                } andCancelBlock:nil andDelegate:self];
            }
        }
    }];
}
- (void)dealloc
{
    KRemoverNotifi(@"ShareFileToStudyGroup");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
