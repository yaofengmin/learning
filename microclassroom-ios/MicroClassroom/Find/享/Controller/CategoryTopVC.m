//
//  CategoryScreenVC.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "CategoryTopVC.h"
#import "SearchCollectionViewCell.h"
#import "SearchCategoryTopHeader.h"

@interface CategoryTopVC ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout
>

@property (nonatomic ,strong) UICollectionView *collection;
@property (nonatomic ,assign) YHSearchTopType type;
@property (nonatomic ,strong) UIButton *clearBtn;
@property (nonatomic,strong) UIButton *sureBtn;

//@property (nonatomic ,strong) NSArray<SeacherTipListModel*> *resultArr;
@property (nonatomic ,strong) NSArray<SeacherItemModel*> *resultArr;

//选中的每个类别的Arr
@property   (nonatomic,strong)NSMutableArray    *selectArr;
@property (nonatomic,strong) UIView *bottomV;

@property (nonatomic,strong) NSMutableArray *stateArr;//状态(本地写死    未认领 已认领  已解决)
@property   (nonatomic,strong)NSMutableArray    *stateSelectArr;

@end

@implementation CategoryTopVC
{
}
-(instancetype)initWithSearchType:(YHSearchTopType)type {
    if (self = [super init]) {
        _type = type;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [MainBackColor colorWithAlphaComponent:0.1];
    _selectArr      = [[NSMutableArray alloc]init];
    _stateSelectArr = [NSMutableArray array];
    _stateArr       = [NSMutableArray array];
    NSArray *titleArr = @[@"未认领",@"已认领",@"已解决"];
    for (int i = 1; i < 4; i ++) {
        SeacherItemModel *model = [[SeacherItemModel alloc]init];
        model.codeName = titleArr[i - 1];
        model.codeId = [NSString stringWithFormat:@"%d",i];
        [_stateArr addObject:model];
    }
    [self.contentView addSubview:self.collection];
    [self.collection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 97, 0));
    }];
    
    [self requestTips];
    
}


-(void)requestTips {
    //ctype 100 == 共享  101 == 申请帮助
    NSDictionary *paramDic = @{Service:AppGetCodeList,
                               @"ctype":@(_type + 100)};
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _resultArr = [SeacherItemModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self layoutContentView];
                [_collection reloadData];
            }
        }
    }];
}


-(void)layoutContentView
{
    NSInteger row = (self.resultArr.count - 1) / itemPerLine + 1;
    if (self.type == YHSearchTypeAskAndShare) {
        row = row + (self.stateArr.count - 1) / itemPerLine + 1;
    }
    CGFloat itemH = (row + 1) * 12 + 30 *row;
    if (self.type == YHSearchTypeAskAndShare) {
        itemH = itemH + 50;
    }
    CGFloat height = ceil(itemH) + 50;
    if (height > kSidebarHeight) {
        height = kSidebarHeight;
    }
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.width.left.right.equalTo(self.view);
        make.height.equalTo(height + 97);
    }];
    [self creatBottomView];
}
//required
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIDa = @"SearchCollectionViewCell";
    SearchCollectionViewCell  *collectionCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIDa forIndexPath:indexPath];
    collectionCell.indextPath = indexPath;
    SeacherItemModel *newModel;
    if (self.type == YHSearchTypeAskAndShare) {
        if (indexPath.section == 0) {
            newModel = _resultArr[indexPath.row];
        }else{
            newModel = _stateArr[indexPath.row];
        }
    }else{
        newModel =_resultArr[indexPath.row];
    }
    [collectionCell.tipsBtn setTitle:newModel.codeName forState:UIControlStateNormal];
    if (newModel.state) {
        collectionCell.tipsBtn.backgroundColor = KColorFromRGB(0xf0cf2e);
    }else{
        collectionCell.tipsBtn.backgroundColor = KColorFromRGB(0xefeff4);
    }
    collectionCell.tipsBtn.selected = newModel.state;
    [collectionCell tipsBtnDown:collectionCell.tipsBtn];
    return collectionCell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.type == YHSearchTypeAskAndShare) {
        if (section == 0) {
            return _resultArr.count;
        }
        return _stateArr.count;
    }
    return _resultArr.count;
}

//optional
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (self.type == YHSearchTypeAskAndShare) {
        return 2;
    }
    return 1;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = @"header";
    SearchCategoryTopHeader *cell = nil;
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        cell = (SearchCategoryTopHeader *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
    }
    if (self.type == YHSearchTypeAskAndShare) {
        if (indexPath.section == 0) {
            cell.title = @"类别";
            cell.imageStr = @"category";
        }else{
            cell.title = @"状态";
            cell.imageStr = @"status";
        }
    }else{
        cell.title = @"标签";
        cell.imageStr = @"share_Label";
    }
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.contentView.viewWidth, 50);
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(kItemW, kItemH);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.type == YHSearchTypeAskAndShare) {
        if (indexPath.section == 1) {
            [self stateChangeWithIndexPath:indexPath];
        }else{
            [self kindChangeWithIndexPath:indexPath];
        }
    }else{
        [self kindChangeWithIndexPath:indexPath];
    }
    
}
#pragma mark === 类别
-(void)kindChangeWithIndexPath:(NSIndexPath *)indexPath
{
    SeacherItemModel *clickModel = _resultArr[indexPath.item];
    if (clickModel.state) {
        clickModel.state = !clickModel.state;
        [_selectArr removeObject:clickModel];
        if (self.cancelTip) {
            self.cancelTip(clickModel);
        }
    }else{
        for (SeacherItemModel *newModel in _resultArr) {//遍历数组将所有model 选中状态置为no
            if (newModel.state == YES) {
                [_selectArr removeObject:newModel];
                newModel.state = NO;
                break;
            }
        }
        
        [_selectArr addObject:clickModel];
        clickModel.state = !clickModel.state;
        
    }
    
    [_collection reloadData];
}

#pragma mark === 状态被选中
-(void)stateChangeWithIndexPath:(NSIndexPath *)indexPath
{
    SeacherItemModel *clickModel = _stateArr[indexPath.item];
    if (clickModel.state) {
        clickModel.state = !clickModel.state;
        [_stateSelectArr removeObject:clickModel];
        if (self.cancelTip) {
            self.cancelTip(clickModel);
        }
    }else{
        for (SeacherItemModel *newModel in _stateArr) {//遍历数组将所有model 选中状态置为no
            if (newModel.state == YES) {
                [_stateSelectArr removeObject:newModel];
                newModel.state = NO;
                break;
            }
        }
        
        [_stateSelectArr addObject:clickModel];
        clickModel.state = !clickModel.state;
        
    }
    
    [_collection reloadData];
}

- (UICollectionView *)collection {
    if (!_collection) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        //设置对齐方式
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        //cell间距
        layout.minimumInteritemSpacing = 10.0f;
        //cell行距
        layout.minimumLineSpacing = 12.0f;
        _collection = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collection.delegate = self;
        _collection.dataSource = self;
        _collection.scrollEnabled = YES;
        _collection.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
        [_collection registerClass:[SearchCategoryTopHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
        [_collection registerNib:[UINib nibWithNibName:@"SearchCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SearchCollectionViewCell"];
    }
    return _collection;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(12, homeListpadding, 12, homeListpadding);// top left bottom right  Cell边界范围
}

#pragma mark === 布局底部按钮

-(void)creatBottomView
{
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = KColorFromRGB(0xededed);
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.collection.mas_bottom).offset(46);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 1));
    }];
    //底部view
    _bottomV = [[UIView alloc]init];
    [self.contentView addSubview:_bottomV];
    [_bottomV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line.mas_bottom);
        make.left.right.bottom.equalTo(self.contentView);
    }];
    
    //重置
    _clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_clearBtn setTitleColor:JColor forState:UIControlStateNormal];
    [_clearBtn setTitle:@"重置" forState:UIControlStateNormal];
    _clearBtn.titleLabel.font = Font(13);
    [_clearBtn addTarget:self action:@selector(clearBtnDown) forControlEvents:UIControlEventTouchUpInside];
    [_bottomV addSubview:_clearBtn];
    [_clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(_bottomV);
        make.width.mas_equalTo((KScreenWidth - 1) / 2);
    }];
    
    UIView *betwLine = [[UIView alloc]init];
    betwLine.backgroundColor = KColorFromRGB(0xededed);
    [_bottomV addSubview:betwLine];
    [betwLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(1, 30));
        make.center.equalTo(_bottomV);
    }];
    
    //确认
    _sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_sureBtn setTitleColor:KColorFromRGB(0xe64f53) forState:UIControlStateNormal];
    [_sureBtn setTitle:@"确认" forState:UIControlStateNormal];
    _sureBtn.titleLabel.font = Font(14);
    [_sureBtn addTarget:self action:@selector(sureBtnDown) forControlEvents:UIControlEventTouchUpInside];
    [_bottomV addSubview:_sureBtn];
    [_sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(betwLine.mas_right);
        make.top.bottom.right.equalTo(_bottomV);
        make.width.mas_equalTo((KScreenWidth - 1) / 2);
    }];
    
    
}

#pragma mark === Action
-(void)clearBtnDown
{
    if (self.clearClick) {
        self.clearClick();
    }
    
    for (SeacherItemModel *clickModel in _selectArr) {
        clickModel.state = NO;
    }
    for (SeacherItemModel *clickModel in _stateSelectArr) {
        clickModel.state = NO;
    }
    [_selectArr removeAllObjects];
    [_stateSelectArr removeAllObjects];
    [self.collection reloadData];
}
//确认
-(void)sureBtnDown
{
    if (self.type == YHSearchTypeAskAndShare) {
        //        if ([self chechPram]) {
        if (self.tipsStateFinishClick) {
            self.tipsStateFinishClick(_selectArr, _stateSelectArr);
        }
        self.view.hidden = YES;
        //        };
    }else{
        //传递值
        if (self.tipsFinishClick) {
            self.tipsFinishClick(_selectArr);
        }
        self.view.hidden = YES;
    }
    
}

-(BOOL)chechPram
{
    if (self.selectArr.count == 0) {
        [WFHudView showMsg:@"请选择类别" inView:nil];
        return NO;
    }if (self.stateSelectArr.count == 0) {
        [WFHudView showMsg:@"请选择状态" inView:nil];
        return NO;
    }
    return YES;
}


@end
