//
//  DownloadWebViewController.h
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

#import "ShareListModel.h"


@interface DownloadWebViewController : YHBaseViewController

@property (nonatomic ,copy) NSString *urlStr;
@property (nonatomic ,copy) NSDictionary *shareDic;

@property (nonatomic,strong) ShareDetailModel *detailModel;

-(instancetype)initWithUrlStr:(NSString *)urlStr andNavTitle:(NSString *)navTitle;

@end
