//
//  FindViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "FindViewController.h"
#import "AwaystationViewController.h"
#import "JiangHuViewController.h"
#import "ShareViewController.h"

#import "YHNavgationController.h"
@interface FindViewController ()<FJSlidingControllerDataSource,FJSlidingControllerDelegate>
@property (nonatomic, strong)NSArray *titles;
@property (nonatomic, strong)NSArray *controllers;
@property (nonatomic, assign)NSInteger index;
@property (nonatomic ,strong) YHBarButtonItem *rightbtn;
@property (nonatomic , strong) NSArray        *cityModelArr;
@property (nonatomic ,strong) NSArray   *dataSource;
@property (nonatomic ,strong) AwaystationViewController *aways;
@end
@implementation FindViewController

-(void)viewDidLoad
{
    NSString *brandId = KgetUserValueByParaName(KBrandInfoId);
    if ([brandId integerValue] > 0) {
        self.isShowLine = NO;
    }else{
        self.isShowLine = YES;
        self.lineW = 20.0;
    }
    [super viewDidLoad];
    self.datasouce = self;
    self.delegate = self;
    [self initalTopSegment];
    [self configController];
    KAddobserverNotifiCation(@selector(reloadDataWithBrandId), NOTIGICATIONFINDVVIEWRELOAD);
}
-(void)reloadDataWithBrandId
{
    NSString *brandId = KgetUserValueByParaName(KBrandInfoId);
    if ([brandId integerValue] > 0) {
        self.isShowLine = NO;
    }else{
        self.isShowLine = YES;
        self.lineW = 20.0;
    }
    [self configController];
    [self reloadData];
}

#pragma mark === 重新布局
-(void)initalTopSegment
{
    self.segTop = KStatusBarHeight;
    [self.segmentTapView removeFromSuperview];
    [self.yh_navigationBar addSubview:self.segmentTapView];
    [self.segmentTapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.bottom.equalTo(self.yh_navigationBar);
        make.height.mas_equalTo(SEGMENTHEIGHT);
    }];
    self.segmentTapView.backgroundColor = [UIColor clearColor];
    self.datasouce = self;
    self.delegate = self;
    
}
-(void)configController{
    
    ShareViewController *share = [[ShareViewController alloc]init];
    share.parentContrl = self;
    
    _aways = [[AwaystationViewController alloc]init];
    _aways.parentContrl = self;
    NSString *brandId = KgetUserValueByParaName(KBrandInfoId);
    if ([brandId integerValue] > 0) {
        self.titles      = @[@"共享"];
        self.controllers = @[share];
        [self addChildViewController:share];
    }else{
        self.titles      = @[@"共享",@"链城小站"];
        self.controllers = @[share,_aways];
        [self addChildViewController:share];
        [self addChildViewController:_aways];
    }
    [self reloadData];
}


#pragma mark dataSouce
- (NSInteger)numberOfPageInFJSlidingController:(FJSlidingController *)fjSlidingController{
    return self.titles.count;
}
- (UIViewController *)fjSlidingController:(FJSlidingController *)fjSlidingController controllerAtIndex:(NSInteger)index{
    return self.controllers[index];
}
- (NSString *)fjSlidingController:(FJSlidingController *)fjSlidingController titleAtIndex:(NSInteger)index{
    return self.titles[index];
}


- (UIColor *)titleNomalColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return JColor;
    
}
- (UIColor *)titleSelectedColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return kNavigationBarColor;
}
- (UIColor *)lineColorInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return kNavigationBarColor;
}
- (CGFloat)titleFontInFJSlidingController:(FJSlidingController *)fjSlidingController {
    return 16.0;
}

#pragma mark delegate
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedIndex:(NSInteger)index{
    // presentIndex
    
    self.title = [self.titles objectAtIndex:index];
    
}

- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedController:(UIViewController *)controller{
    // presentController
}
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedTitle:(NSString *)title{
    // presentTitle
}

- (void)dealloc
{
    KRemoverNotifi(NOTIGICATIONFINDVVIEWRELOAD);
}

@end
