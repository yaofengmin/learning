//
//  CityTool.h
//  pinnc
//
//  Created by Hanks on 16/2/18.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CityModel;
@interface CityTool : NSObject


+(NSArray *)pinying_headArr:(NSArray *)dataSource;
+(NSArray *)cityArr:(NSArray *)dataSource;
+(CityModel *)modelWithName:(NSString *)cityName andSource:(NSArray *)dataSource;
@end
