//
//  CityTool.m
//  pinnc
//
//  Created by Hanks on 16/2/18.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "CityTool.h"
#import "CityModel.h"

@implementation CityTool

static NSMutableArray *_headArr;
static NSMutableArray *_showListArr;
+(void)initialize
{
    
    _headArr = @[].mutableCopy;
    _showListArr =@[].mutableCopy;
}



+(NSArray *)pinying_headArr:(NSArray *)dataSource
{
    NSMutableArray *tempArr = @[].mutableCopy;
    
    for (CityModel *model in dataSource) {
        [tempArr addObject:model.pinying_head];
    }
    
    for (int i = 0 ; i < tempArr.count ; i ++) {
        
        if ([_headArr containsObject:tempArr[i]]==NO) {
            [_headArr addObject:tempArr[i]];
        }
    }
    return [_headArr sortedArrayUsingSelector:@selector(compare:)];
}

+(NSArray *)cityArr:(NSArray *)dataSource
{
    [_showListArr removeAllObjects];
    NSArray *headArr = [self pinying_headArr:dataSource];
    for (int i = 0; i<headArr.count ; i++) {
        NSMutableArray *cityArr = @[].mutableCopy;
        for (CityModel *model in dataSource) {
            if ([headArr[i] isEqualToString:model.pinying_head]) {
                [cityArr addObject:model];
            }
        }
        [_showListArr addObject:cityArr];
    }
    return _showListArr;
}

+(CityModel *)modelWithName:(NSString *)cityName andSource:(NSArray *)dataSource
{
    for (CityModel *model in dataSource) {
        if ([model.cityName isEqualToString:cityName]) {
            return model;
        }
    }
    return nil;
}


@end
