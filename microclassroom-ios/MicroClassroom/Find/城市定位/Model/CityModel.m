//
//  CityModel.m
//  pinnc
//
//  Created by Hanks on 16/2/18.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "CityModel.h"
#import "PinYinForObjc.h"
#define PinyinSeparator @"-"

@implementation CityModel
/**
 *  生成拼音
 */
-(NSString *)pinyin:(NSString *)text
{
    // 拼音格式
    HanyuPinyinOutputFormat *pY = [[HanyuPinyinOutputFormat alloc]init];
    //   小写
    pY.caseType = CaseTypeLowercase;
    // 音调
    pY.toneType = ToneTypeWithoutTone;
    // 类似lv
    pY.vCharType =VCharTypeWithV;
    NSString *pinyin = [PinyinHelper toHanyuPinyinStringWithNSString:text withHanyuPinyinOutputFormat:pY withNSString:PinyinSeparator];
    return pinyin;
}


-(void)setCityName:(NSString *)cityName{
    _cityName = cityName;
    
    NSString *pinyin = [self pinyin:cityName];
    _pinyin = [pinyin stringByReplacingOccurrencesOfString:PinyinSeparator  withString:@""];
    // 替换多音字
    if(cityName.length>1){
        if ([[cityName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"长"]) {
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"C"];
        }else if ([[cityName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"曾"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1)withString:@"Z"];
        }else if ([[cityName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"解"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"X"];
        }else if ([[cityName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"仇"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"Q"];
        }else if ([[cityName substringWithRange:NSMakeRange(0, 1)]    isEqualToString:@"朴"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"P"];
        }else if ([[cityName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"查"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"Z"];
        }else if ([[cityName substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"单"]){
            _pinyin = [_pinyin stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"S"];
        }
    }
    
    NSArray *pinyinArr = [pinyin componentsSeparatedByString:PinyinSeparator];
    NSMutableString *must = [NSMutableString string];
    for (NSString *str in pinyinArr) {
        if (NOEmptyStr(str)) {
            NSString *suStr = [str substringToIndex:1];
            [must appendString:suStr];
        }
        _pinyinHeardZM = must;
        
        NSString *regex = @"[A-Za-z]+";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        NSString *initialStr = [_pinyin length]?[[_pinyin substringToIndex:1] uppercaseString]:@"";
        if ([predicate evaluateWithObject:initialStr])
        {
            _pinying_head= initialStr;
        }else{
            _pinying_head =@"#";
        }
        
    }
    
}
-(NSString*)RemoveSpecialCharacter: (NSString *)str {
    NSRange urgentRange = [str rangeOfCharacterFromSet: [NSCharacterSet characterSetWithCharactersInString: @",.？、 ~￥#&<>《》()[]{}【】^@/￡¤|§¨「」『』￠￢￣~@#&*（）——+|《》$_€-"]];
    if (urgentRange.location != NSNotFound)
    {
        return [self RemoveSpecialCharacter:[str stringByReplacingCharactersInRange:urgentRange withString:@""]];
        
    }
    return str;
}
@end
