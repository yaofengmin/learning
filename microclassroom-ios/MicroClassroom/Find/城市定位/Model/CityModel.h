//
//  CityModel.h
//  pinnc
//
//  Created by Hanks on 16/2/18.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface CityModel : YHBaseModel
/**
 *  城市名称
 */
@property (nonatomic , copy) NSString *cityName;
/**
 *  城市Id
 */
@property (nonatomic , copy) NSString *cityId;
/**
 *  城市全拼首字母
 */
@property (nonatomic , copy) NSString *pinyinHeardZM;
/**
 *  城市全拼
 */
@property (nonatomic , copy) NSString *pinyin;
/**
 *  城市首字母
 */
@property (nonatomic , copy) NSString *pinying_head;

@end
