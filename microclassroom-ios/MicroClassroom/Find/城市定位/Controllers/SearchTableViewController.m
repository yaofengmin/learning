//
//  SearchTableViewController.m
//  pinnc
//
//  Created by Hanks on 16/2/7.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "SearchTableViewController.h"
#import "CityModel.h"
#import "CityTool.h"
@interface SearchTableViewController ()
{
    
    NSArray *_showCitiesArr;
}


@end

@implementation SearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellID"];
    self.tableView.tableFooterView = [[UIView alloc]init];
    
}

-(void)setSearchText:(NSString *)searchText
{
    
    _searchText = searchText.lowercaseString;
    /**
     *  支持 汉字 拼音 字母头 查询
     */
    
    //过滤原始数组
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"cityName contains %@ or pinyin contains %@ or pinyinHeardZM contains %@ or pinying_head contains %@",_searchText,_searchText,_searchText,_searchText];
    
    _showCitiesArr =  [_citis filteredArrayUsingPredicate:pre];
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _showCitiesArr.count;
}



 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     static NSString *CellIndentifier = @"Cell";
     UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIndentifier];
     if (cell==nil) {
         cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIndentifier];
         cell.textLabel.textColor = JColor;
     }
     
     cell.textLabel.text = [_showCitiesArr[indexPath.row] cityName];
 
 return cell;
 }
 

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    CityModel *model = [CityTool modelWithName:cell.textLabel.text andSource:_citis];
    if ([_delegate respondsToSelector:@selector(chooesCity:)]) {
        [_delegate chooesCity:model];
    }
    
}


@end
