//
//  CityChooseViewController.h
//  pinnc
//
//  Created by Hanks on 16/2/7.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "YHBaseViewController.h"
@class CityModel;
@protocol CityChooseViewDelegate  <NSObject>

-(void)chooseCityInfo:(CityModel *)city;

@end

@interface CityChooseViewController : YHBaseViewController
-(instancetype)initWithCityData:(NSArray *)dataArr;
@property(nonatomic ,weak) id <CityChooseViewDelegate> delegate;

@end
