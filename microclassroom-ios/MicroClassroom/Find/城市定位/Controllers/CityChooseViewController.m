//
//  CityChooseViewController.m
//  pinnc
//
//  Created by Hanks on 16/2/7.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "CityChooseViewController.h"
#import "SearchTableViewController.h"
#import "CityModel.h"
#import "CityTool.h"
#import "GpsManager.h"

@interface CityChooseViewController ()<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,SearchTableViewDelegate>
{
    UIView *_conver;
    UITableView *_table ;
    SearchTableViewController *_searchVC;
    NSArray *_cityListModelArr;
    NSArray *_cityDataArr;
    NSArray *_cityHeadArr;
    
    
}

@property (nonatomic , strong) UISearchBar *search;
@property (nonatomic , strong) UIButton    *loctionBtn;
@property (nonatomic , copy)   NSString     *locationCity;


@end

@implementation CityChooseViewController



-(instancetype)initWithCityData:(NSArray *)dataArr
{
    if (self = [super init]) {
        
        _cityListModelArr = dataArr;
        _cityHeadArr = [CityTool pinying_headArr:_cityListModelArr];
        _cityDataArr = [CityTool cityArr:_cityListModelArr];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.title = @"选择城市";
    
    @weakify(self);
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithTitle:@"取消" style:0 handler:^(id sender) {
        @strongify(self);
        [self backBtnAction];
    }];
    
    [self addSearchBar];
    [self addTableView];
    [self getMyLocation];
    
}

#pragma  mark ===获取定位==
-(void)getMyLocation{
    
    @weakify(self);
    [GpsManager getLoctationSuccuss:^(CGFloat lng, CGFloat lat, NSDictionary *addressDic) {
        @strongify(self);
        NSString *city = [addressDic objectForKey:@"City"];
        self.locationCity = city;
        [self layoutWithBtnTitle:self.locationCity];
    } andFaild:^{
        [WFHudView showMsg:@"定位失败" inView:nil];
    }];
    
}

-(void)addTableView
{
    // 表格
    _table = [[UITableView alloc]init];
    _table.delegate  = self;
    _table.dataSource = self;
    _table.tableFooterView = [UIView new];
    [self.view addSubview:_table];
    
    [_table mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight + 39, 0, 0, 0));
    }];
    
    [self.view layoutIfNeeded];
}



-(void)addSearchBar {
    _search = [[UISearchBar alloc]init];
    _search.showsCancelButton = NO;
    _search.frame = CGRectMake(0, KTopHeight - 1, KScreenWidth, 40 );
    _search.barTintColor = kNavigationBarColor;
    _search.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _search.delegate = self ;
    _search.placeholder = @"请输入城市名或者拼音";
    UIImage* searchBarBg = [YHJHelp GetImageWithColor:[UIColor clearColor] andHeight:40];
    //设置背景图片
    [_search setBackgroundImage:searchBarBg];
    //设置背景色
    [_search setBackgroundColor:[UIColor clearColor]];
    //设置文本框背景
    [self setSearchTextFieldBackgroundColor:NDColor];
    UITextField * searchField = [self sa_GetSearchTextFiled];
    searchField.font = Font(12);
    searchField.textColor = JColor;
    
    [self.view addSubview:_search];
}

- (UITextField *)sa_GetSearchTextFiled{
    if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
        return _search.searchTextField;
    }else{
//        UITextField *searchTextField =  [self valueForKey:@"_searchField"];
        return _search.subviews.firstObject.subviews.lastObject;
//        return searchTextField;
    }
}

- (void)setSearchTextFieldBackgroundColor:(UIColor *)backgroundColor
{
    UIView *searchTextField = nil;
    if (IOS7) {
        searchTextField = [[[self.search.subviews firstObject] subviews] lastObject];
    }
    searchTextField.backgroundColor = backgroundColor;
}

//  开始编辑
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    if (!_conver) {
        _conver = [[UIView alloc]initWithFrame:_table.frame];
        _conver.backgroundColor   = [UIColor blackColor];
        _conver.alpha = .5 ;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(converClick)];
        [_conver addGestureRecognizer:tap];
        
    }
    [self.view addSubview:_conver];
}

/**
 *  搜索框的文字改变
 */


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length&&![searchText isEqualToString:@" "]) {  // 显示搜索结果
        if (_searchVC == nil) {
            _searchVC = [[SearchTableViewController alloc]init];
            _searchVC.delegate = self;
            [self addChildViewController:_searchVC];
            _searchVC.citis = _cityListModelArr;
        }
        _searchVC.view.frame = _table.frame;
        _searchVC.view.autoresizingMask = _table.autoresizingMask;
        _searchVC.searchText = searchText;
        [self.view addSubview:_searchVC.view];
        
    }else {
        [_searchVC.view removeFromSuperview];
        [_searchVC removeFromParentViewController];
        _searchVC = nil;
    }
}



-(void)converClick {
    _search.text = @"";
    [_conver removeFromSuperview];
    [_searchVC.view removeFromSuperview];
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section ==0 ) {
        return 1;
    }else
    {
        return [_cityDataArr[section - 1] count];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.section==0) {
        static NSString *cl = @"LocationCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cl];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cl];
            cell.textLabel.textColor = CColor;
        }
        if (!_loctionBtn) {
            _loctionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [_loctionBtn addTarget:self action:@selector(locationBtnDown:) forControlEvents:UIControlEventTouchUpInside];
            _loctionBtn.backgroundColor = KColorRGB(209, 207, 202);
            _loctionBtn.layer.cornerRadius = 5;
            [_loctionBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            _loctionBtn.titleLabel.font = Font(16);
            [cell.contentView addSubview:_loctionBtn];
            [self layoutWithBtnTitle:@"获取定位中"];
        }
        return cell;
    }else {
        static NSString *cl = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cl];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cl];
            cell.textLabel.textColor = CColor;
        }
        cell.textLabel.text = [_cityDataArr[indexPath.section -1][indexPath.row] valueForKey:@"cityName"];
        return cell;
        
    }
    
}


-(void)locationBtnDown:(UIButton *)sender {
    if ([sender.currentTitle isEqualToString:@"获取定位中"]) {
        return;
    }else
    {
        CityModel *model = [CityTool modelWithName:sender.currentTitle andSource:_cityListModelArr];
        if (model) {
            
            if ([_delegate respondsToSelector:@selector(chooseCityInfo:)]) {
                [_delegate chooseCityInfo:model];
            }
            
            [self backBtnAction];
        }else {
            alertContent(@"该城市尚未开通 , 请选择已开通城市");
        }
    }
}

/**
 *  头部的灰色头标
 *
 */
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectZero];
    label.backgroundColor = MainBackColor;
    label.font = Font(15);
    if (section == 0) {
        label.text = @"     您的位置";
    }else {
        
        label.text = [NSString stringWithFormat:@"    %@",[_cityHeadArr objectAtIndex:section-1]];
    }
    return label;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _cityDataArr.count  + 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25;
}


-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    tableView.sectionIndexColor = kNavigationBarColor;
    return _cityHeadArr;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section>0) {
        CityModel *model = [[_cityDataArr objectAtIndex:indexPath.section -1]objectAtIndex:indexPath.row];
        if ([_delegate respondsToSelector:@selector(chooseCityInfo:)]) {
            [_delegate chooseCityInfo:model];
        }
        
        [self backBtnAction];
    }
}


-(void)layoutWithBtnTitle:(NSString *)title {
    
    [_loctionBtn setTitle:title forState:UIControlStateNormal];
    CGFloat width = [YHJHelp sizeWithWidth:KScreenWidth * 0.6 andFont:_loctionBtn.titleLabel.font andString:_loctionBtn.currentTitle].width;
    
    [_loctionBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(0);
        make.left.equalTo(20);
        make.size.equalTo(CGSizeMake(width + 30, 28));
    }];
}

-(void)chooesCity:(CityModel *)modle {
    [self converClick];
    if ([_delegate respondsToSelector:@selector(chooseCityInfo:)]) {
        [_delegate chooseCityInfo:modle];
    }
    [self backBtnAction];
}


@end
