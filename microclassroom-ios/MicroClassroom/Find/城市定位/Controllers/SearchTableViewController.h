//
//  SearchTableViewController.h
//  pinnc
//
//  Created by Hanks on 16/2/7.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CityModel;

@protocol SearchTableViewDelegate <NSObject>

-(void)chooesCity:(CityModel *)modle;

@end

@interface SearchTableViewController : UITableViewController

/**
 *  显示搜索结果
 */

@property (nonatomic ,copy) NSString *searchText;

/**
 *  能进行搜索的城市
 */


@property (nonatomic ,strong) NSArray *citis;

@property (nonatomic ,assign) id <SearchTableViewDelegate> delegate;

@end
