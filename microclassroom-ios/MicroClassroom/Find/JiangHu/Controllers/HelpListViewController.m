//
//  HelpListViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "HelpListViewController.h"
#import "HelpListCell.h"
#import "HelpListModel.h"
#import "ChatViewController.h"
#import "YMTextData.h"
#import "HelpListModel.h"
@interface HelpListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) NSArray *dataArr;
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) YMTextData *data;
@property (nonatomic ,assign) JiangHuHelpType type;
@end

@implementation HelpListViewController


- (instancetype)initWithData:(YMTextData *)data {
    if (self = [super init]) {
        _data = data;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = MainBackColor;
    self.yh_navigationItem.title = @"帮助我的";
    
    switch ([_data.messageBody.helpStatus integerValue]) {
        case 1:{
            _type = HelpIngType;
        }
            break;
        case 2: {
            _type = HelpWorkIng;
        }
            break;
        case 3:{
            _type = HelpSuccessType;
        }
            break;
        case 4:{
            _type = HelpFailType;
        }
            break;
        default:
            break;
    }
    [self creatTabelView];
    [self refreshData];
}
-(void)creatTabelView
{
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [HelpListCell cellHeight];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"HelpListCell";
    HelpListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HelpListCell" owner:nil options:nil]lastObject];
    }
    cell.currentType = _type;
    HelpListModel *model = _dataArr[indexPath.row];
    cell.indextPathRow = indexPath.row;
    [cell configWithModel:model];
   
    @weakify(self);
    cell.helpStateChangeBlock = ^(JiangHuHelpType type){
        @strongify(self);
        if (type != HelpDefaultType) {
               self.type = type;
            if (type == HelpWorkIng) {
                self.data.messageBody.helpStatus = @"2";
            }else {
                self.data.messageBody.helpStatus = @"3";
            }
        }
        [self refreshData];
    };
    
    return cell;
}


- (void)refreshData {
    NSDictionary *paramDic = @{Service:CircleGetHelpListInfo,
                               @"msgId":_data.messageBody.msgId
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _dataArr =   [HelpListModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.tableView reloadData];
                
            }
        }
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HelpListModel *model = _dataArr[indexPath.row];
    //跳转聊天
    ChatViewController * targetVC = [[ChatViewController alloc] initWithConversationChatter:model.hxAccount.username conversationType:EMConversationTypeChat];
    [self.navigationController pushViewController:targetVC animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
