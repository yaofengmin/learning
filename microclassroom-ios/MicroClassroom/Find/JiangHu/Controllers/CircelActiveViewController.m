//
//  CircelActiveViewController.m
//  EnergyConservationPark
//
//  Created by Hanks on 16/6/11.
//  Copyright © 2016年 keanzhu. All rights reserved.
//

#import "CircelActiveViewController.h"
#import "CircleTableHeadView.h"
#import "ActivityModel.h"
#import "AppShareData.h"
#import "CircleActiveCell.h"
#import "ActiveDetailViewController.h"

@interface CircelActiveViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tabel;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,copy) NSString *parkId;
@end

@implementation CircelActiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataArr = @[].mutableCopy;
    _parkId = [ParkManager getCurrentPark].parkId;
    [self creatTabel];
    [self requestData];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([_parkId isEqualToString:[ParkManager getCurrentPark].parkId]) {
        
        return;
    }
    
       _parkId = [ParkManager getCurrentPark].parkId;
        [self requestData];
    
}


-(void)requestData
{

    
    NSDictionary *paramDic = @{@"service":@"beseness.getActivities",
                               @"parkId":_parkId};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                
                _dataArr = [ActivityModel mj_objectArrayWithKeyValuesArray:result[@"list"][@"list"]];
                [self.tabel reloadData];
                
            }else
            {
                [self showMessage:result[REQUEST_MESSAGE]];
            }
            
        }
    }];
}



-(void)creatTabel
{
    CircleTableHeadView *head = [[[NSBundle mainBundle] loadNibNamed:@"CircleTableHeadView" owner:self options:nil]lastObject];
    head.bottomLine.hidden = YES;
    [head clickBtnAtIndext:^(NSInteger indext) {
        
      if (indext == 1){
            
          [self.parentContrl showController:(UIViewController*)self.parentContrl.base];
          [self.parentContrl showRigthBtn:YES];
          
        }
        
    }];
    
    _tabel = [[UITableView alloc] init];
    _tabel.delegate = self;
    _tabel.dataSource = self;
    _tabel.tableFooterView = [[UIView alloc]init];
    _tabel.tableHeaderView = head ;
    [self.view addSubview:_tabel];
    _tabel.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(0);
        
    }];
    
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CircleActiveCell cellHeight];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"CircleActiveCell";
    
    CircleActiveCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell==nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CircleActiveCell" owner:self options:nil]lastObject];
        
    }
    
    
    [cell configWithModel:_dataArr[indexPath.row]];
    
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ActivityModel *model = _dataArr[indexPath.row];
    
    ActiveDetailViewController *act = [[ActiveDetailViewController alloc]initWithActiveID:model.activityId];
    [self.navigationController  pushViewController:act animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
