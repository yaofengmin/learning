//
//  SettingTipsViewController.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"
@class CodeListModel;
@interface SettingTipsViewController : YHBaseViewController

@property(nonatomic,copy) void(^selectFinishBloc)(CodeListModel*model);

@end
