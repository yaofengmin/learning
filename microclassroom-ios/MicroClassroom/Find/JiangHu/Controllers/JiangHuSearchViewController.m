//
//  JiangHuSearchViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "JiangHuSearchViewController.h"
#import "JiangHuSearchCell.h"
#import <MJRefresh.h>
#import "WFMessageBody.h"
#import "CategoryScreenVC.h"
#import "YMTextData.h"
#import "CommentdetailViewController.h"

static NSString * const KJiangHuSearchCell = @"JiangHuSearchCell";
@interface JiangHuSearchViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UISearchBarDelegate
>
@property (strong, nonatomic)  UIView *topView;
@property (strong, nonatomic)  UITableView *tableView;
@property (strong, nonatomic)  NSMutableArray <YMTextData *>* bodyArr;
@property (strong, nonatomic) NSMutableDictionary *paramDic;
@property (strong, nonatomic) CategoryScreenVC *slidebarVC;
@property (strong, nonatomic)  UISearchBar *mySearchBar;
@property (copy ,nonatomic) NSString *keyWords;
@property (copy, nonatomic) NSString *lastMsgIndex;
@end

@implementation JiangHuSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initIvar];
    [self setUpTableView];
}


- (void)pullData {
    
    [self.paramDic setObject:self.keyWords forKey:@"keyWord"];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:self.paramDic andBlocks:^(NSDictionary *result) {
        //业务逻辑层
        if (_lastMsgIndex.intValue == 0) {
            [_bodyArr removeAllObjects];
            [self.tableView.mj_header endRefreshing];
            if (self.tableView.mj_footer.isHidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }else{
            [self.tableView.mj_footer endRefreshing];
        }
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                self.tableView.mj_footer.hidden = NO;
                for (id item in result[REQUEST_LIST]) {
                    YMTextData *ymdata = [[YMTextData alloc]init];
                    ymdata.messageBody = [WFMessageBody mj_objectWithKeyValues:item];
                    [_bodyArr addObject:ymdata];
                }
                _lastMsgIndex = _bodyArr.lastObject.messageBody.msgId;
                [self.tableView.mj_footer endRefreshing];
                [self.tableView reloadData];
                if (_bodyArr.count <= 20) {
                    self.tableView.mj_footer.hidden = YES;
                }else{
                    self.tableView.mj_footer.hidden = NO;
                }
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                self.tableView.mj_footer.hidden = YES;
                [_bodyArr removeAllObjects];
                [self.tableView reloadData];
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
    }];
}



- (void)setUpTableView {
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableHeaderView = self.topView;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefreshAction)];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self
                                                                    refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_footer.hidden = YES;
    [self.tableView registerNib:[UINib nibWithNibName:KJiangHuSearchCell bundle:nil]
         forCellReuseIdentifier:KJiangHuSearchCell];
}


- (void)headRefreshAction {
    [_bodyArr removeAllObjects];
    _lastMsgIndex = @"0";
    [self pullData];
}

- (void)footerRefresh {
    [self pullData];
}


- (void)initIvar {
    self.yh_navigationItem.title  = @"搜索";
    self.keyWords = @"";
     _lastMsgIndex = @"0";
    _bodyArr = [NSMutableArray arrayWithCapacity:1];
    _paramDic     =   @{Service:CircleGetMessages,
                                @"lastId":_lastMsgIndex,
                                @"ctype":@"2",
                                @"classesId":@"0",
                                @"userId":KgetUserValueByParaName(USERID)==nil ? @"" :KgetUserValueByParaName(USERID)}.mutableCopy;
}



- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 44)];
        _mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth- 44, 44)];
        _mySearchBar.showsCancelButton = NO;
        _mySearchBar.delegate = self;
        _mySearchBar.placeholder = @"Search";
        [_topView addSubview:_mySearchBar];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"ic_quanbufenlei"] forState:UIControlStateNormal];
        btn.titleLabel.font = Font(14);
        [btn addTarget:self action:@selector(screenOutBrnDown:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:CColor forState:UIControlStateNormal];
        [_topView addSubview:btn];
        btn.frame = CGRectMake(KScreenWidth - 40, 0, 40, 44);
    }
    return _topView;
}

#pragma mark - 显示侧滑
- (void)screenOutBrnDown:(id)sender {
    [self.view endEditing:YES];
    if (!_slidebarVC) {
        _slidebarVC = [[CategoryScreenVC alloc] initWithSearchType:YHSearchTypeJiangHu];
        _slidebarVC.view.frame  = CGRectMake(0, KTopHeight, self.view.viewWidth, KScreenHeight - KTopHeight);
        [self.view addSubview:_slidebarVC.view];
        
        @weakify(self);
        _slidebarVC.clearClick = ^{
            @strongify(self);
            self.paramDic = @{Service:CircleGetMessages,
                              @"lastId":self.lastMsgIndex,
                              @"ctype":@"2",
                              @"classesId":@"0",
                              @"userId":KgetUserValueByParaName(USERID)==nil ? @"" :KgetUserValueByParaName(USERID)}.mutableCopy;
            [self headRefreshAction];
        };
        
        
        _slidebarVC.tipsFinishClick = ^(NSArray<SeacherItemModel *> * selectArr){
            @strongify(self);
            
            for (SeacherItemModel *mode in selectArr) {
                [self.paramDic setObject:mode.codeId forKey:mode.pareName];
            }
            [self headRefreshAction];
        };
        
        
        _slidebarVC.cancelTip = ^(SeacherItemModel *mode){
            @strongify(self);
            [self.paramDic removeObjectForKey:mode.pareName];
             [self headRefreshAction];
        };
    }
    
    [_slidebarVC showHideSidebar];
}



- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.keyWords = searchText;
    [_bodyArr removeAllObjects];
    _lastMsgIndex = @"0";
    [self pullData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _bodyArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JiangHuSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:KJiangHuSearchCell];
    [cell configWithModel:_bodyArr[indexPath.row]];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    CommentdetailViewController *detailC = [[CommentdetailViewController alloc]initWithShowMessageType:GetMessagesTypeJiangHu andClassId:@""];
//    detailC.ymData = _bodyArr[indexPath.row];
//    [self.navigationController pushViewController:detailC animated:YES];
}

@end
