//
//  HelpListViewController.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"
@class YMTextData;
@interface HelpListViewController : YHBaseViewController

- (instancetype)initWithData:(YMTextData *)data;

@end
