//
//  JiangHuViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/14.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "JiangHuViewController.h"
#import "HelpPublishViewController.h"
#import "YHNavgationController.h"
#import "RDVTabBarController.h"
#import "MWPhotoBrowser.h"
#import "CommentdetailViewController.h"
#import "PersonHomeViewController.h"
#import "JiangHuSearchViewController.h"
#import "YHBaseWebViewController.h"

@interface JiangHuViewController ()<MWPhotoBrowserDelegate,UISearchBarDelegate>
@property (nonatomic ,strong) UISearchBar *mySearchBar;
@end

@implementation JiangHuViewController


-(void)viewDidAppear:(BOOL)animated
{
    if (self.ymData) {
        
        [self reloadOneRowsWithData:self.ymData andRow:self.row];
    }
    [self.view endEditing:YES];
    [super viewDidAppear:animated];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configData];
    self.isShowPublishBtn = YES;
    self.mainTable.tableHeaderView = self.mySearchBar;
    self.mainTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.mainTable.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    self.mainTable.mj_footer.hidden = YES;
    [self loadDataForHeader];
    
    
    @weakify(self);
    [self.publishBtn handleEventTouchUpInsideWithBlock:^{
        @strongify(self);
        if ([YHJHelp isLoginAndIsNetValiadWitchController:self.parentContrl]) {
            HelpPublishViewController *publish = [[HelpPublishViewController alloc]initWithPubilshType:GetMessagesTypeJiangHu];
            YHNavgationController *publishNav = [[YHNavgationController alloc]initWithRootViewController:publish];
            [self.parentContrl presentViewController:publishNav animated:YES completion:nil];
        }
    }];
}


-(void)configData
{
    self.photos            = @[].mutableCopy;
    self.thumbs            = @[].mutableCopy;
}


#pragma mark - 图片点击事件回调
- (void)showImageViewWithImageViews:(NSArray *)imageViews byClickWhich:(NSInteger)clickTag{
    
    [[self.parentContrl rdv_tabBarController] setTabBarHidden:YES animated:YES];
    
    [self.photos removeAllObjects];
    [self.thumbs removeAllObjects];
    
    for ( NSString *thumbUrl in imageViews) {
        
        [self.thumbs addObject:[MWPhoto photoWithURL:[NSURL URLWithString:thumbUrl]]];
        [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[thumbUrl  stringByReplacingOccurrencesOfString:@"_thumb" withString:@""]]]];
    }
    
    BOOL displayActionButton = YES; //分享按钮,默认是
    BOOL displaySelectionButtons = NO; //是否显示选择按钮在图片上,默认否
    BOOL displayNavArrows = NO;  //左右分页切换,默认否
    BOOL enableGrid = NO; //是否允许用网格查看所有图片,默认是
    BOOL startOnGrid = NO; //是否第一张,默认否
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = displayActionButton;
    browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = displaySelectionButtons; //控制条件控件 是否显示,默认否
    browser.zoomPhotosToFill = NO; //是否全屏,默认是
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = YES;
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    browser.wantsFullScreenLayout = YES;//是否全屏
#endif
    
    [browser setCurrentPhotoIndex:clickTag];
    
    if (displaySelectionButtons) {
        self.selections = [NSMutableArray new];
        for (int i = 0; i < self.photos.count; i++) {
            [self.selections addObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        // Push
        [self.parentContrl.navigationController pushViewController:browser animated:YES];
        
    } else {
        // Modal
        YHNavgationController *nc = [[YHNavgationController alloc] initWithRootViewController:browser];
        nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.parentContrl presentViewController:nc animated:YES completion:nil];
    }
    
}


#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < self.thumbs.count)
        return [self.thumbs objectAtIndex:index];
    return nil;
}


- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return [[self.selections objectAtIndex:index] boolValue];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.row = indexPath.row;
    self.ymData = (YMTextData *)[self.tableDataSource objectAtIndex:indexPath.row];
    CommentdetailViewController *detailC = [[CommentdetailViewController alloc]initWithShowMessageType:GetMessagesTypeJiangHu andClassId:@""];
    detailC.ymData = self.ymData;
    [self.parentContrl.navigationController pushViewController:detailC animated:YES];
    @weakify(self);
    detailC.deletedMsg = ^(YMTextData *ymd , NSInteger indext) {
        @strongify(self);
        NSIndexSet *set = [NSIndexSet indexSetWithIndex:indexPath.section];
        [self.tableDataSource removeObject:ymd];
        [self.mainTable deleteSections:set withRowAnimation:UITableViewRowAnimationLeft];
    };
}


-(void)loadDataForHeader
{
    self.ymData = nil;
    [super loadDataForHeader];
}

-(void)loadMoreData
{
    [super loadMoreData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --- 头像点击事件    跳转详情
-(void)userPhotoImageClick:(NSInteger)index adnUsrId:(NSInteger)userId{
    
    PersonHomeViewController *userHome = [[PersonHomeViewController alloc]init];
    userHome.userId = (int)userId;
    [self.parentContrl.navigationController pushViewController:userHome animated:YES];
}


#pragma mark === 完成发布delegate ==
-(void)finishPublish
{
    [self loadDataForHeader];
}



#pragma mark -searchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
    JiangHuSearchViewController *search = [[JiangHuSearchViewController alloc]init];
    [self.parentContrl.navigationController pushViewController:search animated:YES];
    return NO;
}


- (UISearchBar *)mySearchBar {
    if (!_mySearchBar) {
        _mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 36)];
        _mySearchBar.placeholder = @"点击搜索标签";
        _mySearchBar.delegate = self;
        
    }
    return _mySearchBar;
}


@end
