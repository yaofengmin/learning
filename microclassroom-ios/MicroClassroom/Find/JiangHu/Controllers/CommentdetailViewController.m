//
//  CommentdetailViewController.m
//  CarService
//
//  Created by apple on 15/7/30.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "CommentdetailViewController.h"
#import "commentTableViewDataSouce.h"
#import "commentTableViewDelegate.h"
#import "MicroClassCommentTableViewCell.h"
#import "YMReplyInputView.h"
#import "WFTextView.h"
#import "PersonHomeViewController.h"
//#import "HelpListViewController.h"
//#import "HelpListModel.h"
#import "FriendModel.h"
#import "MWPhotoBrowser.h"
#import "YHNavgationController.h"
#import "RDVTabBarController.h"
//#import "BuyVIPViewController.h"
#import "CommentToolBar.h"
#import "UserInfoModel.h"

#import "YHBaseWebViewController.h"

static int reloadCount;
@interface CommentdetailViewController ()<WFCoretextDelegate,CommentToolbarDelegate,MWPhotoBrowserDelegate>{
    
    UITableView     *_commentTable;
    NSMutableArray  *_commentDataSouce;
    NSNumber        *_reReplyUseId ;  // 被回复的userId
    NSString        *_toUserId;
    NSString        *_toUserName;
    GetMessagesType _type;
}


@property (nonatomic,strong)id<UITableViewDataSource>dataSouce;
@property (nonatomic,strong)id<UITableViewDelegate>delegate;
@property (nonatomic,strong) FriendModel *friendModel;
@property (nonatomic,strong) CommentToolBar *toolBar;


@end

@implementation CommentdetailViewController


-(instancetype)initWithShowMessageType:(GetMessagesType)type andClassId:(NSString *)classId {
    
    _type = type;
    
    return [super initWithShowMessageType:type andClassId:classId];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.ymData.messageBody.showCommentTextView = YES; // 关闭评论
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.ymData.messageBody.showCommentTextView = NO;
    
    //MARK: 创建friend
    
    _friendModel = [[FriendModel alloc]init];
    _friendModel.userId = self.ymData.messageBody.userId;
    _friendModel.userName = self.ymData.messageBody.userName;
    _friendModel.pic = self.ymData.messageBody.userPhoto;
    
    self.isShowPublishBtn = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toReply:) name:FriendReply object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(headImageViewClick:) name:@"commentListHeadClick" object:nil];
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"manTableFresh"];
    
    reloadCount = 0;
    _toUserId                  = @"";
    _toUserName                = @"";
    _commentDataSouce             = [NSMutableArray arrayWithCapacity:0];
    
    self.yh_navigationItem.title = @"详情";
    [self selfConfig];
    [self initalKeyBoard];
    [self creatRightItem];
    
}
-(void)initalKeyBoard
{
    CGFloat chatbarHeight = [CommentToolBar defaultHeight];    self.toolBar = [[CommentToolBar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - chatbarHeight, self.view.frame.size.width, chatbarHeight) buttonIsHidden:YES backIsHidden:YES];
    self.toolBar.delegate = self;
    self.toolBar.backBtn.hidden = YES;
    self.toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:self.toolBar];
}
-(void)creatRightItem
{
    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"note_share"] style:YHBarButtonItemStylePlain handler:^(id send) {
        [[YHJHelp shareInstance]  showShareInController:self andShareURL:[NSString stringWithFormat:@"%@%@",[InitConfigModel shareInstance].kejian_url,self.ymData.messageBody.msgId] andTitle:@"学习兴业" andShareText:self.ymData.messageBody.content andShareImage:nil isShowStudyGroup:NO isDIYUrl:YES];
    }];
}
-(void)selfConfig{
    if (self.ymData) {
        [self.tableDataSource addObject:self.ymData];
        [self calculateHeight:self.tableDataSource];
        self.mainTable.separatorColor =[UIColor clearColor];
        [self.mainTable removeFromSuperview];
    }
    [self requestCircleInfo];
    [self requestComment];
    
    
    // 创建tableView
    _commentTable                 = [[UITableView alloc]init];
    _commentTable.tableFooterView = [[UIView alloc]init];
    //    _commentTable.estimatedRowHeight = 50;
    self.mainTable.frame = CGRectMake(0, 0, self.view.frame.size.width, 250);
    _commentTable.tableHeaderView = self.mainTable;
    _commentTable.backgroundColor = NCColor;
    self.delegate                 = [[commentTableViewDelegate alloc]initWithData:_commentDataSouce];
    self.dataSouce                = [[commentTableViewDataSouce alloc]initWithData:_commentDataSouce];
    _commentTable.delegate        = self.delegate;
    _commentTable.dataSource      = self.dataSouce;
    [self.view addSubview:_commentTable];
    CGFloat offset = ([[UIApplication sharedApplication] statusBarFrame].size.height>20?10:0);
    [_commentTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(KTopHeight, 0, 50 + offset, 0));
    }];
    [_commentTable registerClass:[MicroClassCommentTableViewCell class] forCellReuseIdentifier:@"MicroClassCommentCellID"];
    
    
}


-(void)toReply:(NSNotification*)noti{
    WFReplyBody *model = [noti  object];
    NSString *useId         = model.userId;
    NSString *useName       = model.userName;
    if ([useId isEqualToString:KgetUserValueByParaName(USERID)]) {
        UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"确定要删除该条评论吗?" cancelButtonItem:[RIButtonItem itemWithLabel:@"取消" action:^{
            
        }] destructiveButtonItem:[RIButtonItem itemWithLabel:@"删除" action:^{
            NSDictionary *paramDic = @{Service:CircleDeleteCommentMessage,
                                       @"userId":KgetUserValueByParaName(USERID),
                                       @"commentId":model.commentId
                                       };
            
            SHOWHUD;
            [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                HIDENHUD;
                if (result) {  // 删除一条评论
                    if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                        [self requestComment];
                    }
                }
            }];
        }] otherButtonItems:nil, nil];
        [sheet showInView:self.view];
        _toUserId           = @"";
        self.toolBar.inputTextView.placeHolder = [NSString stringWithFormat:@"写评论..."];
        return;  // 如果是自己
    }
    
    if (self.toolBar) {
        self.toolBar.inputTextView.placeHolder = [NSString stringWithFormat:@"回复%@",useName];
        _toUserId           = useId;
        [self.toolBar.inputTextView becomeFirstResponder];
        return;
    }
    
    _toUserId =useId;
    if (self.replyIndex  != -1) { //直接评论
        
        self.toolBar.inputTextView.placeHolder = [NSString stringWithFormat:@"回复%@",useName];
    }
    [self.toolBar.inputTextView becomeFirstResponder];
    
    
}

- (void)replyAction:(YMButton *)sender{
    
    //    if (self.replyView) {
    //        return;
    //    }
    //    self.replyView     = [[YMReplyInputView alloc] initWithFrame:CGRectMake(0, self.view.viewHeight - 49, KScreenWidth,49) andAboveView:self.view];
    //    self.replyView.delegate = self;
    //    self.replyView.replyTag = sender.appendIndexPath.row;
    //    [self.view addSubview:self.replyView];
}


-(void)showReplyView {
    
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        if (self.toolBar) {
            return;
        }
        if (self.replyIndex  != -1) { //直接评论
            
        }
        
    }
}
#pragma mark === 投票成功
-(void)addMessagesQusAFinish{
    [self requestCircleInfo];
}


#pragma mark ---------- 评论请求
-(void)requestComment{
    
    [_commentDataSouce removeAllObjects];
    NSMutableDictionary *sendDic  =  [[NSMutableDictionary alloc]init];
    [sendDic setObject:CircleGetMessageComments forKey:Service];
    [sendDic setObject:self.ymData.messageBody.msgId forKey:@"msgId"];
    [sendDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                NSArray *listArr           = [result objectForSafeKey:REQUEST_LIST];
                if (listArr.count) {
                    reloadCount++;
                    for (NSDictionary *modelDic in listArr) {
                        WFReplyBody *model = [[WFReplyBody alloc] init];
                        [model setValuesForKeysWithDictionary:modelDic];
                        model.publishId    = self.ymData.messageBody.userId;
                        model.msgId  = self.ymData.messageBody.msgId;
                        [_commentDataSouce addObject:model];
                        
                    };
                    
                    self.ymData.messageBody.comments = _commentDataSouce;
                    self.ymData.messageBody.commentCount = [NSString stringWithFormat:@"%ld",self.ymData.messageBody.comments.count];
                    [self reloadMyTableVie];
                    [self reloadTable];
                }else{
                    [_commentDataSouce removeAllObjects];
                    self.ymData.messageBody.comments = _commentDataSouce;
                    [_commentTable reloadData];
                }
                
            }else{
                [_commentDataSouce removeAllObjects];
                self.ymData.messageBody.comments = _commentDataSouce;
                self.ymData.messageBody.commentCount = [NSString stringWithFormat:@"%ld",self.ymData.messageBody.comments.count];
                [_commentTable reloadData];
                [self reloadTable];
            }
            KPostNotifiCation(CircleListReloadOneData, nil);
        }
    }];
    
}

#pragma mark === 获取当前动态详情
-(void)requestCircleInfo
{
    NSMutableDictionary *sendDic  =  [[NSMutableDictionary alloc]init];
    [sendDic setObject:CircleGetMessageInfo forKey:Service];
    [sendDic setObject:self.ymData.messageBody.msgId forKey:@"msgId"];
    [sendDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                WFMessageBody *model = [WFMessageBody   mj_objectWithKeyValues:result[REQUEST_INFO]];
                self.ymData.messageBody = model;
                [self reloadTable];
                KPostNotifiCation(CircleListReloadOneData, nil);
            }
        }
    }];
    
}

-(void)reloadMyTableVie{
    [_commentTable reloadData];
//    if (reloadCount>=2) {
//        [_commentTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_commentDataSouce.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
//    }
    
}


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    KRemoverNotifi(FriendReply);
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.tableDataSource.count) {
        if (indexPath.row>self.tableDataSource.count) {
            return 0;
        }
        
        YMTextData *ym = [self.tableDataSource objectAtIndex:indexPath.row];
        
        BOOL unfold = ym.foldOrNot;
        if ([ym.messageBody.ctype isEqualToString:@"2"]) {
            self.cellH =  kVoteBottom  + (ym.islessLimit?0:30) + ym.showVoteOptionHeight + (unfold?ym.shuoshuoHeight:ym.unFoldShuoHeight) + ym.labelHeight + kTopViewHeight + kCommentH+ (ym.showOptipnArray.count?kOpDistance:0)+ kOpToBottom;
        }else if ([ym.messageBody.ctype isEqualToString:@"3"]){
            self.cellH = kTopViewHeight + kCommentH + 50 + kLocationToBottom +  ym.labelHeight + (ym.labelHeight == 0?0:kDistance);
        }
        else{
            if (ym.showImageArray.count!=1) {
                self.cellH = kTopViewHeight + kLocationToBottom  + ym.showImageHeight  + (ym.showImageArray.count != 0?kDistance:0) + (ym.islessLimit?0:25) + (unfold?ym.shuoshuoHeight:ym.unFoldShuoHeight) + CircleBottomViewH + (ym.labelHeight == 0?0:10) + ym.labelHeight+(ym.showFileArray.count?kOpToBottom:0) + ym.showFileHeight + kCommentH;
            }else
            {
                float image_heght = 100;
//                if ([ym.showImageArray[0] isKindOfClass:[NSString class]]) {
//                    NSString *urlStr =  ym.showImageArray[0];
//
//                    NSArray  *tempArr = [urlStr componentsSeparatedByString:@"_"];
//                    if (tempArr.count>2) {
//                        // 排列单张
//
//                        image_heght =  ceil([tempArr[1]  floatValue]/2);
//                    }
//                }
                
                self.cellH = kTopViewHeight + kLocationToBottom  + image_heght  + kDistance + (ym.islessLimit?0:25) + (unfold?ym.shuoshuoHeight:ym.unFoldShuoHeight)+(ym.labelHeight == 0?0:10) + CircleBottomViewH + ym.labelHeight+(ym.showFileArray.count?kOpToBottom:0) + ym.showFileHeight + kCommentH;
            }
        }
    }
    [self changeHeight:self.cellH];
    
    return self.cellH;
}



- (void)calculateHeight:(NSMutableArray *)dataArray{
    
    for (YMTextData *ymData in dataArray) {
        
        ymData.shuoshuoHeight   = [ymData calculateShuoshuoHeightWithWidth:self.view.frame.size.width withUnFoldState:NO];//折叠
        
        ymData.unFoldShuoHeight = [ymData calculateShuoshuoHeightWithWidth:self.view.frame.size.width withUnFoldState:YES];//展开
        
        ymData.replyHeight = 0;
        
        if (ymData.messageBody.showCommentTextView) {
            
            ymData.replyHeight = [ymData calculateReplyHeightWithWidth:KScreenWidth];
        }
        
    }
    
    [self.mainTable reloadData];
    //    CGRect tableFrame            = self.mainTable.frame;
    //    tableFrame.size.height       = self.cellH ;
    //    [self.mainTable setFrame:tableFrame];
    self.mainTable.scrollEnabled = NO;
    
    
}


- (void)clickWFCoretext:(NSString *)clickString replyIndex:(NSInteger)index
{
    
}

- (void)longClickWFCoretext:(NSString *)clickString replyIndex:(NSInteger)index
{
    
}

- (void)YMReplyInputWithReply:(NSString *)replyText appendTag:(NSInteger)inputTag{
    
    if (![YHJHelp isReachable]) {
        
        return;
    }
    
    NSMutableDictionary *sendDic = [NSMutableDictionary dictionaryWithCapacity:0];
    [sendDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
    [sendDic setObject:replyText forKey:@"content"];
    [sendDic setObject:self.ymData.messageBody.msgId forKey:@"msgId"];
    [sendDic setObject:_toUserId forKey:@"toUserId"];
    [sendDic setObject:CircleCommentMessage forKey:Service];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                
                [self calculateHeight:self.tableDataSource];
                [self requestComment];
            }else
            {
                [WFHudView showMsg:@"评论失败" inView:nil];
            }
        }
        
    }];
    
}

-(void)changeHeight:(CGFloat)cellh
{
    if (!_commentTable) {
        return;
    }
    
    CGRect tableFrame            = self.mainTable.frame;
    tableFrame.size.height       = cellh;
    [self.mainTable setFrame:tableFrame];
    _commentTable.tableHeaderView = self.mainTable;
    
}


#pragma mark   == 点赞完成====
-(void)addFinshWithBody:(WFMessageBody *)body
{
    YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:self.selectedIndexPath];
    ymData.messageBody = body;
    ymData.messageBody.showCommentTextView = NO;
    [ymData.attributedDataFavour removeAllObjects];
    [self reloadOneRowsWithData:ymData andRow:self.selectedIndexPath];
}

#pragma mark === 取消赞的操作
-(void)cancelFinshWithBody:(WFMessageBody *)body
{
    YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:self.selectedIndexPath];
    ymData.messageBody.showCommentTextView = NO;
    ymData.messageBody = body;
    [ymData.attributedDataFavour removeAllObjects];
    [self reloadOneRowsWithData:ymData andRow:self.selectedIndexPath];
}

#pragma mark == 评论头像点击==
-(void)headImageViewClick:(NSNotification *)noti{
    
    NSString *userId     = [[noti    object] objectForKey:USERID];
    PersonHomeViewController *userHome = [[PersonHomeViewController alloc]init];
    userHome.userId = [userId intValue];
    [self.navigationController pushViewController:userHome animated:YES];
    
}

#pragma mark --- 头像点击事件    跳转详情
-(void)userPhotoImageClick:(NSInteger)index adnUsrId:(NSInteger)userId{
    PersonHomeViewController *userHome = [[PersonHomeViewController alloc]init];
    userHome.userId = (int)userId;
    [self.navigationController pushViewController:userHome animated:YES];
}


#pragma mark - 图片点击事件回调
- (void)showImageViewWithImageViews:(NSArray *)imageViews byClickWhich:(NSInteger)clickTag{
    
    [[self rdv_tabBarController] setTabBarHidden:YES animated:YES];
    
    self.photos            = @[].mutableCopy;
    self.thumbs            = @[].mutableCopy;
    for ( NSString *thumbUrl in imageViews) {
        
        [self.thumbs addObject:[MWPhoto photoWithURL:[NSURL URLWithString:thumbUrl]]];
        [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[thumbUrl  stringByReplacingOccurrencesOfString:@"_thumb" withString:@""]]]];
    }
    
    BOOL displayActionButton = YES; //分享按钮,默认是
    BOOL displaySelectionButtons = NO; //是否显示选择按钮在图片上,默认否
    BOOL displayNavArrows = NO;  //左右分页切换,默认否
    BOOL enableGrid = NO; //是否允许用网格查看所有图片,默认是
    BOOL startOnGrid = NO; //是否第一张,默认否
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = displayActionButton;
    browser.displayNavArrows = displayNavArrows;
    browser.displaySelectionButtons = displaySelectionButtons;
    browser.alwaysShowControls = displaySelectionButtons; //控制条件控件 是否显示,默认否
    browser.zoomPhotosToFill = NO; //是否全屏,默认是
    browser.enableGrid = enableGrid;
    browser.startOnGrid = startOnGrid;
    browser.enableSwipeToDismiss = YES;
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    browser.wantsFullScreenLayout = YES;//是否全屏
#endif
    
    [browser setCurrentPhotoIndex:clickTag];
    
    if (displaySelectionButtons) {
        self.selections = [NSMutableArray new];
        for (int i = 0; i < self.photos.count; i++) {
            [self.selections addObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        // Push
        [self.navigationController pushViewController:browser animated:YES];
        
    } else {
        // Modal
        YHNavgationController *nc = [[YHNavgationController alloc] initWithRootViewController:browser];
        nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:nc animated:YES completion:nil];
    }
    
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < self.thumbs.count)
        return [self.thumbs objectAtIndex:index];
    return nil;
}


- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return [[self.selections objectAtIndex:index] boolValue];
}
#pragma mark ===点击评论 回复评论==
- (void)clickRichText:(NSInteger)index replyIndex:(NSInteger)replyIndex
{
    self.replyIndex = replyIndex;
    
    YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:index];
    
    WFReplyBody *b = nil;
    if (self.replyIndex  != -1) {
        
        b = [ymData.messageBody.comments objectAtIndex:replyIndex];
        NSString *userId = [UserInfoModel getInfoModel].userId;
        
        if ([b.userId isEqualToString:userId]) {
            [self destorySelf];
            UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"确定要删除该条评论吗?" cancelButtonItem:[RIButtonItem itemWithLabel:@"取消" action:^{
                
            }] destructiveButtonItem:[RIButtonItem itemWithLabel:@"删除" action:^{
                NSDictionary *paramDic = @{Service:CircleDeleteCommentMessage,
                                           @"userId":KgetUserValueByParaName(USERID),
                                           @"commentId":b.commentId
                                           };
                
                SHOWHUD;
                [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                    HIDENHUD;
                    if (result) {  // 删除一条评论
                        if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                            WFMessageBody *m = ymData.messageBody;
                            NSMutableArray *newComment = m.comments.mutableCopy;
                            [newComment removeObjectAtIndex:replyIndex];
                            m.comments = newComment;
                            ymData.messageBody = m;
                            [self reloadOneRowsWithData:ymData andRow:index];
                        }
                    }
                }];
            }] otherButtonItems:nil, nil];
            [sheet showInView:self.view];
            return;
        }
    }
    
    if (self.replyIndex  != -1) { //直接评论
        
        self.toolBar.inputTextView.placeHolder = [NSString stringWithFormat:@"回复 %@",b.userName];
    }else{
        self.toolBar.inputTextView.placeHolder = @"写评论...";
    }
    [self.toolBar.inputTextView becomeFirstResponder];
    
}

#pragma  mark - 删除说说
-(void)delectMsg:(YMTextData *)ymd andIndext:(NSInteger)indext{
    
    if (self.deletedMsg) {
        self.deletedMsg(ymd,indext);
    }
    [self backBtnAction];
}


-(void)didSendText:(NSString *)text
{
    [self.view endEditing:YES];
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        NSMutableDictionary *sendDic = [NSMutableDictionary dictionaryWithCapacity:0];
        [sendDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
        [sendDic setObject:text forKey:@"content"];
        [sendDic setObject:self.ymData.messageBody.msgId forKey:@"msgId"];
        [sendDic setObject:_toUserId forKey:@"toUserId"];
        [sendDic setObject:CircleCommentMessage forKey:Service];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    _toUserId = @"";
                    self.toolBar.inputTextView.placeHolder = @"写评论...";
                    [self calculateHeight:self.tableDataSource];
                    [self requestComment];
                }else
                {
                    [WFHudView showMsg:@"评论失败" inView:nil];
                }
            }
            
        }];
        
    }
}

-(void)cancelCollectionbWithBody:(WFMessageBody *)body stamp:(NSIndexPath *)stamp
{
    if (self.messageType == GetMessagesTypeCollection){
        //        YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:stamp.row];
        //        ymData.messageBody = body;
        //        //        [ymData.attributedDataFavour removeAllObjects];
        //        [_tableDataSource removeObject:ymData];
        //        [self.mainTable reloadData];
        KPostNotifiCation(CircleListReloadAllData, nil);
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:stamp.row];
        ymData.messageBody = body;
        //        [ymData.attributedDataFavour removeAllObjects];
        [self.tableDataSource replaceObjectAtIndex:stamp.row withObject:ymData];
        //        NSIndexSet *set = [NSIndexSet indexSetWithIndex:stamp.section];
        [self.mainTable reloadData];
        KPostNotifiCation(CircleListReloadOneData, nil);
    }
}

-(void)showFileViewWithFiles:(NSArray *)files byClickWhich:(NSInteger)clickTag
{
    NSArray *fileNameArr = [files[clickTag] componentsSeparatedByString:@"_"];
    NSString *fileName = fileNameArr.lastObject;
    FileWebViewController *web = [[FileWebViewController alloc]initWithUrlStr:files[clickTag] andNavTitle:fileName];
    [self.navigationController pushViewController:web animated:YES];
}

#pragma mark === 展示web
-(void)showWebViewWithlink:(NSString *)linkUrl title:(NSString *)title index:(NSInteger)stamp
{
    if ([linkUrl hasPrefix:@"http"]) {
        YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:linkUrl andNavTitle:title];
        [self.navigationController pushViewController:web animated:YES];
    }else{
        if ([[UserInfoModel getInfoModel].grade integerValue] < 2) {//共享文件 需要 grade >= 2
            @weakify(self);
            [YHJHelp aletWithTitle:@"提示" Message:@"VIP会员等级不足，不能查看" sureTitle:@"立即升级" CancelTitle:@"忽略 " SureBlock:^{
                @strongify(self);
                BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                [self.navigationController pushViewController:buy animated:YES];
                
            } andCancelBlock:nil andDelegate:self];
            return;
        }
        [self requestDetailNewsWithNewId:linkUrl];
    }
}

#pragma mark === 共享详情
-(void)requestDetailNewsWithNewId:(NSString *)newsId
{
    NSDictionary *paramDic = @{Service:GetNewsInfo,
                               @"newsId":newsId,
                               @"user_Id":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                ShareDetailModel *model = [ShareDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                DownloadWebViewController *detailVC = [[DownloadWebViewController alloc]initWithUrlStr:model.filePathShow andNavTitle:model.newsName];
                detailVC.detailModel = model;
                [self.navigationController pushViewController:detailVC animated:YES];
                
            }
        }
        
    }];
}
@end
