//
//  SettingTipsViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "SettingTipsViewController.h"
#import "CodeListModel.h"
#import "ChineseInclude.h"
#import "PinYinForObjc.h"
@interface SettingTipsViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UISearchBarDelegate,
UISearchDisplayDelegate
>
{
    NSMutableArray *searchResults;
    UISearchBar *mySearchBar;
    UISearchDisplayController *searchDisplayController;
}

@property(nonatomic ,strong) NSArray *dataArr;
@property(nonatomic ,strong) UITableView *tableView;

@end

@implementation SettingTipsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    searchResults = @[].mutableCopy;
    self.yh_navigationItem.title = @"设置标签";
    
    [self requestTipListWithKeyWorks:nil];
    
    // Do any additional setup after loading the view.
}

-(void)creatTabelView
{
    
    mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
    mySearchBar.delegate = self;
    mySearchBar.placeholder = @"Search";
    
    searchDisplayController = [[UISearchDisplayController alloc]initWithSearchBar:mySearchBar contentsController:self];
    searchDisplayController.active = NO;
    searchDisplayController.searchResultsDataSource = self;
    searchDisplayController.searchResultsDelegate = self;
    searchDisplayController.delegate = self;
    
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableHeaderView = mySearchBar;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}



-(void)requestTipListWithKeyWorks:(NSString *)keyWords
{
    
    if (keyWords == nil) {
        keyWords = @"";
    }
    
    NSDictionary *paramDic = @{Service:AppGetCodeList,
                               @"ctype":@"6",
                               @"keyWord":keyWords
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                
                _dataArr = [CodeListModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                
                [self creatTabelView];
            }
        }
    }];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
       
        return searchResults.count;
        
    }else
    {
          return _dataArr.count;
    }

}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:0 reuseIdentifier:cellId];
        cell.textLabel.font = Font(14);
        cell.textLabel.textColor = CColor;
    }
    
    CodeListModel *model = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
         model = searchResults[indexPath.row];
        
    }else
    {
         model = _dataArr[indexPath.row];
    }
    
   
    
    cell.textLabel.text = model.codeName;
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self backBtnAction];
    if (self.selectFinishBloc) {
        
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            
            self.selectFinishBloc(searchResults[indexPath.row]);
            
        }else
        {
            
            self.selectFinishBloc(_dataArr[indexPath.row]);
        }
        
    }
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    searchResults = @[].mutableCopy;
    if (mySearchBar.text.length > 0 && ![ChineseInclude isIncludeChineseInString:searchText]) {
        
        for (int i = 0; i< self.dataArr.count; i++) {
            if ([ChineseInclude isIncludeChineseInString:[self.dataArr[i] codeName]]) {
                
                NSString *tempPinYinStr = [PinYinForObjc chineseConvertToPinYin:[self.dataArr[i] codeName]];
                NSRange titleResult=[tempPinYinStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
                if (titleResult.length>0) {
                    [searchResults addObject:self.dataArr[i]];
                }
            }
        }
        
    }else if(mySearchBar.text.length > 0 && [ChineseInclude isIncludeChineseInString:searchText])
    {
    
        for (CodeListModel *model in self.dataArr) {
         
            NSString *name = model.codeName;
            NSRange result = [name rangeOfString:searchBar.text options:NSCaseInsensitiveSearch];
            if (result.length>0) {
                [searchResults addObject:model];
            }
            
        }
    
    }
}


- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    controller.searchResultsTableView.contentInset = UIEdgeInsetsMake(0.0f, 0.f, 0.f, 0.f);
    tableView.frame = CGRectMake(0,KTopHeight+40,KScreenWidth,KScreenHeight - KTopHeight -40); //
    
    tableView.tableFooterView = [[UIView alloc]init];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
