//
//  YHCirleBaseViewController.h
//  yunbo2016
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHBaseViewController.h"
#import "WFMessageBody.h"
#import "YMTextData.h"
#import "WFReplyBody.h"
#import "YMTableViewCell.h"
//#import "UserInfoVC.h"
#import "CirleRequstTool.h"
#import "YMButton.h"
#import "YMReplyInputView.h"
#import "PublishChooseView.h"


@interface YHCirleBaseViewController : YHBaseViewController<InputDelegate,cellDelegate>

/**
 *  服务器返回的数据
 */
@property (nonatomic ,strong) NSMutableArray *tableDataSource;


/**
 *  处理完毕用来显示的数据源
 */
@property (nonatomic ,strong) NSMutableArray *contentDataSource;
@property (nonatomic,assign) NSInteger selectedIndexPath;
@property (nonatomic ,assign) CGFloat cellH;
@property (nonatomic ,strong) UITableView *mainTable;
@property (assign, nonatomic) int             pageIndex; /**<  分页下标  */
//@property (nonatomic ,strong) YMReplyInputView *replyView ;
@property (nonatomic,strong) UIButton *publishBtn;
@property (nonatomic,strong) UIButton *publishVoteBtn;
@property (assign, nonatomic) NSInteger replyIndex; //回复  -1代表直接评论
/** 是否显示发布按钮*/
@property (assign ,nonatomic) BOOL isShowPublishBtn;

/**
 是否显示发送投票按钮
 */
@property (assign ,nonatomic) BOOL isShowSendVoteBtn;

@property(nonatomic,assign) GetMessagesType messageType;


@property (nonatomic,assign) NSInteger row;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *thumbs;
@property (nonatomic, strong)YMTextData *ymData;
@property (nonatomic, strong)NSMutableArray *selections;
@property (nonatomic, strong)UISegmentedControl *segmentedControl;
@property (nonatomic, assign) int friendId;
@property (nonatomic, assign) UIViewController *parentContrl;

@property (nonatomic,copy ) NSString   *keyWord;
@property (nonatomic,copy) NSString    *labelId;
@property (nonatomic,strong) PublishChooseView *chooseView;

/**
 *  刷新table;
 */
-(void)loadTextData;
-(void)calculateHeight:(NSMutableArray *)dataArray;
-(void)reloadOneRowsWithData:(YMTextData *)ymData andRow:(NSInteger) row;
@property (nonatomic , strong)   CirleRequstTool      *tool;
/**
 *  刷新
 */
-(void)loadDataForHeader;
/**
 *  加载更多
 */
-(void)loadMoreData;

-(void)reloadTable;


-(instancetype)initWithShowMessageType:(GetMessagesType)type andClassId:(NSString *)classId;

- (void)replyAction:(YMButton *)sender;


@end
