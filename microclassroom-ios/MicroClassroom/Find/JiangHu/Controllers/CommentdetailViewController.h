//
//  CommentdetailViewController.h
//  CarService
//
//  Created by apple on 15/7/30.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//


#import "YHCirleBaseViewController.h"
#import "YMTextData.h"

@interface CommentdetailViewController : YHCirleBaseViewController

@property (nonatomic ,copy) NSString *msgId;
@property (nonatomic ,copy) void(^deletedMsg)(YMTextData *ymd , NSInteger indext);
@end
