//
//  YHCirleBaseViewController.m
//  yunbo2016
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHCirleBaseViewController.h"
#import "CirleRequstTool.h"
#import "ThumbModel.h"
#import "UserInfoModel.h"

#import "YHBaseWebViewController.h"
#import "HelpPublishViewController.h"
#import "YHNavgationController.h"

#import "DIYTipView.h"


@interface YHCirleBaseViewController ()
<UITableViewDataSource ,
UITableViewDelegate,
CirleRequstToolDelegate,publishChoose
>
{
    WFMessageBody *_lastModel;
    NSString *_lastSeeId ;
    NSString *_ctype; //区分 我的动态和江湖
    
}
@property (nonatomic,copy ) NSString   *classId;
@property (nonatomic,copy ) NSString   *typeStr;

@property (nonatomic, strong) DIYTipView *tipView;
@end

@implementation YHCirleBaseViewController


-(instancetype)initWithShowMessageType:(GetMessagesType)type andClassId:(NSString *)classId
{
    if (self = [super init]) {
        _messageType = type;
        
        if (type == GetMessagesTypeDuringClass) {
            
            _typeStr = @"0";
            //              _classId = @"0"; // 代表课间
            
        }else if (type == GetMessagesTypeStudyGroup)
        {
            _typeStr = @"1";
            //            _classId = @"-1";  // 代表班级
        }else{
            if (type == GetMessagesTypeGetMyCircle) {
                _ctype = @"1";
            }else{
                _ctype = @"2";
            }
        }
        _classId = classId;
        
    }
    return self;
}

#pragma mark === 初始化一些基础参数和控件===
-(void)initConfigData {
    _replyIndex        = -1;
    _tableDataSource   = @[].mutableCopy;
    _contentDataSource = @[].mutableCopy;
    [self initTableView];
    
}


-(void)reloadOneData
{
    [self reloadOneRowsWithData:self.ymData andRow:self.row];
}
-(void)initTableView {
    
    _mainTable = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, KScreenWidth, KScreenHeight - KTopHeight - 50) style:UITableViewStyleGrouped];
    _mainTable.delegate = self;
    _mainTable.dataSource = self;
    _mainTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 10)];
    _mainTable.backgroundColor = NCColor;
    _mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_mainTable];
    
    
    if ([_mainTable respondsToSelector:@selector(setSeparatorInset:)])
    {
        [_mainTable setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([_mainTable respondsToSelector:@selector(setLayoutMargins:)])
    {
        [_mainTable setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (@available(iOS 11.0, *)) {
        self.mainTable.estimatedRowHeight = 0;
        self.mainTable.estimatedSectionFooterHeight = 0;
        self.mainTable.estimatedSectionHeaderHeight = 0;
        self.mainTable.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.pageIndex    = 0;
    _tool = [[CirleRequstTool alloc]init];
    _tool.delegate = self;
    [self initConfigData];
    KAddobserverNotifiCation(@selector(reloadOneData), CircleListReloadOneData);
    KAddobserverNotifiCation(@selector(reloadAllTable:), CircleListReloadAllData);
    
}
-(void)reloadAllTable:(NSNotification *)nofi
{
    [self requestMyCollection];
}

-(void)setIsShowPublishBtn:(BOOL)isShowPublishBtn {
    if (isShowPublishBtn == YES) {
        [self creatPublishBtn];
    }
}

-(void)setIsShowSendVoteBtn:(BOOL)isShowSendVoteBtn
{
    if (isShowSendVoteBtn) {
        [self creatVoteBtn];
    }
}

-(void)creatVoteBtn
{
    //选项布局
    _chooseView = [[NSBundle mainBundle] loadNibNamed:@"PublishChooseView" owner:self options:nil].firstObject;
    _chooseView.delegate = self;
    [self.view addSubview:_chooseView];
    [_chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(_publishBtn.mas_top).offset(-5);
        make.size.mas_equalTo(CGSizeMake(95, 81));
    }];
    _chooseView.hidden = YES;
}
-(void)creatPublishBtn
{
    _publishBtn  =[UIButton buttonWithType:UIButtonTypeCustom];
    [_publishBtn setImage:[UIImage imageNamed:@"dynamic_Add"] forState:UIControlStateNormal];
    [self.view addSubview:_publishBtn];
    [_publishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.bottom.equalTo(-20);
        make.size.equalTo(CGSizeMake(71, 71));
        
    }];
    
}


#pragma mark === publishChoose
-(void)publishTypeChooseWithSender:(UIButton *)sender
{
    if (sender.tag == 101) {//发布动态
        
        
    }else{//发布投票
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark === 刷新一个row ===
-(void)reloadOneRowsWithData:(YMTextData *)ymData andRow:(NSInteger) row {
    
    [ymData.completionReplySource removeAllObjects];
    [ymData.attributedDataReply removeAllObjects];
    if (ymData.messageBody.showCommentTextView) {
        //		ymData.replyHeight = [ymData calculateReplyHeightWithWidth:KScreenWidth];
    }
    if (_tableDataSource.count < row || _tableDataSource.count == 0 || ymData == nil) {
        return;
    }
    [_tableDataSource replaceObjectAtIndex:row withObject:ymData];
    [_mainTable reloadData];
}


#pragma mark - 计算高度
- (void)calculateHeight:(NSMutableArray *)dataArray{
    
    NSMutableArray *tempArr   = [NSMutableArray arrayWithCapacity:0];
    
    for (YMTextData *ymData in dataArray) {
        
        ymData.shuoshuoHeight = [ymData calculateShuoshuoHeightWithWidth:KScreenWidth withUnFoldState:NO];//折叠
        
        ymData.unFoldShuoHeight= [ymData calculateShuoshuoHeightWithWidth:KScreenWidth withUnFoldState:YES];//展开
        
        ymData.replyHeight = 0;
        
        if (ymData.messageBody.showCommentTextView) {
            
            ymData.replyHeight = [ymData calculateReplyHeightWithWidth:KScreenWidth];
        }
        
        [tempArr addObject:ymData];
        
    }
    
    [_tableDataSource addObjectsFromArray:tempArr];
    
    [self reloadTable];
}




-(void)reloadTable
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.mainTable reloadData];
        
    });
    
}

#pragma mark -加载数据
- (void)loadTextData{
    
    NSMutableArray * ymDataArray =[[NSMutableArray alloc]init];
    
    for (int i = 0 ; i < _contentDataSource.count; i ++) {
        
        WFMessageBody *messBody = [_contentDataSource objectAtIndex:i];
        
        YMTextData *ymData = [[YMTextData alloc] init ];
        ymData.messageBody = messBody;
        
        [ymDataArray addObject:ymData];
        
    }
    [self calculateHeight:ymDataArray];
}


#pragma mark === UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.001;
    }
    return 5.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _tableDataSource.count;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //    return _tableDataSource.count;
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (_tableDataSource.count) {
        if (indexPath.row>_tableDataSource.count) {
            return 0;
        }
        
        YMTextData *ym = [_tableDataSource objectAtIndex:indexPath.row];
        BOOL unfold = ym.foldOrNot;
        if ([ym.messageBody.ctype isEqualToString:@"2"]) {
            self.cellH = kVoteBottom  + (ym.islessLimit?0:30) + ym.showVoteOptionHeight + (unfold?ym.shuoshuoHeight:ym.unFoldShuoHeight) + ym.labelHeight + kTopViewHeight + kCommentH+ (ym.showOptipnArray.count?kOpDistance:0)+ kOpToBottom;
        }else if ([ym.messageBody.ctype isEqualToString:@"3"]){
            self.cellH = kTopViewHeight + kCommentH + 50 + kLocationToBottom +  ym.labelHeight + (ym.labelHeight == 0?0:kDistance);
        }else{
            if (ym.showImageArray.count!=1) {
                self.cellH = kTopViewHeight + kLocationToBottom  + ym.showImageHeight  + (ym.showImageArray.count != 0?kDistance:0) + (ym.islessLimit?0:25) + (unfold?ym.shuoshuoHeight:ym.unFoldShuoHeight)+ CircleBottomViewH + (ym.labelHeight == 0?0:10) +(ym.showFileArray.count?kOpToBottom:0) + ym.showFileHeight + ym.labelHeight + kCommentH;
            }else
            {
                float image_heght = 100;
//                if ([ym.showImageArray[0] isKindOfClass:[NSString class]]) {
//                    NSString *urlStr =  ym.showImageArray[0];
//
//                    NSArray  *tempArr = [urlStr componentsSeparatedByString:@"_"];
//                    if (tempArr.count>2) {
//                        // 排列单张
//
//                        image_heght =  ceil([tempArr[1]  floatValue]/2);
//                    }
//                }
                self.cellH = kTopViewHeight + kLocationToBottom  + image_heght  + kDistance + (ym.islessLimit?0:25) + (unfold?ym.shuoshuoHeight:ym.unFoldShuoHeight)+(ym.labelHeight == 0?0:10)+CircleBottomViewH + ym.labelHeight+(ym.showFileArray.count?kOpToBottom:0) + ym.showFileHeight + kCommentH;
            }
        }
        
    }
    
    return self.cellH;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"ILTableViewCell";
    static NSString *CellIdentifierVote = @"CellIdentifierVote";
    static NSString *CellIdentifierShare = @"CellIdentifierShare";
    YMTableViewCell *cell = nil;
    YMTextData *ymdata = [_tableDataSource objectAtIndex:indexPath.row];
    if ([ymdata.messageBody.ctype isEqualToString:@"2"]) {//1=动态 2=投票 3=分享
        cell = (YMTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierVote];
        if (cell == nil) {
            cell = [[YMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierVote WithType:GetMessagesVoteType];
        }
    }else if ([ymdata.messageBody.ctype isEqualToString:@"3"]){
        if (cell == nil) {
            cell = [[YMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierShare WithType:GetMessageShareType];
        }
    }
    else{
        cell = (YMTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[YMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier WithType:GetMessagesListTypeNoraml];
        }
    }
    if (_tableDataSource.count) {
        if (indexPath.row>_tableDataSource.count) {
            return [[YMTableViewCell alloc]init];
        }
        cell.stamp = indexPath.row;
        cell.replyBtn.appendIndexPath = indexPath;
        cell.parentContr = self.parentViewController;
        [cell.replyBtn addTarget:self action:@selector(replyAction:) forControlEvents:UIControlEventTouchUpInside];
        
        @weakify(cell);
        @weakify(self);
        cell.supportBtn.appendIndexPath = indexPath.row;
        
        cell.collectionBlock = ^(BOOL isCollection) {
            @strongify(cell);
            @strongify(self);
            MyLog(@"%ld",indexPath.row);
            if (isCollection) {
                [self.tool cancelCollectionWith:cell.tempDate.messageBody type:@"2" stamp:indexPath vc:self];
            }else{
                [self.tool collectionWith:cell.tempDate.messageBody type:@"2" stamp:indexPath vc:self];
            }
        };
        
        cell.delegate = self;
        ymdata.messageBody.messageType = _messageType;
        [cell setYMViewWith:ymdata];
    }
    
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}
- (void)replyAction:(YMButton *)sender{
    
    //    if (self.replyView) {
    //        return;
    //    }
    //    _replyIndex =  -1;
    //    self.replyView     = [[YMReplyInputView alloc] initWithFrame:CGRectMake(0, self.view.viewHeight, KScreenWidth,49) andAboveView:self.view];
    //    self.replyView.delegate = self;
    //    self.replyView.replyTag = sender.appendIndexPath.section;
    //    [self.view addSubview:self.replyView];
    
}


#pragma mark ===点击评论 回复评论==
- (void)clickRichText:(NSInteger)index replyIndex:(NSInteger)replyIndex
{
    _replyIndex = replyIndex;
    
    YMTextData *ymData = (YMTextData *)[_tableDataSource objectAtIndex:index];
    
    WFReplyBody *b = nil;
    if (_replyIndex != -1) {
        
        b = [ymData.messageBody.comments objectAtIndex:replyIndex];
        NSString *userId = [UserInfoModel getInfoModel].userId;
        
        if ([b.userId isEqualToString:userId]) {
            [self destorySelf];
            UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"确定要删除该条评论吗?" cancelButtonItem:[RIButtonItem itemWithLabel:@"取消" action:^{
                
            }] destructiveButtonItem:[RIButtonItem itemWithLabel:@"删除" action:^{
                NSDictionary *paramDic = @{Service:CircleDeleteCommentMessage,
                                           @"userId":KgetUserValueByParaName(USERID),
                                           @"commentId":b.commentId
                                           };
                
                SHOWHUD;
                [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
                    HIDENHUD;
                    if (result) {  // 删除一条评论
                        if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                            WFMessageBody *m = ymData.messageBody;
                            NSMutableArray *newComment = m.comments.mutableCopy;
                            [newComment removeObjectAtIndex:replyIndex];
                            m.comments = newComment;
                            ymData.messageBody = m;
                            [self reloadOneRowsWithData:ymData andRow:index];
                        }
                    }
                }];
            }] otherButtonItems:nil, nil];
            [sheet showInView:self.view];
            return;
        }
    }
    
    
}

#pragma mark ===InputDelegate====
- (void)YMReplyInputWithReply:(NSString *)replyText appendTag:(NSInteger)inputTag
{
    if (![YHJHelp isReachable]) {
        
        return;
    }
    
    YMTextData *ymData = [_tableDataSource objectAtIndex:inputTag];
    WFReplyBody *replyModel = nil;
    NSString *touserId = @"";
    if (self.replyIndex == -1) { //直接评论
        
    }else
    {
        replyModel = [ymData.messageBody.comments objectAtIndex:self.replyIndex];
        touserId   = replyModel.userId;
    }
    
    NSMutableDictionary *sendDic = [NSMutableDictionary dictionaryWithCapacity:0];
    [sendDic setObject:KgetUserValueByParaName(USERID) forKey:@"userId"];
    [sendDic setObject:replyText forKey:@"content"];
    [sendDic setObject:ymData.messageBody.msgId forKey:@"msgId"];
    [sendDic setObject:touserId forKey:@"toUserId"];
    [sendDic setObject:CircleCommentMessage forKey:Service];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                //创建一个评论数据添加
                
                YMTextData *ymData = (YMTextData *)[_tableDataSource objectAtIndex:inputTag];
                WFMessageBody *m = ymData.messageBody;
                NSMutableArray *newComment = m.comments.mutableCopy;
                WFReplyBody *replymodel = [WFReplyBody new];
                replymodel.userId =KgetUserValueByParaName(USERID);
                replymodel.toUserId = touserId;
                replymodel.userPhoto = [UserInfoModel getInfoModel].photo;
                replymodel.toUserName = replyModel.userName;
                replymodel.postTime = [NSString CurrentTime1970];
                replymodel.content  = replyText;
                replymodel.publishId = replyModel.publishId;
                replymodel.userName = [UserInfoModel getInfoModel].nickname;
                [newComment addObject:replymodel];
                m.comments = newComment;
                ymData.messageBody = m;
                [self reloadOneRowsWithData:ymData andRow:inputTag];
                
            }else {
                [WFHudView showMsg:@"评论失败" inView:nil];
            }
            
            
        }
    }];
}


- (void)destorySelf{

    _replyIndex = -1;
}


#pragma mark -cellDelegate
- (void)changeFoldState:(YMTextData *)ymD onCellRow:(NSInteger)cellStamp{
    
    [_tableDataSource replaceObjectAtIndex:cellStamp withObject:ymD];
    [_mainTable reloadData];
    
}


#pragma mark --- 头像点击事件    跳转详情
-(void)userPhotoImageClick:(NSInteger)index adnUsrId:(NSInteger)userId{
}

#pragma mark - 长按评论整块区域的回调
- (void)longClickRichText:(NSInteger)index replyIndex:(NSInteger)replyIndex{
    
    
    YMTextData *ymData = (YMTextData *)[_tableDataSource objectAtIndex:index];
    //
    if (replyIndex==-1) {
        
        //长按删除说说
        
        return;
    }else {
        
        UIPasteboard *pboard = [UIPasteboard generalPasteboard];
        pboard.string = ymData.messageBody.content;
        
    }
}


-(BOOL)isZan
{
    YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:self.selectedIndexPath];
    for (ThumbModel *model in ymData.messageBody.thumbs) {
        if ([model.userId isEqualToString:KgetUserValueByParaName(USERID)]) {
            return YES;
        }
    }
    return NO;
}


#pragma mark   == 点赞完成====
-(void)addFinshWithBody:(WFMessageBody *)body
{
    YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:self.selectedIndexPath];
    ymData.messageBody = body;
    ymData.messageBody.messageType = _messageType;
    [ymData.attributedDataFavour removeAllObjects];
    [_tableDataSource replaceObjectAtIndex:_selectedIndexPath withObject:ymData];
    
    [self.mainTable reloadData];
    
}

#pragma mark === 取消赞的操作
-(void)cancelFinshWithBody:(WFMessageBody *)body
{
    YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:self.selectedIndexPath];
    ymData.messageBody = body;
    [ymData.attributedDataFavour removeAllObjects];
    [_tableDataSource replaceObjectAtIndex:_selectedIndexPath withObject:ymData];
    [self.mainTable reloadData];
}


#pragma mark === 下拉刷新 ===
- (void)loadDataForHeader{
    self.pageIndex =0;
    [self.tableDataSource removeAllObjects];
    [self.contentDataSource removeAllObjects];
    if (_messageType == GetMessagesTypeGetMyCircle || _messageType == GetMessagesTypeGetMyStudyGroup) {
        [self requestMinePublish];
    }else if (_messageType == GetMessagesTypeCollection){
        [self requestMyCollection];
    }
    else {
        [self requestFirst];
    }
}



#pragma mark === 加载更多===
-(void) loadMoreData{
    
    self.pageIndex = [_lastSeeId intValue];
    if (_messageType == GetMessagesTypeGetMyCircle || _messageType == GetMessagesTypeGetMyStudyGroup) {
        [self requestMinePublish];
    }else if (_messageType == GetMessagesTypeCollection){
        [self requestMyCollection];
    }else {
        [self requestFirst];
    }
    
}

#pragma  mark ===获取自己发布的消息====
//ctype	整型	必须	最小：1	1=动态 2=江湖
//pubType	枚举类型	必须	范围：1/2	 1=获取用户发布的
- (void)requestMinePublish {
    
    NSDictionary *paramDic = @{Service:CircleGetMyCircle,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"ctype":@(0),
                               @"lastId":@(self.pageIndex),
                               @"pubType":@"1",
                               @"friendId":@(self.friendId),
                               @"isClass":@(0),
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            //如果有数据  code = 0   1 没有数据
            if ([result[REQUEST_CODE] integerValue]==0) {
                NSArray *dataArr       = [result objectForSafeKey:@"list"];
                if(self.pageIndex==0){
                    [self.tableDataSource removeAllObjects];
                }
                [ self.contentDataSource removeAllObjects];
                
                for (NSDictionary *modleDic in dataArr ) {
                    WFMessageBody *model = [WFMessageBody   mj_objectWithKeyValues:modleDic];
                    
                    [self.contentDataSource addObject:model];
                }
                _lastModel = [self.contentDataSource lastObject];
                _lastSeeId = _lastModel.msgId;
                [self loadTextData];
                self.mainTable.mj_footer.hidden =[self hasRefreshFooterView];
            }
            
            else {
                self.mainTable.mj_footer.hidden =YES;
            }
        }
        
        [self.mainTable.mj_header endRefreshing];
        [self.mainTable.mj_footer endRefreshing];
        
        
        
        if (self.tableDataSource.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"还没有过发布哦~" andBtnTitle:nil andClickBlock:nil];
                
            }
            [self.mainTable addSubview:self.tipView];
            [self.tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.equalTo(CGSizeMake(120, 120));
                make.center.equalTo(self.view);
            }];
        }else
        {
            if (self.tipView.superview) {
                
                [self.tipView removeFromSuperview];
            }
        }
        
    }];
    
}

#pragma mark ------获取附近最新动态
-(void)requestFirst{
    
    
    NSDictionary *paramDic = @{Service:CircleGetMessages,
                               @"ctype":_typeStr,
                               @"lastId":[NSNumber numberWithInt:self.pageIndex],
                               @"userId":[KgetUserValueByParaName(USERID) length] == 0?@"":KgetUserValueByParaName(USERID),
                               @"keyWord":self.keyWord.length == 0?@"":self.keyWord
                               ,@"labelId":self.labelId.length == 0?@"":self.labelId,
                               @"classesId":_classId,
                               };
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            //如果有数据  code = 0   1 没有数据
            if ([result[REQUEST_CODE] integerValue]==0) {
                NSArray *dataArr       = [result objectForSafeKey:@"list"];
                if(self.pageIndex==0){
                    [self.tableDataSource removeAllObjects];
                }
                [ self.contentDataSource removeAllObjects];
                
                for (NSDictionary *modleDic in dataArr ) {
                    WFMessageBody *model = [WFMessageBody   mj_objectWithKeyValues:modleDic];
                    [self.contentDataSource addObject:model];
                }
                _lastModel = [self.contentDataSource lastObject];
                _lastSeeId = _lastModel.msgId;
                [self loadTextData];
                self.mainTable.mj_footer.hidden = [self hasRefreshFooterView];
            }else {
                if ([_lastSeeId intValue] == 0) {
                    [self.tableDataSource removeAllObjects];
                }
                self.mainTable.mj_footer.hidden = YES;
                [self.mainTable reloadData];
            }
        }
        [self.mainTable.mj_header endRefreshing];
        [self.mainTable.mj_footer endRefreshing];
        
        if (self.tableDataSource.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关动态" andBtnTitle:nil andClickBlock:nil];
                self.tipView.frame = CGRectMake(0, 0, 120, 120);
                self.tipView.center = self.view.center;
            }
            [self.mainTable addSubview:self.tipView];
        }else{
            if (self.tipView.superview) {
                [self.tipView removeFromSuperview];
            }
        }
    }];
}
#pragma mark === 我的收藏
- (void)requestMyCollection {
    NSDictionary *paramDic = @{Service:MyCollections,
                               @"ctype":@"2",
                               @"userId":KgetUserValueByParaName(USERID)};
    SHOWHUD;
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        @strongify(self);
        if (result) {
            //如果有数据  code = 0   1 没有数据
            if ([result[REQUEST_CODE] integerValue]==0) {
                NSArray *dataArr       = [result objectForSafeKey:@"list"];
                [self.tableDataSource removeAllObjects];
                [ self.contentDataSource removeAllObjects];
                
                for (NSDictionary *modleDic in dataArr ) {
                    WFMessageBody *model = [WFMessageBody   mj_objectWithKeyValues:modleDic];
                    model.isCollection = YES;
                    [self.contentDataSource addObject:model];
                }
                _lastModel = [self.contentDataSource lastObject];
                [self loadTextData];
            }
            
            else {
                //                if ([_lastSeeId intValue] == 0) {
                //                }
                [self.tableDataSource removeAllObjects];
                self.mainTable.mj_footer.hidden = YES;
                [self.mainTable reloadData];
            }
        }
        [self.mainTable.mj_header endRefreshing];
        [self.mainTable.mj_footer endRefreshing];
        
        if (self.tableDataSource.count==0) {
            if (!self.tipView) {
                self.tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关动态" andBtnTitle:nil andClickBlock:nil];
                self.tipView.frame = CGRectMake(0, 0, 120, 120);
                self.tipView.center = self.view.center;
            }
            [self.mainTable addSubview:self.tipView];
        }else
        {
            if (self.tipView.superview) {
                
                [self.tipView removeFromSuperview];
            }
        }
        
    }];
}
#pragma mark === 是显示更多   上拉加载==
- (BOOL)hasRefreshFooterView {
    
    
    if ( [_lastSeeId intValue] == 1) {
        return YES;  //不上啦
    }
    return NO; //上啦
    
}

#pragma  mark - 删除说说
-(void)delectMsg:(YMTextData *)ymd andIndext:(NSInteger)indext{
    [self.tableDataSource removeObjectAtIndex:indext];
    [self reloadTable];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]){
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)urlStrClickWithStr:(NSString *)url{
    YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:url andNavTitle:@""];
    
    if (self.parentContrl) {
        [self.parentContrl.navigationController pushViewController:web animated:YES];
    }else{
        [self.navigationController pushViewController:web animated:YES];
    }
};


#pragma mark === 收藏
-(void)addCollectionbWithBody:(WFMessageBody *)body stamp:(NSIndexPath *)stamp
{
    YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:stamp.row];
    ymData.messageBody = body;
    [_tableDataSource replaceObjectAtIndex:stamp.row withObject:ymData];
    [self.mainTable reloadData];
    KPostNotifiCation(CircleListReloadOneData, nil);
}
-(void)cancelCollectionbWithBody:(WFMessageBody *)body stamp:(NSIndexPath *)stamp
{
    if (_messageType == GetMessagesTypeCollection){
        YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:stamp.row];
        ymData.messageBody = body;
        [_tableDataSource removeObject:ymData];
        [self.mainTable reloadData];
    }else{
        YMTextData *ymData = (YMTextData *)[self.tableDataSource objectAtIndex:stamp.row];
        ymData.messageBody = body;
        [_tableDataSource replaceObjectAtIndex:stamp.row withObject:ymData];
        [self.mainTable reloadData];
        KPostNotifiCation(CircleListReloadOneData, nil);
    }
}

- (void)dealloc
{
    KRemoverNotifi(CircleListReloadOneData);
    KRemoverNotifi(CircleListReloadAllData);
}
@end

