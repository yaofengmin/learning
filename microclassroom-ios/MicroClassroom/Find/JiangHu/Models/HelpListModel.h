//
//  HelpListModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"
#import "hxAccountModel.h"
@interface HelpListModel : YHBaseModel

@property (nonatomic ,strong) NSString *acceptTime;
@property (nonatomic ,strong) NSString *finishTime;
@property (nonatomic ,strong) NSString *helpTime;
@property (nonatomic ,strong) NSString *messageHelpId;
@property (nonatomic ,strong) NSString *messageId;
//helps数组中的state 1=等待接受帮助 2=已接授  3=不接受 4=帮助完成  5=帮助失败
@property (nonatomic ,strong) NSString *state;
@property (nonatomic ,strong) NSString *userId;
@property (nonatomic ,strong) NSString *userName;
@property (nonatomic ,strong) NSString *userPhoto;
@property (nonatomic ,strong) hxAccountModel *hxAccount;
@end
