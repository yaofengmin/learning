//
//  WFMessageBody.h
//  WFCoretext
//
//  Created by 阿虎 on 15/4/28.
//  Copyright (c) 2015年 tigerwf. All rights reserved.
//

#import "YHBaseModel.h"

@interface WFReplyBody : YHBaseModel

/**
 *  发布者的Id
 */
@property (nonatomic ,copy) NSString *publishId;
/**
 *  评论者
 */
@property (nonatomic,copy) NSString *userName;

@property (nonatomic ,copy) NSString *userPhoto;/**<  评论者头像  */

/**
 *  评论者ID
 */

@property (nonatomic,copy) NSString *userId;

/**
 *  回复该评论者的人
 */
@property (nonatomic,copy) NSString *toUserName;

/**
 *  回复该评论者的人ID
 */
@property (nonatomic,copy) NSString *toUserId;

/**
 *  回复内容
 */
@property (nonatomic,copy) NSString *content;

@property (nonatomic ,copy) NSString *postTime;/**<  回复时间  */

@property (nonatomic ,copy) NSString *commentId; /**<  评论Id  */

@property (nonatomic ,copy) NSString *FinishCommentId; /**<  完成ID  */

/**
 *  用消息ID
 */
@property (nonatomic,copy) NSString *msgId;

//评论高度
-(CGFloat)commentCellHeight;
@end
