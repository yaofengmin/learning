//
//  AssignmentModel.h
//  Education
//
//  Created by BPO on 2017/7/4.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseModel.h"

@interface AssignmentModel : YHBaseModel
@property (nonatomic,assign) CGFloat photoHeight;//图片的高度
@property (nonatomic,assign) BOOL isEdit;//是否需要编辑

@property (nonatomic,copy) NSString *imageStr;


@property (nonatomic,assign) CGFloat fileHeight;//文件高度

-(CGFloat)returnCellHeight;

-(CGFloat)returnFileHeight;


@end
