//
//  ThumbModel.h
//  yunbo2016
//
//  Created by Hanks on 15/12/31.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHBaseModel.h"

@interface ThumbModel : YHBaseModel<NSCoding>

@property (nonatomic ,copy) NSString *userId;
@property (nonatomic ,copy) NSString *userName;
@property (nonatomic ,copy) NSString *userPhoto;

@end
