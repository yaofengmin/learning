//
//  AssignmentModel.m
//  Education
//
//  Created by BPO on 2017/7/4.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "AssignmentModel.h"

#define Spacing 9 // 每个item的间距
#define LineNum 4 // 每行个数

@implementation AssignmentModel


-(CGFloat)returnCellHeight
{
    
    CGFloat itemW = (KScreenWidth - Spacing * (LineNum - 1)) / LineNum;
    
    return (self.photoHeight + 40) < itemW?itemW:(self.photoHeight + 40);
    
}

-(CGFloat)returnFileHeight
{
	return (self.fileHeight < 50?50:self.fileHeight)+ 70;
}
@end
