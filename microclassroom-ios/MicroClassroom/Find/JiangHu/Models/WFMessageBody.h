//
//  WFMessageBody.h
//  WFCoretext
//
//  Created by 阿虎 on 15/4/29.
//  Copyright (c) 2015年 tigerwf. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "hxAccountModel.h"

@interface WFMessageBody : NSObject
@property (nonatomic,copy) NSString *msgId;
/**
 *  用户头像url 此处直接用图片名代替
 */
@property (nonatomic,copy) NSString *userPhoto;//

/**
 *  用户名
 */
@property (nonatomic,copy) NSString *userName;


/**
 *  发布时间
 */
@property (nonatomic,copy) NSString *postTime_str;
@property (nonatomic,copy) NSString *postTime;

/**
 *  标签
 */
@property (nonatomic,copy) NSArray *labelName;
@property (nonatomic,copy) NSString *labelId;

@property (nonatomic,copy) NSString *commentCount;

@property (nonatomic,copy) NSString *viewCount;//查看次数


@property (nonatomic,assign) BOOL    showCommentTextView;//是否显示评论
/**
 *  用户userId
 */
@property (nonatomic ,copy) NSString *userId;
/**
 *  用户发送的图片数组
 */
@property (nonatomic,strong) NSArray *pics;//


/**
 *  用户说说内容
 */
@property (nonatomic,copy) NSString *content;//

/**单位职务*/
@property (nonatomic,copy) NSString   *title;
/**动态类型*/
@property (nonatomic,copy) NSString   *ctype;//1=动态 2=投票
/**
 分享类型的url或者id,判断是否以http开头的.跳转对应的web或者共享详情界面
 */
@property (nonatomic,copy) NSString   *link;
/**
 分享显示的图片
 */
@property (nonatomic,copy) NSString   *coverPhoto;

/**
 *  admin是否赞过
 */
@property (nonatomic,assign) BOOL isCollection;
@property (nonatomic,strong) hxAccountModel *hxAccount;
@property (nonatomic,strong) NSArray *comments;


/*********************************投票**********************************/
/**
 *投票选项数组
 */
@property (nonatomic,strong) NSArray *qusList;

@property (nonatomic,copy) NSString *qusId;
@property (nonatomic,copy) NSString *endTime;
/**
 *  赞的数组
 */
@property (nonatomic ,strong) NSArray *thumbs;
@property(nonatomic,assign) GetMessagesType messageType;
@property(nonatomic ,strong) UIImage     *shareImage;
/**
 *  说说的状态   1=求助中 2=解决中 3=成功 4=失败
 */
@property (nonatomic ,copy) NSString *helpStatus;
@property (nonatomic, assign) NSInteger isCertified;

/**
 *  消息是否置顶
 */
@property (nonatomic,copy) NSString   *isTop;

/**
 文件
 */
@property (nonatomic,strong) NSArray   *files;

@end

