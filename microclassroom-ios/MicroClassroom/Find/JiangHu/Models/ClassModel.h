//
//  FarmModel.h
//  pinnc
//
//  Created by Hanks on 16/2/20.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface ClassModel : YHBaseModel

@property(nonatomic ,copy) NSString *stuClassId;
@property(nonatomic ,copy) NSString *classId;
@property(nonatomic ,copy) NSString *className;
@property(nonatomic ,copy) NSString *startTime;
@property(nonatomic ,copy) NSString *endTime;
@property(nonatomic ,copy) NSString *remark;


@end
