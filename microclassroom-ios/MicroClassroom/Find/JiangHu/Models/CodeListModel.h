//
//  CodeListModel.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"

@interface CodeListModel : YHBaseModel

@property(nonatomic,copy) NSString *codeId;
@property(nonatomic,copy) NSString *codeName;
/**
 备注
 */
@property(nonatomic,copy) NSString *meno;
@property(nonatomic,assign) BOOL isSelect;

@end
