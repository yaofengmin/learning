//
//  WFMessageBody.m
//  WFCoretext
//
//  Created by 阿虎 on 15/4/28.
//  Copyright (c) 2015年 tigerwf. All rights reserved.
//

#import "WFReplyBody.h"

@implementation WFReplyBody
-(CGFloat)commentCellHeight
{
    CGFloat cellH = 10 + 30 + 5 + 16 + 1;
    cellH = cellH + [self cacluteLabelHeight:self.content];//内容
    return cellH;
}

-(CGFloat)cacluteLabelHeight:(NSString *)text
{
    if (text.length == 0) {
        return 0;
    }
    NSAttributedString *textStr = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    CGRect msgContentSize = [textStr boundingRectWithSize:CGSizeMake(KScreenWidth - 92, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    
    
    return (msgContentSize.size.height + 20) < 35?35:(msgContentSize.size.height + 20);
}
@end
