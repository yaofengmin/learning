//
//  HelpIndexModel.h
//  CarService
//
//  Created by yunbo on 15/7/17.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "YHBaseModel.h"
@interface HelpIndexModel: YHBaseModel
@property (nonatomic ,copy) NSString *roleName;
@property (nonatomic ,copy) NSString *roleId;
@end
