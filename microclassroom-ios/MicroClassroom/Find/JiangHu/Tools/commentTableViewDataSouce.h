//
//  commentTableViewDataSouce.h
//  CarService
//
//  Created by apple on 15/8/3.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WFReplyBody;
@interface commentTableViewDataSouce : NSObject<UITableViewDataSource>

-(id)initWithData:(NSMutableArray *)dataSource;

@end
