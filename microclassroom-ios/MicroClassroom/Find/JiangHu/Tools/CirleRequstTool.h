//
//  CirleRequstTool.h
//  yunbo2016
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHBaseSington.h"
#import "MicroClassBannerSegmentModel.h"
@class WFMessageBody;


@protocol CirleRequstToolDelegate <NSObject>

@optional
-(void)addFinshWithBody:(WFMessageBody *)body;
-(void)cancelFinshWithBody:(WFMessageBody *)body;

-(void)addCollectionbWithBody:(WFMessageBody *)body stamp:(NSIndexPath *)stamp;
-(void)cancelCollectionbWithBody:(WFMessageBody *)body stamp:(NSIndexPath *)stamp;

//图书
//-(void)addMyBooks:(MicroClassBooksDetailModel *)body stamp:(NSIndexPath *)stamp;
//-(void)cancelMyBooks:(MicroClassBooksDetailModel *)body stamp:(NSIndexPath *)stamp;


@end
@interface CirleRequstTool : YHBaseSington

@property (nonatomic ,weak) id <CirleRequstToolDelegate> delegate;
/**
 *  发布消息
 *
 *  @param model        消息模型
 *  @param publishBlock 成功回调
 */
-(void)sendPublishInfoWithModel:(WFMessageBody *)model andFinishBlock:(FinishBlock) publishBlock;

-(void)addThumbWith:(WFMessageBody *)body ;
-(void)cancelThumbWith:(WFMessageBody *)body ;

//图书
-(void)addMyBooks:(MicroClassBooksDetailModel *)body vc:(UIViewController *)vc tamp:(NSIndexPath *)stamp ;
-(void)cancelMyBooks:(MicroClassBooksDetailModel *)body vc:(UIViewController *)vc tamp:(NSIndexPath *)stamp ;


-(void)collectionWith:(WFMessageBody *)body type:(NSString *)type stamp:(NSIndexPath *)stamp vc:(UIViewController *)vc;
-(void)cancelCollectionWith:(WFMessageBody *)body type:(NSString *)type stamp:(NSIndexPath *)stamp vc:(UIViewController *)vc;

@end
