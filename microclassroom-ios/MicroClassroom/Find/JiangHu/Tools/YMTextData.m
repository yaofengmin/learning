//
//  YMTextData.m
//  WFCoretext
//
//  Created by 阿虎 on 14/10/29.
//  Copyright (c) 2014年 tigerwf. All rights reserved.
//

#import "YMTextData.h"
#import "ILRegularExpressionManager.h"
#import "NSString+NSString_ILExtension.h"
#import "WFTextView.h"
#import "WFReplyBody.h"
#import "HXTagsView.h"


#define EmotionItemPattern    @"\\[em:(\\d+):\\]"
#define PlaceHolder @" "

@implementation YMTextData{
    
    TypeView typeview;
    int tempInt;
}



- (id)init
{
    self = [super init];
    if (self) {
        
        _foldOrNot = YES;
        _islessLimit = NO;
        self.completionReplySource  = @[].mutableCopy;
        
    }
    return self;
}

- (void)setMessageBody:(WFMessageBody *)messageBody{
    
    _messageBody = messageBody;
    _foldOrNot = YES;
    _showImageArray = messageBody.pics;
    _showOptipnArray = messageBody.qusList;
    _showFileArray = messageBody.files;
//    _showFileArray = @[@"http://yanshan.51urmaker.com//downloads//2017//12//01//afdasdf_周易.txt",@"http://yanshan.51urmaker.com//downloads//2017//12//01//afdasdf_周易.pdf",@"http://yanshan.51urmaker.com//downloads//2017//12//01//afdasdf_周易.xlsx"];
    _showShuoShuo = messageBody.content;
    _hasFavour = messageBody.isCollection;
    _labelArr = messageBody.labelName;
}
//标签高度
- (void)calculateLabelHeight{
    
    if (self.labelArr.count == 0) {
        self.labelHeight = 0;
    }else{
        HXTagsView *tagV = [[HXTagsView alloc]initWithFrame:CGRectMake(64, kTopViewHeight, KScreenWidth - 64 - 15, 0)];
        NSMutableArray *labelArr = [NSMutableArray array];
        for (NSDictionary *dic in self.labelArr) {
            [labelArr addObject:dic[@"codeName"]];
        }
        self.labelHeight =  [tagV getDisposeTagsViewHeight:labelArr];
    }
    
}


//图片高度
- (void)calculateShowImageHeight{
    NSInteger  judgePhone ;
    if (IPHONE_6P) {
        judgePhone = 4;
    }else {
        judgePhone = 3;
    }
    
    if (self.showImageArray.count == 0) {
        self.showImageHeight = 0;
    }else{
        self.showImageHeight = (ShowImage_H + 10) * ((self.showImageArray.count - 1)/judgePhone + 1);
    }
    
}
//投票选项高度
- (void)calculateVoteOptionHeight{
    
    if (self.showOptipnArray.count == 0) {
        self.showVoteOptionHeight = 0;
    }else{
        self.showVoteOptionHeight = (ShowOption_H + 10) * (self.showOptipnArray.count);
    }
    
}

//文件选项高度
- (void)calculateFileHeight{
    
    if (self.showFileArray.count == 0) {
        self.showFileHeight = 0;
    }else{
        self.showFileHeight = (ShowOption_H + 10) * (self.showFileArray.count);
    }
    
}
- (void)matchString:(NSString *)dataSourceString fromView:(TypeView) isReplyV{
    
    if (isReplyV == TypeReply) {
        
        NSMutableArray *totalArr = [NSMutableArray arrayWithCapacity:0];
        
        //**********号码******
        
        NSMutableArray *mobileLink = [ILRegularExpressionManager matchMobileLink:dataSourceString];
        for (int i = 0; i < mobileLink.count; i ++) {
            
            [totalArr addObject:[mobileLink objectAtIndex:i]];
        }
        
        //*************************
        
        
        //***********匹配网址*********
        
        NSMutableArray *webLink = [ILRegularExpressionManager matchWebLink:dataSourceString];
        for (int i = 0; i < webLink.count; i ++) {
            
            [totalArr addObject:[webLink objectAtIndex:i]];
        }
        
        //******自行添加**********
        
        if (_defineAttrData.count != 0) {
            NSArray *tArr = [_defineAttrData objectAtIndex:tempInt];
            for (int i = 0; i < [tArr count]; i ++) {
                NSString *string = [dataSourceString substringWithRange:NSRangeFromString([tArr objectAtIndex:i])];
                [totalArr addObject:[NSDictionary dictionaryWithObject:string forKey:NSStringFromRange(NSRangeFromString([tArr objectAtIndex:i]))]];
            }
            
        }
        
        
        //***********************
        
        
    }
    
    if(isReplyV == TypeShuoshuo){
        
        [self.attributedDataShuoshuo removeAllObjects];
        //**********号码******
        
        NSMutableArray *mobileLink = [ILRegularExpressionManager matchMobileLink:dataSourceString];
        for (int i = 0; i < mobileLink.count; i ++) {
            
            [self.attributedDataShuoshuo addObject:[mobileLink objectAtIndex:i]];
        }
        
        //*************************
        
        
        //***********匹配网址*********
        
        NSMutableArray *webLink = [ILRegularExpressionManager matchWebLink:dataSourceString];
        for (int i = 0; i < webLink.count; i ++) {
            
            [self.attributedDataShuoshuo addObject:[webLink objectAtIndex:i]];
        }
        
    }
    
    //    if (isReplyV == TypeFavour) {
    //
    //        [self.attributedDataFavour removeAllObjects];
    //        int originX = 0;
    //        for (int i = 0; i < _favourArray.count; i ++) {
    //            NSString *text = [_favourArray objectAtIndex:i];
    //            NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:text,NSStringFromRange(NSMakeRange(originX, text.length)), nil];
    //            [self.attributedDataFavour addObject:dic];
    //            originX += (1 + text.length);
    //        }
    //    }
}
//计算replyview高度
- (float) calculateReplyHeightWithWidth:(float)sizeWidth{
    
    typeview = TypeReply;
    float height = .0f;
    
    for (int i = 0; i < self.replyDataSource.count; i ++ ) {
        
        tempInt = i;
        WFReplyBody *body = (WFReplyBody *)[self.replyDataSource objectAtIndex:i];
        NSString *matchString;
        
        if ([body.userName isEqualToString:@""]) {
            matchString = [NSString stringWithFormat:@"%@:%@",body.userName,body.content];
            
        }else{
            matchString = [NSString stringWithFormat:@"%@回复%@:%@",body.userName,body.toUserName,body.content];
        }
        
        //存新的
        [self.completionReplySource addObject:matchString];
        
        [self matchString:matchString fromView:typeview];
        
        WFTextView *_ilcoreText = [[WFTextView alloc] initWithFrame:CGRectMake(70,0, sizeWidth - 80, 0)];
        
        _ilcoreText.isFold = NO;
        _ilcoreText.isDraw = NO;
        
        [_ilcoreText setOldString:matchString andNewString:matchString];
        
        height =  height + [_ilcoreText getTextHeight] + 10;
        
    }
    
    return height;
    
}



//说说高度
- (float) calculateShuoshuoHeightWithWidth:(float)sizeWidth withUnFoldState:(BOOL)isUnfold{
    
    typeview = TypeShuoshuo;
    
    NSString *matchString =  _showShuoShuo;
    
    NSArray *itemIndexs = [ILRegularExpressionManager itemIndexesWithPattern:EmotionItemPattern inString:matchString];
    
    //用PlaceHolder 替换掉[em:02:]这些
    NSString *newString = [matchString replaceCharactersAtIndexes:itemIndexs
                                                       withString:PlaceHolder];
    //存新的
    self.completionShuoshuo = newString;
    
    [self matchString:newString fromView:typeview];
    
    WFTextView *_wfcoreText = [[WFTextView alloc] initWithFrame:CGRectMake(64,101, sizeWidth - 89, 0)];
    
    _wfcoreText.isDraw = NO;
    
    [_wfcoreText setOldString:_showShuoShuo andNewString:newString];
    
    if ([_wfcoreText getTextLines] <= limitline) {
        self.islessLimit = YES;
    }else{
        self.islessLimit = NO;
    }
    
    if (!isUnfold) {
        _wfcoreText.isFold = YES;
        
    }else{
        _wfcoreText.isFold = NO;
    }
    [self calculateLabelHeight];
    [self calculateShowImageHeight];
    [self calculateVoteOptionHeight];
    [self calculateFileHeight];
    return [_wfcoreText getTextHeight];
    
}


@end

