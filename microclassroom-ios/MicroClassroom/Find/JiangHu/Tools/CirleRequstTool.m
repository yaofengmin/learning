//
//  CirleRequstTool.m
//  yunbo2016
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "CirleRequstTool.h"
#import "WFMessageBody.h"
#import "ThumbModel.h"
#import "UserInfoModel.h"

@interface CirleRequstTool ()

@property (copy) FinishBlock publishBlock;


@end

@implementation CirleRequstTool

//-(void)sendPublishInfoWithModel:(WFMessageBody *)model andFinishBlock:(FinishBlock) publishBlock
//{
//    _publishBlock = [publishBlock copy];
//  
//    NSString *userId = KgetUserValueByParaName(USERID);
//    NSDictionary *publicDic  =@{Service:CirclePublishMessage,
//                  @"userId":userId,
//                  @"content":model.content,
//                  @"address":model.address,
//                  @"longitude":model.longitude,
//                  @"latitude":model.latitude,
//                  @"checkCode":model.checkCode};
//    
//    NSMutableDictionary *sendDic = [NSMutableDictionary dictionaryWithDictionary:publicDic];
//    NSDictionary *parasDic = @{@"pics":model.pics};
//    
//    [HTTPManager publishCarCirclWithUrl:MainAPI andPostParameters:sendDic andImageDic:parasDic andBlocks:^(NSDictionary *result) {
//       
//        _publishBlock(result);
//    
//    }];
//}


-(void)addThumbWith:(WFMessageBody *)body{
    
  
    NSDictionary *sendDic = @{Service:CircleThumbMessage,
                              @"userId":KgetUserValueByParaName(USERID),
                              @"msgId":body.msgId};
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        
        if (result) {
            if ([_delegate respondsToSelector:@selector(addFinshWithBody:)]) {
                [_delegate addFinshWithBody:[self addThumb:body]];
            }
        }
    }];
    
}


-(void)cancelThumbWith:(WFMessageBody *)body{
    
   
    NSDictionary *sendDic = @{Service:CircleCancelThumb,
                              @"userId":KgetUserValueByParaName(USERID),
                              @"msgId":body.msgId};
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([_delegate respondsToSelector:@selector(cancelFinshWithBody:)]) {
                [_delegate cancelFinshWithBody:[self cancelThumb:body]];
            }
        }
      
    }];
    
}

#pragma  mark ==== 本地数据操作 添加或者删除赞 ====
-(WFMessageBody *)addThumb:(WFMessageBody *)msgBody
{
    NSMutableArray *arr = [NSMutableArray arrayWithArray:msgBody.thumbs];
    NSDictionary *thumbDic = @{@"userId":KgetUserValueByParaName(USERID),
                               @"userName":[UserInfoModel getInfoModel].nickname,
                               @"userPhoto":[UserInfoModel getInfoModel].photo};
    ThumbModel *modle = [ThumbModel mj_objectWithKeyValues:thumbDic];
    if (arr.count) {
        [arr insertObject:modle atIndex:0];
    }else
    {
        [arr addObject:modle];
    }
    
    msgBody.thumbs = arr;
    
    return msgBody;
}

-(WFMessageBody *)cancelThumb:(WFMessageBody *)msgBody
{
    NSMutableArray *arr = [NSMutableArray arrayWithArray:msgBody.thumbs];
   
    [arr enumerateObjectsUsingBlock:^(ThumbModel  *model, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([model.userId isEqualToString:KgetUserValueByParaName(USERID)]) {
            [arr removeObject:model];
        }
        
    }];
    
    msgBody.thumbs = arr;
    return msgBody;
}
#pragma mark === 收藏

-(void)collectionWith:(WFMessageBody *)body type:(NSString *)type stamp:(NSIndexPath *)stamp vc:(UIViewController *)vc{//1＝视频 2=动态
    if ([YHJHelp isLoginAndIsNetValiadWitchController:vc]) {
        
        NSDictionary *sendDic = @{Service:CircleAddCollection,
                                  @"userId":KgetUserValueByParaName(USERID),
                                  @"pId":body.msgId,
                                  @"ctype":type};
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
            
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    body.isCollection = YES;
                    if ([_delegate respondsToSelector:@selector(addCollectionbWithBody:stamp:)]) {
                        [_delegate addCollectionbWithBody:body stamp:stamp];
                    }
                }
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }];
    }
}


-(void)cancelCollectionWith:(WFMessageBody *)body type:(NSString *)type stamp:(NSIndexPath *)stamp vc:(UIViewController *)vc{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:vc]) {
        NSDictionary *sendDic = @{Service:CircleCancelCollection,
                                  @"userId":KgetUserValueByParaName(USERID),
                                  @"pId":body.msgId,
                                  @"ctype":type};//1＝视频 2=动态 3=共享
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    body.isCollection = NO;
                    if ([_delegate respondsToSelector:@selector(cancelCollectionbWithBody:stamp:)]) {
                        [_delegate cancelCollectionbWithBody:body stamp:stamp];
                    }
                }
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }];
    }
    
}

#pragma mark === 图书
-(void)addMyBooks:(MicroClassBooksDetailModel *)body vc:(UIViewController *)vc tamp:(NSIndexPath *)stamp
{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:vc]) {
        
        NSDictionary *sendDic = @{Service:CircleAddCollection,
                                  @"userId":KgetUserValueByParaName(USERID),
                                  @"pId":body.videoId,
                                  @"ctype":@"4"};
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
            
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    //                    body.isReed = YES;
                    //                    if ([_delegate respondsToSelector:@selector(addMyBooks:stamp:)]) {
                    //                        [_delegate addMyBooks:body stamp:stamp];
                    //                    }
                }else{
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];
    }
}
//取消在读
-(void)cancelMyBooks:(MicroClassBooksDetailModel *)body vc:(UIViewController *)vc tamp:(NSIndexPath *)stamp
{
    if ([YHJHelp isLoginAndIsNetValiadWitchController:vc]) {
        NSDictionary *sendDic = @{Service:CircleCancelCollection,
                                  @"userId":KgetUserValueByParaName(USERID),
                                  @"pId":body.videoId,
                                  @"ctype":@"4"};
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:sendDic andBlocks:^(NSDictionary *result) {
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    //                    body.isReed = NO;
                    //                    if ([_delegate respondsToSelector:@selector(cancelMyBooks:stamp:)]) {
                    //                        [self.delegate cancelMyBooks:body stamp:stamp];
                    //                    }
                }else{
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
            
        }];
    }
}



@end
