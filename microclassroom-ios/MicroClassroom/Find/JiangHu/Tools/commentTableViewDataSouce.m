//
//  commentTableViewDataSouce.m
//  CarService
//
//  Created by apple on 15/8/3.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "commentTableViewDataSouce.h"
#import "MicroClassCommentTableViewCell.h"


@interface commentTableViewDataSouce ()

@property (nonatomic ,strong)NSMutableArray *dataSource;

@end


@implementation commentTableViewDataSouce

-(id)initWithData:(NSMutableArray *)modelArr{
    if (self = [super init]) {
        self.dataSource = modelArr;
    }
    
    return  self;
}

#pragma mark-------tableViewDataSouce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    
    return self.dataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    MicroClassCommentTableViewCell *cell  = [[NSBundle mainBundle] loadNibNamed:@"MicroClassCommentTableViewCell" owner:self options:nil].firstObject;

    if (self.dataSource.count) {
        WFReplyBody *model = [self.dataSource objectAtIndex:indexPath.row];
       [cell configWithWeCommentModel:model];
        
    }
    
    return cell;

}



@end
