//
//  commentTableViewDelegate.m
//  CarService
//
//  Created by apple on 15/8/3.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "commentTableViewDelegate.h"
#import "MicroClassCommentTableViewCell.h"
//#import <UITableView+FDTemplateLayoutCell.h>

@interface commentTableViewDelegate()

@property (nonatomic ,strong)NSMutableArray *dataSource;

@end

@implementation commentTableViewDelegate


-(id)initWithData:(NSMutableArray *)modelArr{
    if (self = [super init]) {
        self.dataSource = modelArr;
        
    }
    
    return  self;
}


#pragma mark --UITableViewDelegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    if (self.dataSource.count) {
        WFReplyBody *model = self.dataSource[indexPath.row];
        return [model commentCellHeight];
    }
    return 0;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WFReplyBody *model = [self.dataSource objectAtIndex:indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:FriendReply object:model];
    
    
}





@end
