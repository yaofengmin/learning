//
//  CommentTableViewCell.m
//  CarService
//
//  Created by apple on 15/8/4.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "NSString+MD5.h"
#import "DBDataModel.h"

@implementation CommentTableViewCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        _headImageView  = [[UIImageView alloc]init];
        [_headImageView setBackgroundColor:ImageBackColor];
        CALayer *layer = [_headImageView layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:17.5];
        _headImageView.userInteractionEnabled  = YES;
        [self.contentView addSubview:_headImageView];
        
        [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top).offset(6);
            make.left.equalTo(self.contentView.mas_left).offset(13);
            make.size.equalTo(CGSizeMake(35, 35));
            
        }];
        
        
        UITapGestureRecognizer *imageTap       = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(HeaderImageTap:)];
        [_headImageView addGestureRecognizer:imageTap];
        
        
        _commentTimeLabel                = [[UILabel alloc]init];
        _commentTimeLabel.textColor      = [UIColor lightGrayColor];
        _commentTimeLabel.font           = [UIFont systemFontOfSize:10];
        [_commentTimeLabel sizeToFit];
        [self.contentView addSubview:_commentTimeLabel];
        
        
        _nameLabel                       = [[UILabel alloc]init] ;
        _nameLabel.textColor             = CColor;
        _nameLabel.textAlignment         = NSTextAlignmentLeft;
        _nameLabel.font                  = Font(14);
        [self.contentView addSubview:_nameLabel];

    }
    return self;
}

-(void)setModel:(WFReplyBody *)model   {
    
    _model = model;
    NSURL *photoUrl                    = [NSURL URLWithString:_model.userPhoto];
    [self.headImageView sd_setImageWithURL:photoUrl];
    
    NSString *  replaceName = [[DBDataModel sharedDBDataModel] getBangbangDisPlayName:model.userId];
    if (replaceName.length) {
        self.nameLabel.text                = replaceName;
    }else {
        self.nameLabel.text                = _model.userName;
    }

    self.commentTimeLabel.text                = [NSString dataFormatWithTimeSecondStr:_model.postTime  andFormatStr:@"YYY.MM.dd"];
    _CommentId  = _model.commentId;
    
    
    
    if (_commentLabel) {
        [_commentLabel removeFromSuperview];
    }
    
    _commentLabel                      = [[UILabel alloc]init] ;
    _commentLabel.numberOfLines        = 0;
    _commentLabel.textColor            = EColor;
    _commentLabel.font                 = Font(11);
    _commentLabel.lineBreakMode        = NSLineBreakByCharWrapping;
    [self.contentView addSubview:_commentLabel];
    
    NSString *commetText;
    
    ;
      NSString *toUserName    = [[DBDataModel sharedDBDataModel] getBangbangDisPlayName:[NSString stringWithFormat:@"%@",model.toUserId]];
    if (IsEmptyStr(_model.toUserName)) {
        commetText                     = _model.content;
    }else {
        if (toUserName.length) {
            commetText                     = [NSString stringWithFormat:@"回复 %@:%@",toUserName,_model.content];
        }else {
            
            commetText                     = [NSString stringWithFormat:@"回复 %@:%@",_model.toUserName,_model.content];
        }
    }
    
    
    self.commentLabel.text             = commetText;
    [self.commentLabel sizeToFit];
    
    
    /// 名字
    [_nameLabel  mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_headImageView.mas_right).offset(10);
        make.top.equalTo(5);
        make.width.greaterThanOrEqualTo(@(10));
        make.width.lessThanOrEqualTo(@(100));
        make.height.equalTo(15);
    }];
    
    ///评论时间
    [_commentTimeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-15);
        make.top.equalTo(_nameLabel.mas_top);
        
    }];
    
    
    
    ///评论
    [_commentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_nameLabel.mas_bottom).offset(8);
        make.left.equalTo(_nameLabel.mas_left).offset(0);
        make.bottom.equalTo(-5);
        make.right.equalTo(-5);
        
    }];
    
    
}


+ (instancetype)cellWithTableView:(UITableView *)tableView{
    
    static NSString *ID        = @"CommentYableViewCell";
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    if (cell == nil) {
        // 注意可重用标示符需要在XIB中指定
        cell                   = [[CommentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    return cell;
    
}


#pragma mark --点击userHead

-(void)HeaderImageTap:(UITapGestureRecognizer*)tap{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"commentListHeadClick" object:@{@"userId":_model.userId}];
}




@end
