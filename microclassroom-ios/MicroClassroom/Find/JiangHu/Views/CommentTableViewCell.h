//
//  CommentTableViewCell.h
//  CarService
//
//  Created by apple on 15/8/4.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WFReplyBody.h"


@interface CommentTableViewCell : UITableViewCell

@property (nonatomic ,strong) WFReplyBody *model;
@property (strong, nonatomic)   UIImageView *headImageView;
@property (strong, nonatomic)   UILabel *nameLabel;
@property (strong, nonatomic)   UILabel *commentLabel;
@property (strong, nonatomic)   UILabel *commentTimeLabel;


@property (nonatomic ,copy)      NSString *CommentId; /**<  评论cellId  */


+ (instancetype)cellWithTableView:(UITableView *)tableView;



@end
