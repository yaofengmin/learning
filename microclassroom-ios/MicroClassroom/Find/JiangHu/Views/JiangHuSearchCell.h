//
//  JiangHuSearchCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseTableViewCell.h"

@interface JiangHuSearchCell : YHBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *touxianLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
