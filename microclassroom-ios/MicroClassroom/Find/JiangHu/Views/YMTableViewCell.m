

//
//  YMTableViewCell.m
//  WFCoretext
//
//  Created by Hanks on 16/8/9.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YMTableViewCell.h"
#import "WFReplyBody.h"
#import "YMTapGestureRecongnizer.h"
#import "WFHudView.h"
#import "ILRegularExpressionManager.h"
#import "NSString+MD5.h"
//#import "DBDataModel.h"
#import "ThumbModel.h"
#import "UIButton+WebCache.h"
#import "HXTagsView.h"

#import "VoteFileView.h"
#import "VoteBottomView.h"
#import "ZJAlertListView.h"
#import "MessageShareView.h"

#import "YHBaseWebViewController.h"

#define kImageTag 9999
#define kFileTag  99

@interface YMTableViewCell ()<ZJAlertListViewDelegate,ZJAlertListViewDatasource,UIAlertViewDelegate>
@property (nonatomic,assign) GetMessageListType type;
@end

@implementation YMTableViewCell
{
    UIButton             *foldBtn; // 展开的btn
    UIImageView          *_lastImageView; // 最后标记的图片
    UIButton             *_setTopBtn;// 置顶
    UILabel              *_titleLabel; //单位职务
    UIImageView          *_addVImageView;// +v
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier WithType:(GetMessageListType)type{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.type = type;
        _imageArray         = @[].mutableCopy;
        _commentTextArray   = @[].mutableCopy;
        _ymShuoshuoArray    = @[].mutableCopy;
        _shuoshuoBottomArry = @[].mutableCopy;
        _ymFavourArray      = @[].mutableCopy;
        _fileArray          = @[].mutableCopy;
        _voteOptionArray = [[NSMutableArray alloc]init];
        
        self.selectionStyle   = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        _userHeaderImage = [[UIImageView alloc]init];
        
        CALayer *layer = [_userHeaderImage layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:20.0];
        _userHeaderImage.userInteractionEnabled = YES;
        _userHeaderImage.contentMode = UIViewContentModeScaleAspectFill;
        _userHeaderImage.backgroundColor = ImageBackColor;
        [self.contentView addSubview:_userHeaderImage];
        
        
        [_userHeaderImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.top.mas_equalTo(15);
            make.size.mas_equalTo(CGSizeMake(TableHeader, TableHeader));
            
        }];
        
        UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(HeaderImageTap:)];
        [_userHeaderImage addGestureRecognizer:imageTap];
        
        
        _setTopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _setTopBtn.titleLabel.font = Font(12);
        [_setTopBtn setTitleColor:NEColor forState:UIControlStateNormal];
        [_setTopBtn setTitle:@" 置顶" forState:UIControlStateNormal];
        _setTopBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
        [_setTopBtn sizeToFit];
        _setTopBtn.userInteractionEnabled = NO;
        _setTopBtn.backgroundColor = [UIColor clearColor];
        //        _setTopBtn.layer.cornerRadius = 20 / 2.0;
        //        _setTopBtn.layer.masksToBounds = YES;
        [_setTopBtn setImage:[UIImage imageNamed:@"ic_sticktop"] forState:UIControlStateNormal];
        [self.contentView addSubview:_setTopBtn];
        
        _userNameLbl                  = [[UILabel alloc] init];
        _userNameLbl.textAlignment    = NSTextAlignmentLeft;
        _userNameLbl.font             = [UIFont boldSystemFontOfSize:16];
        _userNameLbl.textColor        = HColor;
        _userNameLbl.userInteractionEnabled = YES;
        [self.contentView addSubview:_userNameLbl];
        UITapGestureRecognizer *NameTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(HeaderImageTap:)];
        [_userNameLbl addGestureRecognizer:NameTap];
        
        _addVImageView = [[UIImageView alloc]init];
        _addVImageView.image = [UIImage imageNamed:@"ic_vip"];
        [self.contentView addSubview:_addVImageView];
        
        
        //单位职务
        _titleLabel = [UILabel labelWithText:@"" andFont:12 andTextColor:JColor andTextAlignment:0];
        [self.contentView addSubview:_titleLabel];
        
        
        ///发布时间lable
        _userTimeLabel                = [[UILabel alloc]init];
        _userTimeLabel.textAlignment  = NSTextAlignmentRight;
        _userTimeLabel.textColor      = JColor;
        _userTimeLabel.font           = Font(10);
        [_userTimeLabel sizeToFit];
        [self.contentView addSubview:_userTimeLabel];
        
        
        
        foldBtn                       = [UIButton buttonWithType:0];
        [foldBtn setTitle:@"查看完整内容" forState:0];
        foldBtn.backgroundColor       = [UIColor clearColor];
        foldBtn.titleLabel.font       = [UIFont systemFontOfSize:14];
        [foldBtn setTitleColor:NEColor forState:0];
        foldBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        [foldBtn addTarget:self action:@selector(foldText) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:foldBtn];
        
        //        _replyBtn                     = [YMButton buttonWithType:UIButtonTypeCustom];
        //        [_replyBtn setImage:[UIImage imageNamed:@"moment_btn_chat"] forState:UIControlStateNormal];
        //        [_replyBtn sizeToFit];
        //        [self.contentView addSubview:_replyBtn];
        
        
    }
    
    
    // 用户姓名
    [_userNameLbl mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.userHeaderImage.mas_top).offset(6);
        make.left.equalTo(self.userHeaderImage.mas_right).offset(12);
    }];
    
    [_addVImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(CGSizeMake(15, 15));
        make.left.equalTo(_userNameLbl.mas_right).offset(2);
        make.centerY.equalTo(_userNameLbl);
    }];
    
    //置顶
    [_setTopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.equalTo(_userNameLbl);
        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    
    [_titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_userNameLbl.mas_bottom).offset(5);
        make.left.equalTo(_userNameLbl);
    }];
    
    
    //时间按钮
    [_userTimeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(_titleLabel);
        make.right.mas_equalTo(-15);
    }];
    
    
    //回复
    
    //    [_replyBtn mas_updateConstraints:^(MASConstraintMaker *make) {
    //
    //        make.centerY.equalTo(_userNameLbl);
    //        make.right.equalTo(-10);
    //    }];
    
    
    _bottomView = [[UIView alloc]init];
    [self.contentView addSubview:_bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, kCommentH));
        make.centerX.mas_equalTo(self);
    }];
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = NCColor;
    [_bottomView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 1));
        make.top.mas_equalTo(_bottomView.mas_top);
        make.centerX.equalTo(_bottomView);
    }];
    
    //收藏
    _collectionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_collectionBtn setImage:[UIImage imageNamed:@"We_collect"] forState:UIControlStateNormal];
    [_collectionBtn setImage:[UIImage imageNamed:@"We_collect_sel"] forState:UIControlStateSelected];
    [_collectionBtn setTitle:@"收藏"forState:UIControlStateNormal];
    [_collectionBtn addTarget:self action:@selector(collectionAction:) forControlEvents:UIControlEventTouchUpInside];
    [_collectionBtn setTitleColor:JColor forState:UIControlStateNormal];
    [_collectionBtn setTitleColor:NGColor forState:UIControlStateSelected];
    _collectionBtn.titleLabel.font = Font(12);
    [self.contentView addSubview:_collectionBtn];
    [_collectionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(kCommentH);
        make.centerY.mas_equalTo(_bottomView);
    }];
    
    
    //评论
    _commentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_commentBtn setImage:[UIImage imageNamed:@"ic_comment2"] forState:UIControlStateNormal];
    _commentBtn.userInteractionEnabled = NO;
    [_commentBtn setTitle:@"55"forState:UIControlStateNormal];
    [_commentBtn setTitleColor:JColor forState:UIControlStateNormal];
    _commentBtn.titleLabel.font = Font(12);
    [self.contentView addSubview:_commentBtn];
    [_commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_collectionBtn.mas_left).offset(0);
        make.height.mas_equalTo(kCommentH);
        make.centerY.mas_equalTo(_bottomView);
    }];
    
    //看
    _seeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_seeBtn setImage:[UIImage imageNamed:@"We_See"] forState:UIControlStateNormal];
    [_seeBtn setTitle:@"55"forState:UIControlStateNormal];
    [_seeBtn setTitleColor:JColor forState:UIControlStateNormal];
    _seeBtn.titleLabel.font = Font(12);
    [self.contentView addSubview:_seeBtn];
    [_seeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_commentBtn.mas_left).offset(0);
        make.height.mas_equalTo(kCommentH);
        make.centerY.mas_equalTo(_bottomView);
    }];
    UIView *bottomLine = [[UIView alloc]init];
    bottomLine.backgroundColor = NCColor;
    [_bottomView addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 1));
        make.bottom.mas_equalTo(_bottomView.mas_bottom);
        make.centerX.equalTo(_bottomView);
    }];
    
    UIView *sectionLine = [[UIView alloc]init];
    sectionLine.backgroundColor = NCColor;
    [_bottomView addSubview:sectionLine];
    [sectionLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 10));
        make.bottom.mas_equalTo(bottomLine.mas_bottom);
        make.centerX.equalTo(bottomLine);
    }];
    
    
    [self.contentView layoutIfNeeded];
    
    return self;
}



#pragma mark ==== 展开按钮的文字==
- (void)foldText{
    
    if (_tempDate.foldOrNot == YES) {
        _tempDate.foldOrNot  = NO;
        [foldBtn setTitle:@"收起" forState:0];
    }else{
        _tempDate.foldOrNot  = YES;
        [foldBtn setTitle:@"查看完整内容" forState:0];
    }
    [_delegate changeFoldState:_tempDate onCellRow:self.stamp];
}




#pragma mark === 赋值操作====
- (void)setYMViewWith:(YMTextData *)ymData {
    _tempDate                          = ymData;
    //默认分享图片
    _tempDate.messageBody.shareImage =  [UIImage imageNamed:@"icon-80"];
    _userTimeLabel.text                = [NSString TimeToCurrentTime:_tempDate.messageBody.postTime.intValue];
    [_commentBtn setTitle:[NSString stringWithFormat:@" %ld",_tempDate.messageBody.commentCount.integerValue] forState:UIControlStateNormal];
    [_seeBtn setTitle:[NSString stringWithFormat:@" %@",_tempDate.messageBody.viewCount] forState:UIControlStateNormal];
    _collectionBtn.selected = _tempDate.messageBody.isCollection;
    if (self.deletedBtn.superview) {
        [self.deletedBtn removeFromSuperview];
    }
    
    _userHeaderImage.tag               =[_tempDate.messageBody.userId integerValue];
    _userNameLbl.tag                   = [_tempDate.messageBody.userId integerValue];
    
#pragma mark -  //头像 昵称 简介
    [_userHeaderImage sd_setImageWithURL:[NSURL URLWithString:_tempDate.messageBody.userPhoto] placeholderImage:[UIImage imageNamed:@"moren_head"]];
    
    _userNameLbl.text                  = _tempDate.messageBody.userName;
    _titleLabel.text = ymData.messageBody.title;
    _addVImageView.hidden = !_tempDate.messageBody.isCertified;
    if ([_tempDate.messageBody.isTop integerValue] == 0) {
        
        _setTopBtn.hidden = YES;
        
    }else
    {
        _setTopBtn.hidden = NO;
    }
    
    
    //移除说说view 避免滚动时内容重叠
    for ( int i = 0; i < _ymShuoshuoArray.count; i ++) {
        WFTextView * imageV = (WFTextView *)[_ymShuoshuoArray objectAtIndex:i];
        if (imageV.superview) {
            [imageV removeFromSuperview];
        }
    }
    
    [_ymShuoshuoArray removeAllObjects];
    
#pragma mark - // /////////添加说说view
    
    WFTextView *textView    = [[WFTextView alloc] init];
    textView.delegate       = self;
    textView.attributedData = ymData.attributedDataShuoshuo;
    textView.isFold         = ymData.foldOrNot;
    textView.textColor      = JColor;
    if (self.type == GetMessagesListTypeNoraml) {
        textView.defaultColor = JColor;
    }else{
        textView.defaultColor = HColor;
    }
    textView.isDraw         = YES;
    [textView setOldString:ymData.showShuoShuo andNewString:ymData.completionShuoshuo];
    [self.contentView addSubview:textView];
    
    BOOL foldOrnot                     = ymData.foldOrNot;
    float hhhh                         = foldOrnot?ymData.shuoshuoHeight:ymData.unFoldShuoHeight;
    if ([_tempDate.messageBody.userId isEqualToString:KgetUserValueByParaName(USERID)]) {
        [self.contentView addSubview:self.deletedBtn];
    }
    
    [_ymShuoshuoArray addObject:textView];
    
    if (ymData.islessLimit) {//小于最小限制 隐藏折叠展开按钮
        
        foldBtn.hidden         = YES;
    }else{
        foldBtn.hidden         = NO;
    }
    
    if (_tempDate.foldOrNot     == YES) {
        
        [foldBtn setTitle:@"查看完整内容" forState:0];
    }else{
        
        [foldBtn setTitle:@"收起" forState:0];
    }
    
    if (self.type == GetMessagesListTypeNoraml) {
        [self creatTextViewAndImageVWith:ymData textView:textView height:hhhh];
    }else if (self.type == GetMessagesVoteType){
        [self creatVoteViewWith:ymData textView:textView height:hhhh];
    }else if (self.type == GetMessageShareType){//分享
        [self creatShareViewWith:ymData textView:textView height:hhhh];
    }
#pragma mark ==== 点赞部分=====
    
    //    for ( int i = 0; i < _ymFavourArray.count; i ++) {
    //        UIButton *favourBtn = [_ymFavourArray objectAtIndex:i];
    //        if (favourBtn.superview) {
    //            [favourBtn removeFromSuperview];
    //
    //        }
    //    }
    //
    //    [_ymFavourArray removeAllObjects];
    
    //    for (int i = 0; i< _tempDate.messageBody.thumbs.count; i++) {
    //
    //        ThumbModel *model = _tempDate.messageBody.thumbs[i];
    //        UIButton *headBtn = [self getBtnWithImageUrl:model.userPhoto];
    //        headBtn.tag = i ;
    //        [headBtn addTarget:self action:@selector(likeBtnDown:) forControlEvents:UIControlEventTouchUpInside];
    //        [self.contentView addSubview:headBtn];
    //        [_ymFavourArray addObject:headBtn];
    //    }
    
    //    for (int i = 0 ; i< _ymFavourArray.count; i++) {
    //
    //        [_ymFavourArray[i] mas_makeConstraints:^(MASConstraintMaker *make) {
    //
    //            make.size.equalTo(CGSizeMake(30, 30));
    //
    //            if (_lastImageView) {
    //
    //                make.top.equalTo(_lastImageView.mas_bottom).offset(10);
    //
    //            }else
    //            {
    //                if (foldBtn.hidden == YES) {
    //                    make.top.equalTo(textView.mas_bottom).offset(10);
    //                }else
    //                {
    //                    make.top.equalTo(foldBtn.mas_bottom).offset(5);
    //                }
    //            }
    //
    //
    //            if (i==0) {
    //                make.left.equalTo(_userNameLbl.mas_left);
    //            }else
    //            {
    //                make.left.equalTo([_ymFavourArray[i -1] mas_right]).offset(5);
    //            }
    //
    //        }];
    //    }
    
#pragma mark === 评论模块 ==
    
    if (ymData.messageBody.showCommentTextView == YES) {
        //移除原始评论UI
        
        for (WFTextView *textView in _commentTextArray) {
            if (textView.superview) {
                [textView removeFromSuperview];
            }
        }
        
        [_commentTextArray removeAllObjects];
        
        //开始设置新的评论UI
        for (int i = 0; i < ymData.replyDataSource.count; i ++ ) {
            WFTextView *_ilcoreText    = [[WFTextView alloc] initWithFrame:CGRectMake(70, 0, KScreenWidth - 70, 30)];
            _ilcoreText.delegate       = self;
            _ilcoreText.replyIndex     = i;
            _ilcoreText.attributedData = ymData.attributedDataReply[i];
            _ilcoreText.isFold         = NO;
            _ilcoreText.textColor      = HColor;
            [self.contentView addSubview:_ilcoreText];
            
            WFReplyBody *body = (WFReplyBody *)[ymData.replyDataSource objectAtIndex:i];
            NSString *matchString;
            
            if ([body.toUserId integerValue]== 0) { //没有回复者
                
                matchString = [NSString stringWithFormat:@"%@:%@",body.userName,body.content];
            }else
            {
                matchString = [NSString stringWithFormat:@"%@回复%@:%@",body.userName,body.toUserName,body.content];
            }
            
            [_ilcoreText setOldString:matchString andNewString:[ymData.completionReplySource objectAtIndex:i]];
            [_commentTextArray addObject:_ilcoreText];
        }
        
        
        for (int i = 0 ;i < _commentTextArray.count ; i++) {
            
            CGFloat  _ilcoreListH = [_commentTextArray[i] getTextHeight];
            
            [_commentTextArray[i] mas_makeConstraints:^(MASConstraintMaker *make) {
                
                if (i== 0) {
                    
                    if (_tempDate.messageBody.thumbs.count) {
                        //            判断是否有图片
                        if (_lastImageView) {
                            
                            if (_tempDate.messageBody.thumbs.count) {
                                
                                make.top.equalTo(_lastImageView.mas_bottom).offset(10 + likeBtnHight );
                            }else
                                
                            {
                                make.top.equalTo(_lastImageView.mas_bottom).offset(10);
                            }
                            
                            
                        }else
                        {
                            // 判断是否折叠  //没有图片
                            if (foldBtn.hidden == YES) {
                                
                                make.top.equalTo(textView.mas_bottom).offset(15 +likeBtnHight);
                                
                            }else
                            {
                                make.top.equalTo(foldBtn.mas_bottom).offset(15 +likeBtnHight);
                            }
                        }
                        
                    }else {
                        
                        if (_lastImageView) {
                            
                            if (_tempDate.messageBody.thumbs.count) {
                                
                                make.top.equalTo(_lastImageView.mas_bottom).offset(10 + likeBtnHight );
                            }else
                                
                            {
                                make.top.equalTo(_lastImageView.mas_bottom).offset(10);
                            }
                            
                            
                        }else
                        {
                            // 判断是否折叠
                            if (foldBtn.hidden == YES) {
                                
                                make.top.equalTo(textView.mas_bottom).offset(15);
                                
                            }else
                            {
                                make.top.equalTo(foldBtn.mas_bottom).offset(15);
                            }
                        }
                    }
                    
                }else
                {
                    
                    make.top.equalTo([_commentTextArray[i-1] mas_bottom]).offset(10);
                }
                
                make.left.equalTo(_userTimeLabel.left);
                make.right.equalTo(-10);
                make.height.equalTo(_ilcoreListH);
            }];
        }
    }
    
    
    if (self.deletedBtn.superview) {
        [self.deletedBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.seeBtn.mas_left).offset(0);
            make.size.equalTo(CGSizeMake(30, 30));
            make.centerY.mas_equalTo(self.seeBtn);
        }];
    }
    
}

- (void)longClickWFCoretext:(NSString *)clickString replyIndex:(NSInteger)index{
    
    [_delegate longClickRichText:_stamp replyIndex:index];
}


- (void)clickWFCoretext:(NSString *)clickString replyIndex:(NSInteger)index{
    
    if ([clickString isEqualToString:@""]) {  // 去点赞列表
        
        [_delegate clickRichText:_stamp replyIndex:index];
        
    }else{
        //响应富文本中数字和网址的点击事件
        NSArray *arr1 =   [ILRegularExpressionManager matchMobileLink:clickString];
        if (arr1.count) {
            NSString *MobileStr = [[[arr1 objectAtIndex:0] allValues] objectAtIndex:0];
            if (MobileStr.length==11) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"确定要拨打电话 :%@ 吗",MobileStr] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alert showAlertWithCompletionHandler:^(NSInteger tag) {
                    if (tag == 1) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",MobileStr]]];
                    }
                }];
            }else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@":%@可能是电话号码",MobileStr] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alert showAlertWithCompletionHandler:^(NSInteger tag) {
                    if (tag == 1) {
                        
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",MobileStr]]];
                    }
                }];
                
            }
        }
        NSArray *arr2 = [ILRegularExpressionManager matchWebLink:clickString];
        if (arr2.count) {
            
            NSString *url = [arr2[0] allValues][0];
            if ([self.delegate respondsToSelector:@selector(urlStrClickWithStr:)]) {
                [self.delegate urlStrClickWithStr:url];
            }
        }
    }
    
}



#pragma mark - 点击action
- (void)tapImageView:(YMTapGestureRecongnizer *)tapGes{
    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [_delegate showImageViewWithImageViews:tapGes.appendArray byClickWhich:tapGes.view.tag - kImageTag];
    
    
}

#pragma mark --点击userHead

-(void)HeaderImageTap:(UITapGestureRecognizer*)tap{
    
    [_delegate userPhotoImageClick:self.stamp  adnUsrId:tap.view.tag];
    
}


-(BOOL)isZan
{
    for (ThumbModel *model in _tempDate.messageBody.thumbs) {
        if ([model.userId isEqualToString:KgetUserValueByParaName(USERID)]) {
            return YES;
        }
    }
    return NO;
}


-(UIButton *)getBtnWithImageUrl:(NSString *)urlStr {
    UIButton *likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [likeBtn sd_setImageWithURL:[NSURL URLWithString:urlStr] forState:UIControlStateNormal];
    [likeBtn setBackgroundColor:ImageBackColor];
    likeBtn.layer.cornerRadius = 15.0;
    likeBtn.layer.masksToBounds = YES;
    return likeBtn;
}


#pragma mark === 点赞的头像被点击===
-(void)likeBtnDown:(UIButton *)send {
    ThumbModel *model = [_tempDate.messageBody.thumbs objectAtIndex:send.tag];
    if ([self.delegate respondsToSelector:@selector(userPhotoImageClick:adnUsrId:)]) {
        [self.delegate userPhotoImageClick:0 adnUsrId:[model.userId integerValue]];
    }
    
}

#pragma mark - 删除说说
- (void)deletedBtnDown:(UIButton *)sender {
    [YHJHelp aletWithTitle:nil Message:@"确认删除这条信息吗" sureTitle:@"确定" CancelTitle:@"取消" SureBlock:^{
        NSDictionary *paramDic = @{Service:CircleDeleteMessage,
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"msgId":_tempDate.messageBody.msgId
                                   };
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    if ([self.delegate respondsToSelector:@selector(delectMsg:andIndext:)]) {
                        [self.delegate delectMsg:_tempDate andIndext:self.stamp];
                    }
                }else {
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];
    } andCancelBlock:nil andDelegate:self.viewController];
}


- (UIButton *)deletedBtn {
    if (!_deletedBtn) {
        _deletedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deletedBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deletedBtn setTitleColor:JColor forState:UIControlStateNormal];
        _deletedBtn.titleLabel.font = Font(12);
        [_deletedBtn addTarget:self action:@selector(deletedBtnDown:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return  _deletedBtn;
}
#pragma mark ==== 投票
-(void)creatVoteViewWith:(YMTextData *)ymData textView:(WFTextView *)textView height:(CGFloat )hhhh
{
    for (id view in self.contentView.subviews) {
        if ([view isKindOfClass:[VoteBottomView class]]) {
            [view removeFromSuperview];
        }
    }
    ///说说内容
    [textView mas_updateConstraints:^(MASConstraintMaker *make) {
        if (self.isShowLabel) {
            make.top.equalTo(self.userHeaderImage.mas_bottom).offset(5);
        }else{
            make.top.equalTo(_userHeaderImage.mas_bottom).offset(15);
        }
        make.left.equalTo(_userNameLbl.mas_left);
        make.right.equalTo(-15);
        make.height.equalTo(hhhh);
        
    }];
    /// 更多
    if (foldBtn.hidden==NO) {
        
        [foldBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(textView.mas_bottom).offset(10);
            make.left.equalTo(textView.mas_left).offset(0);
            make.size.equalTo(CGSizeMake(90, 15));
            
        }];
    }
    
    for (int i = 0; i < [_voteOptionArray count]; i++) {
        
        VoteFileView * fileView  = (VoteFileView *)[_voteOptionArray objectAtIndex:i];
        if (fileView.superview) {
            
            [fileView removeFromSuperview];
            
        }
    }
    [_voteOptionArray removeAllObjects];
    
    
    for (int i = 0; i < ymData.showOptipnArray.count; i ++) {
        VoteFileView *fileView = [[NSBundle mainBundle] loadNibNamed:@"VoteFileView" owner:self options:nil].firstObject;
        VoteListModel *model = ymData.showOptipnArray[i];
        fileView.optionName.text = model.question;
        NSString *picStr = @"";
        for (int j = 0; j < ymData.messageBody.pics.count; j ++) {
            picStr = ymData.messageBody.pics[i];
        }
        [fileView.optionImage sd_setImageWithURL:[NSURL URLWithString:picStr] placeholderImage:[UIImage imageNamed:@"We_option"]];
        if ([ymData.messageBody.qusId intValue] != 0) {
            fileView.percentLabel.hidden = NO;
            if ([ymData.messageBody.qusId isEqualToString:model.qusId]) {
                fileView.percentLabel.text = [NSString stringWithFormat:@"%@\n你的选择",model.voteLI];
            }else{
                fileView.percentLabel.text = model.voteLI;
            }
        }else{
            fileView.percentLabel.hidden = YES;
        }
        [self.contentView addSubview:fileView];
        [_voteOptionArray addObject:fileView];
    }
    NSInteger  judgePhone = 1;
    
    for (int i = 0; i < _voteOptionArray.count; i ++) {
        [_voteOptionArray[i] mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(40);
            make.left.equalTo(_userNameLbl.mas_left);
            
            if(i<judgePhone){//1
                if (foldBtn.hidden==YES) {
                    make.top.equalTo(textView.mas_bottom).offset(kOpDistance);
                }else {
                    make.top.equalTo(foldBtn.mas_bottom).offset(kOpDistance);
                }
                
            }else{// not 1
                make.top.equalTo([_voteOptionArray[i - 1] mas_bottom]).offset(10);
            }
            make.size.equalTo(CGSizeMake(KScreenWidth - 101, 40));
        }];
        
    }
    
    VoteBottomView *voteBottom = [[NSBundle mainBundle] loadNibNamed:@"VoteBottomView" owner:self options:nil].firstObject;
    
    NSDate *currentDate = [NSDate date];
    NSDate *endTime = [ymData.messageBody.endTime dateFromStringWithMin:ymData.messageBody.endTime];
    //统计票数
    int count = 0;
    for (VoteListModel *model in ymData.showOptipnArray) {//统计票数
        count = [model.voteCount intValue] + count;
    }
    //比较当前日期和有效日期
    int i = [YHJHelp compareOneDay:currentDate withAnotherDay:endTime];
    if (i == 1) {//当前日期大于截止日期
        voteBottom.deadline.text = @"已截止";
        voteBottom.voteBtn.hidden = YES;
        
        voteBottom.seeLabel.text = [NSString stringWithFormat:@"共计 : %d票",count];
    }else{//未过期
        voteBottom.deadline.text = [NSString stringWithFormat:@"截止: %@",ymData.messageBody.endTime];
        if ([ymData.messageBody.qusId intValue] == 0) {//未过期并且未投票
            voteBottom.voteBtn.hidden = NO;
            voteBottom.seeLabel.text = [NSString stringWithFormat:@"投票后查看结果"];
        }else{//已过期
            voteBottom.voteBtn.hidden = YES;
            voteBottom.seeLabel.text = [NSString stringWithFormat:@"共计 : %d票",count];
        }
    }
    voteBottom.voteBlock = ^(NSIndexPath *indexPath) {
        self.selectedIndexPath = indexPath;
        self.currentData = ymData;
        CGFloat height = self.voteOptionArray.count *40 + 81;
        ZJAlertListView *alertList = [[ZJAlertListView alloc] initWithFrame:CGRectMake(0, 0, 250, height)];
        alertList.datasource = self;
        alertList.delegate = self;
        //点击确定的时候，调用它去做点事情
        @weakify(alertList);
        [alertList setDoneButtonWithBlock:^{
            @strongify(alertList);
            VoteListModel *voteModel = self.tempDate.showOptipnArray[self.selectedIndexPath.row];
            [self addMessagesQusAWith:voteModel andIndext:self.stamp];
            [alertList dismiss];
            
        }];
        [alertList show];
    };
    [self.contentView addSubview:voteBottom];
    [voteBottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(KScreenWidth - 101, 35));
        make.left.mas_equalTo(textView);
        if (self.voteOptionArray.count) {
            make.top.mas_equalTo([self.voteOptionArray[self.voteOptionArray.count - 1] mas_bottom]).offset(kOpToBottom);
        }else{
            make.top.mas_equalTo(textView.mas_bottom).offset(kOpToBottom);
        }
    }];
    
}

#pragma mark === 文件展示
-(void)creatFileViewWith:(YMTextData *)ymData textView:(WFTextView *)textView
{
    for (int i = 0; i < [_fileArray count]; i++) {
        
        VoteFileView * fileView  = (VoteFileView *)[_fileArray objectAtIndex:i];
        if (fileView.superview) {
            
            [fileView removeFromSuperview];
            
        }
    }
    [_fileArray removeAllObjects];
    
    for (id view in self.contentView.subviews) {
        if ([view isKindOfClass:[VoteBottomView class]]) {
            [view removeFromSuperview];
        }
    }
    
    for (int i = 0; i < ymData.showFileArray.count; i ++) {
        VoteFileView *fileView = [[NSBundle mainBundle] loadNibNamed:@"VoteFileView" owner:self options:nil].firstObject;
        fileView.tag = kFileTag + i;
        NSString *webUrl = ymData.showFileArray[i];
        NSArray *fileNameArr = [webUrl componentsSeparatedByString:@"_"];
        fileView.optionName.text = fileNameArr.lastObject;//用_分割url,后面的是要显示的文件名
        fileView.percentLabel.hidden = YES;
        if ([webUrl hasSuffix:@"pdf"]) {//pdf
            fileView.optionImage.image = [UIImage imageNamed:@"pdf_file"];
        }else if ([webUrl hasSuffix:@"txt"]){
            fileView.optionImage.image = [UIImage imageNamed:@"txt_file"];
        }else if ([webUrl hasSuffix:@"xls"] || [webUrl hasSuffix:@"xlsx"]){
            fileView.optionImage.image = [UIImage imageNamed:@"excel_file"];
        }else if ([webUrl hasSuffix:@"doc"] || [webUrl hasSuffix:@"docx"]){
            fileView.optionImage.image = [UIImage imageNamed:@"word_file"];
        }else if ([webUrl hasSuffix:@"ppt"] || [webUrl hasSuffix:@"ppts"]){
            fileView.optionImage.image = [UIImage imageNamed:@"ppt_file"];
        }
        [self.contentView addSubview:fileView];
        YMTapGestureRecongnizer *fileTap     = [[YMTapGestureRecongnizer alloc] initWithTarget:self action:@selector(handleFileAction:)];
        fileTap.appendArray                  = ymData.showFileArray;
        [fileView addGestureRecognizer:fileTap];
        [_fileArray addObject:fileView];
        
    }
    NSInteger  judgePhone = 1;
    
    for (int i = 0; i < _fileArray.count; i ++) {
        [_fileArray[i] mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(40);
            make.left.equalTo(textView.mas_left).offset(0);
            
            if(i<judgePhone){//1
                if (_lastImageView == nil) {
                    if (foldBtn.hidden==YES) {
                        make.top.equalTo(textView.mas_bottom).offset(kOpToBottom);
                    }else {
                        make.top.equalTo(foldBtn.mas_bottom).offset(kOpToBottom);
                    }
                }else{
                    make.top.mas_equalTo(_lastImageView.mas_bottom).offset(kOpToBottom);
                }
                
            }else{// not 1
                make.top.equalTo([_fileArray[i - 1] mas_bottom]).offset(10);
            }
            make.size.equalTo(CGSizeMake(KScreenWidth - 101, 40));
        }];
        
    }
    
}

#pragma mark === 点击文件
-(void)handleFileAction:(YMTapGestureRecongnizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(showFileViewWithFiles:byClickWhich:index:)]) {
        [self.delegate showFileViewWithFiles:tap.appendArray byClickWhich:tap.view.tag - kFileTag index:self.stamp];
    }
}
-(CGFloat)cacluteLabelWidth:(NSString *)text
{
    if (text.length == 0) {
        return 0;
    }
    NSAttributedString *textStr = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]}];
    CGRect msgContentSize = [textStr boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 20) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil];
    
    return msgContentSize.size.width + 25;
}


#pragma mark ===底部功能
-(void)collectionAction:(UIButton *)sender
{
    self.collectionBlock(sender.selected);
}

#pragma mark - // /////////添加说说view
-(void)creatTextViewAndImageVWith:(YMTextData *)ymData textView:(WFTextView *)textView height:(CGFloat)hhhh
{
    for (id view in self.contentView.subviews) {
        if ([view isKindOfClass:[VoteBottomView class]]) {
            [view removeFromSuperview];
        }
    }
    HXTagsView *tagsView;
    if (_tempDate.messageBody.labelName.count) {
        //多行不滚动,则计算出全部展示的高度,让maxHeight等于计算出的高度即可,初始化不需要设置高度
        tagsView = [[HXTagsView alloc] initWithFrame:CGRectMake(64, 48, self.frame.size.width, 0)];
        tagsView.type = 0;
        tagsView.normalBackgroundImage = [UIImage imageNamed:@"label_normal"];
        tagsView.titleColor = JColor;
        tagsView.titleSize = 12;
        self.isShowLabel = YES;
        tagsView.isUserInter = NO;
        NSMutableArray *labelArr = [NSMutableArray array];
        for (NSDictionary *dic in _tempDate.messageBody.labelName) {
            [labelArr addObject:dic[@"codeName"]];
        }
        [tagsView setTagAry:labelArr delegate:self];
        
        [self.contentView addSubview:tagsView];
        [tagsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(64);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(17);
            make.width.mas_equalTo(KScreenWidth - 64 - 15);
            make.height.mas_equalTo(tagsView.frame.size.height);
        }];
        
    }else{
        for (id view in self.contentView.subviews) {
            if ([view isKindOfClass:[HXTagsView class]]) {
                [view removeFromSuperview];
            }
        }
        self.isShowLabel = NO;
    }
    
    
#pragma mark - /////// //图片部分
    _lastImageView = nil;
    for (int i = 0; i < [_imageArray count]; i++) {
        
        UIImageView * imageV   = (UIImageView *)[_imageArray objectAtIndex:i];
        if (imageV.superview) {
            
            [imageV removeFromSuperview];
            
        }
    }
    [_imageArray removeAllObjects];
    
    
    for (int  i = 0; i < [ymData.showImageArray count]; i++) {
        
        UIImageView *image              = [[UIImageView alloc]init];
        image.contentMode = UIViewContentModeScaleAspectFill;
        image.clipsToBounds = YES;
        image.userInteractionEnabled     = YES;
        YMTapGestureRecongnizer *tap     = [[YMTapGestureRecongnizer alloc] initWithTarget:self action:@selector(tapImageView:)];
        [image addGestureRecognizer:tap];
        tap.appendArray                  = ymData.showImageArray;
        image.backgroundColor            = ImageBackColor;
        image.tag                        = kImageTag + i;
        if ([ymData.showImageArray[i]isKindOfClass:[NSString class]]) {
            @weakify(self)
            [image sd_setImageWithURL:[NSURL URLWithString:[ymData.showImageArray objectAtIndex:i]] completed:^(UIImage *image, NSError *error, EMSDImageCacheType cacheType, NSURL *imageURL) {
                @strongify(self)
                if (i == 0) {
                    self.tempDate.messageBody.shareImage = image;
                };
            }];
        }
        
        [self.contentView addSubview:image];
        [_imageArray addObject:image];
        _lastImageView = image;
        
    }
    
    
    ///说说内容
    [textView mas_updateConstraints:^(MASConstraintMaker *make) {
        if (self.isShowLabel) {
            make.top.equalTo(tagsView.mas_bottom).offset(17);
        }else{
            make.top.equalTo(_titleLabel.mas_bottom).offset(17);
        }
        make.left.equalTo(_userNameLbl.mas_left);
        make.right.equalTo(-15);
        make.height.equalTo(hhhh);
        
    }];
    /// 更多
    if (foldBtn.hidden==NO) {
        
        [foldBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(textView.mas_bottom).offset(10);
            make.left.equalTo(textView.mas_left).offset(0);
            make.size.equalTo(CGSizeMake(90, 15));
            
        }];
    }
    
    
    _lastImageView = nil;
    ///照片排列
    NSInteger  judgePhone ;
    __block int row_index=0;
    if (IPHONE_6P) {
        judgePhone = 4;
    }else {
        judgePhone = 3;
    }
    
    if (_imageArray.count==1) {
        float image_with   =  100;
        float image_heght  =  100;
//        if ([ymData.showImageArray[0] isKindOfClass:[NSString class]]) {
//            NSString *urlStr =  ymData.showImageArray[0];
//            NSArray  *tempArr = [urlStr componentsSeparatedByString:@"_"];
//            if (tempArr.count>2) {
//                // 排列单张
//                image_with  =  ceil([tempArr[2]  floatValue]/2);
//                image_heght =  ceil([tempArr[1]  floatValue]/2);
//            }
//        }
        [_imageArray[0] mas_updateConstraints:^(MASConstraintMaker *make) {
            if (foldBtn.hidden==YES) {
                make.top.equalTo(textView.mas_bottom).offset(kDistance);
            }else {
                make.top.equalTo(foldBtn.mas_bottom).offset(kDistance);
            }
            make.left.equalTo(textView.mas_left);
            make.size.equalTo(CGSizeMake(image_with, image_heght));
        }];
        
        _lastImageView  = _imageArray[0];
        
    }else{
        
        for (int i = 0 ; i < _imageArray.count; i ++) {
            
            [_imageArray[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(_imageArray[i]);
                if (i%judgePhone ==0) {
                    make.left.equalTo(textView.mas_left).offset(0);
                    row_index++;
                }else{
                    
                    make.left.equalTo([_imageArray[i-1] mas_right]).offset(5);
                }
                
                if(i<judgePhone){//1
                    if (foldBtn.hidden==YES) {
                        make.top.equalTo(textView.mas_bottom).offset(kDistance);
                    }else {
                        make.top.equalTo(foldBtn.mas_bottom).offset(kDistance);
                    }
                    
                }else{// not 1
                    make.top.equalTo([_imageArray[(row_index-2)*judgePhone] mas_bottom]).offset(kDistance);
                }
                make.size.equalTo(CGSizeMake(70, 70));
                _lastImageView = _imageArray[i];
            }];
        }
        
    }
    
    [self creatFileViewWith:self.tempDate textView:textView];
    
}

#pragma mark ==== 投票选择弹出框
#pragma mark -设置行数
- (NSInteger)alertListTableView:(ZJAlertListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.currentData.messageBody.qusList.count;
}

- (UITableViewCell *)alertListTableView:(ZJAlertListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VoteListModel *model = self.currentData.messageBody.qusList[indexPath.row];
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusableAlertListCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if ( self.selectedIndexPath && NSOrderedSame == [self.selectedIndexPath compare:indexPath])
    {
        cell.imageView.image = [UIImage imageNamed:@"btn_select_sel"];
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"btn_select_nor"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = model.question;
    cell.textLabel.font = Font(14);
    cell.textLabel.textColor = JColor;
    return cell;
}

- (void)alertListTableView:(ZJAlertListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView alertListCellForRowAtIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:@"btn_select_nor"];
}

- (void)alertListTableView:(ZJAlertListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    UITableViewCell *cell = [tableView alertListCellForRowAtIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:@"btn_select_sel"];
}

#pragma mark === 投票
-(void)addMessagesQusAWith:(VoteListModel *)voteModel andIndext:(NSInteger)indext
{
    [self requestAddVoteMessageWithModel:voteModel indext:indext];
}

-(void)requestAddVoteMessageWithModel:(VoteListModel *)model indext:(NSInteger )indext
{
    
    NSDictionary *paramDic = @{Service:CircleAddMessagesQusA,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"qusId":model.qusId,
                               @"msgId":self.tempDate.messageBody.msgId};
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                if ([self.delegate respondsToSelector:@selector(addMessagesQusAFinish)]) {
                    [self.delegate addMessagesQusAFinish];
                }
            }else {
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
    }];
    
}


#pragma mark === 分享动态
-(void)creatShareViewWith:(YMTextData *)ymData textView:(WFTextView *)textView height:(CGFloat)hhhh
{
    for (id view in self.contentView.subviews) {
        if ([view isKindOfClass:[VoteBottomView class]]) {
            [view removeFromSuperview];
        }
    }
    
    HXTagsView *tagsView;
    if (_tempDate.messageBody.labelName.count) {
        //多行不滚动,则计算出全部展示的高度,让maxHeight等于计算出的高度即可,初始化不需要设置高度
        tagsView = [[HXTagsView alloc] initWithFrame:CGRectMake(64, 48, self.frame.size.width, 0)];
        tagsView.type = 0;
        tagsView.normalBackgroundImage = [UIImage imageNamed:@"label_normal"];
        tagsView.titleColor = JColor;
        tagsView.titleSize = 12;
        self.isShowLabel = YES;
        tagsView.isUserInter = NO;
        NSMutableArray *labelArr = [NSMutableArray array];
        for (NSDictionary *dic in _tempDate.messageBody.labelName) {
            [labelArr addObject:dic[@"codeName"]];
        }
        [tagsView setTagAry:labelArr delegate:self];
        
        [self.contentView addSubview:tagsView];
        [tagsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(64);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(17);
            make.width.mas_equalTo(KScreenWidth - 64 - 15);
            make.height.mas_equalTo(tagsView.frame.size.height);
        }];
        
    }else{
        for (id view in self.contentView.subviews) {
            if ([view isKindOfClass:[HXTagsView class]]) {
                [view removeFromSuperview];
            }
        }
        self.isShowLabel = NO;
    }
    
    MessageShareView *shareView = [[NSBundle mainBundle] loadNibNamed:@"MessageShareView" owner:self options:nil].firstObject;
    [self.contentView addSubview:shareView];
    YMTapGestureRecongnizer *tap = [[YMTapGestureRecongnizer alloc]initWithTarget:self action:@selector(handleWebGesture:)];
    tap.webBody = ymData.messageBody;
    [shareView addGestureRecognizer:tap];
    if (ymData.messageBody.pics.count) {
        [shareView.logoImage sd_setImageWithURL:[NSURL URLWithString:ymData.messageBody.pics.firstObject] placeholderImage:[UIImage imageNamed:@"login_icon"]];
    }else{
        shareView.logoImage.image = [UIImage imageNamed:@"login_icon"];
    }
    shareView.shareTitle.text = ymData.messageBody.content;
    [shareView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.isShowLabel) {
            make.top.equalTo(tagsView.mas_bottom).offset(15);
        }else{
            make.top.equalTo(_userHeaderImage.mas_bottom).offset(15);
        }
        make.left.equalTo(_userNameLbl.mas_left);
        make.right.equalTo(-15);
        make.height.mas_equalTo(50);
    }];
    
}
#pragma mark === 点击班级web网页
-(void)handleWebGesture:(YMTapGestureRecongnizer *)ges
{
    WFMessageBody *body = ges.webBody;
    if ([self.delegate respondsToSelector:@selector(showWebViewWithlink:title:index:)]) {
        [self.delegate showWebViewWithlink:body.link title:body.content index:self.stamp];
    }
}

@end
