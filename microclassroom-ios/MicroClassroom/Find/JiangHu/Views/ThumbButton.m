//
//  ThumbButton.m
//  MicroClassroom
//
//  Created by fm on 2017/11/7.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "ThumbButton.h"

@implementation ThumbButton
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = NEColor;
        self.layer.cornerRadius = 25 / 2.0;
        self.layer.masksToBounds = YES;
        [self initalSub];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

#pragma mark === 初始化
-(void)initalSub
{
    //点赞数量
    _thumbNum = [UILabel labelWithText:@"" andFont:12 andTextColor:BColor andTextAlignment:NSTextAlignmentLeft];
    [self addSubview:_thumbNum];
    [_thumbNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(8);
        make.centerY.mas_equalTo(self);
    }];
    
    
    //点赞图片
    _thumbImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"btn_good"]];
    _thumbImage.userInteractionEnabled = YES;
    [self addSubview:_thumbImage];
    [_thumbImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_thumbNum.mas_right).offset(5);
        make.centerY.mas_equalTo(_thumbNum);
    }];
    
    
    
}
-(void)tapAction:(UIGestureRecognizer *)tap
{
    
    [self zanAnimationPlay];
}

-(void)zanAnimationPlay{
    [self setIsZan:!self.isZan];
    if (self.clickHandler!=nil) {
        self.clickHandler(self);
    }
}
#pragma mark - Property method
-(void)setIsZan:(BOOL)isZan{
    _isZan=isZan;
}


@end

