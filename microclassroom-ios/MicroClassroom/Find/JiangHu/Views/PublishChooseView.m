
//
//  PublishChooseView.m
//  yanshan
//
//  Created by fm on 2017/9/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "PublishChooseView.h"

@implementation PublishChooseView
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.publishVote.layer.cornerRadius = 2.0;
    self.publishVote.layer.masksToBounds = YES;
    self.publishCircle.layer.cornerRadius = 2.0;
    self.publishCircle.layer.masksToBounds = YES;
    self.layer.shadowColor = KColorFromRGB(0xc3c3c3).CGColor;
    self.layer.shadowOpacity = 0.44f;
    self.layer.shadowRadius = 3.f;
    self.layer.shadowOffset = CGSizeMake(3,3);

}


#pragma mark === 发布投票
- (IBAction)publishVoteAction:(UIButton *)sender {
    sender.backgroundColor = BColor;
    if ([self.delegate respondsToSelector:@selector(publishTypeChooseWithSender:)]) {
        [self.delegate publishTypeChooseWithSender:sender];
    }
}

- (IBAction)publicBtnHiglitedAction:(UIButton *)sender {
    sender.backgroundColor = KColorFromRGB(0xedeff0);
}

#pragma mark === 发布动态
- (IBAction)publishCircleAction:(UIButton *)sender {
    sender.backgroundColor = BColor;
    if ([self.delegate respondsToSelector:@selector(publishTypeChooseWithSender:)]) {
        [self.delegate publishTypeChooseWithSender:sender];
    }
}
- (IBAction)publishVoteBtnHiglited:(UIButton *)sender {
        sender.backgroundColor = KColorFromRGB(0xedeff0);
}


@end
