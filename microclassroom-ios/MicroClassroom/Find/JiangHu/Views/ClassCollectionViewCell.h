//
//  ClassCollectionViewCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/10.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *classLabel;

@end
