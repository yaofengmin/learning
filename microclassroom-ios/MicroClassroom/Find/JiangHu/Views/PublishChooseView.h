//
//  PublishChooseView.h
//  yanshan
//
//  Created by fm on 2017/9/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol publishChoose <NSObject>

-(void)publishTypeChooseWithSender:(UIButton *)sender;

@end
@interface PublishChooseView : UIView
@property (weak, nonatomic) IBOutlet UIButton *publishCircle;
@property (weak, nonatomic) IBOutlet UIButton *publishVote;

@property (nonatomic,weak) id<publishChoose> delegate;
@end
