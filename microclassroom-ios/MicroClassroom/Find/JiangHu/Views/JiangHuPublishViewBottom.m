//
//  JiangHuPublishViewBottom.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "JiangHuPublishViewBottom.h"
#import "SettingTipsViewController.h"
#import "CodeListModel.h"

@interface JiangHuPublishViewBottom ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property(nonatomic ,strong) UITableView *tabelView;
@property(nonatomic ,strong) NSArray     *dataArr;
@property(nonatomic ,strong) UITableViewCell *tipCell;
@end

@implementation JiangHuPublishViewBottom

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatUI];
    }
    return self;
}


- (void)creatUI {
   
    [self configCell];
    [self addSubview:self.tabelView];
    
    [self.tabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(0);
    }];
    
}



- (UITableView *)tabelView
{
    if (!_tabelView) {
        _tabelView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, self.viewHeight) style:UITableViewStylePlain];
        _tabelView.delegate = self;
        _tabelView.dataSource = self;
  
        if ([_tabelView respondsToSelector:@selector(setSeparatorInset:)])
        {
            [_tabelView setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([_tabelView respondsToSelector:@selector(setLayoutMargins:)])
        {
            [_tabelView setLayoutMargins:UIEdgeInsetsZero];
        }
    }
    
    return _tabelView;
}


- (UITextField*)textField{
    
    if (!_textField) {
        
        _textField             = [[UITextField alloc]init];
        _textField.placeholder = @"输入您愿悬赏的金额";
        _textField.textColor   = kNavigationBarColor;
        _textField.font        = Font(15);
    }
    
    return _textField;
}


- (void)configCell{
    
    UITableViewCell *textFCell = [[UITableViewCell alloc]initWithStyle:0 reuseIdentifier:@"cell1"];
    textFCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [textFCell.contentView addSubview:self.textField];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.equalTo(0);
        make.left.equalTo(15);
        make.size.equalTo(CGSizeMake(180, 38));
    }];
    
    _tipCell = [[UITableViewCell alloc]initWithStyle:1 reuseIdentifier:@"cell2"];
    _tipCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    _tipCell.textLabel.text = @"设置标签";
    _tipCell.textLabel.textColor = CColor;
    _tipCell.detailTextLabel.textColor = kNavigationBarColor;
    _tipCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _dataArr = @[textFCell,_tipCell];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return _dataArr[indexPath.row];
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        
      SettingTipsViewController *setTip = [[SettingTipsViewController alloc]init];
     [self.viewController.navigationController pushViewController:setTip animated:YES];
        
        @weakify(self);
        setTip.selectFinishBloc = ^(CodeListModel *model){
            @strongify(self);
            
            self.model = model;
            self.tipCell.detailTextLabel.text = self.model.codeName;
            
        };
        
    }
}




- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}






@end
