//
//  VoteBottomView.h
//  yanshan
//
//  Created by fm on 2017/8/27.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoteBottomView : UIView
@property (weak, nonatomic) IBOutlet UILabel *seeLabel;
@property (weak, nonatomic) IBOutlet UILabel *deadline;
@property (weak, nonatomic) IBOutlet UIButton *voteBtn;

@property (nonatomic,strong) NSIndexPath *indexPath;

@property (nonatomic,copy) void(^voteBlock) (NSIndexPath *indexPath);

@end
