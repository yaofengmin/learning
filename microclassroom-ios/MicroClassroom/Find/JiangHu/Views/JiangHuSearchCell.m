//
//  JiangHuSearchCell.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "JiangHuSearchCell.h"
#import "YMTextData.h"

@interface JiangHuSearchCell ()
@property (nonatomic ,strong) UILabel *tipLabel;
@end

@implementation JiangHuSearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.headImageView.backgroundColor = ImageBackColor;
    self.headImageView.layer.cornerRadius = 25;
    self.headImageView.layer.masksToBounds = YES;
}

- (void)configWithModel:(YMTextData *)cellModel {

    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:cellModel.messageBody.userPhoto]];
    self.nameLabel.text = cellModel.messageBody.userName;
    self.touxianLabel.text = cellModel.messageBody.title;
    self.contentLabel.text = cellModel.messageBody.content;
    if (cellModel.messageBody.labelName) {
         self.tipLabel.hidden = NO;
         CGFloat width = [YHJHelp sizeWithWidth:100 andFont:Font(11) andString:cellModel.messageBody.labelName].width;
        self.tipLabel.text = cellModel.messageBody.labelName;
        [self.tipLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(CGSizeMake((width>20?(width +5):20),15));
        }];
    }else {
        self.tipLabel.hidden = YES;
    }

}
-(UILabel *)tipLabelWithTitle:(NSString *)title {
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    label.backgroundColor = KColorFromRGB(0xffeeee);
    label.textColor = KColorFromRGB(0xff3534);
    label.layer.cornerRadius = 4.0;
    label.layer.borderColor = KColorFromRGB(0xffa7a6) .CGColor;
    label.layer.borderWidth = 0.5;
    label.layer.masksToBounds = YES;
    label.font = Font(10);
    return label;
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [self tipLabelWithTitle:@""];
        [self.contentView addSubview:_tipLabel];
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.headImageView.mas_right).offset(10);
            make.top.equalTo(self.nameLabel.mas_bottom).offset(5);
        }];
    }
    return _tipLabel;
}

@end
