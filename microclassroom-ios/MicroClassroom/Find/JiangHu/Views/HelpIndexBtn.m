//
//  HelpIndexViewCell.m
//  CarService
//
//  Created by yunbo on 15/7/17.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "HelpIndexBtn.h"

@implementation HelpIndexBtn


-(HelpIndexBtn *)initWithFrame:(CGRect)frame andIndextBtnTitile:(NSString*)title andTag:(NSInteger)btnTag{
    if (self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 87, 22)]) {
        [self setBackgroundImage:[UIImage imageNamed:@"222-02"] forState:UIControlStateNormal];
        [self setBackgroundImage:[UIImage imageNamed:@"222-01"] forState:UIControlStateSelected];
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       
        //上左下右
       
        self.titleLabel.font = [UIFont systemFontOfSize: 14.0];
        [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        self.tag = btnTag;
    }
    
    return self;
    
}


+(id)buttonWithType:(UIButtonType)buttonType{
    
    return UIButtonTypeCustom;
}

@end
