//
//  ClassPublishViewBottom.m
//  CarService
//
//  Created by apple on 15/7/24.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "ClassPublishViewBottom.h"
#import "ClassModel.h"
#import "PublishCollectionLayout.h"
#import "ClassCollectionViewCell.h"

static const CGFloat collectH = 50.0;

@interface ClassPublishViewBottom ()<DIYPickViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    CGFloat _labelWidth ;
}

@property (nonatomic , assign)  BOOL     isAgree;
@property (nonatomic , strong) ClassModel    *classModel;
@property (nonatomic , strong) NSArray<ClassModel *>   *dataArr;
@property (nonatomic ,strong) UIButton  *agreeBtn;
@property (nonatomic ,strong) UICollectionView *collectionView;
@end

@implementation ClassPublishViewBottom

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _labelWidth = KScreenWidth / 3;
        self.backgroundColor = [UIColor whiteColor];
        [self requestFarm];
        [self configCollectionView];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _labelWidth = KScreenWidth / 3;
        self.backgroundColor = [UIColor whiteColor];
        [self requestFarm];
        [self configCollectionView];
    }
    return self;
}

-(void)configCollectionView
{
    UICollectionViewLayout *layout = [[PublishCollectionLayout alloc]init];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, collectH) collectionViewLayout:layout];
//    _collectionView.layer.borderColor = FColor.CGColor;
//    _collectionView.layer.borderWidth = 0.5;
    [self addSubview:_collectionView];
    
    _collectionView.backgroundColor =  [UIColor whiteColor];
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.alwaysBounceHorizontal = YES;
    _collectionView.contentSize = CGSizeMake(_dataArr.count * _labelWidth , 50);
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    [_collectionView registerNib:[UINib nibWithNibName:@"ClassCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ClassCollectionViewCell"];
    
    
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataArr.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ClassCollectionViewCell *cell =  [collectionView dequeueReusableCellWithReuseIdentifier:@"ClassCollectionViewCell" forIndexPath:indexPath];
    
    cell.classLabel.text = _dataArr[indexPath.row].className;
    
    return cell;
    
}


#pragma mark === 点击===
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView setContentOffset:CGPointMake(indexPath.row * _labelWidth, 0) animated: YES];
    if (indexPath.row >= _dataArr.count) {
        return;
    }
    _classModel = _dataArr[indexPath.row];
    
    if ([self.delegate respondsToSelector:@selector(publicWithClassModel:)]) {
        
        [self.delegate publicWithClassModel:_classModel];
    }
}


#pragma mark === 拖动 ===
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewEndAction:scrollView];
}



-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self scrollViewEndAction:scrollView];
}


-(void)scrollViewEndAction:(UIScrollView *) scrollView
{
    
    CGFloat countFloat = scrollView.contentOffset.x / _labelWidth;
    NSInteger count = (NSInteger)countFloat;
    
    if (count < 0 || count > _dataArr.count) {
        return;
    }
    
    if (countFloat - count >= 0.5) {
        
        count += 1;
    }
    
    [scrollView setContentOffset:CGPointMake(count * _labelWidth, 0) animated: YES];
    if (count>= _dataArr.count) {
        return;
    }
    _classModel = _dataArr[count];
    
    if ([self.delegate respondsToSelector:@selector(publicWithClassModel:)]) {
        [self.delegate publicWithClassModel:_classModel];
    }
}



-(void)requestFarm
{
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{Service:UserGetUserClasses,USERID:KgetUserValueByParaName(USERID)} andBlocks:^(NSDictionary *result) {
        
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _dataArr = [ClassModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                if (_dataArr.count) {
                    [_collectionView reloadData];
                    [self.delegate publicWithClassModel:_dataArr[0]];
                }
            }
        }else
        {
            self.hidden = YES;
        }
        
    }];
}




@end
