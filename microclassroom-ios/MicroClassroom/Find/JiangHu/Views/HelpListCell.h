//
//  HelpListCell.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseTableViewCell.h"
@class HelpListModel;
@interface HelpListCell : YHBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *helpStautsLabel;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *refuseBtn;
// 1=求助中 2=解决中 3=成功 4=失败
@property (copy, nonatomic) void(^helpStateChangeBlock)(JiangHuHelpType type);
@property (assign, nonatomic) NSInteger indextPathRow;
@property (assign, nonatomic) JiangHuHelpType currentType;
@end
