//
//  HelpIndexViewCell.h
//  CarService
//
//  Created by yunbo on 15/7/17.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpIndexBtn : UIButton

-(HelpIndexBtn *)initWithFrame:(CGRect)frame andIndextBtnTitile:(NSString*)title andTag:(NSInteger)btnTag;

@end
