//
//  VoteFileView.h
//  yanshan
//
//  Created by fm on 2017/8/27.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoteFileView : UIView
@property (weak, nonatomic) IBOutlet UILabel *optionName;
@property (weak, nonatomic) IBOutlet UIImageView *optionImage;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;

@end
