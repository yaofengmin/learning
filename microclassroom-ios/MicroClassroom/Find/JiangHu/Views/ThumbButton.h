//
//  ThumbButton.h
//  MicroClassroom
//
//  Created by fm on 2017/11/7.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThumbButton : UIView
@property (nonatomic,strong) UILabel *thumbNum;
@property (nonatomic,strong) UIImageView *thumbImage;

@property (nonatomic, copy) void (^clickHandler)(ThumbButton *zanButton);

@property (nonatomic,assign) NSInteger  appendIndexPath;

@property (nonatomic) BOOL isZan;

@end
