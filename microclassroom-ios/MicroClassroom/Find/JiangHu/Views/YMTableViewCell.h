
//
//  YMTableViewCell.m
//  WFCoretext
//
//  Created by Hanks on 16/8/9.
//  Copyright © 2016年 Hanks. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "YMTextData.h"
#import "WFTextView.h"
#import "YMButton.h"
#import "CatZanButton.h"
#import "VoteListModel.h"


#define TableHeader  40
#define likeBtnHight  40

@protocol cellDelegate <NSObject>

@optional
- (void)changeFoldState:(YMTextData *)ymD onCellRow:(NSInteger) cellStamp;
- (void)showImageViewWithImageViews:(NSArray *)imageViews byClickWhich:(NSInteger)clickTag;
- (void)clickRichText:(NSInteger)index replyIndex:(NSInteger)replyIndex;
- (void)longClickRichText:(NSInteger)index replyIndex:(NSInteger)replyIndex;
- (void)userPhotoImageClick:(NSInteger) index adnUsrId:(NSInteger)userId;
- (void)delectMsg:(YMTextData *)ymd andIndext:(NSInteger) indext;
- (void)addMessagesQusAFinish;  //投票接口
//文件
- (void)showFileViewWithFiles:(NSArray *)files byClickWhich:(NSInteger)clickTag index:(NSInteger )stamp;
//web
- (void)showWebViewWithlink:(NSString *)linkUrl title:(NSString *)title index:(NSInteger )stamp;

/**
 网址点击
 
 @param url url
 */
- (void)urlStrClickWithStr:(NSString *)url;

@end

@interface YMTableViewCell : UITableViewCell<WFCoretextDelegate>
@property (nonatomic,strong) NSMutableArray * imageArray;
/**
 *  评论数组
 */
@property (nonatomic,strong) NSMutableArray * commentTextArray;
@property (nonatomic,strong) NSMutableArray * ymShuoshuoArray;
@property (nonatomic,strong) NSMutableArray * shuoshuoBottomArry;
@property (nonatomic,strong) NSMutableArray * voteOptionArray;
@property (nonatomic,strong) NSMutableArray * fileArray;

@property (nonatomic,assign) id<cellDelegate> delegate;
@property (nonatomic,assign) NSInteger stamp;
@property (nonatomic,strong) CatZanButton   *supportBtn;
@property (nonatomic,strong) YMButton *replyBtn;

//投票弹出框需要用到
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic,strong) YMTextData *currentData;

@property (nonatomic,assign) BOOL isShowLabel;//是否显示标签

//次数显示区
@property (nonatomic,strong) UIView *bottomView;
@property(nonatomic ,strong) UIButton    *deletedBtn;//删除

@property (nonatomic,strong) UIButton *seeBtn;//查看次数
@property (nonatomic,strong) UIButton *commentBtn;//评论数量
@property (nonatomic,strong) UIButton *collectionBtn;//收藏
@property (nonatomic,copy) void(^collectionBlock) (BOOL isCollection);
/**
 *  用户头像imageview
 */
@property (nonatomic,strong) UIImageView *userHeaderImage;

/**
 *  用户昵称label
 */
@property (nonatomic,strong) UILabel *userNameLbl;


/**
 *  用户发布时间信息
 */
@property (nonatomic,strong) UILabel *userTimeLabel;
/**
 *  点赞图片
 */
@property (nonatomic,strong) UIImageView *favourImage;//点赞的图
@property (nonatomic,strong) NSMutableArray * ymFavourArray;
@property (nonatomic,weak) UIViewController *parentContr;

@property(nonatomic ,strong) YMTextData *tempDate;

- (void)setYMViewWith:(YMTextData *)ymData;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier WithType:(GetMessageListType)type;
@end
