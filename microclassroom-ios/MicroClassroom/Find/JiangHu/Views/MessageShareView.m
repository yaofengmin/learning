//
//  MessageShareView.m
//  MicroClassroom
//
//  Created by yfm on 2017/11/28.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "MessageShareView.h"

@implementation MessageShareView
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.logoImage.layer.cornerRadius = 2.0;
    self.logoImage.layer.masksToBounds = YES;
    self.logoImage.contentMode = UIViewContentModeScaleAspectFill;
    self.logoImage.clipsToBounds = YES;
    self.layer.cornerRadius = 2.0;
    self.layer.masksToBounds = YES;
}

@end
