//
//  ClassPublishViewBottom.h
//  CarService
//
//  Created by apple on 15/7/24.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ClassModel;
@protocol ClassPublishViewBottomDeleage <NSObject>

-(void)publicWithClassModel:(ClassModel *)model;


@end


@interface ClassPublishViewBottom : UIView

@property (nonatomic ,assign) id <ClassPublishViewBottomDeleage> delegate ;


@end
