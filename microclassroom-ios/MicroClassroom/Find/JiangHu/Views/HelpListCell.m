//
//  HelpListCell.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/4.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "HelpListCell.h"
#import "HelpListModel.h"
#import <UIImageView+WebCache.h>

@interface HelpListCell()

@property(nonatomic, strong) HelpListModel *model;

@end

@implementation HelpListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.headImageView.layer.cornerRadius  = 20;
    self.headImageView.backgroundColor     = ImageBackColor;
    self.headImageView.layer.masksToBounds = YES;
    self.acceptBtn.layer.cornerRadius      = 4.0;
    self.acceptBtn.layer.masksToBounds     = YES;
    self.refuseBtn.layer.cornerRadius      = 4.0;
    self.refuseBtn.layer.masksToBounds     = YES;
}

- (void)configWithModel:(HelpListModel *)cellModel {
    _model = cellModel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:cellModel.userPhoto]];
    self.nickNameLabel.text = cellModel.userName;
    /**helps数组中的state 1=等待接受帮助 2=已接授  3=不接受 4=帮助完成  5=帮助失败*/
    
    switch (self.currentType) {
        case HelpIngType:{
            switch ([cellModel.state integerValue]) {
                case 1:
                {
                    [self.acceptBtn setTitle:@"接受" forState:UIControlStateNormal];
                    [self.refuseBtn setTitle:@"拒绝" forState:UIControlStateNormal];
                    self.helpStautsLabel.text = @"";
                }
                    break;
                case 3:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"已拒绝";
                }
                    break;
                case 5:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"帮助失败";
                }

                default:
                    break;
            }
        }
            break;
        case HelpWorkIng:{
            switch ([cellModel.state integerValue]) {
                case 1:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"";
                }
                    break;
                case 2:
                {
                    [self.acceptBtn setTitle:@"完成" forState:UIControlStateNormal];
                    [self.refuseBtn setTitle:@"放弃" forState:UIControlStateNormal];
                    self.helpStautsLabel.text = @"";
                }
                    break;
                case 3:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"已拒绝";
                }
                    break;
                case 5:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"帮助失败";
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case HelpSuccessType:{
            switch ([cellModel.state integerValue]) {
                case 1:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"";
                }
                    break;
                case 2:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"";
                }
                    break;
                case 3:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"已拒绝";
                }
                    break;
                case 4:
                {
                   self.acceptBtn.hidden = YES;
                   self.refuseBtn.hidden = YES;
                   self.helpStautsLabel.text = @"帮助完成";
                }
                    break;
                case 5:
                {
                  self.acceptBtn.hidden = YES;
                  self.refuseBtn.hidden = YES;
                  self.helpStautsLabel.text = @"帮助失败";
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case HelpFailType:{
            switch ([cellModel.state integerValue]) {
                case 1:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"";
                }
                    break;
                case 2:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"";
                }
                    break;
                case 3:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"已拒绝";
                }
                    break;
                case 5:
                {
                    self.acceptBtn.hidden = YES;
                    self.refuseBtn.hidden = YES;
                    self.helpStautsLabel.text = @"帮助失败";
                }
                    break;
                default:
                    break;
            }
        }
            break;
            break;
        default:
            break;
    }
}

+ (CGFloat)cellHeight {
    return 50;
}

#pragma mark - 接受

- (IBAction)acceptBtnDown:(UIButton *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"您确定要%@吗?",sender.currentTitle] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([sender.currentTitle isEqualToString:@"接受"]) {
            [self acceptMessageWithState:@"2"];
        }else{
            //完成
            [self finshMessageWithState:@"4"];
        }
    }]];
    
    [self.viewController presentViewController:alert animated:YES completion:nil];
}

#pragma mark - 拒绝

- (IBAction)refuseBtnDown:(UIButton *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"您确定要%@吗?",sender.currentTitle] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([sender.currentTitle isEqualToString:@"拒绝"]) {
            [self acceptMessageWithState:@"3"];
        }else { //失败
            [self finshMessageWithState:@"5"];
        }

    }]];
    
    [self.viewController presentViewController:alert animated:YES completion:nil];
   
}

#pragma mark - 接受或不接受
- (void)acceptMessageWithState:(NSString *)state {
//    2=接受 3＝不接受
    NSDictionary *paramDic = @{Service:CircleAcceptMessageHelp,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"msgHelpId":_model.messageHelpId,
                               @"state":state,
                               @"msgId":_model.messageId,
                               };
    
    [MBProgressHUD showHUDAddedTo:self.viewController.view animated:YES];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:self.viewController.view animated:YES];
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    if (self.helpStateChangeBlock) {
                        self.helpStateChangeBlock(HelpWorkIng);
                    }
            }else{
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
        }
    }];
}


#pragma mark -成功或者失败
- (void)finshMessageWithState:(NSString *)state {
    //4=成功 5＝失败
    NSDictionary *paramDic = @{Service:CircleFinishMessageHelp,
                               @"userId":KgetUserValueByParaName(USERID),
                               @"msgHelpId":_model.messageHelpId,
                               @"state":state,
                               @"msgId":_model.messageId,
                               };
    
    [MBProgressHUD showHUDAddedTo:self.viewController.view animated:YES];
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:self.viewController.view animated:YES];
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                if ([state isEqualToString:@"4"]) {
                    if (self.helpStateChangeBlock) {
                        self.helpStateChangeBlock(HelpSuccessType);
                    }
                }else {
                    if (self.helpStateChangeBlock) {
                        if (self.helpStateChangeBlock) {
                            self.helpStateChangeBlock(HelpDefaultType);
                        }
                    }
                }
               
            }else{
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }
    }];
}

@end
