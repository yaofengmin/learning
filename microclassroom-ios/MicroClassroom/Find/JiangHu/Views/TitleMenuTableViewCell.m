//
//  TitleMenuTableViewCell.m
//  CarService
//
//  Created by apple on 15/7/29.
//  Copyright © 2015年 fenglun. All rights reserved.
//

#import "TitleMenuTableViewCell.h"


@interface TitleMenuTableViewCell ()

@property (strong, nonatomic)  UIImageView *Timage;
@property (strong, nonatomic)  UILabel *Tlabel;


@end
@implementation TitleMenuTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _Tlabel = [[UILabel   alloc]init];
        _Tlabel.textColor = [UIColor whiteColor];
        _Tlabel.font    = [UIFont systemFontOfSize:15];
        [_Tlabel sizeToFit];
        [self.contentView addSubview:_Tlabel];
        
        
    }
    
    return self;
}


-(void)configCellWithImage:(NSString *)imageName andTitel:(NSString *)title
{
    _Tlabel.text = title;
    if (imageName) {
        if (!_Timage) {
            _Timage = [[UIImageView alloc]init];
            _Timage.contentMode = UIViewContentModeScaleAspectFill;
            [self.contentView addSubview:_Timage];
        }
        [self.contentView addSubview:_Timage];
        _Timage.image = [UIImage imageNamed:imageName];
        [_Timage mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(10);
            make.centerY.equalTo(0);
            make.size.equalTo(CGSizeMake(21, 21));
        }];
        
        [_Tlabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_Timage.mas_right).offset(10);
            make.centerY.equalTo(0);
            
        }];
        
    }else
    {
        [_Timage removeFromSuperview];
        [_Tlabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.equalTo(0);
            
        }];
    }
    
    
    
}


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


+ (CGFloat) highWithTableViewCellRow{
    return 35;
}

@end

