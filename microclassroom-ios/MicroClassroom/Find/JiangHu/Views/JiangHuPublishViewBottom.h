//
//  JiangHuPublishViewBottom.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/21.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CodeListModel.h"

@interface JiangHuPublishViewBottom : UIView

@property(nonatomic ,strong) CodeListModel *model;
@property(nonatomic ,strong) UITextField *textField;
@end
