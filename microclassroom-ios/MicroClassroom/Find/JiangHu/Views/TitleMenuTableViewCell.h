//
//  TitleMenuTableViewCell.h
//  CarService
//
//  Created by apple on 15/7/29.
//  Copyright © 2015年 fenglun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleMenuTableViewCell : UITableViewCell

-(void)configCellWithImage:(NSString *)imageName andTitel:(NSString *)title;

+ (CGFloat) highWithTableViewCellRow;

@end
