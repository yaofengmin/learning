//
//  VoteBottomView.m
//  yanshan
//
//  Created by fm on 2017/8/27.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "VoteBottomView.h"

@implementation VoteBottomView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.voteBtn.layer.cornerRadius = 2.0;
    self.voteBtn.layer.masksToBounds = YES;
}
- (IBAction)voteAction:(UIButton *)sender {
	self.voteBlock(self.indexPath);
}

@end
