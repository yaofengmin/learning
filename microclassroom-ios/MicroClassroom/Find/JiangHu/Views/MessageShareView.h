//
//  MessageShareView.h
//  MicroClassroom
//
//  Created by yfm on 2017/11/28.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageShareView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *shareTitle;

@end
