//
//  AwaystationViewController.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"
#import "FindViewController.h"
@class CityModel;
@interface AwaystationViewController : YHBaseViewController
@property(nonatomic, weak) FindViewController *parentContrl;
@property(nonatomic ,strong) CityModel *city;

@end
