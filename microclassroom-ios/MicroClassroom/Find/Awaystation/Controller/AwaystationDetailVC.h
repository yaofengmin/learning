//
//  AwaystationDetailVC.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/20.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"

@interface AwaystationDetailVC : YHBaseViewController
@property(nonatomic, copy) NSString * webUrlStr;
@property(nonatomic, assign) int SiteId;
@end
