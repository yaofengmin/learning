//
//  AwaystationCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AwaystationCell.h"
#import "AwaystationModel.h"

#import "UIImageView+AFNetworking.h"
@interface AwaystationCell ()

@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;

@property (weak, nonatomic) IBOutlet UILabel *unitLabel;

@end

@implementation AwaystationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.logoImage.backgroundColor = ImageBackColor;
    // Initialization code
}

- (void)configWithModel:(AwaystationMsgModel *)cellModel {
    
    self.titleLabel.text = cellModel.title;
    self.typeLabel.text = cellModel.codeName;
//    self.costLabel.text = [NSString stringWithFormat:@"%@ %@",cellModel.price, cellModel.unit];
//    self.costLabel.text = [NSString stringWithFormat:@"%@",cellModel.price];
//    self.unitLabel.text = cellModel.unit;
    self.costLabel.hidden = YES;
    self.unitLabel.hidden = YES;
    [self.logoImage setImageWithURL:[NSURL URLWithString:cellModel.pic]];
}

@end
