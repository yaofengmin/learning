//
//  AwaystationViewController.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AwaystationViewController.h"
#import "AwaystationDetailVC.h"
#import "CityModel.h"
#import "AwaystationCell.h"
#import "SDCycleScrollView.h"
#import <MJRefresh.h>
#import "AwaystationModel.h"
#import "MicroClassBannerSegmentModel.h"
#import "YHBaseWebViewController.h"

#import "CityChooseViewController.h"
#import "YHNavgationController.h"
#import "GpsManager.h"
#import "CityTool.h"
#import "AppDelegate.h"

#import "NewPagedFlowView.h"
#import "PGCustomBannerView.h"

static NSString * const kAwaystationCell = @"AwaystationCell";
static CGFloat const kCellMargin = 5.0;
static CGFloat const kCellColumns = 5.0;
static int  pageCount = 1;
@interface AwaystationViewController ()

<
SDCycleScrollViewDelegate,
UISearchBarDelegate,
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,CityChooseViewDelegate
//NewPagedFlowViewDelegate, NewPagedFlowViewDataSource
>

{
    SDCycleScrollView * _bannerView;
    //    NewPagedFlowView  *_bannerView;
    AwaystationHomeMsgModel * _homeModel;
    AwaystationBannerModel * _bannerModel;
    NSInteger   _total;
}

@property (weak, nonatomic) IBOutlet UIScrollView *backScrolview;
@property (strong, nonatomic) NSMutableArray<AwaystationMsgModel *> * resultModel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerContentHeight;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *bannerContainerView;
@property (weak, nonatomic) IBOutlet UIButton *cityBtn;


@property (nonatomic , strong) CityModel      *cityModel;
@property (nonatomic , strong) NSArray        *cityModelArr;
@property (nonatomic ,strong) NSArray *showArr;

@end

@implementation AwaystationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.resultModel = @[].mutableCopy;
    [self.collectionView registerNib:[UINib nibWithNibName:kAwaystationCell bundle:nil]
          forCellWithReuseIdentifier:kAwaystationCell];
    [self layoutSubviews];
    [self pullBanner];
    self.backScrolview.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headRefresh)];
    self.backScrolview.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footRefresh)];
    UIImage* searchBarBg = [YHJHelp GetImageWithColor:[UIColor clearColor] andHeight:44.0];
    //设置背景图片
    [_searchBar setBackgroundImage:searchBarBg];
    //设置背景色
    [_searchBar setBackgroundColor:[UIColor clearColor]];
    //设置文本框背景
    [self setSearchTextFieldBackgroundColor:NCColor];
    
    UITextField * searchField = [self sa_GetSearchTextFiled];
    searchField.font = Font(12);
    searchField.textColor = JColor;
    
    
    [self initModel];
    [self cheakCityData];
    
}

- (UITextField *)sa_GetSearchTextFiled{
    if ([[[UIDevice currentDevice]systemVersion] floatValue] >= 13.0) {
        return self.searchBar.searchTextField;
    }else{
//        UITextField *searchTextField =  [self valueForKey:@"_searchField"];
//        return searchTextField;
        return self.searchBar.subviews.firstObject.subviews.lastObject;
    }
}

- (void)setSearchTextFieldBackgroundColor:(UIColor *)backgroundColor
{
    UIView *searchTextField = nil;
    if (IOS7) {
        searchTextField = [[[self.searchBar.subviews firstObject] subviews] lastObject];
    }
    searchTextField.backgroundColor = backgroundColor;
}
- (void)headRefresh {
    pageCount = 1;
    self.resultModel = @[].mutableCopy;
    [self pullMessageWihtCityName:_city];
}


-(void)footRefresh {
    pageCount ++;
    [self pullMessageWihtCityName:_city];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setResultModel:(NSMutableArray *)resultModel {
    
    _resultModel = resultModel;
//    [self caculateCollectionViewHeight];
    
}

#pragma mark - tool
- (void)caculateCollectionViewHeight {
    
    if (_resultModel.count <= 0) {
        self.collectionViewHeight.constant = 0;
        return;
    }
    
    CGFloat proportion = ((KScreenWidth - kCellMargin * (kCellColumns + 1)) / 2.0) *21 / 35 + 80.0f;
    int rowCount = (int)(_resultModel.count / 2 + _resultModel.count % 2);
    self.collectionViewHeight.constant = rowCount * (proportion + kCellMargin) + kCellMargin;
    
}

- (void)pullBanner {
    
    NSDictionary *paramDic = @{@"service": GetSiteIndex};
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
                _bannerModel = [AwaystationBannerModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                
                [self sortBanner:_bannerModel];
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}

- (void)setCity:(CityModel *)city {
    _city = city;
    [self.resultModel removeAllObjects];
    [self pullMessageWihtCityName:city];
    
}

- (void)pullMessageWihtCityName:(CityModel *)city {
    
    NSDictionary *paramDic = @{@"service": GetSiteMessages,
                               @"pageIndex": @(pageCount),
                               @"pageSize": @20,
                               @"cityId":city.cityId};
    
    SHOWHUD;
    @weakify(self);
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        @strongify(self);
        //业务逻辑层
        if (pageCount == 1) {
            [self.backScrolview.mj_header endRefreshing];
        }else {
            [self.backScrolview.mj_footer endRefreshing];
        }
        
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                _homeModel = [AwaystationHomeMsgModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                self.resultModel =  [self.resultModel arrayByAddingObjectsFromArray:_homeModel.list].mutableCopy;
                [self caculateCollectionViewHeight];
                _total = _homeModel.total;
                [self.collectionView reloadData];
                if (self.resultModel.count == _total) {
                    self.backScrolview.mj_footer.hidden = YES;
                }else {
                    self.backScrolview.mj_footer.hidden = YES;
                }
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];

            }
            
        }
    }];
    
}

- (void)sortBanner:(AwaystationBannerModel *)model {
    
    //    [self layoutSubviews];
    //    NSMutableArray * arr = [NSMutableArray arrayWithCapacity:1];
    //
    //    for (BannerModel * item in model.display_list) {
    //        [arr addObject:item.pic];
    //    }
    //    self.showArr = [NSArray arrayWithArray:arr];
    //    if (arr.count) {
    //        _bannerView.orginPageCount = arr.count;
    //        CGFloat imageH = (KScreenWidth - 40) * 30 / 67;
    //        if (arr.count == 1) {
    //            _bannerView.frame = CGRectMake(0, 10, KScreenWidth, imageH);
    //        }else{
    //            _bannerView.frame = CGRectMake(-20, 10, KScreenWidth + 20, imageH);
    //        }
    //    }else{
    //        _bannerView.frame = CGRectZero;
    //    }
    //
    //    self.bannerContentHeight.constant = _bannerView.frame.size.height;
    //    _bannerView.delegate = self;
    //    _bannerView.dataSource = self;
    //    [_bannerView reloadData];
    //
    //    [self.bannerContainerView addSubview:_bannerView];
    //    [_bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.edges.mas_equalTo(UIEdgeInsetsZero);
    //    }];
    //    _bannerView.imageURLStringsGroup = arr;
    NSMutableArray * arr = [NSMutableArray arrayWithCapacity:1];
    
    for (BannerModel * item in model.display_list) {
        [arr addObject:item.pic];
    }
    
    _bannerView.imageURLStringsGroup = arr;
    
}

- (void)updateFilteredContent:(NSString *)searchString {
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF.title CONTAINS[c] %@", searchString];
    
    self.resultModel = [NSMutableArray arrayWithArray:[self.resultModel filteredArrayUsingPredicate:predicate]];
    [self.collectionView reloadData];
    
}
#pragma mark - Layout
- (void)layoutSubviews {
    
    _bannerView = ({
        
        SDCycleScrollView * view = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:nil];
        
        [self.bannerContainerView addSubview:view];
        
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.edges.equalTo(self.bannerContainerView);
            
        }];
        
        view;
        
    });
    //    if (_bannerView == nil) {
    //        _bannerView = [[NewPagedFlowView alloc] initWithFrame:CGRectZero];
    //        _bannerView.backgroundColor = BColor;
    //        _bannerView.minimumPageAlpha = 0.4;
    //
    //        _bannerView.leftRightMargin = 20;
    //        _bannerView.topBottomMargin = 0;
    //
    //        _bannerView.isOpenAutoScroll = YES;
    //    }
    
    
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
    
    BannerModel *model = _bannerModel.display_list[index];
    if ([model.itemType isEqualToString:@"1"]) {
        if ([[UserInfoModel getInfoModel].grade integerValue] < 2) {// 连城小站详情 需要 grade >=  2
            @weakify(self);
            [YHJHelp aletWithTitle:@"提示" Message:@"VIP会员等级不足，不能查看" sureTitle:@"立即升级" CancelTitle:@"忽略 " SureBlock:^{
                @strongify(self);
                BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                [self.parentContrl.navigationController pushViewController:buy animated:YES];
            } andCancelBlock:nil andDelegate:self.parentContrl];
            return;
        }
        AwaystationDetailVC * targetVC = [[AwaystationDetailVC alloc] init];
        targetVC.webUrlStr = model.site_detail_url;
        targetVC.SiteId = model.outLinkOrId.intValue;
        [self.parentContrl.navigationController pushViewController:targetVC animated:YES];
    }else{
        if (NOEmptyStr(model.outLinkOrId)) {//各类微课堂详情、共享文件 需要 grade >0
            if ([[UserInfoModel getInfoModel].grade integerValue] == 0) {
                @weakify(self);
                [YHJHelp aletWithTitle:@"提示" Message:@"您还不是VIP会员，不能查看" sureTitle:@"立即开通 " CancelTitle:@"忽略 " SureBlock:^{
                    @strongify(self);
                    BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                    [self.parentContrl.navigationController pushViewController:buy animated:YES];
                    
                } andCancelBlock:nil andDelegate:self.parentContrl];
                return;
            }
            YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
            [self.parentContrl.navigationController pushViewController:web animated:YES];
        }
    }
    
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    searchBar.showsCancelButton = NO;
    [self updateFilteredContent:searchBar.text];
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = YES;
    for(id cc in [searchBar subviews])
    {
        for (UIView *view in [cc subviews]) {
            if ([NSStringFromClass(view.class) isEqualToString:@"UINavigationButton"])
            {
                UIButton *btn = (UIButton *)view;
                [btn setTitle:@"取消" forState:UIControlStateNormal];
            }
        }
    }
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    
    self.resultModel = _homeModel.list.mutableCopy;
    [self.collectionView reloadData];
    
}

#pragma mark UICollectionView DataSource Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _resultModel.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AwaystationCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAwaystationCell
                                                                       forIndexPath:indexPath];
    [cell configWithModel:_resultModel[indexPath.row]];
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeZero;
}

//UICollectionView被选中时调用的方法
-(void)collectionView:( UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if ([[UserInfoModel getInfoModel].grade integerValue] <= 1) {
        @weakify(self);
        [YHJHelp aletWithTitle:@"提示" Message:@"VIP会员等级不足，不能查看" sureTitle:@"立即升级" CancelTitle:@"忽略" SureBlock:^{
            @strongify(self);
            BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
            [self.parentContrl.navigationController pushViewController:buy animated:YES];
            
        } andCancelBlock:nil andDelegate:self.parentContrl];
        return;
    }
    AwaystationDetailVC * targetVC = [[AwaystationDetailVC alloc] init];
    targetVC.webUrlStr = _resultModel[indexPath.row].site_detail_url;
    targetVC.SiteId = _resultModel[indexPath.row].SiteId;
    [self.parentContrl.navigationController pushViewController:targetVC animated:YES];
    
}

//返回这个UICollectionViewCell是否可以被选择
-(BOOL)collectionView:( UICollectionView *)collectionView shouldSelectItemAtIndexPath:( NSIndexPath *)indexPath
{
    return YES ;
}

#pragma mark --UICollectionViewDelegateFlowLayout
- ( CGSize )collectionView:( UICollectionView *)collectionView layout:( UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:( NSIndexPath *)indexPath
{
    CGFloat proportion = (KScreenWidth - kCellMargin * (kCellColumns + 1)) / 2;
    return CGSizeMake(proportion, (proportion *21 / 35) + 65.0f);
}


#pragma mark ==== 城市
//初始化城市信息
-(void)initModel {
    _cityModel = [[CityModel alloc]init];
    NSString *areaid = KgetUserValueByParaName(NCUserLastAreaId);
    
    if (!areaid) {
        KsetUserValueByParaName(@"330100", NCUserLastAreaId);
        KsetUserValueByParaName(@"杭州市", NCUserLastAreaName);
        _cityModel.cityId = @"330100";
        _cityModel.cityName = @"杭州市";
        
    }else{
        _cityModel.cityId = KgetUserValueByParaName(NCUserLastAreaId);
        _cityModel.cityName = KgetUserValueByParaName(NCUserLastAreaName);
        
    }
    [self.cityBtn setTitle:[NSString stringWithFormat:@"更换城市 : %@",_cityModel.cityName] forState:UIControlStateNormal];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"更换城市 : %@",_cityModel.cityName]];
    [attributedText addAttributes:@{NSForegroundColorAttributeName : JColor} range:NSMakeRange(0, 6)];
    [self.cityBtn setAttributedTitle:attributedText forState:UIControlStateNormal];
    self.city = _cityModel;
}

#pragma mark == 获取开通城市 获取相关信息 ===
-(void)cheakCityData {
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{@"service":@"App.getCityList"} andBlocks:^(NSDictionary *result) {
        if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
            
            self.cityModelArr=[CityModel mj_objectArrayWithKeyValuesArray:result[@"list"]];
        }else{
            [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
        }
        
    }];
}

#pragma mark -CityChooseViewDelegate
-(void)chooseCityInfo:(CityModel *)city {
    KsetUserValueByParaName(city.cityId,NCUserLastAreaId );
    KsetUserValueByParaName(city.cityName,NCUserLastAreaName);
    [self.cityBtn setTitle:city.cityName forState:UIControlStateNormal];
    self.city = city;
    [self.cityBtn setTitle:[NSString stringWithFormat:@"更换城市 : %@",city.cityName] forState:UIControlStateNormal];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"更换城市 : %@",city.cityName]];
    [attributedText addAttributes:@{NSForegroundColorAttributeName : JColor} range:NSMakeRange(0, 6)];
    [self.cityBtn setAttributedTitle:attributedText forState:UIControlStateNormal];
    
    //      NSString  *controllName = [NSString stringWithFormat:@"链城小站·%@",city.cityName];
    //    self.titles      = @[@"江湖",controllName];
    //    [self reloadData];
    //    [self selectedIndex:1];
}

-(void)setCityModelArr:(NSArray *)cityModelArr {
    _cityModelArr = cityModelArr;
    
    @weakify(self);
    [GpsManager getLoctationSuccuss:^(CGFloat lng, CGFloat lat, NSDictionary *addressDic) {
        @strongify(self);
        NSString *city = [addressDic objectForKey:@"City"];
        _cityModel = [CityTool modelWithName:city andSource:_cityModelArr];
        if (_cityModel) {
            if (![_cityModel.cityId isEqualToString:KgetUserValueByParaName(NCUserLastAreaId)]) {
                NSString *msg = [NSString stringWithFormat:@"当前定位城市是 %@ 是否要切换",_cityModel.cityName];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@""] message:msg delegate:self cancelButtonTitle:@"取消" otherButtonTitles: @"切换",nil];
                [alertView show];
            }else {
                [self.cityBtn setTitle:_cityModel.cityName forState:UIControlStateNormal];
            }
            
        }else {
            [WFHudView showMsg:@"所在地区暂未开通服务" inView:nil];
        }
        
    } andFaild:^{
        [WFHudView showMsg:@"获取位置信息失败" inView:nil];
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex==0) {
        self.city = _cityModel;
        [self chooseCityInfo:_cityModel];
    }
}
- (IBAction)chooseCityAction:(UIButton *)sender {
    CityChooseViewController *city = [[CityChooseViewController alloc]initWithCityData:self.cityModelArr];
    city.delegate = self;
    YHNavgationController *cityNav = [[YHNavgationController alloc]initWithRootViewController:city];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate.window.rootViewController presentViewController:cityNav animated:YES completion:nil];
}
- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
    BannerModel *model = _bannerModel.display_list[subIndex];
    if ([model.itemType isEqualToString:@"1"]) {
        AwaystationDetailVC * targetVC = [[AwaystationDetailVC alloc] init];
        targetVC.webUrlStr = model.site_detail_url;
        targetVC.SiteId = model.outLinkOrId.intValue;
        [self.parentContrl.navigationController pushViewController:targetVC animated:YES];
    }else{
        if (NOEmptyStr(model.outLinkOrId)) {
            YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
            [self.parentContrl.navigationController pushViewController:web animated:YES];
        }
    }
}

//- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
//
////    NSLog(@"CustomViewController 滚动到了第%ld页",pageNumber);
//}

//- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
//    CGFloat imageH = 0;
//    if (self.showArr.count > 0) {
//        imageH = (KScreenWidth - 40) * 30 / 67;
//    }
//    CGFloat imageW = 0;
//    if (self.showArr.count > 1) {
//        imageW = KScreenWidth - 40;
//    }else{
//        imageW = KScreenWidth - 20;
//    }
//    return CGSizeMake(imageW, imageH);
//
//}

//#pragma mark --NewPagedFlowView Datasource
//- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
//
//    return self.showArr.count;
//}

//- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
//
//    PGCustomBannerView *bannerView = (PGCustomBannerView *)[flowView dequeueReusableCell];
//    bannerView.backgroundColor = [UIColor whiteColor];
//    if (!bannerView) {
//        bannerView = [[PGCustomBannerView alloc] init];
//    }
//
//    //在这里下载网络图片
//    [bannerView.mainImageView sd_setImageWithURL:[NSURL URLWithString:self.showArr[index]] placeholderImage:[UIImage imageNamed:@""]];
//    return bannerView;
//}

@end
