//
//  AwaystationCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseTableViewCell.h"

@class AwaystationMsgModel;
@interface AwaystationCell : UICollectionViewCell

- (void)configWithModel:(AwaystationMsgModel *)cellModel;
@end
