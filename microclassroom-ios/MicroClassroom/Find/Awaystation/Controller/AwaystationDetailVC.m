//
//  AwaystationDetailVC.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/20.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AwaystationDetailVC.h"
#import "MyOrderVC.h"
#import "MineWalletViewController.h"
#import "AwaystationBookView.h"
#import "AwaystationModel.h"
#import "FriendModel.h"
#import "ChatViewController.h"
#import "BuyVIPViewController.h"
static const int kBookViewHeight = 234;
static const int kBottomViewHeight = 65;
static NSString * const kBookTitle = @"立即预订";
static NSString * const kPayTitle = @"立即预订";
@interface AwaystationDetailVC ()
<
UIWebViewDelegate
>

{
    AwaystationMsgInfoModel * _infoModel;
}
@property (strong, nonatomic) AwaystationBookView *bookView;
@property (strong, nonatomic) UITapGestureRecognizer *tap;
@property (strong, nonatomic) UIView *mask;
@property (weak, nonatomic) IBOutlet UIButton *bookBtn;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;

@property (weak, nonatomic) IBOutlet UIView *leftBgView;
@property (weak, nonatomic) IBOutlet UIButton *commentAction;
@property (weak, nonatomic) IBOutlet UIView *allBgView;
@property (weak, nonatomic) IBOutlet UIView *bettLine;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftViewWidth;

@end

@implementation AwaystationDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configNav];
    [self loadWebView];
    self.allBgView.hidden = YES;
    [self pullDetailData];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.leftBgView.layer.cornerRadius = 4.0;
    self.leftBgView.layer.masksToBounds = YES;
    
    self.bookBtn.layer.cornerRadius = 4.0;
    self.bookBtn.layer.masksToBounds = YES;
}

- (void)configNav {
    self.yh_navigationBar.hidden = YES;
    //    self.yh_navigationItem.title = @"小站详情";
    
}
- (UIView *)mask {
    
    if (!_mask) {
        _mask = [UIView new];
        //        _mask.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5];
        _mask.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.4];
        [_mask addGestureRecognizer:self.tap];
        [self.view addSubview:_mask];
        [_mask mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.view);
            make.trailing.equalTo(self.view);
            make.top.equalTo(self.view).offset(0);
            make.bottom.equalTo(self.view).offset(-kBottomViewHeight);
        }];
    }
    
    return _mask;
    
}

- (UITapGestureRecognizer *)tap {
    
    if (!_tap) {
        _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnWebView:)];
        _tap.numberOfTapsRequired = 1;
        _tap.numberOfTouchesRequired = 1;
    }
    
    return _tap;
    
}

- (AwaystationBookView *)bookView {
    
    if (!_bookView) {
        _bookView = [[[NSBundle mainBundle] loadNibNamed:@"AwaystationBookView" owner:self options: nil] objectAtIndex:0];
        
        [self.view addSubview:_bookView];
        
        [_bookView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.bottom.equalTo(self.view).offset(kBookViewHeight);
            make.right.equalTo(self.view);
            make.height.equalTo(kBookViewHeight);
        }];
    }
    
    return _bookView;
    
}

- (void)changeToBookState {
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        [self.bookView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(kBookViewHeight);
        }];
        [self.view layoutIfNeeded];
    }];
    [self.view sendSubviewToBack:self.mask];
    [self.bookBtn setTitle:kBookTitle forState:UIControlStateNormal];
}

- (void)changeToPayState {
    
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        [self.view bringSubviewToFront:self.mask];
        [self.view bringSubviewToFront:self.bookView];
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:0.25 animations:^{
            [self.bookView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(-kBottomViewHeight);
            }];
            [self.view layoutIfNeeded];
        }];
        
        [self.bookBtn setTitle:kPayTitle forState:UIControlStateNormal];
        //        [self pay];
    }
    
}

- (void)tapOnWebView:(UITapGestureRecognizer *)sender {
    
    if (CGRectContainsRect(self.view.frame, self.bookView.frame)) {
        [self changeToBookState];
    }
    
}

- (void)loadWebView {
    
    NSURL * webUrl = [NSURL URLWithString:self.webUrlStr];
    
    NSURLRequest * request = [NSURLRequest requestWithURL:webUrl];
    
    [self.webView loadRequest:request];
    
}

- (void)pullDetailData {
    
    NSDictionary *paramDic = @{@"service": GetSiteMessageInfo,
                               @"siteId": @(self.SiteId)};
    
    //    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        //        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                _infoModel = [AwaystationMsgInfoModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                //                self.bookView.priceLabel.text = [NSString stringWithFormat:@"%@ %@", _infoModel.price,_infoModel.unit];
                self.bookView.price = [_infoModel.price doubleValue];
                self.bookView.unit = _infoModel.unit;
                if (NOEmptyStr(_infoModel.userIdIM)) {
                    self.chatBtn.hidden = NO;
                    self.bettLine.hidden = NO;
                    self.leftViewWidth.constant = 140;
                }else{
                    self.chatBtn.hidden = YES;
                    self.bettLine.hidden = YES;
                    self.leftViewWidth.constant = 70;
                }
                self.allBgView.hidden = NO;
                //                if (NOEmptyStr(_infoModel.userIdIM)) {
                //                    self.yh_navigationItem.rightBarButtonItem = [[YHBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"chat"] style:0 handler:^(id send) {
                //                        FriendModel *friendModel = [[FriendModel alloc]init];
                //                        friendModel.nickname = @"客服";
                //
                //                        ChatViewController *chat  = [[ChatViewController alloc]initWithConversationChatter:_infoModel.userIdIM conversationType:EMConversationTypeChat andData:friendModel];
                //                        [self.navigationController pushViewController:chat animated:YES];
                //
                //                    }];
                //                }
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}

- (void)pay {
    
    NSDictionary * productsInfo = @{@"1":@{@"userNote":self.bookView.remarkTxt.text == nil ? @"" :self.bookView.remarkTxt.text,
                                           @"items":@[@{@"outItemId" : @(_infoModel.SiteId),
                                            @"buyAmount" : @(0),
                                            @"outItemType" : @"1",
                                        @"fromTime" :self.bookView.startDateTxt.text,
                                            @"toTime" : self.bookView.endDateTxt.text}]}
                                    //                                                        @"fromTime" : self.bookView.startDateTxt.text,
                                    //                                                        @"buyAmount" : @(self.bookView.count),
                                    //                                                        @"toTime" : self.bookView.endDateTxt.text}]}
                                    };
    
    NSDictionary *paramDic = @{@"service": PayForProducts,
                               @"userId": KgetUserValueByParaName(USERID),
                               @"productsInfo": productsInfo.mj_JSONString};
    NSLog(@"%@",paramDic);
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                if (CGRectContainsRect(self.view.frame, self.bookView.frame)) {
                    [self changeToBookState];
                }
                [WFHudView showMsg:@"预约成功" inView:nil];
                //                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                //                [[UIAlertView bk_showAlertViewWithTitle:@"购买成功!" message:@"前去查看我的订单" cancelButtonTitle:@"取消" otherButtonTitles:@[@"确定"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                //                    switch (buttonIndex) {
                //                        case 1:
                //                        {
                //                            NSLog(@"我的订单");
                //                            MyOrderVC *targetVC = [[MyOrderVC alloc] init];
                //                            [self.navigationController pushViewController:targetVC animated:YES];
                //                            break;
                //                        }
                //                        default:
                //                            break;
                //                    }
                //                }] show];
                
            }else if([result[REQUEST_CODE] isEqualToNumber:@2]){
                @weakify(self);
                [YHJHelp aletWithTitle:@"提示" Message:@"帐号学币不足，是否前往充值" sureTitle:@"充值" CancelTitle:@"取消" SureBlock:^{
                    @strongify(self);
                    MineWalletViewController *contr = [[MineWalletViewController alloc]init];
                    [self.navigationController pushViewController:contr animated:YES];
                } andCancelBlock:nil andDelegate:self];
                
            }else if ([result[REQUEST_CODE] isEqualToNumber:@30]) {
                
                @weakify(self);
                [YHJHelp aletWithTitle:@"提示" Message:result[REQUEST_MESSAGE] sureTitle:@"立即升级" CancelTitle:@"忽略" SureBlock:^{
                    @strongify(self);
                    BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
                    [self.navigationController pushViewController:buy animated:YES];
                    
                } andCancelBlock:nil andDelegate:self];
                
            }else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                
            }
            
        }
    }];
    
}

- (IBAction)book:(UIButton *)sender {
    //
        if (CGRectContainsRect(self.view.frame, self.bookView.frame)) {
            NSLog(@"%s 下单支付", __func__);
    
            [self pay];
    
        } else {
            [self changeToPayState];
    
        }
}


# pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    SHOWHUD;
}
- (void)webViewDidFinishLoad:(UIWebView *)webVie {
    HIDENHUD;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    HIDENHUD;
}

- (IBAction)callAction:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_infoModel.userPhone]]];
}
- (IBAction)commentAction:(UIButton *)sender {
    if (NOEmptyStr(_infoModel.userIdIM)) {
        FriendModel *friendModel = [[FriendModel alloc]init];
        friendModel.nickname = @"客服";
        ChatViewController *chat  = [[ChatViewController alloc]initWithConversationChatter:_infoModel.userIdIM conversationType:EMConversationTypeChat andData:friendModel];
        [self.navigationController pushViewController:chat animated:YES];
    }
}
- (IBAction)backBtnAction:(UIButton *)sender {
    [self backBtnAction];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}



@end
