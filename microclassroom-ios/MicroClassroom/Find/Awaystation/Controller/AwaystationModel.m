//
//  AwaystationModel.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AwaystationModel.h"

@implementation AwaystationMsgModel

@end

@implementation AwaystationHomeMsgModel

+ (NSDictionary *)objectClassInArray{
    return @{
             @"list" : @"AwaystationMsgModel"
             };
}

@end

@implementation AwaystationMsgInfoModel

@end

@implementation AwaystationBannerModel

+ (NSDictionary *)objectClassInArray{
    return @{
             @"display_list" : @"BannerModel"
             };
}

@end
