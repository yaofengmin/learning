//
//  AwaystationModel.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/13.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"
#import "MicroClassBannerSegmentModel.h"
@interface AwaystationMsgModel : YHBaseModel
@property (nonatomic, assign) int SiteId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *codeName;
@property (nonatomic, copy) NSString *site_detail_url;
@end

@interface AwaystationMsgInfoModel : YHBaseModel
@property (nonatomic, assign) int SiteId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *pics;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *sheshi;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *codeName;
@property (nonatomic, copy) NSArray *labels;
@property (nonatomic, copy) NSString *userIdIM;
@property (nonatomic, copy) NSString *photo;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *userPhone;

@end

@interface AwaystationHomeMsgModel : YHBaseModel
@property (nonatomic, assign) int total;
@property (nonatomic, copy) NSArray<AwaystationMsgModel *> *list;
@end

@interface AwaystationBannerModel : YHBaseModel
@property (nonatomic, copy) NSArray<BannerModel *> *display_list;
@end
