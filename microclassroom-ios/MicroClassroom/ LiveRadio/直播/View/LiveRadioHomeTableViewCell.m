//
//  LiveRadioHomeTableViewCell.m
//  yanshan
//
//  Created by fm on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "LiveRadioHomeTableViewCell.h"

#import "ZhiBoModel.h"

@implementation LiveRadioHomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.newsImage.backgroundColor = ImageBackColor;
    self.headImage.backgroundColor = ImageBackColor;
    
    self.newsImage.layer.cornerRadius = 4.0;
    self.newsImage.layer.masksToBounds = YES;
    
    self.headImage.layer.cornerRadius = 15;
    self.headImage.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)configureCellWithZhiBoModel:(ZhiBoModel *)model {//status 2=预约中;1=可直播;3=已结束 4=直播中
	[self setImg:model.videoLogo];
	self.newsTitle.text = model.videoTitle;
	
	self.author.text = model.author;
    [self.headImage sd_setImageWithURL:[NSURL URLWithString:model.authorLogo] placeholderImage:[UIImage imageNamed:@"moren_head"]];
    if (model.status == MCZhiBoStatusBooking){
		self.statusLabel.text = @"预约中";
        self.statusLabel.textColor = KColorFromRGB(0xf0cf2e);
    }else if(model.status == MCZhiBoStatusLiving){
        self.statusLabel.text = @"正在直播";
        self.statusLabel.textColor = NEColor;
    }else{
        self.statusLabel.text = model.startTime;
        self.statusLabel.textColor = NEColor;
    }
	
	
}
- (void)setImg:(NSString *)urlStr {
	
	[self.newsImage sd_setImageWithURL:[NSURL URLWithString:urlStr]];
	
	
}

@end
