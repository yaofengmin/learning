//
//  LiveRadioHomeTableViewCell.h
//  yanshan
//
//  Created by fm on 2017/8/16.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZhiBoModel;


@interface LiveRadioHomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *newsTitle;
@property (weak, nonatomic) IBOutlet UIImageView *newsImage;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *author;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *teacherLabel;

- (void)configureCellWithZhiBoModel:(ZhiBoModel *)model;

@end
