//
//  LiveRadioViewController.m
//  yanshan
//
//  Created by BPO on 2017/8/14.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "LiveRadioHomeViewController.h"

#import "LiveRadioDetailViewController.h"
#import "MicroClassDetailViewController.h"
#import "BooksDetailViewController.h"
#import "YHBaseWebViewController.h"

#import <SDCycleScrollView.h>
#import "LiveRadioHomeTableViewCell.h"

#import "ZhiBoModel.h"

//轮播
#import "NewPagedFlowView.h"
#import "PGCustomBannerView.h"

static NSString *liveRadioCellID = @"LiveRadioHomeCellID";
@interface LiveRadioHomeViewController ()<SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource
//,NewPagedFlowViewDelegate, NewPagedFlowViewDataSource
>
{
    SDCycleScrollView  *_bannerView;
    int _totalCount;
    NSInteger pageCount;
}

@property (strong, nonatomic) NSMutableArray<ZhiBoModel *> *resultModel;
@property (strong, nonatomic) NSMutableArray<ZhiBoModel *> *originModel;
@property (strong, nonatomic) ZhiBoListModel *mainModel;
@property (strong, nonatomic)  NSArray<BannerModel *> *bannerArr;

@property (nonatomic,strong) UITableView *tableV;

@property (nonatomic ,strong) UIView *headerV;

@property (nonatomic ,strong) NSArray *showArr;
@end

@implementation LiveRadioHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = NCColor;
    self.yh_navigationItem.title = @"直播";
    self.yh_navigationBar.hidden = YES;
    [self layoutSubviews];
    [self setUpTableView];
    [self initIvar];
    [self pullData];
    KAddobserverNotifiCation(@selector(headerRefresh), NOTIGICATIONFINDVVIEWRELOAD);
    
}
- (void)initIvar {
    pageCount = 1;
    _totalCount = 0;
    
    _resultModel = [NSMutableArray arrayWithCapacity:1];
    _originModel = [NSMutableArray arrayWithCapacity:1];
}
- (void)pullData {
    
    NSDictionary *paramDic = @{@"service": AppGetPiliList,
                               @"pageSize": @(20),
                               @"pageIndex": @(pageCount),
                               @"userId":KgetUserValueByParaName(USERID) ==nil ? @"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (pageCount == 1) {
            [self.tableV.mj_header endRefreshing];
            [_originModel removeAllObjects];
            [self.resultModel removeAllObjects];
        }else {
            [self.tableV.mj_footer endRefreshing];
        }
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                if (pageCount == 1) {
                    _totalCount = [result[REQUEST_LIST][@"total"] intValue];
                    self.mainModel = [ZhiBoListModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                }
                
                for (id item in result[REQUEST_LIST][REQUEST_LIST]) {
                    [_originModel addObject:[ZhiBoModel mj_objectWithKeyValues:item]];
                }
                
                [self.tableV.mj_footer endRefreshing];
                
                self.tableV.mj_footer.hidden = _originModel.count == _totalCount;
                //                [self.tableHeader setBanner:self.mainModel.display_list];
                if (self.bannerArr.count == 0) {
                    self.bannerArr = [BannerModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST][@"display_list"]];
                    [self sortBanner:self.bannerArr];
                }
                if (_bannerView.superview) {
                    
                }
                _resultModel = _originModel.mutableCopy;
                
                [self.tableV reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                [self.resultModel removeAllObjects];
                [self.tableV reloadData];
                self.tableV.mj_footer.hidden = YES;
            }
            
        }
    }];
    
}
- (void)sortBanner:(NSArray<BannerModel *>*)arr {
    if (arr.count) {
        NSMutableArray *showArr = @[].mutableCopy;
        for (BannerModel * item in arr) {
            [showArr addObject:item.pic];
        }
        _bannerView.imageURLStringsGroup = showArr;
        
        CGFloat imageH = KScreenWidth/320 * arr[0].photoHeight.floatValue;
        [_bannerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(imageH);
        }];
        
    }else{
        [_bannerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
        
    }
    //        _bannerView.orginPageCount = showArr.count;
    //        self.showArr = [NSArray arrayWithArray:showArr];
    //        CGFloat imageH = (KScreenWidth - 40) * 30 / 67;
    //        if (arr.count == 1) {
    //            _bannerView.frame = CGRectMake(0, 10, KScreenWidth, imageH);
    //            _headerV.frame = CGRectMake(0, KStatusBarHeight, KScreenWidth, imageH + 20);
    //        }else{
    //            _bannerView.frame = CGRectMake(-20,  10, KScreenWidth + 20, imageH);
    //            _headerV.frame = CGRectMake(0, KStatusBarHeight, KScreenWidth + 20, imageH + 20);
    //        }
    //    }else{
    //        _bannerView.frame = CGRectZero;
    //        _headerV.frame = CGRectZero;
    //    }
    //    _bannerView.delegate = self;
    //    _bannerView.dataSource = self;
    //    [_bannerView reloadData];
}

#pragma mark - Layout
- (void)layoutSubviews {
    _bannerView = ({
        SDCycleScrollView * view = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage imageNamed:@"banner"]];
        view;
    });
    CGFloat imageH = KScreenWidth/375 * 178;
    _bannerView.currentPageDotImage = [UIImage imageNamed:@"pageContr_Selected"];
    _bannerView.pageDotImage = [UIImage imageNamed:@"pageContr_Normal"];
    _bannerView.delegate = self;
    
    [self.view addSubview:_bannerView];
    [_bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(imageH);
    }];
    
    //    CGFloat imageH = (KScreenWidth - 40) * 30 / 67;
    //    self.headerV = [[UIView alloc]initWithFrame:CGRectMake(0, KStatusBarHeight, KScreenWidth, imageH + 20)];
    //    self.headerV.backgroundColor = NCColor;
    //    if (_bannerView == nil) {
    //        _bannerView = [[NewPagedFlowView alloc] initWithFrame:CGRectZero];
    //        _bannerView.backgroundColor = [UIColor clearColor];
    //        _bannerView.minimumPageAlpha = 0.4;
    //
    //        _bannerView.leftRightMargin = 20;
    //        _bannerView.topBottomMargin = 0;
    //
    //        _bannerView.isOpenAutoScroll = YES;
    //
    //    }
    //
    //    if (self.showArr.count == 1) {
    //        _bannerView.frame = CGRectMake(0,  10, KScreenWidth, imageH);
    //    }else{
    //        _bannerView.frame = CGRectMake(-20, 10, KScreenWidth + 20, imageH);
    //    }
    //    [self.headerV addSubview:_bannerView];
    //    [self.view addSubview:self.headerV];
}
- (void)setUpTableView {
    
    self.tableV = [[UITableView alloc]init];
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableV.backgroundColor = [UIColor clearColor];
    self.tableV.estimatedRowHeight = 0;
    [self.view addSubview:self.tableV];
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.equalTo(_bannerView.mas_bottom);
    }];
    
    if ([self.tableV respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableV setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([self.tableV respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableV setLayoutMargins: UIEdgeInsetsZero];
    }
    if (@available(iOS 11.0, *)) {
        self.tableV.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableV.estimatedRowHeight = 0;
        self.tableV.estimatedSectionFooterHeight = 0;
        self.tableV.estimatedSectionHeaderHeight = 0;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.tableV.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefresh)];
    
    
    self.tableV.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    [self.tableV registerNib:[UINib nibWithNibName:@"LiveRadioHomeTableViewCell" bundle:nil] forCellReuseIdentifier:liveRadioCellID];
    
}

- (void)headerRefresh
{
    pageCount = 1;
    [self pullData];
}

- (void)loadMoreData
{
    pageCount ++ ;
    [self pullData];
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.resultModel.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LiveRadioHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:liveRadioCellID forIndexPath:indexPath];
    [cell configureCellWithZhiBoModel:self.resultModel[indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZhiBoModel *model = self.resultModel[indexPath.row];
    LiveRadioDetailViewController *liveDetailVC = [[LiveRadioDetailViewController alloc]initWithZhiBoid:model.videoId];
    [self.navigationController pushViewController:liveDetailVC animated:YES];
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    //各类微课堂详情、共享文件 需要 grade >0
    BannerModel *model = self.bannerArr[index];
    if ([model.itemType isEqualToString:@"1"]) {
        if (model.isMp3 == MicClassMp3TypeVideo || model.isMp3 == MicClassMp3TypeAudio) {//视频
            MicroClassDetailViewController * targetVC = [[MicroClassDetailViewController alloc] init];
            targetVC.videoId = model.outLinkOrId;
            [self.navigationController pushViewController:targetVC animated:YES];
            
        }else if (model.isMp3 == MicClassMp3TypeBook){
            BooksDetailViewController *detailVC = [[BooksDetailViewController alloc]init];
            detailVC.videoId = model.outLinkOrId;
            [self.navigationController pushViewController:detailVC animated:YES];
            
        }else{
            if (NOEmptyStr(model.outLinkOrId)) {
                YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
                [self.navigationController pushViewController:web animated:YES];
            }
        }
        
    }else{
        if (NOEmptyStr(model.outLinkOrId)) {
            YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
            [self.navigationController pushViewController:web animated:YES];
        }
    }
}
//- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
//
//    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
//    BannerModel *model = self.bannerArr[subIndex];
//    if ([model.itemType isEqualToString:@"1"]) {
//        if (model.isMp3 == MicClassMp3TypeVideo || model.isMp3 == MicClassMp3TypeAudio) {//视频
//            MicroClassDetailViewController * targetVC = [[MicroClassDetailViewController alloc] init];
//            targetVC.videoId = model.outLinkOrId;
//            [self.navigationController pushViewController:targetVC animated:YES];
//
//        }else if (model.isMp3 == MicClassMp3TypeBook){
//            BooksDetailViewController *detailVC = [[BooksDetailViewController alloc]init];
//            detailVC.videoId = model.outLinkOrId;
//            [self.navigationController pushViewController:detailVC animated:YES];
//
//        }else{
//            if (NOEmptyStr(model.outLinkOrId)) {
//                YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
//                [self.navigationController pushViewController:web animated:YES];
//            }
//        }
//
//    }else{
//        if (NOEmptyStr(model.outLinkOrId)) {
//            YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
//            [self.navigationController pushViewController:web animated:YES];
//        }
//    }
//}

//- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
//
////    NSLog(@"CustomViewController 滚动到了第%ld页",pageNumber);
//
//}

//- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
//    CGFloat imageH = 0;
//    if (self.showArr.count > 0) {
//        imageH = (KScreenWidth - 40) * 30 / 67;
//    }
//    CGFloat imageW = 0;
//    if (self.showArr.count > 1) {
//        imageW = KScreenWidth - 40;
//    }else{
//        imageW = KScreenWidth - 20;
//    }
//    return CGSizeMake(imageW, imageH);
//}

#pragma mark --NewPagedFlowView Datasource
//- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
//
//    return self.showArr.count;
//}
//
//- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
//
//    PGCustomBannerView *bannerView = (PGCustomBannerView *)[flowView dequeueReusableCell];
//    bannerView.backgroundColor = [UIColor whiteColor];
//    if (!bannerView) {
//        bannerView = [[PGCustomBannerView alloc] init];
//    }
//
//    //在这里下载网络图片
//    [bannerView.mainImageView sd_setImageWithURL:[NSURL URLWithString:self.showArr[index]] placeholderImage:[UIImage imageNamed:@""]];
//    return bannerView;
//}
- (void)dealloc
{
    KRemoverNotifi(NOTIGICATIONFINDVVIEWRELOAD);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

