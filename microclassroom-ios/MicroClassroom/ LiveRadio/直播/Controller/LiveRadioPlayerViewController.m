//
//  LiveRadioPlayerViewController.m
//  yanshan
//
//  Created by fm on 2017/10/10.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "LiveRadioPlayerViewController.h"

#import "NELivePlayer.h"
#import "NELivePlayerController.h"
#import "NELivePlayerControl.h"

@interface LiveRadioPlayerViewController ()
@property(nonatomic, strong) id<NELivePlayer> liveplayer;
@property(nonatomic, strong) NELivePlayerControl *mediaControl;
@property (nonatomic, strong) UIView *playerView;
@property (nonatomic, strong) UIControl *controlOverlay;

@property (nonatomic, strong) UIActivityIndicatorView *bufferingIndicate;
@property (nonatomic, strong) UILabel *bufferingReminder;
@property (nonatomic ,strong) UIButton *endBtn;

@end

@implementation LiveRadioPlayerViewController



-(void)creatEndBtn
{
    _endBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_endBtn setTitle:@"结束观看" forState:UIControlStateNormal];
    _endBtn.titleLabel.font = Font(15);
    [_endBtn addTarget:self action:@selector(endAction) forControlEvents:UIControlEventTouchUpInside];
    [_endBtn setTitleColor:CColor forState:UIControlStateNormal];
    _endBtn.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    _endBtn.layer.cornerRadius = 40 / 2.0;
    _endBtn.layer.masksToBounds = YES;
    _endBtn.layer.borderWidth = 0.5;
    _endBtn.layer.borderColor = KColorFromRGB(0x000000).CGColor;
    [self.view addSubview:_endBtn];
    [_endBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth - 24, 40));
        make.centerX.mas_equalTo(0);
    }];
}

-(void)endAction
{
    //释放占有的系统资源
    [self.liveplayer shutdown]; // 退出播放并释放相关资源
    [self.liveplayer.view removeFromSuperview];
    self.liveplayer = nil;
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerDidPreparedToPlayNotification object:_liveplayer];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerLoadStateChangedNotification object:_liveplayer];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerPlaybackFinishedNotification object:_liveplayer];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerFirstVideoDisplayedNotification object:_liveplayer];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerFirstAudioDisplayedNotification object:_liveplayer];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerVideoParseErrorNotification object:_liveplayer];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerPlaybackStateChangedNotification object:_liveplayer];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerMoviePlayerSeekCompletedNotification object:_liveplayer];
    
    if (self.presentingViewController) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
    [self backBtnAction];
    
}

- (void)loadView {
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    self.playerView        = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight-20)];
    
    self.mediaControl = [[NELivePlayerControl alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
//    [self.mediaControl addTarget:self action:@selector(onClickMediaControl:) forControlEvents:UIControlEventTouchDown];
    //控制
    self.controlOverlay = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
//    [self.controlOverlay addTarget:self action:@selector(onClickOverlay:) forControlEvents:UIControlEventTouchDown];
    
    //缓冲提示
    self.bufferingIndicate = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [self.bufferingIndicate setCenter:CGPointMake(KScreenWidth/2, KScreenHeight/2)];
    [self.bufferingIndicate setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.bufferingIndicate.hidden = YES;
    
    self.bufferingReminder = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
    [self.bufferingReminder setCenter:CGPointMake(KScreenWidth/2, KScreenHeight/2 - 50)];
    self.bufferingReminder.text = @"缓冲中";
    self.bufferingReminder.textAlignment = NSTextAlignmentCenter; //文字居中
    self.bufferingReminder.textColor = [UIColor whiteColor];
    self.bufferingReminder.hidden = YES;
    [NELivePlayerController setLogLevel:NELP_LOG_VERBOSE];
    
    self.liveplayer = [[NELivePlayerController alloc] initWithContentURL:self.url];
    if (self.liveplayer == nil) {
        NSLog(@"player initilize failed, please tay again!");
    }
    self.liveplayer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.liveplayer.view.frame = self.playerView.bounds;
    
    self.view.autoresizesSubviews = YES;
    
    [self.mediaControl addSubview:self.controlOverlay];
    [self.view addSubview:self.liveplayer.view];
    [self.view addSubview:self.mediaControl];
    [self.view addSubview:self.bufferingIndicate];
    [self.view addSubview:self.bufferingReminder];
    self.mediaControl.delegatePlayer = self.liveplayer;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor blackColor];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NELivePlayerDidPreparedToPlay:)
                                                 name:NELivePlayerDidPreparedToPlayNotification
                                               object:_liveplayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NELivePlayerPlaybackStateChanged:)
                                                 name:NELivePlayerPlaybackStateChangedNotification
                                               object:_liveplayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NeLivePlayerloadStateChanged:)
                                                 name:NELivePlayerLoadStateChangedNotification
                                               object:_liveplayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NELivePlayerPlayBackFinished:)
                                                 name:NELivePlayerPlaybackFinishedNotification
                                               object:_liveplayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NELivePlayerFirstVideoDisplayed:)
                                                 name:NELivePlayerFirstVideoDisplayedNotification
                                               object:_liveplayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NELivePlayerFirstAudioDisplayed:)
                                                 name:NELivePlayerFirstAudioDisplayedNotification
                                               object:_liveplayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NELivePlayerReleaseSuccess:)
                                                 name:NELivePlayerReleaseSueecssNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NELivePlayerVideoParseError:)
                                                 name:NELivePlayerVideoParseErrorNotification
                                               object:_liveplayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(NELivePlayerSeekComplete:)
                                                 name:NELivePlayerMoviePlayerSeekCompletedNotification
                                               object:_liveplayer];
    [self creatEndBtn];//结束按钮

    
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.liveplayer setBufferStrategy:NELPAntiJitter]; // 点播抗抖动
    [self.liveplayer setScalingMode:NELPMovieScalingModeNone]; // 设置画面显示模式，默认原始大小
    [self.liveplayer setShouldAutoplay:YES]; // 设置prepareToPlay完成后是否自动播放
    //    [self.liveplayer setHardwareDecoder:isHardware]; // 设置解码模式，是否开启硬件解码
    [self.liveplayer setPauseInBackground:NO]; // 设置切入后台时的状态，暂停还是继续播放
    [self.liveplayer setPlaybackTimeout:15 *1000]; // 设置拉流超时时间
    
#ifdef KEY_IS_KNOWN // 视频云加密的视频，自己已知密钥
    NSString *key = @"HelloWorld";
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    Byte *flv_key = (Byte *)[keyData bytes];
    
    unsigned long len = [keyData length];
    flv_key[len] = '\0';
    __weak typeof(self) weakSelf = self;
    [self.liveplayer setDecryptionKey:flv_key andKeyLength:(int)len :^(NELPKeyCheckResult ret) {
        if (ret == 0 || ret == 1) {
            [weakSelf.liveplayer prepareToPlay];
        }
    }];
    
#else
    
#ifdef DECRYPT //用视频云整套加解密系统
    if ([self.mediaType isEqualToString:@"videoOnDemand"]) {
        NSString *transferToken = NULL;
        NSString *accid = NULL;
        NSString *appKey = NULL;
        NSString *token = NULL;
        [self.liveplayer initDecryption:transferToken :accid :appKey :token :^(NELPKeyCheckResult ret) {
            NSLog(@"ret = %d", ret);
            switch (ret) {
                case NELP_NO_ENCRYPTION:
                case NELP_ENCRYPTION_CHECK_OK:
                    [self.liveplayer prepareToPlay];
                    break;
                case NELP_ENCRYPTION_UNSUPPORT_PROTOCAL:
                    [self decryptWarning:@"NELP_ENCRYPTION_UNSUPPORT_PROTOCAL"];
                    break;
                case NELP_ENCRYPTION_KEY_CHECK_ERROR:
                    [self decryptWarning:@"NELP_ENCRYPTION_KEY_CHECK_ERROR"];
                    break;
                case NELP_ENCRYPTION_INPUT_INVALIED:
                    [self decryptWarning:@"NELP_ENCRYPTION_INPUT_INVALIED"];
                    break;
                case NELP_ENCRYPTION_UNKNOWN_ERROR:
                    [self decryptWarning:@"NELP_ENCRYPTION_UNKNOWN_ERROR"];
                    break;
                case NELP_ENCRYPTION_GET_KEY_TIMEOUT:
                    [self decryptWarning:@"NELP_ENCRYPTION_GET_KEY_TIMEOUT"];
                    break;
                default:
                    break;
            }
        }];
    }
#else
    [self.liveplayer prepareToPlay];
#endif
#endif
}

- (void)decryptWarning:(NSString *)msg {
    UIAlertController *alertController = NULL;
    UIAlertAction *action = NULL;
    
    alertController = [UIAlertController alertControllerWithTitle:@"注意" message:msg preferredStyle:UIAlertControllerStyleAlert];
    action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        if (self.presentingViewController) {
            [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"viewDidDisappear");
}
- (void)NELivePlayerDidPreparedToPlay:(NSNotification*)notification
{
    //add some methods
    NSLog(@"NELivePlayerDidPreparedToPlay");
    [self.liveplayer play]; //开始播放
}

- (void)NELivePlayerPlaybackStateChanged:(NSNotification*)notification
{
    //    NSLog(@"NELivePlayerPlaybackStateChanged");
}

- (void)NeLivePlayerloadStateChanged:(NSNotification*)notification
{
    NELPMovieLoadState nelpLoadState = _liveplayer.loadState;
    
    if (nelpLoadState == NELPMovieLoadStatePlaythroughOK)
    {
        NSLog(@"finish buffering");
        self.bufferingIndicate.hidden = YES;
        self.bufferingReminder.hidden = YES;
        [self.bufferingIndicate stopAnimating];
    }
    else if (nelpLoadState == NELPMovieLoadStateStalled)
    {
        NSLog(@"begin buffering");
        self.bufferingIndicate.hidden = NO;
        self.bufferingReminder.hidden = NO;
        [self.bufferingIndicate startAnimating];
    }
}

- (void)NELivePlayerPlayBackFinished:(NSNotification*)notification
{
    UIAlertController *alertController = NULL;
    UIAlertAction *action = NULL;
    switch ([[[notification userInfo] valueForKey:NELivePlayerPlaybackDidFinishReasonUserInfoKey] intValue])
    {
        case NELPMovieFinishReasonPlaybackEnded:
        {
            alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"直播结束" preferredStyle:UIAlertControllerStyleAlert];
            action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                if (self.presentingViewController) {
                    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                }}];
            [alertController addAction:action];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
            
        case NELPMovieFinishReasonPlaybackError:
        {
            alertController = [UIAlertController alertControllerWithTitle:@"注意" message:@"播放失败" preferredStyle:UIAlertControllerStyleAlert];
            action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                if (self.presentingViewController) {
                    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                }
            }];
            [alertController addAction:action];
            [self presentViewController:alertController animated:YES completion:nil];
            break;
        }
            
        case NELPMovieFinishReasonUserExited:
            break;
            
        default:
            break;
    }
}

- (void)NELivePlayerFirstVideoDisplayed:(NSNotification*)notification
{
    NSLog(@"first video frame rendered!");
}

- (void)NELivePlayerFirstAudioDisplayed:(NSNotification*)notification
{
    NSLog(@"first audio frame rendered!");
}

- (void)NELivePlayerVideoParseError:(NSNotification*)notification
{
    NSLog(@"video parse error!");
}

- (void)NELivePlayerSeekComplete:(NSNotification*)notification
{
    NSLog(@"seek complete!");
}

- (void)NELivePlayerReleaseSuccess:(NSNotification*)notification
{
    NSLog(@"resource release success!!!");
    // 释放timer
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NELivePlayerReleaseSueecssNotification object:_liveplayer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
