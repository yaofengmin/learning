//
//  LiveRadioDetailDesTableViewCell.m
//  MicroClassroom
//
//  Created by fm on 2017/11/12.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "LiveRadioDetailDesTableViewCell.h"

#import "MicroClassBannerSegmentModel.h"

@implementation LiveRadioDetailDesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.timeBgView.layer.cornerRadius = 4.0;
    self.timeBgView.layer.masksToBounds = YES;
    
    self.teacherView.layer.cornerRadius = 4.0;
    self.teacherView.layer.masksToBounds = YES;
    
    self.teacherImage.layer.cornerRadius = 30 / 2.0;
    self.teacherImage.layer.masksToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configWithModel:(MicroClassDetailModel *)cellModel
{
    self.videoTitle.text = cellModel.videoTitle;
    self.startTime.text = [NSString stringWithFormat:@"开播时间: %@",cellModel.startTime];
    self.estimateTime.text = [NSString stringWithFormat:@"预计时长: %@",cellModel.viedoLength];
    self.videoDes.text = cellModel.videoDesc;
    [self.teacherImage sd_setImageWithURL:[NSURL URLWithString:cellModel.authorLogo] placeholderImage:[UIImage imageNamed:@"login_head"]];
    self.teacherName.text = [NSString stringWithFormat:@"主讲人: %@",cellModel.author];
    self.teacherDes.text = cellModel.autorDes;
}

@end
