//
//  LiveRecordViewController.m
//  yanshan
//
//  Created by fm on 2017/10/9.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "LiveRecordViewController.h"

#import "LSMediaCapture.h"
#import "NECamera.h"
#import "NEAuthorizeManager.h"
#import "Reachability.h"
#import "UIAlertView+NE.h"

@interface LiveRecordViewController ()<NECameraDelegate>
@property (nonatomic ,strong) UIButton *endBtn;

//直播SDK API
@property (nonatomic,strong) LSMediaCapture *mediaCapture;
@property (nonatomic,strong) LSVideoParaCtxConfiguration *paraCtx;;//推流视频参数设置
@property (nonatomic,assign) BOOL isLiving;
@property (nonatomic, strong) UIView *localPreview;//相机预览视图
@property (nonatomic ,strong) Reachability *reachiability;
/**
 作为外部摄像头
 */
//@property (nonatomic, strong) NECamera *camera;
@end

@implementation LiveRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor blackColor];
    [self initalCaputure];
    [self creatEndBtn];//结束按钮
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onStartLiveStream:) name:LS_LiveStreaming_Started object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onFinishedLiveStream:) name:LS_LiveStreaming_Finished object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didNetworkConnectChanged:) name:kReachabilityChangedNotification object:nil];
    self.reachiability = [Reachability reachabilityWithHostname:@"www.baidu.com"];
    
    [self.reachiability startNotifier];  //开始监听，会启动一个run loop
}
//！！！重点在viewWillAppear方法里调用下面两个方法
-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
    
}

-(void)initalCaputure
{
    //默认高清 以后可根据网络状况比如wifi或4G或3G来建议用户选择不同质量
    _paraCtx = [LSVideoParaCtxConfiguration defaultVideoConfiguration:LSVideoParamQuality_High2];
    _paraCtx.cameraPosition = LS_CAMERA_POSITION_BACK;
    //初始化直播参数，并创建音视频直播
    LSLiveStreamingParaCtxConfiguration *streamparaCtx = [LSLiveStreamingParaCtxConfiguration defaultLiveStreamingConfiguration];
    streamparaCtx.eHaraWareEncType = LS_HRD_AV;
    streamparaCtx.eOutFormatType               = LS_OUT_FMT_RTMP;
    streamparaCtx.uploadLog                    = YES;//是否上传sdk日志
    streamparaCtx.sLSVideoParaCtx = _paraCtx;
    
    MyLog(@"%@",_streamUrl);
    _mediaCapture = [[LSMediaCapture alloc]initLiveStream:_streamUrl withLivestreamParaCtxConfiguration:streamparaCtx];
    if (_mediaCapture == nil) {
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"初始化失败" forKey:NSLocalizedDescriptionKey];
        [self showErrorInfo:[NSError errorWithDomain:@"LSMediaCaptureErrorDomain" code:0 userInfo:userInfo]];
    }
    
    //====================视频预览====================//
    self.localPreview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth,  KScreenHeight)];
    self.localPreview.backgroundColor = [UIColor clearColor];
    //    tap.numberOfTapsRequired = 2;
    //====================动态统计信息显示视图====================//
    
    [self.view insertSubview:self.localPreview atIndex:0];
    
    BOOL success = YES;
    @weakify(self);
    success = [[NEAuthorizeManager sharedInstance] requestMediaCapturerAccessWithCompletionHandler:^(BOOL value, NSError* error){
        @strongify(self);
        if (error) {
            [self showErrorInfo:error];
        }
    }];
    _mediaCapture.externalCaptureAudioRawData = ^(unsigned char *rawData, unsigned int rawDataSize) {
        @strongify(self);
        NSLog(@"做一些音频前处理操作");
        //然后塞给 推流sdk
        [self.mediaCapture externalInputAudioRawData:rawData dataSize:rawDataSize];
    };
    
    if (streamparaCtx.eOutStreamType != LS_HAVE_AUDIO) {
        //打开摄像头预览
        [_mediaCapture startVideoPreview:self.localPreview];
    }
    //    [_mediaCapture getSDKVersionID];
    [_mediaCapture startLiveStream:^(NSError *error) {
        if (error != nil) {
            @strongify(self);
            [self showErrorInfo:error];
        }
    }];
    
    
}

//显示错误消息
-(void)showErrorInfo:(NSError*)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *errMsg = @"";
        if(error == nil){
            errMsg = @"推流过程中发生错误，请尝试重新开启";
            
        }else if([error class] == [NSError class]){
            errMsg = [error localizedDescription];
        }else{
            NSLog(@"error = %@", error);
        }
        [WFHudView showMsg:errMsg inView:self.view];
        
    });
}
#pragma mark - UI Setup


-(void)creatEndBtn
{
    _endBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_endBtn setTitle:@"结束直播" forState:UIControlStateNormal];
    _endBtn.titleLabel.font = Font(15);
    [_endBtn addTarget:self action:@selector(endAction) forControlEvents:UIControlEventTouchUpInside];
    [_endBtn setTitleColor:CColor forState:UIControlStateNormal];
    _endBtn.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    _endBtn.layer.cornerRadius = 40 / 2.0;
    _endBtn.layer.masksToBounds = YES;
    _endBtn.layer.borderWidth = 0.5;
    _endBtn.layer.borderColor = KColorFromRGB(0x000000).CGColor;
    [self.view addSubview:_endBtn];
    [_endBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth - 24, 40));
        make.centerX.mas_equalTo(0);
    }];
}

-(void)endAction
{
    //释放占有的系统资源
    [_mediaCapture stopLiveStream:^(NSError *error) {
        
    }];
    
}


//收到此消息，说明直播真的开始了
-(void)onStartLiveStream:(NSNotification*)notification
{
    NSLog(@"on start live stream");//只有收到直播开始的 信号，才可以关闭直播
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        _isLiving = YES;
        [self beginPush];
    });
}
//直播结束的通知消息
-(void)onFinishedLiveStream:(NSNotification*)notification
{
    NSLog(@"on finished live stream");
    @weakify(self);
    dispatch_async(dispatch_get_main_queue(), ^(void){
        @strongify(self);
        _isLiving = NO;
        [self endPush];
    });
}

#pragma mark === NECameraDelegate
//外部采集摄像头的数据塞回来给SDK推流
- (void)didOutputVideoSampleBuffer:(CMSampleBufferRef)sampleBuffer{
    //然后塞给 推流sdk
    [self.mediaCapture externalInputSampleBuffer:sampleBuffer];
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_mediaCapture unInitLiveStream];

}


#pragma mark -网络监听通知
- (void)didNetworkConnectChanged:(NSNotification *)notify{
    Reachability *reachability = notify.object;
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status == ReachableViaWiFi) {
        NSLog(@"切换为WiFi网络");
        //开始直播
        __weak typeof(self) weakSelf = self;
        [_mediaCapture startLiveStream:^(NSError *error) {
            if (error != nil) {
                [weakSelf showErrorInfo:error ];
            }
        }];
    }else if (status == ReachableViaWWAN) {
        NSLog(@"切换为移动网络");
        //提醒用户当前网络为移动网络，是否开启直播
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"当前网络为移动网络，是否开启直播" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
        [alert showAlertWithCompletionHandler:^(NSInteger i) {
            if (i == 0) {
                //开始直播
                __weak typeof(self) weakSelf = self;
                [_mediaCapture startLiveStream:^(NSError *error) {
                    if (error != nil) {
                        [weakSelf showErrorInfo:error];
                    }
                }];
            }
        }];
    }else if(status == NotReachable) {
        NSLog(@"网络已断开");
        //释放资源
        if (_isLiving) {
            [_mediaCapture stopLiveStream:^(NSError *error) {
                if(error == nil)
                {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        _isLiving = NO;
                    });
                }
            }];
        }
    }
}


#pragma mark === 推流
-(void)beginPush
{
    NSDictionary *paramDic = @{@"service": VideoLiveBeginPush,
                               @"videoId":_videoId,
                               @"pushUserId":self.pushUserId};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                //                [self beginRecord];
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }
    }];
}

-(void)endPush
{
    NSDictionary *paramDic = @{@"service": VideoLiveEndPush,
                               @"videoId":_videoId,
                               @"pushUserId":self.pushUserId};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                //                [self endRecord];
                [_mediaCapture unInitLiveStream];
                _mediaCapture = nil;
                [self backBtnAction];
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }
    }];
}
#pragma mark === 开始录制
-(void)beginRecord
{
    NSDictionary *paramDic = @{@"service": SetAlwaysRecordByCid,
                               @"videoId":_videoId,
                               @"pushUserId":self.pushUserId,
                               @"cid":self.cid,
                               @"needRecord":@"1",//1-开启录制； 0-关闭录制
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }
    }];
}

-(void)endRecord
{
    NSDictionary *paramDic = @{@"service": SetAlwaysRecordByCid,
                               @"videoId":_videoId,
                               @"pushUserId":self.pushUserId,
                               @"cid":self.cid,
                               @"needRecord":@"0",//1-开启录制； 0-关闭录制
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

