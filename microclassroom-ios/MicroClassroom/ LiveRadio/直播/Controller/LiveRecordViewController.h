//
//  LiveRecordViewController.h
//  yanshan
//
//  Created by fm on 2017/10/9.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

@interface LiveRecordViewController : YHBaseViewController
@property (nonatomic ,copy) NSString *streamUrl;
@property (nonatomic ,copy) NSString *videoId;
@property (nonatomic ,copy) NSString *cid;
@property (nonatomic ,copy) NSString *pushUserId;
@property (nonatomic ,copy) NSString *status;

@end
