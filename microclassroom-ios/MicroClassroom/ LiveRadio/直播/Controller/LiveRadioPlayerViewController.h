//
//  LiveRadioPlayerViewController.h
//  yanshan
//
//  Created by fm on 2017/10/10.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "YHBaseViewController.h"

@interface LiveRadioPlayerViewController : YHBaseViewController
@property(nonatomic, strong) NSURL *url;

@end
