//
//  LiveRadioDetailDesTableViewCell.h
//  MicroClassroom
//
//  Created by fm on 2017/11/12.
//  Copyright © 2017年 Hanks. All rights reserved.
//

#import "YHBaseTableViewCell.h"

@interface LiveRadioDetailDesTableViewCell : YHBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UIView *timeBgView;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *estimateTime;
@property (weak, nonatomic) IBOutlet UILabel *videoDes;
@property (weak, nonatomic) IBOutlet UIImageView *teacherImage;
@property (weak, nonatomic) IBOutlet UILabel *teacherName;
@property (weak, nonatomic) IBOutlet UIView *teacherView;
@property (weak, nonatomic) IBOutlet UILabel *teacherDes;

@end
