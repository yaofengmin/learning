//
//  ZhiBoHeaderView.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BannerModel;

@protocol ZhiBoHeaderViewDelegate <NSObject>

- (void)ZhiBoHeaderViewDidSelectItemAtIndex:(NSInteger)index;

@end

@interface ZhiBoHeaderView : UIView
@property (nonatomic, weak)id <ZhiBoHeaderViewDelegate>delegate;
- (void)setBanner:(NSMutableArray<BannerModel *> *)model;
@end
