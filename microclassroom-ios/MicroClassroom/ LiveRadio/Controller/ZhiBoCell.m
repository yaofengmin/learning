//
//  ZhiBoCell.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "ZhiBoCell.h"
#import "ZhiBoModel.h"
#import "UIImageView+AFNetworking.h"

static NSString * const kZhiboStatusBookingStr = @"热约中";
static NSString * const kZhiboStatusLivingStr = @"直播中";
static NSString * const kZhiboStatusEndStr = @"已结束";

static NSString * const kZhiboPayStatusPayFinish = @"支付完成";

@interface ZhiBoCell ()
@property (weak, nonatomic) IBOutlet UIImageView *logoImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *liveStateLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *payStateLabel;

@end

@implementation ZhiBoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.logoImg.backgroundColor = ImageBackColor;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithZhiBoModel:(ZhiBoModel *)model {
    [self setImg:model.videoLogo];
    self.timeLabel.text = [NSString stringWithFormat:@"%@ 至 %@",model.startTime, model.endTime];//[self timeFormatted:[model.videoLength intValue]];
    self.authorLabel.text = model.teacherName;
    self.titleLabel.text = model.videoTitle;
    self.payStateLabel.text = @"";
    
    
    self.liveStateLabel.text = [NSString stringWithFormat:@"￥%@",model.money];
    self.liveStateLabel.textColor = CColor;
    
   /* switch (model.status) {
        case MCZhiBoStatusBooking:
            self.liveStateLabel.text = kZhiboStatusBookingStr;
            self.liveStateLabel.textColor = MainBtnColor;
            break;
        case MCZhiBoStatusLiving:
            self.liveStateLabel.text = kZhiboStatusLivingStr;
            self.liveStateLabel.textColor = CColor;
            break;
        case MCZhiBoStatusEnd:
            self.liveStateLabel.text = kZhiboStatusEndStr;
            self.liveStateLabel.textColor = CColor;
            break;
        default:
            break;
    }*/
    
}

- (void)configureCellWithUserZhiBoModel:(ZhiBoModel *)model {
    [self setImg:model.videoLogo];
    
    
    self.timeLabel.text = [NSString stringWithFormat:@"%@ 至 %@",model.startTime, model.endTime];//[self timeFormatted:[model.videoLength intValue]];
    
    
    self.authorLabel.text = model.teacherName;
    self.titleLabel.text = model.videoTitle;
    
    self.liveStateLabel.text = [NSString stringWithFormat:@"￥%@",model.money];
    self.liveStateLabel.textColor = CColor;
    
    /*switch (model.status) {
        case MCZhiBoStatusBooking:
            self.liveStateLabel.text = kZhiboStatusBookingStr;
            self.liveStateLabel.textColor = MainBtnColor;
            break;
        case MCZhiBoStatusLiving:
            self.liveStateLabel.text = kZhiboStatusLivingStr;
            self.liveStateLabel.textColor = CColor;
            break;
        case MCZhiBoStatusEnd:
            self.liveStateLabel.text = kZhiboStatusEndStr;
            self.liveStateLabel.textColor = CColor;
            break;
        default:
            break;
    }*/
//    self.payStateLabel.text = kZhiboPayStatusPayFinish;

}

- (NSString *)timeFormatted:(int)totalSeconds {
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}

- (void)setImg:(NSString *)urlStr {

    [self.logoImg sd_setImageWithURL:[NSURL URLWithString:urlStr]];
  
    
}

@end
