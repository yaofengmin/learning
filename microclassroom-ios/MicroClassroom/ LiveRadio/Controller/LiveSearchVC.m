//
//  LiveSearchVC.m
//  MicroClassroom
//
//  Created by Hanks on 2016/10/7.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "LiveSearchVC.h"
#import "ZhiBoCell.h"
#import "ZhiBoModel.h"
#import "LiveRadioDetailViewController.h"
#import <MJRefresh.h>

static NSInteger pageCount = 1;
static NSString * kZhiBoListCell = @"ZhiBoCell";
@interface LiveSearchVC ()<UISearchBarDelegate,UITableViewDelegate,
UITableViewDataSource>
{
    int _totalCount;
}
@property (nonatomic, strong)  UISearchBar *searchBar;
@property (strong, nonatomic)  UITableView *tableView;
@property (strong, nonatomic) NSMutableArray<ZhiBoModel *> *resultModel;
@property (nonatomic ,strong) DIYTipView *tipView;
@property (strong, nonatomic) ZhiBoListModel *mainModel;
@property (nonatomic ,copy)  NSString *keyWords;

@end

@implementation LiveSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNav];
    [self creatTabelView];
    _resultModel = @[].mutableCopy;
    
    // Do any additional setup after loading the view.
}


#pragma mark - 刷新
-(void)loadDataForHeader {
    pageCount = 1;
    [self.resultModel removeAllObjects];
    [self searchWithKeyWords:_keyWords];
}

#pragma mark - 加载
- (void)loadMoreData {
    pageCount ++ ;
    [self searchWithKeyWords:_keyWords];
}


- (void)searchWithKeyWords:(NSString *)keyWords {
    _keyWords = keyWords;
    NSDictionary *paramDic = @{@"service": AppGetPiliList,
                               @"pageIndex": @(pageCount),
                               @"pageSize": @20,
                               @"keyWord":keyWords,
                               @"userId":KgetUserValueByParaName(USERID) ==nil ? @"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                if (pageCount == 1) {
                    _totalCount = [result[REQUEST_LIST][@"total"] intValue];
                    self.mainModel = [ZhiBoListModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                }
                
                for (id item in result[REQUEST_LIST][REQUEST_LIST]) {
                    
                    [_resultModel addObject:[ZhiBoModel mj_objectWithKeyValues:item]];
                }
                
                [self.tableView.mj_footer endRefreshing];
                
                self.tableView.mj_footer.hidden = _resultModel.count == _totalCount;
                [self.tableView reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                [self.tableView.mj_footer endRefreshing];
            }
            
        }

        
        if (self.resultModel.count==0) {
            if (!_tipView) {
                _tipView = [[DIYTipView alloc]initWithInfo:@"暂无相关数据" andBtnTitle:nil andClickBlock:nil];
                
            }
            [self.tableView addSubview:_tipView];
            [_tipView mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.center.equalTo(self.view);
                make.size.equalTo(CGSizeMake(120, 120));
                
            }];
        }else
        {
            if (_tipView.superview) {
                
                [_tipView removeFromSuperview];
            }
        }
    }];
    
}





-(void)creatTabelView
{
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.tableHeaderView = self.searchBar;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:kZhiBoListCell bundle:nil]
         forCellReuseIdentifier:kZhiBoListCell];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDataForHeader)];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
 
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(void)setupNav {
    
    self.view.backgroundColor = MainBackColor;
    self.yh_navigationItem.title = @"微课搜索";
    
}


-(UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
        _searchBar.delegate = self;
        _searchBar.placeholder = @"类别/名称";
        _searchBar.returnKeyType = UIReturnKeySearch;
        
    }
    return _searchBar;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.resultModel.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   return 80.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZhiBoCell * cell = [tableView dequeueReusableCellWithIdentifier:kZhiBoListCell forIndexPath:indexPath];
    [cell configureCellWithZhiBoModel:self.resultModel[indexPath.row]];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ZhiBoModel *model = self.resultModel[indexPath.row];
    LiveRadioDetailViewController *detail = [[LiveRadioDetailViewController alloc]initWithZhiBoid:model.videoId];
    [self.navigationController pushViewController:detail animated:YES];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
    [self.resultModel removeAllObjects];
    [self searchWithKeyWords:searchBar.text];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}


@end
