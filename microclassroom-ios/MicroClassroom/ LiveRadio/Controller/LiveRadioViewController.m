//
//  LiveRadioViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <MJRefresh.h>
#import "LiveRadioViewController.h"
#import "ZhiBoHeaderView.h"
#import "ZhiBoCell.h"
#import "ZhiBoModel.h"
#import "LiveRadioDetailViewController.h"
#import "LiveSearchVC.h"
#import "YHBaseWebViewController.h"
static NSString * kZhiBoListCell = @"ZhiBoCell";
static  NSInteger _pageIndex;
@interface LiveRadioViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UISearchBarDelegate,
ZhiBoHeaderViewDelegate
>
{
    int _totalCount;
}
@property (strong, nonatomic) NSMutableArray<ZhiBoModel *> *resultModel;
@property (strong, nonatomic) NSMutableArray<ZhiBoModel *> *originModel;
@property (strong, nonatomic) ZhiBoListModel *mainModel;
@property (strong, nonatomic) ZhiBoHeaderView *tableHeader;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation LiveRadioViewController


-(void)viewDidLoad {
    [super viewDidLoad];
    self.yh_navigationItem.titleView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nav_logo"]];
    
    [self initIvar];
    [self setUpTableView];
    [self pullData];
}

- (void)initIvar {
    _pageIndex = 1;
    _totalCount = 0;
    
    _resultModel = [NSMutableArray arrayWithCapacity:1];
    _originModel = [NSMutableArray arrayWithCapacity:1];
}

- (ZhiBoHeaderView *)tableHeader {
    if (!_tableHeader) {
        _tableHeader = [[ZhiBoHeaderView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 170)];
        _tableHeader.delegate = self;
    }
    
    return _tableHeader;
}

- (void)setUpTableView {
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.tableHeaderView = self.tableHeader;
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self
                                                                    refreshingAction:@selector(pullData)];
    
    [self.tableView registerNib:[UINib nibWithNibName:kZhiBoListCell bundle:nil]
         forCellReuseIdentifier:kZhiBoListCell];
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        _pageIndex = 1;
        [_originModel removeAllObjects];
        [self.resultModel removeAllObjects];
        [self pullData];
    }];
    
    
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
         @strongify(self);
        _pageIndex ++ ;
        [self pullData];
    }];
    
}

- (void)pullData {
    
    NSDictionary *paramDic = @{@"service": AppGetPiliList,
                               @"pageSize": @(20),
                               @"pageIndex": @(_pageIndex),
                               @"userId":KgetUserValueByParaName(USERID) ==nil ? @"":KgetUserValueByParaName(USERID)};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (_pageIndex == 1) {
            [self.tableView.mj_header endRefreshing];
        }else {
            [self.tableView.mj_footer endRefreshing];
        }
        //业务逻辑层
        if (result) {
            
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                if (_pageIndex == 1) {
                    _totalCount = [result[REQUEST_LIST][@"total"] intValue];
                    self.mainModel = [ZhiBoListModel mj_objectWithKeyValues:result[REQUEST_LIST]];
                }
                
                for (id item in result[REQUEST_LIST][REQUEST_LIST]) {
                    [_originModel addObject:[ZhiBoModel mj_objectWithKeyValues:item]];
                }
                
                [self.tableView.mj_footer endRefreshing];
                
                self.tableView.mj_footer.hidden = _originModel.count == _totalCount;
                [self.tableHeader setBanner:self.mainModel.display_list];
                _resultModel = _originModel.mutableCopy;
                
                [self.tableView reloadData];
                
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                [self.resultModel removeAllObjects];
                [self.tableView reloadData];
            }
            
        }
    }];
    
}


#pragma mark - ZhiBoHeaderViewDelegate
- (void)ZhiBoHeaderViewDidSelectItemAtIndex:(NSInteger)index {
  
    
    BannerModel *model = self.mainModel.display_list[index];
    if ([model.itemType isEqualToString:@"1"]) {
        LiveRadioDetailViewController *detail = [[LiveRadioDetailViewController alloc]initWithZhiBoid:model.outLinkOrId];
        [self.navigationController pushViewController:detail animated:YES];
        
    }else{
        if (NOEmptyStr(model.outLinkOrId)) {
            YHBaseWebViewController *web = [[YHBaseWebViewController alloc]initWithUrlStr:model.outLinkOrId andNavTitle:model.name];
            [self.navigationController pushViewController:web animated:YES];
        }
    }
    
}


-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    LiveSearchVC *search = [[LiveSearchVC alloc]init];
    [self.navigationController pushViewController:search animated:YES];
    return NO;
}


# pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ZhiBoModel *model = self.resultModel[indexPath.row];
    LiveRadioDetailViewController *detail = [[LiveRadioDetailViewController alloc]initWithZhiBoid:model.videoId];
    [self.navigationController pushViewController:detail animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset: UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins: UIEdgeInsetsZero];
    }
    
}

# pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.resultModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZhiBoCell * cell = [tableView dequeueReusableCellWithIdentifier:kZhiBoListCell forIndexPath:indexPath];
    [cell configureCellWithZhiBoModel:self.resultModel[indexPath.row]];
    return cell;
}



@end
