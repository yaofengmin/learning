//
//  LiveRadioDetailViewController.m
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "LiveRadioDetailViewController.h"
#import "ZhiBoModel.h"
#import "SegmentTapView.h"
#import "LiveDescripViewController.h"
#import "LiveCommentViewController.h"
#import "IQKeyboardManager.h"
#import "MineWalletViewController.h"
#import "MyZhiBoVC.h"
#import "BuyVIPViewController.h"
#import "LiveRadioDetailDesTableViewCell.h"

#import "LiveRecordViewController.h"//直播
#import "LiveRadioPlayerViewController.h"//回放

static NSString *LiveRadioDetailDesCellID = @"LiveRadioDetailDesCellID";
@interface LiveRadioDetailViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
SegmentTapViewDelegate
>{
    MicroClassDetailModel * _mainModel;
    SegmentTapView * _segmentView;
}

@property (nonatomic, copy) NSString *vidoId;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UIImageView *headImageView;
@property (nonatomic,strong) LiveDescripViewController *des;
@property (nonatomic,strong) LiveCommentViewController *com;
@property (nonatomic,strong) UITableViewCell *mainCell;
@property (nonatomic,strong) DIYButton *bookBtn;
@property (nonatomic, strong) UIView *bottomBg;

@property (nonatomic ,strong) UIButton *backBtn;
@property (nonatomic ,strong) UIButton *shareBtn;
@property (nonatomic ,strong) UILabel *statusLabel;

@property (nonatomic ,strong) ZhiBoStateModel *stateModel;



@end

@implementation LiveRadioDetailViewController

- (instancetype)initWithZhiBoid:(NSString*)vidoId {
    if (self = [super init]) {
        _vidoId = vidoId;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    [self creatTabelView];
    [self request];
    
    _des = [[LiveDescripViewController alloc]init];
    _com = [[LiveCommentViewController alloc]init];
    _com.parentController = self;
    [self addChildViewController:_des];
    [self addChildViewController:_com];
    // Do any additional setup after loading the view.
    
    
}

-(void)creatTabelView {
    
    _headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth,KScreenWidth * 422 / 750)];
    _headImageView.backgroundColor = ImageBackColor;
    _headImageView.userInteractionEnabled = YES;
    //返回按钮
    _backBtn = [DIYButton buttonWithImage:@"share_btn_back" andTitel:@"" andBackColor:nil];
    [_backBtn addTarget:self action:@selector(handlebackBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [_headImageView addSubview:_backBtn];
    [_backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(10);
        make.size.mas_equalTo(CGSizeMake(44, 44));
    }];
    
    //分享按钮
    _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_shareBtn setImage:[UIImage imageNamed:@"ic_video_share"] forState:UIControlStateNormal];
    @weakify(self);
    [_shareBtn handleControlEvent:UIControlEventTouchUpInside withBlock:^{
        @strongify(self);
        
        [[YHJHelp shareInstance] showShareInController:self andShareURL:[NSString stringWithFormat:@"%@?inviteCode=%@",APPSHARELINK,KgetUserValueByParaName(MYINVITECODE)] andTitle:self.yh_navigationItem.title andShareText:_mainModel.videoDesc andShareImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_mainModel.videoLogo]]] isShowStudyGroup:NO isDIYUrl:NO];
    }];
    [_headImageView addSubview:_shareBtn];
    [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.mas_equalTo(_backBtn);
        make.size.mas_equalTo(CGSizeMake(44, 44));
    }];
    
    //状态
    _statusLabel = [UILabel labelWithText:@"" andFont:12 andTextColor:BColor andTextAlignment:NSTextAlignmentCenter];
    _statusLabel.adjustsFontSizeToFitWidth = YES;
    _statusLabel.frame = CGRectMake(KScreenWidth - 70, KScreenWidth * 422 / 750 - 30, 70, 30);
    _statusLabel.backgroundColor = NEColor;
    [_headImageView addSubview:_statusLabel];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_statusLabel.bounds byRoundingCorners:UIRectCornerTopLeft cornerRadii:CGSizeMake(15.0, 15.0)];
    //
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = _statusLabel.bounds;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    _statusLabel.layer.mask = maskLayer;
    _statusLabel.layer.masksToBounds = YES;
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = _headImageView;
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}


#pragma mark - ********预订*******
- (void)setPayButton {
    _bottomBg = [[UIView alloc]init];
    _bottomBg.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.85];
    [self.view addSubview:_bottomBg];
    [_bottomBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(CGSizeMake(KScreenWidth, 70));
        make.bottom.equalTo(self.view).offset(- 10);
        make.centerX.equalTo(self.view);
    }];
    
    [_bottomBg addSubview:self.bookBtn];
    [self.bookBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(CGSizeMake(KScreenWidth - 30, 40));
        //        make.bottom.equalTo(self.view).offset(- 10);
        make.center.equalTo(_bottomBg);
    }];
    [self.bookBtn handleEventTouchUpInsideWithBlock:^{
        [self book];
    }];
}


- (void)book {
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        //        NSDictionary * productsInfo = @{@"1":@{@"userNote":@"",
        //                                               @"items":@[@{@"outItemId" :_mainModel.videoId,
        //                                                            @"buyAmount" : @(1),
        //                                                            @"outItemType" : @"2"
        //                                                            }]}
        //                                        };
        //
        //        NSDictionary *paramDic = @{@"service": PayForProducts,
        //                                   @"userId": KgetUserValueByParaName(USERID),
        //                                   @"productsInfo": productsInfo.mj_JSONString};
        //        SHOWHUD;
        //        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        //            HIDENHUD;
        //            //业务逻辑层
        //            if (result) {
        //                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
        //                    //                   self.bookBtn.hidden = YES;
        //                    self.bottomBg.hidden = YES;
        //                    //                    [[UIAlertView bk_showAlertViewWithTitle:@"购买成功!" message:@"前去查看我的直播" cancelButtonTitle:@"取消" otherButtonTitles:@[@"确定"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        //                    [[UIAlertView bk_showAlertViewWithTitle:@"预定成功!" message:@"前去查看我的直播" cancelButtonTitle:@"取消" otherButtonTitles:@[@"确定"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        //                        switch (buttonIndex) {
        //                            case 1:{
        //                                MyZhiBoVC *targetVC = [[MyZhiBoVC alloc] init];
        //                                [self.navigationController pushViewController:targetVC animated:YES];
        //                                break;
        //                            }
        //                            default:
        //                                break;
        //                        }
        //                    }] show];
        //
        //                }else if([result[REQUEST_CODE] isEqualToNumber:@2]){
        //                    @weakify(self);
        //                    [YHJHelp aletWithTitle:@"提示" Message:@"帐号学币不足，是否前往充值" sureTitle:@"充值" CancelTitle:@"取消" SureBlock:^{
        //                        @strongify(self);
        //                        MineWalletViewController *contr = [[MineWalletViewController alloc]init];
        //                        [self.navigationController pushViewController:contr animated:YES];
        //                    } andCancelBlock:nil andDelegate:self];
        //
        //                }else if ([result[REQUEST_CODE] isEqualToNumber:@30]) {
        //
        //                    @weakify(self);
        //                    [YHJHelp aletWithTitle:@"提示" Message:result[REQUEST_MESSAGE] sureTitle:@"立即升级" CancelTitle:@"忽略" SureBlock:^{
        //                        @strongify(self);
        //                        BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
        //                        [self.navigationController pushViewController:buy animated:YES];
        //
        //                    } andCancelBlock:nil andDelegate:self];
        //
        //                }else { //其他代表失败  如果没有详细说明 直接提示错误信息
        //
        //                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
        //
        //                }
        //
        //            }
        //        }];
        //status 2=预约中;1=可直播;3=已结束 4=直播中
        //0：空闲； 1：直播； 2：禁用(网易)
        if (_mainModel.status == MCZhiBoStatusIsAvaliable || _mainModel.status == MCZhiBoStatusLiving) {
            MyLog(@"%@-----%@",_mainModel.pushUserId,KgetUserValueByParaName(USERID));
            if ([_mainModel.pushUserId isEqualToString:KgetUserValueByParaName(USERID)]) {//自己
                LiveRecordViewController *recordVC = [[LiveRecordViewController alloc]init];
                recordVC.streamUrl = _mainModel.vcloud.pushUrl;
                recordVC.videoId = _mainModel.videoId;
                recordVC.cid = _mainModel.vcloud.cid;
                recordVC.pushUserId = _mainModel.pushUserId;
                [self.navigationController pushViewController:recordVC animated:YES];
                
            }else if (self.stateModel.status == MCZhiBoStatusIsAvaliable) {
                return;
            }else{//观看直播
                LiveRadioPlayerViewController *liveRadioPlayer = [[LiveRadioPlayerViewController alloc]init];
                liveRadioPlayer.url = [NSURL URLWithString:_mainModel.vcloud.rtmpPullUrl];
                [self.navigationController pushViewController:liveRadioPlayer animated:YES];
                
            }
        }else{
            if (_mainModel.status == MCZhiBoStatusEnd) {
                [self requestChannelStats];
                
            }else{//预约
                if (_mainModel.isJoin) {
                    [WFHudView  showMsg:@"不能重复预约" inView:self.view];
                    return;
                }
                [self requestenrollVideo];
            }
        }
    }
}


- (void)request {
    
    NSString *userId = KgetUserValueByParaName(USERID) ;
    NSDictionary *paramDic = @{Service:AppGetPiliInfo,
                               @"user_Id":userId == nil ? @"" : userId,
                               @"videoId":self.vidoId.length == 0?@"":self.vidoId
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _mainModel = [MicroClassDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                _des.mainModel = _mainModel;
                _com.mainModel = _mainModel;
                if (_mainModel.status == MCZhiBoStatusBooking){
                    self.statusLabel.text = @"预约中";
                    self.statusLabel.backgroundColor = KColorFromRGB(0xf0cf2e);
                }else if(_mainModel.status == MCZhiBoStatusLiving){
                    self.statusLabel.text = @"正在直播";
                    self.statusLabel.backgroundColor = NEColor;
                }else{
                    self.statusLabel.text = _mainModel.startTime;
                    self.statusLabel.backgroundColor = NEColor;
                }
                [self.headImageView sd_setImageWithURL:[NSURL URLWithString:_mainModel.videoLogo]];
                [self.tableView reloadData];
                [self requestWYStates];//网易状态
                [self selectedIndex:0];
                
                if (NOEmptyStr(_mainModel.buyinfo.orderId)) {
                    //                    self.bookBtn.hidden = YES;
                    self.bottomBg.hidden = YES;
                }else{
                    
                }
            }
        }
        [self selectedIndex:0];
    }];
}

- (void)setUpNav {
    
    self.yh_navigationBar.hidden = YES;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return KScreenHeight - 45 - KTopHeight;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 45)];
        header.backgroundColor = BColor;
        _segmentView = ({
            
            SegmentTapView * view = [[SegmentTapView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 44)];
            view.lineW = 20.0;
            view.delegate = self;
            view.dataArray = @[@"详情", @"评论"];
            view.textNomalColor = JColor;
            view.textSelectedColor = NEColor;
            view.titleFont = 16.0;
            view.lineColor = NEColor;
            view;
            
        });
        
        [header addSubview:_segmentView];
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 44, KScreenWidth, 1)];
        line.backgroundColor = KColorFromRGB(0xededed);
        [header addSubview:line];
        return header;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!_mainCell) {
        _mainCell = [[UITableViewCell alloc]initWithStyle:0 reuseIdentifier:@"cellId"];
        _mainCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return _mainCell;
    
}

-(void)selectedIndex:(NSInteger)index {
    if (index == 0) {
        if (_com.view.superview) {
            [_com.view removeFromSuperview];
        }
        
        [_mainCell.contentView addSubview:_des.view];
        [_des.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_mainCell);
        }];
        
        if (NOEmptyStr(_mainModel.buyinfo.orderId)) {
            //            self.bookBtn.hidden = YES;
            self.bottomBg.hidden = YES;
        }else{
            //          self.bookBtn.hidden = NO;
            self.bottomBg.hidden = NO;
        }
        _com.bottomView.hidden = YES;
        
    }else {
        if (_des.view.superview) {
            [_des.view removeFromSuperview];
        }
        [_mainCell.contentView addSubview:_com.view];
        [_com.view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(_mainCell);
        }];
        //        self.bookBtn.hidden = YES;
        self.bottomBg.hidden = YES;
        _com.bottomView.hidden = NO;
    }
}


- (DIYButton *)bookBtn {
    if (!_bookBtn) {
        _bookBtn = [DIYButton buttonWithImage:nil andTitel:@"" andBackColor:kNavigationBarColor];
        if (_mainModel.status == MCZhiBoStatusIsAvaliable || _mainModel.status == MCZhiBoStatusLiving) {//status 2=预约中;1=可直播;3=已结束 4=直播中
            //0：空闲； 1：直播； 2：禁用(网易)
            if ([_mainModel.pushUserId isEqualToString:KgetUserValueByParaName(USERID)]){
                [_bookBtn setTitle:@"开始直播" forState:UIControlStateNormal];
                [_bookBtn setBackgroundColor:NEColor];
            }else if (_mainModel.status == MCZhiBoStatusIsAvaliable) {
                [_bookBtn setTitle:@"直播暂未开始" forState:UIControlStateNormal];
                [_bookBtn setBackgroundColor:DColor];
            }else{
                [_bookBtn setTitle:@"观看直播" forState:UIControlStateNormal];
                [_bookBtn setBackgroundColor:NEColor];
            }
            
        }else if (_mainModel.status == MCZhiBoStatusBooking){
            if (_mainModel.isJoin) {
                [_bookBtn setTitle:@"已预约" forState:UIControlStateNormal];
                _bookBtn.backgroundColor = DColor;
            }else{
                [_bookBtn setTitle:@"预约" forState:UIControlStateNormal];
                _bookBtn.backgroundColor = NEColor;
            }
            
        }else if (_mainModel.status == MCZhiBoStatusEnd){
            [_bookBtn setTitle:@"已结束" forState:UIControlStateNormal];
            [_bookBtn setBackgroundColor:DColor];
        }
        
    }
    return _bookBtn;
}


-(void)handlebackBtnAction
{
    [self backBtnAction];
}


#pragma mark === 获取网易状态
-(void)requestWYStates
{
    NSDictionary *paramDic = @{@"service": VideoLiveGetChannerState,
                               @"cId":_mainModel.vcloud.cid};
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                self.stateModel = [ZhiBoStateModel mj_objectWithKeyValues:result[@"info"][@"ret"]];
                if (self.stateModel.status != 2) {
                    [self setPayButton];
                }
            }
            
        }
    }];
}
#pragma mark === 获取直播回放地址
-(void)requestChannelStats
{
    NSDictionary *paramDic = @{@"service": VideoLiveGetVideoListByCid,
                               @"cid":_mainModel.vcloud.cid};
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                LiveRadioPlayerViewController *liveRadioPlayer = [[LiveRadioPlayerViewController alloc]init];
                NSDictionary *info = [result objectForSafeKey:@"info"];
                NSDictionary *ret = [info objectForSafeKey:@"ret"];
                NSArray *videoList = [ret objectForSafeKey:@"videoList"];
                NSString *origUrl = [videoList.firstObject objectForSafeKey:@"origUrl"];
                liveRadioPlayer.url = [NSURL URLWithString:origUrl];
                [self.navigationController pushViewController:liveRadioPlayer animated:YES];
            } else { //其他代表失败  如果没有详细说明 直接提示错误信息
                [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            }
            
        }
    }];
}
#pragma mark === 预约状态
-(void)requestenrollVideo
{
    NSDictionary *paramDic = @{@"service": VideoLiveEnrollVideoLives,
                               @"videoId":_mainModel.videoId,
                               @"userId":KgetUserValueByParaName(USERID)
                               };
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        //业务逻辑层
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                [_bookBtn setTitle:@"已预约" forState:UIControlStateNormal];
                [_bookBtn setBackgroundColor:DColor];
                _mainModel.isJoin = 2;
            }
            [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
            
        }
    }];
}
@end
