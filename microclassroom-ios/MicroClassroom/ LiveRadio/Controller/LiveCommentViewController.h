//
//  LiveCommentViewController.h
//  MicroClassroom
//
//  Created by Hanks on 2016/9/25.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MicroClassDetailModel,LiveRadioDetailViewController;
@interface LiveCommentViewController : UIViewController
@property (nonatomic ,strong)    MicroClassDetailModel * mainModel;
@property (nonatomic ,weak)  LiveRadioDetailViewController *parentController;
@property (nonatomic,strong) UIView *bottomView;
@end
