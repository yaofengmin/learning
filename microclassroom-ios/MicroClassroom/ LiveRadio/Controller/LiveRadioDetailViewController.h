//
//  LiveRadioDetailViewController.h
//  MicroClassroom
//
//  Created by Hanks on 16/9/18.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseViewController.h"

@interface LiveRadioDetailViewController : YHBaseViewController

- (instancetype)initWithZhiBoid:(NSString*)vidoId;

@end
