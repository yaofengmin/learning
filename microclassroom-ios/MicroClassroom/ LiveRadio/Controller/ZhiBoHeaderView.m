//
//  ZhiBoHeaderView.m
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "ZhiBoHeaderView.h"

#import "SDCycleScrollView.h"

#import "MicroClassBannerSegmentModel.h"
@interface ZhiBoHeaderView ()
<
SDCycleScrollViewDelegate
>
@property (nonatomic, strong) SDCycleScrollView *bannerView;
@end

@implementation ZhiBoHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (SDCycleScrollView *)bannerView {
    
    if (!_bannerView) {
        
        _bannerView = ({
            
            SDCycleScrollView * view = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:nil];
            
            [self addSubview:view];
            
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.edges.equalTo(self);
                
            }];
            
            view;
            
        });
        
        _bannerView.delegate = self;
    }
    
    return _bannerView;
    
}

- (void)setBanner:(NSMutableArray<BannerModel *> *)model {
    
    NSMutableArray * arr = [NSMutableArray arrayWithCapacity:1];
    
    for (BannerModel * item in model) {
        [arr addObject:item.pic];
    }
    
    self.bannerView.imageURLStringsGroup = arr;
    
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {

    if ([self.delegate respondsToSelector:@selector(ZhiBoHeaderViewDidSelectItemAtIndex:)]) {
        [self.delegate ZhiBoHeaderViewDidSelectItemAtIndex:index];
    }
    
}

@end
