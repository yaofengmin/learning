//
//  ZhiBoCell.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZhiBoModel;

@interface ZhiBoCell : UITableViewCell
- (void)configureCellWithZhiBoModel:(ZhiBoModel *)model;
- (void)configureCellWithUserZhiBoModel:(ZhiBoModel *)model;
@end
