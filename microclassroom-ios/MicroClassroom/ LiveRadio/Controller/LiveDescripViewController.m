//
//  LiveDescripViewController.m
//  MicroClassroom
//
//  Created by Hanks on 2016/9/25.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "LiveDescripViewController.h"
#import "MicroClassSectionView.h"
#import "MicroClassDetailTableViewCell.h"
#import "ZhiBoModel.h"
#import "MicroClassAuthorCell.h"
#import "LiveRadioDetailDesTableViewCell.h"

static NSString * const kMicroClassSectionViewID = @"MicroClassSectionView";
static NSString * const kMicroClassDetailCellID = @"MicroClassDetailTableViewCell";
static NSString * const kMicroClassAuthorCellID = @"MicroClassAuthorCell";

static NSString *LiveRadioDetailDesCellID = @"LiveRadioDetailDesCellID";


@interface LiveDescripViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic, copy) NSArray * sectionTitleArr;
@end

@implementation LiveDescripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatTabelView];
    // Do any additional setup after loading the view.
}

- (void)setMainModel:(MicroClassDetailModel *)mainModel {
    _mainModel = mainModel;
    [self.tableView reloadData];
}

-(void)creatTabelView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 3)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(0);
    }];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassDetailCellID bundle:nil]
         forCellReuseIdentifier:kMicroClassDetailCellID];
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassAuthorCellID bundle:nil]
         forCellReuseIdentifier:kMicroClassAuthorCellID];
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassSectionViewID bundle:nil] forHeaderFooterViewReuseIdentifier:kMicroClassSectionViewID];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LiveRadioDetailDesTableViewCell" bundle:nil] forCellReuseIdentifier:LiveRadioDetailDesCellID];
    
    self.tableView.userInteractionEnabled = NO;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.mainModel != nil) {
        return 1;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    //
    //    MicroClassSectionView * view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kMicroClassSectionViewID];
    //
    //    [view setTitle:self.sectionTitleArr[section]];
    //
    //    return view;
    
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    //        if (indexPath.section == 0) {
    //            MicroClassDetailTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassDetailCellID forIndexPath:indexPath];
    //            [cell configWithModel:_mainModel];
    //            return cell;
    //        }
    //
    //        if (indexPath.section == 1) {
    //            MicroClassAuthorCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassAuthorCellID forIndexPath:indexPath];
    //            [cell configWithTeacherModel:_mainModel.teacherInfo];
    //            cell.careBtn.hidden = YES;
    //
    //            return cell;
    //        }
    LiveRadioDetailDesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:LiveRadioDetailDesCellID forIndexPath:indexPath];
    if (_mainModel != nil) {
        [cell configWithModel:_mainModel];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if (indexPath.section == 0) {
    //        return [MicroClassDetailTableViewCell cellHeightWithModel:_mainModel];
    //    }
    //
    //    if (indexPath.section == 1) {
    //        return [MicroClassAuthorCell cellHeightWithTeacherModel:_mainModel.teacherInfo];
    //    }
    if (self.mainModel != nil) {
        return [self.mainModel LiveRaideoDesCellHeight];
    }
    return 0;
}

#pragma mark - lazy init

- (NSArray *)sectionTitleArr {
    
    if (!_sectionTitleArr) {
        _sectionTitleArr = @[@"课程导读", @"作者介绍"];
    }
    return _sectionTitleArr;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
