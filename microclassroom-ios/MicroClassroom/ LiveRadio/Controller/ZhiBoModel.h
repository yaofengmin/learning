//
//  ZhiBoModel.h
//  MicroClassroom
//
//  Created by DEMO on 16/9/3.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "YHBaseModel.h"
#import "MicroClassBannerSegmentModel.h"

@interface ZhiBoModel : YHBaseModel
@property (nonatomic, copy) NSString *videoId;
@property (nonatomic, copy) NSString *videoTitle;
@property (nonatomic, copy) NSString *videoLength;
@property (nonatomic, copy) NSString *videoLaud;
@property (nonatomic, copy) NSString *videoLogo;

@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *endTime;
@property (nonatomic, copy) NSString *money;
@property (nonatomic, copy) NSString *authorLogo;
@property (nonatomic, copy) NSString *author;


/**
 返回说明 1=直播进行中, 参加者可以进入观看直播
 2=预约中 , 活动预约中,尚未开始
 3=结束 , 活动已结束
 4=录播已上线, 参加者可以观看录播回放
 */
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, copy) NSString *teacherName;
/**
 尾喉直播Id
 */
@property (nonatomic, copy) NSString *webinarId;
@property (nonatomic, copy) NSString *webinarCode;
@property (nonatomic, copy) NSString *webinarUrl;
@end

//网易直播状态
@interface ZhiBoStateModel : YHBaseModel
@property (nonatomic, copy) NSString *needRecord;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *duration;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *filename;

@property (nonatomic, copy) NSString *format;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *ctime;
@property (nonatomic, copy) NSString *recordStatus;
@property (nonatomic, copy) NSString *cid;

@end

@interface ZhiBoListModel : YHBaseModel
@property (nonatomic, strong) NSMutableArray<ZhiBoModel *> *list;
@property (nonatomic, strong) NSMutableArray<BannerModel *> *display_list;
@property (nonatomic, assign) int total;
@end


