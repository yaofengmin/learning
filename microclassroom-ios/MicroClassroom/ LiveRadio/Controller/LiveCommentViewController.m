//
//  LiveCommentViewController.m
//  MicroClassroom
//
//  Created by Hanks on 2016/9/25.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "LiveCommentViewController.h"
#import "ZhiBoModel.h"
#import "MicroClassAuthorCell.h"
#import "MicroClassReplyCommentCell.h"

#import "LiveRadioDetailViewController.h"
#import "MicroClassCommentTableViewCell.h"

static NSString * const kMicroClassReplyCommentCellID = @"MicroClassReplyCommentCell";
static NSString * const kMicroClassAuthorCellID = @"MicroClassAuthorCell";

static NSString *commentCellID = @"MicroClassCommentCellID";

@interface LiveCommentViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate,
deleteAction
>{
    int _commentId;
    int _commentUserId;
}
@property (nonatomic,strong) UITableView *tableView;
@property (strong, nonatomic)  UITextField *commentTxt;
@property (nonatomic,strong) UIButton *sendBtn;
@property (nonatomic,strong) NSMutableArray *comments;

@end

@implementation LiveCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestVideoComments];
    [self creatTabelView];
    // Do any additional setup after loading the view.
}

-(void)setParentController:(LiveRadioDetailViewController *)parentController {
    _parentController = parentController;
    [parentController.view addSubview:self.bottomView];
    self.bottomView.hidden = YES;
}

-(void)requestVideoComments
{
    NSDictionary *paramDic = @{Service:VidoeoGetComments,//GetVideoComments
                               @"userId":KgetUserValueByParaName(USERID),
                               @"toId":self.mainModel.videoId.length == 0?@"":self.mainModel.videoId,
                               @"ctype":@"3"};
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                if (self.comments.count) {
                    [self.comments removeAllObjects];
                }
                self.comments = [MicroClassCommentModel mj_objectArrayWithKeyValuesArray:result[REQUEST_LIST]];
                [self.tableView reloadData];
            }
        }
        
    }];
}

- (void)setMainModel:(MicroClassDetailModel *)mainModel {
    _mainModel = mainModel;
    [self.tableView reloadData];
}

-(void)creatTabelView
{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 100)];
    self.tableView.separatorColor = KColorFromRGB(0xededed);
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(0);
    }];
    
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassReplyCommentCellID bundle:nil]
         forCellReuseIdentifier:kMicroClassReplyCommentCellID];
    
    [self.tableView registerNib:[UINib nibWithNibName:kMicroClassAuthorCellID bundle:nil]
         forCellReuseIdentifier:kMicroClassAuthorCellID];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MicroClassCommentTableViewCell" bundle:nil] forCellReuseIdentifier:commentCellID];
    
    
    
//    @weakify(self);
//    [self.tableView.tableFooterView addGestureRecognizer:[[UITapGestureRecognizer alloc]bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//        @strongify(self);
//        [self.parentController.view endEditing:YES];
//    }]];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.parentController.view endEditing:YES];
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return _mainModel.Comments.count;
//
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    return _mainModel.Comments[section].replay_commnets.count + 1;
    return _comments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if (indexPath.row == 0) {
    //        MicroClassAuthorCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassAuthorCellID forIndexPath:indexPath];
    //        [cell configWithCommentModel:_mainModel.Comments[indexPath.section]];
    //        return cell;
    //    }
    //
    //    MicroClassReplyCommentCell * cell = [tableView dequeueReusableCellWithIdentifier:kMicroClassReplyCommentCellID forIndexPath:indexPath];
    //    MicroClassCommentModel * model = _mainModel.Comments[indexPath.section];
    //    [cell configWithModel:model.replay_commnets[indexPath.row - 1]];
    //
    //    return cell;
    MicroClassCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:commentCellID forIndexPath:indexPath];
    cell.stamp = indexPath;
    cell.delegate = self;
    [cell configWithCommentModel:self.comments[indexPath.row]];
    cell.allBtn.hidden = YES;
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if (indexPath.row == 0) {
    //        return [MicroClassAuthorCell cellHeightWithCommentModel:_mainModel.Comments[indexPath.section]];
    //    }
    //
    //    return [MicroClassReplyCommentCell cellHeightWithReplyCommentModel:(MicroClassReplyCommentModel *)_mainModel.Comments[indexPath.section].replay_commnets[indexPath.row - 1]];
    MicroClassCommentModel *model = self.comments[indexPath.row];
    return [model commentCellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        if (indexPath.row == 0) {
            _commentUserId = _mainModel.Comments[indexPath.section].userId;
        } else {
            _commentUserId = _mainModel.Comments[indexPath.section].replay_commnets[indexPath.row - 1].userId;
        }
        
        _commentId = _mainModel.Comments[indexPath.section].comContentId;
        
        [self.commentTxt becomeFirstResponder];
    }
}


- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenHeight - 45, KScreenWidth, 45)];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.layer.borderColor = LColor.CGColor;
        _bottomView.layer.borderWidth = 0.5;
        _commentTxt = [UITextField new];
        _commentTxt.font = Font(14);
        _commentTxt.placeholder = @"快来评论一下吧";
        _commentTxt.textColor = JColor;
        _commentTxt.layer.cornerRadius = 4.0;
        _commentTxt.layer.masksToBounds = YES;
        _commentTxt.delegate = self;
        _commentTxt.backgroundColor = NCColor;
        [_bottomView addSubview:_commentTxt];
        [_commentTxt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(8, 10, 8, 70));
        }];
        
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendBtn setTitle:@"发送" forState:UIControlStateNormal];
        _sendBtn.titleLabel.font = Font(13);
        _sendBtn.backgroundColor = KColor;
        _sendBtn.layer.cornerRadius = 4.0;
        _sendBtn.layer.masksToBounds =YES;
        [_bottomView addSubview:_sendBtn];
        [_sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(CGSizeMake(50, 25));
            make.right.equalTo(_bottomView).offset(-10);
            make.centerY.equalTo(_bottomView);
        }];
        
        @weakify(self);
        [_sendBtn handleEventTouchUpInsideWithBlock:^{
            @strongify(self);
            [self sendComment];
        }];
        
    }
    return _bottomView;
}

#pragma mark - 发起评论
- (void)sendComment {
    if ([YHJHelp isLoginAndIsNetValiadWitchController:self]) {
        
        [self.view endEditing:YES];
        
        NSDictionary *paramDic = @{@"service": AddComment,
                                   @"userId": KgetUserValueByParaName(USERID),
                                   @"toId": self.mainModel.videoId,
                                   @"ctype": @(3),
                                   @"pId": @(_commentId),
                                   @"toUserId": @(_commentUserId),
                                   @"content": self.commentTxt.text};
        
        SHOWHUD;
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            HIDENHUD;
            //业务逻辑层
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) { // 0代表业务成功
                    
                    _commentUserId = 0;
                    _commentId = 0;
                    
                    [self requestVideoComments];
                    self.commentTxt.text = @"";
                }//其他代表失败  如果没有详细说明 直接提示错误信息
                    
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                    
                
                
            }
        }];
    }
    
}


- (void)request {
    
    NSString *userId = KgetUserValueByParaName(USERID) ;
    NSDictionary *paramDic = @{Service:AppGetPiliInfo,
                               @"user_Id":userId == nil ? @"" : userId,
                               @"videoId":_mainModel.videoId
                               };
    
    SHOWHUD;
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
        HIDENHUD;
        if (result) {
            if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                _mainModel = [MicroClassDetailModel mj_objectWithKeyValues:result[REQUEST_INFO]];
                [self.tableView reloadData];
            }
        }
    }];
}

#pragma mark === 删除某条说说
-(void)delectMsgWithIndext:(NSIndexPath *)indext
{
    MicroClassCommentModel *model = self.comments[indext.row];
    [YHJHelp aletWithTitle:nil Message:@"确认删除这条信息吗" sureTitle:@"确定" CancelTitle:@"取消" SureBlock:^{
        NSDictionary *paramDic = @{Service:VideoDeleteCommentMessage,//VideoDeleteCommentMessage
                                   @"userId":KgetUserValueByParaName(USERID),
                                   @"commentId":@(model.comContentId)
                                   };
        
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:paramDic andBlocks:^(NSDictionary *result) {
            if (result) {
                if ([result[REQUEST_CODE] isEqualToNumber:@0]) {
                    [self.comments removeObject:model];
                    [self.tableView reloadData];
                }else {
                    [WFHudView showMsg:result[REQUEST_MESSAGE] inView:nil];
                }
            }
        }];
    } andCancelBlock:nil andDelegate:self];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
