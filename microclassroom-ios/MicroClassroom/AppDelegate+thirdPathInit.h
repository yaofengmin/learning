//
//  AppDelegate+thirdPathInit.h
//  MicroClassroom
//
//  Created by Hanks on 16/8/25.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (thirdPathInit)

- (void)setKeyBoardManager;
- (void)aFNetworkStatus;
- (void)registerForRemoteNotification;
- (void)setVHallSDK;
- (void)setUMSDK;
- (NSString *)getMessageStr:(EMMessage *)msg;
-(void)setKeyValueWithLabelHave;
- (void)customLaunchImageView;
@end
