//
//  AppDelegate+thirdPathInit.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/25.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "AppDelegate+thirdPathInit.h"
#import <IQKeyboardManager.h>
#import <AFHTTPSessionManager.h>
//友盟
#import <UMShare/UMShare.h>
#import <UMCommon/UMCommon.h>
#import <UShareUI/UShareUI.h>

@implementation AppDelegate (thirdPathInit)


-(void)registerForRemoteNotification
{
    
#if !TARGET_IPHONE_SIMULATOR
    //iOS8 注册APNS
    if ([[UIApplication sharedApplication]  respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [[UIApplication sharedApplication]  registerForRemoteNotifications];
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound |
        UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [[UIApplication sharedApplication]  registerUserNotificationSettings:settings];
    }
#endif
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0 ;
    
}

- (NSString *)getMessageStr:(EMMessage *)msg {
    
    EMMessageBody *msgBody = msg.body;
    
    NSString *msgText;
    
    switch (msgBody.type) {
        case EMMessageBodyTypeText:
        {
            // 收到的文字消息
            EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
            msgText = textBody.text;
        }
            break;
        case EMMessageBodyTypeImage:
        {
            // 得到一个图片消息body
            msgText = @"收到一张图片";
        }
            break;
        case EMMessageBodyTypeLocation:
        {
            msgText = @"收到一条定位消息";
        }
            break;
        case EMMessageBodyTypeVoice:
        {
            // 音频
            msgText = @"收到一条语音消息";
        }
            break;
        case EMMessageBodyTypeVideo:
        {
            msgText = @"收到一条视频消息";
        }
            break;
        case EMMessageBodyTypeFile:
        {
            msgText = @"收到一个文件";
        }
            break;
            
        default:
            msgText = @"收到一条新消息";
            break;
    }
    
    return msgText;
    
}



-(void)setKeyBoardManager {
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    //    manager.enableAutoToolbar = NO;
}

- (void)aFNetworkStatus{
    
    //1.创建网络监测者
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://www.baidu.com"]];
    
    
    /*枚举里面四个状态  分别对应 未知 无网络 数据 WiFi
     typedef NS_ENUM(NSInteger, AFNetworkReachabilityStatus) {
     AFNetworkReachabilityStatusUnknown          = -1,      未知
     AFNetworkReachabilityStatusNotReachable     = 0,       无网络
     AFNetworkReachabilityStatusReachableViaWWAN = 1,       蜂窝数据网络
     AFNetworkReachabilityStatusReachableViaWiFi = 2,       WiFi
     };
     */
    
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        //这里是监测到网络改变的block  可以写成switch方便
        //在里面可以随便写事件
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                
                NSLog(@"未知网络状态");
                break;
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"无网络");
                
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"蜂窝数据网");
                
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"WiFi网络");
                break;
                
            default:
                break;
        }
    }] ;
    
    [manager.reachabilityManager startMonitoring];
}


- (void)setVHallSDK {
    //     [VHallApi registerApp:DEMO_AppKey SecretKey:DEMO_AppSecretKey];
}


- (void)setUMSDK {
    /* 设置友盟appkey */
    NSString *Channel = @"medical_dev";
#if DEBUG
    [UMConfigure setLogEnabled:YES];
#else
    Channel = @"App Store";
#endif



    [UMConfigure initWithAppkey:UMSHAREKEY channel:Channel];

    [UMSocialGlobal shareInstance].universalLinkDic = @{@(UMSocialPlatformType_WechatSession):WeChatUniversalLinks};

  BOOL success =  [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:WeChatShareAppKey appSecret:WeChatShareAppSecret redirectURL:WeChatUniversalLinks];
    
//    //设置微信好友或者朋友圈的分享url,下面是微信好友，微信朋友圈对应wechatTimelineData
[UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine)]];
    [UMSocialShareUIConfig shareInstance].sharePageGroupViewConfig.sharePageGroupViewPostionType = UMSocialSharePageGroupViewPositionType_Bottom;
    [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageItemStyleType = UMSocialPlatformItemViewBackgroudType_IconAndBGRoundAndSuperRadius;
}

#pragma mark === 初始化标签值
-(void)setKeyValueWithLabelHave
{
    //需求
    KsetUserValueByParaName(@(LabelFundingType), kReimbursement);//找投资
    KsetUserValueByParaName(@(LabelExcellent), kCeremonial);//觅人才
    KsetUserValueByParaName(@(LabelManned), kCompounding);//寻合作
    KsetUserValueByParaName(@(LabelDitching), kPersonal);//卖产品
    KsetUserValueByParaName(@(LabelLOtherKind), kOtherKind);//其他
    
    //资源
    KsetUserValueByParaName(@(LabelReimbursement), kFunding);//有资金
    KsetUserValueByParaName(@(LabelDitching) , kDitching);//有渠道
    KsetUserValueByParaName(@(LabelCompounding) , kManned);//有人脉
    KsetUserValueByParaName(@(LabelCeremonial) , kExcellent);//秀能力
    KsetUserValueByParaName(@(LabelOtherKind), kLOtherKind);//其他
    
}
#pragma mark === 自定义启动图
- (void)customLaunchImageView
{
    UIImageView *launchImageView = [[UIImageView alloc] initWithFrame:self.window.bounds];
    launchImageView.image = [self getLaunchImage];
    [self.window addSubview:launchImageView];
    [self.window bringSubviewToFront:launchImageView];
    
    UILabel *vesionLabel = [[UILabel alloc] init];
    vesionLabel.textColor = BColor;
    vesionLabel.font = [UIFont boldSystemFontOfSize:15];
    vesionLabel.text = [NSString stringWithFormat:@"地球历%@",[YHJHelp dateUpperChange]];
    vesionLabel.textAlignment = NSTextAlignmentCenter;
    [launchImageView addSubview:vesionLabel];
    [vesionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-30);
        make.centerX.mas_equalTo(launchImageView);
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.0 animations:^{
            launchImageView.alpha = 0.0;
//            launchImageView.transform = CGAffineTransformMakeScale(1.2, 1.2);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (NOEmptyStr(KgetUserValueByParaName(USERID))) {
                    [self setLauchImage];
                }
            });
        } completion:^(BOOL finished) {
            [launchImageView removeFromSuperview];
        }];
    });
}

- (UIImage *)getLaunchImage
{
    UIImage *lauchImage = nil;
    NSString *viewOrientation = nil;
    CGSize viewSize = [UIScreen mainScreen].bounds.size;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        
        viewOrientation = @"Landscape";
        
    } else {
        
        viewOrientation = @"Portrait";
    }
    
    NSArray *imagesDictionary = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for (NSDictionary *dict in imagesDictionary) {
        
        CGSize imageSize = CGSizeFromString(dict[@"UILaunchImageSize"]);
        if (CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]]) {
            
            lauchImage = [UIImage imageNamed:dict[@"UILaunchImageName"]];
        }
    }
    return lauchImage;
}


@end
