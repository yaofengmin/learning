//
//  DIYButton.h
//  pinnc
//
//  Created by Hanks on 16/1/25.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DIYButton : UIButton

/**
 *  初始化
 *
 *  @param imageName Button上的Image  可为nil   （依赖性0）
 *  @param title     Button上的标题
 *  @param color     Button上的背景色
 *
 *  @return Button＊
 */
+(instancetype)buttonWithImage:(NSString *)imageName andTitel:(NSString *)title andBackColor:(UIColor *)color;
+(instancetype)buttonWithImage:(NSString *)imageName andTitel:(NSString *)title andBackColor:(UIColor *)color andCornerRadius:(CGFloat) cornerRadius;


+(void)btnTouchUp:(UIButton *)send;
@end
