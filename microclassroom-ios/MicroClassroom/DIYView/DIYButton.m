//
//  DIYButton.m
//  pinnc
//
//  Created by Hanks on 16/1/25.
//  Copyright © 2016年 hanks. All rights reserved.
//

#import "DIYButton.h"

static UIColor *showColor = nil;
@implementation DIYButton

+(instancetype)buttonWithImage:(NSString *)imageName andTitel:(NSString *)title andBackColor:(UIColor *)color
{
    showColor = nil;
    DIYButton *button = [DIYButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = color;
    showColor = color;
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = YES;
    //1.image title 2.image 3.title 4.nil
    if (nil != imageName && nil != title) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
        button.imageEdgeInsets =UIEdgeInsetsMake(0, 0, 0, 15);
        
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:EColor forState:UIControlStateHighlighted];
        button.titleLabel.font = [UIFont systemFontOfSize:16];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    }else if(nil == imageName && nil != title){
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitleColor:EColor forState:UIControlStateHighlighted];
        button.titleLabel.font = [UIFont systemFontOfSize:16];
        
    }else if(nil !=imageName && nil == title){
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
    }else{//nil
        
    }
    
//    [button addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
//    [button addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}


+(instancetype)buttonWithImage:(NSString *)imageName andTitel:(NSString *)title andBackColor:(UIColor *)color andCornerRadius:(CGFloat) cornerRadius
{
    DIYButton *button = [self buttonWithImage:imageName andTitel:title andBackColor:color];
    button.layer.cornerRadius = cornerRadius;
    
    return button;
}


+(void)btnTouchDown:(UIButton *)send
{
    
    send.backgroundColor = MainBtnColor;
}


+(void)btnTouchUp:(UIButton *)send
{
    
    send.backgroundColor = showColor;
   
}


//-(void)setSelected:(BOOL)selected
//{
//    if (selected) {
//        
//        self.backgroundColor = MainBtnColor;
//        
//    }else
//    {
//        self.backgroundColor = MainBtnUColor;
//    }
//}



@end
