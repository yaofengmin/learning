//
//  AppDelegate.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/26.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WXApi.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,WXApiDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) dispatch_queue_t homeVCQueue;

typedef void(^UserInfoFinishBlock)();
@property (copy) UserInfoFinishBlock userInitBlock;

- (void)loginEMSDK;
-(void)setUpRootViewController;
-(void)homeRequestWithFinishBlock:(UserInfoFinishBlock) getInitfinishBlock;
-(void)setLauchImage;
@end

