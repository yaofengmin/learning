//
//  FMDatabaseHelper.h
//  toolstest
//
//  Created by dqf on 12-7-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDatabase.h>

@interface FMDatabaseHelper : NSObject
{
	FMDatabase *mDb;
}


-(id)initWith:(NSString*)dbName;

//创建表
-(BOOL)createTableWith:(NSString*)tableName args:(NSDictionary*)args;

//创建带主键的表,主键必须为int型
-(BOOL)createTableWith:(NSString*)tableName args:(NSDictionary*)args mainKey:(NSString*)mainKey;
-(BOOL)updatebysql:(NSString*)sql;
//更新表中的一条记录
-(BOOL)update:(NSString *)tableName wheres:(NSDictionary *)wheres args:(NSDictionary *)args;
-(BOOL)createbysql:(NSString*)sql;
//删除表
-(BOOL)deleteTableWith:(NSString*)tableName;

//从表中删除一条记录
-(BOOL)deleteFrom:(NSString *)tableName wheres:(NSDictionary *)wheres;

//查询表是否存在
-(BOOL)checkTableExist:(NSString*)tableName;

//查询表中某条记录是否存在
-(BOOL)checkIetmExist:(NSString*)tableName wheres:(NSDictionary*)wheres;
-(BOOL)insertAutoCommitToSql:(NSMutableArray*)sql;
//插入一条记录到表中,自动commit
-(BOOL)insertAutoCommitTo:(NSString*)tableName args:(NSDictionary*)args;

//插入一条记录到表中
-(BOOL)insertTo:(NSString*)tableName args:(NSDictionary*)args;
//读取表的内容
-(FMResultSet*) select:(NSString*)tableName wheres:(NSDictionary*)wheres ;

//从某行开始查询固定条数据
-(FMResultSet*) select:(NSString*)tableName startLine:(NSString *)aStartLine lines:(NSString *)totalLines;

-(FMResultSet*) selectbysql:(NSString*)sql;

//数据库里总共有多少条数据
- (int) select:(NSString*)tableName;

//读取表内容按指定键排序,forword=0表示升序，1表示降序
-(FMResultSet*) select:(NSString*)tableName wheres:(NSDictionary*)wheres orderKey:(NSString*)order forword:(int)f;
//清空表
-(BOOL)clearTable:(NSString*)tableName;

//清空表,自动commit
-(BOOL)clearTableAutoCommit:(NSString*)tableName;

//处理事物失败回滚
- (BOOL)rollback;
//处理事物成功提交
- (BOOL)commit;
//开始事物处理
- (BOOL)beginTransaction;



@end
