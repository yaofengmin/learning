//
//  PublishCollectionLayout.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/10.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "PublishCollectionLayout.h"


@interface PublishCollectionLayout()

@property(nonatomic ,assign) CGFloat itemWith;

@end


@implementation PublishCollectionLayout

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _itemWith  = KScreenWidth / 3;
    }
    return self;
}


-(void)prepareLayout
{
    [super prepareLayout];
    self.itemSize = CGSizeMake(_itemWith, 30);
    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.minimumLineSpacing = 0;

    // 设置边距(让第一张图片与最后一张图片出现在最中央)
    CGFloat inset = KScreenWidth  * 0.5 - self.itemSize.width * 0.5;
    self.sectionInset = UIEdgeInsetsMake(0, inset, 0, inset);
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}


-(NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *array = [super layoutAttributesForElementsInRect:rect];
    CGFloat centerX = KScreenWidth * 0.5 + self.collectionView.contentOffset.x;
    
    for (UICollectionViewLayoutAttributes *attrs in array) {
        
        CGFloat itemCenterX = attrs.center.x;
        CGFloat scale = 1 - 0.2 * fabs(itemCenterX - centerX) / _itemWith;
        attrs.transform3D = CATransform3DMakeScale(scale, scale, 1);
        attrs.zIndex = 1;
        attrs.alpha  = 1 - 0.3 * fabs(itemCenterX - centerX) /_itemWith;
        
    }
 
    
    return array;
}



@end
