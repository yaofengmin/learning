//
//  YHJHelp.h
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YHJHelp : NSObject
+(instancetype)shareInstance;

/**
 *  主程序的window
 */
+ (UIWindow *)mainWindow;

/**
 *  判断程序是否第一次启动或者升级后第一次启动
 */
+ (BOOL)isFirstLoad;

/**
 *  是否接通网络
 */
+ (BOOL)isReachable;
/**
 *  手机型号
 *
 */
+ (NSString*)deviceString;
/**
 *  是否是手机号码
 */
+ (NSString *)valiMobile:(NSString *)mobile;


/**
 *  检查是否登录
 *
 *  @param controll 监测的controller
 *
 *  @return
 */
+ (BOOL) isLoginAndIsNetValiadWitchController:(UIViewController *)controll;

/**
 *  监测是否有摄像头
 *
 *  @return 
 */
+(BOOL)isCustom;


+(CGSize)sizeWithWidth:(CGFloat)width andFont:(UIFont*)font  andString:(NSString *)str;

//计算文字所占区域大小
+(CGSize)sizeWithText:(NSString*)text font:(UIFont*)fonnt width:(CGFloat)width height:(CGFloat)height;
/**
 *  字符串去首位空格
 *
 *  @param str 原始字符串
 *
 *  @return 去掉首位的字符串
 */
+(NSString*)trimWithStr:(NSString*)str;


/**
 *  登录环信
 */
+(void)logHuanXinAcount;

+(void)aletWithTitle:(NSString *)title Message:(NSString *)message  sureTitle:(NSString*)sureTitle CancelTitle:(NSString *)cancelTitle SureBlock:(void(^)())sure andCancelBlock:(void(^)()) cancel andDelegate:(id)controller;

/**
 微信分享

 @param controller delegate
 @param urlStr     shareUrl
 @param title      title
 @param shareText
 @param image
 @param isShowStudyGroup   是否显示分享班级功能
 @param isDIY   是否自定义分享url等
 */
-(void)showShareInController:(UIViewController *)controller andShareURL:(NSString *)urlStr andTitle:(NSString *)title andShareText:(NSString *) shareText andShareImage:(UIImage *)image isShowStudyGroup:(BOOL)isShowStudyGroup isDIYUrl:(BOOL)isDIY;
/**
 根据颜色生成图片
 
 @param color 图片颜色
 @param height 图片高度
 @return
 */
+ (UIImage*) GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height;

//比较两个日期大小
+ (int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;
/**
 小写日期转大写日期
 
 @return
 */
+(NSString *)dateUpperChange;
//后台播放设置
+(UIBackgroundTaskIdentifier)backgroundPlayerID:(UIBackgroundTaskIdentifier)backTaskId;

- (void)pushWXLaunchMiniProgramReq:(NSString *)appid;

@end
