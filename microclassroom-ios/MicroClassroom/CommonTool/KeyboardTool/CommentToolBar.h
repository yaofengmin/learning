//
//  CommentToolBar.h
//  DIYToolBar
//
//  Created by BPO on 2017/8/11.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RFTextView.h"


typedef NS_ENUM(NSInteger, ButKind)
{
	kButKindBack,
	kButKindComment,
	kButKindCollection,
	kButKindShare
};

@protocol CommentToolbarDelegate <NSObject>

@optional
- (void)inputTextViewDidBeginEditing:(RFTextView *)inputTextView;

- (void)inputTextViewWillBeginEditing:(RFTextView *)inputTextView;

- (void)didSendText:(NSString *)text;

-(void)handleItemWithSender:(UIButton *)sender;

- (void)chatToolbarDidChangeFrameToHeight:(CGFloat)toHeight;

@end

@interface CommentToolBar : UIView
@property (nonatomic, readonly) CGFloat inputViewMaxHeight;

@property (nonatomic, readonly) CGFloat inputViewMinHeight;

@property (nonatomic, readonly) CGFloat horizontalPadding;

@property (nonatomic, readonly) CGFloat verticalPadding;



@property (strong, nonatomic) UIView *toolbarView;
@property (strong, nonatomic) UIImageView *toolbarBackgroundImageView;
@property (strong, nonatomic) RFTextView *inputTextView;

/** 返回*/
@property (nonatomic, strong) UIButton *backBtn;
/** 查看评论按钮*/
@property (nonatomic, strong) UIButton *commentBtn;
/** 收藏按钮 */
@property (nonatomic, strong) UIButton *collectionBtn;
/** 分享按钮 */
@property (nonatomic, strong) UIButton *shareBtn;

@property (nonatomic,strong) UILabel *commentBadgeNum;

@property (nonatomic,assign) BOOL buttonIsHidden;
@property (nonatomic,assign) BOOL backHidden;


@property (weak, nonatomic) id<CommentToolbarDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame buttonIsHidden:(BOOL )buttonIsHidden backIsHidden:(BOOL)backHidden;//右边的按钮是否隐藏

+ (CGFloat)defaultHeight;

@end
