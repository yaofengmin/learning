//
//  CommentToolBar.m
//  DIYToolBar
//
//  Created by BPO on 2017/8/11.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import "CommentToolBar.h"


@interface CommentToolBar ()<UITextViewDelegate>
@property (nonatomic) CGFloat previousTextViewContentHeight;//上一次inputTextView的contentSize.height
@property (nonatomic) BOOL isShowButtomView;
@property (strong, nonatomic) UIView *activityButtomView;

@end
@implementation CommentToolBar

- (instancetype)initWithFrame:(CGRect)frame buttonIsHidden:(BOOL )buttonIsHidden backIsHidden:(BOOL)backHidden
{
    self = [super initWithFrame:frame];
    if (self) {
        _horizontalPadding = 8;
        _verticalPadding = 5;
        _inputViewMinHeight = 30;
        _inputViewMaxHeight = 150;
        _isShowButtomView = NO;
        _activityButtomView = nil;
        _buttonIsHidden = buttonIsHidden;
        _backHidden = backHidden;
        self.backgroundColor = [UIColor whiteColor];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatKeyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
        [self _setupSubviews];
    }
    return self;
}

- (void)_setupSubviews
{
    
    //toolbar
    _toolbarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _toolbarView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_toolbarView];
    
    //input textview
    _inputTextView = [[RFTextView alloc] init];
    _inputTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _inputTextView.scrollEnabled = YES;
    _inputTextView.returnKeyType = UIReturnKeySend;
    _inputTextView.enablesReturnKeyAutomatically = YES; // UITextView内部判断send按钮是否可以用
    _inputTextView.placeHolder = @"快来评论下吧";
    _inputTextView.font = Font(12);
    _inputTextView.textColor = IColor;
    _inputTextView.delegate = self;
    _inputTextView.backgroundColor = NCColor;
    _inputTextView.layer.cornerRadius = 4.0;
    _inputTextView.layer.masksToBounds = YES;
    
    _previousTextViewContentHeight = [self _getTextViewContentH:_inputTextView];
    
    // addSubView
    
    UIView *line = [[UIView alloc]init];
    line.backgroundColor = KColorFromRGB(0xededed);
    [self.toolbarView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.right.equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    if (!self.buttonIsHidden) {
        self.commentBtn = [self createBtn:kButKindComment action:@selector(toolbarBtnClick:)];
        self.collectionBtn = [self createBtn:kButKindCollection action:@selector(toolbarBtnClick:)];
        self.shareBtn = [self createBtn:kButKindShare action:@selector(toolbarBtnClick:)];
        [self.toolbarView addSubview:self.commentBtn];
        [self.toolbarView addSubview:self.collectionBtn];
        [self.toolbarView addSubview:self.shareBtn];
        
        self.commentBadgeNum = [[UILabel alloc]init];
        self.commentBadgeNum.textColor = BColor;
        self.commentBadgeNum.backgroundColor = NEColor;
        self.commentBadgeNum.font = Font(9);
        self.commentBadgeNum.text = @"0";
        self.commentBadgeNum.layer.cornerRadius = 12 / 2.0;
        self.commentBadgeNum.layer.masksToBounds = YES;
        self.commentBadgeNum.textAlignment = NSTextAlignmentCenter;

        [self.commentBtn addSubview:self.commentBadgeNum];
    }
    
    self.backBtn = [self createBtn:kButKindBack action:@selector(toolbarBtnClick:)];
    [self.toolbarView addSubview:self.backBtn];
    [self.toolbarView addSubview:self.inputTextView];
    
    [self setbarSubViewsFrame];
    
    
}
- (UIButton *)createBtn:(ButKind)btnKind action:(SEL)sel
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    switch (btnKind) {
        case kButKindBack:
            btn.tag = 1;
            break;
        case kButKindComment:
            btn.tag = 2;
            break;
        case kButKindCollection:
            btn.tag = 3;
            break;
        case kButKindShare:
            btn.tag = 4;
            break;
        default:
            break;
    }
    [btn addTarget:self action:sel forControlEvents:UIControlEventTouchUpInside];
    btn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    return btn;
}
#pragma mark -- 工具栏按钮点击事件
- (void)toolbarBtnClick:(UIButton *)sender
{
    switch (sender.tag) {
        case 1:
        {
            [self handelBackClick:sender];
            break;
        }
        case 2:
        {
            if ([YHJHelp isLoginAndIsNetValiadWitchController:self.viewController]) {
                
                [self handelCommentClick:sender];
            }
            break;
        }
        case 3:
        {
            if ([YHJHelp isLoginAndIsNetValiadWitchController:self.viewController]) {
                sender.selected = !sender.selected;
                [self handelCollectionClick:sender];
            }
            break;
        }
        case 4:
        {
            
            [self handelShareClick:sender];
            
            break;
        }
        default:
            break;
    }
    
}
#pragma Action
-(void)handelBackClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(handleItemWithSender:)]) {
        [self.delegate handleItemWithSender:sender];
    }
}
-(void)handelCommentClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(handleItemWithSender:)]) {
        [self.delegate handleItemWithSender:sender];
    }
}
-(void)handelCollectionClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(handleItemWithSender:)]) {
        [self.delegate handleItemWithSender:sender];
    }
}
-(void)handelShareClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(handleItemWithSender:)]) {
        [self.delegate handleItemWithSender:sender];
    }
}
-(void)setbarSubViewsFrame
{
    //返回按钮
    [self.backBtn setImage:[UIImage imageNamed:@"comment_btn_back"] forState:UIControlStateNormal];
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(0);
        make.top.equalTo(1);
        make.width.mas_equalTo(40);
    }];
    //收藏
    [self.collectionBtn setImage:[UIImage imageNamed:@"toolbar_vidoe_collect"] forState:UIControlStateNormal];
    [self.collectionBtn setImage:[UIImage imageNamed:@"We_collect_sel"] forState:UIControlStateSelected];
    [self.collectionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(44, 44));
        make.centerY.equalTo(self.backBtn);
    }];
    
    //分享按钮
    [self.shareBtn setImage:[UIImage imageNamed:@"toobar_video_share"] forState:UIControlStateNormal];
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-10);
//        make.width.mas_equalTo(30);
//        make.centerY.equalTo(self.backBtn);
        make.right.equalTo(self.collectionBtn.mas_left);
        make.size.mas_equalTo(CGSizeMake(44, 44));
        make.centerY.equalTo(self.backBtn);
    }];
    
   
    
//    评论
    [self.commentBtn setImage:[UIImage imageNamed:@"ic_comment2"] forState:UIControlStateNormal];
    [self.commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.shareBtn.mas_left);
        make.centerY.equalTo(self.shareBtn);
        make.size.mas_equalTo(CGSizeMake(44, 44));
    }];
    //小红点
    [self.commentBadgeNum mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(_commentBtn.right).offset(-10);
        make.right.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(18, 12));
        make.top.mas_equalTo(10);
    }];
    
    //输入框
    [self.inputTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (self.backHidden) {
            make.left.mas_equalTo(26);
        }else{
            make.left.equalTo(self.backBtn.mas_right);
        }
        if (self.buttonIsHidden) {
            make.right.mas_equalTo(-26);
        }else{
            make.right.equalTo(self.commentBtn.mas_left).offset(-5);
        }
        make.top.equalTo(9.5);
        make.bottom.equalTo(-(8.5 + kBottomOffSet));
    }];
    
}
#pragma mark - private bottom view

- (void)_willShowBottomHeight:(CGFloat)bottomHeight
{
    CGRect fromFrame = self.frame;
    CGFloat toHeight = self.toolbarView.frame.size.height + bottomHeight;
    CGRect toFrame = CGRectMake(fromFrame.origin.x, fromFrame.origin.y + (fromFrame.size.height - toHeight), fromFrame.size.width, toHeight);
    
    if(bottomHeight == 0 && self.frame.size.height == self.toolbarView.frame.size.height)
    {
        return;
    }
    
    if (bottomHeight == 0) {
        self.isShowButtomView = NO;
    }
    else{
        self.isShowButtomView = YES;
    }
    
    self.frame = toFrame;
    
    if (_delegate && [_delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [_delegate chatToolbarDidChangeFrameToHeight:toHeight];
    }
}

#pragma mark - private input view

- (CGFloat)_getTextViewContentH:(UITextView *)textView
{
    return ceilf([textView sizeThatFits:textView.frame.size].height);
}
- (void)_willShowInputTextViewToHeight:(CGFloat)toHeight
{
    if (toHeight < self.inputViewMinHeight) {
        toHeight = self.inputViewMinHeight;
    }
    if (toHeight > self.inputViewMaxHeight) {
        toHeight = self.inputViewMaxHeight;
    }
    
    if (toHeight == _previousTextViewContentHeight)
    {
        return;
    }
    else{
        CGFloat changeHeight = toHeight - _previousTextViewContentHeight;
        
        CGRect rect = self.frame;
        rect.size.height += changeHeight;
        rect.origin.y -= changeHeight;
        self.frame = rect;
        
        rect = self.toolbarView.frame;
        rect.size.height += changeHeight;
        self.toolbarView.frame = rect;
        
        _previousTextViewContentHeight = toHeight;
        
        if (_delegate && [_delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
            [_delegate chatToolbarDidChangeFrameToHeight:self.frame.size.height];
        }
    }
}
- (void)_willShowBottomView:(UIView *)bottomView
{
    if (![self.activityButtomView isEqual:bottomView]) {
        CGFloat bottomHeight = bottomView ? bottomView.frame.size.height : 0;
        [self _willShowBottomHeight:bottomHeight];
        
        if (bottomView) {
            CGRect rect = bottomView.frame;
            rect.origin.y = CGRectGetMaxY(self.toolbarView.frame);
            bottomView.frame = rect;
            [self addSubview:bottomView];
        }
        
        if (self.activityButtomView) {
            [self.activityButtomView removeFromSuperview];
        }
        self.activityButtomView = bottomView;
    }
}
- (void)willShowBottomView:(UIView *)bottomView
{
    [self _willShowBottomView:bottomView];
}
- (void)_willShowKeyboardFromFrame:(CGRect)beginFrame toFrame:(CGRect)toFrame
{
    if (beginFrame.origin.y == [[UIScreen mainScreen] bounds].size.height)
    {
        [self _willShowBottomHeight:toFrame.size.height];
        if (self.activityButtomView) {
            [self.activityButtomView removeFromSuperview];
        }
        self.activityButtomView = nil;
    }
    else if (toFrame.origin.y == [[UIScreen mainScreen] bounds].size.height)
    {
        [self _willShowBottomHeight:0];
    }
    else{
        [self _willShowBottomHeight:toFrame.size.height];
    }
}
#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([self.delegate respondsToSelector:@selector(inputTextViewWillBeginEditing:)]) {
        [self.delegate inputTextViewWillBeginEditing:self.inputTextView];
    }
    
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
    
    if ([self.delegate respondsToSelector:@selector(inputTextViewDidBeginEditing:)]) {
        [self.delegate inputTextViewDidBeginEditing:self.inputTextView];
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        if ([self.delegate respondsToSelector:@selector(didSendText:)]) {
            [self.delegate didSendText:textView.text];
            self.inputTextView.text = @"";
            [self _willShowInputTextViewToHeight:[self _getTextViewContentH:self.inputTextView]];
        }
        
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self _willShowInputTextViewToHeight:[self _getTextViewContentH:textView]];
}
#pragma mark - UIKeyboardNotification

- (void)chatKeyboardWillChangeFrame:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    CGRect endFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect beginFrame = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    void(^animations)() = ^{
        [self _willShowKeyboardFromFrame:beginFrame toFrame:endFrame];
    };
    
    [UIView animateWithDuration:duration delay:0.0f options:(curve << 16 | UIViewAnimationOptionBeginFromCurrentState) animations:animations completion:nil];
}
#pragma mark - public

+ (CGFloat)defaultHeight
{
    return 50 + kBottomOffSet;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    
    _delegate = nil;
    _inputTextView.delegate = nil;
    _inputTextView = nil;
}
@end
