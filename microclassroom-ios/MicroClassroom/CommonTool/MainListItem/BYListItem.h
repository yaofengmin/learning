//
//  BYSelectionView.h
//  ZQNewHomeTopC
//
//  Created by bassamyan on 15/1/18.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeGameItemModel;
typedef enum{
    top = 0,
    bottom = 1
}itemLocation;


@protocol BYListItemDelegate <NSObject>
@required

- (void)longPress;
- (void)moveWithType:(animateType) type andItemModel:(HomeGameItemModel *)ItemModel andIndext:(int) indext;
- (void)frameChangedWithHeight:(CGFloat) height;

@end

static CGFloat itemBegineLoction = 20.0f;
@interface BYListItem : UIButton
{
    @public
    NSMutableArray *locateView;
    NSMutableArray *topView;
    NSMutableArray *bottomView;
}

@property (nonatomic,weak)   id<BYListItemDelegate> delegate;
@property (nonatomic,strong) UIView   *hitTextLabel;
@property (nonatomic,strong) UIButton *deleteBtn;
@property (nonatomic,strong) UIButton *hiddenBtn;
@property (nonatomic,assign) itemLocation location;
@property(nonatomic,copy) NSString *itemName;
@property(nonatomic,strong) NSArray *allItem;
@property (nonatomic,strong) UIPanGestureRecognizer *gesture;
@property (nonatomic,strong) UILongPressGestureRecognizer *longGesture;
@property(nonatomic ,strong) HomeGameItemModel *model;
-(void)configItemeWithModel:(HomeGameItemModel *) model;
-(void)sortButtonClick;

@end
