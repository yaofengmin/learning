//
//  BYSelectionDetails.h
//  ZQNewHomeTopC
//
//  Created by bassamyan on 15/1/18.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeGameItemModel;

#define KDetailBarH  -(kScreenH - kListBarH)

@protocol BYDetailsListViewDelegate <NSObject>
@required
/**
 *  点击item回传
 *
 *  @param itemName 点击的名字
 *  @param titleAr  回传的整个数组
 */
- (void)clickItemWithModel:(HomeGameItemModel *)itemModel andReturnSourceArr:(NSArray<HomeGameItemModel*> *)topArr;

/**
 *  点击关闭按钮回传
 *
 *  @param itemName 当前选中item
 *  @param titleArr top数据源
 */
- (void)changeItemWithSelectModel:(HomeGameItemModel *)itemModel andReturnSourceArr:(NSArray<HomeGameItemModel*> *)topArr;


/**
 *  长按
 */
- (void)itemLongPress;

@end

@interface BYDetailsList : UIScrollView

@property (nonatomic,weak) id<BYDetailsListViewDelegate> detailDelegate;
@property (nonatomic,strong) NSMutableArray *topView;
@property (nonatomic,strong) NSMutableArray *bottomView;
@property (nonatomic,strong) NSMutableArray *listAll;

/**
 *  触发返回回弹
 */
-(void)detailArrowBtnClick;
-(void)itemRespondFromListBarClickWithItem:(HomeGameItemModel *)itemModel;


@end
