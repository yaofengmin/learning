//
//  BYSelectionView.m
//  ZQNewHomeTopC
//
//  Created by bassamyan on 15/1/18.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "BYListItem.h"
#import "HomeGameItemModel.h"
#import <SDWebImage/UIButton+WebCache.h>

#define kDeleteW 6
#define kItemSizeChangeAdded 0

@implementation BYListItem


- (void)configItemeWithModel:(HomeGameItemModel *) model{
    
    _model = model;
    _itemName = model.cateName;
    [self setTitle:model.cateName forState:0];
    [self setTitleColor:KColorRGB(0,0,0) forState:0];
   self.titleLabel.font = [UIFont systemFontOfSize:kItemFont];
    self.layer.cornerRadius = 4.0;
    self.layer.borderColor = KColorFromRGB(0xffa7a6) .CGColor;
    self.layer.borderWidth = 1;
    [self addTarget:self
    action:@selector(operationWithoutHidBtn:)
    forControlEvents:1<<6];
    
    self.tag = [_model.cateId integerValue];
    
    self.gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pangestureOperation:)];
    self.longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress)];
    self.longGesture.minimumPressDuration = 1;
    self.longGesture.allowableMovement = 20;
    [self addGestureRecognizer:self.longGesture];
    
    //    if (![model.cateId isEqualToString:@"-1"]&&![model.cateId isEqualToString:@"-2"]) {
    if ([model.cateId integerValue] >= 0) {
        if (!self.deleteBtn) {
            self.deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(kItemW - 15,0, 15, 15)];
            self.deleteBtn.userInteractionEnabled = NO;
            [self.deleteBtn setImage:[UIImage imageNamed:@"ic_shanchu"] forState:0];
            self.deleteBtn.hidden = YES;
            [self addSubview:self.deleteBtn];
        }
    }
    if (!self.hiddenBtn) {
        self.hiddenBtn = [[UIButton alloc] initWithFrame:self.bounds];
        self.hiddenBtn.hidden = NO;
        [self.hiddenBtn addTarget:self
                           action:@selector(operationWithHidBtn)
                 forControlEvents:1 << 6];
        [self addSubview:self.hiddenBtn];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sortButtonClick)
                                                 name:@"sortBtnClick"
                                               object:nil];
}

- (void)longPress {
    if (self.hiddenBtn.hidden == NO) {
        if ([self.delegate respondsToSelector:@selector(longPress)]) {
            [self.delegate longPress];
        }
        if (self.location == top) {
            [self addGestureRecognizer:self.gesture];
        }
    }
}

- (void)sortButtonClick {
    if (self.location == top){
        self.deleteBtn.hidden = !self.deleteBtn.hidden;
    }
    self.hiddenBtn.hidden = !self.hiddenBtn.hidden;
    if (self.gestureRecognizers) {
        [self removeGestureRecognizer:self.gesture];
    }
    if (self.hiddenBtn.hidden && self.location == top) {
        [self addGestureRecognizer:self.gesture];
    }
    
}

- (void)operationWithHidBtn {
    if (!self.hiddenBtn.hidden) {
        if (self.location == top) {
            [self setTitleColor:[UIColor redColor] forState:0];
            if ([self.delegate respondsToSelector:@selector(moveWithType:andItemModel:andIndext:)]) {
                [self.delegate moveWithType:topViewClick andItemModel:_model andIndext:0];
            }
            [self animationForWholeView];
        }
        else if (self.location == bottom){
            [self changeFromBottomToTop];
        }
    }
}

- (void)operationWithoutHidBtn:(UIButton *)send {
    
    //    if (send.tag == -1||send.tag == -2) {
    //        return;
    //    }
    if (send.tag < 0) {
        return;
    }
    
    if (self.location == top){
        [self changeFromTopToBottom];
    }
    else if (self.location == bottom) {
        self.deleteBtn.hidden = NO;
        [self addGestureRecognizer:self.gesture];
        [self changeFromBottomToTop];
    }
}

-(void)setAllItem:(NSArray *)allItem{
    _allItem = allItem;
}
- (void)pangestureOperation:(UIPanGestureRecognizer *)pan {
    
    //    if ([_model.cateId isEqualToString:@"-1"]||[_model.cateId isEqualToString:@"-2"]){
    //        return;
    //    }
    if ([_model.cateId integerValue] < 0) {
        return;
    }
    NSMutableArray *cateArr = [NSMutableArray array];
    for (HomeGameItemModel *cateModel in _allItem) {
        if ([cateModel.cateId integerValue] < 0) {
            [cateArr addObject:cateModel];
        }
    }
    [self.superview exchangeSubviewAtIndex:[self.superview.subviews indexOfObject:self] withSubviewAtIndex:[[self.superview subviews] count] - 1];
    CGPoint translation = [pan translationInView:pan.view];
    CGPoint center = pan.view.center;
    center.x += translation.x;
    center.y += translation.y;
    pan.view.center = center;
    [pan setTranslation:CGPointZero inView:pan.view];
    
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:{
            CGRect itemFrame = self.frame;
            [self setFrame:CGRectMake(itemFrame.origin.x-kItemSizeChangeAdded, itemFrame.origin.y-kItemSizeChangeAdded, itemFrame.size.width+kItemSizeChangeAdded*2, itemFrame.size.height+kItemSizeChangeAdded*2)];
        }
            break;
        case UIGestureRecognizerStateChanged:{
            BOOL InTopView = [self whetherInAreaWithArray:topView Point:center];
            if (InTopView) {
                NSInteger indexX = (center.x <= kItemW+2*homeListpadding)? 0 : (center.x - kItemW-2*homeListpadding)/(homeListpadding+kItemW) + 1;
                NSInteger indexY = (center.y <= kItemH+2*homeListpadding)? 0 : (center.y - kItemH-2*homeListpadding)/(homeListpadding+kItemH) + 1;
                NSInteger index = indexX + indexY*itemPerLine;
//                index = (index == 0 || index == 1 || index == 2 || index == 3)? 4:index;
                if (cateArr.count > 0) {
                    index = (index < cateArr.count)? cateArr.count:index;
                }
                [locateView removeObject:self];
                
                if (index > topView.count) {
                    index = topView.count;
                }
                
                [topView insertObject:self atIndex:index];
                locateView = topView;
                [self animationForTopView];
                if ([self.delegate respondsToSelector:@selector(moveWithType:andItemModel:andIndext:)]) {
                    [self.delegate moveWithType:FromTopToTop andItemModel:_model andIndext:(int)index];
                }
            }
            else if (!InTopView && center.y < [self TopViewMaxY]+50) {
                [locateView removeObject:self];
                [topView insertObject:self atIndex:topView.count];
                locateView = topView;
                [self animationForTopView];
                if ([self.delegate respondsToSelector:@selector(moveWithType:andItemModel:andIndext:)]) {
                    [self.delegate moveWithType:FromTopToTopLast andItemModel:_model andIndext:0];
                }
            }
            else if (center.y > [self TopViewMaxY]+50){
                [self changeFromTopToBottom];
            }
        }
            break;
        case UIGestureRecognizerStateEnded:
            [self animationForWholeView];
            break;
        default:
            break;
    }
}


- (void)changeFromTopToBottom {
    [locateView removeObject:self];
    [bottomView insertObject:self atIndex:0];
    locateView = bottomView;
    self.location = bottom;
    self.deleteBtn.hidden = YES;
    [self removeGestureRecognizer:self.gesture];
    
    if ([self.delegate respondsToSelector:@selector(moveWithType:andItemModel:andIndext:)]) {
        [self.delegate moveWithType:FromTopToBottomHead andItemModel:_model andIndext:0];
    }
    
    [self animationForWholeView];
}

- (void)changeFromBottomToTop {
    
    [locateView removeObject:self];
    [topView insertObject:self atIndex:topView.count];
    locateView = topView;
    self.location = top;
    if ([self.delegate respondsToSelector:@selector(moveWithType:andItemModel:andIndext:)]) {
        [self.delegate moveWithType:FromBottomToTopLast andItemModel:_model andIndext:0];
    }
    [self animationForWholeView];
}

- (BOOL)whetherInAreaWithArray:(NSMutableArray *)array Point:(CGPoint)point {
    int row = (array.count%itemPerLine == 0)? itemPerLine : array.count%itemPerLine;
    int column =  (int)(array.count-1)/itemPerLine+1;
    if ((point.x > 0 && point.x <=KScreenWidth &&point.y > 0 && point.y <= (kItemH + homeListpadding)*(column-1)+homeListpadding)||
        (point.x > 0 && point.x <= (row*(homeListpadding+kItemW)+homeListpadding)&& point.y > (kItemH + homeListpadding)*(column -1)+homeListpadding && point.y <= (kItemH+homeListpadding) * column+homeListpadding)){
        return YES;
    }
    return NO;
}

- (unsigned long)TopViewMaxY {
    unsigned long y = 0;
    y = ((topView.count-1)/itemPerLine+1)*(kItemH + homeListpadding) + homeListpadding;
    return y;
}


- (CGFloat)listViewMaxH {
    CGFloat height = 0;
    
    if (bottomView.count == 0) {
        
        height = [self TopViewMaxY] + 60;
        
    }else
    {
        height = [self TopViewMaxY] + 30 + ((bottomView.count-1)/itemPerLine+1)*(kItemH + homeListpadding) + homeListpadding;
    }
    
    return height;
}

- (void)animationForTopView{
    for (int i = 0; i < topView.count; i++){
        if ([topView objectAtIndex:i] != self){
            [self animationWithView:[topView objectAtIndex:i] frame:CGRectMake(homeListpadding+(homeListpadding+kItemW)*(i%itemPerLine), itemBegineLoction +(kItemH + homeListpadding)*(i/itemPerLine), kItemW, kItemH)];
        }
    }
}
-(void)animationForBottomView{
    for (int i = 0; i < bottomView.count; i++) {
        if ([bottomView objectAtIndex:i] != self) {
            [self animationWithView:[bottomView objectAtIndex:i] frame:CGRectMake(homeListpadding+(homeListpadding+kItemW)*(i%itemPerLine), [self TopViewMaxY]+50+(kItemH+homeListpadding)*(i/itemPerLine), kItemW, kItemH)];
        }
    }
    [self animationWithView:self.hitTextLabel frame:CGRectMake(0,[self TopViewMaxY],KScreenWidth,30)];
}

- (void)animationForWholeView{
    
    
    for (int i = 0; i <topView.count; i++) {
        
        [self animationWithView:[topView objectAtIndex:i] frame:CGRectMake(homeListpadding+(homeListpadding+kItemW)*(i%itemPerLine), itemBegineLoction+(homeListpadding+kItemH)*(i/itemPerLine), kItemW, kItemH)];
    }
    
    for (int i = 0; i < bottomView.count; i++) {
        
        [self animationWithView:[bottomView objectAtIndex:i] frame:CGRectMake(homeListpadding+(homeListpadding+kItemW)*(i%itemPerLine), [self TopViewMaxY]+50+(kItemH+homeListpadding)*(i/itemPerLine), kItemW, kItemH)];
    }
    
    
    [self animationWithView:self.hitTextLabel frame:CGRectMake(0,[self TopViewMaxY],KScreenWidth,30)];
    
    if ([self.delegate respondsToSelector:@selector(frameChangedWithHeight:)]) {
        [self.delegate frameChangedWithHeight:[self listViewMaxH]];
    }
}

-(void)animationWithView:(UIView *)view frame:(CGRect)frame{
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
        [view setFrame:frame];
    } completion:^(BOOL finished){}];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}




@end
