//
//  SelectionButton.m
//  ZQNewHomeTopC
//
//  Created by bassamyan on 15/1/18.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "BYArrow.h"

@implementation BYArrow

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setImage:[UIImage imageNamed:@"ic_quxiao"] forState:0];
        [self setImage:[UIImage imageNamed:@"ic_quxiao"] forState:1<<0];
        [self addTarget:self
                 action:@selector(ArrowClick)
       forControlEvents:1 << 6];
        self.selected = YES;
    }
    return self;
}

-(void)ArrowClick{
    
    self.selected = !self.selected;
    if (self.arrowBtnClick) {
        self.arrowBtnClick();
    }
}


@end
