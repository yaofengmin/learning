//
//  BYSelectNewBar.m
//  ZQNewHomeTopC
//
//  Created by bassamyan on 15/1/18.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "BYDeleteBar.h"

@interface BYDeleteBar()

@end

@implementation BYDeleteBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self makeNewBar];
        self.alpha= 0;
    }
    return self;
}
-(void)makeNewBar
{
    self.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(12, (self.frame.size.height - 30)/2, 80, 30)];
    label.font = [UIFont systemFontOfSize:16];
    label.textColor = KColorRGB(119, 119, 119);
    label.text = @"我的分类";
    [self addSubview:label];
    
    
    if (!self.hitText) {
        self.hitText = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label.frame),(self.frame.size.height - 10)/2, 100, 10)];
        self.hitText.font = [UIFont systemFontOfSize:11];
        self.hitText.text = @"拖拽可以排序";
        self.hitText.textColor = KColorRGB(170.0, 170.0, 170.0);
        self.hitText.hidden = YES;
        [self addSubview:self.hitText];
    }
    
    if (!self.sortBtn) {
        self.sortBtn = [[UIButton alloc] initWithFrame:CGRectMake(KScreenWidth-100, (self.frame.size.height - 30)/2, 50, 30)];
        [self.sortBtn setTitle:@"编辑" forState:0];
        [self.sortBtn setTitleColor:KColorRGB(0, 0, 0) forState:0];
        self.sortBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.sortBtn addTarget:self
                         action:@selector(sortBtnClick:)
               forControlEvents:1<<6];
        [self addSubview:self.sortBtn];
    }
    
}

-(void)sortBtnClick:(UIButton *)sender{
    if (sender.selected) {
        [sender setTitle:@"排序" forState:0];
        self.hitText.hidden = YES;
    }
    else{
        [sender setTitle:@"完成" forState:0];
        self.hitText.hidden = NO;
    }
    sender.selected = !sender.selected;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sortBtnClick"
                                                        object:sender
                                                      userInfo:nil];
}

@end
