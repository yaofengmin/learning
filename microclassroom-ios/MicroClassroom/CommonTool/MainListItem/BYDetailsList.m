//
//  BYSelectionDetails.m
//  ZQNewHomeTopC
//
//  Created by bassamyan on 15/1/18.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "BYDetailsList.h"
#import "BYListItem.h"
#import "HomeGameItemModel.h"

@interface BYDetailsList()
<
BYListItemDelegate
>


@property (nonatomic,strong) UIView *sectionHeaderView;

@property (nonatomic,strong) NSMutableArray *allItems;

@property (nonatomic,strong) BYListItem *itemSelect;


@end


@implementation BYDetailsList


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.9];
    }
    return self;
}


-(void)setListAll:(NSMutableArray *)listAll{
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    [self.topView removeAllObjects];
    [self.bottomView removeAllObjects];
    _listAll = listAll;
    self.alwaysBounceVertical = YES;
    self.showsVerticalScrollIndicator = NO;
    self.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
    NSArray *listTop = listAll[0];
    NSArray *listBottom = listAll[1];
    
#pragma 更多频道标签
    self.sectionHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0,homeListpadding+(homeListpadding + kItemH)*((listTop.count -1)/itemPerLine+1),KScreenWidth, 30)];
    
    UILabel *moreText = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 100, 30)];
    moreText.text = @"更多分类";
    moreText.textColor = KColorRGB(119, 119, 119);
    moreText.font = [UIFont systemFontOfSize:16];
    [self.sectionHeaderView addSubview:moreText];
    [self addSubview:self.sectionHeaderView];
    
    NSMutableArray *allItem = [NSMutableArray array];
    [allItem addObjectsFromArray:listTop];
    [allItem addObjectsFromArray:listBottom];
    
    NSInteger count1 = listTop.count;
    for (int i =0; i <count1; i++) {
        BYListItem *item = [[BYListItem alloc] initWithFrame:CGRectMake(homeListpadding+(homeListpadding+kItemW)*(i% itemPerLine), itemBegineLoction +(kItemH + homeListpadding)*(i/itemPerLine), kItemW, kItemH)];
  
        item.delegate = self;
        [item configItemeWithModel:listTop[i]];
        item.location = top;
        item.allItem = [NSArray arrayWithArray:allItem];
        [self.topView addObject:item];
        item->locateView = self.topView;
        item->topView = self.topView;
        item->bottomView = self.bottomView;
        item.hitTextLabel = self.sectionHeaderView;
        [self addSubview:item];
        [self.allItems addObject:item];

        
        if (!self.itemSelect) {
            [item setTitleColor:[UIColor redColor] forState:0];
            self.itemSelect = item;
        }
    }
    
    NSInteger count2 = listBottom.count;
    for (int i=0; i<count2; i++) {
        BYListItem *item = [[BYListItem alloc] initWithFrame:CGRectMake(homeListpadding+(homeListpadding+kItemW)*(i%itemPerLine),CGRectGetMaxY(self.sectionHeaderView.frame)+homeListpadding+(kItemH+homeListpadding)*(i/itemPerLine), kItemW, kItemH)];
        item.delegate = self;
        [item configItemeWithModel:listBottom[i]];
        item.location = bottom;
        item.allItem = [NSArray arrayWithArray:allItem];
        item.hitTextLabel = self.sectionHeaderView;
        [self.bottomView addObject:item];
        item->locateView = self.bottomView;
        item->topView = self.topView;
        item->bottomView = self.bottomView;
        [self addSubview:item];
        [self.allItems addObject:item];

    }
    
    self.contentSize = CGSizeMake(KScreenWidth, CGRectGetMaxY(self.sectionHeaderView.frame)+homeListpadding+(kItemH+homeListpadding)*((count2-1)/4+1));
}

-(void)itemRespondFromListBarClickWithItem:(HomeGameItemModel *)itemModel{
    for (int i = 0 ; i<self.allItems.count; i++) {
        BYListItem *item = (BYListItem *)self.allItems[i];
        if ([item.model.cateId isEqualToString:itemModel.cateId]) {
            [self.itemSelect setTitleColor:KColorRGB(0,0,0) forState:0];
            [item setTitleColor:kNavigationBarColor forState:0];
            self.itemSelect = item;
        }else{
            
        }
    }
}



#pragma mark === 改变数据源的方法 ====
- (void)operationFromBlock:(animateType)type itemModel:(HomeGameItemModel *)itemModel index:(int)index {
    switch (type) {
        case topViewClick: {
            if ([self.detailDelegate respondsToSelector:@selector(clickItemWithModel:andReturnSourceArr:)]) {
                
                [self.detailDelegate clickItemWithModel:itemModel andReturnSourceArr:[self getModelArrWithSourceArr:self.topView]];
            }
        }
            break;
        case FromTopToTop: {
                //改变事件 通过返回方法延时响应
        }
            break;
        case FromTopToTopLast: {

            //改变事件返回方法延时响应
        }
            break;
        case FromTopToBottomHead: {
            //改变事件返回方法延时响应
        }

            break;
        case FromBottomToTopLast: {
            //改变事件返回方法延时响应
        }
          
            break;
        default:
            break;
    }
}


- (void)detailArrowBtnClick {
    if ([self.detailDelegate respondsToSelector:@selector(changeItemWithSelectModel:andReturnSourceArr:)]) {
        
        [self.detailDelegate changeItemWithSelectModel:self.itemSelect.model andReturnSourceArr:[self getModelArrWithSourceArr:self.topView]];
        
    }
    
    // 关闭的时候复位位置
    DELAYEXECUTE(0.4, self.contentOffset = CGPointMake(0, 0));
    
}


- (NSMutableArray *)allItems{
    if (_allItems == nil) {
        _allItems = [NSMutableArray array];
    }
    return _allItems;
}

- (NSMutableArray *)topView{
    if (_topView == nil) {
        _topView = [NSMutableArray array];
    }
    return _topView;
}

- (NSMutableArray *)bottomView {
    if (_bottomView == nil) {
        _bottomView = [NSMutableArray array];
    }
    return _bottomView;
}



- (NSInteger)findIndexOfListsWithTitle:(NSString *)title andSourceArr:(NSArray *)arr {
    for (int i =0; i < arr.count; i++) {
        if ([title isEqualToString:arr[i]]) {
            return i;
        }
    }
    
    return 0;
}


- (NSArray <HomeGameItemModel *> *)getModelArrWithSourceArr:(NSArray<BYListItem*> *)arr {
    NSMutableArray *muarr = @[].mutableCopy;
    for (BYListItem *item in arr) {
        
        [muarr addObject:item.model];
    }
    
    return muarr;
}

#pragma mark ==BYListItemDelegate===

- (void)longPress {
    if ([self.detailDelegate respondsToSelector:@selector(itemLongPress)]) {
        [self.detailDelegate itemLongPress];
    }
}


- (void)moveWithType:(animateType) type andItemModel:(HomeGameItemModel *)itemModel  andIndext:(int) indext {
    
    [self operationFromBlock:type itemModel:itemModel  index:indext];
}


#pragma mark == 改变显示的contentSize ===
- (void)frameChangedWithHeight:(CGFloat) height {
    
    self.contentSize = CGSizeMake(KScreenWidth, height);
   
}




@end
