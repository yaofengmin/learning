
//
//  THPlayer.m
//  THPlayer
//
//  Created by inveno on 16/3/23.
//  Copyright © 2016年 inveno. All rights reserved.
//AVAsset：主要用于获取多媒体信息，是一个抽象类，不能直接使用。
//AVURLAsset：AVAsset的子类，可以根据一个URL路径创建一个包含媒体信息的AVURLAsset对象。
//AVPlayerItem：一个媒体资源管理对象，管理者视频的一些基本信息和状态，一个AVPlayerItem对应着一个视频资源。
//

#import "HTAudioPlayer.h"
#import "Color+Hex.h"

#import "BuyVIPViewController.h"

static void *PlayViewStatusObservationContext = &PlayViewStatusObservationContext;

@interface HTAudioPlayer()

//播放器
@property(nonatomic,retain)AVPlayer *player;

/* playItem */
@property(nonatomic, retain) AVPlayerItem *currentItem;
@property(nonatomic, retain)NSDateFormatter *dateFormatter;
@property(strong, nonatomic)NSTimer *handleBackTime;
@property(assign, nonatomic)BOOL isTouchDownProgress;

@end

@implementation HTAudioPlayer

NSString *const kAudioHTPlayerStartPlayNotificationKey  = @"com.hotoday.kAudioPlayerStartPlayNotificationKey";
NSString *const kAudioHTPlayerFinishedPlayNotificationKey  = @"com.hotoday.kAudioPlayerFinishedPlayNotificationKey";
NSString *const kAudioHTPlayerCloseVideoNotificationKey    = @"com.hotoday.kAudioPlayerCloseVideoNotificationKey";
NSString *const kAudioHTPlayerCloseDetailVideoNotificationKey    = @"com.hotoday.kHTPlayerCloseDetailVideoNotificationKey";
NSString *const kAudioHTPlayerPopDetailNotificationKey = @"com.hotoday.kAduioPlayerPopDetailNotificationKey";
NSString *const kAudioHTPlayerPauseVideoNotificationKey    = @"com.hotoday.kAudioPlayerPauseVideoNotificationKey";

+ (instancetype)defaultManager
{
    static HTAudioPlayer *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

//根据url 获取播放信息
-(AVPlayerItem *)getPlayItemWithURLString:(NSString *)urlString{
    if ([urlString containsString:@"http"]) {
        AVPlayerItem *playerItem=[AVPlayerItem playerItemWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        return playerItem;
    }else{
        AVAsset *movieAsset  = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:urlString] options:nil];
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:movieAsset];
        return playerItem;
    }
}


- (instancetype)initWithFrame:(CGRect)frame videoURLStr:(NSString *)videoURLStr{
    if (self) {
        self.videoURLStr = videoURLStr;
    }
    
    return self;
}

- (void)addObserverFromPlayerItem:(AVPlayerItem *)playerItem{
    //       监听播放状态的变化
    [playerItem addObserver:self
                 forKeyPath:@"status"
                    options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                    context:PlayViewStatusObservationContext];
    //监控网络加载情况属性
    [playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
    
}

-(void)removeObserverFromPlayerItem:(AVPlayerItem *)playerItem{
    [playerItem removeObserver:self forKeyPath:@"status"];
    [playerItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
}

/**
 *  通过KVO监控播放器状态
 *
 *  @param keyPath 监控属性
 *  @param object  监视器
 *  @param change  状态改变
 *  @param context 上下文
 */
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    //    AVPlayerItem *playerItem=object;
    
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status= [[change objectForKey:@"new"] intValue];
        if(status==AVPlayerStatusReadyToPlay){
            
            //            设置进度条最大value= 视频总时长
            if (CMTimeGetSeconds(self.player.currentItem.duration)) {
                self.progressSlider.maximumValue = CMTimeGetSeconds(self.player.currentItem.duration);
            }
            [self initTimer];
            if (self.status)self.status(UIAduioHTPlayerStatusReadyToPlayTyep);
            KPostNotifiCation(kAudioHTPlayerStartPlayNotificationKey,nil);
        }
    }else if([keyPath isEqualToString:@"loadedTimeRanges"]){
        
        if (self.alpha == 0.00) {
            if (self.status)self.status(UIAduioHTPlayeStatusrLoadedTimeRangesType);
            [UIView animateWithDuration:0.5 animations:^{
                self.alpha = 1.0;
            }];
        }
    }
}

#pragma  maik - 定时器 视频进度
-(void)initTimer{
    
    __weak typeof(self) weakSelf = self;
    double interval = .1f;
    CMTime playerDuration = [self playerItemDuration];
    //    if (CMTIME_IS_INVALID(playerDuration))
    //    {
    //        return;
    //    }
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration))
    {
        CGFloat width = CGRectGetWidth([self.progressSlider bounds]);
        interval = 0.5f * duration / width;
    }
    //    定时循环执行。
    [weakSelf.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval *0.1, NSEC_PER_SEC)  queue:NULL
                                             usingBlock:^(CMTime time){
                                                 [weakSelf syncScrubber];
                                             }];
}

- (void)syncScrubber{
    
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration)){
        self.progressSlider.minimumValue = 0.0; //设置进度为0
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration)){
        float minValue = [self.progressSlider minimumValue];
        float maxValue = [self.progressSlider maximumValue];
        double time = CMTimeGetSeconds([self.player currentTime]);
        if (time < 0) {
            return;
        }
        _leftTimeLabel.text = [NSString stringWithFormat:@"%@",[self convertTime:time]];
        _totalTime.text = [NSString stringWithFormat:@"%@",[self convertTime:duration]];
        float value = (maxValue - minValue) * time / duration + minValue;
        if (!_isTouchDownProgress) {
            [self.progressSlider setValue:value];
        }
    }
}

//获取播放时长。
- (CMTime)playerItemDuration{
    AVPlayerItem *playerItem = [self.player currentItem];
    if (playerItem.status == AVPlayerItemStatusReadyToPlay){
        return([playerItem duration]);
    }
    return(kCMTimeInvalid);
}

//转换时间
- (NSString *)convertTime:(CGFloat)second{
    NSDate *d = [NSDate dateWithTimeIntervalSince1970:second];
    if (second/3600 >= 1) {
        [[self dateFormatter] setDateFormat:@"HH:mm:ss"];
    } else {
        [[self dateFormatter] setDateFormat:@"mm:ss"];
    }
    NSString *newTime = [[self dateFormatter] stringFromDate:d];
    return newTime;
}

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
    }
    return _dateFormatter;
}


//获取视频播放的时间
- (double)duration{
    AVPlayerItem *playerItem = self.player.currentItem;
    if (playerItem.status == AVPlayerItemStatusReadyToPlay){
        return CMTimeGetSeconds([[playerItem asset] duration]);
    }
    else{
        return 0.f;
    }
}

- (double)currentTime{
    return CMTimeGetSeconds([[self player] currentTime]);
}

- (void)setCurrentTime:(double)time{
    [[self player] seekToTime:CMTimeMakeWithSeconds(time, 1)];
}

#pragma mark - PlayOrPause
- (void)PlayOrPause:(UIButton *)sender{
    if (self.canPlay == 1) {
        @weakify(self);
        [YHJHelp aletWithTitle:@"提示" Message:@"您还不是VIP会员，不能观看" sureTitle:@"立即开通 " CancelTitle:@"忽略 " SureBlock:^{
            @strongify(self);
            BuyVIPViewController *buy = [[BuyVIPViewController alloc]init];
            [self.viewController.navigationController pushViewController:buy animated:YES];
            
        } andCancelBlock:nil andDelegate:self.viewController];
        return;
    }
    sender.selected = !sender.selected;
    if (self.player.rate != 1.f) {
        if ([self currentTime] == [self duration])
            [self setCurrentTime:0.f];
        [self.player play];
        if ([self duration] == 0.0) {
            return;
        }
        //        NSString *time = [NSString stringWithFormat:@"%f",[self duration]];
        KPostNotifiCation(kAudioHTPlayerStartPlayNotificationKey, nil);
    } else {
        [self.player pause];
        KPostNotifiCation(kAudioHTPlayerPauseVideoNotificationKey, nil);
    }
}

- (void)play
{
    self.playBtn.selected = YES;
    [self.player play];;
    
}
-(void)stopPlay
{
    self.playBtn.selected = NO;
    [self.player pause];
}
#pragma mark - 单击手势方法
- (void)removeHandleBackTime {
    if (self.handleBackTime) {
        [self.handleBackTime invalidate];
        self.handleBackTime = nil;
    }
}

#pragma mark - 双击手势方法
- (void)handleDoubleTap{
    [self PlayOrPause:_playBtn];
}


#pragma mark - 设置播放的视频
- (void)seekTime:(double)time {
    [self.player seekToTime:CMTimeMakeWithSeconds(time, 1)];
}

- (void)setVideoURLStr:(NSString *)videoURLStr
{
    self.alpha = 0;
    _videoURLStr = videoURLStr;
    
    if (self.currentItem) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_currentItem];
        [self removeObserverFromPlayerItem:self.currentItem];
        self.currentItem = nil;
    }
    self.currentItem = [self getPlayItemWithURLString:videoURLStr];
    
    [self addObserverFromPlayerItem:self.currentItem];
    //    切换视频
    [self.player replaceCurrentItemWithPlayerItem:self.currentItem];
    
    // 添加视频播放结束通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:_currentItem];
    
    if (self.player == nil) {
        self.player = [AVPlayer playerWithPlayerItem:self.currentItem];
        if(IOS10){
            self.player.automaticallyWaitsToMinimizeStalling = NO;
        }
        //        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
        //        self.playerLayer.frame = self.layer.bounds;
        //        if ([_playerLayer superlayer] == nil)[self.layer addSublayer:_playerLayer];
    }
    
    self.playBtn.selected = NO;
    [self bringSubviewToFront:self.playBtn];
    if (self.player.rate != 1.f) {
        if ([self currentTime] == self.duration){
            [self setCurrentTime:0.f];
        }
        
        [self PlayOrPause:self.playBtn];
        
    }
    
    if (self.status)self.status(UIAduioHTPlayerStatusLoadingType);
    
}

- (void)moviePlayDidEnd:(NSNotification *)notification {
    __weak typeof(self) weakSelf = self;
    [self.player seekToTime:kCMTimeZero completionHandler:^(BOOL finished) {
        
        [weakSelf.progressSlider setValue:0.0 animated:YES];
        weakSelf.playBtn.selected = NO;
        //播放完成后的通知
        [[NSNotificationCenter defaultCenter] postNotificationName:kAudioHTPlayerFinishedPlayNotificationKey object:nil];
    }];
}
//#pragma mark - 播放进度
- (void)updateProgress:(UISlider *)slider{
    [self.player seekToTime:CMTimeMakeWithSeconds(slider.value, 1)];
    _isTouchDownProgress = NO;
}
- (void)changeProgress:(UISlider *)slider{
    _isTouchDownProgress = YES;
}
- (void)TouchBeganProgress:(UISlider *)slider{
    [self removeHandleBackTime];
}

-(void)dealloc{
    [self releaseWMPlayer];
    if(_handleBackTime) [_handleBackTime invalidate];
    _handleBackTime = nil;
}

-(void)releaseWMPlayer{
    [self.currentItem removeObserver:self forKeyPath:@"status"];
    [self.currentItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
    
    [self.player.currentItem cancelPendingSeeks];
    [self.player.currentItem.asset cancelLoading];
    [self.player pause];
    [self removeFromSuperview];
    [self.playerLayer removeFromSuperlayer];
    [self.player replaceCurrentItemWithPlayerItem:nil];
    self.player = nil;
    self.currentItem = nil;
    self.playBtn = nil;
    self.playerLayer = nil;
    
}

- (IBAction)playPauseAction:(UIButton *)sender {
    [self PlayOrPause:sender];
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self.progressSlider setThumbImage:[UIImage imageNamed:@"slider_thumb"] forState:UIControlStateNormal];
    self.progressSlider.userInteractionEnabled = NO;
    self.progressSlider.minimumTrackTintColor =  NEColor;
    self.progressSlider.value = 0.0;//指定初始值
    self.progressSlider.frame = CGRectMake(0, 0, KScreenWidth - 159, 30);
    
    //    // slider开始滑动事件
    //    [self.progressSlider addTarget:self action:@selector(TouchBeganProgress:) forControlEvents:UIControlEventTouchDown];
    //    // slider滑动中事件
    //    [self.progressSlider addTarget:self action:@selector(changeProgress:) forControlEvents:UIControlEventValueChanged];
    //    // slider结束滑动事件
    //    [self.progressSlider addTarget:self action:@selector(updateProgress:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel | UIControlEventTouchUpOutside];
}



@end

