//
//  HTAudioPlayer.h
//  yanshan
//
//  Created by BPO on 2017/8/21.
//  Copyright © 2017年 BPO. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MediaPlayer;
@import AVFoundation;

typedef NS_ENUM(NSInteger, UIHTAudioPlayerStatusChangeType) {
	UIAduioHTPlayerStatusLoadingType          = 0, //正在加载
	UIAduioHTPlayerStatusReadyToPlayTyep      = 1,//开始播放
	UIAduioHTPlayeStatusrLoadedTimeRangesType = 2//开始缓存
};

extern NSString *const kAudioHTPlayerFinishedPlayNotificationKey; //播放完成通知
extern NSString *const kAudioHTPlayerPauseVideoNotificationKey; //播放暂停通知
extern NSString *const kAudioHTPlayerStartPlayNotificationKey; //开始播放通知



typedef void (^AduioPlayerStatusChange) (UIHTAudioPlayerStatusChangeType status);
@interface HTAudioPlayer : UIView
@property (weak, nonatomic) IBOutlet UILabel *leftTimeLabel;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UILabel *totalTime;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;

@property(nonatomic,retain)AVPlayerLayer *playerLayer;
@property (strong, nonatomic)AduioPlayerStatusChange status;//播放状态
@property(nonatomic,copy) NSString *videoURLStr;//播放地址

@property (assign ,nonatomic) NSInteger canPlay;// 标记是否有权限播放  0 可以放  1 不能放

+ (instancetype)defaultManager;
- (id)initWithFrame:(CGRect)frame videoURLStr:(NSString *)videoURLStr;

- (void)play;

-(void)releaseWMPlayer;

- (void)seekTime:(double)time;
- (double)currentTime;

-(void)stopPlay;
//转换时间
- (NSString *)convertTime:(CGFloat)second;
@end
