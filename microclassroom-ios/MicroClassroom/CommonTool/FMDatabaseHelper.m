//
//  FMDatabaseHelper.m
//  toolstest
//
//  Created by dqf on 12-7-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FMDatabaseHelper.h"
#import "FileHelper.h"
#import "FMDatabaseAdditions.h"


@implementation FMDatabaseHelper



-(void)dealloc
{
	if(mDb!=nil){
        [mDb close];
        [mDb release];
    }
    [super dealloc];
}

//初始化并打开db
-(id)initWith:(NSString*)dbName
{
	self = [super init];
    NSString* path = [NSString stringWithFormat:@"%@/%@",[FileHelper getDocPath],dbName];
    if(self!=nil){
        mDb = [[FMDatabase databaseWithPath:path] retain];
        if(mDb!=nil)
            [mDb open];
    }
  //[Log print:@"FMDatabaseHelper" msg:@"db path=%@",path];
    return self;
}

//创建表
-(BOOL)createTableWith:(NSString*)tableName args:(NSDictionary*)args
{
	BOOL ret=NO; 
    
    if(mDb==nil){
    	//[Log print:@"FMDatabaseHelper" msg:@"create table fail, db is nil"];
        return NO;
    }
    
    if(args==nil || tableName==nil){
    //[Log print:@"FMDatabaseHelper" msg:@"create table fail, parms error"];
    	return NO;
    }
    
    NSString* sql;
    NSMutableString* parms=[[NSMutableString alloc] init];
    int i=0;
    //@"create table collection (puid text, area text, road text)
    for (NSString* key in [args keyEnumerator]) {
        if(i==0)
        	[parms appendFormat:@"%@ %@",key,[args valueForKey:key]];
        else {
            [parms appendFormat:@", %@ %@",key,[args valueForKey:key]];
        }
        i++;
    }
    
    sql = [NSString stringWithFormat:@"create table %@ (%@)",tableName,parms];
    
  // [Log print:@"FMDatabaseHelper" msg:sql];
    
    ret = [mDb executeUpdate:sql];
    [parms release];
    return ret;
}

-(BOOL)createbysql:(NSString*)sql
{
    if(mDb==nil){
        //[Log print:@"FMDatabaseHelper" msg:@"create table fail, db is nil"];
        return NO;
    }
    
    [mDb executeUpdate:sql];
    return YES;
}

-(BOOL)updatebysql:(NSString*)sql
{

    if(mDb==nil){
       // [Log print:@"FMDatabaseHelper" msg:@"update table fail, db is nil"];
        return NO;
    }
    //[Log print:@"FMDatabaseHelper" msg:sql];
     [mDb executeUpdate:sql];
    return YES;
}




-(BOOL)createTableWith:(NSString*)tableName args:(NSDictionary*)args mainKey:(NSString*)mainKey
{
    BOOL ret=NO; 
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"create table fail, db is nil"];
        return NO;
    }
    
    if(args==nil || tableName==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"create table fail, parms error"];
    	return NO;
    }
    
    NSString* sql;
    NSMutableString* parms=[[NSMutableString alloc] init];
    int i=0;

    //@"create table collection (puid text, area text, road text)
    for (NSString* key in [args keyEnumerator]) {
        if(i==0){
            if([mainKey isEqualToString:key])
                [parms appendFormat:@"%@ %@ NOT NULL primary key",key,[args valueForKey:key]];
            else
        		[parms appendFormat:@"%@ %@",key,[args valueForKey:key]];
        }
        else {
            if([mainKey isEqualToString:key])
                [parms appendFormat:@", %@ %@ NOT NULL primary key",key,[args valueForKey:key]];
            else
            	[parms appendFormat:@", %@ %@",key,[args valueForKey:key]];
        }
        i++;
    }
    
    sql = [NSString stringWithFormat:@"create table %@ (%@)",tableName,parms];
    
   //[Log print:@"FMDatabaseHelper" msg:sql];
    
    ret = [mDb executeUpdate:sql];
    [parms release];
    return ret;

}


//删除表
-(BOOL)deleteTableWith:(NSString*)tableName
{
	BOOL ret=NO;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"delete table fail, db is nil"];
        return NO;
    }
    if(tableName==nil)
        return NO;
    
    [mDb beginTransaction];
    NSString *sql=[NSString stringWithFormat:@"drop table %@;",tableName];
    ret = [mDb executeUpdate:sql];
    [mDb commit];
//    [Log print:@"FMDatabaseHelper" msg:sql];
    return ret;
}


//查询表是否存在
-(BOOL)checkTableExist:(NSString*)tableName
{
	BOOL ret=NO;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"check table fail, db is nil"];
        return NO;
    }
    if(tableName==nil)
        return NO;

    ret = [mDb tableExists:tableName];
    return ret;
}



-(BOOL)checkIetmExist:(NSString*)tableName wheres:(NSDictionary*)wheres
{
    BOOL ret=NO;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"checkIetmExist table fail, db is nil"];
        return NO;
    }
    if(wheres==nil || tableName==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"checkIetmExist table fail, parms error"];
    	return NO;
    }
    NSMutableString* whereKey=[[NSMutableString alloc] init];
    NSMutableArray * whereVal=[[NSMutableArray alloc] init];
    int i=0;
    NSString *sql ;//= [NSString stringWithFormat:@"select * from %@ where %@ = ?",tableName,key];
    
    for (NSString* key in [wheres keyEnumerator]) {
        if(i==0){
            [whereKey appendFormat:@"%@ = ?",key];
            [whereVal addObject:[wheres valueForKey:key]];
        }
        else {
            [whereKey appendFormat:@"and %@ = ?",key];   
 
            [whereVal addObject:[wheres valueForKey:key]];
        }
        i++;

    }
    sql = [NSString stringWithFormat:@"select * from %@ where %@",tableName,whereKey];
    FMResultSet *rs=[mDb executeQuery:sql withArgumentsInArray:whereVal];
    ret = [rs next];
    [rs close];
    [whereKey release];
    [whereVal release];
    
//    [Log print:@"FMDatabaseHelper" msg:sql];
    return ret;

}

//插入一条记录到表中
-(BOOL)insertTo:(NSString*)tableName args:(NSDictionary*)args
{
	BOOL ret=NO;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"insert table fail, db is nil"];
        return NO;
    }
    if(args==nil || tableName==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"insert table fail, parms error"];
    	return NO;
    }
    
    //@"insert into nulltest2 (s, d, i, f, b) values (?, ?, ?, ?, ?)"
    NSString* sql;
    NSMutableString* parms1=[[NSMutableString alloc] init];
    NSMutableString* parms2=[[NSMutableString alloc] init];
    NSMutableArray* parms3=[[NSMutableArray alloc] init];
    int i=0;
    for (NSString* key in [args keyEnumerator]) {
        if(i==0){
            [parms1 appendFormat:@"%@",key,[args valueForKey:key]];
            [parms2 appendFormat:@"?"];
            [parms3 addObject:[args valueForKey:key]];
        }
        else {
            [parms1 appendFormat:@", %@",key,[args valueForKey:key]];   
            [parms2 appendFormat:@", ?"];
            [parms3 addObject:[args valueForKey:key]];
        }
        i++;
    }
	sql = [NSString stringWithFormat:@"insert into %@ (%@) values (%@)",tableName,parms1,parms2];
    
    
    ret = [mDb executeUpdate:sql withArgumentsInArray:parms3];
    
    
    [parms1 release];
    [parms2 release];
    [parms3 release];
    
  // [Log print:@"FMDatabaseHelper" msg:sql];
    return ret;
}
-(BOOL)insertAutoCommitToSql:(NSMutableArray*)sql
{
    BOOL ret=NO,ret2=NO;
    if(mDb==nil)
        return NO;
    
    ret=[mDb beginTransaction];
    if(!ret)
        return ret;
    for(int i=0;i<sql.count;i++){
        ret = [mDb executeUpdate:sql[i]];
       // MyLog(@"%@",sql[i]);
    }
    
    ret2=[mDb commit];
    
    return (ret & ret2);
}
//auto commit
-(BOOL)insertAutoCommitTo:(NSString*)tableName args:(NSDictionary*)args
{
    BOOL ret=NO,ret2=NO;
    if(mDb==nil)
        return NO;
    
	ret=[mDb beginTransaction];
    if(!ret)
        return ret;
    ret = [self insertTo:tableName args:args];
    ret2=[mDb commit];
    
    return (ret & ret2);
}


//从表中删除一条记录
-(BOOL)deleteFrom:(NSString *)tableName wheres:(NSDictionary *)wheres
{
	BOOL ret=NO;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"delete item fail, db is nil"];
        return NO;
    }
    if(wheres==nil || tableName==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"delete item  fail, parms error"];
    	return NO;
    }
    
    NSMutableString* whereKey=[[NSMutableString alloc] init];
    NSMutableArray * whereVal=[[NSMutableArray alloc] init];
    int i=0;
    NSString *sql ;
    
    for (NSString* key in [wheres keyEnumerator]) {
        if(i==0){
            [whereKey appendFormat:@"%@ = ?",key];
            [whereVal addObject:[wheres valueForKey:key]];
        }
        else {
            [whereKey appendFormat:@"and %@ = ?",key];   
            
            [whereVal addObject:[wheres valueForKey:key]];
        }
        i++;
        
    }
    
    sql = [NSString stringWithFormat:@"delete from %@ where %@",tableName,whereKey];
    ret = [mDb executeUpdate:sql withArgumentsInArray:whereVal];
    
    [whereKey release];
    [whereVal release];
    
  // [Log print:@"FMDatabaseHelper" msg:sql];
    return ret;
}

//auto commit
-(BOOL)deleteFromAutoCommit:(NSString *)tableName wheres:(NSDictionary *)wheres
{
    BOOL ret=NO,ret2=NO;
    if(mDb==nil)
        return NO;
    
    ret=[mDb beginTransaction];
    if(!ret)
        return ret;
    ret = [self deleteFrom:tableName wheres:wheres];
    ret2=[mDb commit];
    
    return (ret & ret2);
}


//更新表中的一条记录
-(BOOL)update:(NSString *)tableName wheres:(NSDictionary *)wheres args:(NSDictionary *)args
{
	BOOL ret=NO;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"update table fail, db is nil"];
        return NO;
    }

    if(wheres==nil || tableName==nil || args==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"update item  fail, parms error"];
    	return NO;
    }
    
    NSMutableString* setKey=[[NSMutableString alloc] init];
    NSMutableString* whereKey=[[NSMutableString alloc] init];
    NSMutableArray * Vals=[[NSMutableArray alloc] init];
    int i=0;
    NSString *sql ;
    
    for (NSString* key in [args keyEnumerator]) {
        if(i==0){
            [setKey appendFormat:@"%@ = ?",key];
            [Vals addObject:[args valueForKey:key]];
        }
        else {
            [setKey appendFormat:@", %@ = ?",key];   
            
            [Vals addObject:[args valueForKey:key]];
        }
        i++;
        
    }
    
    i=0;
    for (NSString* key in [wheres keyEnumerator]) {
        if(i==0){
            [whereKey appendFormat:@"%@ = ?",key];
            [Vals addObject:[wheres valueForKey:key]];
        }
        else {
            [whereKey appendFormat:@"and %@ = ?",key];   
            
            [Vals addObject:[wheres valueForKey:key]];
        }
        i++;
        
    }
    
    
    sql = [NSString stringWithFormat:@"update %@ set %@ where %@",tableName,setKey,whereKey];

    ret = [mDb executeUpdate:sql withArgumentsInArray:Vals];

    
    [whereKey release];
    [setKey release];
    [Vals release];
    
//    [Log print:@"FMDatabaseHelper" msg:sql];
    return ret;
}

//auto commit
-(BOOL)updateAutoCommit:(NSString*)tableName wheres:(NSDictionary*)wheres args:(NSDictionary*)args
{
    BOOL ret=NO,ret2=NO;
    if(mDb==nil)
        return NO;
    
	ret=[mDb beginTransaction];
    if(!ret)
        return ret;
    ret = [self update:tableName wheres:wheres args:args];
    ret2=[mDb commit];
    
    return (ret & ret2);
}


//读取表的内容
-(FMResultSet*) select:(NSString*)tableName wheres:(NSDictionary*)wheres 
{
	FMResultSet *rs=nil;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"select table fail, db is nil"];
        return nil;
    }
    
	if(tableName==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"select fail, parms error"];
    	return nil;
    }
    
    if(wheres==nil){
    	rs = [mDb executeQuery:[NSString stringWithFormat:@"select * from %@",tableName]];
        return rs;
    }
    
    NSMutableString* whereKey=[[NSMutableString alloc] init];
    NSMutableArray * whereVal=[[NSMutableArray alloc] init];
    int i=0;
    NSString *sql ;//= [NSString stringWithFormat:@"select * from %@ where %@ = ?",tableName,key];
    
    for (NSString* key in [wheres keyEnumerator]) {
        if(i==0){
            [whereKey appendFormat:@"%@ = ?",key];
            [whereVal addObject:[wheres valueForKey:key]];
        }
        else {
            [whereKey appendFormat:@"and %@ = ?",key];   
            
            [whereVal addObject:[wheres valueForKey:key]];
        }
        i++;
        
    }
    
    sql = [NSString stringWithFormat:@"select * from %@ where %@",tableName,whereKey];
    rs=[mDb executeQuery:sql withArgumentsInArray:whereVal];
    
    [whereKey release];
    [whereVal release];
    
//    [Log print:@"FMDatabaseHelper" msg:sql];
    return rs;
}
//直接执行ＳＱＬ
-(FMResultSet*) selectbysql:(NSString*)sql
{
    FMResultSet *rs=nil;
    if(mDb==nil){
//            	[Log print:@"FMDatabaseHelper" msg:@"select table fail, db is nil"];
        return nil;
    }
    rs=[mDb executeQuery:sql withArgumentsInArray:nil];
//    [Log print:@"FMDatabaseHelper" msg:sql];
    return rs;
}

//从某行开始查询固定条数据
-(FMResultSet*) select:(NSString*)tableName startLine:(NSString *)aStartLine lines:(NSString *)totalLines
{
	FMResultSet *rs=nil;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"select table fail, db is nil"];
        return nil;
    }
    
	if(tableName==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"select fail, parms error"];
    	return nil;
    }
    
    NSString *sql = [NSString stringWithFormat:@"select * from %@ limit %@,%@",tableName,aStartLine,totalLines];
    rs=[mDb executeQuery:sql withArgumentsInArray:nil];
    
    return rs;
}
//数据库里总共有多少条数据
- (int) select:(NSString*)tableName
{
	FMResultSet *rs=nil;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"select table fail, db is nil"];
        return 0;
    }
    
	if(tableName==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"select fail, parms error"];
    	return NO;
    }
    
    NSString *sql = [NSString stringWithFormat:@"select count(*) from %@",tableName];
    rs=[mDb executeQuery:sql withArgumentsInArray:nil];
    
    if (rs) {
        return rs.columnCount;
    }
    
    return 0;
}

-(FMResultSet*)select:(NSString *)tableName wheres:(NSDictionary *)wheres orderKey:(NSString *)order forword:(int)f
{
	FMResultSet *rs=nil;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"select table fail, db is nil"];
        return nil;
    }
    
	if(tableName==nil){
//        [Log print:@"FMDatabaseHelper" msg:@"select fail, parms error"];
    	return nil;
    }
    
    if(wheres==nil && order==nil){
    	rs = [mDb executeQuery:[NSString stringWithFormat:@"select * from %@",tableName]];
        return rs;
    }
    
    if(wheres==nil && order!=nil){
        if(f==0)//升序
    		rs = [mDb executeQuery:[NSString stringWithFormat:@"select * from %@ order by %@",tableName,order]];
        else {//降序
            rs = [mDb executeQuery:[NSString stringWithFormat:@"select * from %@ order by %@ desc",tableName,order]];
        }
        return rs;
    }
    
    NSMutableString* whereKey=[[NSMutableString alloc] init];
    NSMutableArray * whereVal=[[NSMutableArray alloc] init];
    int i=0;
    NSString *sql ;//= [NSString stringWithFormat:@"select * from %@ where %@ = ?",tableName,key];
    
    for (NSString* key in [wheres keyEnumerator]) {
        if(i==0){
            [whereKey appendFormat:@"%@ = ?",key];
            [whereVal addObject:[wheres valueForKey:key]];
        }
        else {
            [whereKey appendFormat:@"and %@ = ?",key];   
            
            [whereVal addObject:[wheres valueForKey:key]];
        }
        i++;
        
    }
    if(order!=nil){
        if(f==0)
            sql = [NSString stringWithFormat:@"select * from %@ where %@ order by %@",tableName,whereKey,order];
        else
            sql = [NSString stringWithFormat:@"select * from %@ where %@ order by %@ desc",tableName,whereKey,order];
    }else {
        sql = [NSString stringWithFormat:@"select * from %@ where %@",tableName,whereKey];
    }
    
    rs=[mDb executeQuery:sql withArgumentsInArray:whereVal];
    
    [whereKey release];
    [whereVal release];
    
//    [Log print:@"FMDatabaseHelper" msg:sql];
    return rs;
}


-(BOOL)clearTable:(NSString*)tableName
{
	BOOL ret=NO;
    
    if(mDb==nil){
//    	[Log print:@"FMDatabaseHelper" msg:@"clear table content, db is nil"];
        return NO;
    }
    if(tableName==nil)
        return NO;

    NSString *sql=[NSString stringWithFormat:@"delete from %@;",tableName];
    ret = [mDb executeUpdate:sql];
    
//    [Log print:@"FMDatabaseHelper" msg:sql];
    return ret;
}

-(BOOL)clearTableAutoCommit:(NSString*)tableName
{
    BOOL ret=NO,ret2=NO;
    if(mDb==nil)
        return NO;
    
    ret=[mDb beginTransaction];
    if(!ret)
        return ret;
    ret = [self clearTable:tableName];
    ret2=[mDb commit];
    
    return (ret & ret2);
}


- (BOOL)rollback
{
	if(mDb!=nil)
        return [mDb rollback];
    else {
        return NO;
    }
}


- (BOOL)commit
{
    if(mDb!=nil)
        return [mDb commit];
    else {
        return NO;
    }
}

- (BOOL)beginTransaction
{
	if(mDb!=nil)
        return [mDb beginTransaction];
    else {
        return NO;
    }
}

@end
