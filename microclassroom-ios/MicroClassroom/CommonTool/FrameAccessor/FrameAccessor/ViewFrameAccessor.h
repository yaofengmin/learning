//
//  ViewFrameAccessor.h
//  ViewFrameAccessor
//
//  Created by Alex Denisov on 18.03.12.
//  Copyright (c) 2013 okolodev.org. All rights reserved.
//


#define IS_IOS_DEVICE (TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE)

#if IS_IOS_DEVICE
    #import <UIKit/UIKit.h>
    #define View UIView
#else
    #import <Foundation/Foundation.h>
    #define View NSView
#endif


@interface View (FrameAccessor)

// Frame
@property (nonatomic) CGPoint viewOrigin;
@property (nonatomic) CGSize viewSize;

// Frame Origin
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;

// Frame Size
@property (nonatomic) CGFloat viewWidth;
@property (nonatomic) CGFloat viewHeight;

// Frame Borders
@property (nonatomic) CGFloat viewTop;
@property (nonatomic) CGFloat viewLeft;
@property (nonatomic) CGFloat viewBottom;
@property (nonatomic) CGFloat viewRight;

// Center Point
#if !IS_IOS_DEVICE
@property (nonatomic) CGPoint viewCenter;
#endif
@property (nonatomic) CGFloat viewCenterX;
@property (nonatomic) CGFloat viewCenterY;

// Middle Point
@property (nonatomic, readonly) CGPoint middlePoint;
@property (nonatomic, readonly) CGFloat middleX;
@property (nonatomic, readonly) CGFloat middleY;

@end
