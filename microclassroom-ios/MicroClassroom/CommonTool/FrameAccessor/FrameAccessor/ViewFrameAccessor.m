//
//  ViewFrameAccessor.m
//  ViewFrameAccessor
//
//  Created by Alex Denisov on 18.03.12.
//  Copyright (c) 2013 okolodev.org. All rights reserved.
//

#import "ViewFrameAccessor.h"


@implementation View (FrameAccessor)

#pragma mark Frame

- (CGPoint)viewOrigin
{
    return self.frame.origin;
}

- (void)setViewOrigin:(CGPoint)newOrigin
{
    CGRect newFrame = self.frame;
    newFrame.origin = newOrigin;
    self.frame = newFrame;
}

- (CGSize)viewSize
{
    return self.frame.size;
}

- (void)setViewSize:(CGSize)newSize
{
    CGRect newFrame = self.frame;
    newFrame.size = newSize;
    self.frame = newFrame;
}


#pragma mark Frame Origin

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (void)setX:(CGFloat)newX
{
    CGRect newFrame = self.frame;
    newFrame.origin.x = newX;
    self.frame = newFrame;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (void)setY:(CGFloat)newY
{
    CGRect newFrame = self.frame;
    newFrame.origin.y = newY;
    self.frame = newFrame;
}


#pragma mark Frame Size

- (CGFloat)viewHeight
{
    return self.frame.size.height;
}

- (void)setViewHeight:(CGFloat)viewHeight
{
    CGRect newFrame = self.frame;
    newFrame.size.height = viewHeight;
    self.frame = newFrame;
}

- (CGFloat)viewWidth
{
    return self.frame.size.width;
}

- (void)setViewWidth:(CGFloat)newWidth
{
    CGRect newFrame = self.frame;
    newFrame.size.width = newWidth;
    self.frame = newFrame;
}


#pragma mark Frame Borders

- (CGFloat)viewLeft
{
    return self.x;
}

- (void)setViewLeft:(CGFloat)left
{
    self.x = left;
}

- (CGFloat)viewRight
{
    return self.frame.origin.x + self.frame.size.width;
}

- (void)setViewRight:(CGFloat)right
{
    self.x = right - self.viewWidth;
}

- (CGFloat)viewTop
{
    return self.y;
}

- (void)setViewTop:(CGFloat)top
{
    self.y = top;
}

- (CGFloat)viewBottom
{
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setViewBottom:(CGFloat)bottom
{
    self.y = bottom - self.viewHeight;
}


#pragma mark Center Point

#if !IS_IOS_DEVICE
- (CGPoint)center
{
    return CGPointMake(self.left + self.middleX, self.top + self.middleY);
}

- (void)setCenter:(CGPoint)newCenter
{
    self.left = newCenter.x - self.middleX;
    self.top = newCenter.y - self.middleY;
}
#endif

- (CGFloat)viewCenterX
{
    return self.center.x;
}

- (void)setViewCenterX:(CGFloat)newCenterX
{
    self.center = CGPointMake(newCenterX, self.center.y);
}

- (CGFloat)viewCenterY
{
    return self.center.y;
}

- (void)setViewCenterY:(CGFloat)newCenterY
{
    self.center = CGPointMake(self.center.x, newCenterY);
}


#pragma mark Middle Point

- (CGPoint)middlePoint
{
    return CGPointMake(self.middleX, self.middleY);
}

- (CGFloat)middleX
{
    return self.viewWidth / 2;
}

- (CGFloat)middleY
{
    return self.viewHeight / 2;
}

@end
