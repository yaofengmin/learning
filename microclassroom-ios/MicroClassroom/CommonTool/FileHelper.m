//
//  FileHelper.m
//  toolstest
//
//  Created by wyfly on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FileHelper.h"

@implementation FileHelper


//获取应用的docment目录
+(NSString*)getDocPath
{
	NSString* mDocPath=nil;
    mDocPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    return mDocPath;
}

//创建文件夹,可以递归创建
+(BOOL)createDir:(NSString*)path
{
    BOOL ret=NO;
	if(path!=nil){
        
        if([self isExist:path])
            return YES;
        
    	ret = [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    if(!ret)
        MyLog(@"create dir fail,path=%@",path);
    return ret;
}

//创建文件,指定的文件夹必须存在，否则创建失败.fileName为绝对路径
+(BOOL)createFile:(NSString*)fileName
{
	BOOL ret=NO;
    if(fileName!=nil){
        if([self isExist:fileName])
            return YES;
    	ret = [[NSFileManager defaultManager] createFileAtPath:fileName contents:nil attributes:nil];
    }
    if(!ret)
        MyLog(@"create file fail,filenme=%@",fileName);
    return ret;
}

//检索指定文件夹下的文件，可指定是否递归检索
+(NSArray*)enumPath:(NSString*)path isRecursion:(BOOL)isRecursion
{
	if(path!=nil){
        if(isRecursion)
    		return [[NSFileManager defaultManager] subpathsAtPath:path];
        else
            return [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    }else {
        return nil;
    }
}

//判断文件/文件夹是否存在
+(BOOL)isExist:(NSString*)path
{
	BOOL ret=NO;
    BOOL isdir=NO;
    ret = [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isdir];
   // MyLog(@"file or dir is exist=%d isdir=%d path=%@",ret,isdir,path);
    return ret;
}

//删除文件/文件夹
+(BOOL)removeItem:(NSString*)path
{
	BOOL ret=NO;
    if(path!=nil){
    	ret = [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
    MyLog(@"remove path=%@",path);
    return ret;
}

//获取文件/文件夹属性
+(NSDictionary*)getAttributes:(NSString*)path
{
	if(path!=nil){
        return [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
    }else {
        return nil;
    }
}

//重命名、移动文件/文件夹
+(BOOL)rename:(NSString*)srcPath toPath:(NSString*)dstPath
{
	BOOL ret=NO;
    if(srcPath==nil || dstPath==nil)
        return ret;
    
    ret = [[NSFileManager defaultManager] moveItemAtPath:srcPath toPath:dstPath error:nil];
    return ret;
}

//复制文件/文件夹
+(BOOL)copyItem:(NSString*)srcPath toPath:(NSString*)dstPath
{
	BOOL ret = NO;
    if(srcPath==nil || dstPath==nil)
        return ret;
    NSError* err=nil;
    ret = [[NSFileManager defaultManager] copyItemAtPath:srcPath toPath:dstPath error:&err];
    if(err){
    	MyLog(@"%@",err.userInfo);
    }

    return ret;
}

//获取整个文件夹的大小
+(unsigned long long)getDirSize:(NSString*)path
{
	NSDirectoryEnumerator *files=nil;
    unsigned long long size=0;
    NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
    NSFileManager* defaultmgr=[NSFileManager defaultManager];
    
    if(path==nil)
        return 0;
    
    files = [defaultmgr enumeratorAtPath:path];
    for (NSString* file in files) {
        NSString* filepath=[NSString stringWithFormat:@"%@/%@",path,file];
        NSDictionary *attr = [defaultmgr attributesOfItemAtPath:filepath error:nil];
        size+=attr.fileSize;
    }
    
    [pool release];

	return size;
}


//获取文件的大小
+(unsigned long long)getFileSize:(NSString*)file
{  
    if(file==nil)
        return 0;
    
   return[[NSFileManager defaultManager] attributesOfItemAtPath:file error:nil].fileSize;

}

//文件追加UTF8编码字符
+(BOOL)appendUTF8:(NSString*)path str:(NSString*)str
{
//    BOOL ret=NO;
//    if(![FileHelper isExist:path])
//        return [str writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
//    
//	NSOutputStream* output = [[NSOutputStream alloc] initToFileAtPath:path append:YES];
//
//    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
//    const void *bytes = [data bytes];
//    int length = [data length];
//    
//    if([output write:(const uint8_t*)bytes maxLength:length]==length)
//        ret = YES;
//    else {
//        ret = NO;
//    }
//    
//    [output close];
//    [output release];
    
//the code on the up does work when to write something to a file by append
    if (![FileHelper isExist:path]) {
        return NO;
    }
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    
    [fileHandle seekToEndOfFile];
    [fileHandle writeData:data];
    MyLog(@"bytepath=%@",path);
    
    return YES;
}


+(NSArray*)enumPath:(NSString*)vagueFileName fileDir:(NSString*)fileDir
{
	NSArray* array=[self enumPath:fileDir isRecursion:NO];
    NSMutableArray* arr=[[[NSMutableArray alloc] init] autorelease];
    if(array && array.count>0){
    	for (NSString* file in array) {
            NSRange range;
            range = [file rangeOfString:vagueFileName];
            if(range.length>0)
                [arr addObject:file];
        }
    }else {
        return nil;
    }
    
    if(arr.count>0)
	    return arr;
	else 
    	return nil;

}

@end
