//
//  YHJHelp.m
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHJHelp.h"
#import "sys/utsname.h"
#import <AVFoundation/AVFoundation.h>
#import "LoginViewController.h"
#import "YHNavgationController.h"
#import <AFNetworkReachabilityManager.h>
#import "UserInfoModel.h"
//#import "UMSocial.h"
//#import "UMSocialWechatHandler.h"
#import <UShareUI/UShareUI.h>
#import <WechatAuthSDK.h>
#import <WXApi.h>
#import <WXApiObject.h>

static NSString *WLastVersion = @"last_run_version_of_application";
@interface YHJHelp ()
@property (nonatomic,weak) UIViewController *vc;
@property (nonatomic,copy) NSString *url;
@property (nonatomic,copy) NSString *sc_title;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,assign) BOOL isDIY;

@end
@implementation YHJHelp
+(instancetype)shareInstance
{
    return [[super alloc]init];
}
+ (UIWindow *)mainWindow{
    return [UIApplication sharedApplication].delegate.window;
}


#pragma mark 是否是第一次启动新版本
+ (BOOL) isFirstLoad{
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary]
                                objectForKey:(NSString*)kCFBundleVersionKey];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastRunVersion = [defaults objectForKey:WLastVersion];
    if (!lastRunVersion) {
        [defaults setObject:currentVersion forKey:WLastVersion];
        return YES;
    }else if (![lastRunVersion isEqualToString:currentVersion]) {
        [defaults setObject:currentVersion forKey:WLastVersion];
        return YES;
    }
    
    return NO;
}



+ (BOOL)isReachable{
    return [AFNetworkReachabilityManager sharedManager].isReachable;
}


-(void)showShareInController:(UIViewController *)controller andShareURL:(NSString *)urlStr andTitle:(NSString *)title andShareText:(NSString *) shareText andShareImage:(UIImage *)image isShowStudyGroup:(BOOL)isShowStudyGroup isDIYUrl:(BOOL)isDIY
{
    NSString *desStr = shareText;
    NSString *destitle = title;
    if (desStr.length > 100) {
        desStr = [desStr substringWithRange:NSMakeRange(0, 100)];
    }
    
    if (destitle.length > 10) {
        destitle = [destitle substringWithRange:NSMakeRange(0, 10)];
    }
    self.vc = controller;
    self.url = urlStr;
    self.sc_title = title;
    self.content = shareText;
    self.isDIY = isDIY;

    if (isShowStudyGroup) {
        [UMSocialUIManager addCustomPlatformWithoutFilted:UMSocialPlatformType_UserDefine_Begin+2 withPlatformIcon:[UIImage imageNamed:@"login_icon"] withPlatformName:@"班级"];
    }
   
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        //在回调里面获得点击的
        if (platformType == UMSocialPlatformType_UserDefine_Begin + 2) {//班级
            KPostNotifiCation(@"ShareFileToStudyGroup", nil);
        }else{
            [self shareWebPageToPlatformType:platformType ];
        }
    }];
}

- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UIImage *thumbImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[InitConfigModel shareInstance].logoUrl]]];
    NSString *title = @"";
    NSString *content = @"";
    if (self.isDIY) {
        if (platformType == UMSocialPlatformType_WechatTimeLine) {
            title = self.content;
            content = @"";
        }else{
            title = self.sc_title;
            content = self.content;
        }
    }else{
        title = @"学习兴业";
        content = @"APP下载地址";
    }
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:content thumImage:thumbImage];
    //设置网页地址
    if (self.isDIY) {
        shareObject.webpageUrl = self.url;
    }else{
        shareObject.webpageUrl = Micro_ITUNES;
    }
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self.vc completion:^(id data, NSError *error) {
        if (error) {
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
        MyLog(@"%@",error);
    }];
}

- (void)pushWXLaunchMiniProgramReq:(NSString *)appid
{
    WXLaunchMiniProgramReq *launchMiniProgramReq = [WXLaunchMiniProgramReq object];
    launchMiniProgramReq.userName = [InitConfigModel shareInstance].wxGhId;
//    @"gh_d769e39b9e65";  //拉起的小程序的username
    launchMiniProgramReq.path = [NSString stringWithFormat:@"%@?appid=%@",[InitConfigModel shareInstance].wxStartPage,appid];  ////拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
    launchMiniProgramReq.miniProgramType = WXMiniProgramTypeRelease; //拉起小程序的类型
     [WXApi sendReq:launchMiniProgramReq completion:nil];
}

#pragma mark == 登录环信帐号===
+ (void)logHuanXinAcount {
    
    if (NOEmptyStr(KgetUserValueByParaName(USERID))) {
        
        BOOL isAutoLogin = [EMClient sharedClient].options.isAutoLogin;
        if (!isAutoLogin) {
            MyLog(@"环信账号:%@ --- 密码:%@ ---- 未加密密码:%@",[UserInfoModel getInfoModel].hxAccount.username,[NSString md5:KgetUserValueByParaName(USERSecret)],KgetUserValueByParaName(USERSecret));
            [[EMClient sharedClient] loginWithUsername:[UserInfoModel getInfoModel].hxAccount.username password:[NSString md5:KgetUserValueByParaName(USERSecret)] completion:^(NSString *aUsername, EMError *aError) {
                
                EMPushOptions *options = [[EMClient sharedClient] getPushOptionsFromServerWithError:&aError];
                options.displayStyle   = EMPushDisplayStyleMessageSummary;
                
                [[EMClient sharedClient] setApnsNickname:[UserInfoModel getInfoModel].nickname];
                [[EMClient sharedClient] updatePushNotificationOptionsToServerWithCompletion:nil];
                [[EMClient sharedClient].options setIsAutoLogin:YES];
            }];
        }
        
    }
}


+ (NSString*)deviceString
{
    // 需要#import "sys/utsname.h"
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G";
    
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone 6s";
    
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus";
    
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    
    if ([platform isEqualToString:@"iPhone9,1"]) return @"iPhone 7";
    
    if ([platform isEqualToString:@"iPhone9,2"]) return @"iPhone 7 Plus";
    
    if ([platform isEqualToString:@"iPod1,1"])  return @"iPod Touch 1G";
    
    if ([platform isEqualToString:@"iPod2,1"])  return @"iPod Touch 2G";
    
    if ([platform isEqualToString:@"iPod3,1"])  return @"iPod Touch 3G";
    
    if ([platform isEqualToString:@"iPod4,1"])  return @"iPod Touch 4G";
    
    if ([platform isEqualToString:@"iPod5,1"])  return @"iPod Touch 5G";
    
    if ([platform isEqualToString:@"iPad1,1"])  return @"iPad 1G";
    
    if ([platform isEqualToString:@"iPad2,1"])  return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,2"])  return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,3"])  return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,4"])  return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,5"])  return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,6"])  return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,7"])  return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad3,1"])  return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,2"])  return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,3"])  return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,4"])  return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,5"])  return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,6"])  return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad4,1"])  return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,2"])  return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,3"])  return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,4"])  return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,5"])  return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,6"])  return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"i386"]) return @"iPhone Simulator";
    
    if ([platform isEqualToString:@"x86_64"])  return @"iPhone Simulator";
    
    return platform;
    
}

+ (NSString *)valiMobile:(NSString *)mobile{
    if (mobile.length != 11)
    {
        return @"手机号长度只能是11位";
    }
    //    else{
    //        /**
    //         * 移动号段正则表达式
    //         */
    //        NSString *CM_NUM = @"^((13[4-9])|(147)|(15[0-2,7-9])|(178)|(18[2-4,7-8]))\\d{8}|(1705)\\d{7}$";
    //        /**
    //         * 联通号段正则表达式
    //         */
    //        NSString *CU_NUM = @"^((13[0-2])|(145)|(15[5-6])|(176)|(18[5,6]))\\d{8}|(1709)\\d{7}$";
    //        /**
    //         * 电信号段正则表达式
    //         */
    //        NSString *CT_NUM = @"^((133)|(153)|(177)|(18[0,1,9]))\\d{8}$";
    //        NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM_NUM];
    //        BOOL isMatch1 = [pred1 evaluateWithObject:mobile];
    //        NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU_NUM];
    //        BOOL isMatch2 = [pred2 evaluateWithObject:mobile];
    //        NSPredicate *pred3 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT_NUM];
    //        BOOL isMatch3 = [pred3 evaluateWithObject:mobile];
    //
    //        if (isMatch1 || isMatch2 || isMatch3) {
    //            return nil;
    //        }else{
    //            return @"请输入正确的电话号码";
    //        }
    //    }
    return nil;
}

//判断是否有开启相机
+(BOOL)isCustom{
    BOOL Custom= [UIImagePickerController
                  isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];//判断摄像头是否能用
    
    if (Custom) {
        //  IOS 7 以上判断
        if(IOS7){
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied)
            { //无权限
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                message:@"没有访问权限,请先在隐私-相机中设置"
                                                               delegate:nil
                                                      cancelButtonTitle:@"确定"
                                                      otherButtonTitles:nil];
                [alert show];
                
                return NO;
                
            }else{
                
                return YES;
            }
            
        }else{
            
            return YES;
            
        }
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"没有检测到摄像头"
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    
}

+(CGSize)sizeWithWidth:(CGFloat)width andFont:(UIFont*)font  andString:(NSString *)str
{
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    
    return size;
}


+ (BOOL) isLoginAndIsNetValiadWitchController:(UIViewController *)controll
{
    
    //    if (![YHJHelp isReachable]) {
    //
    //        [WFHudView showMsg:@"请检查网络设置" inView:nil];
    //        return NO;
    //    }
    
    NSString *userid = KgetUserValueByParaName(USERID);
    if (IsEmptyStr(userid)) {
        LoginViewController *log = [[LoginViewController alloc]init];
        YHNavgationController *logNav = [[YHNavgationController alloc]initWithRootViewController:log];
        [controll presentViewController:logNav animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}


//计算文字所占区域大小
+(CGSize)sizeWithText:(NSString*)text font:(UIFont*)fonnt width:(CGFloat)width height:(CGFloat)height{
    NSDictionary *attribute = @{NSFontAttributeName: fonnt};
    return [text boundingRectWithSize:CGSizeMake(width, height) options:
            NSStringDrawingTruncatesLastVisibleLine |
            NSStringDrawingUsesLineFragmentOrigin |
            NSStringDrawingUsesFontLeading attributes:attribute
                              context:nil].size;
}

+(NSString*)trimWithStr:(NSString*)str{
    NSString *tmp = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return [tmp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+(void)aletWithTitle:(NSString *)title Message:(NSString *)message  sureTitle:(NSString*)sureTitle CancelTitle:(NSString *)cancelTitle SureBlock:(void(^)())sure andCancelBlock:(void(^)()) cancel andDelegate:(id)controller{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (cancel) {
            cancel();
        }
    }]];
    
    if (sureTitle) {
        [alert addAction:[UIAlertAction actionWithTitle:sureTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (sure) {
                sure();
            }
        }]];
    }
    [controller presentViewController:alert animated:YES completion:nil];
    
}

/**
 *  生成图片
 *
 *  @param color  图片颜色
 *  @param height 图片高度
 *
 *  @return 生成的图片
 */
+ (UIImage*) GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height
{
    CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, r);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

+ (int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy-HH:mm"];
    NSString *oneDayStr = [dateFormatter stringFromDate:oneDay];
    NSString *anotherDayStr = [dateFormatter stringFromDate:anotherDay];
    NSDate *dateA = [dateFormatter dateFromString:oneDayStr];
    NSDate *dateB = [dateFormatter dateFromString:anotherDayStr];
    NSComparisonResult result = [dateA compare:dateB];
    //    NSLog(@"date1 : %@, date2 : %@", oneDay, anotherDay);
    if (result == NSOrderedDescending) {
        //        NSLog(@"Date1  is in the future");
        return 1;
    }
    else if (result == NSOrderedAscending){
        //        NSLog(@"Date1 is in the past");
        return -1;
    }
    //    NSLog(@"Both dates are the same");
    return 0;
    
}

+(NSString *)dateUpperChange
{
    
    NSArray *upper = @[@"零",@"一",@"二",@"三",@"四",@"五",@"六",@"七",@"八",@"九",@"十"];
    //根据小写数字格式的日期转换成大写格式的日期
    NSDateFormatter* formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *time = [formatter stringFromDate:[NSDate date]];
    if (time.length == 0) {
        return @"";
    }
    time = [time stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if (time.length != 8) {
        return @"";
    }
    NSMutableString *mString = [[NSMutableString alloc]init];
    for (int i = 0; i < 4; i ++) {//年
        NSString *str = [time substringWithRange:NSMakeRange(i, 1)];
        NSInteger intStr = [str integerValue];
        [mString appendString:upper[intStr]];
    }
    //    [mString appendString:@"年"];//拼接年
    //月
    NSInteger month = [[time substringWithRange:NSMakeRange(4, 2)] integerValue];
    if (month <= 10) {
        [mString appendString:upper[month]];
    }else{
        [mString appendString:@"十"];
        [mString appendString:upper[month % 10]];
    }
    [mString appendString:@"月"];
    
    //日
    NSInteger day = [[time substringWithRange:NSMakeRange(6, 2)] integerValue];
    if (day <= 10) {
        [mString appendString:upper[day]];
    }else if (day < 20){
        [mString appendString:@"十"];
        [mString appendString:upper[day % 10]];
    }else{
        [mString appendString:upper[day / 10]];
        [mString appendString:@"十"];
        NSInteger tmp = day % 10;
        if (tmp != 0) {
            [mString appendString:upper[tmp]];
        }
    }
    [mString appendString:@"号"];
    
    return mString;
}

//这样做，可以在按home键进入后台后 ，播放一段时间，几分钟吧。但是不能持续播放网络歌曲，若需要持续播放网络歌曲，还需要申请后台任务id，具体做法是：
//其中的_bgTaskId是后台任务UIBackgroundTaskIdentifier _bgTaskId;
//}
//实现一下backgroundPlayerID:这个方法:
+(UIBackgroundTaskIdentifier)backgroundPlayerID:(UIBackgroundTaskIdentifier)backTaskId
{
    //设置并激活音频会话类别
    AVAudioSession *session=[AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    [session setActive:YES error:nil];
    //允许应用程序接收远程控制
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    //设置后台任务ID
    UIBackgroundTaskIdentifier newTaskId=UIBackgroundTaskInvalid;
    newTaskId=[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
    if(newTaskId!=UIBackgroundTaskInvalid&&backTaskId!=UIBackgroundTaskInvalid)
    {
        [[UIApplication sharedApplication] endBackgroundTask:backTaskId];
    }
    return newTaskId;
}
@end
