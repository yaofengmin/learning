//
//  FJSlidingController.h
//  FJSlidingController
//
//  Created by fujin on 15/12/17.
//  Copyright © 2015年 fujin. All rights reserved.
//

#import "YHBaseViewController.h"
#import "SegmentTapView.h"
@protocol FJSlidingControllerDataSource;
@protocol FJSlidingControllerDelegate;

@interface FJSlidingController : YHBaseViewController
@property (nonatomic, assign)id<FJSlidingControllerDataSource> datasouce;
@property (nonatomic, assign)id<FJSlidingControllerDelegate> delegate;
@property (nonatomic, strong) SegmentTapView *segmentTapView;
@property (nonatomic, strong) UIPageViewController *pageController;
@property (nonatomic, assign) CGFloat segTop;
@property (nonatomic,assign) CGFloat lineW;
@property (nonatomic,assign) BOOL isShowLine;

-(void)reloadData;
-(void)selectedIndex:(NSInteger)index;
@end

@protocol FJSlidingControllerDataSource <NSObject>
@required
// pageNumber
- (NSInteger)numberOfPageInFJSlidingController:(FJSlidingController *)fjSlidingController;
// index -> UIViewController
- (UIViewController *)fjSlidingController:(FJSlidingController *)fjSlidingController controllerAtIndex:(NSInteger)index;
// index -> Title
- (NSString *)fjSlidingController:(FJSlidingController *)fjSlidingController titleAtIndex:(NSInteger)index;

@optional
// textNomalColor
- (UIColor *)titleNomalColorInFJSlidingController:(FJSlidingController *)fjSlidingController;
// textSelectedColor
- (UIColor *)titleSelectedColorInFJSlidingController:(FJSlidingController *)fjSlidingController;
// lineColor
- (UIColor *)lineColorInFJSlidingController:(FJSlidingController *)fjSlidingController;
// titleFont
- (CGFloat)titleFontInFJSlidingController:(FJSlidingController *)fjSlidingController;
@end

@protocol FJSlidingControllerDelegate <NSObject>
@optional
// selctedIndex
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedIndex:(NSInteger)index;
// selectedController
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedController:(UIViewController *)controller;
// selectedTitle
- (void)fjSlidingController:(FJSlidingController *)fjSlidingController selectedTitle:(NSString *)title;
@end
