//
//  GpsManager.m
//  MicroClassroom
//
//  Created by Hanks on 16/8/9.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "GpsManager.h"
#import <CoreLocation/CoreLocation.h>



static GpsManager *gps = nil;

@interface GpsManager ()
<
CLLocationManagerDelegate
>
@property(nonatomic ,copy) void(^succuss)(CGFloat lng, CGFloat lat , NSDictionary *addressDic) ;
@property(nonatomic ,copy) void(^failed)();
@property(nonatomic, assign) CLLocationCoordinate2D userCoordinate;
@property(nonatomic, strong) CLGeocoder  *geocoder;
@property(nonatomic ,strong) CLLocationManager * locationManager;

@end

@implementation GpsManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUpLocationManager];
    }
    return self;
}


+(void)getLoctationSuccuss:(void(^)( CGFloat lng, CGFloat lat , NSDictionary *addressDic)) succuss andFaild:(void(^)()) faild
{
    gps = [[GpsManager alloc]init];
    if (succuss) {
        gps.succuss = succuss;
    }
    
    if (faild) {
        gps.failed =faild;
    }
}


- (void)setUpLocationManager {
    
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager requestWhenInUseAuthorization];
    _geocoder = [[CLGeocoder alloc]init];
    
    [self startLoaction];
    
}


- (void)startLoaction
{
    if([CLLocationManager locationServicesEnabled]){
        
        _locationManager.distanceFilter  = kCLDistanceFilterNone;    //距离筛选器，单位米（移动多少米才回调更新）
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest; //精确度
        [_locationManager setDelegate:self];
        [_locationManager startUpdatingLocation];
        
    }
    
}


#pragma mark – CLLocationManagerDelegate
//定位成功回调
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    _userCoordinate = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
    [_locationManager stopUpdatingLocation];
    
    @weakify(self);
    [_geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        @strongify(self);
        // 如果解析结果的集合元素的个数大于1，表明解析得到了经度、纬度信息
        if (placemarks.count > 0) {
            // 只处理第一个解析结果，实际项目可使用列表让用户选择
            CLPlacemark* placemark = placemarks[0];
            // 获取详细地址信息
            gps.succuss(self.userCoordinate.longitude,self.userCoordinate.latitude,placemark.addressDictionary);
            self.locationManager.delegate = nil;
        }
        // 没有得到解析结果。
        else {
            
            // 使用UIAlertView提醒用户
//            [[[UIAlertView alloc] initWithTitle:@"提醒"
//              
//                                        message:@"您输入的地址无法解析" delegate:nil
//              
//                              cancelButtonTitle:@"确定" otherButtonTitles: nil]
//             show];
        }
    }];
}

//定位失败回调
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    gps.failed();
    _locationManager.delegate = nil;
}



@end
