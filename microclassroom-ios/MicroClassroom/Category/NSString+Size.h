//
//  NSString+Size.h
//  MicroClassroom
//
//  Created by DEMO on 16/8/9.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Size)
- (CGSize)sizeWithFontSize:(CGFloat)fontSize andRectSize:(CGSize)size;
@end
