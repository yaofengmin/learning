//
//  NSString+MD5.h
//  TZS
//
//  Created by yandi on 14/12/9.
//  Copyright (c) 2014年 NongFuSpring. All rights reserved.
//


@interface NSString (MD5)

+ (NSString *)md5:(NSString *)originalStr;

+ (NSString *)CurrentTime1970;

+ (NSString *)userLocWith:(double) uerLog andLat:(double) userLat PublisLocWith:(double) Plog andLat:(double) Plat;

+ (NSString *)TimeToCurrentTime:(NSInteger)time;

+ (NSString *)CurrentTimeByStrHaveSecond:(BOOL)have;

/**
 *  当前剩余时间
 *
 *  @param time 剩余时间字符串(秒)
 *
 *  @return 返回剩余时间字符串
 */
+(NSString *)lessSecondToDay:(NSUInteger)seconds showSecond:(BOOL) show;

/**
 *  剩余多少秒
 *
 *  @param timeString1 到期时间
 *
 *  @return 到现在多少秒
 */
+ (NSInteger)intervalFromLastDate:(NSString *) timeString1  toTheDate:(NSString *) timeString2;
/**
 *  判断字符串是否是纯数字
 *
 *  @param string <#string description#>
 *
 *  @return <#return value description#>
 */
+(BOOL)isPureNumandCharacters:(NSString *)string;


/**
 *  将秒数转换成时间
 *
 *  @param secondeStr <#secondeStr description#>
 *
 *  @return <#return value description#>
 */
+(NSString *)dataFormatWithTimeSecondStr:(NSString *)secondeStr andFormatStr:(NSString *)formatStr;
//年-月-日 时:分 字符串转日期
-(NSDate*)dateFromStringWithMin:(NSString*)dateString;
@end
