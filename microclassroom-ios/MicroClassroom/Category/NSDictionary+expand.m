//
//  NSDictionary+expand.m
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "NSDictionary+expand.h"

@implementation NSDictionary (expand)


-(id)objectForSafeKey:(NSString *)key{
    NSArray *keyArr = [self allKeys];
    if (keyArr.count) {
        for (NSString *obj in keyArr) {
            if ([obj isEqualToString:key]) {
                return [self objectForKey:key];
            }
        }
    }
    return @"";
}


@end
