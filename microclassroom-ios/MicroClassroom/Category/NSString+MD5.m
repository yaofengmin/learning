//
//  NSString+MD5.m
//  TZS
//
//  Created by yandi on 14/12/9.
//  Copyright (c) 2014年 NongFuSpring. All rights reserved.
//

#import "NSString+MD5.h"
#import <CommonCrypto/CommonCrypto.h>
#import <CoreLocation/CoreLocation.h>

@implementation NSString (MD5)
+ (NSString *)md5:(NSString *)originalStr {
    const char *cStr = [originalStr UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result); // This is the md5 call
    
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
    
}



+(NSString*)CurrentTime1970{
    
    NSDate *date = [NSDate date];
    NSTimeInterval time  = date.timeIntervalSince1970;
    NSString *timeStr = [NSString stringWithFormat:@"%.f",time];
    return timeStr;
    
}

+(NSString *)userLocWith:(double) uerLog andLat:(double) userLat PublisLocWith:(double) Plog andLat:(double) Plat{
    
    CLLocation *userL = [[CLLocation alloc] initWithLatitude:userLat longitude:uerLog];
    CLLocation *PubL  = [[CLLocation alloc] initWithLatitude:Plat longitude:Plog];
    CLLocationDistance kilometers = [userL distanceFromLocation:PubL] / 1000;
    
    return [NSString stringWithFormat:@"%.2fkm",kilometers];
}

+(NSString*)TimeToCurrentTime:(NSInteger)time{
    
    NSInteger giveTime = time ;
    NSInteger returnTime ;
    NSInteger  mm      = 60;       //分
    NSInteger  hh      = mm * 60;  // 时
    NSInteger  dd      = hh * 24 ; // 天
    NSInteger  MM      = dd * 30;  // 月
    NSInteger  yy      = MM * 12;  // 年
    
    if (giveTime < mm) {
        return [NSString stringWithFormat:@"%ld秒前",(long)giveTime];//秒
    }else if(mm       <= giveTime && giveTime<hh){
        returnTime  = giveTime / mm ;
        return [NSString stringWithFormat:@"%ld分钟前",(long)returnTime];//分
    }else if (hh      <= giveTime && giveTime < dd){
        returnTime     =  giveTime /hh;
        return [NSString stringWithFormat:@"%ld小时前",(long)returnTime];
    }else if (dd      <= giveTime && giveTime < MM){
        returnTime     = giveTime / dd ;
        return [NSString stringWithFormat:@"%ld天前",(long)returnTime];
    }else if (MM      <= giveTime && giveTime < yy){
        returnTime     = giveTime / MM ;
        return [NSString stringWithFormat:@"%ld月前",(long)returnTime];
    }else if (yy <= giveTime){
        returnTime    = giveTime / yy ;
        return [NSString stringWithFormat:@"%ld年前",(long)returnTime];
    }
    return @"0秒前";
}



+(NSString*)CurrentTimeByStrHaveSecond:(BOOL)have{
    NSDate *date = [NSDate date];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];//格式化
    if (have) {
//        hh 小时，mm 分钟，ss 秒
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    }else{
        [df setDateFormat:@"yyyy-MM-dd"];
    }
    
    NSString * nowTimeStr = [df stringFromDate:date];
    
    return nowTimeStr;
}


+(NSString *)lessSecondToDay:(NSUInteger)seconds showSecond:(BOOL) show {
    
    NSUInteger day  = (NSUInteger)seconds/(24*3600);
    NSUInteger hour = (NSUInteger)(seconds%(24*3600))/3600;
    NSUInteger min  = (NSUInteger)(seconds%(3600))/60;
    NSUInteger second = (NSUInteger)(seconds%60);
    
    NSString *time;
    
    if (day ==0) {
        if (show) {
            time = [NSString stringWithFormat:@"%lu时%lu分%lu秒",(unsigned long)hour,(unsigned long)min,(unsigned long)second];
            if (hour == 0) {
                time = [NSString stringWithFormat:@"%lu分%lu秒",(unsigned long)min,(unsigned long)second];
            }
        }else
        {
            time = [NSString stringWithFormat:@"%lu时%lu分",(unsigned long)hour,(unsigned long)min];
            if (hour == 0) {
                time = [NSString stringWithFormat:@"%lu分",(unsigned long)min];
            }
        }
    }else
    {
        if (show) {
            time = [NSString stringWithFormat:@"%lu天%lu时%lu分%lu秒 ",(unsigned long)day,(unsigned long)hour,(unsigned long)min,(unsigned long)second];
        }else
        {
            time = [NSString stringWithFormat:@"%lu天%lu时%lu分",(unsigned long)day,(unsigned long)hour,(unsigned long)min];
        }
        
    }
    
    
    return time;
}


+ (NSInteger)intervalFromLastDate:(NSString *)timeString1  toTheDate:(NSString *) timeString2
{
        NSDateFormatter *date=[[NSDateFormatter alloc] init];
        [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *d1=[date dateFromString:timeString1];
        NSTimeInterval late1=[d1 timeIntervalSince1970];
        NSDate *d2=[date dateFromString:timeString2];
        NSTimeInterval late2=[d2 timeIntervalSince1970];
        NSTimeInterval cha=late2-late1;
        return cha;
}

+(BOOL)isPureNumandCharacters:(NSString *)string
{
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(string.length > 0)
    {
        return NO;
    }
    return YES;
}


+(NSString *)dataFormatWithTimeSecondStr:(NSString *)secondeStr andFormatStr:(NSString *)formatStr
{
    if ([secondeStr integerValue]==0) {
        return nil;
    }
    
    NSDate  *date = [NSDate dateWithTimeIntervalSinceNow:-[secondeStr integerValue]] ;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format  setDateFormat:formatStr];
    
    NSString *timeStr = [format stringFromDate:date];
    return timeStr;
    
}

-(NSDate*)dateFromStringWithMin:(NSString*)dateString{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate* date=[format dateFromString:dateString];
    
    return date;
}



@end
