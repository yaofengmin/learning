//
//  NSDictionary+expand.h
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (expand)

/**
 *  
 *判断 key 在dic 里是否存在
 */

-(id)objectForSafeKey:(NSString *)key;
@end
