//
//  UIView+expand.h
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (expand)
/**
 *  获取当前 view 的 viewController
 */
- (UIViewController*)viewController;

- (void)removeAllSubviews;
@end
