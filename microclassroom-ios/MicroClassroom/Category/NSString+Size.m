//
//  NSString+Size.m
//  MicroClassroom
//
//  Created by DEMO on 16/8/9.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import "NSString+Size.h"

@implementation NSString (Size)

- (CGSize)sizeWithFontSize:(CGFloat)fontSize andRectSize:(CGSize)size {
    
    if (self.length == 0) {
        return CGSizeZero;
    }
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]};
    
    CGSize strSize = [self boundingRectWithSize:size
                                        options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                     attributes:attribute
                                        context:nil].size;
    
    return strSize;
    
}
@end
