//
//  YhjRootViewController.h
//  MicroClassroom
//
//  Created by Hanks on 16/7/30.
//  Copyright © 2016年 Hanks. All rights reserved.
//

#import <RDVTabBarController/RDVTabBarController.h>

@interface YhjRootViewController : RDVTabBarController

-(void)setupUnreadMessageCount;
@end
